package com.pigeon.models;

import org.junit.Assert;
import org.junit.Test;

public class LocationTest {

    @Test
    public void should_serialize_to_json() throws Exception {
        Location location = new Location(41, 29);
        String result = location.toJson().toString();
        Assert.assertEquals("{\"longitude\":29,\"latitude\":41}", result);
    }
}
