package com.pigeon.models;

import org.junit.Assert;
import org.junit.Test;

public class RetentionTest {

    @Test
    public void should_serialize_to_json() throws Exception {
        Retention retention = new Retention("123123", "token1", "push12", "O");
        String result = retention.toJson().toString();
        Assert.assertEquals(result, "{\"pushId\":\"push12\",\"status\":\"O\",\"token\":\"token1\",\"key\":\"123123\"}");
    }
}
