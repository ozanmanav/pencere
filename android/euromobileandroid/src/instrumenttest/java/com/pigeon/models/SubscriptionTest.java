package com.pigeon.models;

import org.junit.Assert;
import org.junit.Test;

public class SubscriptionTest {
    @Test
    public void should_serialize_to_json() throws Exception {
        Subscription subscription = new Subscription( "appkey1", "os1", "osVersion1",
                "deviceType1", "deviceName1", "carrier1", "local1", "identifierForVendor1",
                "advertisingIdentifier1", 0);
        subscription.add("location", new Location(23.3D, 41.2D));
        subscription.add("email", "uysalmo@gmail.com");
        subscription.add("msisdn", "123-123-123-123");
        subscription.add("keyID", "123123123");
        subscription.setAppVersion("appVersion1");
        subscription.setToken("token1");
        String result = subscription.toJson().toString();
        Assert.assertEquals("{\"os\":\"os1\",\"advertisingIdentifier\":\"advertisingIdentifier1\",\"osVersion\":\"osVersion1\",\"identifierForVendor\":\"identifierForVendor1\",\"extra\":{\"location\":{\"longitude\":41.2,\"latitude\":23.3},\"email\":\"uysalmo@gmail.com\",\"msisdn\":\"123-123-123-123\",\"keyID\":\"123123123\"},\"firstTime\":0,\"deviceName\":\"deviceName1\",\"token\":\"token1\",\"appVersion\":\"appVersion1\",\"deviceType\":\"deviceType1\",\"carrier\":\"carrier1\",\"local\":\"local1\",\"appKey\":\"appkey1\"}", result);
    }
}
