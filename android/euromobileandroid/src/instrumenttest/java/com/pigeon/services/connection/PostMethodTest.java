package com.pigeon.services.connection;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;

@RunWith(MockitoJUnitRunner.class)
public class PostMethodTest {

    @Mock
    private HttpURLConnection httpURLConnection;

    @Mock
    private OutputStream outputStream;

    @Test
    public void should_set_request_type_post_and_add_headers_to_connection() throws Exception {
        PostMethod postMethod = new PostMethod(httpURLConnection);
        postMethod.setBody("a");
        Mockito.when(httpURLConnection.getOutputStream()).thenReturn(outputStream);

        BaseMatcher arrayStartingWithA = new BaseMatcher() {
            @Override
            public void describeTo(Description description) {
            }

            // check that first character is A
            @Override
            public boolean matches(Object item) {
                byte[] actual = (byte[]) item;
                return actual[0] == 'a';
            }
        };


        postMethod.executeInternal(httpURLConnection);
        Mockito.verify(httpURLConnection).setRequestMethod("POST");
        Mockito.verify(httpURLConnection).setRequestProperty("Content-Type", "application/json");
        Mockito.verify(httpURLConnection).setRequestProperty("Accept", "application/json");
        Mockito.verify(outputStream).close();
        Mockito.verify(outputStream).flush();
        Mockito.verify(outputStream).write((byte[]) Matchers.argThat(arrayStartingWithA), Matchers.eq(0), Matchers.eq(1));
    }


    @Test
    public void should_set_response_and_status_code_to_result() throws IOException {
        PostMethod postMethod = new PostMethod(httpURLConnection);
        postMethod.setBody("a");
        Mockito.when(httpURLConnection.getOutputStream()).thenReturn(outputStream);
        InputStream fakeStream = new ByteArrayInputStream("content".getBytes("utf-8"));
        Mockito.when(httpURLConnection.getInputStream()).thenReturn(fakeStream);
        Mockito.when(httpURLConnection.getResponseCode()).thenReturn(200);
        HttpResult result = postMethod.execute(0);
        Assert.assertEquals("content", result.getResponse());
        Assert.assertEquals(200, result.getStatusCode());
    }

}
