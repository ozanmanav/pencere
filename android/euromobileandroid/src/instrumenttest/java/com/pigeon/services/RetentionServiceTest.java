package com.pigeon.services;

import com.pigeon.models.Retention;
import com.pigeon.services.RetentionService;
import com.pigeon.services.connection.HttpConnectionService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class RetentionServiceTest {

    @Mock
    private HttpConnectionService httpConnectionService;

    @Test
    public void should_convert_and_call_retention_service() throws Exception {

        RetentionService retentionService = new RetentionService(httpConnectionService);
        Retention retention = new Retention("key", "token", "pushId", "status");

        retentionService.save(retention);

        Mockito.verify(httpConnectionService).post(":4242/retention", "{\"pushId\":\"pushId\",\"status\":\"status\",\"token\":\"token\",\"key\":\"key\"}");
    }
}
