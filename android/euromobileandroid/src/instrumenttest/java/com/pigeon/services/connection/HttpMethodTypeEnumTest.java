package com.pigeon.services.connection;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;

@RunWith(MockitoJUnitRunner.class)
public class HttpMethodTypeEnumTest {

    @Mock
    private HttpURLConnection httpURLConnection;

    @Test
    public void should_return_get_method_when_http_method_type_is_get() throws MalformedURLException {
        HttpMethod httpMethod = HttpMethodType.Get.create(httpURLConnection);
        Assert.assertEquals(GetMethod.class, httpMethod.getClass());
    }

    @Test
    public void should_return_post_method_when_http_method_type_is_post() throws MalformedURLException {
        HttpMethod httpMethod = HttpMethodType.Post.create(httpURLConnection);
        Assert.assertEquals(PostMethod.class, httpMethod.getClass());
    }
}
