package com.pigeon.services;

import com.pigeon.models.Subscription;
import com.pigeon.services.connection.HttpConnectionService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class SubscriptionServiceTest {

    @Mock
    private HttpConnectionService httpConnectionService;

    @Test
    public void should_convert_and_call_retention_service() throws Exception {

        SubscriptionService subscriptionService = new SubscriptionService(httpConnectionService);
        Subscription subscription = new Subscription( "2", "3", "4", "5", "6", "7", "8", "9", "10", 0);
        subscription.setToken("1");
        subscription.setAppVersion("12");

        subscriptionService.save(subscription);

        Mockito.verify(httpConnectionService).post(":4243/subscription", "{\"os\":\"3\",\"advertisingIdentifier\":\"10\",\"osVersion\":\"4\",\"identifierForVendor\":\"9\",\"extra\":{},\"firstTime\":0,\"deviceName\":\"6\",\"token\":\"1\",\"appVersion\":\"12\",\"deviceType\":\"5\",\"carrier\":\"7\",\"local\":\"8\",\"appKey\":\"2\"}");
    }
}
