package com.pigeon.services.connection;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;

@RunWith(MockitoJUnitRunner.class)
public class GetMethodTest {

    @Mock
    private HttpURLConnection httpURLConnection;

    @Test
    public void should_set_request_type_get_to_connection() throws Exception {
        GetMethod getMethod = new GetMethod(httpURLConnection);
        getMethod.executeInternal(httpURLConnection);
        Mockito.verify(httpURLConnection).setRequestMethod("GET");
    }

    @Test
    public void should_set_response_and_status_code_to_result() throws IOException {
        GetMethod getMethod = new GetMethod(httpURLConnection);
        InputStream fakeStream = new ByteArrayInputStream("content".getBytes("utf-8"));
        Mockito.when(httpURLConnection.getInputStream()).thenReturn(fakeStream);
        Mockito.when(httpURLConnection.getResponseCode()).thenReturn(200);
        HttpResult result = getMethod.execute(0);
        Assert.assertEquals("content", result.getResponse());
        Assert.assertEquals(200, result.getStatusCode());
    }

}
