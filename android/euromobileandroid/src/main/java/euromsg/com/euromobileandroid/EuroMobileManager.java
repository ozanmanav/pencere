package euromsg.com.euromobileandroid;


import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.gson.Gson;

import euromsg.com.euromobileandroid.activity.PopupActivity;
import euromsg.com.euromobileandroid.connection.ConnectionManager;
import euromsg.com.euromobileandroid.connection.EuroMsgCallback;
import euromsg.com.euromobileandroid.enums.MessageStatus;
import euromsg.com.euromobileandroid.model.Location;
import euromsg.com.euromobileandroid.model.Message;
import euromsg.com.euromobileandroid.model.Retention;
import euromsg.com.euromobileandroid.model.Subscription;
import euromsg.com.euromobileandroid.service.EuroRegisterService;
import euromsg.com.euromobileandroid.utils.EuroLogger;
import euromsg.com.euromobileandroid.utils.Utils;
import retrofit2.Call;

/**
 * Created by ozanuysal on 25/01/15.
 */
public class EuroMobileManager {

    private static EuroMobileManager instance;

    private Context context;
    public Subscription subscription = new Subscription();
    public String senderId;

    public EuroMobileManager(String applicationKey, Context context) {

        this.context = context;
        subscription.setFirstTime(1);
        subscription.setAppKey(applicationKey);
        subscription.setCarrier(Utils.carrier(context));
        subscription.setAppVersion(Utils.appVersion(context));
        subscription.setDeviceName(Utils.deviceName());
        subscription.setDeviceType(Utils.deviceType());
        subscription.setIdentifierForVendor(Utils.deviceUDID(context));
        subscription.setLocal(Utils.local(context));
        subscription.setOs(Utils.osType());
        subscription.setOsVersion(Utils.osVersion());
        subscription.setSdkVersion(Constants.SDK_VERSION);

        if(Utils.hasPrefString(context, Constants.EURO_SUBSCRIPTION_KEY)) {
            Log.d("prefString ", Utils.getPrefString(context, Constants.EURO_SUBSCRIPTION_KEY));
            Subscription oldSubcription = new Gson().fromJson(Utils.getPrefString(context, Constants.EURO_SUBSCRIPTION_KEY), Subscription.class);
            Log.d("old subs", "pushPermit:" + oldSubcription.getPushPermit() + " token:" + oldSubcription.getToken());
            subscription.addAll(oldSubcription.getExtra());
            subscription.setToken(oldSubcription.getToken());
            subscription.setAdvertisingIdentifier(oldSubcription.getAdvertisingIdentifier());
            subscription.setFirstTime(0);
        }
    }

    public static EuroMobileManager sharedManager(Context context, String applicationKey, String senderId) {
        if (instance == null) {
            instance = new EuroMobileManager(applicationKey,context);
        }
        instance.senderId = senderId;
        EuroLogger.debugLog("SharedManager App Key : " + instance.subscription.getAppKey());
        return instance;
    }

    public static EuroMobileManager getInstance() {
        return instance;
    }

    public void registerToGCM() {
        Intent intent = new Intent(context, EuroRegisterService.class);
        context.startService(intent);
    }

    public String getSenderId() {
        return senderId;
    }

    public void reportReceived(String pushId) {

        if(pushId != null) {
            EuroLogger.debugLog("Report Received : " + pushId);

            Retention retention = new Retention();
            retention.setKey(subscription.getAppKey());
            retention.setPushId(pushId);
            retention.setStatus(MessageStatus.Received.toString());
            retention.setToken(subscription.getToken());
            Call call = ConnectionManager.getInstance().getRetentionService().retention(retention);
            call.enqueue(new EuroMsgCallback());
        } else {
            EuroLogger.debugLog("reportReceived : Push Id cannot be null!");
        }
    }

    public void reportRead(String pushId) {

        if(pushId != null) {
            EuroLogger.debugLog("Report Read : " + pushId);
            Retention retention = new Retention();
            retention.setKey(subscription.getAppKey());
            retention.setPushId(pushId);
            retention.setStatus(MessageStatus.Read.toString());
            retention.setToken(subscription.getToken());
            Call call = ConnectionManager.getInstance().getRetentionService().retention(retention);
            call.enqueue(new EuroMsgCallback());
        } else {
            EuroLogger.debugLog("reportRead : Push Id cannot be null!");
        }
    }

    public void reportRead(Message message) {
        reportRead(message.getPushId());
    }

    public void reportReceived(Message message) { reportReceived(message.getPushId());}

    public void subscribe(String token) {

        this.subscription.setToken(token);
        sync();
    }

    public void setPermit(Boolean permit){
        String permitChar = permit?"Y":"N";
        this.subscription.add("pushPermit", permitChar);
        Log.d("permit", this.subscription.toJson());
        sync();
    }

    public void sync() {
        EuroLogger.debugLog("Sync "+this.subscription.getToken() + this.subscription.getAppKey() + this.subscription.getFirstTime());
        if(this.subscription.isValid()) {
            saveSubscription();
            Call call = ConnectionManager.getInstance().getSubscriptionService().subscribe(this.subscription);
            call.enqueue(new EuroMsgCallback());
        }

    }

    public void setAppVersion(String appVersion) {
        this.subscription.setAppVersion(appVersion);
    }

    public void setTwitterId(String twitterId) {
        setSubscriptionProperty(Constants.EURO_TWITTER_KEY, twitterId);
    }

    public void setEmail(String email) {
        setSubscriptionProperty(Constants.EURO_EMAIL_KEY,email);
    }

    public void setFacebook(String facebookId) {
        setSubscriptionProperty(Constants.EURO_FACEBOOK_KEY,facebookId);
    }

    public void setLocation(double latitude, double longitude) {
        setSubscriptionProperty(Constants.EURO_LOCATION_KEY, new Location(latitude,longitude));
    }

    public void setEuroUserKey(String userKey) {
        setSubscriptionProperty(Constants.EURO_USER_KEY, userKey);
    }

    public void setMsisdn(String msisdn) {
        setSubscriptionProperty(Constants.EURO_MSISDN_KEY, msisdn);
    }

    public void setUserProperty(String key, String value) {
        setSubscriptionProperty(key, value);
    }

    public void removeUserProperties() {
        this.subscription.removeAll();
        saveSubscription();
    }

    private void saveSubscription() {
        try {
            EuroLogger.debugLog("save subs " + Constants.EURO_SUBSCRIPTION_KEY + " " + this.subscription.toJson());
            Utils.savePrefString(context, Constants.EURO_SUBSCRIPTION_KEY, this.subscription.toJson());
            EuroLogger.debugLog("subs saved " + Utils.getPrefString(this.context,Constants.EURO_SUBSCRIPTION_KEY));

        } catch (Exception e) {}
    }

    private void setSubscriptionProperty(String key, Object value) {

        this.subscription.add(key, value);
        saveSubscription();
    }

    public void showPopup(IntentService service, Intent intent) {
        synchronized (this)
        {
            intent.putExtra("EuroMsgKey",subscription.getAppKey());
            service.startActivity(new Intent(service, PopupActivity.class).putExtras(intent).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }
    }

    public void setVisiUrl(String visiUrl) {
        ConnectionManager.getInstance().get(visiUrl);
    }
}
