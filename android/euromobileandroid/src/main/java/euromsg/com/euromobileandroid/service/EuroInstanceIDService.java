package euromsg.com.euromobileandroid.service;

import android.content.Intent;

import com.google.android.gms.iid.InstanceIDListenerService;

/**
 * Created by ozanuysal on 02/04/16.
 */
public class EuroInstanceIDService extends InstanceIDListenerService {

    @Override
    public void onTokenRefresh() {
        Intent intent = new Intent(this, EuroRegisterService.class);
        startService(intent);
    }
}