package euromsg.com.euromobileandroid.service;

import android.app.IntentService;
import android.content.Intent;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import euromsg.com.euromobileandroid.EuroMobileManager;
import euromsg.com.euromobileandroid.utils.EuroLogger;

/**
 * Created by ozanuysal on 30/03/16.
 */
public class EuroRegisterService extends IntentService {

    private static final String TAG = "EuroRegisterService";

    public EuroRegisterService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        EuroLogger.debugLog("RegistrationIntentService started");
        try {

            InstanceID instanceID = InstanceID.getInstance(this);
            String token = instanceID.getToken(EuroMobileManager.getInstance().getSenderId(),
                    GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);

            EuroLogger.debugLog("GCM Registration Token: " + token);

            sendRegistrationToServer(token);

        } catch (Exception e) {
            EuroLogger.debugLog(e.getMessage());
            EuroLogger.debugLog("Failed to complete token refresh");
        }
    }

    private void sendRegistrationToServer(String token) {
        EuroMobileManager.getInstance().subscribe(token);
    }


}
