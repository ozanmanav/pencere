package euromsg.com.euromobileandroid.service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import androidx.core.app.NotificationCompat;
import androidx.core.content.IntentCompat;
import android.text.TextUtils;

import com.google.android.gms.gcm.GcmListenerService;

import euromsg.com.euromobileandroid.EuroMobileManager;
import euromsg.com.euromobileandroid.connection.ConnectionManager;
import euromsg.com.euromobileandroid.enums.PushType;
import euromsg.com.euromobileandroid.model.Message;
import euromsg.com.euromobileandroid.utils.EuroLogger;
import euromsg.com.euromobileandroid.utils.Utils;

/**
 * Created by ozanuysal on 02/04/16.
 */
public class EuroGcmListenerService extends GcmListenerService {

    @Override
    public void onMessageReceived(String from, final Bundle data) {

        Message pushMessage = new Message(data);

        EuroLogger.debugLog("Message received : " + pushMessage.getMessage());
        if(!TextUtils.isEmpty(pushMessage.getMessage())) {

            if(pushMessage.getPushType() == PushType.Image) {
                generateNotification(getApplicationContext(), data, ConnectionManager.getInstance().getBitmap(pushMessage.getMediaUrl()));
            } else {
                generateNotification(getApplicationContext(), data, null);
            }

        }
        EuroMobileManager.getInstance().reportReceived(pushMessage.getPushId());
    }

    private void generateNotification(Context context, Bundle data, Bitmap image) {
        try {
            Message pushMessage = new Message(data);
            NotificationManager mNotificationManager = (NotificationManager) context
                    .getSystemService(Context.NOTIFICATION_SERVICE);
            // find and start the launcher activity
            PackageManager packageManager = context.getPackageManager();
            final ApplicationInfo applicationInfo = packageManager.getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
            final int appIconResId = applicationInfo.icon;
            Intent intent = packageManager.getLaunchIntentForPackage(context.getPackageName());
            ComponentName componentName = intent.getComponent();
            Intent notificationIntent = Intent.makeRestartActivityTask(componentName);
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            notificationIntent.putExtras(data);
            PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

            NotificationCompat.Style style = image == null ?
                    new NotificationCompat.BigTextStyle().bigText(pushMessage.getMessage()) :
                    new NotificationCompat.BigPictureStyle().bigPicture(image).setSummaryText(pushMessage.getMessage());

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                    context)
                    .setSmallIcon(appIconResId)
                    .setAutoCancel(true)
                    .setContentTitle(Utils.getAppLabel(getApplicationContext(), ""))
                    .setStyle(style)
                    .setContentText(pushMessage.getMessage());
            mBuilder.setContentIntent(contentIntent);

            mNotificationManager.notify(12, mBuilder.build());
        } catch(Exception e) {}
    }
}
