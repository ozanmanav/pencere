package euromsg.com.euromobileandroid.connection;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import java.net.HttpURLConnection;
import java.net.URL;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ozanuysal on 30/03/16.
 */
public final class ConnectionManager {

    private static ConnectionManager instance;

    public static ConnectionManager getInstance() {
        return instance;
    }

    private SubscriptionService subscriptionService;
    private RetentionService retentionService;

    static {
        instance = new ConnectionManager();
    }

    private ConnectionManager() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build();
        Retrofit retentionRetrofit = new Retrofit.Builder()
                .baseUrl("https://pushr.euromsg.com")
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        Retrofit subscriptionRetrofit = new Retrofit.Builder()
                .baseUrl("https://pushs.euromsg.com")
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        subscriptionService = subscriptionRetrofit.create(SubscriptionService.class);
        retentionService = retentionRetrofit.create(RetentionService.class);
    }

    public void get(final String urlString) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                HttpURLConnection urlConnection = null;
                try {
                    urlConnection = (HttpURLConnection) new URL(urlString).openConnection();
                }catch(Exception e){
                }
                finally{
                    if (urlConnection != null) {
                        urlConnection.disconnect();
                    }
                }
                return null;
            }
        }.execute();
    }

    public Bitmap getBitmap(final String urlString) {
        try {
            return BitmapFactory.decodeStream(new URL(urlString).openConnection().getInputStream());
        } catch (Exception e) {
            return null;
        }
    }

    public SubscriptionService getSubscriptionService() {
        return subscriptionService;
    }

    public RetentionService getRetentionService() {
        return retentionService;
    }
}
