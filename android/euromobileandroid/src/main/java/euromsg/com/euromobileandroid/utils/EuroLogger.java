package euromsg.com.euromobileandroid.utils;

import androidx.annotation.NonNull;
import android.util.Log;

import euromsg.com.euromobileandroid.BuildConfig;
import euromsg.com.euromobileandroid.Constants;

/**
 * Created by ozanuysal on 30/03/16.
 */
public class EuroLogger {

    public static final boolean DEBUG = BuildConfig.DEBUG;

    public static void debugLog(@NonNull String message) {
        if(DEBUG) {
            Log.d(Constants.LOG_TAG, message);
        }
    }

}
