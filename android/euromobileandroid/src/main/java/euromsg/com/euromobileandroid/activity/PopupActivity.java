package euromsg.com.euromobileandroid.activity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import euromsg.com.euromobileandroid.EuroMobileManager;
import euromsg.com.euromobileandroid.R;
import euromsg.com.euromobileandroid.enums.PushType;
import euromsg.com.euromobileandroid.model.Message;

/**
 * Created by ozanuysal on 13/08/15.
 */
public class PopupActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_popup);

        final Message message = new Message(getIntent().getExtras());
        ((TextView) findViewById(R.id.textView)).setText(message.getMessage());

        if(message.getPushType() == PushType.Image) {
            Picasso.with(this).load(message.getMediaUrl()).into((ImageView) findViewById(R.id.imageView));
        } else {
            findViewById(R.id.imageView).setVisibility(View.GONE);
        }

        findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(message.getPushType() == PushType.Video) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(message.getMediaUrl()));
                    intent.setDataAndType(Uri.parse(message.getMediaUrl()), "video/mp4");
                    startActivity(intent);
                } else if (!"".equals(message.getUrl())) {
                    Intent intent = null;
                    if (message.getUrl().startsWith("http")) { // if we have a web site, open browser
                        intent = new Intent(Intent.ACTION_VIEW, Uri.parse(message.getUrl()));
                    } else { // parse the deep link defined in app
                        intent = new Intent(getApplicationContext().getPackageName(), Uri.parse(message.getUrl()));
                    }
                    startActivity(intent);
                }
                EuroMobileManager.getInstance().reportRead(message);
                finish();
            }
        });



    }
}
