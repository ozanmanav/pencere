package euromsg.com.euromobileandroid.connection;

import euromsg.com.euromobileandroid.model.Retention;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by ozanuysal on 30/03/16.
 */
public interface RetentionService {

    @POST("retention")
    Call<Object> retention(@Body Retention retention);
}
