package euromsg.com.euromobileandroid.enums;

/**
 * Created by ozanuysal on 25/01/15.
 */
public enum MessageStatus {
    Read("O"), Received("D");

    private final String name;

    private MessageStatus(String s) {
        name = s;
    }

    public boolean equalsName(String otherName) {
        return (otherName != null) && name.equals(otherName);
    }

    public String toString() {
        return name;
    }
}
