package euromsg.com.euromobileandroid;

/**
 * Created by ozanuysal on 25/01/15.
 */
public class Constants {

    public static final String SDK_VERSION = BuildConfig.VERSION_NAME;

    //public static final String EURO_PUSH_ID_KEY = "pushId";
    public static final String EURO_EMAIL_KEY = "email";
    public static final String EURO_TWITTER_KEY = "twitter";
    public static final String EURO_FACEBOOK_KEY = "facebook";
    public static final String EURO_MSISDN_KEY = "msisdn";
    public static final String EURO_USER_KEY = "keyID";
    public static final String EURO_LOCATION_KEY = "location";
    public static final String EURO_SUBSCRIPTION_KEY = "subscription";
    //public static final String EURO_PROPS_KEY = "properties";

    public static final String LOG_TAG = "EuroPush";

}
