package euromsg.com.euromobileandroid.connection;

import euromsg.com.euromobileandroid.model.Subscription;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by ozanuysal on 30/03/16.
 */
public interface SubscriptionService {

    @POST("subscription")
    Call<Object> subscribe(@Body Subscription subscription);
}
