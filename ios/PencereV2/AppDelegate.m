/**
 * Copyright (c) 2015-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */
#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#import "AppDelegate.h"

#import <React/RCTBundleURLProvider.h>
#import <React/RCTRootView.h>
#import <React/RCTLinkingManager.h>
#import "RNQuickActionManager.h"
#import "RNEuromsgEventEmitter.h"
#import "euroiosframework/EuroManager.h"
#import "RNEuromsgEventEmitter.h"
#define APP_KEY @"Pencere_IOS"

#import <Firebase.h>

@implementation AppDelegate

RNEuromsgEventEmitter *eventEmitter;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
  [FIRApp configure];
  [[RNEuromsgEventEmitter sharedEuroManager:APP_KEY] setDebug:YES];
  [[RNEuromsgEventEmitter sharedEuroManager:APP_KEY] registerForPush];
  
  if ([UNUserNotificationCenter class] != nil) {
       // iOS 10 or later
       // For iOS 10 display notification (sent via APNS)
       [UNUserNotificationCenter currentNotificationCenter].delegate = self;
       UNAuthorizationOptions authOptions = UNAuthorizationOptionAlert |
       UNAuthorizationOptionSound | UNAuthorizationOptionBadge;
       [[UNUserNotificationCenter currentNotificationCenter]
        requestAuthorizationWithOptions:authOptions
        completionHandler:^(BOOL granted, NSError * _Nullable error) {
            // ...
        }];
   } else {
       // iOS 10 notifications aren't available; fall back to iOS 8-9 notifications.
       UIUserNotificationType allNotificationTypes =
       (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
       UIUserNotificationSettings *settings =
       [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
       [application registerUserNotificationSettings:settings];
   }
  
  //[GMSServices provideAPIKey:@"_YOUR_API_KEY_"]; // add this line using the api key obtained from Google Console
  NSURL *jsCodeLocation;

  jsCodeLocation = [[RCTBundleURLProvider sharedSettings] jsBundleURLForBundleRoot:@"index" fallbackResource:nil];

  RCTRootView *rootView = [[RCTRootView alloc] initWithBundleURL:jsCodeLocation
                                                      moduleName:@"PencereV2"
                                               initialProperties:nil
                                                   launchOptions:launchOptions];

  rootView.backgroundColor = [[UIColor alloc] initWithRed:1.0f green:1.0f blue:1.0f alpha:1];

  self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
  UIViewController *rootViewController = [UIViewController new];

  rootViewController.view = rootView;

  self.window.rootViewController = rootViewController;
  [self.window makeKeyAndVisible];
  return YES;
}

- (void)application:(UIApplication *)application performActionForShortcutItem:(UIApplicationShortcutItem *)shortcutItem completionHandler:(void (^)(BOOL succeeded)) completionHandler {
  [RNQuickActionManager onQuickActionPress:shortcutItem completionHandler:completionHandler];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
  return [RCTLinkingManager application:application openURL:url
                      sourceApplication:sourceApplication annotation:annotation];
}

- (void) application:(UIApplication *)application didRegisterUserNotificationSettings:
(UIUserNotificationSettings *)notificationSettings {
  NSLog(@"didRegisterUserNotificationSettings");

    [application registerForRemoteNotifications];
}

- (void) application:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
  NSLog(@"didRegisterForRemoteNotificationsWithDeviceToken %@", deviceToken);
  [[RNEuromsgEventEmitter sharedEuroManager:APP_KEY] registerToken:deviceToken];
  
}

- (void) application:(UIApplication *)application
didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
  NSLog(@"Registration failed : %@",error.description);
 
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler {
  // Update the app interface directly.
  NSDictionary *userInfo = notification.request.content.userInfo;
  for(NSString *key in [userInfo allKeys]) {
    NSLog(@"userInfo %@ %@", key, [userInfo objectForKey:key]);
  }
  if(userInfo!=NULL)
    [[RNEuromsgEventEmitter sharedEuroManager:APP_KEY] handlePush:userInfo];
  if( [UIApplication sharedApplication].applicationState == UIApplicationStateInactive )
   {
       NSLog( @"INACTIVE" );
       completionHandler(UNNotificationPresentationOptionSound|UNNotificationPresentationOptionAlert);
   }
   else if( [UIApplication sharedApplication].applicationState == UIApplicationStateBackground )
   {
       NSLog( @"BACKGROUND" );
       completionHandler(UNNotificationPresentationOptionSound|UNNotificationPresentationOptionAlert);
   }
   else
   {
       NSLog( @"FOREGROUND" );
       completionHandler(UNNotificationPresentationOptionSound|UNNotificationPresentationOptionAlert);
   }
}

#pragma mark - UNUserNotificationCenterDelegate

- (void)userNotificationCenter:(UNUserNotificationCenter *)center
didReceiveNotificationResponse:(UNNotificationResponse *)response
         withCompletionHandler:(void (^)(void))completionHandler {
  NSLog(@"response %@",response.notification.request.trigger);
  UIApplication *application = [UIApplication sharedApplication];
  NSDictionary *userInfo = response.notification.request.content.userInfo;
  if(application.applicationState == UIApplicationStateActive){
    // if foreground
    eventEmitter = [RNEuromsgEventEmitter allocWithZone: nil];
    [eventEmitter  tellJS:@"opened" withBody:userInfo];
    if ([response.actionIdentifier isEqualToString:UNNotificationDismissActionIdentifier]) {
      // The user dismissed the notification without taking action.
    }
    else if ([response.actionIdentifier isEqualToString:UNNotificationDefaultActionIdentifier]) {
      if(userInfo!=nil)
      {
      for(NSString *key in [userInfo allKeys]) {
        NSLog(@"didReceiveNotificationResponse userInfo %@ %@", key, [userInfo objectForKey:key]);
      }
    }
    // Else handle any custom actions. . .
   
  }
}
  [[RNEuromsgEventEmitter sharedEuroManager:APP_KEY] handlePush:userInfo];
  completionHandler();
}

@end
