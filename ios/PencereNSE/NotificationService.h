//
//  NotificationService.h
//  PencereNSE
//
//  Created by Sait Banazılı on 10.12.2018.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import <UserNotifications/UserNotifications.h>

@interface NotificationService : UNNotificationServiceExtension

@end
