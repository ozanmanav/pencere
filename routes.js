import React, { Component } from "react";
import { Router, Stack, Scene } from "react-native-router-flux";

import Login from "./components/login";
import ProductList from "./components/productList";

export default class Routes extends Component<{}> {
  render() {
    return (
      <Router>
        <Stack key="root" hideNavbar="true">
          <Scene key="login" component={Login} />
          <Scene key="productList" component={ProductList} title="Register" />
        </Stack>
      </Router>
    );
  }
}
