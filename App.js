if (!__DEV__) {
  console.log = () => {};
}
var visilabsManager = require("@app/visilabsManager");

import React, { Component } from "react";
import { Platform, UIManager, Alert, BackHandler } from "react-native";
import { createSwitchNavigator, createAppContainer } from "react-navigation";

import { createStackNavigator } from "react-navigation-stack";
import { createBottomTabNavigator } from "react-navigation-tabs";
import { SafeAreaProvider } from "react-native-safe-area-context";
import { TabBar, TabBarIcon } from "@components";
import {
  AraSayfaModalScreen,
  LoginScreen,
  RouterScreen,
  OrdersScreen,
  OrderDetailsScreen,
  GridScreen,
  HomeScreen,
  PaymentOptionsScreen,
  AddressOptionsScreen,
  NewAddressScreen,
  OrderConfirmationScreen,
  OrderResultScreen,
  PriceSettingsScreen,
  SettingsScreen,
  ListingSettingsScreen,
  AnnouncementSettingsScreen,
  HelpScreen,
  HelpDetailsScreen,
  SupportScreen,
  MyAccountOthersScreen,
  AcikCeklerScreen,
  AcikKalemlerScreen,
  HesapEkstresiScreen,
  HesapEkstresiDetailsScreen,
  AcikKalemDetailsScreen,
  AcikCekDetailsScreen,
  SearchFilterScreen,
  ProductDetailsScreen,
  ShoppingCartScreen,
  ShippingScreen,
  FavoritesScreen,
  SearchScreen,
  EditOrderScreen,
  AnnouncementsScreen,
  AnnouncementDetailsScreen,
  MyAccountScreen,
  OthersScreen,
  ContactScreen,
  PaynetCampaignsScreen,
  InvoicesListScreen,
  InvoiceDetailsScreen,
  CambiumScreen,
  CheckoutScreen,
  PriceRequestsScreen,
  BankAccountsScreen,
  MusteriTemsilcisiScreen,
  GarantiScreen,
  CouponsScreen,
  UserManagementScreen,
} from "@screens";
import store from "@redux/store";
import SplashScreen from "react-native-splash-screen";
import NavigationService from "./src/NavigationService";
import QuickActions from "react-native-quick-actions";
import Euromsg from "react-native-euromsg";
import analytics from "@react-native-firebase/analytics";
import axios from "axios";
import { Provider } from "react-redux";
import TopLevelComponent from "./TopLevelComponent";
import { bugsnag } from "@common/bugsnag_config";

const prefix = Platform.OS === "android" ? "pencere://pencere/" : "pencere://";

QuickActions.clearShortcutItems();

QuickActions.setShortcutItems([
  {
    type: "Account", // Required
    title: "Hesap Ekstresi", // Optional, if empty, `type` will be used instead
    icon: "account", // Pass any of UIApplicationShortcutIconType<name>
    userInfo: {
      url: "account/", // provide custom data, like in-app url you want to open
    },
  },
  {
    type: "Orders", // Required
    title: "Siparişlerim", // Optional, if empty, `type` will be used instead
    icon: "orders", // Pass any of UIApplicationShortcutIconType<name>
    userInfo: {
      url: "/orders", // provide custom data, like in-app url you want to open
    },
  },
  {
    type: "Search", // Required
    title: "Arama", // Optional, if empty, `type` will be used instead
    icon: "magnifier", // Pass any of UIApplicationShortcutIconType<name>
    userInfo: {
      url: "app://SearchScreen", // provide custom data, like in-app url you want to open
    },
  },
]);
const LoginStack = createStackNavigator(
  {
    LoginScreen: { screen: LoginScreen },
  },
  {
    headerMode: "none",
    initialRouteName: "LoginScreen",
  },
);

const HomeScreenTabNavigator = createBottomTabNavigator(
  {
    HomeStack: {
      screen: createStackNavigator(
        {
          HomeScreen: {
            screen: HomeScreen,
            defaultNavigationOptions: {
              cardStyle: {
                backgroundColor: "#efefef",
                opacity: 1,
                shadowColor: "transparent",
              },
            },
          },
          ProductDetailsScreen: {
            key: "home",
            screen: ProductDetailsScreen,
          },
        },
        {
          headerMode: "none",
          initialRouteName: "HomeScreen",
          cardStyle: {
            backgroundColor: "#efefef",
          },
          navigationOptions: {
            header: null,
            tabBarIcon: ({ tintColor }) => (
              <TabBarIcon
                style={{ width: 18, height: 18 }}
                icon={require("@assets/images/home.png")}
                label="Anasayfa"
                tintColor={tintColor}
              />
            ),
          },
        },
      ),
    },
    ShoppingCartStack: {
      screen: createStackNavigator(
        {
          ShoppingCartScreen: {
            screen: ShoppingCartScreen,
          },
          ProductDetailsScreen: {
            key: "cart",
            screen: ProductDetailsScreen,
          },
          PriceRequestsScreen: {
            key: "cart",
            screen: PriceRequestsScreen,
          },
        },
        {
          headerMode: "none",
          initialRouteName: "ShoppingCartScreen",
          cardStyle: {
            backgroundColor: "#efefef",
            opacity: 1,
            shadowColor: "transparent",
          },
        },
      ),
    },
    SearchStack: {
      screen: createStackNavigator(
        {
          SearchScreen: {
            screen: SearchScreen,
          },
          SearchFilterScreen: {
            screen: SearchFilterScreen,
          },
          ProductDetailsScreen: {
            key: "search",
            screen: ProductDetailsScreen,
          },
        },
        {
          headerMode: "none",
          initialRouteName: "SearchScreen",
          navigationOptions: {
            header: null,
            tabBarIcon: ({ tintColor }) => (
              <TabBarIcon
                style={{ width: 18, height: 18 }}
                icon={require("@assets/images/search.png")}
                label="Arama"
                tintColor={tintColor}
              />
            ),
          },
        },
      ),
    },

    HelpStack: {
      screen: createStackNavigator(
        {
          HelpScreen: {
            screen: HelpScreen,
          },
          HelpDetailsScreen: {
            screen: HelpDetailsScreen,
          },
        },
        {
          headerMode: "none",
          initialRouteName: "HelpScreen",
        },
      ),
    },
    AnnouncementsStack: {
      screen: createStackNavigator(
        {
          AnnouncementsScreen: {
            key: "AnnouncementsScreen",
            screen: AnnouncementsScreen,
          },
          AnnouncementDetailsScreen: {
            key: "stack",
            screen: AnnouncementDetailsScreen,
          },
        },
        {
          headerMode: "none",
          initialRouteName: "AnnouncementsScreen",
        },
      ),
    },
    GridScreen: { key: "tabGrid", screen: GridScreen },
    FavoritesStack: {
      screen: createStackNavigator(
        {
          FavoritesScreen: {
            screen: FavoritesScreen,
          },
          ProductDetailsScreen: {
            key: "favorites",
            screen: ProductDetailsScreen,
          },
        },
        {
          headerMode: "none",
          initialRouteName: "FavoritesScreen",
          navigationOptions: {
            header: null,
            tabBarIcon: ({ tintColor }) => (
              <TabBarIcon
                style={{ width: 18, height: 18 }}
                icon={require("@assets/images/star.png")}
                label="Favorilerim"
                tintColor={tintColor}
              />
            ),
          },
        },
      ),
    },
    MyAccountStack: {
      screen: createStackNavigator(
        {
          MyAccountScreen: {
            screen: MyAccountScreen,
          },
          HesapEkstresiScreen: {
            screen: HesapEkstresiScreen,
          },
          HesapEkstresiDetailsScreen: {
            screen: HesapEkstresiDetailsScreen,
          },
          GarantiScreen: {
            screen: GarantiScreen,
          },
          AcikKalemlerScreen: {
            screen: AcikKalemlerScreen,
          },
          AcikKalemDetailsScreen: {
            screen: AcikKalemDetailsScreen,
          },
          UserManagementScreen: {
            screen: UserManagementScreen,
          },
          MyAccountOthersScreen: {
            screen: MyAccountOthersScreen,
          },
          AcikCeklerScreen: {
            screen: AcikCeklerScreen,
          },
          AcikCekDetailsScreen: {
            screen: AcikCekDetailsScreen,
          },
          OrdersStack: {
            path: "orders",
            screen: createStackNavigator(
              {
                OrdersScreen: {
                  screen: OrdersScreen,
                },
                OrderDetailsScreen: { screen: OrderDetailsScreen },
              },
              {
                headerMode: "none",
                initialRouteName: "OrdersScreen",
              },
            ),
          },
          SettingsScreen: { screen: SettingsScreen },
          PriceSettingsScreen: { screen: PriceSettingsScreen },
          PriceRequestsScreen: {
            screen: PriceRequestsScreen,
          },
          ListingSettingsScreen: { screen: ListingSettingsScreen },
          AnnouncementSettingsScreen: { screen: AnnouncementSettingsScreen },
          PaynetCampaignsScreen: { screen: PaynetCampaignsScreen },
          CouponsScreen: { screen: CouponsScreen },
          CambiumScreen: { screen: CambiumScreen },
          InvoicesListStack: {
            screen: createStackNavigator(
              {
                InvoicesListScreen: { screen: InvoicesListScreen },
                InvoiceDetailsScreen: { screen: InvoiceDetailsScreen },
              },
              {
                headerMode: "none",
                initialRouteName: "InvoicesListScreen",
              },
            ),
          },
        },
        {
          headerMode: "none",
          initialRouteName: "MyAccountScreen",
          navigationOptions: {
            header: null,
            tabBarIcon: ({ tintColor }) => (
              <TabBarIcon
                style={{ width: 18, height: 18 }}
                icon={require("@assets/images/user.png")}
                label="Hesabım"
                tintColor={tintColor}
              />
            ),
          },
        },
      ),
    },

    OthersStack: {
      screen: createStackNavigator(
        {
          OthersScreen: { screen: OthersScreen },
          BankAccountsScreen: { key: "others", screen: BankAccountsScreen },
          SupportScreen: { screen: SupportScreen },
          ContactScreen: { screen: ContactScreen },
          MusteriTemsilcisiScreen: { screen: MusteriTemsilcisiScreen },
        },
        {
          headerMode: "none",
          initialRouteName: "OthersScreen",
          navigationOptions: {
            header: null,
            tabBarIcon: ({ tintColor }) => (
              <TabBarIcon
                style={{ width: 18, height: 18 }}
                icon={require("@assets/images/ellipsis.png")}
                label="Diğer"
                tintColor={tintColor}
              />
            ),
          },
        },
      ),
    },
  },
  {
    tabBarComponent: TabBar,
    tabBarPosition: "bottom",
    animationEnabled: false,
    tabBarOptions: {
      activeTintColor: "#f7c948",
      inactiveTintColor: "white",
      showIcon: true,
      showLabel: true,
      tabStyle: { width: 100, backgroundColor: "white" },
    },
    lazy: true,
    swipeEnabled: false,
    headerMode: "none",
    initialRouteName: "HomeStack",
  },
);

HomeScreenTabNavigator.defaultNavigationOptions = ({ navigation }) => {
  let activeRoute = navigation.state.routes[navigation.state.index];
  return {
    drawerLockMode: activeRoute.routes && activeRoute.index > 0 ? "locked-closed" : "unlocked",
  };
};

HomeScreenTabNavigator.defaultNavigationOptions = ({ navigation }) => {
  let drawerLockMode = "unlocked";
  //if search stack is selected and its index is greater than 0
  if (navigation.state.index === 3 && navigation.state.routes[3].index > 0) {
    drawerLockMode = "locked-closed";
  }

  return {
    drawerLockMode,
  };
};

const MainStack = createStackNavigator(
  {
    HomeScreenTabNavigator: { screen: HomeScreenTabNavigator, path: "home" },
    ShippingScreen: { screen: ShippingScreen },
    PaymentOptionsScreen: { screen: PaymentOptionsScreen },
    AddressOptionsScreen: { screen: AddressOptionsScreen },
    NewAddressScreen: { screen: NewAddressScreen },
    EditOrderScreen: { screen: EditOrderScreen },
    BankAccountsScreen: { key: "main", screen: BankAccountsScreen },
    OrderConfirmationScreen: { screen: OrderConfirmationScreen },
    OrderResultScreen: { screen: OrderResultScreen },
    AraSayfaModal: {
      screen: AraSayfaModalScreen,
    },
    ProductDetailsScreen: {
      key: "main",
      screen: ProductDetailsScreen,
    },
    CheckoutStack: {
      screen: createStackNavigator(
        {
          CheckoutScreen: {
            screen: CheckoutScreen,
            navigationOptions: {
              tabBarVisible: false,
            },
          },
        },
        {
          headerMode: "none",
          initialRouteName: "CheckoutScreen",
          cardStyle: {
            backgroundColor: "#efefef",
            opacity: 1,
            shadowColor: "transparent",
          },
          navigationOptions: {
            tabBarVisible: false,
          },
        },
      ),
    },
  },
  {
    headerMode: "none",
    initialRouteName: "HomeScreenTabNavigator",
    cardStyle: {
      backgroundColor: "white",
      shadowOpacity: 0,
    },
  },
);

const SwitchNavigator = createSwitchNavigator(
  {
    AuthLoading: RouterScreen,
    App: MainStack,
    Auth: LoginStack,
  },
  {
    initialRouteName: "AuthLoading",
    cardStyle: {
      backgroundColor: "white",
      shadowOpacity: 0,
    },
  },
);

const AppContainer = createAppContainer(SwitchNavigator);

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loggedIn: false,
      navState: null,
    };
    UIManager.setLayoutAnimationEnabledExperimental &&
      UIManager.setLayoutAnimationEnabledExperimental(true);
  }

  componentDidMount() {
    visilabsManager.createApi();
    BackHandler.addEventListener("hardwareBackPress", function () {
      // this.onMainScreen and this.goBack are just examples, you need to use your own implementation here
      // Typically you would use the navigator here to go to the last state.
      if (NavigationService.getCurrentRoute().routeName !== "HomeScreen") {
        NavigationService.goBack();
      } else {
        Alert.alert("Çıkış", "Uygulamayı kapatmak istediğinize emin misiniz?", [
          { text: "Evet", onPress: () => BackHandler.exitApp() },
          { text: "Hayır" },
        ]);
      }

      return true;
    });
    SplashScreen.hide();

    console.disableYellowBox = true;
    if (Platform.OS === "ios") Euromsg.setDebug(false);
  }

  findRouteNameFromNavigatorState(routes) {
    let route = routes[routes.length - 1];
    while (route.index !== undefined) route = route.routes[route.index];
    return route.routeName;
  }

  render() {
    return (
      <SafeAreaProvider>
        <Provider store={store}>
          <TopLevelComponent
            navState={this.state.navState}
            navigator={
              <AppContainer
                uriPrefix={prefix}
                ref={(navigatorRef) => {
                  NavigationService.setTopLevelNavigator(navigatorRef);
                }}
                onNavigationStateChange={(prevState, newState) => {
                  const rName = this.findRouteNameFromNavigatorState(
                    newState.routes[newState.index].routes,
                  );
                  this.setState({ navState: newState, rName });
                  if (new RegExp("^[a-zA-Z()]+$").test(rName)) {
                    try {
                      analytics().setCurrentScreen(rName, rName);
                      visilabsManager.customEvent(rName);
                    } catch (error) {
                      bugsnag.notify(error);
                    }
                  }
                }}
              />
            }
          />
        </Provider>
      </SafeAreaProvider>
    );
  }
}
