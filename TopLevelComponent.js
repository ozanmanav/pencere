import React, { Component } from "react";
import { View, StatusBar, Dimensions, Platform, Keyboard, Animated } from "react-native";
import SideMenu from "react-native-side-menu";
import { connect } from "react-redux";
import CustomSideMenu from "@components/SideMenu";
import { openDrawer, closeDrawer, getHomePage } from "@redux/actions";
import Toast from "react-native-easy-toast";
import { moderateScale } from "react-native-size-matters";
import { SafeAreaView } from "react-navigation";

const win = Dimensions.get("window");

const DRAWER_SCREENS = [
  "SearchScreen",
  "FavoritesScreen",
  "AnnouncementsScreen",
  "ShoppingCartScreen",
  "MyAccountScreen",
  "OthersScreen",
  "HomeScreen",
];

const WHITE_BOTTOM = ["CheckoutScreen", "AddressOptionsScreen"];

class TopLevelComponent extends Component {
  constructor(props) {
    super(props);
    this.routeName = "";
    this.state = {
      isOpen: false,
      hideStatusBar: false,
      chatVisible: false,
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.drawer.isOpen !== this.props.drawer.isOpen) {
      this.setState({ isOpen: this.props.drawer.isOpen });
    }
    if (prevProps.toast.toggle !== this.props.toast.toggle) {
      this.toast.show(this.props.toast.msg);
    }
  }

  onChange = isOpen => {
    this.setState({ isOpen });
    if (isOpen) {
      Keyboard.dismiss();
      this.props.openDrawer();
    } else this.props.closeDrawer();
    if (this.state.hideStatusBar) this.setState({ hideStatusBar: false });
  };

  lastLeft = 0;

  getCurrentRoute(navState) {
    var route;
    if (this.navState) {
      let routes = navState;
      while (routes.routes) {
        route = route.routes[route.index];
      }
      return route.routeName;
    }
  }

  setGlobalSettings(navState) {
    if (navState) {
      var routeName = this.findRouteNameFromNavigatorState(navState.routes[navState.index].routes);
      this.routeName = routeName;
      this.showStatusBar = routeName !== "RouterScreen" && routeName !== "LoginScreen";
      if (navState.index === 1) {
        this.shouldDrawerWork = DRAWER_SCREENS.includes(routeName);
      } else {
        this.shouldDrawerWork = false;
      }
    } else {
      this.shouldDrawerWork = true;
      this.showStatusBar = false;
    }
  }

  shouldDrawerWork = true;
  showStatusBar = true;

  findRouteNameFromNavigatorState(routes) {
    let route = routes[routes.length - 1];
    while (route.index !== undefined) route = route.routes[route.index];
    return route.routeName;
  }

  render() {
    this.setGlobalSettings(this.props.navState);
    return (
      <SideMenu
        disableGestures={!this.shouldDrawerWork}
        bounceBackOnOverdraw={false}
        menu={<CustomSideMenu />}
        onChange={this.onChange}
        isOpen={this.state.isOpen}
        openMenuOffset={win.width * 0.85}
        toleranceX={5}
        menuPosition="left"
        animationFunction={(prop, value) =>
          Animated.spring(prop, {
            toValue: value,
            bounciness: 1,
            useNativeDriver: true,
          })
        }
      >
        {
          <SafeAreaView
            style={{ flex: 1, backgroundColor: this.props.statusbar.backgroundColor }}
            forceInset={
              !this.showStatusBar
                ? { top: "never", bottom: "never" }
                : { top: "always", bottom: "always" }
            }
          >
            <View
              style={{
                height: moderateScale(40, 0.5),
                position: "absolute",
                bottom: 0,
                width: win.width,
                backgroundColor: WHITE_BOTTOM.includes(this.routeName)
                  ? "white"
                  : this.props.statusbar.bottomTrickColor,
              }}
            />
            <StatusBar
              hidden={
                (this.state.isOpen && Platform.OS === "android" && this.showStatusBar) ||
                (this.state.hideStatusBar && Platform.OS === "android") ||
                this.state.chatVisible
              }
              translucent={true}
              backgroundColor={this.props.statusbar.backgroundColor}
              barStyle={
                this.props.statusbar.backgroundColor === "white" ? "dark-content" : "light-content"
              }
            />
            {this.props.navigator}
          </SafeAreaView>
        }
        <Toast
          ref={toast => {
            this.toast = toast;
          }}
        />
      </SideMenu>
    );
  }
}

function mapStateToProps(state) {
  return {
    drawer: state.drawer,
    toast: state.toast,
    statusbar: state.statusbar,
    session: state.session,
    liveChat: state.liveChat,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    openDrawer: () => dispatch(openDrawer()),
    closeDrawer: () => dispatch(closeDrawer()),
    getHomePage: (hideLoading, refreshing) => dispatch(getHomePage(hideLoading, refreshing)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(TopLevelComponent);
