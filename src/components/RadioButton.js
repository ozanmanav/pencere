import React, { Component } from "react";
import { View, TouchableOpacity } from "react-native";

export default class RadioButton extends Component {
  render() {
    return (
      <TouchableOpacity
        style={[
          {
            height: 24,
            width: 24,
            borderRadius: 12,
            borderWidth: 1,
            borderColor: "#bebebe",
            alignItems: "center",
            justifyContent: "center",
          },
          this.props.style,
        ]}
        disabled={this.props.disabled}
        onPress={this.props.onPress}
      >
        {this.props.selected ? (
          <View
            style={[
              {
                height: 12,
                width: 12,
                borderRadius: 6,
                backgroundColor: "#EF6C00",
              },
              this.props.disabled && { backgroundColor: "gray" },
              this.props.innerStyle,
            ]}
          />
        ) : null}
      </TouchableOpacity>
    );
  }
}
