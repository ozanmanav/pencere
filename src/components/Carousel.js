import React, { Component } from "react";
import { Animated, View, StyleSheet, Dimensions, ScrollView, TouchableOpacity } from "react-native";
import { connect } from "react-redux";
import Text from "@components/MyText";
import SmartImage from "@components/SmartImage";

const deviceWidth = Dimensions.get("window").width;
const FIXED_BAR_WIDTH = deviceWidth * 0.8;
const BAR_SPACE = 10;

var self;
var lastScrollTime = 0;

class Carousel extends Component {
  animVal = new Animated.Value(0);
  constructor(props) {
    super(props);
    this._onPostPress = this._onPostPress.bind(this);
    //this.onScrollEnd = this.onScrollEnd.bind(this);

    this.state = {
      scrollX: 0,
    };
  }

  componentDidMount() {
    self = this;
    setInterval(function () {
      if (Date.now() - lastScrollTime > 1000 || self.state.scrollX === 0) {
        if (self.state.scrollX === deviceWidth * (self.props.items.length - 1))
          self.scrollView.scrollTo({ x: 0, y: 0, animated: true });
        else
          self.scrollView.scrollTo({
            x: self.state.scrollX + deviceWidth,
            y: 0,
            animated: true,
          });
      }
    }, 3000);
  }

  handleScroll = function (event) {
    self.setState({ scrollX: event.nativeEvent.contentOffset.x });
    Animated.event([{ nativeEvent: { contentOffset: { x: this.animVal } } }]);
    lastScrollTime = Date.now();
  };

  render() {
    const numItems = this.props.items.length;
    const itemWidth = FIXED_BAR_WIDTH / numItems - BAR_SPACE;
    let imageArray = [];
    let barArray = [];
    this.props.items.forEach((item, i) => {
      var isFavorite = false;
      /*             this.props.favorites.items.forEach(favorite => {
              if(favorite !=null)
              {
                if (favorite.post_id == post.post_id)
                isFavorite = true;
              }
            
            }); */
      const thisImage = (
        <TouchableOpacity
          style={{ height: (deviceWidth * 9) / 16 }}
          onPress={() => {
            this._onPostPress(item, isFavorite);
          }}
        >
          <SmartImage
            key={`image${i}`}
            source={{ uri: item.UrunResimBuyuk }}
            style={{ width: deviceWidth, height: (deviceWidth * 9) / 16 }}
            resizeMode={"contain"}
          />
          <Text style={styles.sliderTitle}>{item.description}</Text>
        </TouchableOpacity>
      );
      imageArray.push(thisImage);
      const index = Math.ceil(this.state.scrollX / deviceWidth);
      const thisBar = (
        <View
          key={`bar${i}`}
          style={[
            styles.track,
            {
              width: itemWidth / 2,
              height: itemWidth / 2,
              marginLeft: BAR_SPACE,
              backgroundColor: i === index ? "#26A69A" : "gray",
              borderRadius: 5,
              borderWidth: 1,
              borderColor: i === index ? "#26A69A" : "gray",
            },
          ]}
        />
      );
      barArray.push(thisBar);
    });

    return (
      <View style={styles.container} flex={1}>
        <ScrollView
          ref={(c) => (this.scrollView = c)}
          horizontal
          showsHorizontalScrollIndicator={false}
          scrollEventThrottle={10}
          pagingEnabled
          onScroll={this.handleScroll}
          removeClippedSubviews={false}
          // onScrollEndDrag={this.onScrollEnd}
        >
          {imageArray}
        </ScrollView>
        <View style={styles.barContainer}>{barArray}</View>
      </View>
    );
  }
}
function mapStateToProps(state) {
  return {
    favorites: state.favorites,
  };
}
export default connect(mapStateToProps, null)(Carousel);

const styles = StyleSheet.create({
  sliderTitle: {
    width: deviceWidth,
    height: (deviceWidth * 9) / 16,
    alignContent: "center",
    textAlign: "center",
    color: "white",
    position: "absolute",
    fontSize: 22,
    padding: 10,
    top: (deviceWidth * 9) / 16 - 100,
    textShadowColor: "gray",
    textShadowRadius: 6,
    textShadowOffset: {
      width: 1,
      height: 1,
    },
  },
  container: {
    flex: 1,
    height: (deviceWidth * 9) / 16,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "white",
  },
  barContainer: {
    position: "absolute",
    top: (deviceWidth * 9) / 16 - 20,
    flexDirection: "row",
  },
  track: {
    backgroundColor: "green",
    overflow: "hidden",
    height: 2,
  },
});
