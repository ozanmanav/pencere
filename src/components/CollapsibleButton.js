import React, { Component } from "react";
import { View, TouchableOpacity } from "react-native";
import Collapsible from "react-native-collapsible";

export default class CollapsibleButton extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isCollapsed: true,
    };
  }

  render() {
    return (
      <TouchableOpacity
        onPress={() => this.setState({ isCollapsed: !this.state.isCollapsed })}
        style={[
          {
            flex: 1,
            borderColor: "#dddddd",
            borderWidth: 1,
            backgroundColor: "white",
            justifyContent: "center",
          },
          this.props.style,
        ]}
      >
        <View
          style={{
            flex: 1,
            height: 40,
            alignItems: "center",
            borderBottomColor: "#dddddd",
            borderBottomWidth: this.state.isCollapsed ? 0 : 1,
          }}
        >
          {this.props.renderContent(!this.state.isCollapsed)}
        </View>
        <Collapsible collapsed={this.state.isCollapsed}>
          {this.props.renderCollapsibleContent()}
        </Collapsible>
      </TouchableOpacity>
    );
  }
}
