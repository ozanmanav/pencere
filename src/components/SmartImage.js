import React, { Component } from "react";
import { Animated, ActivityIndicator, View } from "react-native";
import FastImage from "react-native-fast-image";
import { ParallaxImage } from "react-native-snap-carousel";
const AnimatedFastImage = Animated.createAnimatedComponent(FastImage);
const AnimatedParallaxImage = Animated.createAnimatedComponent(ParallaxImage);

export default class SmartImage extends Component {
  constructor(props) {
    super(props);
    var opacityAnim = new Animated.Value(0);
    this.state = {
      source: null,
      error: false,
      loading: true,
      opacityAnim,
    };
  }

  componentDidMount() {
    this.setState({
      source: this.props.source,
    });
  }

  onLoadEnd = () => {
    if (this.state.source.uri && this.state.source.uri.includes("/urunyok/default.gif")) {
      this.setState({
        source: require("@assets/images/no-image.png"),
        error: true,
      });
    }
    this.setState(
      {
        loading: false,
      },
      () => {
        Animated.timing(this.state.opacityAnim, {
          toValue: 1,
          duration: 250,
          useNativeDriver: true,
        }).start();
      },
    );
  };

  render() {
    return (
      <View style={[{ justifyContent: "center", alignItems: "center" }, this.props.containerStyle]}>
        {this.parallax ? (
          <AnimatedParallaxImage
            {...this.props}
            key={this.props.key}
            parallaxFactor={0.4}
            style={[
              {
                opacity: this.state.opacityAnim,
              },
              this.props.style,
              this.state.error
                ? {
                    resizeMode: "contain",
                    padding: this.props.style.height / 10,
                    tintColor: "#e4e4e4",
                  }
                : null,
              this.state.error && this.props.defaultImageStyle
                ? this.props.defaultImageStyle
                : null,
            ]}
            source={this.state.source}
            resizeMode={
              this.state.error ? "contain" : this.props.resizeMode ? this.props.resizeMode : "cover"
            }
            onError={(err) => {
              console.log("smart image", "onError", err);
              this.setState({
                source: require("@assets/images/no-image.png"),
                error: true,
              });
            }}
            onLoad={(e) => this.props.onLoad(e)}
            onLoadEnd={this.onLoadEnd}
            onLoadStart={() => {
              this.setState({
                loading: true,
              });
            }}
            {...this.props.parallaxProps}
          />
        ) : (
          <AnimatedFastImage
            {...this.props}
            key={this.props.key}
            style={[
              {
                opacity: this.state.opacityAnim,
              },
              this.props.style,
              this.state.error
                ? {
                    resizeMode: "contain",
                    padding: this.props.style.height / 10,
                    tintColor: "#e4e4e4",
                  }
                : null,
              this.state.error && this.props.defaultImageStyle
                ? this.props.defaultImageStyle
                : null,
            ]}
            source={this.state.source}
            resizeMode={
              this.state.error ? "contain" : this.props.resizeMode ? this.props.resizeMode : "cover"
            }
            onError={(err) => {
              console.log("smart image", "onError", err);
              this.setState({
                source: require("@assets/images/no-image.png"),
                error: true,
              });
            }}
            //onLoad={(e) => this.props.onLoad(e)}
            onLoadEnd={this.onLoadEnd}
            onLoadStart={() => {
              this.setState({
                loading: true,
              });
            }}
          />
        )}
        {this.props.showLoadingIndicator && this.state.loading && (
          <View
            style={{
              position: "absolute",
            }}
          >
            <ActivityIndicator color="#607D8B" />
          </View>
        )}
      </View>
    );
  }
}
