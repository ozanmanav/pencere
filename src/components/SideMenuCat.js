import React, { Component } from "react";
import { View, TouchableOpacity, Dimensions, Image, LayoutAnimation } from "react-native";
import Text from "@components/MyText";

import { moderateScale, ScaledSheet } from "react-native-size-matters";
import { UPDATE_SPRINGY } from "@common/LayoutAnimations";

const win = Dimensions.get("window");
const paddingHorizontal = win.width * 0.04;

export default class componentName extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isCollapsed: true,
    };
  }

  render() {
    return (
      <View
        key={this.props.key}
        style={{
          flexDirection: "column",
          borderBottomColor: "#4e4e4e",
          borderBottomWidth: 1,
        }}
      >
        <TouchableOpacity
          accessibilityLabel={"drawer_" + this.props.item.Hid}
          onPress={() => {
            if (this.props.item.ilH && this.props.item.ilH.length > 0) {
              this.setState({ isCollapsed: !this.state.isCollapsed });
              LayoutAnimation.configureNext(UPDATE_SPRINGY);
            } else this.props.onPressItem(this.props.item, true);
          }}
          style={[styles.catItem]}
        >
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            {this.props.stackIndex === 0 && this.props.item.Hk === 1 && (
              <Image
                key={this.props.item.Hid}
                source={{
                  uri: !this.props.item.ImgURL.startsWith("http")
                    ? "https://" + this.props.item.ImgURL
                    : this.props.item.ImgURL,
                }}
                style={{
                  width: moderateScale(20, 0.5),
                  height: moderateScale(20, 0.5),
                  resizeMode: "contain",
                  tintColor: "white",
                }}
              />
            )}
            <Text
              fontScale={0.5}
              ellipsizeMode="clip"
              style={{
                marginLeft: this.props.stackIndex === 0 ? win.width * 0.04 : 0,
                color: this.props.stackIndex > 0 ? "#66BB6A" : "white",
                fontFamily: "Poppins-Medium",
                fontSize: this.props.stackIndex > 0 ? 15 : 14,
                lineHeight: 17,
                marginTop: 5,
                maxWidth: win.width * 0.7,
                flex: 1,
              }}
            >
              {this.props.item.Htxt}
            </Text>
          </View>
          {!this.props.item.ilH || this.props.item.ilH.length === 0 ? (
            <Image
              source={require("@assets/images/arrow-right.png")}
              style={{
                width: moderateScale(10, 0.5),
                height: moderateScale(10, 0.5),
                resizeMode: "contain",
                tintColor: "#adadad",
              }}
            />
          ) : (
            <Image
              source={
                this.state.isCollapsed
                  ? require("@assets/images/plus.png")
                  : require("@assets/images/minus.png")
              }
              style={{
                width: moderateScale(10, 0.5),
                height: moderateScale(10, 0.5),
                resizeMode: "contain",
                tintColor: "#adadad",
              }}
            />
          )}
        </TouchableOpacity>
        <View
          style={{
            flexDirection: "column",
            backgroundColor: "#484848",
            borderBottomColor: "gray",
            borderBottomWidth: !this.state.isCollapsed ? 0 : 0,
          }}
        >
          {!this.state.isCollapsed &&
            this.props.stackIndex > 0 &&
            this.props.item.ilH &&
            this.props.item.ilH.length > 0 &&
            this.props.item.ilH.map((subItem) => {
              return (
                <TouchableOpacity
                  accessibilityLabel={"drawer_" + subItem.Hid}
                  key={subItem.Hid}
                  style={styles.subItem}
                  onPress={() => this.props.onPressItem(subItem, true)}
                >
                  <Text
                    fontScale={0.5}
                    ellipsizeMode="clip"
                    style={{
                      color: "white",
                      fontFamily: "Poppins-Medium",
                      fontSize: 14,
                      lineHeight: 17,
                      marginTop: 5,
                      maxWidth: win.width * 0.8,
                    }}
                  >
                    {subItem.Htxt}
                  </Text>
                  <Image
                    source={require("@assets/images/arrow-right.png")}
                    style={{
                      width: moderateScale(10, 0.5),
                      height: moderateScale(10, 0.5),
                      resizeMode: "contain",
                      tintColor: "#a2a2a2",
                    }}
                  />
                </TouchableOpacity>
              );
            })}
        </View>
      </View>
    );
  }
}

const styles = ScaledSheet.create({
  catItem: {
    borderBottomWidth: 1,
    backgroundColor: "#353535",
    alignItems: "center",
    justifyContent: "space-between",
    height: "40@ms0.5",
    paddingHorizontal,
    flexDirection: "row",
  },
  subItem: {
    alignItems: "center",
    justifyContent: "space-between",
    height: "40@ms0.5",
    paddingHorizontal,
    flexDirection: "row",
  },
});
