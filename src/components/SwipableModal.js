import React, { Component } from "react";
import { Platform, View, TouchableOpacity, Dimensions, Modal } from "react-native";
import Interactable from "react-native-interactable";

const win = Dimensions.get("window");

export default class SwipableModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isVisible: false,
      keyboardVisible: false,
      keyboardHeight: 0,
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.isVisible !== prevProps.isVisible)
      this.setState({
        isVisible: this.props.isVisible,
      });
  }

  toggleVisibility = () => {
    this.setState({ isVisible: !this.state.isVisible });
  };

  render() {
    if (this.state.isVisible)
      return (
        <View
          style={{
            position: "absolute",
            top: 0,
            left: 0,
            height: win.height,
            width: win.width,
            backgroundColor: this.state.isVisible ? "#00000055" : "transparent",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <Modal
            presentationStyle="overFullScreen"
            animationType="slide"
            transparent={true}
            onRequestClose={() => this.setState({ fsCarousel: false })}
          >
            <TouchableOpacity
              onPress={() => this.toggleVisibility()}
              style={{
                position: "absolute",
                top: 0,
                left: 0,
                height: win.height,
                width: win.width,
                backgroundColor: "transparent",
                alignItems: "center",
                justifyContent: "center",
              }}
              activeOpacity={1}
            />
            <View
              style={{
                top: win.height / 3,
                height: win.height / 4,
                width: win.width,
                alignItems: "center",
                justifyContent: "center",
              }}
              activeOpacity={1}
            >
              <Interactable.View
                boundaries={{ top: 0 }}
                snapPoints={[{ y: 0 }, { y: win.height }]}
                onSnapStart={({ nativeEvent }) => {
                  if (nativeEvent.index === 1 && Platform.OS === "ios") {
                    this.toggleVisibility();
                  }
                }}
                onSnap={({ nativeEvent }) => {
                  if (nativeEvent.index === 1 && Platform.OS === "android") {
                    this.toggleVisibility();
                  }
                }}
                verticalOnly={true}
                animatedNativeDriver={true}
                style={{ flexDirection: "column", justifyContent: "center" }}
              >
                {this.props.children}
              </Interactable.View>
            </View>
          </Modal>
        </View>
      );
    else return null;
  }
}
