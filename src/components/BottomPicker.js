import React, { Component } from "react";
import { View, Picker, TouchableOpacity, Dimensions, Modal, Platform } from "react-native";
import Text from "@components/MyText";
import { moderateScale } from "react-native-size-matters";
import { WheelPicker } from "@delightfulstudio/react-native-wheel-picker-android";
import colors from "@common/styles/colors";
const win = Dimensions.get("window");

export default class BottomPicker extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      selectedIndex: 0,
      selectedItem: null,
      selectedValue: null,
    };
  }

  openPicker = () => {
    if (this.props?.initialItem) {
      this.setState({
        isOpen: true,
        selectedIndex: this.findSelectedItemPosition(this.props.initialItem),
        selectedItem: this.props.initialItem,
        selectedValue: this.props.valueExtractor
          ? this.props.valueExtractor(this.props.initialItem)
          : this.props.initialItem,
      });
    } else {
      this.setState({
        isOpen: true,
        selectedIndex: 0,
        selectedItem: this.props.items[0],
        selectedValue: this.props.valueExtractor
          ? this.props.valueExtractor(this.props.items[0])
          : this.props.items[0],
      });
    }
  };

  closePicker = () => {
    this.setState({
      isOpen: false,
    });
  };

  findSelectedItemPosition = item => {
    if (item != null && this.props.items) {
      if (this.props.valueExtractor) {
        var selectedIndex = this.props.items.findIndex(
          i => this.props.valueExtractor(i) === this.props.valueExtractor(item),
        );
        return selectedIndex === -1 ? 0 : selectedIndex;
      } else {
        var selectedIndex = this.props.items.findIndex(i => i === item);
        return selectedIndex === -1 ? 0 : selectedIndex;
      }
    } else return 0;
  };

  render() {
    console.log("see bottom", { state: this.state });
    if (this.props.items == null) return null;
    if (Platform.OS === "android") {
      console.log("android selected item", this.state.selectedItem, this.state.selectedIndex);
      return (
        <Modal
          onRequestClose={() => this.closePicker()}
          transparent={true}
          visible={this.state.isOpen}
          presentationStyle="overFullScreen"
        >
          <TouchableOpacity
            style={{
              flex: 1,
              alignItems: "center",
              justifyContent: "center",
              backgroundColor: "rgba(0,0,0,0.5)",
            }}
            onPress={this.closePicker}
          >
            {this.props.children}
          </TouchableOpacity>
          <View
            style={[
              {
                position: "absolute",
                bottom: 0,
                width: win.width,
                height: 240,
                backgroundColor: "white",
                alignItems: "center",
                flexDirection: "column",
              },
            ]}
          >
            <View
              style={{
                width: win.width,
                borderTopColor: "#e0e0e0",
                borderTopWidth: 1,
                backgroundColor: "#dedede",
                justifyContent: this.props.hideCancel ? "flex-end" : "space-between",
                flexDirection: "row",
                paddingHorizontal: 10,
                paddingVertical: 10,
              }}
            >
              {!this.props.hideCancel && (
                <TouchableOpacity onPress={() => this.closePicker()}>
                  <Text
                    fontScale={0.5}
                    style={{
                      fontSize: 15,
                      color: "gray",
                      fontFamily: "Poppins-SemiBold",
                    }}
                  >
                    İptal
                  </Text>
                </TouchableOpacity>
              )}
              <TouchableOpacity
                onPress={() => {
                  this.closePicker();
                  if (this.state.selectedItem === "special_item")
                    this.props.specialItemAction && this.props.specialItemAction();
                  else {
                    this.props.onPressOK &&
                      this.props.onPressOK(this.state.selectedValue, this.state.selectedIndex);
                  }
                }}
              >
                <Text
                  fontScale={0.5}
                  style={{
                    fontSize: 15,
                    color: colors.red,
                    fontFamily: "Poppins-SemiBold",
                  }}
                >
                  Tamam
                </Text>
              </TouchableOpacity>
            </View>
            <WheelPicker
              renderIndicator={true}
              itemSpace={20}
              indicatorColor={
                this.props?.getIndicatorColor != null
                  ? this.props.getIndicatorColor(this.props.items[this.state.selectedIndex])
                  : "#dedede"
              }
              selectedItemPosition={this.state.selectedIndex}
              onItemSelected={event => {
                const itemIndex = event.position;
                const item = this.props.items[itemIndex];
                const itemValue = this.props.valueExtractor
                  ? this.props.valueExtractor(item)
                  : item;
                console.log("onItemSelected_", event);
                if (typeof this.props.onSelect !== "undefined")
                  this.props.onSelect(item, itemIndex);
                if (itemValue !== "special_item") {
                  this.setState({
                    selectedItem: item,
                    selectedIndex: itemIndex,
                    selectedValue: itemValue,
                  });
                } else {
                  this.setState({
                    selectedItem: "special_item",
                    itemValue: "special_item",
                  });
                }
              }}
              isCurved
              data={this.props.items.map(item =>
                this.props.labelExtractor ? this.props.labelExtractor(item) : item,
              )}
              style={{ width: 300, height: 240 }}
            />
          </View>
        </Modal>
      );
    } else
      return this.state.isOpen ? (
        <Modal
          onRequestClose={() => this.closePicker()}
          transparent={true}
          visible={this.state.isOpen}
          presentationStyle="overFullScreen"
        >
          <TouchableOpacity
            style={{
              flex: 1,
              alignItems: "center",
              justifyContent: "center",
              backgroundColor: "rgba(0,0,0,0.5)",
            }}
            activeOpacity={1.0}
            onPress={this.closePicker}
          >
            <View
              style={{
                position: "absolute",
                bottom: 0,
                width: win.width,
                height: 240,
                backgroundColor: "white",
                alignItems: "center",
                flexDirection: "column",
              }}
            >
              <View
                onPress={this.closePicker}
                style={{
                  width: win.width,
                  borderTopColor: "#e0e0e0",
                  borderTopWidth: 1,
                  backgroundColor: "#efefef",
                  justifyContent: this.props.hideCancel ? "flex-end" : "space-between",
                  flexDirection: "row",
                  paddingHorizontal: 10,
                  paddingVertical: 10,
                }}
              >
                {!this.props.hideCancel && (
                  <TouchableOpacity onPress={this.closePicker}>
                    <Text fontScale={0.5} style={{ fontSize: 15, color: "gray" }}>
                      İptal
                    </Text>
                  </TouchableOpacity>
                )}
                <TouchableOpacity
                  onPress={() => {
                    this.closePicker();
                    if (this.state.selectedItem === "special_item")
                      this.props.specialItemAction && this.props.specialItemAction();
                    else if (this.props.onPressOK) {
                      this.props.onPressOK(this.state.selectedValue, this.state.selectedIndex);
                    }
                  }}
                >
                  <Text
                    fontScale={0.5}
                    style={{
                      fontSize: 15,
                      color: colors.red,
                      fontFamily: "Poppins-SemiBold",
                    }}
                  >
                    Tamam
                  </Text>
                </TouchableOpacity>
              </View>
              <Picker
                selectedValue={
                  this.props.valueExtractor
                    ? this.props.valueExtractor(this.props.items[this.state.selectedIndex])
                    : this.props.items[this.state.selectedIndex]
                }
                itemStyle={{ fontSize: moderateScale(16, 0.4) }}
                style={{
                  height: moderateScale(50, 0.5),
                  width: win.width * 0.8,
                }}
                onValueChange={(itemValue, itemIndex) => {
                  if (typeof this.props.onSelect !== "undefined")
                    this.props.onSelect(itemValue, itemIndex);
                  if (itemValue !== "special_item")
                    this.setState({
                      selectedItem: this.props.valueExtractor
                        ? this.props.valueExtractor(this.props.items[itemIndex])
                        : this.props.items[itemIndex],
                      selectedIndex: itemIndex,
                      selectedValue: itemValue,
                    });
                  else {
                    this.setState({
                      selectedItem: "special_item",
                      itemValue: "special_item",
                    });
                  }
                }}
              >
                {this.props.items.map((item, index) => {
                  return (
                    <Picker.Item
                      color={this.props.getInd || "#121212"}
                      style={[{ fontSize: moderateScale(13, 0.4) }]}
                      value={this.props.valueExtractor ? this.props.valueExtractor(item) : item}
                      label={this.props.labelExtractor ? this.props.labelExtractor(item) : item}
                      onPress={() => this.setState({ selectedIndex: index })}
                    />
                  );
                })}
                {this.props.specialItem && this.props.specialItem()}
              </Picker>
            </View>
          </TouchableOpacity>
        </Modal>
      ) : null;
  }
}
