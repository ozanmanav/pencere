import React, { Component } from "react";
import { View, TouchableOpacity, StyleSheet, Dimensions } from "react-native";
import Text from "@components/MyText";
import ModalBox from "@components/ModalBox";
import CheckBox from "react-native-check-box";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";

const win = Dimensions.get("window");

export default class AddToFavoriteDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      f: false,
      s: false,
      isVisible: false,
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.isVisible !== prevProps.isVisible)
      this.setState({
        isVisible: this.props.isVisible,
      });
  }

  toggleVisibility = () => {
    this.modalBox.toggleVisibility();
    this.setState({
      f: false,
      s: false,
      isVisible: !this.props.isVisible,
    });
  };

  render() {
    return (
      <ModalBox
        ref={c => (this.modalBox = c)}
        animationType="none"
        presentationStyle="overFullScreen"
        scrollable={false}
      >
        <View style={styles.container}>
          <View
            style={{ borderBottomColor: "#dedede", borderBottomWidth: 1, alignItems: "center" }}
          >
            <Text style={{ fontSize: 15, paddingVertical: 8 }}>Takip Listesine Ekle</Text>
          </View>
          <View
            style={{
              marginBottom: 12,
              flexDirection: "row",
              alignItems: "center",
              marginTop: 10,
              marginLeft: 5,
            }}
          >
            <CheckBox
              style={{ height: 16, marginHorizontal: 5 }}
              onClick={() => this.setState({ f: !this.state.f })}
              isChecked={this.state.f}
              checkedImage={<MaterialIcons size={18} name="check-box" color="green" />}
              unCheckedImage={<MaterialIcons size={18} name="check-box-outline-blank" />}
            />
            <Text style={{ fontFamily: "Poppins", fontSize: 12, lineHeight: 14, marginTop: 6 }}>
              Fiyatı düştüğünde haber ver
            </Text>
          </View>
          <View
            style={{ flexDirection: "row", alignItems: "center", marginBottom: 5, marginLeft: 5 }}
          >
            <CheckBox
              style={{ height: 16, marginHorizontal: 5 }}
              onClick={() => this.setState({ s: !this.state.s })}
              isChecked={this.state.s}
              checkedImage={<MaterialIcons size={18} name="check-box" color="green" />}
              unCheckedImage={<MaterialIcons size={18} name="check-box-outline-blank" />}
            />
            <Text style={{ fontFamily: "Poppins", fontSize: 12, lineHeight: 14, marginTop: 6 }}>
              Stoğa girdiğinde haber ver
            </Text>
          </View>
          <View
            style={{
              flexDirection: "row",
              width: win.width * 0.8,
              justifyContent: "center",
              alignItems: "center",
              marginTop: 10,
              borderTopColor: "#dedede",
              borderTopWidth: 1,
            }}
          >
            <TouchableOpacity
              style={{
                flex: 1,
                alignItems: "center",
                padding: 10,
                borderRightWidth: 1,
                borderRightColor: "#dedede",
              }}
              onPress={() => this.toggleVisibility()}
            >
              <Text style={{ fontSize: 14, color: "#5d5d5d" }}>İptal</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{ flex: 1, alignItems: "center", padding: 5 }}
              onPress={() => {
                this.props.addFavorite(this.state.f, this.state.s);
              }}
            >
              <Text style={{ fontSize: 14, color: "#5d5d5d" }}>Ekle</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ModalBox>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
    width: win.width * 0.8,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.5,
    shadowRadius: 2,
    elevation: 1,
    borderRadius: 3,
    overflow: "hidden",
  },
});
