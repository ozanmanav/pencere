import React, { Component } from "react";
import { Text } from "react-native";
import { moderateScale } from "react-native-size-matters";

export default class MyText extends Component {
  render() {
    const fontscale = this.props.fontScale || 1;
    var styles = [];

    var mergedStyle = this.props.style;
    if (this.props.style instanceof Array) {
      mergedStyle = this.props.style.reduce(function(result, currentObject) {
        for (var key in currentObject) {
          if (key in currentObject) {
            result[key] = currentObject[key];
          }
        }
        return result;
      }, {});
    }

    styles = [
      mergedStyle,
      mergedStyle &&
        mergedStyle.fontSize &&
        !this.props.fontAbsolute && {
          fontSize: moderateScale(mergedStyle.fontSize, 0.5 * fontscale),
        },
      mergedStyle &&
        mergedStyle.lineHeight &&
        !this.props.fontAbsolute && {
          lineHeight: moderateScale(mergedStyle.lineHeight, 0.5 * fontscale),
        },
      mergedStyle &&
        mergedStyle.marginTop &&
        !this.props.fontAbsolute && {
          marginTop: moderateScale(mergedStyle.marginTop, 0.5 * fontscale),
        },
      mergedStyle &&
        !mergedStyle.textAlignVertical && {
          textAlignVertical: "center",
        },
      mergedStyle &&
        !mergedStyle.includeFontPadding && {
          includeFontPadding: false,
        },
    ];

    return (
      <Text {...this.props} style={styles}>
        {this.props.children}
      </Text>
    );
  }
}
