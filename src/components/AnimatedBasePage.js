import React, { Component } from "react";
import {
  View,
  SafeAreaView,
  FlatList,
  Platform,
  Dimensions,
  StyleSheet,
  Animated,
  StatusBar,
  ScrollView,
} from "react-native";
import AnimatedHeader from "@components/AnimatedHeader";
import PencereIndicator from "@components/PencereIndicator";
import colors from "@common/styles/colors";
import PropTypes from "prop-types";

const win = Dimensions.get("window");

export default class AnimatedBasePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
    };
  }

  componentDidMount() {
    console.log("base page props", this.props);
  }

  render() {
    console.log("base page", this.props);
    return (
      <SafeAreaView
        style={{ backgroundColor: colors.primary_dark, flex: 1 }}
        forceInset={this.props.forceInset}
      >
        <View style={{ width: win.width, backgroundColor: "#efefef", flexGrow: 1 }}>
          {this.props.showFlatList && this.renderFlatList()}
          {this.props.showScrollView && this.renderScrollView()}
          {this.props.children}
          <AnimatedHeader
            ref={c => (this.animatedHeader = c)}
            navigation={this.props.navigation}
            title={this.props.headerProps.title}
            back={this.props.headerProps.back}
            alignLeft={this.props.headerProps.alignLeft}
            showCart={this.props.headerProps.showCart}
            searchBar={this.props.headerProps.searchBar}
            search={this.props.headerProps.search}
          />
        </View>
        {this.props.isLoading && (
          <View style={[styles.loadingContainer]}>
            <PencereIndicator />
          </View>
        )}
      </SafeAreaView>
    );
  }

  renderScrollView = () => (
    <ScrollView
      ref={c => (this.scrollView = c)}
      style={{ backgroundColor: "#efefef" }}
      contentOffset={{ y: -AnimatedHeader.height }}
      contentInset={{ top: AnimatedHeader.height, left: 0, bottom: 0, right: 0 }}
      contentContainerStyle={{
        ...this.props.scrollViewProps.contentContainerStyle,
        paddingTop:
          Platform.OS === "android"
            ? AnimatedHeader.height +
              (Platform.Version < 20 ? 0 : StatusBar.currentHeight) +
              (this.props.flatListProps?.contentContainerStyle?.paddingTop ?? 0)
            : 0,
        flexGrow: 1,
      }}
      scrollIndicatorInsets={{
        top: AnimatedHeader.height,
        left: 0,
        bottom: 0,
        right: 0,
      }}
      scrollEventThrottle={50}
      ListHeaderComponent={this.props.scrollViewProps.ListHeaderComponent}
      onScroll={Animated.event(
        [
          {
            nativeEvent: { contentOffset: { y: this.state.scrollAnim } },
          },
        ],
        {
          listener: event => {
            this.onScroll(event);
            this.props.scrollViewProps.onScroll(event);
          },
        },
      )}
    >
      {this.props.scrollViewProps.renderContent != null && this.props.scrollViewProps.renderContent}
    </ScrollView>
  );

  renderFlatList = () => (
    <FlatList
      ref={c => (this.flatlist = c)}
      onLayout={this.props.flatListProps.onLayout}
      style={{ zIndex: -1 }}
      contentOffset={{ y: -AnimatedHeader.height }}
      contentInset={{ top: AnimatedHeader.height, left: 0, bottom: 0, right: 0 }}
      contentContainerStyle={{
        ...this.props.flatListProps.contentContainerStyle,
        paddingTop:
          Platform.OS === "android"
            ? AnimatedHeader.height +
              (Platform.Version < 20 ? 0 : StatusBar.currentHeight) +
              (this.props.flatListProps?.contentContainerStyle?.paddingTop ?? 0)
            : 0,
        flexGrow: 1,
      }}
      scrollIndicatorInsets={{
        top: AnimatedHeader.height,
        left: 0,
        bottom: 0,
        right: 0,
      }}
      scrollEventThrottle={50}
      overScrollMode={this.props.overScrollMode}
      onContentSizeChange={this.props.onContentSizeChange}
      refreshControl={this.props.flatListProps.refreshControl}
      keyExtractor={this.props.flatListProps.keyExtractor}
      data={this.props.flatListProps.data}
      renderItem={this.props.flatListProps.renderItem}
      ListEmptyComponent={this.props.flatListProps.ListEmptyComponent}
      ListHeaderComponent={this.props.flatListProps.ListHeaderComponent}
      ListFooterComponent={this.props.flatListProps.ListFooterComponent}
      onScroll={event => {
        this.onScroll(event);
        console.log("onscroll", this.props.flatListProps.onScroll);
        if (this.props.flatListProps.onScroll != null) {
          this.props.flatListProps.onScroll(event);
        }
      }}
    />
  );

  onScroll = event => {
    var dir = event.nativeEvent.contentOffset.y > this.currentOffset ? "out" : "in";
    if (
      Math.abs(event.nativeEvent.contentOffset.y - this.currentOffset) > 10 &&
      Math.abs(event.nativeEvent.contentOffset.y - this.currentOffset) < 100 &&
      event.nativeEvent.contentOffset.y > -99
    ) {
      console.log("startSearchBarAnim", dir);
      this.animatedHeader.getWrappedInstance().startSearchBarAnim(dir);
    }
    this.currentOffset = event.nativeEvent.contentOffset.y;
  };
}

const styles = StyleSheet.create({
  loadingContainer: {
    height: win.height,
    width: win.width,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0,0,0,0.3)",
    position: "absolute",
  },
});

AnimatedBasePage.propTypes = {
  testID: PropTypes.string,
  showFlatList: PropTypes.bool,
  showScrollView: PropTypes.bool,
  showChildren: PropTypes.bool,
  children: PropTypes.array,
  headerProps: PropTypes.shape({
    title: PropTypes.string,
    back: PropTypes.bool,
    alignLeft: PropTypes.bool,
    showCart: PropTypes.bool,
    searchBar: PropTypes.bool,
    search: PropTypes.bool,
  }),
  scrollViewProps: PropTypes.shape({
    renderContent: PropTypes.element,
    onScroll: PropTypes.func,
    ListHeaderComponent: PropTypes.element,
  }),
  flatListProps: PropTypes.shape({
    ref: PropTypes.func,
    data: PropTypes.array,
    renderItem: PropTypes.func,
    onContentSizeChange: PropTypes.func,
    overScrollMode: PropTypes.string,
    refreshControl: PropTypes.element,
    keyExtractor: PropTypes.func,
    ListHeaderComponent: PropTypes.element,
    ListFooterComponent: PropTypes.element,
    ListEmptyComponent: PropTypes.element,
    automaticallyAdjustContentInsets: PropTypes.bool,
    onScroll: PropTypes.func,
    removeClippedSubviews: PropTypes.bool,
    enableEmptySections: PropTypes.bool,
  }),
};

AnimatedBasePage.defaultProps = {
  showFlatList: false,
  showScrollView: false,
  showChildren: true,
  flatListProps: {
    data: [],
    overScrollMode: "never",
    extraPaddingTop: 0,
  },
  scrollViewProps: {
    renderContent: <View />,
    ListHeaderComponent: <View />,
  },
  headerProps: {
    back: true,
    alignLeft: true,
    showCart: false,
    searchBar: true,
    search: true,
  },
};
