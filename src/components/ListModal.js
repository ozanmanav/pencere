import React, { Component } from "react";
import { Platform, View, TouchableOpacity, Dimensions, Modal, ScrollView } from "react-native";
import Text from "@components/MyText";
import Feather from "react-native-vector-icons/Feather";
import { scale } from "react-native-size-matters";
import Interactable from "react-native-interactable";

const win = Dimensions.get("window");

export default class ListModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isVisible: false,
      keyboardVisible: false,
      keyboardHeight: 0,
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.isVisible !== prevProps.isVisible)
      this.setState({
        isVisible: this.props.isVisible,
      });
  }

  toggleVisibility = items => {
    this.setState({ isVisible: !this.state.isVisible, items });
  };

  render() {
    if (this.state.isVisible)
      return (
        <Modal
          presentationStyle="overFullScreen"
          transparent={true}
          onRequestClose={() => this.setState({ fsCarousel: false })}
        >
          <View
            style={{
              height: win.height,
              width: win.width,
              alignItems: "center",
              justifyContent: "center",
            }}
            activeOpacity={1}
          >
            <TouchableOpacity
              onPress={() => this.toggleVisibility()}
              style={{
                position: "absolute",
                top: 0,
                left: 0,
                height: win.height,
                width: win.width,
                backgroundColor: this.state.isVisible ? "#00000055" : "transparent",
                alignItems: "center",
                justifyContent: "center",
              }}
              activeOpacity={1}
            />
            <Interactable.View
              boundaries={{ top: 0 }}
              snapPoints={[{ y: 0 }, { y: win.height }]}
              onSnapStart={({ nativeEvent }) => {
                if (nativeEvent.index === 1 && Platform.OS === "ios") {
                  this.toggleVisibility();
                  this.props.onCancel != null && this.props.onCancel;
                }
              }}
              onSnap={({ nativeEvent }) => {
                if (nativeEvent.index === 1) {
                  this.toggleVisibility();
                  this.props.onCancel != null && this.props.onCancel;
                }
              }}
              verticalOnly={true}
              animatedNativeDriver={true}
              style={{ flexDirection: "column", justifyContent: "center" }}
            >
              <View
                style={[
                  {
                    flexDirection: "column",
                    borderRadius: 4,
                    overflow: "hidden",
                    marginHorizontal: 20,
                  },
                ]}
              >
                <View
                  style={[
                    {
                      flexDirection: "row",
                      paddingHorizontal: 10,
                      alignItems: "center",
                      justifyContent: "space-between",
                      height: 40,
                      backgroundColor: "#dedede",
                    },
                    this.props.titleStyle,
                  ]}
                >
                  <Text style={{ color: "gray", fontSize: 14 }}>{this.props.title}</Text>
                  <TouchableOpacity
                    onPress={() => {
                      this.toggleVisibility();
                      this.props.onCancel != null && this.props.onCancel;
                    }}
                  >
                    <Feather size={22} name="x-circle" color="#a4a4a4" />
                  </TouchableOpacity>
                </View>
                <ScrollView
                  keyboardShouldPersistTaps="always"
                  keyboardDismissMode="on-drag"
                  style={{
                    width: win.width - 30,
                    padding: 10,
                    flexGrow: 0,
                    maxHeight: win.height * 0.6,
                    alignSelf: "center",
                    backgroundColor: "white",
                    paddingTop: scale(15),
                  }}
                >
                  <TouchableOpacity activeOpacity={0.9}>
                    {this.props.items && this.props.items.length > 0
                      ? this.props.items.map((item, index) => this.props.renderItem(item, index))
                      : this.state.items &&
                        this.state.items.length &&
                        this.state.items.map((item, index) => this.props.renderItem(item, index))}
                    {this.props.renderFooter && this.props.renderFooter()}
                  </TouchableOpacity>
                </ScrollView>
              </View>
            </Interactable.View>
          </View>
        </Modal>
      );
    else return null;
  }
}
