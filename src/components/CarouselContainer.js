import React, { Component } from "react";
import { Animated, View, Image, Dimensions } from "react-native";
import SliderEntry from "./SliderEntry";
import styles from "@common/styles/index.style";
import Carousel, { Pagination } from "react-native-snap-carousel";
import { sliderWidth, itemWidth } from "@common/styles/SliderEntry.style";
import commonColors from "@common/styles/colors";

const deviceWidth = Dimensions.get("window").width;

export default class CarouselContainer extends Component {
  animVal = new Animated.Value(0);
  constructor(props) {
    super(props);
    this._onProductPress = this._onProductPress.bind(this);

    this.state = {
      scrollX: 0,
      slider1ActiveSlide: 0,
      ratio: 0.625,
      resizeMethod: "contain",
    };
    this._renderItem = this._renderItem.bind(this);
  }

  _renderItem({ item }) {
    return (
      <SliderEntry
        ratio={this.state.ratio}
        resizeMethod={this.state.resizeMethod}
        parallaxProps
        imageExtractor={this.props.imageExtractor}
        data={item}
        navigation={this.props.navigation}
        hasTitle={this.props.hasTitle}
        bannerOnPress={this.props.bannerOnPress}
      />
    );
  }

  _onProductPress(product) {
    this.props.navigation.navigate({
      key: "main",
      routeName: "ProductDetailsScreen",
      params: {
        UrunKodu: product.UrunKodu,
        SeriNo: product.UrunSeriNo,
        isFavorite: false,
      },
    });
  }
  state = {
    ratio: 0.625,
  };
  componentDidMount() {
    this.pointer = 0;
    if (this.props.items[0] && this.props.items[0].URL != null)
      Image.getSize(this.props.items[0].URL, (width, height) => {
        if ((height / width).toFixed(2) === "0.28" && this.state.ratio !== 345 / 775) {
          // if it's a known nonsene
          this.setState({ ratio: 345 / 775, resizeMethod: "cover" });
        } else if (height / width !== this.state.ratio) {
          this.setState({ ratio: height / width, resizeMethod: "contain" });
        }
      });
  }

  render() {
    if (this.props.items) {
      return (
        <View style={{ backgroundColor: commonColors.bg_gray }}>
          <View
            style={{
              alignItems: "center",
              justifyContent: "center",
              width: deviceWidth,
              backgroundColor: "white",
            }}
          >
            <Carousel
              data={this.props.items}
              renderItem={this._renderItem}
              sliderWidth={sliderWidth}
              loop={true}
              autoplay={true}
              autoplayInterval={4000}
              itemWidth={itemWidth}
              inactiveSlideScale={0.8}
              inactiveSlideOpacity={0}
              enableMomentum={false}
              activeSlideAlignment={"center"}
              onSnapToItem={(index) => this.setState({ slider1ActiveSlide: index })}
            />
          </View>
          <Pagination
            dotsLength={this.props.items.length}
            activeDotIndex={this.state.slider1ActiveSlide}
            containerStyle={styles.paginationContainer}
            dotColor={"gray"}
            dotStyle={styles.paginationDot}
            inactiveDotColor={"gray"}
            inactiveDotOpacity={0.4}
            inactiveDotScale={0.75}
            carouselRef={this._slider1Ref}
            tappableDots={!!this._slider1Ref}
          />
        </View>
      );
    } else return null;
  }
}
