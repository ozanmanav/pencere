import React, { Component } from "react";
import {
  TextInput,
  View,
  ScrollView,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  Modal,
  TouchableHighlight,
  Image,
} from "react-native";
import Text from "@components/MyText";

import { moderateScale } from "react-native-size-matters";

const win = Dimensions.get("window");

export default class CrossPicker extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isVisible: false,
      selectedVal: null,
      searchText: "",
      searchResult: [],
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.isVisible !== prevProps.isVisible)
      this.setState({
        isVisible: this.props.isVisible,
      });
  }

  search() {
    var searchResult = this.props.items.filter(item => item.search(this.state.searchText) !== -1);
    this.setState({ searchResult });
  }

  toggleVisibility = () => {
    this.setState({
      isVisible: !this.state.isVisible,
      searchResult: this.props.items,
      searchText: "",
    });
  };

  render() {
    if (this.state.isVisible) {
      return (
        <View
          style={{
            width: win.width,
            height: win.height,
            backgroundColor: "rgba(0,0,0,0.2)",
            position: "absolute",
            top: 0,
            left: 0,
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Modal
            animationType="fade"
            transparent={true}
            presentationStyle="overFullScreen"
            visible={true}
            onRequestClose={() => {
              this.setState({
                isVisible: false,
              });
            }}
            style={{ justifyContent: "center", alignItems: "center" }}
          >
            <TouchableHighlight
              style={{
                width: win.width,
                height: win.height,
                backgroundColor: "rgba(0,0,0,0.2)",
                justifyContent: "center",
                alignItems: "center",
              }}
              onPress={() => this.setState({ isVisible: false })}
            >
              <TouchableOpacity style={styles.container}>
                <View style={{ flexDirection: "column" }}>
                  <View
                    style={{
                      height: moderateScale(30, 0.5),
                      marginHorizontal: moderateScale(10, 0.5),
                      marginTop: moderateScale(10, 0.5),
                      flexDirection: "row",
                      alignItems: "center",
                    }}
                  >
                    <Image
                      source={require("@assets/images/search.png")}
                      style={{
                        width: moderateScale(18, 0.5),
                        height: moderateScale(18, 0.5),
                        tintColor: "#dedede",
                      }}
                    />
                    <TextInput
                      value={this.state.searchText}
                      placeholder="Arama Yapın..."
                      placeholderTextColor="gray"
                      style={{
                        marginLeft: moderateScale(10, 0.5),
                        flex: 1,
                        padding: 0,
                      }}
                      onChangeText={searchText =>
                        this.setState({ searchText }, () => this.search())
                      }
                    />
                  </View>
                  <View
                    style={{
                      height: 8,
                      backgroundColor: "#61a545",
                      marginHorizontal: 10,
                      borderBottomColor: "white",
                      borderBottomWidth: 6,
                    }}
                  />
                </View>
                <ScrollView>
                  {this.state.searchResult.map((item, index) => {
                    return (
                      <TouchableOpacity
                        key={index.toString()}
                        onPress={() => {
                          this.setState({ selectedVal: item }, () => {
                            this.toggleVisibility();
                            this.props.forceUpdate();
                          });
                          this.props.onChangeValue(item);
                        }}
                        style={{
                          backgroundColor: this.state.selectedVal === item ? "#8BC34A" : "white",
                          borderBottomColor: "#e9e9e9",
                          borderBottomWidth: 1,
                          height: 40,
                          alignItems: "center",
                          justifyContent: "center",
                        }}
                      >
                        <Text
                          style={{
                            color: this.state.selectedVal === item ? "white" : "gray",
                          }}
                        >
                          {item}
                        </Text>
                      </TouchableOpacity>
                    );
                  })}
                </ScrollView>
              </TouchableOpacity>
            </TouchableHighlight>
          </Modal>
        </View>
      );
    } else return null;
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
    width: win.width * 0.8,
    height: win.height * 0.8,
    shadowColor: "#000",
  },
});
