import React, { Component } from "react";
import {
  Modal,
  TouchableHighlight,
  TouchableOpacity,
  StatusBar,
  Dimensions,
  View,
} from "react-native";
import colors from "@common/styles/colors";
import { SafeAreaView } from "react-navigation";

const win = Dimensions.get("window");

export default class ModalBox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isVisible: false,
    };
  }

  toggleVisibility = () => {
    return new Promise(resolve => {
      this.setState(
        {
          isVisible: !this.state.isVisible,
        },
        () => resolve(),
      );
    });
  };

  render() {
    return (
      this.state.isVisible && (
        <TouchableHighlight
          style={{
            width: win.width,
            height: win.height,
            backgroundColor: "rgba(0,0,0,0.4)",
            position: "absolute",
            top: 0,
            left: 0,
            alignItems: "center",
            justifyContent: "center",
          }}
          onPress={() => this.setState({ isVisible: false })}
        >
          <Modal
            animationType={this.props.animationType || "none"}
            transparent={true}
            presentationStyle={this.props.presentationStyle || "overFullScreen"}
            visible={this.state.isVisible}
            onRequestClose={() => {
              this.setState({
                isVisible: false,
              });
            }}
          >
            <StatusBar hidden={this.props.statusBarHidden} backgroundColor={colors.primary_dark} />
            <SafeAreaView
              forceInset={this.props.forceInset ?? { top: "always" }}
              style={{
                backgroundColor:
                  this.props.presentationStyle === "fullScreen"
                    ? colors.primary_dark
                    : "transparent",
              }}
            >
              {!this.props.scrollable ? (
                <TouchableHighlight
                  style={{
                    width: win.width,
                    height: win.height,
                    backgroundColor: "transparent",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                  onPress={() => this.setState({ isVisible: false })}
                >
                  <TouchableOpacity activeOpacity={1}>{this.props.children}</TouchableOpacity>
                </TouchableHighlight>
              ) : this.props.presentationStyle === "fullScreen" ? (
                <View
                  style={{ backgroundColor: "transparent" }}
                  onPress={() => this.setState({ isVisible: false })}
                >
                  {this.props.children}
                </View>
              ) : (
                <View
                  style={{
                    width: win.width,
                    height: win.height,
                    backgroundColor: "transparent",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                  onPress={() => this.setState({ isVisible: false })}
                >
                  <View onPress={() => this.setState({ isVisible: false })}>
                    {this.props.children}
                  </View>
                </View>
              )}
            </SafeAreaView>
          </Modal>
        </TouchableHighlight>
      )
    );
  }
}
