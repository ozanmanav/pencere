import React, { Component } from "react";
import {
  DatePickerIOS,
  Dimensions,
  TouchableOpacity,
  DatePickerAndroid,
  Platform,
  Text,
} from "react-native";
import { View } from "react-native-animatable";

const win = Dimensions.get("window");

export class DatePickerDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      date: new Date(),
      isOpen: false,
    };
    this.close = this.close.bind(this);
    this.open = this.open.bind(this);
  }
  async openAndroidDatePicker(date, id, minDate, maxDate) {
    try {
      const { action, year, month, day } = await DatePickerAndroid.open({
        date,
        minDate,
        maxDate,
      });
      if (action !== DatePickerAndroid.dismissedAction) {
        this.setState({ date: new Date(year, month, day) });
        this.props.onChangeDate(new Date(year, month, day), this.id);
      }
    } catch ({ code, message }) {
      console.warn("Cannot open date picker", message);
    }
  }
  componentDidMount() {}
  setDate = (date) => {
    this.setState({
      date,
    });
    this.props.datepicker.setDate(date);
  };
  render() {
    return this.state.isOpen && Platform.OS === "ios" ? (
      <View
        style={{
          position: "absolute",
          bottom: 0,
          width: win.width,
          backgroundColor: "white",
          zIndex: 9999,
        }}
      >
        <View
          style={{
            flexDirection: "row",
            flex: 1,
            backgroundColor: "#e9e9e9",
            justifyContent: "space-between",
          }}
        >
          <TouchableOpacity style={{ padding: 10 }} onPress={() => this.close()}>
            <Text>İptal</Text>
          </TouchableOpacity>
          <TouchableOpacity style={{ padding: 10 }} onPress={() => this.close()}>
            <Text>Tamam</Text>
          </TouchableOpacity>
        </View>
        <DatePickerIOS
          mode="date"
          date={this.state.date}
          onDateChange={(date) => {
            this.setState({ date });
            this.props.onChangeDate(date, this.id);
          }}
          minimumDate={this.minimumDate}
          maximumDate={this.maximumDate}
        />
      </View>
    ) : null;
  }
  open(date, id, minimumDate, maximumDate) {
    console.log("open datepicker", id, date);
    this.id = id;
    this.minimumDate = minimumDate;
    this.maximumDate = maximumDate;
    this.setState({
      date,
      isOpen: true,
    });
    if (Platform.OS === "android")
      this.openAndroidDatePicker(date, this.id, this.minimumDate, this.maximumDate);
  }

  close() {
    this.setState({
      isOpen: false,
    });
    this.props.collapseDatesView();
  }
}

export default DatePickerDialog;
