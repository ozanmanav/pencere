import React, { Component } from "react";
import PropTypes from "prop-types";
import { View, StyleSheet } from "react-native";

import colors from "@common/styles/colors";

export default class Line extends Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    color: PropTypes.string,
  };

  static defaultProps = {
    color: colors.bd_line,
  };

  render() {
    return (
      <View style={[styles.line, { backgroundColor: this.props.color }, this.props.style || {}]} />
    );
  }
}

const styles = StyleSheet.create({
  line: {
    height: 1,
  },
});
