import React, { Component } from "react";
import { TouchableOpacity, ActivityIndicator, Platform } from "react-native";
import { View, Dimensions, StyleSheet } from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";
import Text from "@components/MyText";
import ModalBox from "@components/ModalBox";
import { scale, moderateScale } from "react-native-size-matters";
import { WebView } from "react-native-webview";

var win = Dimensions.get("window");

export default class WebViewModal extends Component {
  constructor() {
    super();
    this.state = {
      isOpen: false,
      isDisabled: false,
      swipeToClose: false,
      sliderValue: 0.3,
      source: { html: "" },
      loading: true,
      firstRoute: true,
      title: null,
      dynamicStyle: null,
    };
  }

  async toggleModal(source, title, dynamicStyle) {
    this.setState({ source, firstRoute: true, title, dynamicStyle });
    await this.modalBox.toggleVisibility();
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.source && prevProps.source !== this.props.source) {
      this.setState({ source: this.props.source });
    }
    if (this.props.title && prevProps.title !== this.props.title) {
      this.setState({ title: this.props.title });
    }
  }

  render() {
    return (
      <ModalBox
        ref={c => (this.modalBox = c)}
        statusBarHidden={false}
        animationType="slide"
        scrollable={true}
        presentationStyle="overFullScreen"
      >
        <View
          style={[
            styles.container,
            this.props.style,
            this.state.dynamicStyle && this.state.dynamicStyle,
          ]}
        >
          <View
            style={{
              width: win.width * 0.9,
              alignItems: "center",
              padding: moderateScale(10, 0.5),
              flexDirection: "row",
              justifyContent: "space-between",
              backgroundColor: "#efefef",
            }}
          >
            <Text fontScale={0.5} style={{ fontSize: 16, color: "gray", fontWeight: "500" }}>
              {this.state.title}
            </Text>
            <TouchableOpacity
              style={{
                width: moderateScale(24, 0.4),
                height: moderateScale(24, 0.4),
              }}
              onPress={() => this.modalBox.toggleVisibility()}
            >
              <Icon name="times-circle" size={moderateScale(24, 0.4)} color="rgba(0,0,0,0.3)" />
            </TouchableOpacity>
          </View>
          {this.state.loading && (
            <ActivityIndicator
              size="small"
              color="#263238"
              style={{ backgroundColor: "transparent", marginTop: scale(10) }}
            />
          )}
          <WebView
            source={
              this.state.source?.html
                ? { baseUrl: "", html: this.state.source.html }
                : this.state.source?.uri
                ? { uri: this.state.source.uri }
                : "<h2>Beklenmedik bir hata oluştu.</h2>"
            }
            originWhitelist={["*"]}
            style={[{ width: win.width * 0.88, backgroundColor: "transparent" }]}
            automaticallyAdjustContentInsets={true}
            scalesPageToFit={Platform.OS === "ios" ? false : true}
            onLoadStart={() => this.setState({ loading: true })}
            onLoadEnd={() => this.setState({ loading: false })}
            onError={event => console.log(event)}
            onNavigationStateChange={event => {
              console.log("webview url", event);
              if (
                event.url.includes("/paynetjgate/tds_return") &&
                !event.url.includes("DOCTYPE html")
              ) {
                this.toggleModal();
                this.props.charge();
              }
            }}
          />
        </View>
      </ModalBox>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "column",
    backgroundColor: "white",
    width: win.width * 0.9,
    height: win.height * 0.6,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.5,
    shadowRadius: 2,
    elevation: 1,
    alignItems: "center",
    marginBottom: 50,
    borderRadius: 6,
    overflow: "hidden",
  },
});
