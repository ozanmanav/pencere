import React, { Component } from "react";
import { View, StyleSheet, TouchableOpacity, TextInput } from "react-native";
import Text from "@components/MyText";

export default class VerticalCounter extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity
          onPress={() => this.props.decreaseNumber(this.props.skk, this.props.number)}
          style={{
            flexDirection: "row",
            alignItems: "center",
            backgroundColor: "white",
            borderColor: "#e2e2e2",
            borderWidth: 1,
            width: this.props.style ? this.props.style.width / 3 : 32,
            height: this.props.style ? this.props.style.width / 3 : 32,
          }}
        >
          <Text
            style={{
              marginTop: 2,
              fontFamily: "Poppins-Medium",
              textAlign: "center",
              fontSize: 19,
              color: "rgba(0,0,0,0.5)",
            }}
          >
            -
          </Text>
        </TouchableOpacity>
        <TextInput
          style={{
            paddingTop: 2,
            fontFamily: "Poppins-Medium",
            fontSize: 15,
            width: this.props.style ? this.props.style.width / 3 : 32,
            height: this.props.style ? this.props.style.width / 3 : 32,
            borderTopColor: "#e2e2e2",
            borderTopWidth: 1,
            borderBottomColor: "#e2e2e2",
            borderBottomWidth: 1,
            backgroundColor: "#e4e4e4",
            textAlign: "center",
          }}
          value={"4"}
        />
        <TouchableOpacity
          onPress={() => this.props.increaseNumber(this.props.skk, this.props.number)}
          style={{
            flexDirection: "row",
            alignItems: "center",
            backgroundColor: "white",
            borderColor: "#e2e2e2",
            borderWidth: 1,
            width: this.props.style ? this.props.style.width / 3 : 32,
            height: this.props.style ? this.props.style.width / 3 : 32,
          }}
        >
          <Text
            style={{
              marginTop: 2,
              fontFamily: "Poppins-Medium",
              textAlign: "center",
              fontSize: 19,
              color: "rgba(0,0,0,0.5)",
            }}
          >
            +
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    width: 32,
    backgroundColor: "rgba(0,0,0,0.0)",
    borderColor: "gray",
    justifyContent: "space-between",
    alignItems: "center",
  },
});
