import React, { Component } from "react";
import { ScrollView, Dimensions, View } from "react-native";
import Text from "@components/MyText";
import { moderateScale } from "react-native-size-matters";

const win = Dimensions.get("window");

export default class Swiper extends Component {
  constructor(props) {
    super(props);
    this.state = {
      scrollX: 0,
    };
  }

  handleScroll = (event) => {
    this.setState({ scrollX: event.nativeEvent.contentOffset.x });
  };

  itemWidth = win.width / 2.25;

  render() {
    var barArray = [];
    this.props.children.forEach((element, i) => {
      const index = Math.ceil(this.state.scrollX / this.itemWidth);
      const thisBar = (
        <View
          key={`bar${i}`}
          style={[
            {
              overflow: "hidden",
              width: 10,
              height: 10,
              marginLeft: 4,
              backgroundColor: i === index ? "#d6421e" : "#d1d1d1",
              borderRadius: 5,
              borderWidth: 1,
              borderColor: i === index ? "#d6421e" : "#d1d1d1",
              alignItems: "center",
              justifyContent: "center",
            },
          ]}
        />
      );
      barArray.push(thisBar);
    });

    return (
      <View style={[{ flexDirection: "column", backgroundColor: "white" }, this.props.style]}>
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            height: 40,
            alignSelf: "stretch",
            paddingHorizontal: 10,
          }}
        >
          {typeof this.props.title !== undefined && (
            <Text
              style={{
                fontFamily: "Poppins-SemiBold",
                fontSize: 16,
                marginTop: 2,
                flex: 0.7,
              }}
            >
              {this.props.title}
            </Text>
          )}
          <View
            style={{
              flexDirection: "row",
              height: 40,
              paddingRight: 10,
              flex: 0.3,
              alignItems: "center",
              justifyContent: "flex-end",
            }}
          >
            {barArray}
          </View>
        </View>

        <ScrollView
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          scrollEventThrottle={16}
          onScroll={this.handleScroll}
          style={{ backgroundColor: "white" }}
          contentContainerStyle={{
            paddingVertical: 0,
            paddingBottom: 0,
            paddingRight: moderateScale(20),
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          {this.props.children}
        </ScrollView>
      </View>
    );
  }
}
