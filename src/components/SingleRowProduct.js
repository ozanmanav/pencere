import React, { Component } from "react";
import { View, Dimensions, StyleSheet, Image, TouchableOpacity, TextInput } from "react-native";
import Text from "@components/MyText";
import SmartImage from "@components/SmartImage";
import colors from "@common/styles/colors";
import { moderateScale } from "react-native-size-matters";

const win = Dimensions.get("window");

export default class SingleRowProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      number: "1",
    };
  }

  _onProductPress = () => {
    const product = this.props.item;
    this.props.navigation.navigate({
      key: this.props.search ? "search" : "ProductDetailsScreen",
      routeName: "ProductDetailsScreen",
      params: { UrunKodu: product.UrunKodu, SeriNo: product.UrunSeriNo, isFavorite: false },
    });
  };

  renderPrices = () => {
    const TES = this.props.item.MobileUrunFiyatListesi.filter((item) => item.UrunFiyatTipi === "T");
    const OZEL = this.props.item.MobileUrunFiyatListesi.filter(
      (item) => item.UrunFiyatTipi === "S",
    );
    const FIRSAT = this.props.item.MobileUrunFiyatListesi.filter(
      (item) => item.UrunFiyatTipi === "F",
    );
    const BAYI = this.props.item.MobileUrunFiyatListesi.filter(
      (item) => item.UrunFiyatTipi === "B",
    );

    return (
      <View
        style={{
          flexDirection: "row",
          borderTopColor: "#ededed",
          borderTopWidth: 1,
          paddingTop: moderateScale(11, 0.5),
        }}
      >
        <View style={{ flexDirection: "column", paddingRight: moderateScale(6, 0.5) }}>
          {TES && TES[0] && (
            <Text style={{ fontFamily: "Poppins-Light", fontSize: 12, color: "gray" }}>
              {TES[0].UrunFiyatLabel}
            </Text>
          )}
          {OZEL && OZEL[0] && (
            <Text
              style={{
                fontFamily: "Poppins-Light",
                fontSize: FIRSAT && FIRSAT[0] ? 11 : 12,
                color: FIRSAT && FIRSAT[0] ? "gray" : colors.price_color,
              }}
            >
              {OZEL[0].UrunFiyatLabel}
            </Text>
          )}
          {BAYI && BAYI[0] && (
            <Text
              style={{
                fontFamily: "Poppins-Light",
                fontSize: FIRSAT && FIRSAT[0] ? 11 : 12,
                color: FIRSAT && FIRSAT[0] ? "gray" : colors.price_color,
              }}
            >
              {BAYI[0].UrunFiyatLabel}
            </Text>
          )}
          {FIRSAT && FIRSAT[0] && (
            <Text style={{ fontFamily: "Poppins-Light", fontSize: 12, color: colors.price_color }}>
              {"Fırsat Fiyatı"}
            </Text>
          )}
        </View>
        <View style={{ flexDirection: "column" }}>
          {TES && TES[0] && (
            <View style={{ flexDirection: "row" }}>
              <Text style={{ fontFamily: "Poppins-Light", fontSize: 11, color: "gray" }}>
                {TES[0].UrunFiyatStr + " + KDV"}
              </Text>
            </View>
          )}
          {OZEL && OZEL[0] && (
            <Text
              style={{
                fontFamily: FIRSAT && FIRSAT[0] ? "Poppins-Light" : "Poppins",
                textDecorationLine: FIRSAT && FIRSAT[0] ? "line-through" : "none",
                fontSize: FIRSAT && FIRSAT[0] ? 11 : 12,
                color: FIRSAT && FIRSAT[0] ? "gray" : colors.price_color,
              }}
            >
              {OZEL[0].UrunFiyatStr + " + KDV"}
            </Text>
          )}
          {BAYI && BAYI[0] && (
            <Text
              style={{
                marginTop: 2,
                fontFamily: FIRSAT && FIRSAT[0] ? "Poppins-Light" : "Poppins",
                fontSize: FIRSAT && FIRSAT[0] ? 11 : 12,
                color: FIRSAT && FIRSAT[0] ? "gray" : colors.price_color,
                textDecorationLine: FIRSAT && FIRSAT[0] ? "line-through" : "none",
              }}
            >
              {BAYI[0].UrunFiyatStr + " + KDV"}
            </Text>
          )}
          {FIRSAT && FIRSAT[0] && (
            <Text style={{ fontFamily: "Poppins", fontSize: 12, color: colors.price_color }}>
              {FIRSAT[0].UrunFiyatStr + " + KDV"}
            </Text>
          )}
        </View>
      </View>
    );
  };

  render() {
    console.log("rendering product", this.props);
    return (
      <TouchableOpacity
        key={this.props.item.UrunKodu}
        style={styles.holder}
        onPress={this._onProductPress}
      >
        <View style={{ flexDirection: "column", flex: 1 }}>
          <View style={{ flexDirection: "row" }}>
            <View
              style={{
                flexDirection: "column",
                flex: 0.3,
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <SmartImage
                showLoadingIndicator={true}
                style={styles.productImage}
                source={{ uri: this.props.item.UrunResimBuyukThumbnail }}
                resizeMode={"contain"}
              />
            </View>
            <View style={{ flexDirection: "column", flex: 0.5 }}>
              <View style={{ flexDirection: "row" }}>
                <Text
                  style={{
                    fontSize: 11,
                    color: "#68aef2",
                    fontFamily: "Poppins-Bold",
                    letterSpacing: 0.5,
                    marginRight: 5,
                  }}
                >
                  {this.props.item.UrunMarkaText}
                </Text>
                <Text
                  numberOfLines={1}
                  ellipsizeMode="tail"
                  style={{
                    width: win.width * 0.3,
                    fontSize: 11,
                    color: "#cecece",
                    fontFamily: "Poppins",
                    flexWrap: "wrap",
                  }}
                >
                  {this.props.item.UrunKodu}
                </Text>
              </View>
              <Text style={styles.name}>{this.props.item.UrunAciklamasi}</Text>
            </View>
            <View style={{ flexDirection: "column", flex: 0.2, alignItems: "flex-end" }}>
              <TextInput
                ref={(c) => (this.quantityInput = c)}
                onFocus={() => {
                  this.quantityInput.measure((fx, fy, width, height, px, py) => {
                    this.props.scrollToFocusedInput(py);
                  });
                }}
                selectTextOnFocus={true}
                returnKeyType="done"
                value={this.state.number}
                onChangeText={(text) => this.setState({ number: text })}
                keyboardType="numeric"
                style={{
                  fontSize: moderateScale(14, 0.4),
                  width: moderateScale(38, 0.5),
                  height: moderateScale(42, 0.5),
                  padding: 0,
                  borderRadius: 2,
                  borderLeftColor: "#ececec",
                  borderRightColor: "#e5e5e5",
                  borderTopColor: "#dedede",
                  borderWidth: 1,
                  borderBottomWidth: 0,
                  textAlign: "center",
                  backgroundColor: "#f5f4f5",
                }}
              />
            </View>
          </View>
          <View style={{ flexDirection: "row" }}>
            <View style={{ flexDirection: "column", flex: 0.3, justifyContent: "flex-end" }}>
              <Text
                style={{
                  textAlign: "center",
                  fontSize: 12,
                  fontFamily: "Poppins",
                  color: this.props.item.UrunStokDurumu === "Var" ? "#4CAF50" : "gray",
                }}
              >
                {this.props.item.UrunStokDurumu === "Var"
                  ? "Stokta"
                  : "Stok " + this.props.item.UrunStokDurumu}
              </Text>
            </View>
            <View style={{ flexDirection: "column", flex: 0.5, justifyContent: "flex-start" }}>
              {this.renderPrices()}
            </View>
            <View style={{ flexDirection: "column", flex: 0.2, alignItems: "flex-end" }}>
              <TouchableOpacity
                onPress={() => this.props.addToCart(this.props.item, this.state.number)}
                style={{
                  width: moderateScale(38, 0.5),
                  height: moderateScale(42, 0.5),
                  borderColor: colors.light_green,
                  borderWidth: 1,
                  borderRadius: 2,
                  backgroundColor: colors.light_green,
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <Image
                  source={require("@assets/images/cart.png")}
                  style={{
                    width: moderateScale(16, 0.5),
                    height: moderateScale(16, 0.5),
                    tintColor: "white",
                  }}
                />
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  holder: {
    flexDirection: "row",
    alignItems: "center",
    padding: 8,
    borderBottomColor: "rgba(0,0,0,0.2)",
    borderBottomWidth: 1,
    marginBottom: win.width / 25,
    backgroundColor: "white",
    marginHorizontal: win.width / 24,
  },
  productImage: {
    width: win.width / 6,
    height: win.width / 6,
  },
  name: {
    marginTop: 6,
    marginBottom: 6,
    color: colors.txt_description,
    fontSize: 12,
    flexWrap: "wrap",
  },
});
