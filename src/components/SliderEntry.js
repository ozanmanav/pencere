import React, { Component } from "react";
import { View, TouchableOpacity, Dimensions } from "react-native";
import styles from "@common/styles/SliderEntry.style";
import PropTypes from "prop-types";
import Text from "@components/MyText";
import SmartImage from "@components/SmartImage";
import { moderateScale } from "react-native-size-matters";

const win = Dimensions.get("window");
export default class SliderEntry extends Component {
  static propTypes = {
    data: PropTypes.object.isRequired,
    parallax: PropTypes.bool,
    parallaxProps: PropTypes.object,
  };
  state = {
    ratio: 0.625,
  };

  render() {
    return (
      <TouchableOpacity
        activeOpacity={1}
        style={{
          width: win.width,
          height: win.width * this.props.ratio,
          alignItems: "center",
          justifyContent: "center",
        }}
        onPress={() => this.props.bannerOnPress(this.props.data)}
      >
        <SmartImage
          // onLoad={(e) => {
          //     console.log("sliderEntry", e.nativeEvent);
          //     if ((e.nativeEvent.height / e.nativeEvent.width).toFixed(2) === '0.28' && this.state.ratio!==340/780)
          //         this.setState({ ratio: 340/780 });
          //     else if ((e.nativeEvent.height / e.nativeEvent.width) !== this.state.ratio) {
          //         this.setState({ ratio: e.nativeEvent.height / e.nativeEvent.width });
          //     }
          // }}
          //parallax={true}
          //parallaxProps={this.props.parallaxProps}
          showLoadingIndicator={true}
          defaultImageStyle={{ margin: moderateScale(20) }}
          source={{ uri: this.props.imageExtractor(this.props.data) }}
          resizeMode={this.props.resizeMethod}
          style={{
            width: win.width,
            height: win.width * this.props.ratio,
            resizeMode: this.props.resizeMethod,
          }}
        />
        {this.props.hasTitle && (
          <View style={styles.titleContainer}>
            <Text style={styles.sliderTitle}>{this.props.data.UrunAciklamasi}</Text>
          </View>
        )}
      </TouchableOpacity>
    );
  }
}
