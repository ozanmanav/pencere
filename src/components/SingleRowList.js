import React, { Component } from "react";
import { View, Dimensions, FlatList } from "react-native";
import Text from "@components/MyText";
import SingleRowProduct from "@components/SingleRowProduct";

var { height } = Dimensions.get("window");

export default class SingleRowList extends Component {
  constructor(props) {
    super(props);
  }
  page = 1;
  scrollToTop = () => {
    this.listView.scrollTo({ x: 0, y: 0, animated: true });
  };
  _onRefresh = () => {
    this.props.refreshSearch();
  };
  _renderRow(rowData) {
    return (
      <SingleRowProduct
        search={this.props.search}
        product={rowData}
        navigation={this.props.navigation}
        addToCart={this.props.addToCart}
      />
    );
  }
  _genRows() {
    this.page++;
    this.props.genRows(this.page);
  }
  render() {
    if (this.props.products.length > 0) {
      return (
        <FlatList
          ref={c => {
            this.listView = c;
          }}
          style={{ backgroundColor: "#e6e6e6" }}
          data={this.props.products}
          renderItem={rowData => this._renderRow(rowData)}
          onEndReachedThreshold={height / 3}
          onEndReached={() => this._genRows()}
          keyExtractor={item => item.UrunKodu}
          //contentContainerStyle={styles.list}
        />
      );
    } else
      return (
        <View style={{ marginTop: 20, flex: 1, alignItems: "center" }}>
          <Text style={{ fontSize: 16, color: "gray" }}>
            Kriterlerinize uygun sonuç bulunamadı...
          </Text>
        </View>
      );
  }
}
