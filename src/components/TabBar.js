import React, { Component } from "react";
import { View, Platform, TouchableWithoutFeedback, Alert } from "react-native";
import * as Animatable from "react-native-animatable";
import { connect } from "react-redux";
import { setDirty } from "@redux/actions";
import Device from "@common/Device";
import colors from "@common/styles/colors";
import { ScaledSheet } from "react-native-size-matters";
import { moderateScale } from "react-native-size-matters";

const styles = ScaledSheet.create({
  tabbar: {
    height: moderateScale(50, 0.4),
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: colors.primary_dark,
    borderWidth: 0,
  },
  tab: {
    alignSelf: "stretch",
    flex: 1,
    alignItems: "center",
    ...Platform.select({
      ios: {
        justifyContent: Device.isIphoneX ? "flex-start" : "center",
        paddingTop: Device.isIphoneX ? 12 : 0,
      },
      android: {
        justifyContent: "center",
      },
    }),
  },
});

class TabBar extends Component {
  static getCurrentHeight() {
    return moderateScale(50, 0.4) + (Device.isIphoneX ? 12 : 0) * 2;
  }

  tabbarButton = (route, index, focused, tintColor, renderIcon) => {
    var ref;
    return (
      <TouchableWithoutFeedback
        testID={"tabBar_" + route.routeName}
        key={route.key}
        style={styles.tab}
        onPress={() => this.onPress(route, index, ref)}
      >
        <Animatable.View useNativeDriver={true} ref={(c) => (ref = c)} style={styles.tab}>
          {renderIcon({
            route,
            index,
            focused,
            tintColor,
          })}
        </Animatable.View>
      </TouchableWithoutFeedback>
    );
  };

  onPress = (route, index, ref) => {
    ref.pulse(500);
    if (route.routeName === "SearchStack") {
      this.props.navigation.navigate({ routeName: "SearchScreen", params: { focus: false } });
      return;
    }
    if (this.props.homePage.dirty)
      Alert.alert(
        "Değişiklikler yapıldı",
        "Bu sayfayı terkederek yaptığınız değişikliklerin iptal edilmesini onaylıyormusunuz?",
        [
          {
            text: "Evet",
            onPress: () => {
              this.props.setDirty(false);
              if (route.routes && route.routes.length > 0)
                this.props.navigation.navigate(route.routes[0].routeName);
              else this.props.navigation.navigate(route.routeName);
            },
          },
          {
            text: "Hayır",
            style: "cancel",
          },
        ],
        { cancelable: false },
      );
    else {
      if (route.routes && route.routes.length > 0)
        this.props.navigation.navigate(route.routes[0].routeName);
      else this.props.navigation.navigate(route.routeName);
    }
  };

  render() {
    const { navigation, renderIcon, activeTintColor, inactiveTintColor } = this.props;

    const { routes } = navigation.state;
    const ignoreScreen = [
      "ShoppingCartStack",
      "CheckoutScreen",
      "CheckoutStack",
      "SearchScreen",
      "SettingsStack",
      "AnnouncementsStack",
      "LoginScreen",
      "SignUpScreen",
      "HelpStack",
      "OrdersStack",
      "ProductDetailsScreen",
      "GridScreen",
      "AccountsStack",
    ];
    return (
      <View ref={(c) => (this.tabbar = c)} style={styles.tabbar}>
        {routes &&
          routes.map((route, index) => {
            const focused = index === navigation.state.index;
            const tintColor = focused ? activeTintColor : inactiveTintColor;
            if (ignoreScreen.indexOf(route.key) > -1) {
              return <View key={route.key} />;
            }

            if (route.key === "MyOrders") {
              return <View key={route.key} />;
            }
            return this.tabbarButton(route, index, focused, tintColor, renderIcon);
          })}
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    cart: state.cart,
    favorites: state.favorites,
    homePage: state.homePage,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    setDirty: (dirty) => dispatch(setDirty(dirty)),
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(TabBar);
