import React, { Component } from "react";
import PropTypes from "prop-types";
import { View, StyleSheet, TouchableOpacity } from "react-native";

import colors from "@common/styles/colors";

import Icon from "react-native-vector-icons/FontAwesome";

import Line from "./Line";
import Text from "@components/MyText";

export default class ListPanel extends Component {
  static propTypes = {
    title: PropTypes.string.isRequired,
    description: PropTypes.string,
    onPressSeeAll: PropTypes.func,
  };

  constructor(props) {
    super(props);
  }

  render() {
    let headerLeftPartStyle = this.props.description
      ? { justifyContent: "center" }
      : { flexDirection: "row", alignItems: "center" };
    return (
      <View>
        <View style={styles.holder}>
          <View style={styles.header}>
            <View style={headerLeftPartStyle}>
              <Text style={{ fontSize: 16 }}>{this.props.title}</Text>
              <Text style={{ color: colors.txt_description }}>{this.props.description}</Text>
            </View>
            <TouchableOpacity onPress={this.props.onPressSeeAll} style={styles.headerRightPart}>
              <Text style={{ marginRight: 10, color: colors.txt_dark }}>Tümünü Gör</Text>
              <Icon style={{ color: colors.txt_dark }} name="angle-right" size={23} />
            </TouchableOpacity>
          </View>
          {this.props.children}
        </View>
        <Line />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  holder: {
    flexDirection: "column",
    paddingBottom: 20,
  },
  header: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingLeft: 15,
    paddingRight: 15,
  },
  headerRightPart: {
    flexDirection: "row",
    alignItems: "center",
    paddingTop: 20,
    paddingBottom: 20,
  },
});
