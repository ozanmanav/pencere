import React, { Component } from "react";
import { View, Animated, Easing } from "react-native";
import Text from "@components/MyText";
import { moderateScale } from "react-native-size-matters";
const AnimatedView = Animated.createAnimatedComponent(View);

export default class PencereIndicator extends Component {
  spinValue = new Animated.Value(0);

  constructor(props) {
    super(props);
  }

  runAnimation() {
    this.spinValue.setValue(0);

    Animated.timing(this.spinValue, {
      toValue: 1,
      duration: 1000,
      easing: Easing.linear,
      useNativeDriver: true,
    }).start(() => {
      this.runAnimation();
    });
  }

  componentDidMount() {
    this.runAnimation();
  }

  render() {
    const spin = this.spinValue.interpolate({
      inputRange: [0, 1],
      outputRange: ["0deg", "360deg"],
    });

    return (
      <View
        style={[
          {
            shadowColor: "#dedede",
            shadowRadius: 10,
            shadowOpacity: 0,
            position: "absolute",
            borderRadius: moderateScale(8, 0.5),
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
            borderColor: "#dedede",
            borderWidth: moderateScale(1, 0.5),
            height: moderateScale(90, 0.5),
            width: moderateScale(120, 0.5),
            backgroundColor: "white",
          },
          this.props.style,
        ]}
      >
        <AnimatedView
          style={{
            transform: [{ rotate: spin }],
            backgroundColor: "#00000000",
            width: moderateScale(33, 0.5),
            height: moderateScale(33, 0.5),
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <AnimatedView
            style={{
              position: "absolute",
              backgroundColor: "#00000000",
              width: moderateScale(32, 0.5),
              height: moderateScale(32, 0.5),
              borderRadius: moderateScale(16, 0.5),
              borderWidth: moderateScale(4, 0.5),
              borderColor: "#D6411E",
            }}
          />
          <AnimatedView
            style={{
              position: "absolute",
              width: moderateScale(8, 0.5),
              height: moderateScale(8, 0.5),
              backgroundColor: "white",
              right: 0,
              top: moderateScale(12, 0.5),
            }}
          />
          <View
            style={{
              backgroundColor: "black",
              width: moderateScale(8, 0.5),
              height: moderateScale(8, 0.5),
              borderRadius: moderateScale(4, 0.5),
            }}
          />
        </AnimatedView>
        <Text
          style={{
            fontFamily: "Poppins",
            color: "gray",
            fontSize: 13,
            marginTop: moderateScale(4, 0.5),
          }}
        >
          Yükleniyor...
        </Text>
      </View>
    );
  }
}
