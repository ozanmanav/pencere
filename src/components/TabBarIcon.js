import React, { Component } from "react";
import { View, Image } from "react-native";
import { connect } from "react-redux";
import colors from "@common/styles/colors";
import Text from "@components/MyText";
import { ScaledSheet } from "react-native-size-matters";

const styles = ScaledSheet.create({
  icon: {
    width: "18@ms0.4",
    height: "18@ms0.4",
    resizeMode: "contain",
    marginTop: "2@ms0.4",
  },
  numberWrap: {
    position: "absolute",
    top: -8,
    right: -10,
    height: 18,
    minWidth: 18,
    backgroundColor: "#f5c549",
    borderRadius: 9,
    alignItems: "center",
    justifyContent: "center",
  },
  number: {
    color: colors.light_black,
    fontSize: 13,
    marginLeft: 3,
    marginRight: 3,
    textAlign: "center",
  },
});

class TabBarIcon extends Component {
  render() {
    const { icon, label, tintColor, style } = this.props;
    //console.log("cart", cart);

    return (
      <View style={{ alignItems: "center" }}>
        <Image source={icon} style={[style, styles.icon, { tintColor }]} />
        <Text
          fontScale={0.5}
          style={{
            color: "white",
            textAlign: "center",
            fontSize: 12,
            lineHeight: 12,
            marginTop: 6,
            includeFontPadding: false,
          }}
        >
          {label}
        </Text>
      </View>
    );
  }
}

function mapStateToProps(state) {
  //console.log("state",state.cart);
  return {
    cart: state.cart,
    favorites: state.favorites,
  };
}
export default connect(mapStateToProps, null, null)(TabBarIcon);
