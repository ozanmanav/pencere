import React, { Component } from "react";
import { View, StyleSheet, Dimensions } from "react-native";
import ModalBox from "@components/ModalBox";
import CarouselContainer from "@components/CarouselContainer";

import colors from "@common/styles/colors";

const win = Dimensions.get("window");

export default class FullScreenCarousel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      specs: [],
    };
  }

  toggleVisibility = (specs) => {
    this.setState({
      specs,
    });
    this.modalBox.toggleVisibility();
  };

  render() {
    return (
      <ModalBox
        ref={(c) => (this.modalBox = c)}
        animationType="slide"
        presentationStyle="fullScreen"
        statusBarHidden={false}
        scrollable={true}
      >
        <View style={styles.container}>
          <View
            style={{
              flexDirection: "column",
              flexGrow: 1,
              backgroundColor: "white",
              justifyContent: "center",
            }}
          >
            {/* <View style={{ flexDirection: 'row', flex: 1, alignItems: 'center', justifyContent: 'flex-end' }}>
                            <TouchableOpacity onPress={() => this.toggleVisibility()}>
                                <Image
                                    source={require('@assets/images/x.png')}
                                    style={{ width: moderateScale(20, .5), height: moderateScale(20, .5), tintColor: '#dadada', resizeMode: 'contain' }} />
                            </TouchableOpacity>
                        </View> */}
            <CarouselContainer
              items={this.props.items}
              imageExtractor={(item) => item.UrunResimBuyuk}
              navigation={this.props.navigation}
              hasTitle={false}
            />
          </View>
        </View>
      </ModalBox>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.primary_dark,
    width: win.width,
    height: win.height,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.3,
    shadowRadius: 1,
    elevation: 1,
  },
});
