import React, { Component } from "react";
import { View, TouchableOpacity, Dimensions, ScrollView } from "react-native";
import Text from "@components/MyText";
import ModalBox from "@components/ModalBox";

import Feather from "react-native-vector-icons/Feather";
import { scale, moderateScale } from "react-native-size-matters";
import SafeAreaView from "react-native-safe-area-view";

const win = Dimensions.get("window");

export default class ScrollModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isVisible: false,
      keyboardVisible: false,
      keyboardHeight: 0,
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.isVisible !== prevProps.isVisible)
      this.setState({
        isVisible: this.props.isVisible,
      });
  }

  toggleVisibility = () => {
    this.modalBox.toggleVisibility();
  };

  render() {
    console.log("scrollModal", this.props);
    return (
      <ModalBox
        ref={c => (this.modalBox = c)}
        statusBarHidden={false}
        animationType="slide"
        presentationStyle="overFullScreen"
        scrollable={true}
        forceInset={this.props.forceInset}
      >
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            width: win.width,
          }}
        >
          <View
            style={{
              flexDirection: "column",
              alignSelf: "baseline",
              width: win.width,
              maxHeight: win.height,
              backgroundColor: "white",
            }}
          >
            <SafeAreaView
              forceInset={{ top: "always" }}
              style={[
                {
                  flexDirection: "row",
                  width: win.width,
                  alignItems: "center",
                  justifyContent: "space-between",
                  backgroundColor: this.props.headerColor ?? "#dedede",
                  paddingVertical: 10,
                },
                this.props.titleStyle,
              ]}
            >
              <Text
                style={{
                  color: this.props.titleColor ?? "black",
                  fontSize: 14,
                  fontWeight: "bold",
                  marginLeft: scale(10),
                }}
              >
                {this.props.title ?? ""}
              </Text>
              <TouchableOpacity
                style={{ marginRight: scale(10) }}
                onPress={() => this.toggleVisibility()}
              >
                <Feather
                  size={moderateScale(22, 0.5)}
                  name="x-circle"
                  color={this.props.titleColor ?? "#a4a4a4"}
                />
              </TouchableOpacity>
            </SafeAreaView>
            <ScrollView
              contentContainerStyle={[
                { backgroundColor: "white", width: win.width },
                this.props.contentContainerStyle,
              ]}
            >
              {this.props.renderContent()}
            </ScrollView>
          </View>
        </View>
      </ModalBox>
    );
  }
}
