import React, { Component } from "react";
import { View, TouchableOpacity, StyleSheet, Dimensions } from "react-native";
import Text from "@components/MyText";

const win = Dimensions.get("window");

export default class DatePicker extends Component {
  constructor(props) {
    super(props);
    this.state = {
      date: new Date(),
      isDialogActive: false,
    };
    this.options = { year: "numeric", month: "numeric", day: "numeric" };
  }

  setDate(newDate) {
    this.setState({ chosenDate: newDate });
  }

  componentDidMount() {
    this.setState({
      date: this.props.date,
    });
  }

  onPress = () => {
    this.props.openDatePickerIOS(
      this.props.date,
      this.props.id,
      this.props.minimumDate,
      this.props.maximumDate,
    );
    this.setState({
      isDialogActive: !this.state.isDialogActive,
    });
  };

  render() {
    return (
      <View>
        <TouchableOpacity style={styles.container} onPress={this.onPress}>
          <Text style={styles.dateTxt}>
            {this.props.date.toLocaleDateString("tr-TR", this.options)}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: win.width * 0.4,
    padding: 5,
    borderColor: "#d0d0d0",
    borderRadius: 5,
    borderWidth: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  dateTxt: {
    fontSize: 16,
    color: "#333",
  },
});
