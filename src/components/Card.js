import React, { Component } from "react";
import { TouchableOpacity, Dimensions, StyleSheet } from "react-native";

const win = Dimensions.get("window");

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#fff",
    marginHorizontal: win.width * 0.02,
    borderWidth: 1,
    borderRadius: 2,
    borderColor: "#ddd",
    borderBottomWidth: 0,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.3,
    shadowRadius: 1,
    elevation: 1,
    marginTop: 10,
  },
});

export default class Card extends Component {
  render() {
    return (
      <TouchableOpacity
        style={[styles.container, this.props.containerStyle]}
        onPress={this.props.onPress}
      >
        {this.props.renderContent(this.props.item)}
      </TouchableOpacity>
    );
  }
}
