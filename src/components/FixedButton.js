import React, { Component } from "react";
import { TouchableOpacity, Image, Animated } from "react-native";

export default class FixedButton extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fadeAnim: new Animated.Value(0),
      isArrowVisible: false,
    };
    this.startTime = 0;
  }

  startFadeAnim(reversed) {
    if (Date.now() - this.startTime > 300) {
      this.setState({ isArrowVisible: !reversed });
      this.startTime = Date.now();
      Animated.timing(this.state.fadeAnim, {
        toValue: reversed ? 0 : 0.75,
        duration: 300,
      }).start();
    }
  }

  render() {
    return (
      <Animated.View
        style={[
          {
            position: "absolute",
            bottom: 20,
            right: 20,
            alignItems: "center",
            justifyContent: "center",
            height: 50,
            width: 50,
            opacity: this.state.fadeAnim,
            transform: [
              {
                scale: this.state.fadeAnim.interpolate({
                  inputRange: [0, 0.75],
                  outputRange: [0, 1],
                  extrapolate: "clamp",
                }),
              },
            ],
          },
        ]}
      >
        <TouchableOpacity onPress={this.props.scrollToTop}>
          <Image
            style={{ width: 40, height: 40 }}
            source={require("@assets/images/arrow-fab.png")}
          />
        </TouchableOpacity>
      </Animated.View>
    );
  }
}
