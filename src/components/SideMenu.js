import PropTypes from "prop-types";
import React, { Component } from "react";
import { NavigationActions, DrawerActions } from "react-navigation";
import {
  Dimensions,
  ScrollView,
  View,
  Image,
  TouchableOpacity,
  SafeAreaView,
  LayoutAnimation,
  RefreshControl,
} from "react-native";
import { connect } from "react-redux";
import Text from "@components/MyText";
import SideMenuCat from "@components/SideMenuCat";

import { logout, closeDrawer, fetchSearchFromApi, fetchCategoriesFromApi } from "@redux/actions";
import NavigationService from "@app/NavigationService";
import { ScaledSheet } from "react-native-size-matters";
import { moderateScale } from "react-native-size-matters";
var visilabsManager = require("@app/visilabsManager");

const win = Dimensions.get("window");
const paddingHorizontal = win.width * 0.04;

var SpringYLayoutAnim = {
  duration: 400,
  create: {
    type: LayoutAnimation.Types.spring,
    property: LayoutAnimation.Properties.scaleY,
    springDamping: 0.9,
  },
  update: {
    type: LayoutAnimation.Types.spring,
    property: LayoutAnimation.Properties.scaleY,
    springDamping: 0.9,
  },
};
var SpringXLayoutAnim = {
  duration: 400,
  create: {
    type: LayoutAnimation.Types.spring,
    property: LayoutAnimation.Properties.scaleXY,
    springDamping: 0.9,
  },
  update: {
    type: LayoutAnimation.Types.spring,
    property: LayoutAnimation.Properties.scaleXY,
    springDamping: 0.9,
  },
};

class SideMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedCategoryIndex: -1,
      categoriesStack: [],
      stackIndex: 0,
      selectedTitle: "",
    };
  }

  backHandler = () => {
    this.props.closeDrawer();
    return true;
  };

  componentDidUpdate(prevProps, prevState) {
    if (
      this.props.category.items &&
      prevProps.category !== this.props.category &&
      this.props.category.items.length > 0
    ) {
      this.setState({
        categoriesStack: new Array(this.props.category.items),
      });
    }
  }

  onPressItem(item, goSearch) {
    if (goSearch) {
      this.props.closeDrawer();
      this.props.search("", true, { Hiyerarsi: item.Hid });
      NavigationService.navigate(null, "SearchScreen", { focus: false });
      var data = { "OM.clist": item.Hid };
      visilabsManager.customEvent("Category View", data);
    } else if (item.ilH.length > 0) {
      var stack = this.state.categoriesStack;
      stack.push(item.ilH);
      this.setState({
        categoriesStack: stack,
        stackIndex: this.state.stackIndex + 1,
        selectedTitle: item.Htxt,
      });
      LayoutAnimation.configureNext(SpringYLayoutAnim);
    }
  }

  pop = () => {
    var stack = this.state.categoriesStack;
    stack.pop();
    this.setState({
      categoriesStack: stack,
      stackIndex: this.state.stackIndex - 1,
      selectedTitle: "",
    });
    LayoutAnimation.configureNext(SpringXLayoutAnim);
  };

  navigateToScreen = route => () => {
    const navigateAction = NavigationActions.navigate({
      routeName: route,
    });
    this.props.navigation.dispatch(navigateAction);
  };

  onPressLogOut = () => {
    this.props.navigation.dispatch(DrawerActions.closeDrawer());
    this.props.logout().then(() => {
      this.props.navigation.navigate("Auth");
    });
  };

  onRefresh = () => {
    this.props.getCategories();
  };

  render() {
    return (
      <SafeAreaView
        testID="sideMenu"
        style={{
          backgroundColor: "#4e4e4e",
          width: win.width * 0.9,
          height: win.height,
          paddingRight: win.width * 0.05,
        }}
      >
        <ScrollView
          testID="scroll_drawer"
          refreshControl={
            <RefreshControl
              tintColor="white"
              refreshing={this.props.category.isFetching}
              onRefresh={this.onRefresh}
              progressViewOffset={130}
            />
          }
          style={styles.container}
        >
          <View
            style={{
              height: moderateScale(40, 0.5),
              backgroundColor: "#4e4e4e",
              padding: moderateScale(10, 0.5),
              paddingHorizontal,
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center",
            }}
          >
            <Text fontScale={0.5} style={styles.title}>
              KATEGORİLER
            </Text>
            <TouchableOpacity onPress={() => this.props.closeDrawer()}>
              <Text
                fontScale={0.5}
                style={{
                  color: "#a2a2a2",
                  fontFamily: "Poppins-Medium",
                  fontSize: 13,
                  lineHeight: 16,
                  marginTop: 5,
                }}
              >
                KAPAT
              </Text>
            </TouchableOpacity>
          </View>
          {this.state.stackIndex > 0 && (
            <View style={{ borderBottomColor: "#4a4a4a", borderBottomWidth: 1 }}>
              <TouchableOpacity
                style={styles.backItem}
                onPress={() => {
                  this.setState({
                    categoriesStack: new Array(this.props.category.items),
                    stackIndex: 0,
                  });
                  LayoutAnimation.configureNext(SpringXLayoutAnim);
                }}
              >
                <Image
                  source={require("@assets/images/left-arrow-mini.png")}
                  style={[
                    styles.arrow,
                    {
                      tintColor: "#c4c4c4",
                      height: moderateScale(12, 0.5),
                      width: moderateScale(12, 0.5),
                    },
                  ]}
                />
                <Text
                  fontScale={0.5}
                  style={{
                    color: "#FFCA28",
                    fontFamily: "Poppins-Medium",
                    fontSize: 15,
                    lineHeight: 18,
                    marginTop: 4,
                  }}
                >
                  {this.state.selectedTitle}
                </Text>
              </TouchableOpacity>
            </View>
          )}
          {/* {
            this.state.stackIndex > 1 &&
            <TouchableOpacity style={styles.backItem} onPress={() => this.setState({ categoriesStack: new Array(this.props.category.items) })} >
              <Image source={require('@assets/images/left-arrow-mini.png')} style={styles.arrow} />
              <Text fontScale={0.5}
                onPress={this.pop}
                style={{ color: '#51a32e', fontFamily: 'Poppins-Medium', fontSize: 18, lineHeight: 22, marginTop: 6, }} >{this.state.selectedTitle}</Text>
            </TouchableOpacity>
          } */}
          {this.state.categoriesStack &&
            this.state.categoriesStack.length > 0 &&
            this.state.categoriesStack[this.state.categoriesStack.length - 1].map(item => {
              if (this.state.stackIndex > 0) {
                return (
                  <SideMenuCat
                    key={item.Hid}
                    item={item}
                    stackIndex={this.state.stackIndex}
                    onPressItem={(_item, goSearch) => this.onPressItem(_item, goSearch)}
                  />
                );
              } else {
                //main cats
                return (
                  <View key={item.Hid} style={{ flexDirection: "column" }}>
                    <TouchableOpacity
                      accessibilityLabel={"drawer_" + item.Hid}
                      style={[styles.catItem]}
                      onPress={() => this.onPressItem(item, this.state.stackIndex > 0)}
                    >
                      <View style={{ flexDirection: "row", alignItems: "center" }}>
                        {this.state.stackIndex === 0 && item.Hk === 1 && (
                          <Image
                            key={item.Hid}
                            source={{
                              uri: !item.ImgURL.startsWith("http")
                                ? "https://" + item.ImgURL
                                : item.ImgURL,
                            }}
                            style={{
                              width: moderateScale(20, 0.5),
                              height: moderateScale(20, 0.5),
                              resizeMode: "contain",
                              tintColor: "white",
                            }}
                          />
                        )}
                        <Text
                          fontScale={0.5}
                          ellipsizeMode="clip"
                          style={{
                            marginLeft: this.state.stackIndex === 0 ? win.width * 0.04 : 0,
                            color: this.state.stackIndex > 0 ? "#66BB6A" : "white",
                            fontFamily: "Poppins-Medium",
                            fontSize: this.state.stackIndex > 0 ? 15 : 14,
                            lineHeight: 17,
                            marginTop: 5,
                            maxWidth: win.width * 0.7,
                            flex: 1,
                          }}
                        >
                          {item.Htxt}
                        </Text>
                      </View>
                      <Image
                        source={require("@assets/images/arrow-right.png")}
                        style={styles.arrow}
                      />
                    </TouchableOpacity>
                  </View>
                );
              }
            })}
          <TouchableOpacity
            onPress={() => {
              this.props.closeDrawer();
              NavigationService.navigate(null, "SearchScreen", {
                searchParams: { SearchText: "firsat" },
                focus: false,
              });
            }}
            style={styles.outerItem}
          >
            <Text fontScale={0.5} style={styles.btnText}>
              FIRSAT ÜRÜNLERİ
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              this.props.closeDrawer();
              NavigationService.navigate(null, "SearchScreen", {
                searchParams: { SearchText: "defolu ürünler" },
                focus: false,
              });
            }}
            style={styles.outerItem}
          >
            <Text fontScale={0.5} style={styles.btnText}>
              DEFOLU ÜRÜNLER
            </Text>
          </TouchableOpacity>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

SideMenu.propTypes = {
  navigation: PropTypes.object,
};

function mapStateToProps(state) {
  return {
    category: state.category,
    drawer: state.drawer,
    session: state.session,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    logout: () => dispatch(logout()),
    closeDrawer: () => dispatch(closeDrawer()),
    getCategories: () => dispatch(fetchCategoriesFromApi()),
    search: (query, refresh, searchParams) =>
      dispatch(fetchSearchFromApi(query, refresh, searchParams)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SideMenu);

const styles = ScaledSheet.create({
  arrow: {
    width: moderateScale(10, 0.5),
    height: moderateScale(10, 0.5),
    resizeMode: "contain",
    tintColor: "#a2a2a2",
    marginRight: moderateScale(10, 0.5),
  },
  container: {
    backgroundColor: "#4e4e4e",
    width: win.width * 0.85,
    borderRightColor: "#4e4e4e",
    borderRightWidth: 1,
  },
  catItem: {
    borderTopColor: "#4e4e4e",
    borderBottomColor: "black",
    borderBottomWidth: 1,
    borderTopWidth: 1,
    backgroundColor: "#353535",
    alignItems: "center",
    justifyContent: "space-between",
    height: "40@ms0.5",
    paddingHorizontal: paddingHorizontal * 1.5,
    flexDirection: "row",
  },
  backItem: {
    borderBottomColor: "black",
    borderBottomWidth: 1,
    backgroundColor: "#353535",
    alignItems: "center",
    height: "40@ms0.5",
    paddingHorizontal,
    flexDirection: "row",
  },
  outerItem: {
    backgroundColor: "transparent",
    alignItems: "center",
    justifyContent: "space-between",
    height: "40@ms0.5",
    paddingHorizontal,
    flexDirection: "row",
  },
  title: {
    color: "white",
    fontFamily: "Poppins-Medium",
    fontSize: 15,
    lineHeight: 19,
    marginTop: 5,
  },
  btnText: {
    color: "white",
    fontFamily: "Poppins-Medium",
    fontSize: 13,
    lineHeight: 16,
    marginTop: 5,
  },
});
