import React, { Component } from "react";
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Image,
  Dimensions,
  Platform,
  StatusBar,
  TouchableWithoutFeedback,
} from "react-native";
const win = Dimensions.get("window");
import Text from "@components/MyText";

import { moderateScale } from "react-native-size-matters";

export default class SimpleHeader extends Component {
  render() {
    return (
      <View style={[styles.header, this.props.style]}>
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "space-between",
            flex: 1,
            paddingRight: moderateScale(10, 0.4),
          }}
        >
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
            }}
          >
            <TouchableOpacity
              onPress={
                this.props.onBackPress
                  ? this.props.onBackPress
                  : () => this.props.navigation.goBack()
              }
              style={{
                paddingHorizontal: 5,
                height: 36,
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <Image
                style={[
                  {
                    width: moderateScale(18, 0.4),
                    height: moderateScale(20, 0.4),
                    tintColor: this.props.iconColor || "#212121",
                  },
                ]}
                source={require("@assets/images/left-arrow-mini.png")}
              />
            </TouchableOpacity>
            <TouchableWithoutFeedback
              onPress={
                this.props.onBackPress
                  ? this.props.onBackPress
                  : () => this.props.navigation.goBack()
              }
            >
              <Text
                fontScale={0.5}
                style={{
                  color: "white",
                  fontSize: 18,
                  fontFamily: "Poppins-SemiBold",
                  textAlign: "center",
                  paddingLeft: moderateScale(8, 0.5),
                  marginTop: moderateScale(2, 0.5),
                }}
              >
                {this.props.title}
              </Text>
            </TouchableWithoutFeedback>
          </View>
          {this.props.address && (
            <TouchableOpacity
              style={{ padding: moderateScale(5, 0.4), paddingHorizontal: 0 }}
              onPress={() => this.props.navigation.navigate("NewAddressScreen")}
            >
              <Text style={{ color: "white", fontSize: 16, fontWeight: "bold" }}>
                Adres Oluştur
              </Text>
            </TouchableOpacity>
          )}
          {this.props.newAddress && (
            <TouchableOpacity
              style={{ padding: moderateScale(5, 0.4) }}
              onPress={() => this.props.onPressContinue()}
            >
              <Text style={{ color: "white", fontSize: 16, fontWeight: "bold" }}>Devam</Text>
            </TouchableOpacity>
          )}
          {this.props.contract && (
            <TouchableOpacity
              style={{ padding: moderateScale(5, 0.4) }}
              onPress={() => this.props.toggleDialog()}
            >
              <Text style={{ color: "white", fontSize: 16, fontWeight: "bold" }}>Onaylıyorum</Text>
            </TouchableOpacity>
          )}
          {this.props.button && this.props.button()}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    borderWidth: 1,
    borderColor: "#ddd",
    borderBottomWidth: 0,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    backgroundColor: "white",
    width: win.width,
    height: moderateScale(50, 0.4),
    position: "absolute",
    zIndex: 9999,
    top: Platform.OS === "android" ? (Platform.Version < 20 ? 0 : StatusBar.currentHeight) : 0,
  },
});
