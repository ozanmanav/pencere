import React, { Component } from "react";
import {
  TouchableOpacity,
  Image,
  Animated,
  View,
  Dimensions,
  TextInput,
  Platform,
  StatusBar,
  LayoutAnimation,
  TouchableWithoutFeedback,
} from "react-native";
import { connect } from "react-redux";
import Text from "@components/MyText";
import colors from "@common/styles/colors";
import { openDrawer, closeDrawer, addToSearchHistory } from "@redux/actions";
import { ScaledSheet } from "react-native-size-matters";
import { moderateScale } from "react-native-size-matters";
const win = Dimensions.get("window");

const NAVBAR_HEIGHT = moderateScale(100, 0.4);

var CustomLayoutSpring = {
  duration: 400,
  create: {
    type: LayoutAnimation.Types.spring,
    property: LayoutAnimation.Properties.scaleXY,
    springDamping: 0.9,
  },
  update: {
    type: LayoutAnimation.Types.spring,
    springDamping: 0.8,
  },
};

class AnimatedHeader extends Component {
  static height = NAVBAR_HEIGHT;

  constructor(props) {
    super(props);
    var navbarAutoTranslate = new Animated.Value(0);
    this.state = {
      searchActive: false,
      text: "",
      navbarAutoTranslate,
      searchVisible: true,
    };
    this.renderContent.bind(this);
    this.focusSearch.bind(this);
  }

  componentDidMount() {
    this.setState({
      text: this.props.searchText ?? "",
    });
  }

  handleSubmit = event => {
    if (this.props.search) {
      this.props.search({}, event.nativeEvent.text);
      if (event.nativeEvent.text !== "") {
        this.props.addToSearchHistory(event.nativeEvent.text.toLowerCase());
      }
    }
  };

  toggleSearch(state) {
    this.setState({
      searchActive: state,
    });
  }

  clearText() {
    this.setState({
      text: "",
    });
  }
  renderContent() {
    return (
      <View
        style={{
          backgroundColor: colors.primary_dark,
          flexDirection: "row",
          height: moderateScale(50, 0.4),
          width: win.width,
          alignItems: "center",
          justifyContent: this.props.alignLeft ? "flex-start" : "center",
          paddingHorizontal: moderateScale(10, 0.4),
        }}
      >
        <View style={{ position: "absolute", left: moderateScale(0, 0.4) }}>
          {this.props.back ? (
            <TouchableOpacity
              testID="button_back"
              onPress={() =>
                this.props.onBackPressed
                  ? this.props.onBackPressed()
                  : this.props.navigation.goBack()
              }
              style={[
                styles.navbarBtnLeft,
                this.props.twoItems && { position: "absolute", top: 0, left: 0 },
              ]}
            >
              <Image
                style={[styles.headerIcons, { tintColor: "white" }]}
                source={require("@assets/images/left-arrow-mini.png")}
              />
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              testID="button_drawer"
              onPress={() => {
                this.props.openDrawer();
              }}
              style={[
                styles.navbarBtnLeft,
                this.props.twoItems && { position: "absolute", top: 0, left: 0 },
              ]}
            >
              <Image
                style={[styles.headerIcons, { tintColor: "white" }]}
                source={require("@assets/images/menu.png")}
              />
            </TouchableOpacity>
          )}
        </View>
        {this.props.title ? (
          <TouchableWithoutFeedback
            onPress={() =>
              this.props.onBackPressed ? this.props.onBackPressed() : this.props.navigation.goBack()
            }
          >
            <Text
              fontScale={0.5}
              style={{
                color: "white",
                fontFamily: "Poppins-SemiBold",
                fontSize: 18,
                marginTop: moderateScale(2, 0.5),
                textAlign: "center",
                marginLeft: this.props.alignLeft ? moderateScale(36, 0.4) : 0,
              }}
            >
              {this.props.title}
            </Text>
          </TouchableWithoutFeedback>
        ) : (
          <TouchableOpacity onPress={() => this.props.navigation.navigate("HomeScreen")}>
            <Image
              source={require("@assets/images/logo_text_white.png")}
              style={{
                height: moderateScale(18, 0.4),
                width: moderateScale(18 * 4.79, 0.4),
                resizeMode: "contain",
              }}
            />
          </TouchableOpacity>
        )}
        {!this.props.hideIcons && (
          <View
            style={{
              flexDirection: "row",
              justifyContent: "flex-end",
              position: "absolute",
              right: moderateScale(10, 0.4),
            }}
          >
            <TouchableOpacity
              style={{
                justifyContent: "center",
                padding: 5,
                paddingHorizontal: 6,
              }}
              onPress={() => this.props.navigation.navigate("AnnouncementsScreen")}
            >
              <Image
                source={require("@assets/images/bell.png")}
                style={[styles.icon, { tintColor: "white" }]}
              />
              {this.props.announcements.unreadCount > 0 && (
                <View style={styles.numberWrap}>
                  <Text fontScale={0.7} style={styles.number} includeFontPadding={false}>
                    {this.props.announcements.unreadCount > 9
                      ? "9+"
                      : this.props.announcements.unreadCount}
                  </Text>
                </View>
              )}
            </TouchableOpacity>

            <TouchableOpacity
              testID="button_cart"
              style={{
                justifyContent: "center",
                padding: moderateScale(5, 0.4),
                paddingHorizontal: moderateScale(8, 0.4),
              }}
              onPress={() =>
                this.props.navigation.navigate({
                  routeName: "ShoppingCartScreen",
                })
              }
            >
              <Image
                source={require("@assets/images/cart.png")}
                style={[styles.icon, { tintColor: "white" }]}
              />
              {this.props.cart.cartInfo.IlSeciliSepetKalemler && this.props.cart.count > 0 && (
                <View style={styles.numberWrap}>
                  <Text fontScale={0.7} style={styles.number}>
                    {this.props.cart.count > 9 ? "9+" : this.props.cart.count}
                  </Text>
                </View>
              )}
            </TouchableOpacity>
          </View>
        )}
      </View>
    );
  }

  focusSearch(focus) {
    if (focus) this.searchBox.focus();
    else this.searchBox.blur();
  }

  startSearchBarAnim = dir => {
    if (dir === "out") {
      if (this.state.searchVisible) {
        this.setState({ searchVisible: false });
        LayoutAnimation.configureNext(CustomLayoutSpring);
      }
    } else if (dir === "in") {
      if (!this.state.searchVisible) {
        this.setState({ searchVisible: true });
        LayoutAnimation.configureNext(CustomLayoutSpring);
      }
    }
  };
  searchPos = 0;
  onResponderEnd = () => {
    if (this.props.navigation.state.routeName !== "SearchScreen")
      this.props.navigation.navigate({
        routeName: "SearchScreen",
        params: { focus: true },
      });
  };

  render() {
    var navbarTranslate = 0;
    if (this.props.clampedScroll) {
      navbarTranslate = this.props.clampedScroll.interpolate({
        inputRange: [0, moderateScale(50, 0.4)],
        outputRange: [0, moderateScale(-50, 0.4)],
        extrapolate: "clamp",
      });
    }
    return (
      <Animated.View
        style={[
          styles.navbar,
          Platform.OS === "android" &&
            Platform.Version > 18 &&
            this.props.searchBar && {
              height:
                NAVBAR_HEIGHT +
                (Platform.Version < 20 ? 0 : StatusBar.currentHeight) +
                (!this.state.searchVisible ? -moderateScale(50, 0.4) : 0),
            },
        ]}
      >
        {Platform.OS === "android" && Platform.Version > 18 && (
          <View
            style={{
              height: Platform.Version < 20 ? 0 : StatusBar.currentHeight,
              width: win.width,
              backgroundColor: "#ce290c",
            }}
          />
        )}
        {this.props.searchBar && (
          <Animated.View
            style={[
              {
                height: moderateScale(80, 0.4),
                width: win.width,
                backgroundColor: colors.primary_dark,
                position: "absolute",
                top:
                  Platform.OS === "android" && Platform.Version > 18
                    ? (this.state.searchVisible
                        ? moderateScale(20, 0.4)
                        : moderateScale(-30, 0.4)) +
                      (Platform.Version < 20 ? 0 : StatusBar.currentHeight)
                    : this.state.searchVisible
                    ? moderateScale(20, 0.4)
                    : moderateScale(-30, 0.4),
                left: 0,
              },
              {
                transform: [
                  {
                    translateY: this.props.clampedScroll
                      ? navbarTranslate
                      : this.state.navbarAutoTranslate,
                  },
                ],
              },
            ]}
          >
            <View style={{ height: moderateScale(30, 0.4) }} />
            <View
              style={{
                flexDirection: "row",
                backgroundColor: "white",
                borderRadius: 5,
                height: moderateScale(34, 0.4),
                marginTop: 0,
                marginHorizontal: moderateScale(10, 0.4),
                alignItems: "center",
                justifyContent: "center",
                paddingRight: moderateScale(5, 0.4),
              }}
            >
              {this.props.showSuggestions ? (
                <TouchableOpacity
                  onPress={() => this.searchBox.blur()}
                  style={{
                    justifyContent: "center",
                    alignItems: "center",
                    paddingRight: moderateScale(10, 0.4),
                    paddingLeft: moderateScale(8, 0.4),
                  }}
                >
                  <Image
                    source={require("@assets/images/left-arrow-mini.png")}
                    style={{
                      width: moderateScale(18, 0.4),
                      height: moderateScale(18, 0.4),
                      tintColor: colors.light_gray,
                    }}
                  />
                </TouchableOpacity>
              ) : (
                <TouchableOpacity
                  style={{
                    justifyContent: "center",
                    alignItems: "center",
                    paddingHorizontal: moderateScale(10, 0.4),
                  }}
                >
                  <Image
                    source={require("@assets/images/search.png")}
                    style={{
                      width: moderateScale(18, 0.4),
                      height: moderateScale(18, 0.4),
                      tintColor: colors.light_gray,
                      resizeMode: "contain",
                    }}
                  />
                </TouchableOpacity>
              )}
              {this.props.search ? (
                <TextInput
                  testID="input_search"
                  returnKeyType="search"
                  ref={node => {
                    this.searchBox = node;
                  }}
                  placeholder={"Pencere'de ara..."}
                  style={{
                    fontSize: moderateScale(16, 0.5),
                    flex: 1,
                    height: moderateScale(30, 0.4),
                    color: "black",
                    padding: 0,
                  }}
                  autoCapitalize="none"
                  selectTextOnFocus
                  onSubmitEditing={this.handleSubmit}
                  multiline={false}
                  editable={
                    Platform.OS === "android" ? true : typeof this.props.search === "function"
                  }
                  onFocus={() =>
                    this.props.toggleSuggestions && this.props.toggleSuggestions(true, true)
                  }
                  onBlur={() =>
                    this.props.toggleSuggestions && this.props.toggleSuggestions(false, true)
                  }
                  onResponderEnd={this.onResponderEnd}
                  value={this.props.searchText}
                  onChangeText={this.props.onChangeText}
                />
              ) : (
                <TouchableOpacity
                  style={{
                    fontSize: moderateScale(16, 0.5),
                    flex: 1,
                    height: moderateScale(30, 0.4),
                    color: "black",
                    padding: 0,
                    justifyContent: "center",
                  }}
                  onPress={this.onResponderEnd}
                >
                  <Text style={{ color: "grey", fontSize: 16 }}>Pencere&apos;de ara...</Text>
                </TouchableOpacity>
              )}
            </View>
          </Animated.View>
        )}
        {this.renderContent()}
      </Animated.View>
    );
  }
}

function mapStateToProps(state) {
  return {
    cart: state.cart,
    homePage: state.homePage,
    announcements: state.announcements,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    openDrawer: () => dispatch(openDrawer()),
    closeDrawer: () => dispatch(closeDrawer()),
    addToSearchHistory: text => dispatch(addToSearchHistory(text)),
  };
}
export default connect(mapStateToProps, mapDispatchToProps, null, { withRef: true })(
  AnimatedHeader,
);

const styles = ScaledSheet.create({
  icon: {
    width: "22@ms0.4",
    height: "22@ms0.4",
    resizeMode: "contain",
  },
  numberWrap: {
    position: "absolute",
    top: 0,
    right: 0,
    height: "16@ms0.4",
    minWidth: "17@ms0.4",
    backgroundColor: "#f5c549",
    borderRadius: "7@ms0.4",
    alignItems: "center",
    justifyContent: "center",
  },
  number: {
    color: "black",
    fontSize: 9,
    textAlign: "center",
    includeFontPadding: false,
    letterSpacing: 0,
  },
  navbarBtnLeft: {
    paddingRight: moderateScale(12, 0.5),
    paddingVertical: moderateScale(6, 0.5),
    paddingLeft: moderateScale(10, 0.4),
    alignItems: "center",
    justifyContent: "center",
  },
  headerIcons: {
    width: "20@ms0.4",
    height: "20@ms0.4",
    resizeMode: "contain",
    opacity: 0.9,
  },
  navbarBtnRight: {
    position: "absolute",
    top: 0,
    right: 0,
  },
  navbar: {
    flexDirection: "column",
    width: win.width,
    backgroundColor: "transparent",
    position: "absolute",
    top: 0,
    left: 0,
  },
});
