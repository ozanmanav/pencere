import React, { Component } from "react";
import { View, Dimensions, StyleSheet, Image, TouchableOpacity } from "react-native";
import colors from "@common/styles/colors";
import Text from "@components/MyText";
import { moderateScale } from "react-native-size-matters";
import SmartImage from "./SmartImage";

const { width } = Dimensions.get("window");
const prdWidth = width / 2.25;
const horizontalPrdWidth = width / 1.3;

export default class SwiperProductThumb extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    if (!this.props.horizontal)
      return (
        <TouchableOpacity style={styles.holder} onPress={this.props.onPress}>
          <View style={{ alignItems: "center", flexDirection: "column", flex: 6 }}>
            <Image
              style={styles.productImage}
              source={{ uri: this.props.ThumbImageURL }}
              resizeMode={"contain"}
            />
          </View>
          <View style={{ flexDirection: "column", flex: 4 }}>
            <View style={{ flex: 2, alignItems: "flex-start", justifyContent: "flex-start" }}>
              <Text style={styles.name}>{this.props.Title}</Text>
            </View>
            <View style={{ flex: 1 }}>
              <Text
                style={[
                  styles.name,
                  { color: colors.price_color, fontSize: 14, fontFamily: "Poppins-Medium" },
                ]}
              >
                {this.props.PriceText}
              </Text>
            </View>
          </View>
          {/* <View style={styles.promotionHolder}>
                    <Text style={[styles.name, { textDecorationLine: 'line-through' }]}>{this.UrunFiyatStr}</Text>
                </View> */}
        </TouchableOpacity>
      );
    else {
      return (
        <TouchableOpacity
          style={{
            flexDirection: "row",
            width: horizontalPrdWidth + moderateScale(20, 0.5),
            paddingRight: moderateScale(15, 0.5),
          }}
          onPress={this.props.onPress}
        >
          <View style={{ flexDirection: "column" }}>
            <SmartImage
              showLoadingIndicator={true}
              style={{
                width: horizontalPrdWidth,
                height: horizontalPrdWidth * 0.35,
              }}
              source={{ uri: this.props.ThumbImageURL }}
              resizeMode={"contain"}
            />
            <Text numberOfLines={2} ellipsizeMode="tail" style={[styles.name]}>
              {this.props.Title}
            </Text>
            <Text
              style={[
                styles.name,
                { color: colors.price_color, fontSize: 14, fontFamily: "Poppins-SemiBold" },
              ]}
            >
              {this.props.PriceText}
            </Text>
            {this.props.addToCart && (
              <TouchableOpacity
                onPress={() => this.props.addToCart({ Code: this.props.UrunKodu }, 1)}
                style={{
                  marginRight: 15,
                  backgroundColor: "#5eb139",
                  justifyContent: "center",
                  alignItems: "center",
                  height: 32,
                  borderRadius: 3,
                }}
              >
                <Text style={{ fontFamily: "Poppins-Medium", fontSize: 15, color: "white" }}>
                  Sepete Ekle
                </Text>
              </TouchableOpacity>
            )}
          </View>
        </TouchableOpacity>
      );
    }
  }
}

const styles = StyleSheet.create({
  holder: {
    width: prdWidth + 20,
    flexDirection: "column",
    borderRightColor: "gray",
    justifyContent: "center",
    alignItems: "center",
  },
  productImage: {
    width: prdWidth * 0.6,
    height: prdWidth * 0.6,
  },
  name: {
    fontFamily: "Poppins",
    paddingTop: moderateScale(2, 0.5),
    paddingBottom: moderateScale(2, 0.5),
    paddingLeft: moderateScale(2, 0.5),
    color: colors.txt_black,
    fontSize: 14,
    textAlign: "center",
  },
});
