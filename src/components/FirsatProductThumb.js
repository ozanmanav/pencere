import React, { Component } from "react";
import { View, Dimensions, StyleSheet, TouchableOpacity } from "react-native";
import Text from "@components/MyText";

import colors from "@common/styles/colors";
import { moderateScale } from "react-native-size-matters";
import SmartImage from "./SmartImage";

const { width } = Dimensions.get("window");
const prdWidth = width / 2.25;

export default class FirsatProductThumb extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <TouchableOpacity style={styles.holder} onPress={this.props.onPress}>
        <View
          style={{
            alignItems: "center",
            flexDirection: "column",
            marginTop: moderateScale(5, 0.5),
          }}
        >
          <SmartImage
            showLoadingIndicator={true}
            style={styles.productImage}
            source={{ uri: this.props.item.FirsatResim }}
            resizeMode={"contain"}
          />
        </View>
        <View style={{ flexDirection: "column" }}>
          <View
            style={{
              flexDirection: "row",
              alignItems: "flex-start",
              justifyContent: "center",
              flex: 1,
              marginTop: moderateScale(5, 0.4),
            }}
          >
            <Text numberOfLines={2} ellipsizeMode="tail" style={styles.name}>
              {this.props.item.UrunAciklama}
            </Text>
          </View>
          {this.props?.item?.FirsatEskiFiyat != null && this.props?.item?.FirsatEskiFiyat !== "" && (
            <View style={styles.promotionHolder}>
              <Text style={[styles.name, { textDecorationLine: "line-through" }]}>
                {this.props.item.FirsatEskiFiyat + " " + this.props.item.FirsatFiyati.split(" ")[1]}
              </Text>
            </View>
          )}
          <View style={{ flex: 1 }}>
            <Text
              style={[
                styles.name,
                { color: colors.price_color, fontSize: 14, fontFamily: "Poppins" },
              ]}
            >
              {this.props.item.FirsatFiyati}
            </Text>
          </View>
        </View>
        {typeof this.props.item.KalanGun !== "undefined" && this.props.item.KalanGun !== "" && (
          <View
            style={{
              position: "absolute",
              top: 0,
              right: 0,
              flexDirection: "row",
              alignItems: "center",
              width: moderateScale(50, 0.5),
              height: moderateScale(50, 0.5),
              borderRadius: moderateScale(25.5),
              paddingHorizontal: moderateScale(5, 0.5),
              justifyContent: "space-evenly",
              backgroundColor: colors.light_green,
            }}
          >
            <View
              style={{ alignItems: "center", justifyContent: "center", flex: 1, paddingLeft: 2 }}
            >
              <Text
                fontScale={0.6}
                style={{
                  fontSize: 7,
                  color: "white",
                  textAlign: "center",
                  fontFamily: "Poppins-SemiBold",
                  lineHeight: 9,
                  marginTop: 1,
                }}
              >
                {"SON GÜN"}
              </Text>
            </View>
            {this.props.item && (
              <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
                <Text
                  fontScale={0.5}
                  style={{
                    fontSize:
                      this.props.item.KalanGun.toString().length === 1
                        ? 20
                        : this.props.item.KalanGun.toString().length === 2
                        ? 12
                        : 8,
                    color: "white",
                    textAlign: "center",
                    fontFamily: "Poppins-SemiBold",
                  }}
                >
                  {this.props.item.KalanGun > 99 ? "99+" : this.props.item.KalanGun}
                </Text>
              </View>
            )}
          </View>
        )}
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  holder: {
    width: prdWidth + 20,
    flexDirection: "column",
    borderRightColor: "gray",
    justifyContent: "center",
  },
  productImage: {
    width: prdWidth * 0.6,
    height: prdWidth * 0.6,
  },
  name: {
    fontFamily: "Poppins",
    paddingBottom: moderateScale(2, 0.5),
    paddingLeft: (4, 0.5),
    color: colors.txt_black,
    fontSize: 14,
    textAlign: "center",
  },
});
