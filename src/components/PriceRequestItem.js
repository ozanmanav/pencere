import React from "react";
import { View, TouchableOpacity, Image } from "react-native";
import Text from "@components/MyText";
import { ScaledSheet, moderateScale } from "react-native-size-matters";
import { getWidth, getDate } from "@common/Utils";
import colors from "@common/styles/colors";

const PADDING = getWidth() / 25;

export default function PriceRequestItem(props) {
  const indirim =
    props.item.TeklifIndirimi > 0
      ? props.item.TeklifIndirimi.toFixed(2).replace(",", "").replace(".", ",") +
        " " +
        (props.item.ParaBirimi ? props.item.ParaBirimi : "")
      : "---";
  return (
    <View style={styles.card}>
      <View style={{ flexDirection: "column", flex: 1 }}>
        {props.item.FiyatTalepNo && (
          <View style={styles.row}>
            <Text fontScale={0.5} numberOfLines={1} style={styles.titleTxt}>
              Talep No
            </Text>
          </View>
        )}
        {props.item.Urunler && (
          <View style={styles.row}>
            <Text fontScale={0.5} numberOfLines={1} style={styles.titleTxt}>
              Teklifteki Ürünler
            </Text>
          </View>
        )}
        {props.item.ToplamTutar != null && (
          <View style={styles.row}>
            <Text fontScale={0.5} numberOfLines={1} style={styles.titleTxt}>
              Toplam Tutar
            </Text>
          </View>
        )}
        {props.item.TeklifIndirimi != null && (
          <View style={styles.row}>
            <Text fontScale={0.5} numberOfLines={1} style={styles.titleTxt}>
              Teklif İndirimi
            </Text>
          </View>
        )}
        {props.item.OluşturmaTarihi != null && (
          <View style={styles.row}>
            <Text fontScale={0.5} numberOfLines={1} style={styles.titleTxt}>
              Oluşturma Tarihi
            </Text>
          </View>
        )}
        {props.item.GecerlilikTarihi != null && (
          <View style={styles.row}>
            <Text fontScale={0.5} numberOfLines={1} style={styles.titleTxt}>
              Geçerlilik Tarihi
            </Text>
          </View>
        )}
        {props.item.Durum != null && (
          <View style={styles.row}>
            <Text fontScale={0.5} numberOfLines={1} style={styles.titleTxt}>
              Durum
            </Text>
          </View>
        )}
        {props.item.Onaylandimi != null && (
          <View style={styles.row}>
            <Text fontScale={0.5} numberOfLines={1} style={styles.titleTxt}>
              İşlem
            </Text>
          </View>
        )}
      </View>
      <View style={{ flexDirection: "column", flex: 1 }}>
        {props.item.FiyatTalepNo && (
          <View style={styles.row}>
            <Text fontScale={0.5} numberOfLines={1} style={styles.valueTxt}>
              {props.item.FiyatTalepNo}
            </Text>
          </View>
        )}
        {props.item.Urunler && (
          <TouchableOpacity
            style={styles.row}
            onPress={() =>
              props.item.Urunler.length > 0 && props.listModal.toggleVisibility(props.item.Urunler)
            }
          >
            <Text
              fontScale={0.5}
              numberOfLines={1}
              style={[styles.valueTxt, { alignItems: "center" }]}
            >{`(${props.item.Urunler.length}) Ürünleri Göster`}</Text>
            <Image
              source={require("@assets/images/down.png")}
              style={{
                width: moderateScale(14, 0.3),
                height: moderateScale(14, 0.3),
                tintColor: "#dedede",
                resizeMode: "contain",
                marginLeft: moderateScale(10, 0.5),
                marginTop: moderateScale(2, 0.3),
              }}
            />
          </TouchableOpacity>
        )}
        {props.item.ToplamTutar != null && (
          <View style={styles.row}>
            <Text fontScale={0.5} numberOfLines={1} style={styles.valueTxt}>
              {props.item.ToplamTutar.toFixed(2).replace(",", "").replace(".", ",") +
                " " +
                (props.item.ParaBirimi ? props.item.ParaBirimi : "")}
            </Text>
          </View>
        )}
        {props.item.TeklifIndirimi != null && (
          <View style={styles.row}>
            <Text
              fontScale={0.5}
              numberOfLines={1}
              style={[styles.valueTxt, { color: colors.light_green }]}
            >
              {indirim}
            </Text>
          </View>
        )}
        {props.item.OluşturmaTarihi != null && (
          <View style={styles.row}>
            <Text fontScale={0.5} numberOfLines={1} style={styles.valueTxt}>
              {getDate(props.item.OluşturmaTarihi)}
            </Text>
          </View>
        )}
        {props.item.GecerlilikTarihi != null && (
          <View style={styles.row}>
            <Text fontScale={0.5} numberOfLines={1} style={styles.valueTxt}>
              {getDate(props.item.GecerlilikTarihi)}
            </Text>
          </View>
        )}
        {props.item.Durum != null && (
          <View style={styles.row}>
            <Text
              fontScale={0.5}
              numberOfLines={1}
              style={[
                styles.valueTxt,
                {
                  color:
                    props.item.Durum === "Onaylandı"
                      ? colors.light_green
                      : props.item.Durum === "Onaylanmadı"
                      ? "#d50000"
                      : "#e53935",
                },
              ]}
            >
              {props.item.Durum}
            </Text>
          </View>
        )}
        {props.item.urunkodu != null && (
          <View style={styles.row}>
            <Text fontScale={0.5} numberOfLines={1} style={styles.valueTxt}>
              {props.item.urunkodu}
            </Text>
          </View>
        )}
        {props.item.urunaciklama && (
          <View style={styles.row}>
            <Text fontScale={0.5} numberOfLines={1} style={styles.valueTxt}>
              {props.item.urunaciklama}
            </Text>
          </View>
        )}
        {
          // teklif adı
        }
        {
          // teklifteki ürün sayısı
        }
        {
          // toplam tutar
        }
        {props.item.adet && (
          <View style={styles.row}>
            <Text fontScale={0.5} numberOfLines={1} style={styles.valueTxt}>
              {props.item.adet}
            </Text>
          </View>
        )}
        {props.item.olusturmaTarihi && (
          <View style={styles.row}>
            <Text fontScale={0.5} numberOfLines={1} style={styles.valueTxt}>
              {props.item.olusturmaTarihi}
            </Text>
          </View>
        )}
        {props.item.gecerlilikTarihi && (
          <View style={styles.row}>
            <Text fontScale={0.5} numberOfLines={1} style={styles.valueTxt}>
              {props.item.gecerlilikTarihi}
            </Text>
          </View>
        )}
        {props.item.durumAciklama && (
          <View style={styles.row}>
            <Text fontScale={0.5} numberOfLines={1} style={styles.valueTxt}>
              {props.item.durumAciklama}
            </Text>
          </View>
        )}
        {props.item.Onaylandimi != null && (
          <View style={styles.row}>
            <TouchableOpacity
              disabled={!props.item.Onaylandimi || props.isAddToCartDisabled}
              onPress={() => {
                props.addPriceRequestToCart(props.item.FiyatTalepNo, props.item.ParaBirimi);
              }}
              style={[
                styles.btn,
                (!props.item.Onaylandimi || props.isAddToCartDisabled) && {
                  backgroundColor: "#dedede",
                },
              ]}
            >
              <Text fontScale={0.5} numberOfLines={1} style={styles.btnTxt}>
                SEPETE EKLE
              </Text>
            </TouchableOpacity>
          </View>
        )}
        {props.item.Onaylandimi != null && (
          <View style={[styles.row, { marginTop: 10 }]}>
            <TouchableOpacity
              disabled={!props.item.Onaylandimi}
              onPress={() => props.startFromPriceReq(props.item.FiyatTalepNo)}
              style={[
                styles.btn,
                { backgroundColor: colors.orange },
                !props.item.Onaylandimi && { backgroundColor: "#dedede" },
              ]}
            >
              <Text fontScale={0.5} numberOfLines={1} style={styles.btnTxt}>
                SİPARİŞ VER
              </Text>
            </TouchableOpacity>
          </View>
        )}
      </View>
    </View>
  );
}

const styles = ScaledSheet.create({
  card: {
    backgroundColor: "#fff",
    marginHorizontal: getWidth() * 0.02,
    borderWidth: 1,
    borderRadius: 2,
    borderColor: "#ddd",
    borderBottomWidth: 0,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.3,
    shadowRadius: 1,
    elevation: 1,
    marginTop: 10,
    padding: "10@ms0.5",
    flexDirection: "row",
    width: getWidth() - PADDING * 2,
  },
  titleTxt: {
    fontFamily: "Poppins-SemiBold",
    fontSize: 13,
    color: "orange",
    flex: 1,
  },
  valueTxt: {
    fontFamily: "Poppins",
    fontSize: 13,
    color: "black",
  },
  row: {
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "flex-start",
    height: "28@ms0.5",
  },
  btn: {
    paddingHorizontal: "12@ms0.5",
    height: "28@ms0.4",
    borderRadius: "4@ms0.5",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: colors.light_green,
    alignSelf: "baseline",
  },
  btnTxt: {
    color: "white",
    fontFamily: "Poppins-Bold",
    fontSize: 14,
  },
});
