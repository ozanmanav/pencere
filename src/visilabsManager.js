import { convertToUsd } from "@common/Utils";
import { create_api } from "@relateddigital/visilabs-react-native";
import { Platform } from "react-native";

var api;
var organizationID = "764F416D613137307A6B4D3D";
var siteID = "2B646D544B434D2F48426F3D";
var segmentURL = "http://lgr.visilabs.net";
var dataSource = "3";
var realTimeURL = "http://rt.visilabs.net";
var channel = "mobile";
var euroMsgSubscriptionURL = "https://pushs.euromsg.com/subscription";
var euroMsgRetentionURL = "https://pushr.euromsg.com/retention";
var locale = "tr-TR";

function createApi() {
  const EUROMSG_KEY = Platform.OS === "ios" ? "Pencere_IOS_TEST" : "Pencere_Android";
  api = create_api(
    organizationID,
    siteID,
    segmentURL,
    dataSource,
    realTimeURL,
    channel,
    EUROMSG_KEY,
    euroMsgSubscriptionURL,
    euroMsgRetentionURL,
    locale,
  );
}

function reportCart(sepetKodu, productList) {
  console.log("report cart", sepetKodu, productList);
  try {
    var data = {
      "OM.pbid": sepetKodu,
      "OM.pb": productList.map(item => item.UrunKodu).join(";"),
      "OM.pu": productList.map(item => item.UrunMiktar).join(";"),
      "OM.ppr": productList
        .map(item =>
          String(
            item.UrunMiktar * convertToUsd(item.UrunBirimFiyati, item.UrunBirimFiyatPb),
          ).replace(".", ","),
        )
        .join(";"),
    };
  } catch (error) {
    console.log(error);
  }
  if (data != null) {
    console.log("report cart", data, api);
    api.customEvent("Cart", data);
  }
}

function customEvent(name, data) {
  console.log("custom event", { name, data, api });
  api.customEvent(name, data);
}

function login(user) {
  console.log("login visilabs", user, api);
  api.euromsg.setUser(user);
  api.login(user.keyID);
}

module.exports = {
  createApi,
  reportCart,
  customEvent,
  login,
};
