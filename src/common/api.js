import axios from "axios";
import endpoints from "@common/endpoints";
import { bugsnag } from "@common/bugsnag_config";

export class ApiMode {
  static TEST = "TEST";
  static LIVE = "LIVE";
}

export class Api {
  static createApi(apiMode) {
    this.instance = axios.create({
      baseURL: apiMode === ApiMode.LIVE ? endpoints.API_URL : endpoints.TEST_API_URL,
      timeout: 5000,
      headers: {
        Accept: "application/json; charset=UTF-8",
      },
    });
    this.instance.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
    this.instance.interceptors.response.use(
      function (res) {
        console.log("Response: ", res);
        if (res.headers["content-type"].includes("javascript")) {
          return res;
        } else {
          throw `Content-Type was ${res.headers["content-type"]}. Json is expected.`;
        }
      },
      function (err) {
        // log all failed/errored requests with their config details
        bugsnag.notify(err);
      },
    );

    this.instance.interceptors.request.use(
      function (config) {
        console.log("Request: ", config);
        return config;
      },
      function (error) {
        console.error("Request error", error);
        return Promise.reject(error);
      },
    );
  }
}
