import RNFetchBlob from "rn-fetch-blob";
import { Platform, PermissionsAndroid, Dimensions, Alert } from "react-native";
import store from "@redux/store";
import moment from "pencere/moment";
import NavigationService from "@app/NavigationService";
import * as constants from "./Constants";
import bugsnag from "@common/bugsnag_config";
import { getDetailsFromApi } from "@features/giris/redux/session_actions";
import { getBucket } from "@features/sepet/redux/cart_actions";

export const getWidth = () => Dimensions.get("window").width;
export const getHeight = () => Dimensions.get("window").height;

export function getEndPoint(type, base) {
  if (store.getState().appState.stage === "TEST") {
    if (base === "paynet") return constants.TEST_PAYNET_BASE_URL + constants[type];
    else return constants.TEST_API_URL + constants[type];
  } else {
    if (base === "paynet") return constants.PAYNET_BASE_URL + constants[type];
    else return constants.API_URL + constants[type];
  }
}

export function getStage() {
  return store.getState().appState.stage;
}

export function saveSettings(params, func) {
  return new Promise((resolve, reject) => {
    var prepared_url = "";
    if (func === 1) {
      prepared_url = getEndPoint("ANNOUNCEMENTS_SETTINGS_URL");
    } else {
      prepared_url = getEndPoint("UTIL_URL");
    }
    params.forEach((element, index) => {
      if (index === 0) prepared_url = prepared_url + "?" + element.name + "=" + element.value;
      else {
        prepared_url = prepared_url + "&" + element.name + "=" + element.value;
      }
    });
    fetch(prepared_url)
      .then((res) => {
        resolve(res);
      })
      .catch((err) => {
        bugsnag.notify(err, (report) => {
          report.errorClass = "saveSettings";
        });
        reject(err);
      });
  });
}

export function padDigits(number, digits) {
  return Array(Math.max(digits - String(number).length + 1, 0)).join(0) + number;
}

export function getDate(raw) {
  if (raw.includes("T")) {
    const d = new Date(raw);
    var datestring =
      padDigits(d.getDate(), 2) +
      "." +
      padDigits(d.getMonth() + 1, 2) +
      "." +
      padDigits(d.getFullYear(), 4);
    return datestring;
  }
  var isStart = false;
  var dateStringPre = "";
  for (var j = 0; ; j++) {
    if (raw[j] === "+") {
      break;
    }
    if (isStart) {
      dateStringPre += raw[j];
    }
    if (raw[j] === "(") {
      isStart = true;
    }
  }
  var date = new Date(Number(dateStringPre));
  var dateString = date.getDate() + "." + (date.getMonth() + 1) + "." + date.getFullYear();
  return dateString;
}

export function getLastNMonths(n) {
  var prevDate = moment();
  var datesRaw = [];
  for (let i = 0; i < n; i++) {
    datesRaw.push(moment(prevDate));
    prevDate = prevDate.subtract(1, "month");
  }
  return datesRaw;
}

export function getMonthYearString(d) {
  return constants.months[d.month()] + " " + d.year();
}

export function emptyCart(params) {
  var keys = Object.keys(params);
  var preparedUrl = getEndPoint("CART_URL") + "?";
  keys.forEach((key, index) => {
    preparedUrl += key + "=" + params[key];
    if (index < keys.length - 1) {
      preparedUrl += "&";
    }
  });
  return new Promise((resolve, reject) => {
    fetch(preparedUrl)
      .then((res) => res.json())
      .then(() => {
        store
          .dispatch(getBucket(false))
          .then((payload) => {
            resolve(payload);
          })
          .catch((err) => {
            bugsnag.notify(err, (report) => {
              report.errorClass = "emptyCart";
            });
          });
      })
      .catch((err) => {
        bugsnag.notify(err, (report) => {
          report.errorClass = "emptyCart";
        });
        reject(err);
      });
  });
}

export function requestBayiOl(params) {
  var keys = Object.keys(params);
  var preparedUrl = getEndPoint("BAYI_OL_URL") + "?";
  keys.forEach((key, index) => {
    preparedUrl += key + "=" + encodeURIComponent(params[key]);
    if (index < keys.length - 1) {
      preparedUrl += "&";
    }
  });

  return new Promise((resolve, reject) => {
    fetch(preparedUrl)
      .then((res) => res.json())
      .then((responseJson) => {
        resolve(responseJson);
      })
      .catch((err) => {
        bugsnag.notify(err, (report) => {
          report.errorClass = "requestBayiOl";
        });
        reject(err);
      });
  });
}

function getMimeType(ext) {
  switch (ext) {
    case "pdf":
      return "application/pdf";
    case "csv":
      return Platform.OS === "android" ? "text/comma-separated-values" : "application/csv";
    default:
      return ext;
  }
}

// Return 1 if a > b
// Return -1 if a < b
// Return 0 if a == b
export function compareVersions(a, b) {
  if (a == null || b == null) return 0;
  if (a === b) {
    return 0;
  }

  var a_components = a.split(".");
  var b_components = b.split(".");

  var len = Math.min(a_components.length, b_components.length);

  // loop while the components are equal
  for (var i = 0; i < len; i++) {
    // A bigger than B
    if (parseInt(a_components[i]) > parseInt(b_components[i])) {
      return 1;
    }

    // B bigger than A
    if (parseInt(a_components[i]) < parseInt(b_components[i])) {
      return -1;
    }
  }

  // If one's a prefix of the other, the longer one is greater.
  if (a_components.length > b_components.length) {
    return 1;
  }

  if (a_components.length < b_components.length) {
    return -1;
  }

  // Otherwise they are the same.
  return 0;
}

function showFile(path, mime) {
  return new Promise((resolve, reject) => {
    if (Platform.OS === "android") {
      RNFetchBlob.android
        .actionViewIntent(path, mime)
        .then(() => {
          resolve();
        })
        .catch((err) => {
          console.log(err);
          reject(
            "Dosya açılırken bir hata oluştu. Dosyayı açabilecek bir uygulamaya sahip olduğunuzu kontrol ediniz.",
          );
        });
    } else if (Platform.OS === "ios") {
      RNFetchBlob.ios
        .openDocument(path)
        .then(() => {
          resolve();
        })
        .catch((err) => {
          console.error(err);
          reject(
            "Dosya açılırken bir hata oluştu. Dosyayı açabilecek bir uygulamaya sahip olduğunuzu kontrol ediniz.",
          );
        });
    }
  });
}

function createCsvFromCart(list) {
  let csv = "Ürün Kodu,Ürün Açıklaması,KDV,Adet,Birim Fiyat,Toplam Fiyat,Para Birimi\r\n";
  list.forEach((element) => {
    let row =
      element.UrunKodu +
      "," +
      '"' +
      element.UrunAciklamasi.replace(/"/g, '""') +
      '"' +
      "," +
      element.UrunVergisiText +
      "," +
      element.UrunMiktar +
      "," +
      '"' +
      element.UrunBirimFiyati.toFixed(2).replace(".", ",") +
      '"' +
      "," +
      '"' +
      element.UrunToplamFiyat.toFixed(2).replace(".", ",") +
      '"' +
      "," +
      element.UrunBirimFiyatPb +
      "\r\n";
    csv = csv.concat(row);
  });
  return csv;
}

export async function downlaodCsv(list, filename) {
  if (Platform.OS === "ios") {
    let csv = createCsvFromCart(list);
    const fs = RNFetchBlob.fs;
    await fs.writeFile(RNFetchBlob.fs.dirs.DocumentDir + "/" + filename, csv, "utf8");
    await showFile(RNFetchBlob.fs.dirs.DocumentDir + "/" + filename, getMimeType("csv"));
  } else if (Platform.OS === "android") {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        let csv = createCsvFromCart(list);
        const fs = RNFetchBlob.fs;

        await fs.writeFile(RNFetchBlob.fs.dirs.DownloadDir + "/" + filename, csv, "utf8");
        showFile(RNFetchBlob.fs.dirs.DownloadDir + "/" + filename, getMimeType("csv"));
      }
    } catch (e) {
      console.log(e);
      Alert.alert("Hata", "Beklenmedik bir hata oluştu");
    }
  }
}

export async function downloadPdf(url, filename) {
  console.log("downloading file", url, filename);
  if (Platform.OS === "ios") {
    let dirs = RNFetchBlob.fs.dirs;
    var res = await RNFetchBlob.config({
      // response data will be saved to this path if it has access right.
      path: dirs.DocumentDir + "/" + filename,
      mime: "application/pdf",
    }).fetch("GET", url);
    console.log("The file saved to ", res);
    await showFile(res.data, getMimeType("pdf"));
  } else if (Platform.OS === "android") {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        var resp = await RNFetchBlob.config({
          addAndroidDownloads: {
            useDownloadManager: true, // <-- this is the only thing required
            // Optional, override notification setting (default to true)
            notification: true,
            // Optional, but recommended since android DownloadManager will fail when
            // the url does not contains a file extension, by default the mime type will be text/plain
            mime: getMimeType("pdf"),
            description: "Pencere Sepet",
            path: RNFetchBlob.fs.dirs.DownloadDir + "/" + filename,
          },
        }).fetch("GET", url);
        console.log("The file saved to ", resp.path());
        await showFile(resp.path(), getMimeType("pdf"));
      }
    } catch (error) {
      console.log(error);
      Alert.alert("Hata", error || "Beklenmedik bir hata oluştu");
    }
  }
}
export function handleUrl(url) {
  if (url != null && url !== "") {
    const splittedURL = url.split("/");
    console.log("notif url", splittedURL);
    if (splittedURL[0] === "detay") {
      const uk = splittedURL[1];
      if (uk != null && uk !== "") {
        if (NavigationService.getCurrentRoute().routeName !== "ProductDetailsScreen") {
          NavigationService.navigate("home", "ProductDetailsScreen", {
            UrunKodu: uk,
          });
        } else {
          store.dispatch(getDetailsFromApi(uk));
        }
      }
    } else if (splittedURL[0] === "ara") {
      var params;
      if (splittedURL.length > 0) {
        params = splittedURL[1];
      }
      if (typeof params !== "undefined" && params !== "") {
        var splittedParams = params.split(",");
        var paramsObj = {};
        splittedParams.forEach((param) => {
          const paramParts = param.split(":");
          paramsObj[paramParts[0]] = paramParts[1] ?? "";
        });
        console.log("search params", paramsObj);
        NavigationService.navigate("SearchScreen", "SearchScreen", {
          searchParams: { ...paramsObj },
          focus: false,
          doSearch: true,
        });
      }
    } else if (splittedURL[0] === "fiyatTaleplerim") {
      NavigationService.navigate("PriceRequestsScreen", "PriceRequestsScreen", {
        showAlert: false,
      });
    } else if (splittedURL.length > 1 && splittedURL[2] === "fiyatTaleplerim") {
      NavigationService.navigate("PriceRequestsScreen", "PriceRequestsScreen", {
        showAlert: false,
      });
    }
  }
}

export function numberToLocaleString(val) {
  var formattedString = val.toFixed(2);
  var splittedString = formattedString.split(".");
  if (splittedString[0].length > 3) {
    splittedString[0] = splittedString[0].replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
  }
  formattedString = splittedString.join(",");
  return formattedString;
}

export function handleNotification(notif) {
  console.log("notif opened", notif);
  if (notif != null) {
    if (notif.data != null && notif.data.aps != null) {
      const url = notif?.data?.aps.url;
      if (url != null) handleUrl(url);
      else if (notif.data.url != null) {
        handleUrl(notif.data.url);
      }
    }
  }
}

export function fetchBankAccounts() {
  return new Promise((resolve, reject) => {
    fetch("https://www.pencere.com/mBankaHesapNumaralari.js")
      .then((res) => res.json())
      .then((responseJson) => {
        resolve(responseJson);
      })
      .catch((err) => {
        bugsnag.notify(err, (report) => {
          report.errorClass = "fetchBankAccounts";
        });
        console.log(err);
        reject(err);
      });
  });
}

export function updateDeliveryInfo(si, fn) {
  return new Promise((resolve, reject) => {
    fetch(getEndPoint("ACCOUNTS_URL") + "?si=" + si + "&func=10" + "&fn=" + fn)
      .then((res) => res.json())
      .then((responseJson) => {
        resolve(responseJson);
      })
      .catch((err) => {
        bugsnag.notify(err, (report) => {
          report.errorClass = "updateDeliveryInfo";
        });
        console.log(err);
        reject(err);
      });
  });
}

export function convertToUsd(amount, pb) {
  var usdRate = store.getState().homePage.info.GununKuru.UsdKur;
  var eurRate = store.getState().homePage.info.GununKuru.EuroKur;
  if (pb === "TRY" || pb === "TL") {
    return amount / usdRate;
  } else if (pb === "EUR") {
    return amount * (eurRate / usdRate);
  } else return amount;
}
