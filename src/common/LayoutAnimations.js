import { LayoutAnimation } from "react-native";
export const CREATE_SPRINGY = {
  duration: 400,
  create: {
    type: LayoutAnimation.Types.spring,
    property: LayoutAnimation.Properties.scaleY,
    springDamping: 0.8,
  },
};
export const CREATE_UPDATE_SPRINGY = {
  create: {
    duration: 400,
    type: LayoutAnimation.Types.spring,
    property: LayoutAnimation.Properties.scaleY,
    springDamping: 0.8,
  },
  update: {
    duration: 400,
    type: LayoutAnimation.Types.spring,
    property: LayoutAnimation.Properties.scaleY,
    springDamping: 0.8,
  },
};
export const CREATE_UPDATE_DELETE_SPRINGY = {
  create: {
    duration: 200,
    type: LayoutAnimation.Types.spring,
    property: LayoutAnimation.Properties.scaleY,
    springDamping: 0.8,
  },
  update: {
    duration: 400,
    type: LayoutAnimation.Types.spring,
    property: LayoutAnimation.Properties.scaleY,
    springDamping: 0.8,
  },
  delete: {
    duration: 400,
    type: LayoutAnimation.Types.spring,
    property: LayoutAnimation.Properties.scaleY,
    springDamping: 0.8,
  },
};
export const CREATE_DELETE_SPRINGY = {
  duration: 400,
  create: {
    type: LayoutAnimation.Types.spring,
    property: LayoutAnimation.Properties.scaleY,
    springDamping: 0.8,
  },
  delete: {
    type: LayoutAnimation.Types.spring,
    property: LayoutAnimation.Properties.scaleY,
    springDamping: 0.8,
  },
};
export const CREATE_SPRINGXY = {
  duration: 400,
  create: {
    type: LayoutAnimation.Types.spring,
    property: LayoutAnimation.Properties.scaleXY,
    springDamping: 0.8,
  },
};
export const DELETE_SPRINGY = {
  duration: 400,
  delete: {
    type: LayoutAnimation.Types.spring,
    property: LayoutAnimation.Properties.scaleY,
    springDamping: 0.8,
  },
};
export const DELETE_SPRINGXY = {
  duration: 400,
  delete: {
    type: LayoutAnimation.Types.spring,
    property: LayoutAnimation.Properties.scaleXY,
    springDamping: 0.8,
  },
};
export const UPDATE_SPRINGX = {
  duration: 400,
  update: {
    type: LayoutAnimation.Types.spring,
    property: LayoutAnimation.Properties.scaleX,
    springDamping: 0.8,
  },
};
export const UPDATE_SPRINGY = {
  duration: 400,
  update: {
    type: LayoutAnimation.Types.spring,
    property: LayoutAnimation.Properties.scaleY,
    springDamping: 0.8,
  },
};
export const UPDATE_LINEARY = {
  update: {
    type: LayoutAnimation.Types.easeInEaseOut,
    property: LayoutAnimation.Properties.scaleY,
    springDamping: 0.8,
  },
};
export const UPDATE_SPRINGXY = {
  duration: 400,
  update: {
    type: LayoutAnimation.Types.spring,
    property: LayoutAnimation.Properties.scaleXY,
    springDamping: 0.9,
  },
};
