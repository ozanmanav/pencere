import { Dimensions, Platform, PixelRatio } from "react-native";

const { width, height } = Dimensions.get("window");

const isIphoneX =
  Platform.OS === "ios" && !Platform.isPad && !Platform.isTVOS && (height === 812 || width === 812);

const isTablet = () => {
  let pixelDensity = PixelRatio.get();
  const adjustedWidth = width * pixelDensity;
  const adjustedHeight = height * pixelDensity;
  if (pixelDensity < 2 && (adjustedWidth >= 1000 || adjustedHeight >= 1000)) {
    return true;
  } else return pixelDensity === 2 && (adjustedWidth >= 1920 || adjustedHeight >= 1920);
};

export default {
  isTablet,
  isIphoneX,
  ToolbarHeight: isIphoneX ? 35 : 0,
};
