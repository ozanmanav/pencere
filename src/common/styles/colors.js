const colors = {
  txt_main: "#0765d7",
  txt_description: "#777777",
  txt_description_light: "#fff",
  txt_dark: "#3c3c3c",
  txt_black: "#666a6c",
  txt1: "white",

  bg_main: "#0765d7",
  bg_header: "#455a64",
  bg1: "white",

  bd_main: "#0765d7",
  bd1: "white",
  bd_line: "#dddddd",
  bd_input: "#cbcbcb",

  white: "#FDFDFD",
  tuna: "#4B4E55",
  malibu: "#4FB2E2",
  light_gray: "#D1CECE",
  dark_gray: "#A5A3A3",
  light_black: "#212121",
  light_blue: "#03A9F4",

  ebb: "#E3DAD8",

  primary_dark: "#d0391c",
  primary_light: "#d14c40",
  price_color: "#d55749",

  green: "#59ad3a",
  red: "#f44336",
  light_green: "#6baf4e",
  bg_gray: "#efefef",

  orange: "#fc7c05",
};

export default colors;
