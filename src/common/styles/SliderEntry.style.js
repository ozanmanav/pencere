import { StyleSheet, Dimensions, Platform } from "react-native";
import { colors } from "./index.style";
const win = Dimensions.get("window");

const IS_IOS = Platform.OS === "ios";
const { width: viewportWidth } = Dimensions.get("window");

const slideHeight = (win.width * 9) / 16;
const slideWidth = win.width;
const itemHorizontalMargin = 0;

export const sliderWidth = viewportWidth;
export const itemWidth = slideWidth + itemHorizontalMargin;

const entryBorderRadius = 0;

export default StyleSheet.create({
  slideInnerContainer: {
    width: win.width,
    height: win.width * 0.625,
    alignItems: "center",
    justifyContent: "center",
  },
  autoHeightContainer: {
    width: itemWidth,
  },
  shadow: {
    position: "absolute",
    top: 0,
    left: itemHorizontalMargin,
    right: itemHorizontalMargin,
    bottom: 18,
    shadowColor: colors.gray,
    shadowOpacity: 0.25,
    shadowOffset: { width: 0, height: 10 },
    shadowRadius: 10,
    borderRadius: entryBorderRadius,
  },
  imageContainer: {
    flex: 1,
    marginBottom: IS_IOS ? 0 : -1, // Prevent a random Android rendering issue
    backgroundColor: "transparent",
    borderTopLeftRadius: entryBorderRadius,
    borderTopRightRadius: entryBorderRadius,
  },
  titleContainer: {
    backgroundColor: "rgba(50,50,50,0.8)",
    position: "absolute",
    padding: 10,
    width: itemWidth,
    marginTop: slideHeight - ((win.width * 9) / 16 + 160) / 5,
    height: ((win.width * 9) / 16 + 160) / 5,
    alignItems: "center",
    justifyContent: "center",
  },
  imageContainerEven: {
    backgroundColor: colors.black,
  },
  image: {
    width: win.width,
    height: win.width * 0.625,
    resizeMode: "cover",
  },
  // image's border radius is buggy on iOS; let's hack it!
  radiusMask: {
    position: "absolute",
    bottom: 0,
    left: 0,
    right: 0,
    height: entryBorderRadius,
    backgroundColor: "white",
  },
  radiusMaskEven: {
    backgroundColor: colors.black,
  },
  textContainer: {
    justifyContent: "center",
    paddingTop: 20 - entryBorderRadius,
    paddingBottom: 20,
    paddingHorizontal: 16,
    backgroundColor: "white",
    borderBottomLeftRadius: entryBorderRadius,
    borderBottomRightRadius: entryBorderRadius,
  },
  sliderTitle: {
    textAlign: "center",
    color: "white",
    fontSize: 20,
    fontFamily: Platform.OS === "ios" ? "Ubuntu-Bold" : "Ubuntu-Bold",
    textShadowColor: "gray",
    textShadowRadius: 6,
    textShadowOffset: {
      width: 1,
      height: 1,
    },
  },
  textContainerEven: {
    backgroundColor: colors.black,
  },
  title: {
    color: colors.black,
    fontSize: 13,
    fontWeight: "bold",
    letterSpacing: 0.5,
  },
  titleEven: {
    color: "white",
  },
  subtitle: {
    marginTop: 6,
    color: colors.gray,
    fontSize: 12,
    fontStyle: "italic",
  },
  subtitleEven: {
    color: "rgba(255, 255, 255, 0.7)",
  },
});
