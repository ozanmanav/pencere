export const CITIES = [
  "Adana",
  "Adıyaman",
  "Afyonkarahisar",
  "Ağrı",
  "Aksaray",
  "Amasya",
  "Ankara",
  "Antalya",
  "Ardahan",
  "Artvin",
  "Aydın",
  "Balıkesir",
  "Bartın",
  "Batman",
  "Bayburt",
  "Bilecik",
  "Bingöl",
  "Bitlis",
  "Bolu",
  "Burdur",
  "Bursa",
  "Çanakkale",
  "Çankırı",
  "Çorum",
  "Denizli",
  "Diyarbakır",
  "Düzce",
  "Edirne",
  "Elazığ",
  "Erzincan",
  "Erzurum",
  "Eskişehir",
  "Gaziantep",
  "Giresun",
  "Gümüşhane",
  "Hakkari",
  "Hatay",
  "Iğdır",
  "Isparta",
  "İstanbul",
  "İzmir",
  "Kahramanmaraş",
  "Karabük",
  "Karaman",
  "Kars",
  "Kastamonu",
  "Kayseri",
  "Kilis",
  "Kırıkkale",
  "Kırklareli",
  "Kırşehir",
  "Kocaeli",
  "Konya",
  "Kütahya",
  "Malatya",
  "Manisa",
  "Mardin",
  "Mersin",
  "Muğla",
  "Muş",
  "Nevşehir",
  "Niğde",
  "Ordu",
  "Osmaniye",
  "Rize",
  "Sakarya",
  "Samsun",
  "Şanlıurfa",
  "Siirt",
  "Sinop",
  "Şırnak",
  "Sivas",
  "Tekirdağ",
  "Tokat",
  "Trabzon",
  "Tunceli",
  "Uşak",
  "Van",
  "Yalova",
  "Yozgat",
  "Zonguldak",
];

export const months = [
  "Ocak",
  "Şubat",
  "Mart",
  "Nisan",
  "Mayıs",
  "Haziran",
  "Temmuz",
  "Ağustos",
  "Eylül",
  "Ekim",
  "Kasım",
  "Aralık",
];

export const STAGE = "PROD"; // 'TEST' or 'PROD';
export const API_URL = "https://www.pencere.com";
export const TEST_API_URL = "https://test3.pencere.com";
export const PENCERE_URL = STAGE === "TEST" ? TEST_API_URL : API_URL;
export const LOGIN_URL = "/mLogin.js";
export const SEARCH_URL = "/mUrunArama.js";
export const PRODUCT_DETAILS_URL = "/mUrunDetay.js";
export const CATEGORIES_URL = "/mHiyerarsiler.js";
export const UPLOADS_URL = API_URL + "/uploads/mobile/";
export const CART_URL = "/mSepet.js";
export const SEARCH_ORDERS_URL = "/mSiparisAra.js";
export const ORDER_DETAILS_URL = "/mSiparisDetay.js";
export const ACCOUNTS_URL = "/mCariHesap.js";
export const HOMEPAGE_URL = "/mAnaSayfa.js";
export const AUTOCOMPLETE_URL = "/mAutoComplete.js";
export const ORDER_URL = "/mSiparisVer.js";
export const ORDER_RESULT_URL = "/mSiparisSonuc.js";
export const UTIL_URL = "/mUtil.js";
export const FORGOT_PASSWORD_URL = "/mSifre.js";
export const CONTACTS_URL = "/mIletisimBilgileri.js";
export const FAVORITES_URL = "/mTakipListesi.js";
export const PAYNET_CAMPAIGNS_URL = "/mKrediKartiKampanyalar.js";
export const BAYI_OL_URL = "/mBayiBasvuru.js";
export const ANNOUNCEMENTS_URL = "/mBildirim.js";
export const ANNOUNCEMENTS_SETTINGS_URL = "/mDuyuruTercihleri.js";
export const PRICE_REQUISITIONS_URL = "/mFiyatTaleplerim.js";
export const PAYNET_RATIOS_URL = "/mPaynetOranlar.js";
export const GARANTI_URL = "/mGarantiSuresiSorgulama.js";
export const KUPON_URL = "/mKuponlarim.js";

export const TEST_MOBILE_CONFIG_URL = "http://test3.pencere.com/uploads/mobile_config.txt";
export const MOBILE_CONFIG_URL = "https://www.pencere.com/uploads/mobile_config.txt";

export const TEST_PAYNET_BASE_URL = "https://pts-api.paynet.com.tr";
export const PAYNET_BASE_URL = "https://api.paynet.com.tr";
export const PAYNET_URL = STAGE === "TEST" ? TEST_PAYNET_BASE_URL : PAYNET_BASE_URL;

export const PAYNET_PUBLIC_KEY = "pbk_pcs_veiDeHGz0kjJDdlSxoO5S6q0MJNvDevv";

export const PAYNET_AUTH_URL = "/v1/paynetj/auth";
export const PAYNET_CREATE_TOKEN_URL = "/v1/paynetj/create_token";
export const PAYNET_CARD_TOKEN_URL = "/v1/paynetj/create_card_token";
export const PAYNET_GET_RATIOS_URL = "/v1/ratio/Get_public";

export const GET_3DSECURE_URL = "/v1/paynetjgate/tds";
export const SAVE_CARD_UPDATE_URL = "/v1/paynetj/auth_save_card_update";

export const EXPORT_CSV_URL = "/ExportToCsv.aspx";
export const EXPORT_PDF_URL = "/ExportToPdf.aspx";

export const RATIO_CODE = "YJ2T3VKJ";
export const RATIO_CODE_TEST = "ADY0Y1AL";

export const DEPARTMENTS = [
  { title: "Çalıştığınız Departman", Value: "" },
  { title: "Satınalma", Value: "0002" },
  { title: "Satış", Value: "0003" },
  { title: "Yönetim", Value: "0005" },
];

export const ROLES = [
  { title: "Göreviniz", Value: "" },
  { title: "Genel Müdür", Value: "01" },
  { title: "Pazarlama Müdürü", Value: "02" },
  { title: "Satış Müdürü", Value: "03" },
  { title: "Satınalma Müdürü", Value: "04" },
  { title: "Ürün Sorumlusu", Value: "07" },
  { title: "Satış Personeli", Value: "11" },
];
