import { Platform } from "react-native";
import { Client } from "bugsnag-react-native";

const bugsnag = configureBugsnag();

export function configureBugsnag() {
  var client = new Client(
    Platform.OS === "android"
      ? "3964d4a0e3c234a11a7950ea6302ba9d"
      : "272ba83748366a7a72f25f69a2fad7ea",
  );
  return client;
}

export default bugsnag;
