import React, { Component } from "react";
import { connect } from "react-redux";
import {
  logout,
  fetchCategoriesFromApi,
  getSessionFromStorage,
  setLiveChatVisibility,
  login,
  getMobileConfig,
  setAppState,
} from "@redux/actions";
import {
  Dimensions,
  View,
  StatusBar,
  Linking,
  Platform,
  AppState,
  TouchableOpacity,
} from "react-native";
import { PulseIndicator } from "react-native-indicators";
import colors from "@common/styles/colors";
import bugsnag from "@common/bugsnag_config";
import VersionNumber from "react-native-version-number";
import { compareVersions } from "@common/Utils";
import Modal from "react-native-modal";
import { moderateScale } from "react-native-size-matters";
import { Text } from "@components";
var visilabsManager = require("@app/visilabsManager");

const dim = Dimensions.get("window");

class RouterScreen extends Component {
  constructor(props) {
    super(props);
    this.checkLoggedIn.bind(this);
    this.state = {
      UrunKodu: null,
      appState: AppState.currentState,
      alert: false,
    };
  }

  _handleAppStateChange = (nextAppState) => {
    if (this.state.appState.match(/inactive|background/) && nextAppState === "active") {
      this.checkLoggedIn();
    }
    this.setState({ appState: nextAppState });
  };

  componentWillUnmount() {
    AppState.removeEventListener("change", this._handleAppStateChange);
  }

  componentDidMount() {
    AppState.addEventListener("change", this._handleAppStateChange);
    this.props
      .getSessionFromStorage()
      .then(() => {
        this.checkLoggedIn();
      })
      .catch(() => {
        this.props.navigation.navigate("Auth");
      });
    StatusBar.setHidden(true);
  }

  goStore = () => {
    if (Platform.OS === "android") {
      this.setState({ alert: false });
      Linking.openURL("market://details?id=" + VersionNumber.bundleIdentifier).catch((err) => {
        bugsnag.notify(err);
      });
    } else {
      this.setState({ alert: false });
      Linking.openURL("itms-apps://itunes.apple.com/app/twitter/id751936010?mt=8").catch(() => {
        this.tryLogin();
      });
    }
  };

  checkLoggedIn() {
    this.props
      .getMobileConfig()
      .then((res) => {
        console.log(
          "compareVersions",
          compareVersions(VersionNumber.appVersion, res.iosVersion),
          VersionNumber.appVersion,
          res.iosVersion,
        );
        if (Platform.OS === "android") {
          if (res.androidVersion != null) {
            if (compareVersions(VersionNumber.appVersion, res.androidVersion) === -1) {
              if (!this.state.alert) {
                this.setState({ alert: true });
              }
            } else {
              this.tryLogin();
            }
          } else {
            this.tryLogin();
          }
        } else if (Platform.OS === "ios") {
          if (res.iosVersion != null) {
            if (compareVersions(VersionNumber.appVersion, res.iosVersion) === -1) {
              if (!this.state.alert) {
                this.setState({ alert: true });
              }
            } else {
              this.tryLogin();
            }
          } else {
            this.tryLogin();
          }
        }
      })
      .catch((err) => {
        bugsnag.notify(err, (report) => {
          report.errorClass = "getMobileConfig";
        });
        console.log("config", err);
        this.tryLogin();
      });
  }

  tryLogin = () => {
    this.setState({ alert: false });
    if (
      this.props?.session?.info?.KullaniciKodu &&
      this.props.session.info.KullaniciKodu.startsWith("test3@")
    ) {
      this.props.setAppState({ stage: "TEST" });
    } else {
      this.props.setAppState({ stage: "PROD" });
    }
    if (
      this.props.session.info.BayiKodu != null &&
      this.props.session.info.KullaniciKodu != null &&
      this.props.session.info.Sifre != null
    ) {
      this.props
        .login(
          this.props.session.info.BayiKodu,
          this.props.session.info.KullaniciKodu.replace("test3@", ""),
          this.props.session.info.Sifre,
        )
        .then(() => {
          bugsnag.setUser(
            this.props.session.info.BayiKodu,
            this.props.session.info.Ad + " " + this.props.session.info.Soyad,
            this.props.session.info.Email,
          );
          bugsnag.notifyReleaseStages = ["production", "staging"];
          this.props.navigation.navigate("App");
          var user = {
            keyID: this.props.session.info.BayiKodu + "_" + this.props.session.info.KullaniciKodu,
            email: this.props.session.info.Email,
          };
          visilabsManager.login(user);
        })
        .catch((err) => {
          console.log("login err", err);
          this.props.navigation.navigate("Auth");
        });
    } else {
      this.props.navigation.navigate("Auth");
    }
  };

  render() {
    return (
      <View
        style={{
          width: dim.width,
          height: dim.height,
          alignItems: "center",
          justifyContent: "center",
          backgroundColor: "white",
        }}
      >
        <StatusBar hidden={true} backgroundColor="transparent" translucent={true} />
        <View
          style={{
            flexDirection: "row",
            width: 200,
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <View style={{ alignItems: "center", justifyContent: "center" }}>
            <View
              style={{
                width: 50,
                height: 50,
                borderRadius: 48 / 2,
                backgroundColor: colors.price_color,
              }}
            />
            <View
              style={{
                width: 40,
                height: 40,
                borderRadius: 40 / 2,
                backgroundColor: "white",
                position: "absolute",
              }}
            />
            <PulseIndicator size={24} style={{ position: "absolute" }} color={colors.light_black} />
            <View
              style={{
                width: 10,
                height: 10,
                position: "absolute",
                backgroundColor: "white",
                top: 19,
                right: 0,
              }}
            />
          </View>
        </View>
        <Modal
          isVisible={this.state.alert}
          onRequestDismiss={() => this.setState({ alert: false })}
        >
          <View
            style={{
              backgroundColor: "white",
              flexDirection: "column",
              borderRadius: 4,
            }}
          >
            <View
              style={{
                flexDirection: "row",
                padding: moderateScale(30, 0.5),
                flexGrow: 1,
              }}
            >
              <Text fontScale={0.6} style={{ fontFamily: "Poppins", color: "gray" }}>
                Uygulamanın yeni bir sürümü mevcut. Devam etmek için lütfen uygulamayı
                güncelleyiniz.
              </Text>
            </View>
            <TouchableOpacity
              style={{
                flexDirection: "row",
                justifyContent: "center",
                alignItems: "center",
                padding: moderateScale(15, 0.5),
                flexGrow: 1,
                borderTopColor: "#cecece",
                borderTopWidth: 1,
              }}
              onPress={this.goStore}
            >
              <Text
                fontScale={0.6}
                style={{
                  fontSize: 14,
                  fontFamily: "Poppins-Medium",
                  color: colors.primary_dark,
                }}
              >
                Güncelle
              </Text>
            </TouchableOpacity>
          </View>
        </Modal>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    session: state.session,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    login: (customerID, uname, pass) => dispatch(login(customerID, uname, pass)),
    logout: () => dispatch(logout()),
    getCategories: () => dispatch(fetchCategoriesFromApi()),
    getSessionFromStorage: () => dispatch(getSessionFromStorage()),
    setLiveChatVisibility: (isVisible) => dispatch(setLiveChatVisibility(isVisible)),
    getMobileConfig: () => dispatch(getMobileConfig()),
    setAppState: (newParams) => dispatch(setAppState(newParams)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(RouterScreen);
