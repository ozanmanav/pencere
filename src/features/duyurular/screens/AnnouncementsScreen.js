import React, { Component } from "react";
import {
  View,
  SafeAreaView,
  FlatList,
  Dimensions,
  TouchableOpacity,
  Platform,
  Animated,
  StatusBar,
} from "react-native";
import { AnimatedHeader, Text, PencereIndicator } from "@components";
import colors from "@common/styles/colors";
import { connect } from "react-redux";
import { fetchAnnouncementsFromApi, sendReadInfo } from "@redux/actions";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import { scale, moderateScale } from "react-native-size-matters";

const win = Dimensions.get("window");

const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);

class AnnouncementsScreen extends Component {
  constructor(props) {
    super(props);
    const scrollAnim = new Animated.Value(0);
    this.state = {
      scrollAnim,
      clampedScroll: Animated.diffClamp(
        Animated.add(
          Animated.add(scrollAnim, AnimatedHeader.height).interpolate({
            inputRange: [0, 1],
            outputRange: [0, 1],
            extrapolateLeft: "clamp",
          }),
          -AnimatedHeader.height,
        ),
        0,
        AnimatedHeader.height,
      ),
    };
  }

  renderItem = ({ item }) => {
    return (
      <TouchableOpacity
        onPress={() => {
          this.props.sendReadInfo(item.BildirimId);
          if (
            item.Baslik.toLowerCase().includes("fiyat talebi") ||
            item.Baslik.toLowerCase().includes("fiyat teklifi")
          )
            this.props.navigation.navigate({
              key: "PriceRequestsScreen",
              routeName: "PriceRequestsScreen",
              params: { showAlert: false },
            });
          else
            this.props.navigation.navigate({
              key: "stack",
              routeName: "AnnouncementDetailsScreen",
              params: { item },
            });
        }}
        style={{
          backgroundColor: "white",
          width: win.width - scale(15) * 2,
          marginBottom: 15,
        }}
      >
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
            padding: 10,
          }}
        >
          <Text
            fontScale={0.6}
            style={{
              fontFamily: "Poppins-SemiBold",
              color: "white",
              fontSize: 10,
              backgroundColor: this.getColor(item.type),
            }}
          >
            {item.BildirimTipiId !== "Varsayılan"}
          </Text>
          {item.BasariliGonderimTarihi && (
            <Text fontScale={0.6} style={{ color: "#989898" }}>
              {item.BasariliGonderimTarihi}
            </Text>
          )}
        </View>
        <View style={{ paddingHorizontal: 20, paddingVertical: 5 }}>
          <Text
            fontScale={0.6}
            style={{
              fontFamily: "Poppins-SemiBold",
              fontSize: 14,
              marginBottom: 3,
            }}
          >
            {item.Baslik}
          </Text>
          <Text
            fontScale={0.6}
            numberOfLines={2}
            ellipsizeMode="tail"
            style={{ fontFamily: "Poppins", fontSize: 14 }}
          >
            {item.Icerik}
          </Text>
        </View>
        <View
          style={{
            flex: 1,
            flexDirection: "row",
            justifyContent: "flex-end",
            alignItems: "center",
          }}
        >
          {item.KullaniciOkumaTarihi ? (
            <Text
              fontScale={0.6}
              style={{
                fontFamily: "Poppins-Medium",
                fontSize: 10,
                color: "white",
                backgroundColor: item.KullaniciOkumaTarihi ? "#8BC34A" : "#f44336",
                paddingHorizontal: 5,
                paddingVertical: 2,
              }}
            >
              Okundu
            </Text>
          ) : (
            <Text
              fontScale={0.6}
              style={{
                fontFamily: "Poppins-Medium",
                fontSize: 10,
                color: "white",
                backgroundColor: item.KullaniciOkumaTarihi ? "#8BC34A" : "#f44336",
                paddingHorizontal: 5,
                paddingVertical: 2,
              }}
            >
              Okunmadı
            </Text>
          )}
        </View>
      </TouchableOpacity>
    );
  };

  getColor(type) {
    switch (type) {
      case "DUYURU":
        return "#b9b9b9";
      case "KAMPANYA":
        return "#db2e2e";
      case "FIRSAT":
        return "#86c71e";
      case "MESAJ":
        return "#4a9fee";
      case "SİZE ÖZEL":
        return "#ffc20e";
    }
  }

  renderNoContent = (section) => {
    if (section.data.length === 0) {
      return (
        <View style={{ width: win.width }}>
          <Text
            fontScale={0.6}
            style={{
              fontSize: 16,
              color: "gray",
              textAlign: "center",
              marginVertical: 10,
              backgroundColor: "transparent",
            }}
          >
            Görüntülenecek duyuru bulunamadı
          </Text>
        </View>
      );
    }
    return null;
  };

  renderHeader = () =>
    this.props.announcements.unreadCount > 0 && (
      <View
        style={{
          width: win.width,
          alignItems: "center",
          height: 50,
          flexDirection: "row",
          justifyContent: "space-between",
          paddingHorizontal: win.width * 0.05,
        }}
      >
        <Text>
          <Text
            fontScale={0.6}
            style={{
              fontFamily: "Poppins-Medium",
              fontSize: 14,
              includeFontPadding: false,
            }}
          >
            Okunmayan bildirim sayısı:{" "}
          </Text>
          <Text
            fontScale={0.6}
            style={{
              fontFamily: "Poppins-Medium",
              fontSize: 14,
              includeFontPadding: false,
            }}
          >
            {this.props.announcements.unreadCount}
          </Text>
        </Text>
        <TouchableOpacity
          style={{
            flexDirection: "column",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <Text fontScale={0.6} style={{ fontSize: 10, color: "gray" }}>
            Tümünü Okundu
          </Text>
          <Text fontScale={0.6} style={{ fontSize: 10, color: "gray" }}>
            Olarak İşaretle
          </Text>
        </TouchableOpacity>
      </View>
    );

  render() {
    return (
      <SafeAreaView
        style={{ backgroundColor: colors.primary_dark, flex: 1 }}
        forceInset={{ top: "always" }}
      >
        <View style={{ flexGrow: 1, width: win.width, backgroundColor: "#efefef" }}>
          {this.props.announcements.error && (
            <View
              style={{
                flexDirection: "column",
                width: win.width,
                marginTop: moderateScale(100, 0.4),
                height: moderateScale(100, 0.4),
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <Text fontScale={0.6} style={{ fontFamily: "Poppins", fontSize: 14 }}>
                Bildirimler yüklenirken bir problem oluştu.
              </Text>
              <TouchableOpacity
                onPress={() => this.props.getAnnouncements()}
                style={{ flexDirection: "row", alignItems: "center" }}
              >
                <MaterialIcons size={14} name="refresh" color="blue" />
                <Text
                  fontScale={0.6}
                  style={{ fontFamily: "Poppins", fontSize: 14, color: "blue" }}
                >
                  Yeniden Yükle
                </Text>
              </TouchableOpacity>
            </View>
          )}

          <AnimatedFlatList
            data={this.props.announcements.items}
            renderItem={this.renderItem}
            contentOffset={{ y: -AnimatedHeader.height }}
            contentInset={{
              top: AnimatedHeader.height,
              left: 0,
              bottom: 0,
              right: 0,
            }}
            keyExtractor={(item) => item.BildirimId}
            overScrollMode="never"
            //ListHeaderComponent={this.renderHeader}
            ListEmptyComponent={() => (
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <Text fontScale={0.6} style={{ fontFamily: "Poppins", color: "gray" }}>
                  Görünütülenecek bildirim yok
                </Text>
              </View>
            )}
            contentContainerStyle={{
              flexGrow: this.props.announcements.items.length > 0 ? 1 : 0,
              alignItems: "center",
              paddingBottom: 0,
              paddingTop:
                Platform.OS === "android"
                  ? AnimatedHeader.height +
                    (Platform.Version < 20 ? 0 : StatusBar.currentHeight) +
                    scale(15)
                  : scale(15),
            }}
            onScroll={Animated.event(
              [{ nativeEvent: { contentOffset: { y: this.state.scrollAnim } } }],
              {
                useNativeDriver: true,
              },
            )}
            scrollEventThrottle={16}
          />
          {this.props.announcements.isFetching && (
            <PencereIndicator
              size="large"
              color="#0000ff"
              style={{
                position: "absolute",
                top: win.height / 2 - moderateScale(45, 0.5),
                left: win.width / 2 - moderateScale(60, 0.5),
              }}
            />
          )}
          <AnimatedHeader
            navigation={this.props.navigation}
            clampedScroll={this.state.clampedScroll}
            home={true}
            title="Bildirimler"
            searchBar={true}
            search={false}
          />
        </View>
      </SafeAreaView>
    );
  }
}

function mapStateToProps(state) {
  return {
    announcements: state.announcements,
    session: state.session,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    getAnnouncements: () => dispatch(fetchAnnouncementsFromApi()),
    sendReadInfo: (b) => dispatch(sendReadInfo(b)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(AnnouncementsScreen);
