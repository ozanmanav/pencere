import React, { Component } from "react";
import { SafeAreaView, View, Dimensions, Platform, StatusBar } from "react-native";
import { SimpleHeader, Text } from "@components";
import colors from "@common/styles/colors";
import { connect } from "react-redux";
import { sendReadInfo } from "@redux/actions";
import { moderateScale } from "react-native-size-matters";

const win = Dimensions.get("window");

class AnnouncementDetailsScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      webViewVisible: false,
    };
  }

  render() {
    return (
      <SafeAreaView
        style={{ backgroundColor: colors.primary_dark, flex: 1 }}
        forceInset={{ top: "always" }}
      >
        <View
          style={{
            flexGrow: 1,
            width: win.width,
            backgroundColor: "white",
            alignItems: "center",
            paddingTop:
              moderateScale(50, 0.4) +
              (Platform.OS === "android"
                ? Platform.Version < 20
                  ? 0
                  : StatusBar.currentHeight
                : 0),
          }}
        >
          <SimpleHeader
            style={{
              backgroundColor: colors.primary_dark,
              borderColor: colors.primary_dark,
            }}
            titleStyle={{ color: "white" }}
            iconColor="white"
            navigation={this.props.navigation}
            title="Duyuru Detayı"
            onBackPress={() =>
              this.props.navigation.state?.params.origin
                ? this.props.navigation.navigate(this.props.navigation.state.params.origin)
                : this.props.navigation.goBack()
            }
          />
          <View
            style={{
              flexGrow: 1,
              flexDirection: "column",
              paddingVertical: 10,
            }}
          >
            <View
              style={{
                paddingHorizontal: 10,
                paddingVertical: 5,
                width: win.width,
              }}
            >
              {this.props.navigation.state.params.item.BasariliGonderimTarihi && (
                <Text>
                  <Text
                    style={{
                      fontFamily: "Poppins",
                      fontSize: 14,
                      marginBottom: 3,
                    }}
                  >
                    {"Gönderim Tarihi: "}
                  </Text>
                  <Text
                    style={{
                      fontFamily: "Poppins",
                      fontSize: 14,
                      marginBottom: 3,
                      color: "gray",
                    }}
                  >
                    {this.props.navigation.state.params.item.BasariliGonderimTarihi}
                  </Text>
                </Text>
              )}
              <Text>
                <Text
                  style={{
                    fontFamily: "Poppins",
                    fontSize: 14,
                    marginBottom: 3,
                  }}
                >
                  {"Başlık: "}
                </Text>
                <Text
                  style={{
                    fontFamily: "Poppins",
                    fontSize: 14,
                    marginBottom: 3,
                    color: "gray",
                  }}
                >
                  {this.props.navigation.state.params.item.Baslik}
                </Text>
              </Text>
              <Text>
                <Text
                  style={{
                    fontFamily: "Poppins",
                    fontSize: 14,
                    marginBottom: 3,
                  }}
                >
                  {"Mesaj: "}
                </Text>
                <Text style={{ fontFamily: "Poppins", fontSize: 14, color: "gray" }}>
                  {this.props.navigation.state.params.item.Icerik}
                </Text>
              </Text>
            </View>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    sendReadInfo: (b) => dispatch(sendReadInfo(b)),
  };
}

export default connect(null, mapDispatchToProps)(AnnouncementDetailsScreen);
