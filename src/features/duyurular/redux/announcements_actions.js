import {
  GET_ANNOUNCEMENTS,
  GET_ANNOUNCEMENTS_SUCCESS,
  GET_ANNOUNCEMENTS_FAILURE,
} from "@redux/actions/types";
import bugsnag from "@common/bugsnag_config";
import { getEndPoint } from "@common/Utils";
import Euromsg from "react-native-euromsg";
import { Platform } from "react-native";

export function fetchAnnouncementsFromApi() {
  return (dispatch, getState) => {
    dispatch({ type: GET_ANNOUNCEMENTS });
    fetch(getEndPoint("ANNOUNCEMENTS_URL") + "?si=" + getState().session.info.SessionID + "&func=1")
      .then((res) => {
        return res.json();
      })
      .then((responseJson) => {
        if (responseJson) {
          dispatch(getAnnouncementsSuccess(responseJson));
          if (Platform.OS === "ios") Euromsg.setBadgeCount(responseJson.OkunmamisBildirimSayisi);
        } else {
          dispatch(getAnnouncementsFailure());
        }
      })
      .catch((error) => {
        bugsnag.notify(error, (report) => {
          report.errorClass = "fetchAnnouncementsFromApi";
        });
        console.error(error);
        dispatch(getAnnouncementsFailure());
      });
  };
}

export function sendReadInfo(b) {
  return (dispatch, getState) => {
    fetch(
      getEndPoint("ANNOUNCEMENTS_URL") +
        "?si=" +
        getState().session.info.SessionID +
        "&func=2" +
        "&b=" +
        b,
    ).then(() => {
      dispatch(fetchAnnouncementsFromApi());
    });
  };
}

function getAnnouncementsSuccess(data) {
  return {
    type: GET_ANNOUNCEMENTS_SUCCESS,
    data,
  };
}

function getAnnouncementsFailure() {
  return {
    type: GET_ANNOUNCEMENTS_FAILURE,
  };
}
