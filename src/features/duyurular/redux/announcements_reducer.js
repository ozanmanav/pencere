import {
  GET_ANNOUNCEMENTS,
  GET_ANNOUNCEMENTS_SUCCESS,
  GET_ANNOUNCEMENTS_FAILURE,
} from "@redux/actions/types";

const INITIAL_STATE = {
  isFetching: false,
  items: [],
  error: false,
  unreadCount: 0,
  lastUnreadAnnouncement: null,
};

export default function announcementReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case GET_ANNOUNCEMENTS: {
      return {
        isFetching: true,
        error: false,
        ...state,
      };
    }
    case GET_ANNOUNCEMENTS_SUCCESS: {
      var lastUnreadAnnouncement = null;
      for (let el of action.data.BildirimListesi) {
        if (!el.KullaniciOkumaTarihi) {
          lastUnreadAnnouncement = el;
          break;
        }
      }
      return {
        isFetching: false,
        error: false,
        items: action.data.BildirimListesi,
        unreadCount: action.data.OkunmamisBildirimSayisi,
        lastUnreadAnnouncement,
      };
    }
    case GET_ANNOUNCEMENTS_FAILURE: {
      return {
        isFetching: false,
        error: true,
        items: [],
      };
    }
    default:
      return state;
  }
}
