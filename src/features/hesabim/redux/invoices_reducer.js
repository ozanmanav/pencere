const INITAL_STATE = {
  list: [],
  isFetching: false,
  isRefreshing: false,
  error: false,
};

import {
  FETCH_INVOICES,
  REFRESH_INVOICES,
  FETCH_INVOICES_FAILURE,
  FETCH_INVOICES_SUCCESS,
} from "@redux/actions/types";

export default function invoicesReducer(state = INITAL_STATE, action) {
  switch (action.type) {
    case FETCH_INVOICES:
      return {
        ...state,
        isFetching: true,
        error: false,
      };
    case REFRESH_INVOICES:
      return {
        ...state,
        isRefreshing: true,
        error: false,
      };
    case FETCH_INVOICES_SUCCESS:
      return {
        ...state,
        list: action.data,
        isFetching: false,
        isRefreshing: false,
        error: false,
      };
    case FETCH_INVOICES_FAILURE:
      return {
        ...state,
        isFetching: false,
        isRefreshing: false,
        error: true,
      };
    default:
      return state;
  }
}
