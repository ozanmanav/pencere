import {
  FETCH_CAMBIUMS,
  REFRESH_CAMBIUMS,
  FETCH_CAMBIUMS_SUCCESS,
  FETCH_CAMBIUMS_FAILURE,
  FETCH_CAMBIUM_PREVIEWS,
  FETCH_CAMBIUM_PREVIEWS_SUCCESS,
  FETCH_CAMBIUM_PREVIEWS_FAILURE,
  DO_CAMBIUM,
  DO_CAMBIUM_SUCCESS,
  DO_CAMBIUM_FAILURE,
} from "../../../redux/actions/types";
import bugsnag from "@common/bugsnag_config";
import { getEndPoint } from "@common/Utils";

export function fetchCambiums(refresh, pb) {
  //Bayi Açık Kalemleri Kambiyo İçin Listele
  return (dispatch, getState) => {
    dispatch({
      type: refresh ? REFRESH_CAMBIUMS : FETCH_CAMBIUMS,
    });

    fetch(
      getEndPoint("ACCOUNTS_URL") +
        "?si=" +
        getState().session.info.SessionID +
        "&func=8" +
        "&pb=" +
        pb,
    )
      .then(response => response.json())
      .then(responseJson => {
        dispatch({
          type: FETCH_CAMBIUMS_SUCCESS,
          pb,
          data: responseJson,
        });
      })
      .catch(error => {
        bugsnag.notify(error, report => {
          report.errorClass = "fetchCambiums";
        });
        console.log(error);
        dispatch({
          type: FETCH_CAMBIUMS_FAILURE,
          data: error,
        });
      });
  };
}

export function fetchCambiumPreviews(pb, spb, bn) {
  //Ekli belgelerin toplam tutar bilgilerini verir.

  return (dispatch, getState) => {
    dispatch({
      type: FETCH_CAMBIUM_PREVIEWS,
    });
    fetch(
      getEndPoint("ACCOUNTS_URL") +
        "?si=" +
        getState().session.info.SessionID +
        "&func=9&o=0" +
        "&pb=" +
        pb +
        "&spb=" +
        spb +
        "&bn=" +
        bn,
    )
      .then(response => response.json())
      .then(responseJson => {
        dispatch({
          type: FETCH_CAMBIUM_PREVIEWS_SUCCESS,
          data: responseJson,
        });
      })
      .catch(error => {
        bugsnag.notify(error, report => {
          report.errorClass = "fetchCambiumPreviews";
        });
        console.log(error);
        dispatch({
          type: FETCH_CAMBIUM_PREVIEWS_FAILURE,
          data: error,
        });
      });
  };
}

export function doCambium(pb, spb, bn) {
  return (dispatch, getState) => {
    return new Promise((resolve, reject) => {
      dispatch({
        type: DO_CAMBIUM,
      });
      fetch(
        getEndPoint("ACCOUNTS_URL") +
          "?si=" +
          getState().session.info.SessionID +
          "&func=9&o=1" +
          "&pb=" +
          pb +
          "&spb=" +
          spb +
          "&bn=" +
          bn,
      )
        .then(res => res.json())
        .then(responseJson => {
          dispatch({
            type: DO_CAMBIUM_SUCCESS,
          });
          resolve(responseJson.mesaj);
        })
        .catch(err => {
          bugsnag.notify(err, report => {
            report.errorClass = "doCambium";
          });
          dispatch({
            type: DO_CAMBIUM_FAILURE,
          });
          console.log(err);
          reject(err);
        });
    });
  };
}
