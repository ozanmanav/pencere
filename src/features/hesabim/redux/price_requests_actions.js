import { GET_REQUESTS, GET_REQUESTS_SUCCESS, GET_REQUESTS_FAILURE } from "@redux/actions/types";
import bugsnag from "@common/bugsnag_config";
import { getEndPoint } from "@common/Utils";

export function getPriceRequests(refresh) {
  return (dispatch, getState) => {
    dispatch({
      type: GET_REQUESTS,
      isFetching: !refresh,
      isRefreshing: refresh,
    });

    fetch(
      getEndPoint("PRICE_REQUISITIONS_URL") +
        "?Si=" +
        getState().session.info.SessionID +
        "&func=1",
    )
      .then((response) => response.json())
      .then((responseJson) => {
        var uygunsuzUrun = false;
        getState().cart.cartInfo.IlSeciliSepetKalemler.forEach((prod) => {
          if (prod.KalemFiyatTalepDurumu === "X") {
            uygunsuzUrun = true;
          }
        });
        dispatch({
          type: GET_REQUESTS_SUCCESS,
          data: responseJson,
          uygunsuzUrun,
        });
      })
      .catch((error) => {
        bugsnag.notify(error, (report) => {
          report.errorClass = "getPriceRequests";
        });
        console.log(error);
        dispatch({
          type: GET_REQUESTS_FAILURE,
          error,
        });
      });
  };
}
