import {
  FETCH_INVOICES,
  REFRESH_INVOICES,
  FETCH_INVOICES_SUCCESS,
  FETCH_INVOICES_FAILURE,
} from "../../../redux/actions/types";
import bugsnag from "@common/bugsnag_config";
import { getEndPoint } from "@common/Utils";

export function fetchInvoices(refresh, month, year) {
  return (dispatch, getState) => {
    dispatch({
      type: refresh ? REFRESH_INVOICES : FETCH_INVOICES,
    });
    fetch(
      getEndPoint("ACCOUNTS_URL") +
        "?si=" +
        getState().session.info.SessionID +
        "&func=7" +
        "&ay=" +
        month +
        "&yil=" +
        year,
    )
      .then((response) => response.json())
      .then((responseJson) => {
        dispatch({
          type: FETCH_INVOICES_SUCCESS,
          data: responseJson,
        });
      })
      .catch((error) => {
        bugsnag.notify(error, (report) => {
          report.errorClass = "fetchInvoices";
        });
        console.log(error);
        dispatch({
          type: FETCH_INVOICES_FAILURE,
          data: error,
        });
      });
  };
}
