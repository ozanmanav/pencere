import { GET_REQUESTS, GET_REQUESTS_SUCCESS, GET_REQUESTS_FAILURE } from "@redux/actions/types";

const initialState = {
  items: [],
  uygunsuzUrun: false,
  isFetching: false,
  isRefreshing: false,
  error: false,
  errMsg: "",
};

export default function favoritesReducer(state = initialState, action) {
  switch (action.type) {
    case GET_REQUESTS:
      return {
        ...state,
        isFetching: action?.isFetching || false,
        isRefreshing: action?.isRefreshing || false,
        uygunsuzUrun: false,
      };
    case GET_REQUESTS_SUCCESS:
      return {
        ...state,
        isFetching: false,
        isRefreshing: false,
        items: action.data,
        uygunsuzUrun: action.uygunsuzUrun,
      };
    case GET_REQUESTS_FAILURE:
      return {
        ...state,
        items: [],
        error: true,
        isFetching: false,
        isRefreshing: false,
        errMsg: action.error,
        uygunsuzUrun: action.uygunsuzUrun,
      };
    default:
      return state;
  }
}
