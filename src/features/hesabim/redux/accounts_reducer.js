import {
  GET_ACCOUNTS,
  GET_ACCOUNTS_SUCCESS,
  GET_ACCOUNTS_FAILURE,
  GET_ACIK_CEKLER_SUCCESS,
  GET_ACIK_KALEMLER_SUCCESS,
  GET_ACIK_KALEMLER_TOTAL_SUCESS,
  GET_ACIK_KALEMLER_TOTAL_FAILURE,
  GET_HESAP_EKSTRESI_SUCCESS,
  GET_HESAP_EKSTRESI_FAILURE,
  TOGGLE_PROGRESS,
} from "@redux/actions/types";

const initialState = {
  info: [],
  acikCeklerTRY: [],
  acikCeklerEUR: [],
  acikCeklerUSD: [],
  acikKalemlerTRY: [],
  acikKalemlerEUR: [],
  acikKalemlerUSD: [],
  acikKalemlerToplam: {},
  hesapEkstresiTRY: {},
  hesapEkstresiEUR: {},
  hesapEkstresiUSD: {},
  isFetchingAccounts: false,
  error: false,
};

export default function accountsReducer(state = initialState, action) {
  switch (action.type) {
    case GET_ACCOUNTS:
      return {
        ...state,
        isFetchingAccounts: true,
      };
    case TOGGLE_PROGRESS:
      return {
        ...state,
        isFetchingAccounts: action.progress,
      };
    case GET_ACCOUNTS_SUCCESS:
      return {
        ...state,
        info: action.data,
      };
    case GET_ACCOUNTS_FAILURE:
      return {
        ...state,
        error: true,
      };
    case GET_ACIK_CEKLER_SUCCESS:
      switch (action.currency) {
        case "TRY":
          return {
            ...state,
            isFetchingAccounts: false,
            error: true,
            acikCeklerTRY: action.data,
          };
        case "EUR":
          return {
            ...state,
            isFetchingAccounts: false,
            error: true,
            acikCeklerEUR: action.data,
          };
        case "USD":
          return {
            ...state,
            isFetchingAccounts: false,
            error: true,
            acikCeklerUSD: action.data,
          };
      }
      break;
    case GET_ACIK_KALEMLER_SUCCESS:
      switch (action.currency) {
        case "TRY":
          return {
            ...state,
            isFetchingAccounts: false,
            error: true,
            acikKalemlerTRY: action.data,
          };
        case "EUR":
          return {
            ...state,
            isFetchingAccounts: false,
            error: true,
            acikKalemlerEUR: action.data,
          };
        case "USD":
          return {
            ...state,
            isFetchingAccounts: false,
            error: true,
            acikKalemlerUSD: action.data,
          };
      }
      break;
    case GET_ACIK_KALEMLER_TOTAL_SUCESS:
      return {
        ...state,
        acikKalemlerToplam: action.data,
      };
    case GET_ACIK_KALEMLER_TOTAL_FAILURE:
      return {
        ...state,
        error: true,
      };
    case GET_HESAP_EKSTRESI_SUCCESS:
      switch (action.currency) {
        case "TRY":
          return {
            ...state,
            isFetchingAccounts: false,
            error: true,
            hesapEkstresiTRY: action.data,
          };
        case "EUR":
          return {
            ...state,
            isFetchingAccounts: false,
            error: true,
            hesapEkstresiEUR: action.data,
          };
        case "USD":
          return {
            ...state,
            isFetchingAccounts: false,
            error: true,
            hesapEkstresiUSD: action.data,
          };
      }
      break;
    case GET_HESAP_EKSTRESI_FAILURE:
      return {
        ...state,
        error: true,
      };
    default:
      return state;
  }
}
