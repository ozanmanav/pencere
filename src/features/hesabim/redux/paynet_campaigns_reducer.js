import {
  GET_PAYNET_CAMPAIGNS,
  GET_PAYNET_CAMPAIGNS_SUCCESS,
  GET_PAYNET_CAMPAIGNS_FAILURE,
} from "@redux/actions/types";

const INITIAL_STATE = {
  items: [],
  isFetching: false,
  error: false,
};

export default function paynetCampaignsReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case GET_PAYNET_CAMPAIGNS:
      return {
        isFetching: true,
        error: false,
        items: [],
      };
    case GET_PAYNET_CAMPAIGNS_SUCCESS:
      return {
        isFetching: false,
        items: action.data,
        error: false,
      };
    case GET_PAYNET_CAMPAIGNS_FAILURE:
      return {
        isFetching: false,
        error: true,
        ...state,
      };
    default:
      return state;
  }
}
