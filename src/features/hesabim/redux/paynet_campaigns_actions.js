import {
  GET_PAYNET_CAMPAIGNS,
  GET_PAYNET_CAMPAIGNS_SUCCESS,
  GET_PAYNET_CAMPAIGNS_FAILURE,
} from "@redux/actions/types";
import bugsnag from "@common/bugsnag_config";
import { getEndPoint } from "@common/Utils";

export function getPaynetCampaigns(month, year) {
  return (dispatch, getState) => {
    dispatch({
      type: GET_PAYNET_CAMPAIGNS,
    });
    fetch(
      getEndPoint("PAYNET_CAMPAIGNS_URL") +
        "?Si=" +
        getState().session.info.SessionID +
        "&y=" +
        year +
        "&a=" +
        month,
    )
      .then((response) => response.json())
      .then((responseJson) => {
        dispatch({
          type: GET_PAYNET_CAMPAIGNS_SUCCESS,
          data: responseJson,
        });
      })
      .catch((error) => {
        bugsnag.notify(error, (report) => {
          report.errorClass = "getPaynetCampaigns";
        });
        console.log(error);
        dispatch({
          type: GET_PAYNET_CAMPAIGNS_FAILURE,
          error,
        });
      });
  };
}
