import {
  GET_ACCOUNTS,
  GET_ACCOUNTS_SUCCESS,
  GET_ACCOUNTS_FAILURE,
  GET_ACIK_CEKLER_SUCCESS,
  GET_ACIK_CEKLER_FAILURE,
  GET_ACIK_KALEMLER_SUCCESS,
  GET_ACIK_KALEMLER_TOTAL_SUCESS,
  GET_ACIK_KALEMLER_TOTAL_FAILURE,
  GET_HESAP_EKSTRESI_SUCCESS,
  GET_HESAP_EKSTRESI_FAILURE,
  TOGGLE_PROGRESS,
} from "@redux/actions/types";
import bugsnag from "@common/bugsnag_config";
import { getEndPoint } from "@common/Utils";

export function fetchAccountsFromApi() {
  return (dispatch, getState) => {
    var prepared_url =
      getEndPoint("ACCOUNTS_URL") + "?si=" + getState().session.info.SessionID + "&func=5";
    dispatch({ type: TOGGLE_PROGRESS });
    fetch(prepared_url)
      .then((response) => response.json())
      .then((responseJson) => {
        dispatch(getAccountsSuccess(responseJson));
        dispatch({ type: TOGGLE_PROGRESS });
      })
      .catch((error) => {
        bugsnag.notify(error, (report) => {
          report.errorClass = "accounts redux";
        });
        dispatch(getAccountsFailure(error));
        dispatch({ type: TOGGLE_PROGRESS });
      });
  };
}

export function fetchAcikCeklerFromApi(currency) {
  return (dispatch, getState) => {
    var prepared_url =
      getEndPoint("ACCOUNTS_URL") +
      "?si=" +
      getState().session.info.SessionID +
      "&func=4" +
      "&parabirimi=" +
      currency;
    dispatch({ type: GET_ACCOUNTS });
    fetch(prepared_url)
      .then((response) => response.json())
      .then((responseJson) => {
        dispatch({ type: GET_ACIK_CEKLER_SUCCESS, data: responseJson, currency });
      })
      .catch((error) => {
        bugsnag.notify(error, (report) => {
          report.errorClass = "accounts redux";
        });
        console.log(error);
        dispatch({ type: GET_ACIK_CEKLER_FAILURE });
      });
  };
}

export function fetchAcikKalemlerTotal() {
  return (dispatch, getState) => {
    var prepared_url =
      getEndPoint("ACCOUNTS_URL") + "?si=" + getState().session.info.SessionID + "&func=1";
    fetch(prepared_url)
      .then((res) => res.json())
      .then((responseJson) => {
        dispatch({ type: GET_ACIK_KALEMLER_TOTAL_SUCESS, data: responseJson });
      })
      .catch((error) => {
        bugsnag.notify(error, (report) => {
          report.errorClass = "fetchAcikKalemlerTotal parse json";
        });
        dispatch({ type: GET_ACIK_KALEMLER_TOTAL_FAILURE, error });
      });
  };
}

export function fetchAcikKalemlerFromApi(currency) {
  return (dispatch, getState) => {
    var prepared_url =
      getEndPoint("ACCOUNTS_URL") +
      "?si=" +
      getState().session.info.SessionID +
      "&func=3" +
      "&parabirimi=" +
      currency;
    dispatch({ type: GET_ACCOUNTS });
    fetch(prepared_url)
      .then((response) => response.json())
      .then((responseJson) => {
        console.log("açık kalemler", responseJson);
        dispatch({ type: GET_ACIK_KALEMLER_SUCCESS, data: responseJson, currency });
      })
      .catch((error) => {
        bugsnag.notify(error, (report) => {
          report.errorClass = "fetchAcikKalemlerFromApi";
        });
        console.log(error);
        dispatch({ type: GET_HESAP_EKSTRESI_FAILURE });
      });
  };
}

export function fetchHesapEkstresiFromApi(currency, startMonth, endMonth, year) {
  return (dispatch, getState) => {
    return new Promise(function (resolve, reject) {
      var prepared_url =
        getEndPoint("ACCOUNTS_URL") +
        "?si=" +
        getState().session.info.SessionID +
        "&func=2" +
        "&parabirimi=" +
        currency +
        "&baslangicAyi=" +
        (startMonth + 1) +
        "&bitisAyi=" +
        (endMonth + 1) +
        "&yil=" +
        year;
      dispatch({ type: TOGGLE_PROGRESS, progress: true });
      fetch(prepared_url)
        .then((response) => response.json())
        .then((responseJson) => {
          console.log("hesap ekstresi", responseJson);
          dispatch({ type: GET_HESAP_EKSTRESI_SUCCESS, data: responseJson, currency });
          dispatch({ type: TOGGLE_PROGRESS, progress: false });
          resolve();
        })
        .catch((error) => {
          bugsnag.notify(error, (report) => {
            report.errorClass = "fetchHesapEkstresiFromApi";
          });
          console.log(error);
          dispatch({ type: TOGGLE_PROGRESS, progress: false });
          reject();
        });
    });
  };
}

export function fetchAllHesapEkstresi(startMonth, endMonth, year) {
  var currencies = ["TRY", "USD", "EUR"];
  return async (dispatch, getState) => {
    dispatch({ type: TOGGLE_PROGRESS, progress: true });
    for (const currency of currencies) {
      var prepared_url =
        getEndPoint("ACCOUNTS_URL") +
        "?si=" +
        getState().session.info.SessionID +
        "&func=2" +
        "&parabirimi=" +
        currency +
        "&baslangicAyi=" +
        (startMonth + 1) +
        "&bitisAyi=" +
        (endMonth + 1) +
        "&yil=" +
        year;
      try {
        var resp = await fetch(prepared_url);
        var responseJson = await resp.json();
        dispatch({ type: GET_HESAP_EKSTRESI_SUCCESS, data: responseJson, currency });
      } catch (error) {
        dispatch({ type: GET_HESAP_EKSTRESI_FAILURE });
      }
    }
    dispatch({ type: TOGGLE_PROGRESS, progress: false });
  };
}

function getAccountsSuccess(data) {
  return {
    type: GET_ACCOUNTS_SUCCESS,
    data,
  };
}

function getAccountsFailure(error) {
  console.log("getCampaignsFailure", error);
  return {
    type: GET_ACCOUNTS_FAILURE,
  };
}
