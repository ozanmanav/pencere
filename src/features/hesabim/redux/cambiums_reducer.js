const INITAL_STATE = {
  itemsTRY: [],
  itemsUSD: [],
  itemsEUR: [],
  isFetching: false,
  isFetchingPreview: false,
  isRefreshing: false,
  preview: {},
  error: false,
};

import {
  FETCH_CAMBIUMS,
  REFRESH_CAMBIUMS,
  FETCH_CAMBIUMS_SUCCESS,
  FETCH_CAMBIUMS_FAILURE,
  FETCH_CAMBIUM_PREVIEWS,
  FETCH_CAMBIUM_PREVIEWS_SUCCESS,
  FETCH_CAMBIUM_PREVIEWS_FAILURE,
  DO_CAMBIUM,
  DO_CAMBIUM_SUCCESS,
  DO_CAMBIUM_FAILURE,
} from "@redux/actions/types";

export default function cambiumsReducer(state = INITAL_STATE, action) {
  switch (action.type) {
    case DO_CAMBIUM:
      return {
        ...state,
        isFetching: true,
        error: false,
      };
    case FETCH_CAMBIUMS:
      return {
        ...state,
        isFetching: true,
        error: false,
      };
    case REFRESH_CAMBIUMS:
      return {
        ...state,
        items: [],
        isRefreshing: true,
        error: false,
      };
    case FETCH_CAMBIUMS_SUCCESS:
      console.log("fetchCambiums reducer", action);
      return {
        ...state,
        itemsTRY: action.pb === "TRY" ? action.data : state.itemsTRY,
        itemsEUR: action.pb === "EUR" ? action.data : state.itemsEUR,
        itemsUSD: action.pb === "USD" ? action.data : state.itemsUSD,
        isFetching: false,
        isRefreshing: false,
        error: false,
      };
    case FETCH_CAMBIUMS_FAILURE:
      return {
        ...state,
        isFetching: false,
        isRefreshing: false,
        error: true,
      };
    case FETCH_CAMBIUM_PREVIEWS:
      return {
        ...state,
        isFetchingPreview: true,
      };
    case FETCH_CAMBIUM_PREVIEWS_SUCCESS:
      return {
        ...state,
        preview: action.data,
        isFetchingPreview: false,
        error: false,
      };
    case FETCH_CAMBIUM_PREVIEWS_FAILURE:
      return {
        ...state,
        preview: {},
        isFetchingPreview: false,
        error: true,
      };
    case DO_CAMBIUM_SUCCESS:
      return {
        ...state,
        isFetching: false,
        error: false,
      };
    case DO_CAMBIUM_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true,
      };
    default:
      return state;
  }
}
