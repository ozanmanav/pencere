import React, { Component } from "react";
import { SafeAreaView, View, StyleSheet, Dimensions, ScrollView } from "react-native";
import { Text, AnimatedHeader, Card } from "@components";
import colors from "@common/styles/colors";
import { getDate } from "@common/Utils";
import { moderateScale } from "react-native-size-matters";

const win = Dimensions.get("window");

export default class AcikCekDetailsScreen extends Component {
  componentDidMount() {}

  render() {
    const item = this.props.navigation.state.params.item;
    return (
      <SafeAreaView style={{ backgroundColor: colors.primary_dark }} forceInset={{ top: "always" }}>
        <View style={{ height: win.height, width: win.width, backgroundColor: "#efefef" }}>
          <ScrollView style={{ height: win.height, marginTop: moderateScale(50, 0.4) }}>
            <Card
              renderContent={() => {
                return (
                  <View style={{ flexDirection: "column", padding: moderateScale(10, 0.5) }}>
                    <View>
                      <Text fontScale={0.5} style={styles.titleTxt}>
                        {"Belge No:"}
                      </Text>
                      <Text fontScale={0.5} style={styles.valueTxt}>
                        {item.BelgeNo !== "" ? item.BelgeNo : "Yok"}
                      </Text>
                    </View>
                    <View>
                      <Text fontScale={0.5} style={styles.titleTxt}>
                        {"Çek No:"}
                      </Text>
                      <Text fontScale={0.5} style={styles.valueTxt}>
                        {item.CekNo}
                      </Text>
                    </View>
                    <View>
                      <Text fontScale={0.5} style={styles.titleTxt}>
                        {"Banka / Şube Kodu:"}
                      </Text>
                      <Text fontScale={0.5} style={styles.valueTxt}>
                        {item.BankaKodu}
                      </Text>
                    </View>
                    <View>
                      <Text fontScale={0.5} style={styles.titleTxt}>
                        {"Banka Adı:"}
                      </Text>
                      <Text fontScale={0.5} style={styles.valueTxt}>
                        {item.BankaAdi}
                      </Text>
                    </View>
                    <View>
                      <Text fontScale={0.5} style={styles.titleTxt}>
                        {"Hesap No:"}
                      </Text>
                      <Text fontScale={0.5} style={styles.valueTxt}>
                        {item.HesapNo}
                      </Text>
                    </View>
                    <View>
                      <Text fontScale={0.5} style={styles.titleTxt}>
                        {"Kayıt Tarihi:"}
                      </Text>
                      <Text fontScale={0.5} style={styles.valueTxt}>
                        {getDate(item.KayitTarihi)}
                      </Text>
                    </View>
                    <View>
                      <Text fontScale={0.5} style={styles.titleTxt}>
                        {"Vade Tarihi:"}
                      </Text>
                      <Text fontScale={0.5} style={styles.valueTxt}>
                        {getDate(item.VadeTarihi)}
                      </Text>
                    </View>
                    <View>
                      <Text fontScale={0.5} style={styles.titleTxt}>
                        {"Para Birimi:"}
                      </Text>
                      <Text fontScale={0.5} style={styles.valueTxt}>
                        {item.ParaBirimi}
                      </Text>
                    </View>
                    <View>
                      <Text fontScale={0.5} style={styles.titleTxt}>
                        {"Tutar:"}
                      </Text>
                      <Text fontScale={0.5} style={styles.valueTxt}>
                        {item.Tutar}
                      </Text>
                    </View>
                  </View>
                );
              }}
            />
          </ScrollView>
        </View>
        <AnimatedHeader
          navigation={this.props.navigation}
          title="Açık Çek Detayları"
          back={true}
          searchBar={false}
          alignLeft={true}
        />
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  titleTxt: {
    fontSize: 15,
    color: "orange",
    marginBottom: 5,
  },
  valueTxt: {
    fontSize: 15,
    color: "black",
    marginBottom: 5,
  },
});
