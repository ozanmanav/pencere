import React, { Component } from "react";
import {
  View,
  StatusBar,
  SafeAreaView,
  TouchableOpacity,
  Animated,
  StyleSheet,
  FlatList,
  Dimensions,
  Platform,
  Image,
  RefreshControl,
} from "react-native";
import { AnimatedHeader, Text, BottomPicker, PencereIndicator } from "@components";
import { InvoiceItem } from "@features/hesabim/components";
import { connect } from "react-redux";
import colors from "@common/styles/colors";
import { fetchInvoices } from "@redux/actions";
import { moderateScale } from "react-native-size-matters";
import { getLastNMonths, getMonthYearString } from "@common/Utils";

const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);

const win = Dimensions.get("window");

class InvoicesListScreen extends Component {
  data = [
    {
      tarih: "2017-09-07",
      belgeAciklama: "SATI\u015e FATURASI",
      siparisNo: "001147366",
      sasNo: "BT SARF: K",
      faturaNo: "00986378",
      referans: "A402017000014",
      tutar: "90,68 TRY",
      teslimBilgisi: "-",
      faturaLinkP:
        "https://test3.pencere.com/EArsiv.aspx?fn=9QCvRIpvNDdhQJ6cCCddBg%3d%3d&etnn=unu10LL2GJ9tYSCQkwdt7pGoVpYewrhlwFUeN12NPpNEpamlyPWpwQ%3d%3d&type=pdf",
      faturaLinkX: "https://test3.pencere.com/EArsiv.aspx?fn=9QCv&type=xml",
      faturaGosterilebilir: "1",
      mesaj: "Elektronik Faturan\u0131z Hen\u00fcz Olu\u015fmam\u0131\u015ft\u0131r",
    },
    {
      tarih: "2018-04-17",
      belgeAciklama: "SATI\u015e FATURASI",
      siparisNo: "111111111",
      sasNo: "BT SARF: K",
      faturaNo: "örnek",
      referans: "deneme",
      tutar: "90,68 TRY",
      teslimBilgisi: "-",
      faturaLinkP:
        "https://test3.pencere.com/EArsiv.aspx?fn=9QCvRIpvNDdhQJ6cCCddBg%3d%3d&etnn=unu10LL2GJ9tYSCQkwdt7pGoVpYewrhlwFUeN12NPpNEpamlyPWpwQ%3d%3d&type=pdf",
      faturaLinkX: "https://test3.pencere.com/EArsiv.aspx?fn=9QCv&type=xml",
      faturaGosterilebilir: "0",
      mesaj: "Elektronik Faturan\u0131z Hen\u00fcz Olu\u015fmam\u0131\u015ft\u0131r",
    },
  ];

  constructor(props) {
    super(props);
    const scrollAnim = new Animated.Value(0);
    const offsetAnim = new Animated.Value(0);
    this.state = {
      selectedDateIndex: 0,
      dates: [],
      data: [],
      seg: 0,
      scrollAnim,
      clampedScroll: Animated.diffClamp(
        Animated.add(
          scrollAnim.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 1],
            extrapolateLeft: "clamp",
          }),
          offsetAnim,
        ),
        0,
        50,
      ),
    };
    this.datesRaw = getLastNMonths(12);
  }

  componentDidMount() {
    this.state.scrollAnim.addListener(({ value }) => {
      // This is the same calculations that diffClamp does.
      this.diff = value - this._scrollValue;
      this._scrollValue = value;
      // if (this.diff > 0 && value > (dim.width * 9 / 16 + 50))
      //     this.refs.fixedBtn.startFadeAnim(false);
      // else if (this.refs.fixedBtn.state.isArrowVisible)
      //     this.refs.fixedBtn.startFadeAnim(true);

      this._clampedScrollValue = Math.min(Math.max(this._clampedScrollValue + this.diff, 0), 50);
    });

    this.props.fetchInvoices(false, this.datesRaw[0].month() + 1, this.datesRaw[0].year());
  }

  renderItem = ({ item, index }) => {
    return <InvoiceItem item={item} navigation={this.props.navigation} />;
  };

  renderNoContent = () => {
    if (!this.props.invoices.isFetching && !this.props.invoices.isRefreshing)
      return (
        <View
          style={{
            paddingTop: 0,
            height: moderateScale(70, 0.5),
            width: win.width,
            alignItems: "center",
          }}
        >
          <Text fontScale={0.5} style={{ fontSize: 16, color: "gray" }}>
            Seçiminize uygun fatura bulunmamaktadır.
          </Text>
        </View>
      );
    else return null;
  };

  selectDate = (selectedVal, selectedDateIndex) => {
    console.log("selected", this.datesRaw[selectedDateIndex].month());
    this.setState(
      {
        selectedDateIndex,
      },
      () => {
        this.props.fetchInvoices(
          false,
          this.datesRaw[this.state.selectedDateIndex].month() + 1,
          this.datesRaw[this.state.selectedDateIndex].year(),
        );
      },
    );
  };

  onRefresh = () => {
    this.props.fetchInvoices(
      true,
      this.datesRaw[this.state.selectedDateIndex].month() + 1,
      this.datesRaw[this.state.selectedDateIndex].year(),
    );
  };

  render() {
    return (
      <SafeAreaView
        style={{ backgroundColor: colors.primary_dark, flex: 1 }}
        forceInset={{ top: "always" }}
      >
        <View style={{ flexGrow: 1, width: win.width, backgroundColor: "#efefef" }}>
          <AnimatedFlatList
            style={{ zIndex: -1 }}
            contentOffset={{ y: -moderateScale(90, 0.4) }}
            contentInset={{ top: moderateScale(90, 0.4), left: 0, right: 0 }}
            contentContainerStyle={{
              flexGrow: 1,
              paddingBottom: moderateScale(20, 0.5),
              paddingTop:
                Platform.OS === "android"
                  ? moderateScale(90, 0.4) + (Platform.Version < 20 ? 0 : StatusBar.currentHeight)
                  : 0,
            }}
            data={this.props.invoices.list}
            renderItem={this.renderItem}
            scrollEventThrottle={16}
            ListEmptyComponent={this.renderNoContent}
            refreshControl={
              <RefreshControl
                refreshing={this.props.invoices.isRefreshing}
                onRefresh={this.onRefresh}
                progressViewOffset={130}
              />
            }
            onScroll={Animated.event(
              [{ nativeEvent: { contentOffset: { y: this.state.scrollAnim } } }],
              {
                useNativeDriver: true,
              },
            )}
          />
          <AnimatedHeader
            navigation={this.props.navigation}
            title="Fatura Listesi"
            clampedScroll={this.state.clampedScroll}
            searchBar={false}
            back={true}
            alignLeft={true}
            onBackPressed={() => this.props.navigation.navigate("MyAccountScreen")}
          />
          <View
            style={{
              width: win.width,
              height: 30,
              position: "absolute",
              top:
                moderateScale(50, 0.4) +
                (Platform.OS === "android"
                  ? Platform.Version < 20
                    ? 0
                    : StatusBar.currentHeight
                  : 0),
              left: 0,
              elevation: 1,
            }}
          >
            <TouchableOpacity
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                width: win.width,
                paddingHorizontal: moderateScale(10, 0.5),
                backgroundColor: "white",
                position: "absolute",
                height: moderateScale(36, 0.5),
                shadowColor: "#000",
                shadowOffset: { width: 0, height: 1 },
                shadowOpacity: 0.2,
                shadowRadius: 1,
                elevation: 1,
                alignItems: "center",
              }}
              onPress={() => this.bottomPicker.openPicker()}
            >
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Text fontScale={0.5} style={{ fontSize: 16, color: "gray" }}>
                  {"Tarih Seçin:   " +
                    getMonthYearString(this.datesRaw[this.state.selectedDateIndex])}
                </Text>
                <Image
                  style={{
                    width: 16,
                    height: 16,
                    resizeMode: "contain",
                    tintColor: "gray",
                    marginHorizontal: 5,
                    marginTop: 2,
                  }}
                  source={require("@assets/images/down.png")}
                />
              </View>
            </TouchableOpacity>
          </View>
        </View>
        {this.props.invoices.isFetching && (
          <View style={[styles.loadingContainer]}>
            <PencereIndicator />
          </View>
        )}
        <BottomPicker
          ref={(c) => (this.bottomPicker = c)}
          items={this.datesRaw}
          initialItem={this.datesRaw[this.state.selectedDateIndex]}
          labelExtractor={(d) => getMonthYearString(d)}
          onPressOK={this.selectDate}
        />
      </SafeAreaView>
    );
  }
}

function mapStateToProps(state) {
  return {
    invoices: state.invoices,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    fetchInvoices: (refresh, ay, yil) => dispatch(fetchInvoices(refresh, ay, yil)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(InvoicesListScreen);

const styles = StyleSheet.create({
  loadingContainer: {
    height: win.height,
    width: win.width,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0,0,0,0.3)",
    position: "absolute",
  },
});
