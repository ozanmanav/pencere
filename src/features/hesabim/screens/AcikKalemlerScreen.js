import React, { Component } from "react";
import {
  View,
  SafeAreaView,
  TouchableOpacity,
  Animated,
  StyleSheet,
  FlatList,
  Dimensions,
  Platform,
  StatusBar,
} from "react-native";
import { AnimatedHeader, Text, Card, PencereIndicator } from "@components";
import { connect } from "react-redux";
import colors from "@common/styles/colors";
import { getDate } from "@common/Utils";
import { BlurView } from "@react-native-community/blur";
import { moderateScale } from "react-native-size-matters";

const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);
const STATUSBAR_HEIGHT =
  Platform.OS === "android" ? (Platform.Version < 20 ? 0 : StatusBar.currentHeight) : 0;

const win = Dimensions.get("window");

class AcikKalemlerScreen extends Component {
  constructor(props) {
    super(props);
    this.data = this.props.accounts.acikKalemlerTRY.IlCariHesap;
    const scrollAnim = new Animated.Value(0);
    const offsetAnim = new Animated.Value(0);
    this.state = {
      seg: 0,
      scrollAnim,
      clampedScroll: Animated.diffClamp(
        Animated.add(
          scrollAnim.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 1],
            extrapolateLeft: "clamp",
          }),
          offsetAnim,
        ),
        0,
        50,
      ),
    };
  }

  renderItem = ({ item }) => (
    <Card
      onPress={() => this.props.navigation.push("AcikKalemDetailsScreen", { item })}
      renderContent={() => {
        return (
          <View style={{ flexDirection: "row", padding: moderateScale(10, 0.5) }}>
            <View
              style={{
                flex: 0.5,
                backgroundColor: "transparent",
                flexDirection: "column",
              }}
            >
              <Text fontScale={0.5} style={styles.titleTxt}>
                {"Belge No"}
              </Text>
              <Text fontScale={0.5} style={styles.titleTxt}>
                {"Belge Tipi"}
              </Text>
              <Text fontScale={0.5} style={styles.titleTxt}>
                {"Açıklama"}
              </Text>
              <Text fontScale={0.5} style={styles.titleTxt}>
                {"Kayıt Tarihi"}
              </Text>
              <Text fontScale={0.5} style={styles.titleTxt}>
                {"Vade Tarihi"}
              </Text>
              <Text fontScale={0.5} style={styles.titleTxt}>
                {"Tutar"}
              </Text>
            </View>
            <View
              style={{
                flex: 0.5,
                backgroundColor: "transparent",
                flexDirection: "column",
              }}
            >
              <Text fontScale={0.5} numberOfLines={1} style={styles.valueTxt}>
                {item.BelgeNo}
              </Text>
              <Text fontScale={0.5} numberOfLines={1} style={styles.valueTxt}>
                {item.BelgeTipiAciklamasi}
              </Text>
              <Text fontScale={0.5} numberOfLines={1} style={styles.valueTxt}>
                {item.Aciklama}
              </Text>
              <Text fontScale={0.5} numberOfLines={1} style={styles.valueTxt}>
                {getDate(item.KayitTarihi)}
              </Text>
              <Text fontScale={0.5} numberOfLines={1} style={styles.valueTxt}>
                {getDate(item.VadeTarihi)}
              </Text>
              <Text fontScale={0.5} numberOfLines={1} style={styles.valueTxt}>
                {item.Tutar + " " + item.ParaBirimi}
              </Text>
            </View>
          </View>
        );
      }}
    />
  );

  renderTotalContent = () => {
    if (this.props.accounts.acikKalemlerToplam.IlBakiyeler)
      return (
        <View
          style={{
            marginTop:
              Platform.OS === "android" ? (Platform.Version < 20 ? 0 : StatusBar.currentHeight) : 0,
          }}
        >
          <View
            style={{
              flexDirection: "row",
              borderBottomColor: "rgba(0, 0, 0, 0.2)",
              borderBottomWidth: 1,
              padding: 10,
            }}
          >
            <View style={{ flexDirection: "column", flex: 1 }}>
              <Text fontScale={0.5} style={styles.totalTitle}>
                Para Birimi
              </Text>
              <Text fontScale={0.5} style={styles.totalVal}>
                {this.props.accounts.acikKalemlerToplam.IlBakiyeler[0].ParaBirimi}
              </Text>
              <Text fontScale={0.5} style={styles.totalVal}>
                {this.props.accounts.acikKalemlerToplam.IlBakiyeler[1].ParaBirimi}
              </Text>
              <Text fontScale={0.5} style={styles.totalVal}>
                {this.props.accounts.acikKalemlerToplam.IlBakiyeler[2].ParaBirimi}
              </Text>
            </View>
            <View style={{ flexDirection: "column", flex: 1 }}>
              <Text fontScale={0.5} style={styles.totalTitle}>
                Bakiye
              </Text>
              <Text fontScale={0.5} style={styles.totalVal}>
                {this.props.accounts.acikKalemlerToplam.IlBakiyeler[0].Bakiye}
              </Text>
              <Text fontScale={0.5} style={styles.totalVal}>
                {this.props.accounts.acikKalemlerToplam.IlBakiyeler[1].Bakiye}
              </Text>
              <Text fontScale={0.5} style={styles.totalVal}>
                {this.props.accounts.acikKalemlerToplam.IlBakiyeler[2].Bakiye}
              </Text>
            </View>
            <View style={{ flexDirection: "column", flex: 1 }}>
              <Text fontScale={0.5} style={styles.totalTitle}>
                Bakiye TL
              </Text>
              <Text fontScale={0.5} style={styles.totalVal}>
                {this.props.accounts.acikKalemlerToplam.IlBakiyeler[0].TrlBakiye}
              </Text>
              <Text fontScale={0.5} style={styles.totalVal}>
                {this.props.accounts.acikKalemlerToplam.IlBakiyeler[1].TrlBakiye}
              </Text>
              <Text fontScale={0.5} style={styles.totalVal}>
                {this.props.accounts.acikKalemlerToplam.IlBakiyeler[2].TrlBakiye}
              </Text>
            </View>
          </View>
          <View
            style={{
              flexDirection: "row",
              padding: 5,
              paddingHorizontal: 10,
              justifyContent: "space-between",
            }}
          >
            <Text fontScale={0.5} style={{ fontWeight: "bold", color: "gold", fontSize: 14 }}>
              Toplam
            </Text>
            <Text fontScale={0.5} style={{ fontWeight: "bold", color: "gold", fontSize: 14 }}>
              {this.props.accounts.acikKalemlerToplam.IlBakiyeler[0].TrlBakiye +
                this.props.accounts.acikKalemlerToplam.IlBakiyeler[1].TrlBakiye +
                this.props.accounts.acikKalemlerToplam.IlBakiyeler[2].TrlBakiye +
                " TL"}
            </Text>
          </View>
          {this.renderHeader()}
        </View>
      );
    else return null;
  };
  renderTotalView = () => {
    return Platform.OS === "ios" ? (
      <BlurView
        style={{ backgroundColor: "rgba(5, 10, 15, 0.9)" }}
        blurType="light"
        blurAmount={10}
      >
        {this.renderTotalContent()}
      </BlurView>
    ) : (
      <View style={{ backgroundColor: "rgba(90, 120, 130, 1)" }}>{this.renderTotalContent()}</View>
    );
  };

  onChangeTab(tabIndex) {
    this.setState({ seg: tabIndex });
    switch (tabIndex) {
      case 0:
        this.data = this.props.accounts.acikCeklerTRY;
        break;
      case 1:
        this.data = this.props.accounts.acikCeklerUSD;
        break;
      case 2:
        this.data = this.props.accounts.acikCeklerEUR;
        break;
    }
  }

  renderHeader = () => {
    return (
      <View style={styles.segment}>
        <TouchableOpacity
          activeOpacity={1}
          style={this.state.seg === 0 ? styles.activeSegmentButton : styles.inactiveSegmentButton}
          onPress={() => this.onChangeTab(0)}
        >
          <Text
            fontScale={0.5}
            style={this.state.seg === 0 ? styles.activeSegmentText : styles.inactiveSegmentText}
          >
            TL
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          activeOpacity={1}
          style={this.state.seg === 1 ? styles.activeSegmentButton : styles.inactiveSegmentButton}
          onPress={() => this.onChangeTab(1)}
        >
          <Text
            fontScale={0.5}
            style={this.state.seg === 1 ? styles.activeSegmentText : styles.inactiveSegmentText}
          >
            EURO
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          activeOpacity={1}
          style={this.state.seg === 2 ? styles.activeSegmentButton : styles.inactiveSegmentButton}
          onPress={() => this.onChangeTab(2)}
        >
          <Text
            fontScale={0.5}
            style={this.state.seg === 2 ? styles.activeSegmentText : styles.inactiveSegmentText}
          >
            USD
          </Text>
        </TouchableOpacity>
      </View>
    );
  };

  renderNoContent = () => {
    return (
      <View style={{ paddingTop: 10, width: win.width, alignItems: "center" }}>
        <Text fontScale={0.5} style={{ fontSize: 16, color: "black" }}>
          Açık kalem kaydı bulanamadı.
        </Text>
      </View>
    );
  };

  render() {
    return (
      <SafeAreaView
        style={{ backgroundColor: colors.primary_dark, flex: 1 }}
        forceInset={{ top: "always" }}
      >
        <View style={{ width: win.width, backgroundColor: "#efefef", flexGrow: 1 }}>
          <AnimatedFlatList
            style={{ zIndex: -1 }}
            contentOffset={{ y: -moderateScale(50, 0.4) }}
            contentInset={{
              top: moderateScale(50, 0.4),
              left: 0,
              bottom: 0,
              right: 0,
            }}
            contentContainerStyle={{
              flexGrow: 1,
              paddingBottom: moderateScale(10, 0.5),
              paddingTop: Platform.OS === "android" ? moderateScale(30, 0.4) + STATUSBAR_HEIGHT : 0,
            }}
            data={this.data}
            renderItem={this.renderItem}
            ListHeaderComponent={this.renderTotalView}
            ListEmptyComponent={this.renderNoContent}
            stickyHeaderIndices={[0]}
          />
          <AnimatedHeader
            navigation={this.props.navigation}
            title="Açık Kalemler"
            clampedScroll={this.state.clampedScroll}
            back={true}
            alignLeft={true}
          />
        </View>
        {this.props.accounts.isFetchingAccounts && (
          <View style={[styles.loadingContainer]}>
            <PencereIndicator />
          </View>
        )}
      </SafeAreaView>
    );
  }
}

function mapStateToProps(state) {
  return {
    accounts: state.accounts,
  };
}

export default connect(mapStateToProps, null)(AcikKalemlerScreen);
const styles = StyleSheet.create({
  segment: {
    flexDirection: "row",
    flex: 1,
    backgroundColor: "rgba(255,255,255,0.1)",
    height: 30,
    width: win.width,
  },
  loadingContainer: {
    height: win.height,
    width: win.width,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0,0,0,0.3)",
    position: "absolute",
  },
  activeSegmentButton: {
    justifyContent: "center",
    flex: 0.5,
    flexDirection: "column",
    borderBottomColor: "white",
    borderBottomWidth: 2,
    marginHorizontal: 5,
    paddingVertical: 4,
  },
  inactiveSegmentButton: {
    justifyContent: "center",
    width: 100,
    flex: 0.5,
    flexDirection: "column",
    marginHorizontal: 5,
    paddingVertical: 4,
  },
  activeSegmentText: {
    textAlign: "center",
    color: "white",
    fontSize: 15,
    fontWeight: "bold",
  },
  inactiveSegmentText: {
    textAlign: "center",
    color: "white",
    fontSize: 14,
    fontWeight: "bold",
  },
  titleTxt: {
    fontSize: 12,
    color: "orange",
    marginBottom: 5,
  },
  valueTxt: {
    fontSize: 12,
    color: "black",
    marginBottom: 5,
  },
  totalTitle: {
    fontSize: 14,
    color: "white",
    marginBottom: 4,
  },
  totalVal: {
    fontSize: 12,
    color: "#dedede",
    marginBottom: 2,
  },
});
