import React, { Component } from "react";
import {
  View,
  SafeAreaView,
  StyleSheet,
  Image,
  Clipboard,
  Platform,
  Animated,
  StatusBar,
  Dimensions,
  TouchableOpacity,
  FlatList,
} from "react-native";
import { connect } from "react-redux";
import { Text, AnimatedHeader, PencereIndicator } from "@components";
import colors from "@common/styles/colors";
import { showToast } from "@redux/actions/";
import { moderateScale } from "react-native-size-matters";
import { getEndPoint } from "@common/Utils";
import bugsnag from "@common/bugsnag_config";

const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);

const win = Dimensions.get("window");
const NAVBAR_HEIGHT = AnimatedHeader.height;
const STATUS_BAR_HEIGHT = 0;

class CouponsScreen extends Component {
  constructor(props) {
    super(props);
    const scrollAnim = new Animated.Value(0);
    this.state = {
      isFetching: false,
      items: [],
      scrollAnim,
      clampedScroll: Animated.diffClamp(
        Animated.add(
          Animated.add(scrollAnim, 100).interpolate({
            inputRange: [0, 1],
            outputRange: [0, 1],
            extrapolateLeft: "clamp",
          }),
          -100,
        ),
        0,
        NAVBAR_HEIGHT - STATUS_BAR_HEIGHT,
      ),
    };
  }

  componentDidMount() {
    this.setState({ isFetching: true });
    fetch(getEndPoint("KUPON_URL") + "?si=" + this.props.session.info.SessionID)
      .then(res => res.json())
      .then(responseJson => {
        this.setState({
          items: responseJson.MobileKuponList,
          isFetching: false,
        });
      })
      .catch(err => {
        console.log("kupon error", err);
        this.setState({ isFetching: false });
        bugsnag.notify(err, report => {
          report.errorClass = "getCoupons";
        });
      });
  }

  _keyboardDidShow = e => {
    this.setState({
      keyboardVisible: true,
      keyboardHeight: e.endCoordinates.height,
    });
  };

  _keyboardDidHide = () => {
    this.setState({ keyboardVisible: false });
  };

  renderItem = ({ item, index }) => {
    return (
      <TouchableOpacity
        onPress={() => {
          Clipboard.setString(item.KuponKodu);
          this.props.showToast("Ürün kodu kopyalandı");
        }}
        style={{
          flexDirection: "column",
          borderBottomColor: "#cecece",
          borderBottomWidth: index < this.state.items.length - 1 ? 1 : 0,
          marginBottom: moderateScale(10, 0.5),
          width: win.width - 2 * moderateScale(10, 0.5),
          backgroundColor: "white",
          alignSelf: "center",
          padding: moderateScale(10, 0.5),
        }}
      >
        <View style={{ flexDirection: "row", alignItems: "center" }}>
          <Text
            style={{
              fontFamily: "Poppins",
              fontSize: 11,
              color: "black",
            }}
          >
            {"Kupon Kodu: " + item.KuponKodu}
          </Text>
          <Image
            source={require("@assets/images/copy.png")}
            style={{
              marginLeft: moderateScale(5, 0.5),
              width: moderateScale(16, 0.4),
              height: moderateScale(16, 0.4),
              resizeMode: "contain",
              tintColor: "gray",
            }}
          />
        </View>
        <Text
          style={{
            fontFamily: "Poppins",
            fontSize: 11,
            color: colors.light_black,
          }}
        >
          {item.KuponText}
        </Text>
        <Text
          style={{
            fontFamily: "Poppins",
            fontSize: 11,
            color: colors.light_black,
          }}
        >
          {"Son Kullanma Tarihi: " + item.KuponSonKullanmaTarihi}
        </Text>
      </TouchableOpacity>
    );
  };

  render() {
    return (
      <SafeAreaView
        style={{ backgroundColor: colors.primary_dark, flex: 1 }}
        forceInset={{ top: "always" }}
      >
        <View
          behavior={Platform.OS === "ios" ? "position" : null}
          style={{ flexGrow: 1, width: win.width, backgroundColor: "#efefef" }}
        >
          <AnimatedFlatList
            data={this.state.items}
            renderItem={this.renderItem}
            scrollEventThrottle={16}
            overScrollMode="never"
            contentOffset={{ y: -AnimatedHeader.height }}
            contentInset={{ top: AnimatedHeader.height, left: 0, right: 0 }}
            ListEmptyComponent={() => (
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <Text fontScale={0.6} style={{ fontFamily: "Poppins", color: "gray" }}>
                  Görünütülenecek kupon yok
                </Text>
              </View>
            )}
            contentContainerStyle={{
              flexGrow: 1,
              paddingTop:
                Platform.OS === "android"
                  ? AnimatedHeader.height +
                    (Platform.Version < 20 ? 0 : StatusBar.currentHeight) +
                    moderateScale(10, 0.5)
                  : moderateScale(10, 0.5),
              paddingBottom: moderateScale(60, 0.4),
              backgroundColor: "#efefef",
              alignItems: "center",
            }}
            onScroll={Animated.event(
              [{ nativeEvent: { contentOffset: { y: this.state.scrollAnim } } }],
              {
                useNativeDriver: true,
              },
            )}
          />
          {this.state.isFetching && (
            <View style={[styles.loadingContainer]}>
              <PencereIndicator />
            </View>
          )}
          <AnimatedHeader
            navigation={this.props.navigation}
            title="Kuponlarım"
            clampedScroll={this.state.clampedScroll}
            home={true}
            searchBar={true}
            search={false}
            back={true}
            alignLeft={true}
          />
        </View>
      </SafeAreaView>
    );
  }
}

function mapStateToProps(state) {
  return {
    session: state.session,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    showToast: msg => dispatch(showToast(msg)),
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(CouponsScreen);

const styles = StyleSheet.create({
  loadingContainer: {
    height: win.height,
    width: win.width,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0,0,0,0.5)",
    position: "absolute",
    top: 0,
    left: 0,
  },
});
