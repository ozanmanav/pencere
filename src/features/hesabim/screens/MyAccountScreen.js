import React, { Component } from "react";
import {
  SafeAreaView,
  View,
  TouchableOpacity,
  Image,
  Dimensions,
  Animated,
  ScrollView,
  Platform,
  StatusBar,
} from "react-native";
import { Text, TabBarIcon, AnimatedHeader } from "@components";
import colors from "@common/styles/colors";
import {
  fetchAccountsFromApi,
  fetchAcikCeklerFromApi,
  fetchAcikKalemlerFromApi,
  fetchAcikKalemlerTotal,
  logout,
} from "@redux/actions";
import { connect } from "react-redux";
import { moderateScale, ScaledSheet } from "react-native-size-matters";

const AnimatedScrollView = Animated.createAnimatedComponent(ScrollView);

const win = Dimensions.get("window");

class MyAccountScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    header: null,
    tabBarIcon: ({ tintColor }) => (
      <TabBarIcon
        navigation={navigation}
        style={{ width: 20, height: 20 }}
        icon={require("@assets/images/user.png")}
        label="Hesabım"
        tintColor={tintColor}
      />
    ),
  });

  constructor(props) {
    super(props);
    const scrollAnim = new Animated.Value(0);
    const offsetAnim = new Animated.Value(0);
    this.state = {
      scrollAnim,
      clampedScroll: Animated.diffClamp(
        Animated.add(
          scrollAnim.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 1],
            extrapolateLeft: "clamp",
          }),
          offsetAnim,
        ),
        0,
        50,
      ),
    };
  }

  componentDidMount() {
    this.props.getAccounts();

    this.props.getAcikCekler("TRY");
    this.props.getAcikCekler("USD");
    this.props.getAcikCekler("EUR");

    this.props.getAcikKalemler("TRY");
    this.props.getAcikKalemler("USD");
    this.props.getAcikKalemler("EUR");

    this.props.getAcikKalemlerToplam();
  }

  logout = () => {
    this.props.logout().then(() => {
      console.log("logging out");
      this.props.navigation.navigate("Auth");
    });
  };

  renderHeader = () => {
    return (
      <View style={{ flexDirection: "column", alignItems: "center" }}>
        <View
          style={{
            flexDirection: "row",
            width: win.width * 0.94,
            alignItems: "center",
            justifyContent: "space-between",
            marginVertical: moderateScale(10, 0.5),
          }}
        >
          <View style={{ flexDirection: "column", marginRight: 5, flex: 1 }}>
            <Text
              fontScale={0.5}
              ellipsizeMode="tail"
              numberOfLines={1}
              style={{
                fontFamily: "Poppins",
                fontSize: 13,
                color: "#4a4a4a",
                flex: 1,
                flexWrap: "wrap",
              }}
            >
              {this.props.session.info.BayiAdi || ""}
            </Text>
            <Text
              fontScale={0.5}
              style={{ fontFamily: "Poppins", fontSize: 13, color: "#4a4a4a" }}
            >{`${this.props.session.info.Ad || ""} ${this.props.session.info.Soyad || ""}`}</Text>
          </View>
          <TouchableOpacity
            testID="button_logout"
            onPress={this.logout}
            style={{
              borderWidth: 1,
              borderColor: "#bababa",
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "center",
              borderRadius: 4,
              backgroundColor: "white",
              paddingHorizontal: moderateScale(12, 0.5),
              paddingVertical: moderateScale(4, 0.5),
            }}
          >
            <Text
              fontScale={0.5}
              style={{
                fontFamily: "Poppins-Medium",
                fontSize: 12,
                lineHeight: 14,
                marginTop: 3,
                color: "gray",
              }}
            >
              Çıkış Yap
            </Text>
            <Image
              source={require("@assets/images/door.png")}
              style={{
                height: 14,
                transform: [{ rotateZ: "0deg" }],
                width: 14,
                tintColor: "#7a7a7a",
                resizeMode: "contain",
                marginLeft: moderateScale(5, 0.5),
              }}
            />
          </TouchableOpacity>
        </View>
        {/* <TouchableOpacity style={styles.itemContainer} onPress={() => this.props.navigation.navigate('UserManagementScreen')} >
                    <Text fontScale={.5} style={styles.itemTitle} >Kullanıcı Yönetimi</Text>
                    <Image source={require('@assets/images/arrow-right.png')} style={styles.arrow} />
                </TouchableOpacity> */}
        {!this.props.session.info.SadeceUlusalPB && this.props.homePage.info.GununKuru && (
          <View
            style={{
              flexDirection: "row",
              width: win.width * 0.94,
              marginHorizontal: win.width * 0.03,
              backgroundColor: "white",
              alignItems: "center",
              justifyContent: "space-between",
              height: moderateScale(40, 0.5),
              borderRadius: 4,
              marginTop: -5,
            }}
          >
            <View style={{ flexDirection: "row" }}>
              <Text fontScale={0.5} style={{ fontFamily: "Poppins", fontSize: 12, color: "gray" }}>
                Döviz Kurları:
              </Text>
            </View>
            <View style={{ flexDirection: "row" }}>
              <Text fontScale={0.5} style={{ fontFamily: "Poppins", fontSize: 12, color: "gray" }}>
                {"USD: " + this.props.homePage.info.GununKuru.UsdKur + "  "}
              </Text>
              <Text fontScale={0.5} style={{ fontFamily: "Poppins", fontSize: 12, color: "gray" }}>
                {"EUR: " + this.props.homePage.info.GununKuru.EuroKur}
              </Text>
            </View>
          </View>
        )}

        <TouchableOpacity
          style={styles.itemContainer}
          onPress={() => this.props.navigation.navigate("OrdersScreen")}
        >
          <Text fontScale={0.5} style={styles.itemTitle}>
            Siparişlerim
          </Text>
          <Image source={require("@assets/images/arrow-right.png")} style={styles.arrow} />
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.itemContainer}
          onPress={() => this.props.navigation.navigate("HesapEkstresiScreen")}
        >
          <Text fontScale={0.5} style={styles.itemTitle}>
            Hesap Ekstresi
          </Text>
          <Image source={require("@assets/images/arrow-right.png")} style={styles.arrow} />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate("InvoicesListScreen")}
          style={styles.itemContainer}
        >
          <Text fontScale={0.5} style={styles.itemTitle}>
            Fatura Listesi
          </Text>
          <Image source={require("@assets/images/arrow-right.png")} style={styles.arrow} />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() =>
            this.props.navigation.navigate({
              routeName: "PriceRequestsScreen",
              key: "myAccount",
              params: { showAlert: false },
            })
          }
          style={styles.itemContainer}
        >
          <Text fontScale={0.5} style={styles.itemTitle}>
            Fiyat Tekliflerim
          </Text>
          <Image source={require("@assets/images/arrow-right.png")} style={styles.arrow} />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate("CambiumScreen")}
          style={styles.itemContainer}
        >
          <Text fontScale={0.5} style={styles.itemTitle}>
            Kambiyo İşlemleri
          </Text>
          <Image source={require("@assets/images/arrow-right.png")} style={styles.arrow} />
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.itemContainer}
          onPress={() => this.props.navigation.navigate("AcikKalemlerScreen")}
        >
          <Text fontScale={0.5} style={styles.itemTitle}>
            Açık Kalemler
          </Text>
          <Image source={require("@assets/images/arrow-right.png")} style={styles.arrow} />
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.itemContainer}
          onPress={() => this.props.navigation.navigate("AcikCeklerScreen")}
        >
          <Text fontScale={0.5} style={styles.itemTitle}>
            Açık Çekler
          </Text>
          <Image source={require("@assets/images/arrow-right.png")} style={styles.arrow} />
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.itemContainer}
          onPress={() => this.props.navigation.navigate("GarantiScreen")}
        >
          <Text fontScale={0.5} style={styles.itemTitle}>
            Garanti Süresi Sorgulama
          </Text>
          <Image source={require("@assets/images/arrow-right.png")} style={styles.arrow} />
        </TouchableOpacity>
        {/* <TouchableOpacity style={styles.itemContainer}>
                    <Text fontScale={.5} style={styles.itemTitle} >Arızalı Ürün Takip</Text>
                    <Image source={require('@assets/images/arrow-right.png')} style={styles.arrow} />
                </TouchableOpacity> */}
        <TouchableOpacity
          style={styles.itemContainer}
          onPress={() => this.props.navigation.navigate("SettingsScreen")}
        >
          <Text fontScale={0.5} style={styles.itemTitle}>
            Kişisel Ayarlar
          </Text>
          <Image source={require("@assets/images/arrow-right.png")} style={styles.arrow} />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate("MyAccountOthersScreen")}
          style={styles.itemContainer}
        >
          <Text fontScale={0.5} style={styles.itemTitle}>
            Diğer İşlemler
          </Text>
          <Image source={require("@assets/images/arrow-right.png")} style={styles.arrow} />
        </TouchableOpacity>
      </View>
    );
  };

  render() {
    return (
      <SafeAreaView style={{ backgroundColor: colors.primary_dark, flex: 1 }}>
        <View style={{ flexGrow: 1, width: win.width, backgroundColor: "white" }}>
          <AnimatedScrollView
            onScroll={Animated.event(
              [{ nativeEvent: { contentOffset: { y: this.state.scrollAnim } } }],
              {
                useNativeDriver: true,
              },
            )}
            scrollIndicatorInsets={{
              top: AnimatedHeader.height,
              left: 0,
              right: 0,
            }}
            scrollEventThrottle={16}
            overScrollMode="never"
            contentContainerStyle={{
              alignItems: "center",
              flexGrow: 1,
              paddingTop:
                Platform.OS === "android"
                  ? AnimatedHeader.height + (Platform.Version < 20 ? 0 : StatusBar.currentHeight)
                  : AnimatedHeader.height,
            }}
            renderItem={this.renderItem}
          >
            {this.renderHeader()}
          </AnimatedScrollView>
        </View>
        <AnimatedHeader
          navigation={this.props.navigation}
          clampedScroll={this.state.clampedScroll}
          back={false}
          alignLeft={false}
          searchBar={true}
        />
      </SafeAreaView>
    );
  }
}

const styles = ScaledSheet.create({
  itemContainer: {
    flexDirection: "row",
    width: win.width * 0.94,
    paddingHorizontal: moderateScale(15, 0.5),
    alignItems: "center",
    justifyContent: "space-between",
    backgroundColor: "#f3f3f3",
    borderRadius: 3,
    marginBottom: moderateScale(10, 0.5),
    height: "50@ms0.5",
  },
  itemTitle: {
    fontSize: 13,
    lineHeight: 16,
    marginTop: 3,
    fontFamily: "Poppins-Medium",
  },
  arrow: {
    width: "8@ms0.5",
    height: "8@ms0.5",
    tintColor: "black",
  },
});

function mapStateToProps(state) {
  return {
    accounts: state.accounts,
    session: state.session,
    homePage: state.homePage,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    getAccounts: () => dispatch(fetchAccountsFromApi()),
    getAcikCekler: (curr) => dispatch(fetchAcikCeklerFromApi(curr)),
    getAcikKalemler: (curr) => dispatch(fetchAcikKalemlerFromApi(curr)),
    getAcikKalemlerToplam: () => dispatch(fetchAcikKalemlerTotal()),
    logout: () => dispatch(logout()),
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(MyAccountScreen);
