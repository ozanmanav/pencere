import React, { Component } from "react";
import {
  View,
  StatusBar,
  SafeAreaView,
  TouchableOpacity,
  Animated,
  StyleSheet,
  FlatList,
  Dimensions,
  Platform,
  Image,
} from "react-native";
import { AnimatedHeader, Text, Card, BottomPicker, PencereIndicator } from "@components";
import { connect } from "react-redux";
import colors from "@common/styles/colors";
import { getDate, getLastNMonths, getMonthYearString } from "@common/Utils";
import { fetchAllHesapEkstresi } from "@redux/actions";
import { moderateScale } from "react-native-size-matters";

const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);

const win = Dimensions.get("window");

class HesapEkstresiScreen extends Component {
  constructor(props) {
    super(props);
    const scrollAnim = new Animated.Value(0);
    const offsetAnim = new Animated.Value(0);
    this.state = {
      selectedDateIndex: 0,
      dates: [],
      data: [],
      seg: 0,
      scrollAnim,
      clampedScroll: Animated.diffClamp(
        Animated.add(
          scrollAnim.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 1],
            extrapolateLeft: "clamp",
          }),
          offsetAnim,
        ),
        0,
        50,
      ),
    };
    this.datesRaw = getLastNMonths(12);
  }

  componentDidMount() {
    this.state.scrollAnim.addListener(({ value }) => {
      // This is the same calculations that diffClamp does.
      this.diff = value - this._scrollValue;
      this._scrollValue = value;
      // if (this.diff > 0 && value > (dim.width * 9 / 16 + 50))
      //     this.refs.fixedBtn.startFadeAnim(false);
      // else if (this.refs.fixedBtn.state.isArrowVisible)
      //     this.refs.fixedBtn.startFadeAnim(true);

      this._clampedScrollValue = Math.min(Math.max(this._clampedScrollValue + this.diff, 0), 50);
    });
    this.props.getAllHesapEkstresi(
      this.datesRaw[0].month(),
      this.datesRaw[0].month(),
      this.datesRaw[0].year(),
    );
  }

  renderItem = ({ item, index }) => {
    return (
      <Card
        onPress={() => this.props.navigation.push("HesapEkstresiDetailsScreen", { item })}
        renderContent={() => {
          return (
            <View style={{ flexDirection: "row", padding: 10 }}>
              <View
                style={{
                  flex: 1,
                  backgroundColor: "transparent",
                  flexDirection: "column",
                }}
              >
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "space-between",
                  }}
                >
                  <Text fontScale={0.5} style={styles.titleTxt}>
                    {"Belge No"}
                  </Text>
                  <Text fontScale={0.5} numberOfLines={1} style={styles.valueTxt}>
                    {item.BelgeNo}
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "space-between",
                  }}
                >
                  <Text fontScale={0.5} style={styles.titleTxt}>
                    {"Belge Tipi"}
                  </Text>
                  <Text fontScale={0.5} numberOfLines={1} style={styles.valueTxt}>
                    {item.BelgeTipi}
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "space-between",
                  }}
                >
                  <Text fontScale={0.5} style={styles.titleTxt}>
                    {"İşlem Açıklaması"}
                  </Text>
                  <Text fontScale={0.5} numberOfLines={1} style={styles.valueTxt}>
                    {item.IslemAciklamasi}
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "space-between",
                  }}
                >
                  <Text fontScale={0.5} style={styles.titleTxt}>
                    {"Ödeme Tarihi"}
                  </Text>
                  <Text fontScale={0.5} numberOfLines={1} style={styles.valueTxt}>
                    {getDate(item.OdemeTarihi)}
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "space-between",
                  }}
                >
                  <Text fontScale={0.5} style={styles.titleTxt}>
                    {"Kayıt Tarihi"}
                  </Text>
                  <Text fontScale={0.5} numberOfLines={1} style={styles.valueTxt}>
                    {getDate(item.KayitTarihi)}
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "space-between",
                  }}
                >
                  <Text fontScale={0.5} style={styles.titleTxt}>
                    {"Alacak Tutarı"}
                  </Text>
                  <Text fontScale={0.5} numberOfLines={1} style={styles.valueTxt}>
                    {item.AlacakTutari + " " + item.ParaBirimi}
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "space-between",
                  }}
                >
                  <Text fontScale={0.5} style={[styles.titleTxt, { marginBottom: 0 }]}>
                    {"Bakiye Tutarı"}
                  </Text>
                  <Text
                    fontScale={0.5}
                    numberOfLines={1}
                    style={[styles.valueTxt, { marginBottom: 0 }]}
                  >
                    {item.BakiyeTutari + " " + item.ParaBirimi}
                  </Text>
                </View>
              </View>
            </View>
          );
        }}
      />
    );
  };

  renderHeader = (navbarTranslate) => {
    return (
      <Animated.View style={[styles.segment, { transform: [{ translateY: navbarTranslate }] }]}>
        <TouchableOpacity
          activeOpacity={1}
          style={this.state.seg === 0 ? styles.activeSegmentButton : styles.inactiveSegmentButton}
          onPress={() => this.setState({ seg: 0 })}
        >
          <Text
            fontScale={0.5}
            style={this.state.seg === 0 ? styles.activeSegmentText : styles.inactiveSegmentText}
          >
            TL
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          activeOpacity={1}
          style={this.state.seg === 1 ? styles.activeSegmentButton : styles.inactiveSegmentButton}
          onPress={() => this.setState({ seg: 1 })}
        >
          <Text
            fontScale={0.5}
            style={this.state.seg === 1 ? styles.activeSegmentText : styles.inactiveSegmentText}
          >
            EURO
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          activeOpacity={1}
          style={this.state.seg === 2 ? styles.activeSegmentButton : styles.inactiveSegmentButton}
          onPress={() => this.setState({ seg: 2 })}
        >
          <Text
            fontScale={0.5}
            style={this.state.seg === 2 ? styles.activeSegmentText : styles.inactiveSegmentText}
          >
            USD
          </Text>
        </TouchableOpacity>
      </Animated.View>
    );
  };

  renderNoContent = () => {
    return (
      <View
        style={{
          paddingTop: 10,
          height: 130,
          width: win.width,
          alignItems: "center",
        }}
      >
        <Text fontScale={0.5} style={{ fontSize: 16, color: "black" }}>
          Hesap ekstresi kaydı bulanamadı.
        </Text>
      </View>
    );
  };

  selectDate = (selectedVal, selectedDateIndex) => {
    this.setState({
      selectedDateIndex,
    });
    console.log(
      "selected",
      this.datesRaw,
      this.datesRaw[selectedDateIndex].month(),
      selectedDateIndex,
    );
    this.props.getAllHesapEkstresi(
      this.datesRaw[selectedDateIndex].month(),
      this.datesRaw[selectedDateIndex].month(),
      this.datesRaw[selectedDateIndex].year(),
    );
  };

  renderPicker = (navbarTranslate) => {
    return (
      <Animated.View
        style={[
          {
            position: "absolute",
            width: win.width,
            height: moderateScale(30, 0.4),
            top:
              moderateScale(80, 0.4) +
              (Platform.OS === "android"
                ? Platform.Version < 20
                  ? 0
                  : StatusBar.currentHeight
                : 0),
            left: 0,
            elevation: 1,
            transform: [{ translateY: navbarTranslate }],
          },
        ]}
      >
        {
          <TouchableOpacity
            style={{
              flexDirection: "row",
              justifyContent: "space-between",
              width: win.width,
              paddingHorizontal: moderateScale(10, 0.5),
              backgroundColor: "white",
              position: "absolute",
              height: moderateScale(30, 0.4),
              shadowColor: "#000",
              shadowOffset: { width: 0, height: 2 },
              shadowOpacity: 0.3,
              shadowRadius: 1,
              elevation: 1,
              alignItems: "center",
            }}
            onPress={() => this.bottomPicker.openPicker(null, null, Platform.OS === "android")}
          >
            <View style={{ flexDirection: "row", alignItems: "center" }}>
              <Text fontScale={0.5} style={{ fontSize: 16, color: "gray" }}>
                {"Tarih Seçin:   " +
                  getMonthYearString(this.datesRaw[this.state.selectedDateIndex])}
              </Text>
              <Image
                style={{
                  width: 16,
                  height: 16,
                  resizeMode: "contain",
                  tintColor: "gray",
                  marginHorizontal: 5,
                  marginTop: 2,
                }}
                source={require("@assets/images/down.png")}
              />
            </View>
            {/* <TouchableOpacity style={{ flexDirection: 'row', }} onPress={() => { this.setState({ showDatePanel: !this.state.showDatePanel }) }}>
                            <Image style={{ width: 16, height: 16, resizeMode: 'contain', tintColor: 'gray', marginHorizontal: 5, marginTop: 2, padding: 2 }} source={require('@assets/images/calendar.png')} />
                        </TouchableOpacity> */}
          </TouchableOpacity>
        }
      </Animated.View>
    );
  };

  render() {
    var data = [];
    switch (this.state.seg) {
      case 0:
        data = this.props.accounts.hesapEkstresiTRY.IlEkstreKalemleri;
        break;
      case 1:
        data = this.props.accounts.hesapEkstresiEUR.IlEkstreKalemleri;
        break;
      case 2:
        data = this.props.accounts.hesapEkstresiUSD.IlEkstreKalemleri;
        break;
    }
    const navbarTranslate = this.state.clampedScroll.interpolate({
      inputRange: [0, moderateScale(30, 0.4)],
      outputRange: [0, -moderateScale(30, 0.4)],
      extrapolate: "clamp",
    });
    return (
      <SafeAreaView
        style={{ backgroundColor: colors.primary_dark, flex: 1 }}
        forceInset={{ top: "always" }}
      >
        <View style={{ width: win.width, backgroundColor: "#efefef", flexGrow: 1 }}>
          {this.renderHeader(navbarTranslate)}
          {this.renderPicker(navbarTranslate)}
          <AnimatedFlatList
            style={{ zIndex: -1 }}
            contentOffset={{ y: -moderateScale(110, 0.4) }}
            contentInset={{ top: moderateScale(110, 0.4), left: 0, right: 0 }}
            contentContainerStyle={{
              flexGrow: 1,
              paddingTop:
                Platform.OS === "android"
                  ? moderateScale(110, 0.4) + (Platform.Version < 20 ? 0 : StatusBar.currentHeight)
                  : 0,
              paddingBottom: moderateScale(10, 0.5),
            }}
            data={data}
            renderItem={this.renderItem}
            scrollEventThrottle={16}
            ListEmptyComponent={this.renderNoContent}
            onScroll={Animated.event(
              [{ nativeEvent: { contentOffset: { y: this.state.scrollAnim } } }],
              {
                useNativeDriver: true,
              },
            )}
          />
          <BottomPicker
            ref={(c) => (this.bottomPicker = c)}
            items={this.datesRaw}
            labelExtractor={(d) => getMonthYearString(d)}
            initialItem={this.datesRaw[this.state.selectedDateIndex]}
            onPressOK={this.selectDate}
          />
        </View>
        {this.props.accounts.isFetchingAccounts && (
          <View style={[styles.loadingContainer]}>
            <PencereIndicator />
          </View>
        )}
        <AnimatedHeader
          navigation={this.props.navigation}
          title="Hesap Ekstresi"
          clampedScroll={this.state.clampedScroll}
          back={true}
          alignLeft={true}
        />
      </SafeAreaView>
    );
  }
}

function mapStateToProps(state) {
  return {
    accounts: state.accounts,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    getAllHesapEkstresi: (startMonth, endMonth, year) =>
      dispatch(fetchAllHesapEkstresi(startMonth, endMonth, year)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(HesapEkstresiScreen);

const styles = StyleSheet.create({
  segment: {
    flexDirection: "row",
    flex: 1,
    position: "absolute",
    top:
      moderateScale(50, 0.4) +
      (Platform.OS === "android" ? (Platform.Version < 20 ? 0 : StatusBar.currentHeight) : 0),
    backgroundColor: colors.primary_light,
    height: moderateScale(30, 0.4),
    alignItems: "center",
    left: 0,
    width: win.width,
  },
  activeSegmentButton: {
    justifyContent: "center",
    borderColor: colors.primary_light,
    backgroundColor: colors.primary_light,
    borderWidth: 1,
    height: 30,
    flex: 0.5,
    flexDirection: "column",
  },
  inactiveSegmentButton: {
    justifyContent: "center",
    borderColor: colors.primary_light,
    backgroundColor: colors.primary_light,
    borderWidth: 1,
    width: 100,
    height: 30,
    flex: 0.5,
    flexDirection: "column",
  },
  activeSegmentText: {
    textAlign: "center",
    color: "white",
    fontSize: 16,
    fontWeight: "bold",
  },
  inactiveSegmentText: {
    textAlign: "center",
    color: "#e5e5e5",
    fontSize: 14,
    fontWeight: "bold",
  },
  titleTxt: {
    fontFamily: "Poppins",
    fontSize: 14,
    color: "orange",
    marginBottom: 2,
    flex: 0.5,
  },
  valueTxt: {
    fontFamily: "Poppins",
    fontSize: 14,
    color: "black",
    marginBottom: 2,
    flex: 0.5,
  },
  loadingContainer: {
    height:
      win.height -
      (Platform.OS === "android" ? 50 + (Platform.Version < 20 ? 0 : StatusBar.currentHeight) : 50),
    width: win.width,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0,0,0,0.3)",
    position: "absolute",
    top:
      Platform.OS === "android" ? 50 + (Platform.Version < 20 ? 0 : StatusBar.currentHeight) : 50,
  },
});
