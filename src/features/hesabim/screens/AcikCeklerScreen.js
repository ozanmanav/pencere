import React, { Component } from "react";
import {
  View,
  SafeAreaView,
  TouchableOpacity,
  Animated,
  StyleSheet,
  FlatList,
  Dimensions,
  Platform,
  StatusBar,
} from "react-native";
import { AnimatedHeader, Text, Card, PencereIndicator } from "@components";
import { connect } from "react-redux";
import colors from "@common/styles/colors";
import { getDate } from "@common/Utils";
import { moderateScale } from "react-native-size-matters";

const STATUSBAR_HEIGHT =
  Platform.OS === "android" ? (Platform.Version < 20 ? 0 : StatusBar.currentHeight) : 0;
const win = Dimensions.get("window");

class AcikCeklerScreen extends Component {
  constructor(props) {
    super(props);
    this.data = this.props.accounts.acikCeklerTRY;
    const scrollAnim = new Animated.Value(0);
    const offsetAnim = new Animated.Value(0);
    this.state = {
      seg: 0,
      scrollAnim,
      clampedScroll: Animated.diffClamp(
        Animated.add(
          scrollAnim.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 1],
            extrapolateLeft: "clamp",
          }),
          offsetAnim,
        ),
        0,
        50,
      ),
    };
  }

  renderItem = ({ item, index }) => {
    return (
      <Card
        onPress={() =>
          this.props.navigation.navigate({
            key: "AcikCekDetailsScreen",
            routeName: "AcikCekDetailsScreen",
            params: { item },
          })
        }
        renderContent={() => {
          return (
            <View style={{ flexDirection: "row", padding: moderateScale(10, 0.5) }}>
              <View style={{ flex: 0.5, backgroundColor: "transparent", flexDirection: "column" }}>
                <Text fontScale={0.5} style={styles.titleTxt}>
                  {"Belge No"}
                </Text>
                <Text fontScale={0.5} style={styles.titleTxt}>
                  {"Çek No"}
                </Text>
                <Text fontScale={0.5} style={styles.titleTxt}>
                  {"Banka / Şube Kodu"}
                </Text>
                <Text fontScale={0.5} style={styles.titleTxt}>
                  {"Hesap No"}
                </Text>
                <Text fontScale={0.5} style={styles.titleTxt}>
                  {"Kayıt Tarihi"}
                </Text>
                <Text fontScale={0.5} style={styles.titleTxt}>
                  {"Vade Tarihi"}
                </Text>
                <Text fontScale={0.5} style={[styles.titleTxt, { marginBottom: 0 }]}>
                  {"Tutar"}
                </Text>
              </View>
              <View style={{ flex: 0.5, backgroundColor: "transparent", flexDirection: "column" }}>
                <Text fontScale={0.5} numberOfLines={1} style={styles.valueTxt}>
                  {item.BelgeNo}
                </Text>
                <Text fontScale={0.5} numberOfLines={1} style={styles.valueTxt}>
                  {item.CekNo}
                </Text>
                <Text fontScale={0.5} numberOfLines={1} style={styles.valueTxt}>
                  {item.BankaKodu}
                </Text>
                <Text fontScale={0.5} numberOfLines={1} style={styles.valueTxt}>
                  {item.HesapNo}
                </Text>
                <Text fontScale={0.5} numberOfLines={1} style={styles.valueTxt}>
                  {getDate(item.KayitTarihi)}
                </Text>
                <Text fontScale={0.5} numberOfLines={1} style={styles.valueTxt}>
                  {getDate(item.VadeTarihi)}
                </Text>
                <Text
                  fontScale={0.5}
                  numberOfLines={1}
                  style={[styles.valueTxt, { marginBottom: 0 }]}
                >
                  {item.Tutar + " " + item.ParaBirimi}
                </Text>
              </View>
            </View>
          );
        }}
      />
    );
  };

  onChangeTab(tabIndex) {
    this.setState({ seg: tabIndex });
    switch (tabIndex) {
      case 0:
        this.data = this.props.accounts.acikCeklerTRY;
        break;
      case 1:
        this.data = this.props.accounts.acikCeklerUSD;
        break;
      case 2:
        this.data = this.props.accounts.acikCeklerEUR;
        break;
    }
  }

  renderHeader = () => {
    return (
      <View style={styles.segment}>
        <TouchableOpacity
          activeOpacity={1}
          style={this.state.seg === 0 ? styles.activeSegmentButton : styles.inactiveSegmentButton}
          onPress={() => this.onChangeTab(0)}
        >
          <Text
            fontScale={0.5}
            style={this.state.seg === 0 ? styles.activeSegmentText : styles.inactiveSegmentText}
          >
            TL
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          activeOpacity={1}
          style={this.state.seg === 1 ? styles.activeSegmentButton : styles.inactiveSegmentButton}
          onPress={() => this.onChangeTab(1)}
        >
          <Text
            fontScale={0.5}
            style={this.state.seg === 1 ? styles.activeSegmentText : styles.inactiveSegmentText}
          >
            EURO
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          activeOpacity={1}
          style={this.state.seg === 2 ? styles.activeSegmentButton : styles.inactiveSegmentButton}
          onPress={() => this.onChangeTab(2)}
        >
          <Text
            fontScale={0.5}
            style={this.state.seg === 2 ? styles.activeSegmentText : styles.inactiveSegmentText}
          >
            USD
          </Text>
        </TouchableOpacity>
      </View>
    );
  };

  renderNoContent = () => {
    return (
      <View
        style={{
          width: win.width,
          alignItems: "center",
          paddingTop: moderateScale(10, 0.5) + STATUSBAR_HEIGHT,
        }}
      >
        <Text fontScale={0.5} style={{ fontSize: 16, color: "gray" }}>
          Açık çek kaydı bulanamadı.
        </Text>
      </View>
    );
  };

  render() {
    return (
      <SafeAreaView
        style={{ backgroundColor: colors.primary_dark, flex: 1 }}
        forceInset={{ top: "always" }}
      >
        <View style={{ width: win.width, backgroundColor: "#efefef", flexGrow: 1 }}>
          {this.renderHeader()}
          <FlatList
            style={{ zIndex: -1 }}
            contentOffset={{ y: -moderateScale(80, 0.4) }}
            contentInset={{ top: moderateScale(80, 0.4), left: 0, bottom: 0, right: 0 }}
            contentContainerStyle={{
              paddingBottom: 10,
              paddingTop: Platform.OS === "android" ? moderateScale(80, 0.4) + 30 : 0,
              flexGrow: 1,
            }}
            data={this.data}
            renderItem={this.renderItem}
            ListEmptyComponent={this.renderNoContent}
          />
          <AnimatedHeader
            navigation={this.props.navigation}
            title="Açık Çekler"
            clampedScroll={this.state.clampedScroll}
            back={true}
            alignLeft={true}
          />
        </View>
        {this.props.accounts.isFetchingAccounts && (
          <View style={[styles.loadingContainer]}>
            <PencereIndicator />
          </View>
        )}
      </SafeAreaView>
    );
  }
}

function mapStateToProps(state) {
  return {
    accounts: state.accounts,
  };
}

export default connect(mapStateToProps, null)(AcikCeklerScreen);

const styles = StyleSheet.create({
  segment: {
    flexDirection: "row",
    flex: 1,
    position: "absolute",
    top: moderateScale(50, 0.4) + STATUSBAR_HEIGHT,
    left: 0,
    height: moderateScale(30, 0.4),
    backgroundColor: colors.primary_light,
    alignItems: "center",
    width: win.width,
  },
  activeSegmentButton: {
    justifyContent: "center",
    borderBottomColor: "white",
    backgroundColor: colors.primary_light,
    borderBottomWidth: 2,
    flex: 0.5,
    height: moderateScale(30, 0.4),
    flexDirection: "column",
  },
  loadingContainer: {
    height: win.height,
    width: win.width,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0,0,0,0.3)",
    position: "absolute",
  },
  inactiveSegmentButton: {
    justifyContent: "center",
    borderColor: colors.primary_light,
    backgroundColor: colors.primary_light,
    borderWidth: 1,
    width: 100,
    height: moderateScale(30, 0.4),
    flex: 0.5,
    flexDirection: "column",
  },
  activeSegmentText: {
    textAlign: "center",
    color: "white",
    fontSize: 16,
    fontWeight: "bold",
  },
  inactiveSegmentText: {
    textAlign: "center",
    color: "#e5e5e5",
    fontSize: 16,
    fontWeight: "bold",
  },
  titleTxt: {
    fontSize: 15,
    color: "orange",
    marginBottom: 5,
  },
  valueTxt: {
    fontSize: 15,
    color: "black",
    marginBottom: 5,
  },
});
