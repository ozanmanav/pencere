import React, { Component } from "react";
import {
  SafeAreaView,
  View,
  StyleSheet,
  Dimensions,
  ScrollView,
  Platform,
  StatusBar,
} from "react-native";
import { Text, AnimatedHeader, Card } from "@components";
import colors from "@common/styles/colors";
import { getDate } from "@common/Utils";
import { moderateScale } from "react-native-size-matters";

const win = Dimensions.get("window");

export default class AcikKalemDetailsScreen extends Component {
  render() {
    const item = this.props.navigation.state.params.item;
    return (
      <SafeAreaView
        style={{ backgroundColor: colors.primary_dark, flex: 1 }}
        forceInset={{ top: "always" }}
      >
        <View style={{ flexGrow: 1, width: win.width, backgroundColor: "#efefef" }}>
          <ScrollView
            contentOffset={{ y: -moderateScale(50, 0.4) }}
            contentInset={{ top: moderateScale(50, 0.4) }}
            style={{
              height: win.height,
              paddingTop:
                Platform.OS === "android"
                  ? moderateScale(50, 0.4) + (Platform.Version < 20 ? 0 : StatusBar.currentHeight)
                  : 0,
            }}
          >
            <Card
              renderContent={() => {
                return (
                  <View style={{ flexDirection: "column", padding: moderateScale(8, 0.4) }}>
                    <View>
                      <Text fontScale={0.5} style={styles.titleTxt}>
                        {"Belge No:"}
                      </Text>
                      <Text fontScale={0.5} style={styles.valueTxt}>
                        {item.BelgeNo !== "" ? item.BelgeNo : "Yok"}
                      </Text>
                    </View>
                    <View>
                      <Text fontScale={0.5} style={styles.titleTxt}>
                        {"Belge Tipi:"}
                      </Text>
                      <Text fontScale={0.5} style={styles.valueTxt}>
                        {item.BelgeTipiAciklamasi}
                      </Text>
                    </View>
                    <View>
                      <Text fontScale={0.5} style={styles.titleTxt}>
                        {"Geri Ödeme Mi?"}
                      </Text>
                      <Text fontScale={0.5} style={styles.valueTxt}>
                        {item.GeriOdemeMi ? "Evet" : "Hayır"}
                      </Text>
                    </View>
                    <View>
                      <Text fontScale={0.5} style={styles.titleTxt}>
                        {"Açıklama:"}
                      </Text>
                      <Text fontScale={0.5} style={styles.valueTxt}>
                        {item.Aciklama}
                      </Text>
                    </View>
                    <View>
                      <Text fontScale={0.5} style={styles.titleTxt}>
                        {"Kayıt Tarihi:"}
                      </Text>
                      <Text fontScale={0.5} style={styles.valueTxt}>
                        {getDate(item.KayitTarihi)}
                      </Text>
                    </View>
                    <View>
                      <Text fontScale={0.5} style={styles.titleTxt}>
                        {"Vade Tarihi:"}
                      </Text>
                      <Text fontScale={0.5} style={styles.valueTxt}>
                        {getDate(item.VadeTarihi)}
                      </Text>
                    </View>
                    <View>
                      <Text fontScale={0.5} style={styles.titleTxt}>
                        {"Para Birimi:"}
                      </Text>
                      <Text fontScale={0.5} style={styles.valueTxt}>
                        {item.ParaBirimi}
                      </Text>
                    </View>
                    <View>
                      <Text fontScale={0.5} style={styles.titleTxt}>
                        {"Tutar:"}
                      </Text>
                      <Text fontScale={0.5} style={styles.valueTxt}>
                        {item.Tutar}
                      </Text>
                    </View>
                  </View>
                );
              }}
            />
          </ScrollView>
        </View>
        <AnimatedHeader
          navigation={this.props.navigation}
          title="Açık Kalem Detayları"
          back={true}
          searchBar={false}
          alignLeft={true}
        />
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  titleTxt: {
    fontSize: 14,
    color: "orange",
    marginBottom: moderateScale(5, 0.4),
  },
  valueTxt: {
    fontSize: 14,
    color: "black",
    marginBottom: moderateScale(5, 0.4),
  },
});
