import React, { Component } from "react";
import {
  View,
  SafeAreaView,
  Dimensions,
  Alert,
  Keyboard,
  Platform,
  StatusBar,
  TouchableOpacity,
  TextInput,
} from "react-native";
import { AnimatedHeader, Text, Card } from "@components";
import colors from "@common/styles/colors";
import { ScaledSheet, moderateScale } from "react-native-size-matters";
const win = Dimensions.get("window");
import { connect } from "react-redux";
import Axios from "axios";
import bugsnag from "@common/bugsnag_config";
import { getEndPoint } from "@common/Utils";

class GarantiScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      garantiInfo: null,
      sNo: "",
      isFetching: false,
    };
  }

  fetchGaranti = () => {
    Keyboard.dismiss();
    this.setState({ isFetching: true });
    Axios.get(
      getEndPoint("GARANTI_URL") +
        "?si=" +
        this.props.session.info.SessionID +
        "&sno=" +
        this.state.sNo,
    )
      .then((responseJson) => {
        this.setState({ isFetching: false });
        if (responseJson.data.Mesaj !== "") {
          Alert.alert(responseJson.data.Mesaj);
        }
        this.setState({ garantiInfo: responseJson.data });
      })
      .catch((err) => {
        Alert.alert("Hata", "Beklenmeyen bir hata oluştu");
        console.log("garanti error", err);
        this.setState({ isFetching: false });
        bugsnag.notify(err, (report) => {
          report.errorClass = "emptyCart";
        });
      });
  };

  renderHeader = () => {
    return (
      <View style={styles.headerContainer}>
        <Text style={{ fontFamily: "Poppins", fontSize: 12, color: "gray" }}>
          Ürünlerinizin garanti sürelerini sorgulamak için ürününüzün seri numarasını aşağıdaki
          alana yazın ve aramayı başlatın.
        </Text>
        <TextInput
          placeholder="Seri Kodu Giriniz"
          placeholderTextColor="gray"
          style={{
            borderColor: "#cacaca",
            borderWidth: 1,
            borderRadius: 4,
            width: win.width - moderateScale(30, 0.5),
            marginVertical: moderateScale(10, 0.5),
            height: moderateScale(30, 0.5),
            padding: 0,
            textAlign: "center",
            backgroundColor: "white",
          }}
          value={this.state.sNo}
          onChangeText={(sNo) => this.setState({ sNo })}
        />
        <TouchableOpacity
          disabled={this.state.isFetching}
          onPress={() => this.fetchGaranti()}
          style={{
            backgroundColor: colors.primary_dark,
            borderRadius: 4,
            height: moderateScale(30, 0.5),
            width: win.width - moderateScale(30, 0.5),
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <Text
            style={{
              fontFamily: "Poppins-SemiBold",
              color: "white",
              fontSize: 14,
            }}
          >
            {this.state.isFetching ? "Sorgulanıyor..." : "Listele"}
          </Text>
        </TouchableOpacity>
        {this.state.garantiInfo && this.state.garantiInfo.SeriNo !== "" && (
          <Card
            containerStyle={{
              width: win.width - moderateScale(30, 0.5),
              padding: moderateScale(5, 0.5),
            }}
            renderContent={() => {
              return (
                <View style={{ flexDirection: "column" }}>
                  {this.state.garantiInfo?.SeriNo != null && (
                    <View style={{ flexDirection: "row" }}>
                      <Text style={styles.title}>Seri No: </Text>
                      <Text style={styles.value}>{this.state.garantiInfo.SeriNo}</Text>
                    </View>
                  )}
                  {this.state.garantiInfo?.UrunKodu != null && (
                    <View style={{ flexDirection: "row" }}>
                      <Text style={styles.title}>Ürün Kodu: </Text>
                      <Text style={styles.value}>{this.state.garantiInfo.UrunKodu}</Text>
                    </View>
                  )}
                  {this.state.garantiInfo?.UrunAciklama != null && (
                    <View style={{ flexDirection: "row" }}>
                      <Text style={styles.title}>Ürün Açıklaması: </Text>
                      <Text style={styles.value}>{this.state.garantiInfo.UrunAciklama}</Text>
                    </View>
                  )}
                  {this.state.garantiInfo?.GarantiBitisTarihi != null && (
                    <View style={{ flexDirection: "row" }}>
                      <Text style={styles.title}>Garanti Sonu: </Text>
                      <Text style={styles.value}>{this.state.garantiInfo.GarantiBitisTarihi}</Text>
                    </View>
                  )}
                </View>
              );
            }}
          />
        )}
      </View>
    );
  };

  render() {
    return (
      <SafeAreaView style={{ backgroundColor: colors.primary_dark, flex: 1 }}>
        <View
          style={{
            flexGrow: 1,
            width: win.width,
            backgroundColor: "#efefef",
            paddingTop:
              AnimatedHeader.height +
              moderateScale(10, 0.4) +
              (Platform.OS === "android"
                ? Platform.Version < 20
                  ? 0
                  : StatusBar.currentHeight
                : 0),
          }}
        >
          {this.renderHeader()}
        </View>
        <AnimatedHeader
          title="Garanti Süresi Sorgulama"
          navigation={this.props.navigation}
          alignLeft={true}
          back={true}
          hideIcons={true}
          searchBar={true}
          onBackPressed={() => this.props.navigation.navigate("MyAccountScreen")}
        />
      </SafeAreaView>
    );
  }
}

const styles = ScaledSheet.create({
  headerContainer: {
    flexDirection: "column",
    width: win.width,
    justifyContent: "center",
    alignItems: "center",
    paddingHorizontal: moderateScale(10, 0.5),
  },
  title: {
    fontFamily: "Poppins-Medium",
    fontSize: 12,
    color: colors.price_color,
  },
  value: {
    fontFamily: "Poppins-Medium",
    fontSize: 12,
    color: "gray",
  },
});

export function mapStateToProps(state) {
  return {
    session: state.session,
  };
}

export default connect(mapStateToProps, null)(GarantiScreen);
