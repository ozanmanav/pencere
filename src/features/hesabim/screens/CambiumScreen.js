import React, { Component } from "react";
import {
  View,
  SafeAreaView,
  TouchableOpacity,
  Animated,
  StyleSheet,
  LayoutAnimation,
  FlatList,
  Alert,
  Dimensions,
  Platform,
  StatusBar,
  Image,
} from "react-native";
import { AnimatedHeader, Text, Card, PencereIndicator } from "@components";
import { CambiumItem, CurrencyModal } from "@features/hesabim/components";
import { connect } from "react-redux";
import colors from "@common/styles/colors";
import { fetchCambiumPreviews, fetchCambiums, doCambium } from "@redux/actions";
import { CREATE_SPRINGXY } from "@common/LayoutAnimations";
import { BallIndicator } from "react-native-indicators";
import { moderateScale } from "react-native-size-matters";

const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);

const win = Dimensions.get("window");

class CambiumScreen extends Component {
  bNoList = "";
  constructor(props) {
    super(props);
    const scrollAnim = new Animated.Value(0);
    const offsetAnim = new Animated.Value(0);
    this.state = {
      cambiumTotalVisible: false,
      fadeAnim: new Animated.Value(0),
      seg: "TRY",
      spb: "Para birimi seçiniz",
      scrollAnim,
      addedDocs: [],
      data: [],
      clampedScroll: Animated.diffClamp(
        Animated.add(
          scrollAnim.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 1],
            extrapolateLeft: "clamp",
          }),
          offsetAnim,
        ),
        0,
        50,
      ),
    };
  }

  componentDidMount() {
    this.props.fetchCambiums(false, "TRY");
    this.props.fetchCambiums(false, "USD");
    this.props.fetchCambiums(false, "EUR");
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      this.props.cambiums.itemsTRY !== prevProps.cambiums.itemsTRY &&
      this.state.data.length === 0
    ) {
      this.setState({ data: this.props.cambiums.itemsTRY });
    }
  }

  renderItem = ({ item }) => (
    <Card
      renderContent={() => {
        return (
          <CambiumItem
            item={item}
            addToCambium={this.addToCambium}
            removeFromCambium={this.removeFromCambium}
            isAdded={this.state.addedDocs.filter(b => b === item.belgeNo).length > 0}
          />
        );
      }}
    />
  );

  addToCambium = belgeNo => {
    this.setState({ addedDocs: this.state.addedDocs.concat(belgeNo) }, () => {
      this.getCambiumPreview();
      if (this.state.addedDocs.length === 1) {
        this.zoomIn(300);
      }
    });
  };

  removeFromCambium = belgeNo => {
    let filteredList = this.state.addedDocs.filter(bn => bn !== belgeNo);
    this.setState({ addedDocs: filteredList }, () => {
      this.getCambiumPreview();
      if (this.state.addedDocs.length === 0) {
        this.zoomOut(300);
      }
    });
  };

  getCambiumPreview = () => {
    var bNoList = "";
    this.state.addedDocs.forEach((doc, index) => {
      bNoList += doc;
      if (index < this.state.addedDocs.length - 1) bNoList += "|";
    });
    this.props.fetchCambiumPreviews(this.state.seg, this.state.spb, bNoList);
  };

  onChangeTab(currency) {
    this.setState({ seg: currency });
    switch (currency) {
      case "TRY":
        this.setState({ data: this.props.cambiums.itemsTRY });
        break;
      case "EUR":
        this.setState({ data: this.props.cambiums.itemsEUR });
        break;
      case "USD":
        this.setState({ data: this.props.cambiums.itemsUSD });
        break;
    }
    this.cancelAll();
    LayoutAnimation.configureNext(CREATE_SPRINGXY);
  }

  renderHeader = () => {
    return (
      <View style={styles.segment}>
        <TouchableOpacity
          activeOpacity={1}
          style={
            this.state.seg === "TRY" ? styles.activeSegmentButton : styles.inactiveSegmentButton
          }
          onPress={() => {
            this.onChangeTab("TRY");
          }}
        >
          <Text
            style={this.state.seg === "TRY" ? styles.activeSegmentText : styles.inactiveSegmentText}
          >
            TL
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          activeOpacity={1}
          style={
            this.state.seg === "EUR" ? styles.activeSegmentButton : styles.inactiveSegmentButton
          }
          onPress={() => {
            this.onChangeTab("EUR");
          }}
        >
          <Text
            style={this.state.seg === "EUR" ? styles.activeSegmentText : styles.inactiveSegmentText}
          >
            EURO
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          activeOpacity={1}
          style={
            this.state.seg === "USD" ? styles.activeSegmentButton : styles.inactiveSegmentButton
          }
          onPress={() => {
            this.onChangeTab("USD");
          }}
        >
          <Text
            style={this.state.seg === 2 ? styles.activeSegmentText : styles.inactiveSegmentText}
          >
            USD
          </Text>
        </TouchableOpacity>
      </View>
    );
  };

  renderNoContent = () => {
    if (this.props.cambiums.isFetching) return null;
    return (
      <View style={{ paddingTop: 40, width: win.width, alignItems: "center" }}>
        <Text style={{ fontSize: 16, color: "black", textAlign: "center" }}>
          Kambiyo işlemi için uygun açık kalem kaydı bulanamadı.
        </Text>
      </View>
    );
  };

  doCambium = () => {
    if (this.state.spb.includes("seç")) {
      Alert.alert(
        "Para birimi seçiniz",
        "Kambiyo işlemi yapmadan önce çevirim yapacağınız para birimini seçmeniz gerekir.",
      );
    } else {
      this.bNoList = "";
      this.state.addedDocs.forEach((doc, index) => {
        this.bNoList += doc;
        if (index < this.state.addedDocs.length - 1) this.bNoList += "|";
      });
      this.props
        .doCambium(this.state.seg, this.state.spb, this.bNoList)
        .then(msg => {
          this.setState({ addedDocs: [] }, () => {
            this.getCambiumPreview();
            this.zoomOut(300);
            this.props.fetchCambiums(true, this.state.seg);
            this.props.fetchCambiums(true, this.state.spb);
          });
          Alert.alert("Sonuç", msg);
        })
        .catch(err => {
          console.log(err);
          Alert.alert("Hata", "Bir hata oluştu. Lütfen daha sonra tekrar deneyiniz.");
        });
    }
  };

  zoomIn = d => {
    this.setState({ cambiumTotalVisible: true });
    Animated.timing(this.state.fadeAnim, {
      toValue: 1,
      useNativeDriver: true,
      duration: d,
    }).start();
  };

  zoomOut = d => {
    Animated.timing(this.state.fadeAnim, {
      toValue: 0,
      useNativeDriver: true,
      duration: d,
    }).start(() => this.setState({ cambiumTotalVisible: false }));
  };

  cancelAll = () => {
    this.setState({ addedDocs: [] });
    this.getCambiumPreview();
    this.zoomOut(300);
  };

  renderCambiumTotal = () => {
    return (
      <Animated.View
        ref={c => (this.cambiumTotalView = c)}
        style={{
          transform: [{ scaleY: this.state.fadeAnim }],
          position: "absolute",
          bottom: 0,
          width: win.width,
          opacity: this.state.fadeAnim,
          flexDirection: "column",
          backgroundColor: "white",
          paddingTop: 15,
          borderTopColor: "#ddd",
          borderTopWidth: 1,
        }}
      >
        <View style={{ paddingHorizontal: this.state.cambiumTotalVisible ? 6 : 0 }}>
          <Text style={{ marginBottom: 10 }}>
            <Text style={{ fontFamily: "Poppins", color: colors.price_color }}>{"Tutar: "}</Text>
            {!this.props.cambiums.isFetchingPreview && (
              <Text style={{ fontFamily: "Poppins", color: "#444" }}>
                {this.props.cambiums.preview.toplamTutar +
                  " " +
                  this.props.cambiums.preview.paraBirimi}
              </Text>
            )}
          </Text>
          <Text style={{ marginBottom: 10 }}>
            <Text style={{ fontFamily: "Poppins", color: colors.price_color }}>
              {"Çevrim Tutarı: "}
            </Text>
            {!this.props.cambiums.isFetchingPreview && (
              <Text style={{ fontFamily: "Poppins", color: "#444" }}>
                {this.props.cambiums.preview.cevrimTutari +
                  " " +
                  (this.state.spb.includes("birimi")
                    ? ""
                    : this.props.cambiums.preview.seciliParaBirimi)}
              </Text>
            )}
          </Text>
          <View style={{ flexDirection: "row", alignItems: "center", height: 28 }}>
            <Text style={{ fontFamily: "Poppins", color: colors.price_color }}>
              {"Dönüştürülecek Para Birimi: "}
            </Text>
            {!this.props.cambiums.isFetchingPreview && (
              <TouchableOpacity
                onPress={() => this.currencyModal.toggleVisibility()}
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  paddingHorizontal: 5,
                  paddingVertical: 3,
                  borderRadius: 3,
                  borderWidth: 1,
                  borderColor: "#e4e4e4",
                }}
              >
                <Text style={{ fontFamily: "Poppins", color: "#444" }}>
                  {this.state.spb.includes("birimi")
                    ? "Seçiniz"
                    : this.props.cambiums.preview.seciliParaBirimi}{" "}
                </Text>
                <Image
                  source={require("@assets/images/down-arrow.png")}
                  style={{ width: 12, height: 12, tintColor: "#cecece" }}
                />
              </TouchableOpacity>
            )}
          </View>
        </View>
        <View
          style={{
            flexDirection: "row",
            flex: 1,
            marginTop: 8,
            paddingVertical: 10,
            borderTopColor: "#ddd",
            borderTopWidth: 1,
            justifyContent: "space-evenly",
            alignItems: "center",
          }}
        >
          <TouchableOpacity
            onPress={this.cancelAll}
            style={{
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: "#d6421e",
              padding: 8,
              width: win.width * 0.3,
              borderRadius: 3,
            }}
          >
            <Text style={{ color: "white", fontFamily: "Poppins-SemiBold" }}>İptal</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={this.doCambium}
            style={{
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: "#5eb139",
              padding: 8,
              width: win.width * 0.3,
              borderRadius: 3,
            }}
          >
            <Text style={{ color: "white", fontFamily: "Poppins-SemiBold" }}>Onayla</Text>
          </TouchableOpacity>
        </View>
        {(this.props.cambiums.isFetchingPreview || this.props.cambiums.isFetching) && (
          <BallIndicator
            size={moderateScale(24, 0.5)}
            color="#424242"
            style={{ position: "absolute", top: 5, right: 5 }}
          />
        )}
      </Animated.View>
    );
  };

  render() {
    console.log(
      "added docs",
      this.state.addedDocs,
      this.state.spb,
      this.props.cambiums,
      this.state.data,
    );
    var currenciesToPick = [];
    switch (this.state.seg) {
      case "USD":
        currenciesToPick = ["TRY", "EUR"];
        break;
      case "TRY":
        currenciesToPick = ["USD", "EUR"];
        break;
      case "EUR":
        currenciesToPick = ["USD", "TRY"];
        break;
    }
    return (
      <SafeAreaView
        style={{ backgroundColor: colors.primary_dark, flex: 1 }}
        forceInset={{ top: "always" }}
      >
        <View style={{ flexGrow: 1, width: win.width, backgroundColor: "#efefef" }}>
          {this.renderHeader()}
          <AnimatedFlatList
            style={{ zIndex: -1 }}
            contentOffset={{ y: -moderateScale(80, 0.4) }}
            contentInset={{
              top: moderateScale(80, 0.4),
              left: 0,
              bottom: 0,
              right: 0,
            }}
            contentContainerStyle={{
              flexGrow: 1,
              backgroundColor: "transparent",
              paddingBottom: 10 + (this.state.addedDocs.length > 0 ? 150 : 0),
              paddingTop:
                Platform.OS === "android"
                  ? 80 + (Platform.Version < 20 ? 0 : StatusBar.currentHeight)
                  : 0,
            }}
            data={this.state.data}
            keyExtractor={item => item.belgeNo}
            renderItem={this.renderItem}
            ListEmptyComponent={this.renderNoContent}
          />
        </View>
        {(this.props.accounts.isFetchingAccounts ||
          this.props.cambiums.isFetching ||
          this.props.cambiums.isFetchingPreview) && (
          <View style={[styles.loadingContainer]}>
            <PencereIndicator size="large" color="#0000ff" />
          </View>
        )}

        <CurrencyModal
          ref={c => (this.currencyModal = c)}
          items={currenciesToPick}
          initialItem={"Para birimi seçiniz"}
          hideTitle={true}
          updateCurrency={pb => {
            this.setState({ spb: pb }, () => this.getCambiumPreview());
          }}
        />

        {this.renderCambiumTotal()}

        <AnimatedHeader
          navigation={this.props.navigation}
          title="Kambiyo İşlemleri"
          clampedScroll={this.state.clampedScroll}
          back={true}
          alignLeft={true}
        />
      </SafeAreaView>
    );
  }
}

function mapStateToProps(state) {
  return {
    cambiums: state.cambiums,
    accounts: state.accounts,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    fetchCambiums: (refresh, pb) => dispatch(fetchCambiums(refresh, pb)),
    fetchCambiumPreviews: (pb, spb, bn) => dispatch(fetchCambiumPreviews(pb, spb, bn)),
    doCambium: (pb, spb, bn) => dispatch(doCambium(pb, spb, bn)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(CambiumScreen);

const styles = StyleSheet.create({
  segment: {
    flexDirection: "row",
    backgroundColor: colors.primary_light,
    height: moderateScale(30, 0.4),
    width: win.width,
    position: "absolute",
    top:
      moderateScale(50, 0.4) +
      (Platform.OS === "android" ? (Platform.Version < 20 ? 0 : StatusBar.currentHeight) : 0),
    left: 0,
  },
  loadingContainer: {
    height: win.height,
    width: win.width,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0,0,0,0.3)",
    position: "absolute",
  },
  activeSegmentButton: {
    justifyContent: "center",
    flex: 0.5,
    flexDirection: "column",
    borderBottomColor: "white",
    borderBottomWidth: 2,
    marginHorizontal: 5,
    paddingVertical: 4,
  },
  inactiveSegmentButton: {
    justifyContent: "center",
    width: 100,
    flex: 0.5,
    flexDirection: "column",
    marginHorizontal: 5,
    paddingVertical: 4,
  },
  activeSegmentText: {
    textAlign: "center",
    color: "white",
    fontSize: 16,
    fontWeight: "bold",
  },
  inactiveSegmentText: {
    textAlign: "center",
    color: "white",
    fontSize: 15,
    fontWeight: "bold",
  },
});
