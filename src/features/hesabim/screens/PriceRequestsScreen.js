import React, { Component } from "react";
import {
  View,
  SafeAreaView,
  Dimensions,
  FlatList,
  Animated,
  Platform,
  RefreshControl,
  Image,
  TouchableOpacity,
  Alert,
} from "react-native";
import { Text, AnimatedHeader, PencereIndicator, ListModal } from "@components";
import { connect } from "react-redux";
import colors from "@common/styles/colors";
import { moderateScale } from "react-native-size-matters";
import { getWidth } from "@common/Utils";
import PriceRequestItem from "@components/PriceRequestItem";
import { addPriceRequestToCart, startFromPriceReq } from "@redux/actions";
import { getPriceRequests } from "../redux/price_requests_actions";

const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);

const win = Dimensions.get("window");

const PADDING = getWidth() / 25;

class PriceRequestsScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      scrollAnim: new Animated.Value(0),
    };
  }

  onRefresh = () => {
    this.props.getPriceRequests(true);
  };

  componentDidMount() {
    this.setState({ inProgress: false });
    this.props.getPriceRequests(false);
  }

  renderItem = ({ item }) => {
    return (
      <PriceRequestItem
        item={item}
        isAddToCartDisabled={
          this.props.cart.cartInfo.IlSeciliSepetKalemler.findIndex(
            (cartItem) => cartItem.UrunKodu === "FTN" + item.FiyatTalepNo,
          ) !== -1
        }
        listModal={this.listModal}
        addPriceRequestToCart={(ftn, pb) => {
          this.setState({ inProgress: true });
          this.props
            .addPriceRequestToCart(ftn, pb)
            .then((resp) => {
              this.setState({ inProgress: false });
              Alert.alert("Başarılı İşlem", resp.DurumMesaj, [{ text: "Tamam" }]);
            })
            .catch((msg) => {
              this.setState({ inProgress: false });
              Alert.alert(
                "Başarısız İşlem",
                msg || "Beklenmedik bir hata oluştu. Lütfen daha sonra tekrar deneyiniz.",
                [{ text: "Tamam" }],
              );
            });
        }}
        startFromPriceReq={async (ftn) => {
          this.setState({ inProgress: true });
          await this.props.startFromPriceReq(ftn);
          this.setState({ inProgress: false });
        }}
      />
    );
  };

  renderHeader = () => {
    if (this.props.priceRequests.uygunsuzUrun && this.props?.navigation?.state?.params?.showAlert)
      return (
        <View style={{ flexDirection: "column", width: getWidth() - 2 * PADDING }}>
          {
            <TouchableOpacity
              style={{
                padding: moderateScale(5, 0.5),
                flexDirection: "row",
                backgroundColor: "#ffedc2",
              }}
              onPress={() => this.props.navigation.navigate("ShoppingCartScreen")}
            >
              <Image
                source={require("@assets/images/info_white.png")}
                style={{
                  width: moderateScale(14, 0.5),
                  height: moderateScale(14, 0.5),
                  tintColor: "#A88F80",
                  resizeMode: "contain",
                  marginRight: moderateScale(8, 0.5),
                  marginLeft: moderateScale(3, 0.5),
                }}
              />
              <Text fontScale={0.5} style={{ flexWrap: "wrap", flex: 1 }}>
                <Text
                  fontScale={0.5}
                  style={{
                    fontFamily: "Poppins",
                    color: "#707070",
                    fontSize: 11,
                  }}
                >
                  {
                    "Sepetinizde teklife uygun olmayan ürünler bulunmaktadır. Sepetinize giderek ürünleri satın alabilirsiniz."
                  }
                </Text>
              </Text>
            </TouchableOpacity>
          }
        </View>
      );
    else return null;
  };

  render() {
    return (
      <SafeAreaView
        style={{ backgroundColor: colors.primary_dark, flex: 1 }}
        forceInset={{ top: "always" }}
      >
        <View style={{ flexGrow: 1, width: win.width, backgroundColor: "#efefef" }}>
          <AnimatedFlatList
            data={this.props.priceRequests.items}
            renderItem={this.renderItem}
            ListEmptyComponent={() =>
              !this.props.priceRequests.isFetching &&
              !this.props.priceRequests.isRefreshing && (
                <View
                  style={{
                    width: win.width,
                    marginTop: 8,
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <Text style={{ fontSize: 14, color: "gray" }}>
                    Görüntülenecek fiyat teklifi bulunamadı.
                  </Text>
                </View>
              )
            }
            refreshControl={
              <RefreshControl
                refreshing={this.props.priceRequests.isRefreshing}
                onRefresh={this.onRefresh}
                progressViewOffset={130}
              />
            }
            contentOffset={{ y: -AnimatedHeader.height }}
            contentInset={{
              top: AnimatedHeader.height,
              left: 0,
              bottom: 0,
              right: 0,
            }}
            keyExtractor={(item) => item.BildirimId}
            overScrollMode="never"
            ListHeaderComponent={this.renderHeader}
            contentContainerStyle={{
              flexGrow: this.props.priceRequests.items.length > 0 ? 1 : 0,
              alignItems: "center",
              paddingTop:
                Platform.OS === "android" ? moderateScale(130, 0.4) : moderateScale(10, 0.4),
              paddingBottom: moderateScale(10, 0.4),
            }}
            onScroll={Animated.event(
              [{ nativeEvent: { contentOffset: { y: this.state.scrollAnim } } }],
              {
                useNativeDriver: true,
              },
            )}
            scrollEventThrottle={16}
          />
          <AnimatedHeader
            navigation={this.props.navigation}
            title="Fiyat Tekliflerim"
            searchBar={true}
            search={false}
            back={true}
            alignLeft={true}
          />
        </View>
        <ListModal
          contentContainerstyle={{ padding: moderateScale(10, 0.5) }}
          title="Teklifteki Ürünler"
          ref={(c) => (this.listModal = c)}
          renderItem={(item) => {
            return (
              <TouchableOpacity
                onPress={() => {
                  this.listModal.toggleVisibility();
                  this.props.navigation.navigate({
                    key: "priceRequest",
                    routeName: "ProductDetailsScreen",
                    params: {
                      UrunKodu: item.UrunKodu,
                      latestScreen: "PriceRequestsScreen",
                    },
                  });
                }}
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  padding: moderateScale(10, 0.5),
                  paddingVertical: moderateScale(5, 0.5),
                  borderRadius: 3,
                  borderWidth: 1,
                  borderColor: "#ddd",
                  marginBottom: moderateScale(10, 0.5),
                  flex: 1,
                }}
              >
                <Image
                  source={{ uri: item.UrunResmi }}
                  style={{
                    width: moderateScale(50, 0.5),
                    height: moderateScale(50, 0.5),
                    paddingRight: moderateScale(5, 0.5),
                    resizeMode: "contain",
                    marginRight: moderateScale(5, 0.5),
                  }}
                />
                <View
                  style={{
                    flexDirection: "column",
                    flex: 1,
                  }}
                >
                  <Text fontScale={0.5} style={{ flex: 1 }}>
                    <Text
                      fontScale={0.5}
                      style={{
                        fontFamily: "Poppins",
                        fontSize: 12,
                        color: "gray",
                      }}
                    >
                      Ürün Kodu:{" "}
                    </Text>
                    <Text fontScale={0.5} style={{ fontFamily: "Poppins", fontSize: 12 }}>
                      {item.UrunKodu}
                    </Text>
                  </Text>
                  <Text fontScale={0.5} style={{ flex: 1 }}>
                    <Text
                      fontScale={0.5}
                      style={{
                        fontFamily: "Poppins",
                        fontSize: 12,
                        color: "gray",
                      }}
                    >
                      Ürün Açıklaması:{" "}
                    </Text>
                    <Text
                      fontScale={0.5}
                      numberOfLines={2}
                      ellipsizeMode="tail"
                      style={{ fontFamily: "Poppins", fontSize: 12, flex: 1 }}
                    >
                      {item.UrunAciklamasi}
                    </Text>
                  </Text>
                </View>
              </TouchableOpacity>
            );
          }}
        />
        {((this.props.priceRequests.isFetching && !this.props.priceRequests.isRefreshing) ||
          this.state.inProgress) && (
          <View
            style={{
              width: win.width,
              height: win.height,
              justifyContent: "center",
              alignItems: "center",
              position: "absolute",
              top: 0,
              left: 0,
              backgroundColor: "rgba(0,0,0,0.3)",
              zIndex: 9999,
            }}
          >
            <PencereIndicator />
          </View>
        )}
      </SafeAreaView>
    );
  }
}

function mapStateToProps(state) {
  return {
    session: state.session,
    placeOrder: state.placeOrder,
    cart: state.cart,
    priceRequests: state.priceRequests,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    addPriceRequestToCart: (ftk, pb) => dispatch(addPriceRequestToCart(ftk, pb)),
    startFromPriceReq: (ftn) => dispatch(startFromPriceReq(ftn)),
    getPriceRequests: (refresh) => dispatch(getPriceRequests(refresh)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(PriceRequestsScreen);
