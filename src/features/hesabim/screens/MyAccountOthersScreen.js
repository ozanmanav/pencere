import React, { Component } from "react";
import {
  View,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Platform,
  Animated,
  TouchableOpacity,
  Image,
  Dimensions,
  FlatList,
} from "react-native";
import { connect } from "react-redux";
import { Text, AnimatedHeader } from "@components";
import { moderateScale } from "react-native-size-matters";

const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);

const win = Dimensions.get("window");
const NAVBAR_HEIGHT = AnimatedHeader.height;
const STATUS_BAR_HEIGHT = 0;
const items = [
  { title: "Paynet Kampanyaları", routeName: "PaynetCampaignsScreen" },
  { title: "Kuponlarım", routeName: "CouponsScreen" },
];

class MyAccountOthersScreen extends Component {
  constructor(props) {
    super(props);
    const scrollAnim = new Animated.Value(0);
    this.state = {
      isFetching: false,
      scrollAnim,
      clampedScroll: Animated.diffClamp(
        Animated.add(
          Animated.add(scrollAnim, 100).interpolate({
            inputRange: [0, 1],
            outputRange: [0, 1],
            extrapolateLeft: "clamp",
          }),
          -100,
        ),
        0,
        NAVBAR_HEIGHT - STATUS_BAR_HEIGHT,
      ),
    };
  }

  renderItem = ({ item }) => {
    return (
      <TouchableOpacity
        onPress={() => this.props.navigation.navigate(item.routeName)}
        style={styles.itemContainer}
      >
        <Text style={styles.itemTitle}>{item.title}</Text>
        <Image source={require("@assets/images/arrow-right.png")} style={styles.arrow} />
      </TouchableOpacity>
    );
  };

  render() {
    return (
      <SafeAreaView style={{ backgroundColor: "white", flex: 1 }} forceInset={{ top: "always" }}>
        <View style={{ width: win.width, backgroundColor: "white", flexGrow: 1 }}>
          <AnimatedFlatList
            data={items}
            renderItem={this.renderItem}
            contentOffset={{ y: -moderateScale(100, 0.4) }}
            contentInset={{ top: moderateScale(100, 0.4), left: 0, bottom: 0, right: 0 }}
            contentContainerStyle={{
              flexGrow: 1,
              alignItems: "center",
              paddingBottom: 10,
              paddingTop:
                Platform.OS === "android"
                  ? moderateScale(110, 0.4) + (Platform.Version < 20 ? 0 : StatusBar.currentHeight)
                  : 10,
            }}
            scrollEventThrottle={16}
          />
          <AnimatedHeader
            navigation={this.props.navigation}
            title="Diğer İşlemler"
            clampedScroll={this.state.clampedScroll}
            home={true}
            searchBar={true}
            search={false}
            back={true}
            alignLeft={true}
          />
        </View>
      </SafeAreaView>
    );
  }
}

function mapStateToProps(state) {
  return {
    session: state.session,
  };
}

export default connect(mapStateToProps, null)(MyAccountOthersScreen);

const styles = StyleSheet.create({
  itemContainer: {
    flexDirection: "row",
    width: win.width * 0.94,
    paddingHorizontal: moderateScale(15, 0.4),
    alignItems: "center",
    justifyContent: "space-between",
    backgroundColor: "#f3f3f3",
    borderRadius: 3,
    marginBottom: 10,
    height: moderateScale(50, 0.4),
  },
  itemTitle: {
    fontSize: 13,
    lineHeight: 16,
    marginTop: 3,
    fontFamily: "Poppins-Medium",
  },
  arrow: {
    width: moderateScale(8, 0.5),
    height: moderateScale(8, 0.5),
    tintColor: "black",
  },
});
