import React, { Component } from "react";
import {
  SafeAreaView,
  View,
  Platform,
  Linking,
  StyleSheet,
  Dimensions,
  ScrollView,
  TouchableOpacity,
  StatusBar,
} from "react-native";
import { Text, AnimatedHeader, Card, WebViewModal } from "@components";
import { DeliveryConfirmationDialog } from "@features/hesabim/components";
import { downloadFile, updateDeliveryInfo } from "@common/Utils";
import { connect } from "react-redux";
import { moderateScale } from "react-native-size-matters";

const win = Dimensions.get("window");

class InvoiceDetailsScreen extends Component {
  downloadFile = (item) => {
    downloadFile(item.faturaLinkP, "fatura.pdf")
      .then((res) => {
        this.wvModal.toggleModal({ uri: res.path });
      })
      .catch((err) => {
        console.log(err);
      });
  };

  showConfirmationDialog = () => {
    this.deliveryConfirmationDialog.toggleVisibility();
  };

  render() {
    const item = this.props.navigation.state.params.item;
    return (
      <SafeAreaView style={{ backgroundColor: "white", flex: 1 }}>
        <View style={{ width: win.width, backgroundColor: "#efefef", flexGrow: 1 }}>
          <ScrollView
            contentOffset={{ y: -moderateScale(100, 0.4) }}
            contentInset={{
              top: moderateScale(100, 0.4),
              left: 0,
              bottom: 0,
              right: 0,
            }}
            contentContainerStyle={{
              flexGrow: 1,
              paddingBottom: Platform.OS === "android" ? 10 : 0,
            }}
          >
            <Card
              renderContent={() => {
                return (
                  <View
                    style={{
                      flexDirection: "column",
                      padding: win.width * 0.04,
                      marginTop:
                        Platform.OS === "android"
                          ? moderateScale(100, 0.4) +
                            (Platform.Version < 20 ? 0 : StatusBar.currentHeight)
                          : 0,
                    }}
                  >
                    <View>
                      <Text fontScale={0.5} style={styles.titleTxt}>
                        {"Tarih:"}
                      </Text>
                      <Text fontScale={0.5} style={styles.valueTxt}>
                        {new Date(item.tarih).toLocaleDateString()}
                      </Text>
                    </View>
                    <View>
                      <Text fontScale={0.5} style={styles.titleTxt}>
                        {"Belge Açıklaması:"}
                      </Text>
                      <Text fontScale={0.5} style={styles.valueTxt}>
                        {item.belgeAciklama}
                      </Text>
                    </View>
                    <View>
                      <Text fontScale={0.5} style={styles.titleTxt}>
                        {"Sipariş No:"}
                      </Text>
                      <Text fontScale={0.5} style={styles.valueTxt}>
                        {item.siparisNo}
                      </Text>
                    </View>
                    <View>
                      <Text fontScale={0.5} style={styles.titleTxt}>
                        {"SAS No:"}
                      </Text>
                      <Text fontScale={0.5} style={styles.valueTxt}>
                        {item.sasNo}
                      </Text>
                    </View>
                    <View>
                      <Text fontScale={0.5} style={styles.titleTxt}>
                        {"Fatura No:"}
                      </Text>
                      <Text fontScale={0.5} style={styles.valueTxt}>
                        {item.faturaNo}
                      </Text>
                    </View>
                    <View>
                      <Text fontScale={0.5} style={styles.titleTxt}>
                        {"Referans:"}
                      </Text>
                      <Text fontScale={0.5} style={styles.valueTxt}>
                        {item.referans}
                      </Text>
                    </View>
                    <View>
                      <Text fontScale={0.5} style={styles.titleTxt}>
                        {"Tutar:"}
                      </Text>
                      <Text fontScale={0.5} style={styles.valueTxt}>
                        {item.tutar}
                      </Text>
                    </View>
                    <View>
                      <Text fontScale={0.5} style={styles.titleTxt}>
                        {"Teslim Bilgisi:"}
                      </Text>
                      <Text fontScale={0.5} style={styles.valueTxt}>
                        {item.teslimBilgisi}
                      </Text>
                    </View>
                    {item.faturaGosterilebilir === "1" && (
                      <View>
                        <Text fontScale={0.5} style={styles.titleTxt}>
                          {"Fatura Link PDF:"}
                        </Text>
                        <Text
                          fontScale={0.5}
                          style={styles.valueTxt}
                          onPress={() => Linking.openURL(item.faturaLinkP)}
                        >
                          {item.faturaLinkP}
                        </Text>
                      </View>
                    )}
                    {item.faturaGosterilebilir === "0" && (
                      <View>
                        <Text fontScale={0.5} style={styles.titleTxt}>
                          {"Fatura Link PDF:"}
                        </Text>
                        <TouchableOpacity onPress={this.showConfirmationDialog}>
                          <Text fontScale={0.5} style={styles.blueTxt}>
                            {"Sipariş onayı bekleniyor"}
                          </Text>
                        </TouchableOpacity>
                      </View>
                    )}
                    {item.faturaGosterilebilir === "1" && (
                      <View>
                        <Text fontScale={0.5} style={styles.titleTxt}>
                          {"Fatura Link XML:"}
                        </Text>
                        <Text
                          fontScale={0.5}
                          style={styles.valueTxt}
                          onPress={() => Linking.openURL(item.faturaLinkX)}
                        >
                          {item.faturaLinkX}
                        </Text>
                      </View>
                    )}
                    {item.faturaGosterilebilir === "0" && (
                      <View>
                        <Text fontScale={0.5} style={styles.titleTxt}>
                          {"Fatura Link XML:"}
                        </Text>
                        <TouchableOpacity onPress={this.showConfirmationDialog}>
                          <Text fontScale={0.5} style={styles.blueTxt}>
                            {"Sipariş onayı bekleniyor"}
                          </Text>
                        </TouchableOpacity>
                      </View>
                    )}
                    <View>
                      <Text fontScale={0.5} style={styles.titleTxt}>
                        {"Alacak Tutarı:"}
                      </Text>
                      <Text fontScale={0.5} style={styles.valueTxt}>
                        {item.TrlAlacakTutari}
                      </Text>
                    </View>
                    <View>
                      <Text fontScale={0.5} style={styles.titleTxt}>
                        {"Bakiye Tutarı:"}
                      </Text>
                      <Text fontScale={0.5} style={styles.valueTxt}>
                        {item.TrlBakiyeTutari}
                      </Text>
                    </View>
                    <View>
                      <Text fontScale={0.5} style={styles.titleTxt}>
                        {"Borç Tutarı:"}
                      </Text>
                      <Text fontScale={0.5} style={styles.valueTxt}>
                        {item.TrlBorcTutari}
                      </Text>
                    </View>
                  </View>
                );
              }}
            />
          </ScrollView>
        </View>
        <WebViewModal
          title="Fatura"
          ref={(c) => (this.wvModal = c)}
          style={{ height: win.height * 0.9 }}
        />
        <DeliveryConfirmationDialog
          ref={(c) => (this.deliveryConfirmationDialog = c)}
          item={this.props.navigation.state.params.item}
          updateDeliveryInfo={(fn) => updateDeliveryInfo(this.props.session.info.SessionID, fn)}
        />
        <AnimatedHeader
          navigation={this.props.navigation}
          title="Fatura Detayları"
          home={true}
          searchBar={true}
          search={false}
          back={true}
          alignLeft={true}
        />
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  titleTxt: {
    fontSize: 14,
    fontFamily: "Poppins",
    color: "orange",
    marginBottom: moderateScale(5, 0.5),
  },
  valueTxt: {
    fontSize: 14,
    fontFamily: "Poppins",
    color: "black",
    marginBottom: moderateScale(5, 0.5),
  },
  blueTxt: {
    fontSize: 14,
    fontFamily: "Poppins",
    color: "#0000EE",
    marginBottom: moderateScale(5, 0.5),
  },
});
function mapStateToProps(state) {
  return {
    session: state.session,
  };
}
export default connect(mapStateToProps, null)(InvoiceDetailsScreen);
