import React, { Component } from "react";
import {
  SafeAreaView,
  View,
  StyleSheet,
  Dimensions,
  ScrollView,
  Platform,
  StatusBar,
} from "react-native";
import { Text, AnimatedHeader, Card } from "@components";
import colors from "@common/styles/colors";
import { getDate } from "@common/Utils";
import { moderateScale } from "react-native-size-matters";

const win = Dimensions.get("window");

class HesapEkstresiDetailsScreen extends Component {
  render() {
    const item = this.props.navigation.state.params.item;
    return (
      <SafeAreaView
        style={{ backgroundColor: colors.primary_dark, flex: 1 }}
        forceInset={{ top: "always" }}
      >
        <View style={{ width: win.width, backgroundColor: "#efefef", flexGrow: 1 }}>
          <ScrollView
            style={{ height: win.height }}
            contentOffset={{ y: -moderateScale(50, 0.4) }}
            contentInset={{ top: moderateScale(50, 0.4), left: 0, bottom: 0, right: 0 }}
            contentContainerStyle={{
              flexGrow: 1,
              paddingTop:
                Platform.OS === "android"
                  ? moderateScale(50, 0.4) + (Platform.Version < 20 ? 0 : StatusBar.currentHeight)
                  : 0,
            }}
          >
            <Card
              renderContent={() => {
                return (
                  <View style={{ flexDirection: "column", padding: moderateScale(10, 0.5) }}>
                    <View>
                      <Text fontScale={0.5} style={styles.titleTxt}>
                        {"Belge No:"}
                      </Text>
                      <Text fontScale={0.5} style={styles.valueTxt}>
                        {item.BelgeNo !== "" ? item.BelgeNo : "Yok"}
                      </Text>
                    </View>
                    <View>
                      <Text fontScale={0.5} style={styles.titleTxt}>
                        {"Belge Tipi:"}
                      </Text>
                      <Text fontScale={0.5} style={styles.valueTxt}>
                        {item.BelgeTipi}
                      </Text>
                    </View>
                    <View>
                      <Text fontScale={0.5} style={styles.titleTxt}>
                        {"İşlem Açıklaması:"}
                      </Text>
                      <Text fontScale={0.5} style={styles.valueTxt}>
                        {item.IslemAciklamasi}
                      </Text>
                    </View>
                    <View>
                      <Text fontScale={0.5} style={styles.titleTxt}>
                        {"Ödeme Tarihi:"}
                      </Text>
                      <Text fontScale={0.5} style={styles.valueTxt}>
                        {getDate(item.OdemeTarihi)}
                      </Text>
                    </View>
                    <View>
                      <Text fontScale={0.5} style={styles.titleTxt}>
                        {"Kayıt Tarihi:"}
                      </Text>
                      <Text fontScale={0.5} style={styles.valueTxt}>
                        {getDate(item.KayitTarihi)}
                      </Text>
                    </View>
                    <View>
                      <Text fontScale={0.5} style={styles.titleTxt}>
                        {"Para Birimi:"}
                      </Text>
                      <Text fontScale={0.5} style={styles.valueTxt}>
                        {item.ParaBirimi}
                      </Text>
                    </View>
                    <View>
                      <Text fontScale={0.5} style={styles.titleTxt}>
                        {"Alacak Tutarı:"}
                      </Text>
                      <Text fontScale={0.5} style={styles.valueTxt}>
                        {item.AlacakTutari + " " + item.ParaBirimi}
                      </Text>
                    </View>
                    <View>
                      <Text fontScale={0.5} style={styles.titleTxt}>
                        {"Bakiye Tutarı:"}
                      </Text>
                      <Text fontScale={0.5} style={styles.valueTxt}>
                        {item.BakiyeTutari + " " + item.ParaBirimi}
                      </Text>
                    </View>
                    <View>
                      <Text fontScale={0.5} style={styles.titleTxt}>
                        {"Alacak Tutarı(TL):"}
                      </Text>
                      <Text fontScale={0.5} style={styles.valueTxt}>
                        {item.TrlAlacakTutari + " " + item.TrlParaBirimi}
                      </Text>
                    </View>
                    <View>
                      <Text fontScale={0.5} style={styles.titleTxt}>
                        {"Bakiye Tutarı(TL)"}
                      </Text>
                      <Text fontScale={0.5} style={styles.valueTxt}>
                        {item.TrlBakiyeTutari + " " + item.TrlParaBirimi}
                      </Text>
                    </View>
                    <View>
                      <Text fontScale={0.5} style={styles.titleTxt}>
                        {"Borç Tutarı(TL)"}
                      </Text>
                      <Text fontScale={0.5} style={styles.valueTxt}>
                        {item.TrlBorcTutari + " " + item.TrlParaBirimi}
                      </Text>
                    </View>
                  </View>
                );
              }}
            />
          </ScrollView>
        </View>
        <AnimatedHeader
          navigation={this.props.navigation}
          title="Hesap Ekstresi Detayları"
          back={true}
          searchBar={false}
          alignLeft={true}
        />
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  titleTxt: {
    fontSize: 14,
    color: "orange",
    marginBottom: 5,
    fontFamily: "Poppins",
  },
  valueTxt: {
    fontSize: 14,
    color: "black",
    marginBottom: 5,
    fontFamily: "Poppins",
  },
});

export default HesapEkstresiDetailsScreen;
