import React, { Component } from "react";
import {
  View,
  SafeAreaView,
  Dimensions,
  TouchableOpacity,
  Platform,
  StatusBar,
} from "react-native";
import { connect } from "react-redux";
import { Text, AnimatedHeader } from "@components";
import colors from "@common/styles/colors";
import { logout } from "@redux/actions";

const win = Dimensions.get("window");

class UserManagementScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  logout = () => {
    this.props.logout().then(() => {
      console.log("logging out");
      this.props.navigation.navigate("Auth");
    });
  };

  render() {
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: colors.primary_dark }}>
        <View
          style={{
            height: win.height,
            width: win.width,
            backgroundColor: "white",
            paddingTop:
              Platform.OS === "android"
                ? 100 + (Platform.Version < 20 ? 0 : StatusBar.currentHeight)
                : 100,
            paddingHorizontal: win.width * 0.05,
          }}
        >
          <View style={{ backgroundColor: "#e5e5e5", padding: 10, marginTop: 15 }}>
            <Text
              style={{
                fontFamily: "Poppins-Medium",
                fontSize: 14,
                lineHeight: 19,
                marginTop: 0,
                marginBottom: 4,
              }}
            >
              Kullanıcı Bilgileri
            </Text>
            <Text
              style={{
                fontFamily: "Poppins-Medium",
                fontSize: 14,
                lineHeight: 18,
                marginTop: 0,
                color: "gray",
              }}
            >
              {this.props.session.info.Ad + " " + this.props.session.info.Soyad}{" "}
            </Text>
            <Text
              style={{
                fontFamily: "Poppins-Medium",
                fontSize: 14,
                lineHeight: 18,
                marginTop: 0,
                color: "gray",
              }}
            >
              {"Bayi Kodu: " + this.props.session.info.BayiKodu}{" "}
            </Text>
            <Text
              style={{
                fontFamily: "Poppins-Medium",
                fontSize: 14,
                lineHeight: 18,
                marginTop: 0,
                color: "gray",
              }}
            >
              {"Kullanıcı Kodu: " + this.props.session.info.KullaniciKodu}{" "}
            </Text>
          </View>
          <TouchableOpacity
            onPress={this.logout}
            style={{
              marginTop: win.width * 0.05,
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "center",
              borderRadius: 4,
              width: win.width * 0.9,
              backgroundColor: colors.primary_dark,
              height: 40,
            }}
          >
            <Text
              style={{
                fontFamily: "Poppins-Medium",
                fontSize: 16,
                lineHeight: 20,
                marginTop: 4,
                color: "white",
              }}
            >
              Çıkış Yap
            </Text>
          </TouchableOpacity>
          <AnimatedHeader
            navigation={this.props.navigation}
            back={true}
            alignLeft={true}
            title="Kullanıcı Yönetimi"
            searchBar={true}
          />
        </View>
      </SafeAreaView>
    );
  }
}

function mapStateToProps(state) {
  return {
    session: state.session,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    logout: () => dispatch(logout()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(UserManagementScreen);
