import React, { Component } from "react";
import {
  View,
  SafeAreaView,
  StyleSheet,
  Platform,
  Animated,
  StatusBar,
  Dimensions,
  FlatList,
} from "react-native";
import { connect } from "react-redux";
import { Text, AnimatedHeader, PaynetCampaignItem, PencereIndicator } from "@components";
import colors from "@common/styles/colors";
import { getPaynetCampaigns } from "@redux/actions/";
import { moderateScale } from "react-native-size-matters";

const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);

const win = Dimensions.get("window");
const NAVBAR_HEIGHT = AnimatedHeader.height;
const STATUS_BAR_HEIGHT = 0;

class PaynetCampaignsScreen extends Component {
  constructor(props) {
    super(props);
    const scrollAnim = new Animated.Value(0);
    this.state = {
      isFetching: false,
      items: [],
      searchText: "",
      scrollAnim,
      keyboardVisible: false,
      keyboardHeight: 0,
      clampedScroll: Animated.diffClamp(
        Animated.add(
          Animated.add(scrollAnim, 100).interpolate({
            inputRange: [0, 1],
            outputRange: [0, 1],
            extrapolateLeft: "clamp",
          }),
          -100,
        ),
        0,
        NAVBAR_HEIGHT - STATUS_BAR_HEIGHT,
      ),
    };
  }

  componentDidMount() {
    //this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
    //this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
    let today = new Date();
    let month = ("00" + (today.getMonth() + 1)).slice(-2);
    let year = today.getFullYear();
    this.props.getPaynetCampaigns(month, year);
  }

  componentWillUnmount() {
    // this.keyboardDidShowListener.remove();
    // this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow = e => {
    console.log("keyboard here");
    this.setState({
      keyboardVisible: true,
      keyboardHeight: e.endCoordinates.height,
    });
  };

  _keyboardDidHide = () => {
    this.setState({ keyboardVisible: false });
  };

  renderItem = ({ item }) => {
    return <PaynetCampaignItem item={item} />;
  };

  render() {
    return (
      <SafeAreaView
        style={{ backgroundColor: colors.primary_dark, flex: 1 }}
        forceInset={{ top: "always" }}
      >
        <View
          behavior={Platform.OS === "ios" ? "position" : null}
          style={{ flexGrow: 1, width: win.width, backgroundColor: "#efefef" }}
        >
          <AnimatedFlatList
            data={this.props.paynetCampaigns.items}
            renderItem={this.renderItem}
            scrollEventThrottle={16}
            overScrollMode="never"
            contentOffset={{ y: -AnimatedHeader.height }}
            contentInset={{ top: AnimatedHeader.height, left: 0, right: 0 }}
            ListEmptyComponent={() => (
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <Text fontScale={0.6} style={{ fontFamily: "Poppins", color: "gray" }}>
                  Görünütülenecek kampanya yok
                </Text>
              </View>
            )}
            contentContainerStyle={{
              flexGrow: 1,
              paddingTop:
                Platform.OS === "android"
                  ? AnimatedHeader.height +
                    (Platform.Version < 20 ? 0 : StatusBar.currentHeight) +
                    moderateScale(10, 0.5)
                  : moderateScale(10, 0.5),
              paddingBottom: moderateScale(60, 0.4),
              backgroundColor: "#efefef",
              alignItems: "center",
            }}
            onScroll={Animated.event(
              [{ nativeEvent: { contentOffset: { y: this.state.scrollAnim } } }],
              {
                useNativeDriver: true,
              },
            )}
          />
          {this.props.paynetCampaigns.isFetching && (
            <View style={[styles.loadingContainer]}>
              <PencereIndicator />
            </View>
          )}
          <AnimatedHeader
            navigation={this.props.navigation}
            title="Paynet Kampanyaları"
            clampedScroll={this.state.clampedScroll}
            home={true}
            searchBar={true}
            search={false}
            back={true}
            alignLeft={true}
          />
        </View>
      </SafeAreaView>
    );
  }
}

function mapStateToProps(state) {
  return {
    session: state.session,
    paynetCampaigns: state.paynetCampaigns,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    getPaynetCampaigns: (month, year) => dispatch(getPaynetCampaigns(month, year)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(PaynetCampaignsScreen);

const styles = StyleSheet.create({
  loadingContainer: {
    height: win.height,
    width: win.width,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0,0,0,0.5)",
    position: "absolute",
    top: 0,
    left: 0,
  },
});
