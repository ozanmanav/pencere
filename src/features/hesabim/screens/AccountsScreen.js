import React, { Component } from "react";
import {
  Platform,
  SafeAreaView,
  View,
  StyleSheet,
  ScrollView,
  Animated,
  Dimensions,
  TouchableOpacity,
  Image,
} from "react-native";
import {
  fetchAccountsFromApi,
  fetchAcikCeklerFromApi,
  fetchAcikKalemlerFromApi,
  fetchAcikKalemlerTotal,
} from "@redux/actions";
import { connect } from "react-redux";
import { AnimatedHeader, Text } from "@components";
const AnimatedScrollView = Animated.createAnimatedComponent(ScrollView);
const win = Dimensions.get("window");
import colors from "@common/styles/colors";

class AccountsScreen extends Component {
  componentDidMount() {
    this.props.getAccounts();

    this.props.getAcikCekler("TRY");
    this.props.getAcikCekler("USD");
    this.props.getAcikCekler("EUR");

    this.props.getAcikKalemler("TRY");
    this.props.getAcikKalemler("USD");
    this.props.getAcikKalemler("EUR");

    this.props.getAcikKalemlerToplam();
  }

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <SafeAreaView style={{ backgroundColor: colors.primary_dark }}>
        <View
          style={{
            height: win.height,
            width: win.width,
            backgroundColor: "#efefef",
          }}
        >
          <AnimatedScrollView
            contentOffset={{ y: -50 }}
            contentInset={{ top: 50, left: 0, bottom: 0, right: 0 }}
            contentContainerStyle={{
              paddingBottom: 60,
              paddingTop: Platform.OS === "android" ? 50 : 0,
            }}
            style={{ height: win.height }}
            removeClippedSubviews={false}
          >
            <View style={styles.infoContainer}>
              <View style={styles.titleContainer}>
                <Text style={{ fontSize: 18, paddingBottom: 10 }}>Hesap Bilgileri</Text>
              </View>
              <View style={{ flexDirection: "row" }}>
                <View style={{ flexDirection: "column", flex: 6 }}>
                  <Text style={styles.title}>Açık Hesap Toplamı</Text>
                  <Text style={styles.title}>Kredi Limiti</Text>
                  <Text style={styles.title}>Ödeme Günü</Text>
                  <Text style={styles.title}>Tahsilat Günü</Text>
                </View>
                <View style={{ flexDirection: "column", flex: 4 }}>
                  <Text style={styles.title}>{this.props.accounts.info.AcikHesapToplami}</Text>
                  <Text style={styles.title}>{this.props.accounts.info.KrediLimiti}</Text>
                  <Text style={styles.title}>{this.props.accounts.info.OdemeGunu}</Text>
                  <Text style={styles.title}>{this.props.accounts.info.TahsilatGunu}</Text>
                </View>
              </View>
            </View>
            <View style={styles.bottomPane}>
              <TouchableOpacity
                style={styles.button}
                onPress={() =>
                  this.props.navigation.navigate({
                    routeName: "HesapEkstresiScreen",
                  })
                }
              >
                <Text style={{ fontSize: 18, color: "gray" }}>Hesap Ekstresi</Text>
                <Image
                  source={require("@assets/images/arrow-right.png")}
                  style={{ width: 14, height: 14, tintColor: "gray" }}
                />
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.button}
                onPress={() =>
                  this.props.navigation.navigate({
                    routeName: "AcikKalemlerScreen",
                  })
                }
              >
                <Text style={{ fontSize: 18, color: "gray" }}>Açık Kalemler</Text>
                <Image
                  source={require("@assets/images/arrow-right.png")}
                  style={{ width: 14, height: 14, tintColor: "gray" }}
                />
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.button}
                onPress={() =>
                  this.props.navigation.navigate({
                    routeName: "AcikCeklerScreen",
                  })
                }
              >
                <Text style={{ fontSize: 18, color: "gray" }}>Açık Çekler</Text>
                <Image
                  source={require("@assets/images/arrow-right.png")}
                  style={{ width: 14, height: 14, tintColor: "gray" }}
                />
              </TouchableOpacity>
            </View>
          </AnimatedScrollView>
          <AnimatedHeader
            navigation={this.props.navigation}
            title="Cari Hesaplar"
            back={true}
            alignLeft={true}
          />
        </View>
      </SafeAreaView>
    );
  }
}
function mapStateToProps(state) {
  return {
    accounts: state.accounts,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    getAccounts: () => dispatch(fetchAccountsFromApi()),
    getAcikCekler: (curr) => dispatch(fetchAcikCeklerFromApi(curr)),
    getAcikKalemler: (curr) => dispatch(fetchAcikKalemlerFromApi(curr)),
    getAcikKalemlerToplam: () => dispatch(fetchAcikKalemlerTotal()),
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(AccountsScreen);

const styles = StyleSheet.create({
  button: {
    backgroundColor: "white",
    paddingVertical: 15,
    paddingHorizontal: 15,
    justifyContent: "space-between",
    flexDirection: "row",
    marginHorizontal: 10,
    marginBottom: 10,
    borderRadius: 4,
    alignItems: "center",
  },
  bottomPane: {
    marginTop: 20,
    flexDirection: "column",
  },
  titleContainer: {
    borderBottomWidth: 1,
    borderBottomColor: "gray",
    marginBottom: 10,
  },
  infoContainer: {
    flexDirection: "column",
    backgroundColor: "#fff",
    marginHorizontal: 10,
    paddingVertical: 5,
    borderWidth: 1,
    borderRadius: 2,
    borderColor: "#ddd",
    borderBottomWidth: 0,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.5,
    shadowRadius: 2,
    elevation: 1,
    padding: 10,
    marginTop: 10,
  },
  title: {
    fontSize: 16,
    color: colors.light_black,
    paddingVertical: 5,
  },
});
