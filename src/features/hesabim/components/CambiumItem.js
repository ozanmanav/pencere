import React, { Component } from "react";
import { View, StyleSheet, TouchableOpacity, Dimensions } from "react-native";
import { Text } from "@components";
import colors from "@common/styles/colors";
import { moderateScale } from "react-native-size-matters";

const win = Dimensions.get("window");

export default class CambiumItem extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={{ flexDirection: "column", alignItems: "center" }}>
        <View style={{ flexDirection: "row", padding: moderateScale(8, 0.5) }}>
          <View style={{ flex: 0.5, backgroundColor: "transparent", flexDirection: "column" }}>
            <Text style={styles.titleTxt}>{"Açıklama"}</Text>
            <Text style={styles.titleTxt}>{"Belge No"}</Text>
            <Text style={styles.titleTxt}>{"Kayıt Tarihi"}</Text>
            <Text style={styles.titleTxt}>{"Vade Tarihi"}</Text>
            <Text style={styles.titleTxt}>{"Tutar"}</Text>
          </View>
          <View style={{ flex: 0.5, backgroundColor: "transparent", flexDirection: "column" }}>
            <Text numberOfLines={1} style={styles.valueTxt}>
              {this.props.item.aciklama}
            </Text>
            <Text numberOfLines={1} style={styles.valueTxt}>
              {this.props.item.belgeNo}
            </Text>
            <Text numberOfLines={1} style={styles.valueTxt}>
              {this.props.item.kayitTarihi}
            </Text>
            <Text numberOfLines={1} style={styles.valueTxt}>
              {this.props.item.vadeTarihi}
            </Text>
            <Text numberOfLines={1} style={styles.valueTxt}>
              {this.props.item.tutar + " " + this.props.item.paraBirimi}
            </Text>
          </View>
        </View>
        <TouchableOpacity
          onPress={() => {
            this.props.isAdded
              ? this.props.removeFromCambium(this.props.item.belgeNo)
              : this.props.addToCambium(this.props.item.belgeNo);
          }}
          style={{
            backgroundColor: this.props.isAdded ? colors.red : colors.green,
            width: win.width * 0.6,
            height: moderateScale(24, 0.5),
            borderRadius: 3,
            marginBottom: moderateScale(10, 0.5),
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <Text style={{ fontSize: 15, color: "white", fontWeight: "500" }}>
            {this.props.isAdded ? "Çıkar" : "Ekle"}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  titleTxt: {
    fontSize: 12,
    color: "orange",
    marginBottom: moderateScale(5, 0.5),
  },
  valueTxt: {
    fontSize: 12,
    color: "black",
    marginBottom: moderateScale(5, 0.5),
  },
});
