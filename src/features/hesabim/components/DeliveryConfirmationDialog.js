import React, { Component } from "react";
import {
  Platform,
  Keyboard,
  Image,
  View,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  Alert,
} from "react-native";
import { Text, ModalBox } from "@components";
import colors from "@common/styles/colors";
import bugsnag from "@common/bugsnag_config";

const win = Dimensions.get("window");

export default class DeliveryConfirmationDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isVisible: false,
      keyboardVisible: false,
      keyboardHeight: 0,
      progress: false,
    };
  }

  componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener("keyboardDidShow", this._keyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener("keyboardDidHide", this._keyboardDidHide);
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow = e => {
    this.setState({ keyboardVisible: true, keyboardHeight: e.endCoordinates.height });
  };

  _keyboardDidHide = () => {
    this.setState({ keyboardVisible: false });
  };

  componentDidUpdate(prevProps, prevState) {
    if (this.props.isVisible !== prevProps.isVisible)
      this.setState({
        isVisible: this.props.isVisible,
      });
  }

  toggleVisibility = () => {
    return this.modalBox.toggleVisibility();
  };

  render() {
    return (
      <ModalBox
        ref={c => (this.modalBox = c)}
        statusBarHidden={false}
        animationType="slide"
        scrollable={false}
        presentationStyle="overFullScreen"
      >
        <TouchableOpacity
          style={[
            styles.container,
            {
              marginBottom: this.state.keyboardVisible
                ? this.state.keyboardHeight - (Platform.OS === "ios" ? win.height / 16 : 0)
                : 0,
            },
          ]}
          onPress={() => Keyboard.dismiss()}
        >
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "space-between",
              width: win.width * 0.9,
              paddingHorizontal: 16,
              paddingVertical: 12,
            }}
          >
            <Text
              style={{
                fontSize: 14,
                letterSpacing: 1.5,
                color: "black",
                fontFamily: "Poppins-SemiBold",
                marginTop: 4,
              }}
            >
              Teslimat Onayı
            </Text>
            <TouchableOpacity onPress={() => this.toggleVisibility()}>
              <Image
                source={require("@assets/images/close.png")}
                style={{ width: 20, height: 20, tintColor: "#afafaf", resizeMode: "contain" }}
              />
            </TouchableOpacity>
          </View>
          <Text
            style={{
              flexDirection: "row",
              flexWrap: "wrap",
              width: win.width * 0.8,
              marginBottom: 5,
            }}
          >
            <Text
              style={{
                fontFamily: "Poppins",
                fontSize: 15,
                color: colors.primary_light,
                textDecorationLine: "underline",
                textDecorationColor: colors.primary_light,
              }}
            >
              {this.props.item.siparisNo}
            </Text>
            <Text style={{ fontFamily: "Poppins", fontSize: 15 }}>
              {" "}
              numaralı siparişe ait ürünler tarafımızdan teslim alınmıştır.
            </Text>
          </Text>
          <View
            style={{
              flexDirection: "row",
              width: win.width * 0.9,
              justifyContent: "center",
              alignItems: "center",
              marginTop: 10,
              borderTopColor: "#dedede",
              borderTopWidth: 1,
            }}
          >
            <TouchableOpacity
              style={{
                flex: 1,
                alignItems: "center",
                padding: 10,
                borderRightWidth: 1,
                borderRightColor: "#dedede",
              }}
              onPress={() => this.toggleVisibility()}
            >
              <Text style={{ fontSize: 16, color: "#5d5d5d" }}>İptal</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                this.props
                  .updateDeliveryInfo(this.props.item.siparisNo)
                  .then(res => {
                    if (res === "0") {
                      Alert.alert(
                        "Başarısız İşlem",
                        "Siparişinizin teslim bilgisini güncellenemedi. Daha sonra tekrar deneyiniz.",
                        [
                          {
                            text: "OK",
                            onPress: () => {
                              this.toggleVisibility();
                            },
                          },
                        ],
                      );
                    } else {
                      Alert.alert(
                        "Başarılı İşlem",
                        "Siparişinizin teslim bilgisi başarıyla güncellendi.",
                      );
                    }
                  })
                  .catch(err => {
                    bugsnag.notify(err, report => {
                      report.errorClass = "updateDeliveryInfo";
                    });
                    console.log(err);
                    Alert.alert("Hata", "Bir hata oluştu.", [
                      {
                        text: "OK",
                        onPress: () => {
                          this.toggleVisibility();
                        },
                      },
                    ]);
                  });
              }}
              style={{ flex: 1, alignItems: "center", padding: 10 }}
            >
              <Text style={{ fontSize: 16, color: "#5d5d5d" }}>Onayla</Text>
            </TouchableOpacity>
          </View>
        </TouchableOpacity>
      </ModalBox>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "column",
    backgroundColor: "white",
    width: win.width * 0.9,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.5,
    shadowRadius: 2,
    elevation: 1,
    alignItems: "center",
    marginBottom: 50,
    borderRadius: 6,
  },
});
