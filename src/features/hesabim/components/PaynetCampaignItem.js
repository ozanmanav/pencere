import React, { Component } from "react";
import { View, Dimensions, StyleSheet } from "react-native";
import Slider from "react-native-slider";
import { Text } from "@components";
import AutoHeightWebView from "react-native-autoheight-webview";

const win = Dimensions.get("window");

export default class PaynetCampaignItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      amount: 0,
    };
  }

  renderPaymentBar() {
    return (
      <View style={{ flexDirection: "column", marginTop: 50 }}>
        <View
          style={{
            flexDirection: "row",
            flex: 1,
            justifyContent: "space-between",
          }}
        >
          <Text style={{ fontFamily: "Poppins", fontSize: 12, color: "#e15b1c" }}>
            {this.state.amount + " TL Alım"}
          </Text>
          <Text style={{ fontFamily: "Poppins", fontSize: 12, color: "#5eb139" }}>
            {(this.props.item.maxAmount || 0) + " TL Ödeme"}
          </Text>
        </View>
        <Slider
          disabled={!this.props.item.maxAmount && this.props.item.maxAmount === 0}
          step={1}
          trackStyle={styles.track}
          thumbStyle={
            !this.props.item.maxAmount || this.props.item.maxAmount === 0
              ? { width: 0, height: 0 }
              : styles.thumb
          }
          minimumTrackTintColor={
            !this.props.item.maxAmount || this.props.item.maxAmount === 0 ? "#d0d0d0" : "#e15b1c"
          }
          maximumTrackTintColor={
            !this.props.item.maxAmount || this.props.item.maxAmount === 0 ? "#e4e4e4" : "#5eb139"
          }
          value={this.state.amount}
          onValueChange={(amount) => this.setIntegerPart(amount)}
          maximumValue={this.props.item.maxAmount || 0}
        />
      </View>
    );
  }

  // Takes a number, sets the integer part of the amount
  setIntegerPart = (val) => {
    const newIntPart = Math.floor(val);
    var floatingPart = String(this.state.amount).split(".")[1];

    if (floatingPart) {
      floatingPart = Number(floatingPart) / Math.pow(10, floatingPart.length).toFixed(2);
      this.setState({
        amount: newIntPart + floatingPart,
      });
    } else {
      this.setState({
        amount: newIntPart,
      });
    }
  };

  // Takes a string, sets the floating part of the amount
  setFloatingPart = (val) => {
    const newFloatingPart = Number(val) / Math.pow(10, val.length).toFixed(2);
    const integerPart = Math.floor(this.state.amount);

    this.setState({
      amount: integerPart + newFloatingPart,
    });
  };

  render() {
    var html = this.props.item.KampanyaIcerik;
    if (html.substring(0, 6) === "&nbsp;") {
      html = html.substring(6, html.length);
    }
    return (
      <View
        style={{
          flexDirection: "column",
          backgroundColor: "white",
          borderRadius: 3,
          width: win.width * 0.9,
          marginBottom: win.width * 0.05,
          padding: win.width * 0.04,
        }}
      >
        <Text style={{ fontFamily: "Poppins", fontSize: 14, marginBottom: 10 }}>
          {this.props.item.KampanyaKonu}
        </Text>
        <AutoHeightWebView
          source={{ html: `<div style=""> ${html}</div>` }}
          style={{ width: win.width * 0.8 }}
        />
        {/* <TouchableOpacity style={{ alignSelf: 'center', backgroundColor: '#e15b1c', width: win.width * .82, height: 30, alignItems: 'center', justifyContent: 'center', marginBottom: 10, borderRadius: 3 }} >
                    <Text style={{ fontFamily: 'Poppins', fontSize: 15, color: 'white' }}>Kampanya Detayı</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{ alignSelf: 'center', backgroundColor: '#439ef3', width: win.width * .82, height: 30, alignItems: 'center', justifyContent: 'center', borderRadius: 3, marginBottom: 10 }}>
                    <Text style={{ fontFamily: 'Poppins', fontSize: 15, color: 'white' }}>Kart Oranları</Text>
                </TouchableOpacity> */}
        {/* <View style={{ flexDirection: 'row', justifyContent: 'space-between', height: 30 }} >
                    <View style={{ flexDirection: 'row', flex: .6, alignItems: 'flex-end' }}>
                        <TextInput
                            returnKeyType="done"
                            keyboardType="numeric"
                            style={{ height: 30, flex: 5, borderColor: '#e4e4e4', borderWidth: 1, padding: 0, paddingLeft: 10 }}
                            value={String(Math.floor(this.state.amount))}
                            onChangeText={(val) => this.setIntegerPart(Number(val))}
                        />
                        <Text style={{ marginHorizontal: 5, }} >,</Text>
                        <TextInput
                            returnKeyType="done"
                            keyboardType="numeric"
                            style={{ height: 30, flex: 1, borderColor: '#e4e4e4', borderWidth: 1, padding:0, paddingLeft: 10 }}
                            value={!floatingPart ? '00' : floatingPart}
                            onChangeText={(val) => this.setFloatingPart(val)}
                        />
                    </View>
                    <TouchableOpacity style={{ backgroundColor: '#5eb139', flex: .35, height: 30, alignItems: 'center', justifyContent: 'center', marginBottom: 10, borderRadius: 3 }} >
                        <Text style={{ fontFamily: 'Poppins', fontSize: 14, color: 'white' }}>Ödeme Yap</Text>
                    </TouchableOpacity>
                </View> */}
        {
          //this.renderPaymentBar(this.props.item)
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  track: {
    height: 16,
    borderRadius: 3,
  },
  thumb: {
    width: 0,
    height: 0,
    backgroundColor: "transparent",
    borderStyle: "solid",
    borderLeftWidth: 8,
    borderRightWidth: 8,
    borderBottomWidth: 8,
    top: 28,
    borderLeftColor: "transparent",
    borderRightColor: "transparent",
    borderBottomColor: "black",
  },
});
