import React, { Component } from "react";
import { View, TouchableOpacity, StyleSheet } from "react-native";
import { Text } from "@components";
import { moderateScale } from "react-native-size-matters";

export default class InvoiceItem extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <TouchableOpacity
        style={styles.container}
        onPress={() =>
          this.props.navigation.push("InvoiceDetailsScreen", { item: this.props.item })
        }
      >
        <View style={{ flex: 0.5, backgroundColor: "transparent", flexDirection: "column" }}>
          <Text fontScale={0.5} style={styles.titleTxt}>
            {"Tarih"}
          </Text>
          <Text fontScale={0.5} style={styles.titleTxt}>
            {"Belge Açıklaması"}
          </Text>
          <Text fontScale={0.5} style={styles.titleTxt}>
            {"Sipariş No"}
          </Text>
          <Text fontScale={0.5} style={styles.titleTxt}>
            {"Referans"}
          </Text>
          <Text fontScale={0.5} style={styles.titleTxt}>
            {"Tutar"}
          </Text>
        </View>
        <View style={{ flex: 0.5, backgroundColor: "transparent", flexDirection: "column" }}>
          <Text fontScale={0.5} numberOfLines={1} style={styles.valueTxt}>
            {new Date(this.props.item.tarih).toLocaleDateString()}
          </Text>
          <Text fontScale={0.5} numberOfLines={1} style={styles.valueTxt}>
            {this.props.item.belgeAciklama}
          </Text>
          <Text fontScale={0.5} numberOfLines={1} style={styles.valueTxt}>
            {this.props.item.siparisNo}
          </Text>
          <Text fontScale={0.5} numberOfLines={1} style={styles.valueTxt}>
            {this.props.item.referans}
          </Text>
          <Text fontScale={0.5} numberOfLines={1} style={styles.valueTxt}>
            {this.props.item.tutar}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    flex: 1,
    backgroundColor: "#fff",
    paddingVertical: 5,
    borderWidth: 1,
    borderRadius: 2,
    borderColor: "#ddd",
    borderBottomWidth: 0,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.3,
    shadowRadius: 2,
    elevation: 1,
    marginHorizontal: moderateScale(10, 0.5),
    marginTop: moderateScale(10, 0.5),
    paddingTop: moderateScale(10, 0.5),
    paddingLeft: moderateScale(10, 0.5),
  },
  titleTxt: {
    fontSize: 14,
    fontFamily: "Poppins",
    color: "orange",
    marginBottom: 5,
  },
  valueTxt: {
    fontSize: 14,
    fontFamily: "Poppins",
    color: "black",
    marginBottom: 5,
  },
});
