import React, { Component } from "react";
import { View, TouchableOpacity, Dimensions, Image } from "react-native";
import { Text, ModalBox } from "@components";
import { moderateScale, ScaledSheet } from "react-native-size-matters";

const win = Dimensions.get("window");

export default class CurrencyModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedItem: "",
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.isVisible !== prevProps.isVisible)
      this.setState({
        isVisible: this.props.isVisible,
      });
  }

  toggleVisibility = () => {
    this.modalBox.toggleVisibility();
  };

  componentDidMount() {
    this.setState({
      selectedItem: this.props.defaultItem || this.props.selectedItem,
    });
  }

  render() {
    const selectedItem = this.props.useSelectedProps
      ? this.props.selectedItem
      : this.state.selectedItem;

    return (
      <ModalBox ref={c => (this.modalBox = c)} statusBarHidden={false} scrollable={false}>
        <View style={styles.container}>
          {!this.props.hideTitle && (
            <View
              style={{
                borderBottomColor: "#dedede",
                borderBottomWidth: 1,
                alignItems: "center",
              }}
            >
              <Text style={{ fontSize: 18, paddingBottom: moderateScale(10, 0.5) }}>
                Para Birimi Seç
              </Text>
            </View>
          )}
          {this.props.defaultItem && (
            <TouchableOpacity
              onPress={() => {
                this.props.updateSelectedCart &&
                  this.props.updateSelectedCart(this.props.defaultItem);
                this.props.updateCurrency && this.props.updateCurrency(this.props.defaultItem);
                this.toggleVisibility();
                this.setState({ selectedItem: this.props.defaultItem });
              }}
              style={{
                backgroundColor: selectedItem === this.props.defaultItem ? "#E8F5E9" : "white",
                alignItems: "center",
                padding: moderateScale(5, 0.5),
                paddingVertical: moderateScale(8, 0.5),
                fontSize: 16,
              }}
            >
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                {(this.props.defaultItem === "TRY" || this.props.defaultItem === "TL") && (
                  <Image
                    source={require("@assets/images/tl.png")}
                    style={{
                      width: moderateScale(16, 0.5),
                      height: moderateScale(16, 0.5),
                      tintColor: "gray",
                      marginRight: moderateScale(5, 0.5),
                    }}
                  />
                )}
                <Text style={{ fontSize: 16, fontFamily: "Poppins" }}>
                  {this.props.defaultItem}
                </Text>
              </View>
            </TouchableOpacity>
          )}
          {this.props.items.map((item, i) => {
            return (
              <TouchableOpacity
                onPress={() => {
                  this.props.updateSelectedCart && this.props.updateSelectedCart(item);
                  this.props.updateCurrency && this.props.updateCurrency(item);
                  this.toggleVisibility();
                  this.setState({ selectedItem: item });
                }}
                style={{
                  backgroundColor: selectedItem === item ? "#E8F5E9" : "white",
                  alignItems: "center",
                  padding: moderateScale(5, 0.5),
                  paddingVertical: moderateScale(8, 0.5),
                  fontSize: 16,
                  borderBottomColor: "#dedede",
                  borderBottomWidth: i < this.props.items.length - 1 ? 1 : 0,
                }}
              >
                <View style={{ flexDirection: "row", alignItems: "center" }}>
                  {(item === "TRY" || item === "TL") && (
                    <Image
                      source={require("@assets/images/tl.png")}
                      style={{
                        width: moderateScale(18, 0.5),
                        height: moderateScale(18, 0.5),
                        tintColor: "gray",
                        marginRight: moderateScale(5, 0.5),
                      }}
                    />
                  )}
                  {item === "EUR" && (
                    <Image
                      source={require("@assets/images/euro.png")}
                      style={{
                        width: moderateScale(18, 0.5),
                        height: moderateScale(18, 0.5),
                        tintColor: "gray",
                        marginRight: moderateScale(5, 0.5),
                      }}
                    />
                  )}
                  {item === "USD" && (
                    <Image
                      source={require("@assets/images/dollar.png")}
                      style={{
                        width: moderateScale(18, 0.5),
                        height: moderateScale(18, 0.5),
                        tintColor: "gray",
                        marginRight: moderateScale(5, 0.5),
                      }}
                    />
                  )}
                  <Text style={{ fontSize: 18 }}>{item}</Text>
                </View>
              </TouchableOpacity>
            );
          })}
        </View>
      </ModalBox>
    );
  }
}

const styles = ScaledSheet.create({
  container: {
    padding: "5@ms0.5",
    backgroundColor: "white",
    width: "240@ms0.5",
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.3,
    shadowRadius: 2,
    elevation: 1,
    borderRadius: 3,
  },
  overlay: {
    position: "absolute",
    flex: 1,
    width: win.width,
    height: win.height,
    backgroundColor: "rgba(0,0,0,0.2)",
  },
});
