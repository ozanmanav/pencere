import React, { Component } from "react";
import { View, TouchableOpacity, Image, Dimensions, TextInput, Clipboard } from "react-native";
import { Text, SmartImage } from "@components";
import colors from "@common/styles/colors";
import { moderateScale, ScaledSheet } from "react-native-size-matters";

const win = Dimensions.get("window");

export default class SingleLargeProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      number: "1",
    };
  }

  renderPrices = () => {
    const TES = this.props.item.MobileUrunFiyatListesi.filter((item) => item.UrunFiyatTipi === "T");
    const OZEL = this.props.item.MobileUrunFiyatListesi.filter(
      (item) => item.UrunFiyatTipi === "S",
    );
    const BAYI = this.props.item.MobileUrunFiyatListesi.filter(
      (item) => item.UrunFiyatTipi === "B",
    );
    const FIRSAT = this.props.item.MobileUrunFiyatListesi.filter(
      (item) => item.UrunFiyatTipi === "F",
    );

    if (this.props.layout === "large")
      return (
        <View
          style={{
            flexDirection: "row",
            borderTopColor: "#ededed",
            borderTopWidth: 1,
            paddingTop: moderateScale(10, 0.5),
          }}
        >
          <View
            style={{
              flexDirection: "column",
              paddingRight: moderateScale(6, 0.5),
            }}
          >
            {TES && TES[0] && (
              <Text
                style={{
                  fontFamily: "Poppins-Light",
                  fontSize: 12,
                  color: "gray",
                }}
              >
                {TES[0].UrunFiyatLabel}
              </Text>
            )}
            {OZEL && OZEL[0] && (
              <Text
                style={{
                  fontFamily: "Poppins-Light",
                  fontSize: FIRSAT && FIRSAT[0] ? 12 : 14,
                  color: FIRSAT && FIRSAT[0] ? "gray" : colors.price_color,
                }}
              >
                {OZEL[0].UrunFiyatLabel}
              </Text>
            )}
            {BAYI && BAYI[0] && (
              <Text
                style={{
                  fontFamily: "Poppins-Light",
                  fontSize: FIRSAT && FIRSAT[0] ? 12 : 14,
                  color: FIRSAT && FIRSAT[0] ? "gray" : colors.price_color,
                }}
              >
                {BAYI[0].UrunFiyatLabel}
              </Text>
            )}
            {FIRSAT && FIRSAT[0] && (
              <Text
                style={{
                  fontFamily: "Poppins-Light",
                  fontSize: 14,
                  color: colors.price_color,
                }}
              >
                {"Fırsat Fiyatı"}
              </Text>
            )}
          </View>
          <View style={{ flexDirection: "column" }}>
            {TES && TES[0] && (
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Text
                  style={{
                    fontFamily: "Poppins-Light",
                    fontSize: 12,
                    color: "gray",
                  }}
                >
                  :{" "}
                </Text>
                <Text
                  style={{
                    fontFamily: "Poppins-Light",
                    fontSize: 12,
                    color: "gray",
                  }}
                >
                  {TES[0].UrunFiyatStr + " + KDV"}
                </Text>
              </View>
            )}
            {OZEL && OZEL[0] && (
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Text
                  style={{
                    fontFamily: "Poppins-Light",
                    fontSize: 12,
                    color: FIRSAT && FIRSAT[0] ? "gray" : colors.price_color,
                  }}
                >
                  :{" "}
                </Text>
                <Text
                  style={{
                    fontFamily: FIRSAT && FIRSAT[0] ? "Poppins-Light" : "Poppins",
                    textDecorationLine: FIRSAT && FIRSAT[0] ? "line-through" : "none",
                    fontSize: FIRSAT && FIRSAT[0] ? 12 : 14,
                    color: FIRSAT && FIRSAT[0] ? "gray" : colors.price_color,
                  }}
                >
                  {OZEL[0].UrunFiyatStr + " + KDV"}
                </Text>
              </View>
            )}
            {BAYI && BAYI[0] && (
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Text
                  style={{
                    fontFamily: "Poppins-Light",
                    fontSize: 12,
                    color: FIRSAT && FIRSAT[0] ? "gray" : colors.price_color,
                  }}
                >
                  :{" "}
                </Text>
                <Text
                  style={{
                    fontFamily: "Poppins",
                    fontSize: 14,
                    color: FIRSAT && FIRSAT[0] ? "gray" : colors.price_color,
                    textDecorationLine: FIRSAT && FIRSAT[0] ? "line-through" : "none",
                  }}
                >
                  {BAYI[0].UrunFiyatStr + " + KDV"}
                </Text>
              </View>
            )}
            {FIRSAT && FIRSAT[0] && (
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Text
                  style={{
                    fontFamily: "Poppins",
                    fontSize: 14,
                    color: colors.price_color,
                  }}
                >
                  :{" "}
                </Text>
                <Text
                  style={{
                    fontFamily: "Poppins",
                    fontSize: 14,
                    top: 1,
                    color: colors.price_color,
                  }}
                >
                  {FIRSAT[0].UrunFiyatStr + " + KDV"}
                </Text>
              </View>
            )}
          </View>
        </View>
      );
    else {
      return (
        <View
          style={{
            flexDirection: "row",
            borderTopColor: "#ededed",
            borderTopWidth: 1,
            paddingTop: moderateScale(11, 0.5),
          }}
        >
          <View
            style={{
              flexDirection: "column",
              paddingRight: moderateScale(6, 0.5),
              justifyContent: "center",
            }}
          >
            {TES && TES[0] && (
              <Text
                style={{
                  fontFamily: "Poppins-Light",
                  fontSize: 12,
                  color: "gray",
                }}
              >
                {TES[0].UrunFiyatLabel}
              </Text>
            )}
            {OZEL && OZEL[0] && (
              <Text
                style={{
                  fontFamily: "Poppins-Light",
                  fontSize: FIRSAT && FIRSAT[0] ? 11 : 12,
                  color: FIRSAT && FIRSAT[0] ? "gray" : colors.price_color,
                }}
              >
                {OZEL[0].UrunFiyatLabel}
              </Text>
            )}
            {BAYI && BAYI[0] && (
              <Text
                style={{
                  fontFamily: "Poppins-Light",
                  fontSize: FIRSAT && FIRSAT[0] ? 11 : 12,
                  color: FIRSAT && FIRSAT[0] ? "gray" : colors.price_color,
                }}
              >
                {BAYI[0].UrunFiyatLabel}
              </Text>
            )}
            {FIRSAT && FIRSAT[0] && (
              <Text
                style={{
                  fontFamily: "Poppins-Light",
                  fontSize: 12,
                  color: colors.price_color,
                }}
              >
                {"Fırsat Fiyatı"}
              </Text>
            )}
          </View>
          <View style={{ flexDirection: "column", justifyContent: "center" }}>
            {TES && TES[0] && (
              <Text
                style={{
                  fontFamily: "Poppins-Light",
                  fontSize: 11,
                  color: "gray",
                  marginTop: 1,
                }}
              >
                {TES[0].UrunFiyatStr + " + KDV"}
              </Text>
            )}
            {OZEL && OZEL[0] && (
              <Text
                style={{
                  marginTop: 1,
                  fontFamily: FIRSAT && FIRSAT[0] ? "Poppins-Light" : "Poppins",
                  textDecorationLine: FIRSAT && FIRSAT[0] ? "line-through" : "none",
                  fontSize: FIRSAT && FIRSAT[0] ? 11 : 12,
                  color: FIRSAT && FIRSAT[0] ? "gray" : colors.price_color,
                }}
              >
                {OZEL[0].UrunFiyatStr + " + KDV"}
              </Text>
            )}
            {BAYI && BAYI[0] && (
              <Text
                style={{
                  marginTop: 1,
                  fontFamily: FIRSAT && FIRSAT[0] ? "Poppins-Light" : "Poppins",
                  fontSize: FIRSAT && FIRSAT[0] ? 11 : 12,
                  color: FIRSAT && FIRSAT[0] ? "gray" : colors.price_color,
                  textDecorationLine: FIRSAT && FIRSAT[0] ? "line-through" : "none",
                }}
              >
                {BAYI[0].UrunFiyatStr + " + KDV"}
              </Text>
            )}
            {FIRSAT && FIRSAT[0] && (
              <Text
                style={{
                  fontFamily: "Poppins",
                  fontSize: 12,
                  color: colors.price_color,
                }}
              >
                {FIRSAT[0].UrunFiyatStr + " + KDV"}
              </Text>
            )}
          </View>
        </View>
      );
    }
  };

  render() {
    const splittedPb = this.props.item.MobileUrunFiyatListesi[0].UrunFiyatStr.split(" ");
    return (
      <TouchableOpacity
        testID={"product" + this.props.index}
        key={this.props.item.UrunKodu}
        onPress={() => {
          this.props.navigation.navigate({
            key: this.props.search ? "search" : "ProductDetailsScreen",
            routeName: "ProductDetailsScreen",
            params: {
              UrunKodu: this.props.item.UrunKodu,
              SeriNo: this.props.item.UrunSeriNo,
              isFavorite: false,
              pb: splittedPb != null ? splittedPb[splittedPb.length - 1] : "TRY",
            },
          });
        }}
        style={this.props.layout === "large" ? styles.containerLarge : styles.containerSmall}
      >
        <View
          style={{
            flexDirection: this.props.layout === "large" ? "column" : "row",
            flex: 1,
          }}
        >
          {this.props.layout === "large" && (
            <View style={{ flexDirection: "row", flex: 1, alignItems: "center" }}>
              <Text
                style={{
                  fontSize: 12,
                  color: "#b11014",
                  fontFamily: "Poppins-Bold",
                  letterSpacing: 0.8,
                  marginRight: 5,
                }}
              >
                {this.props.item.UrunMarkaText}
              </Text>
              <Text
                onPress={() => {
                  Clipboard.setString(this.props.item.UrunKodu);
                  this.props.showToast("Ürün kodu kopyalandı");
                }}
                style={{
                  marginRight: moderateScale(5, 0.5),
                  fontSize: 12,
                  color: "gray",
                  fontFamily: "Poppins",
                }}
              >
                {this.props.item.UrunKodu}
              </Text>
              <TouchableOpacity
                onPress={() => {
                  Clipboard.setString(this.props.item.UrunKodu);
                  this.props.showToast("Ürün kodu kopyalandı");
                }}
              >
                <Image
                  source={require("@assets/images/copy.png")}
                  style={{
                    tintColor: "#cecece",
                    width: moderateScale(12, 0.4),
                    height: moderateScale(12, 0.4),
                  }}
                />
              </TouchableOpacity>
            </View>
          )}
          {this.props.layout === "large" && (
            <Text
              onLongPress={() => {
                Clipboard.setString(
                  this.props.item.UrunSeriNo === ""
                    ? this.props.item.UrunAciklamasi
                    : this.props.item.UrunAdi,
                );
                this.props.showToast("Ürün açıklaması kopyalandı");
              }}
              onPress={() => {
                this.props.navigation.navigate({
                  key: this.props.search ? "search" : "ProductDetailsScreen",
                  routeName: "ProductDetailsScreen",
                  params: {
                    UrunKodu: this.props.item.UrunKodu,
                    SeriNo: this.props.item.UrunSeriNo,
                    isFavorite: false,
                    pb: splittedPb != null ? splittedPb[splittedPb.length - 1] : "TRY",
                  },
                });
              }}
              style={styles.aciklamaTextLarge}
            >
              {this.props.item.UrunSeriNo === ""
                ? this.props.item.UrunAciklamasi
                : this.props.item.UrunAdi}
            </Text>
          )}
          {}
          <View style={this.props.layout === "small" ? styles.pictureContainerSmall : {}}>
            <SmartImage
              showLoadingIndicator={false}
              source={{ uri: this.props.item.UrunResimBuyuk }}
              resizeMode="contain"
              style={this.props.layout === "large" ? styles.pictureLarge : styles.pictureSmall}
            />
          </View>
          {this.props.layout === "small" && (
            <View style={{ flexDirection: "column", flex: 0.5 }}>
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  alignSelf: "baseline",
                }}
              >
                <Text
                  style={{
                    fontSize: 11,
                    color: "#68aef2",
                    fontFamily: "Poppins-Bold",
                    letterSpacing: 0.5,
                    marginRight: 5,
                  }}
                >
                  {this.props.item.UrunMarkaText}
                </Text>
              </View>
              <View style={{ flex: 1, flexDirection: "row" }}>
                <Text
                  numberOfLines={1}
                  ellipsizeMode="tail"
                  onPress={() => {
                    Clipboard.setString(this.props.item.UrunKodu);
                    this.props.showToast("Ürün kodu kopyalandı");
                  }}
                  style={{
                    marginRight: moderateScale(5, 0.4),
                    fontSize: 11,
                    color: "gray",
                    fontFamily: "Poppins",
                    flexWrap: "wrap",
                  }}
                >
                  {this.props.item.UrunKodu}
                </Text>
                <TouchableOpacity
                  onPress={() => {
                    Clipboard.setString(this.props.item.UrunKodu);
                    this.props.showToast("Ürün kodu kopyalandı");
                  }}
                >
                  <Image
                    source={require("@assets/images/copy.png")}
                    style={{
                      tintColor: "#bebebe",
                      width: moderateScale(12, 0.4),
                      height: moderateScale(12, 0.4),
                    }}
                  />
                </TouchableOpacity>
              </View>
              <Text
                onLongPress={() => {
                  Clipboard.setString(
                    this.props.item.UrunSeriNo === ""
                      ? this.props.item.UrunAciklamasi
                      : this.props.item.UrunAdi,
                  );
                  this.props.showToast("Ürün açıklaması kopyalandı");
                }}
                onPress={() => {
                  this.props.navigation.navigate({
                    key: this.props.search ? "search" : "ProductDetailsScreen",
                    routeName: "ProductDetailsScreen",
                    params: {
                      UrunKodu: this.props.item.UrunKodu,
                      SeriNo: this.props.item.UrunSeriNo,
                      isFavorite: false,
                      pb: splittedPb != null ? splittedPb[splittedPb.length - 1] : "TRY",
                    },
                  });
                }}
                style={styles.aciklamaTextSmall}
              >
                {this.props.item.UrunSeriNo === ""
                  ? this.props.item.UrunAciklamasi
                  : this.props.item.UrunAdi}
              </Text>
            </View>
          )}
          {this.props.layout === "small" && (
            <View
              style={{
                flexDirection: "column",
                flex: 0.2,
                alignItems: "flex-end",
              }}
            >
              <TextInput
                ref={(c) => (this.quantityInput = c)}
                onFocus={() => {
                  this.quantityInput.measure((fx, fy, width, height, px, py) => {
                    this.props.scrollToFocusedInput(py);
                  });
                }}
                selectTextOnFocus={true}
                returnKeyType="done"
                value={this.state.number}
                onChangeText={(text) => this.setState({ number: text })}
                keyboardType="numeric"
                style={{
                  fontSize: moderateScale(14, 0.4),
                  width: moderateScale(38, 0.5),
                  height: moderateScale(42, 0.5),
                  padding: 0,
                  borderRadius: 2,
                  borderLeftColor: "#ececec",
                  borderRightColor: "#e5e5e5",
                  borderTopColor: "#dedede",
                  borderWidth: 1,
                  borderBottomWidth: 0,
                  textAlign: "center",
                  backgroundColor: "#f5f4f5",
                }}
              />
            </View>
          )}
        </View>
        <View
          style={{
            flexDirection: this.props.layout === "large" ? "column" : "row",
            flex: 1,
          }}
        >
          {this.props.layout === "large" && this.renderPrices()}
          {this.props.layout === "small" && (
            <View style={{ flexDirection: "row", flex: 1 }}>
              <View
                style={{
                  flexDirection: "column",
                  flex: 0.3,
                  justifyContent: "flex-end",
                }}
              >
                <Text
                  style={{
                    textAlign: "center",
                    fontSize: 12,
                    fontFamily: "Poppins",
                    color: this.props.item.UrunStokDurumu === "Var" ? "#4CAF50" : "gray",
                  }}
                >
                  {this.props.item.UrunStokDurumu === "Var"
                    ? "Stokta"
                    : "Stok " + this.props.item.UrunStokDurumu}
                </Text>
              </View>
              <View
                style={{
                  flexDirection: "column",
                  flex: 0.5,
                  justifyContent: "flex-start",
                }}
              >
                {this.renderPrices()}
              </View>
              <View
                style={{
                  flexDirection: "column",
                  flex: 0.2,
                  alignItems: "flex-end",
                }}
              >
                <TouchableOpacity
                  onPress={() => this.props.addToCart(this.props.item, this.state.number)}
                  style={{
                    width: moderateScale(38, 0.5),
                    height: moderateScale(42, 0.5),
                    borderColor: colors.light_green,
                    borderWidth: 1,
                    borderRadius: 2,
                    backgroundColor: colors.light_green,
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  <Image
                    source={require("@assets/images/cart.png")}
                    style={{
                      width: moderateScale(16, 0.5),
                      height: moderateScale(16, 0.5),
                      tintColor: "white",
                    }}
                  />
                </TouchableOpacity>
              </View>
            </View>
          )}
          {this.props.layout === "large" && (
            <Text
              style={{
                fontSize: 15,
                fontFamily: "Poppins",
                color: this.props.item.UrunStokDurumu === "Var" ? "#4CAF50" : "gray",
              }}
            >
              {this.props.item.UrunStokDurumu === "Var"
                ? "Stokta"
                : "Stok " + this.props.item.UrunStokDurumu}
            </Text>
          )}
          {this.props.layout === "large" && (
            <View
              style={{
                flexDirection: "row",
                flex: 1,
                height: moderateScale(40, 0.5),
                justifyContent: "space-between",
                marginTop: moderateScale(10, 0.5),
              }}
            >
              <TextInput
                ref={(c) => (this.quantityInput = c)}
                onFocus={() => {
                  this.quantityInput.measure((fx, fy, width, height, px, py) => {
                    this.props.scrollToFocusedInput(py);
                  });
                }}
                selectTextOnFocus={true}
                returnKeyType="done"
                value={this.state.number}
                onChangeText={(text) => this.setState({ number: text })}
                keyboardType="numeric"
                style={{
                  borderRadius: 3,
                  fontSize: moderateScale(14, 0.4),
                  borderLeftColor: "#ececec",
                  borderRightColor: "#e5e5e5",
                  borderTopColor: "#dedede",
                  borderWidth: 1,
                  borderBottomWidth: 0,
                  height: moderateScale(40, 0.5),
                  flex: 0.12,
                  textAlign: "center",
                  backgroundColor: "#f5f4f5",
                }}
              />
              <TouchableOpacity
                onPress={() => this.props.addToCart(this.props.item, this.state.number)}
                style={{
                  flexDirection: "row",
                  flex: 0.85,
                  backgroundColor: colors.green,
                  paddingVertical: 4,
                  alignItems: "center",
                  justifyContent: "center",
                  borderRadius: 2,
                }}
              >
                <Image
                  source={require("@assets/images/cart.png")}
                  style={{
                    marginRight: moderateScale(10, 0.5),
                    width: moderateScale(17, 0.5),
                    height: moderateScale(17, 0.5),
                    tintColor: "white",
                  }}
                />
                <Text style={{ color: "white", fontSize: 18 }}>Sepete Ekle</Text>
              </TouchableOpacity>
            </View>
          )}
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = ScaledSheet.create({
  price: {
    color: "#db4b14",
    fontSize: 16,
    flexWrap: "wrap",
    fontFamily: "Poppins",
  },
  containerLarge: {
    flex: 1,
    padding: win.width / 25,
    backgroundColor: "white",
    marginBottom: win.width / 25,
    marginHorizontal: win.width / 24,
  },
  aciklamaTextLarge: {
    fontFamily: "Poppins",
    fontSize: 16,
    lineHeight: 17,
    marginTop: 8,
  },
  pictureLarge: {
    marginVertical: moderateScale(16, 0.4),
    width: win.width / 2,
    height: (win.width / 2) * 0.62,
    resizeMode: "contain",
    alignSelf: "center",
  },
  pictureSmall: {
    width: win.width / 6,
    height: win.width / 6,
    resizeMode: "contain",
  },
  containerSmall: {
    flexDirection: "column",
    flex: 1,
    padding: "8@ms0.5",
    borderBottomColor: "rgba(0,0,0,0.2)",
    borderBottomWidth: 1,
    marginBottom: win.width / 25,
    backgroundColor: "white",
    marginHorizontal: win.width / 24,
  },
  aciklamaTextSmall: {
    marginTop: 6,
    marginBottom: 6,
    color: colors.txt_description,
    fontSize: 12,
    flexWrap: "wrap",
  },
  pictureContainerSmall: {
    flexDirection: "column",
    flex: 0.3,
    alignItems: "center",
    justifyContent: "center",
  },
});
