import {
  GET_SEARCH_HISTORY_SUCCESS,
  GET_SEARCH_HISTORY_FAILURE,
  ADD_TO_SEARCH_HISTORY_SUCCESS,
  CLEAR_SEARCH_HISTORY_SUCCESS,
  REMOVE_FROM_SEARCH_HISTORY_SUCCESS,
  REMOVE_FROM_SEARCH_HISTORY_FAILURE,
} from "@redux/actions/types";

const initialState = {
  items: [],
  error: false,
};

export default function searchHistory(state = initialState, action) {
  switch (action.type) {
    case GET_SEARCH_HISTORY_SUCCESS:
      return {
        ...state,
        items: action.data ? action.data : [],
        error: false,
      };
    case GET_SEARCH_HISTORY_FAILURE:
      return {
        ...state,
        error: true,
      };
    case ADD_TO_SEARCH_HISTORY_SUCCESS:
      return {
        ...state,
        items: action.data,
      };
    case CLEAR_SEARCH_HISTORY_SUCCESS:
      return {
        ...state,
        items: [],
        error: false,
      };
    case REMOVE_FROM_SEARCH_HISTORY_SUCCESS:
      return {
        ...state,
        items: action.data,
        error: false,
      };
    case REMOVE_FROM_SEARCH_HISTORY_FAILURE:
      return {
        ...state,
        error: true,
      };
    default:
      return state;
  }
}
