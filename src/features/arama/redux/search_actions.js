import {
  CLEAR_SEARCH,
  FETCHING_SEARCH,
  FETCHING_SEARCH_SUCCESS,
  FETCHING_SEARCH_FAILURE,
} from "@redux/actions/types";
import { getEndPoint } from "@common/Utils";
import bugsnag from "@common/bugsnag_config";
var visilabsManager = require("@app/visilabsManager");

function addExtraFilters(responseJson) {
  if (
    responseJson.IlFiyatlar != null &&
    responseJson.IlFiyatlar.length != null &&
    responseJson.IlFiyatlar.length > 0
  ) {
    responseJson.IlOzellikler.unshift({
      OzellikTanim: "Fiyat",
      IlSiraliOzellikOzet: responseJson.IlFiyatlar,
      OzelliktekiToplamUrun: responseJson.ToplamBulunanUrunSayisi,
    });
  }
  if (
    responseJson.IlMarkalar != null &&
    responseJson.IlMarkalar.length != null &&
    responseJson.IlMarkalar.length > 0
  ) {
    responseJson.IlOzellikler.unshift({
      OzellikTanim: "Marka",
      IlSiraliOzellikOzet: responseJson.IlMarkalar,
      OzelliktekiToplamUrun: responseJson.ToplamBulunanUrunSayisi,
    });
  }
  if (
    responseJson.IlHiyerarsiler != null &&
    responseJson.IlHiyerarsiler.length != null &&
    responseJson.IlHiyerarsiler.length > 0
  ) {
    responseJson.IlOzellikler.unshift({
      OzellikTanim: "Kategori",
      IlSiraliOzellikOzet: responseJson.IlHiyerarsiler,
      OzelliktekiToplamUrun: responseJson.ToplamBulunanUrunSayisi,
    });
  }
}

export function fetchSearchFromApi(query = "", refresh = false, searchParams) {
  console.log("query", query);
  return (dispatch, getState) => {
    return new Promise(function (resolve, reject) {
      console.log("search action", searchParams);
      var prepared_url =
        getEndPoint("SEARCH_URL") +
        "?Si=" +
        getState().session.info.SessionID +
        "&YA=1" +
        "&SearchText=" +
        query +
        "&ss=10";
      if (searchParams) {
        var keys = Object.keys(searchParams);
        keys.forEach((key) => {
          if (key && searchParams[key] !== "") prepared_url += "&" + key + "=" + searchParams[key];
        });
      }
      dispatch(getProducts(refresh));
      fetch(prepared_url)
        .then((response) => response.json())
        .then((responseJson) => {
          addExtraFilters(responseJson);
          if (responseJson.MesajTipi === "S") {
            dispatch(getProductsSuccess(responseJson, refresh));
            resolve(responseJson.IlUrunListesi);
          } else {
            dispatch(getProductsFailure());
            resolve(responseJson);
          }
          var keyword = "";
          if (query === "") {
            if (searchParams?.Hiyerarsi && searchParams?.Hiyerarsi !== "")
              keyword = searchParams?.Hiyerarsi;
            else if (searchParams?.Marka && searchParams?.Marka !== "")
              keyword = searchParams?.Marka;
          } else {
            keyword = query;
          }
          var data = {
            "OM.oss": keyword,
            "OM.ossr": responseJson.ToplamBulunanUrunSayisi + "",
          };
          visilabsManager.customEvent("In App Search", data);
        })
        .catch((error) => {
          reject(error);
          bugsnag.notify(error, (report) => {
            report.errorClass = "fetchSearchFromApi";
          });
          getProductsFailure();
        });
    });
  };
}

export function moreProducts(page) {
  console.log("more prods", page);
  return async (dispatch, getState) => {
    var prepared_url =
      getEndPoint("SEARCH_URL") + "?Si=" + getState().session.info.SessionID + "&Sn=" + page;
    console.log("more products url", prepared_url);
    var keys = Object.keys(getState().filters.params);
    keys.forEach((key) => {
      if (key && getState().filters.params[key] !== "")
        prepared_url += "&" + key + "=" + getState().filters.params[key];
    });
    dispatch(getProducts(false, true));
    await fetch(prepared_url)
      .then((response) => response.json())
      .then((responseJson) => {
        console.log("more prods resp", responseJson);
        if (responseJson.SuankiSayfa === page && page > 1) {
          addExtraFilters(responseJson);
          dispatch(getProductsSuccess(responseJson, false));
        } else {
          dispatch(getProductsFailure(true));
        }
      })
      .catch((error) => {
        bugsnag.notify(error, (report) => {
          report.errorClass = "moreProducts";
        });
        dispatch(getProductsFailure());
      });
  };
}

export function filterSearch(params, ya) {
  console.log("filter params", params);

  return (dispatch, getState) => {
    var prepared_url = "";
    if (params.FiltreTipi === "EFiyat") {
      prepared_url =
        getEndPoint("SEARCH_URL") +
        "?Si=" +
        getState().session.info.SessionID +
        "&ss=10&fb=" +
        params.MinFiyat +
        "&fi=" +
        params.MaxFiyat;
    } else if (params.FiltreTipi === "BMarka") {
      prepared_url =
        getEndPoint("SEARCH_URL") +
        "?Si=" +
        getState().session.info.SessionID +
        "&ss=10&ft=BMarka" +
        "&fd=" +
        params.od;
    } else if (params.FiltreTipi === "DOzellik") {
      prepared_url = getEndPoint("SEARCH_URL") + "?Si=" + getState().session.info.SessionID;
      prepared_url += "&ft=" + params.ft;
      prepared_url += "&fd=" + params.fd;
      prepared_url += "&od=" + params.od;
    } else if (params.FiltreTipi === "AHiyerarsi") {
      prepared_url = getEndPoint("SEARCH_URL") + "?Si=" + getState().session.info.SessionID;
      prepared_url += "&ft=AHiyerarsi";
      prepared_url += "&fd=" + params.od;
    } else {
      prepared_url = getEndPoint("SEARCH_URL") + "?Si=" + getState().session.info.SessionID;
      Object.keys(params).forEach((paramKey) => {
        prepared_url += "&" + paramKey + "=" + params[paramKey];
      });
    }
    if (ya) {
      prepared_url += "&ya=1";
    }
    dispatch(getProducts(true));
    fetch(prepared_url)
      .then((response) => response.json())
      .then((responseJson) => {
        console.log("filter response", responseJson);
        addExtraFilters(responseJson);
        if (responseJson.MesajTipi === "S") dispatch(getProductsSuccess(responseJson, true));
        else dispatch(getProductsFailure());
      })
      .catch((error) => {
        bugsnag.notify(error, (report) => {
          report.errorClass = "filterSearch";
        });
        dispatch(getProductsFailure());
      });
  };
}

export function removeFilter(id) {
  return (dispatch, getState) => {
    var prepared_url =
      getEndPoint("SEARCH_URL") +
      "?Si=" +
      getState().session.info.SessionID +
      "&fk=" +
      id +
      "&ss=10";
    dispatch(getProducts(true));
    fetch(prepared_url)
      .then((response) => response.json())
      .then((responseJson) => {
        addExtraFilters(responseJson);
        dispatch(getProductsSuccess(responseJson, true));
      })
      .catch((error) => {
        bugsnag.notify(error, (report) => {
          report.errorClass = "removeFilter";
        });
        dispatch(getProductsFailure());
      });
  };
}
export function clearSearch() {
  return async (dispatch) => {
    dispatch({ type: CLEAR_SEARCH });
  };
}

function getProducts(refresh, more = false) {
  return {
    type: FETCHING_SEARCH,
    more,
    refresh,
  };
}

function getProductsSuccess(data, refresh) {
  return {
    type: FETCHING_SEARCH_SUCCESS,
    data,
    refresh,
  };
}

function getProductsFailure(noMore) {
  ("getProductsFailure");
  return {
    type: FETCHING_SEARCH_FAILURE,
    noMore,
  };
}
