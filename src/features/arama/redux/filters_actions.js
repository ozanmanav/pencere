import { SET_FILTER, CLEAR_FILTERS } from "../../../redux/actions/types";

export function setFilter(newParams) {
  return (dispatch, getState) => {
    return new Promise((resolve) => {
      var params = getState().filters.params;
      Object.keys(newParams).forEach((key) => {
        params[key] = newParams[key];
      });
      dispatch({ type: SET_FILTER, params });
      resolve(params);
    });
  };
}

export function clearFilters() {
  return (dispatch) => {
    dispatch({ type: CLEAR_FILTERS });
  };
}
