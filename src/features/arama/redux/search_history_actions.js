import {
  GET_SEARCH_HISTORY_SUCCESS,
  GET_SEARCH_HISTORY_FAILURE,
  ADD_TO_SEARCH_HISTORY_SUCCESS,
  ADD_TO_SEARCH_HISTORY_FAILURE,
  CLEAR_SEARCH_HISTORY_SUCCESS,
  CLEAR_SEARCH_HISTORY_FAILURE,
  REMOVE_FROM_SEARCH_HISTORY_FAILURE,
  REMOVE_FROM_SEARCH_HISTORY_SUCCESS,
} from "@redux/actions/types";
import AsyncStorage from "@react-native-community/async-storage";
import bugsnag from "@common/bugsnag_config";

export function addToSearchHistory(q) {
  console.log("addToSearchHistory", q);
  return (dispatch, getState) => {
    console.log("addToSearchHistory", q);
    var arr = getState().searchHistory.items;
    arr.unshift(q);
    arr = arr.slice(0, 10);
    arr = [...new Set(arr)];
    AsyncStorage.setItem("searchHistory", JSON.stringify(arr))
      .then(() => {
        dispatch({ type: ADD_TO_SEARCH_HISTORY_SUCCESS, data: arr });
        getSearchHistory();
      })
      .catch((err) => {
        bugsnag.notify(err, (report) => {
          report.errorClass = "addToSearchHistory";
        });
        dispatch({ type: ADD_TO_SEARCH_HISTORY_FAILURE });
      });
  };
}

export function clearSearchHistory() {
  return (dispatch) => {
    AsyncStorage.removeItem("searchHistory")
      .then(() => {
        dispatch({ type: CLEAR_SEARCH_HISTORY_SUCCESS });
      })
      .catch((err) => {
        bugsnag.notify(err, (report) => {
          report.errorClass = "clearSearchHistory";
        });
        dispatch({ type: CLEAR_SEARCH_HISTORY_FAILURE });
        console.log(err);
      });
  };
}

export function removeFromSearchHistory(index) {
  return (dispatch, getState) => {
    var searchHistory = getState().searchHistory.items;
    console.log("the history", searchHistory);
    searchHistory.splice(index, 1);
    console.log("removed history", searchHistory);
    AsyncStorage.setItem("searchHistory", JSON.stringify(searchHistory))
      .then(() => {
        dispatch({ type: REMOVE_FROM_SEARCH_HISTORY_SUCCESS, data: searchHistory });
      })
      .catch((err) => {
        bugsnag.notify(err, (report) => {
          report.errorClass = "removeFromSearchHistory";
        });
        dispatch({ type: REMOVE_FROM_SEARCH_HISTORY_FAILURE });
      });
  };
}

export function getSearchHistory() {
  return (dispatch) => {
    AsyncStorage.getItem("searchHistory")
      .then((val) => {
        dispatch({ type: GET_SEARCH_HISTORY_SUCCESS, data: JSON.parse(val) });
      })
      .catch((err) => {
        bugsnag.notify(err, (report) => {
          report.errorClass = "getSearchHistory";
        });
        dispatch({ type: GET_SEARCH_HISTORY_FAILURE });
        console.log(err);
      });
  };
}
