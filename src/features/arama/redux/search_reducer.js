import {
  FETCHING_SEARCH,
  FETCHING_SEARCH_SUCCESS,
  FETCHING_SEARCH_FAILURE,
  CLEAR_SEARCH,
} from "@redux/actions/types";

const initialState = {
  totalQuantity: 0,
  currentPage: 1,
  items: [],
  hierarchies: [],
  searchIds: [],
  features: [],
  currentFilters: [],
  currentSorting: "FulltextDefault",
  isFetchingSearchProducts: false,
  isRefreshingSearch: false,
  loadingMore: false,
  error: false,
  noMore: false,
};

export default function searchReducer(state = initialState, action) {
  switch (action.type) {
    case FETCHING_SEARCH:
      return {
        ...state,
        isFetchingSearchPosts: !action.refresh,
        isRefreshingSearch: action.refresh,
        loadingMore: action.more,
        noMore: !action.more,
        items: action.refresh ? [] : state.items,
      };
    case CLEAR_SEARCH:
      return {
        ...state,
        items: [],
        noMore: false,
      };
    case FETCHING_SEARCH_SUCCESS:
      var lastIndex = state.items.length > 0 ? state.items[state.items.length - 1].index : 0;
      if (action?.data?.IlUrunListesi?.length) {
        action.data.IlUrunListesi.map((item, i) => {
          action.data.IlUrunListesi[i].index = lastIndex + i + 1;
        });
      }

      return {
        ...state,
        isFetchingSearchProducts: false,
        isRefreshingSearch: false,
        loadingMore: false,
        items: action.refresh
          ? action.data.IlUrunListesi
          : state.items.concat(action.data.IlUrunListesi),
        currentSorting: action.data.SuankiSiralamaTipi,
        currentPage: action.data.SuankiSayfa,
        totalQuantity: action.data.ToplamBulunanUrunSayisi,
        hierarchies: action.data.IlHiyerarsiler,
        searchIds: action.data.idSiralamaTipleri,
        features: action.data.IlOzellikler,
        currentFilters: action.data.IlFiltreler,
      };
    case FETCHING_SEARCH_FAILURE:
      console.log("FETCHING_SEARCH_FAILURE");
      return {
        ...state,
        isFetchingSearchProducts: false,
        isRefreshingSearch: false,
        loadingMore: false,
        searchIds: [],
        error: true,
        noMore: action.noMore,
      };
    default:
      return state;
  }
}
