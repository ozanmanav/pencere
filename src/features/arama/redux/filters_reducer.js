const INITAL_STATE = {
  params: { st: "", Hiyerarsi: "", Marka: "", ft: "", fd: "", od: "" },
};
import { SET_FILTER, CLEAR_FILTERS } from "@redux/actions/types";

export default function filterReducer(state = INITAL_STATE, action) {
  switch (action.type) {
    case SET_FILTER:
      console.log("SET_FILTER", action);
      return {
        ...state,
        params: action.params,
      };
    case CLEAR_FILTERS:
      console.log("CLEAR_FILTERS", action);
      return {
        ...state,
        params: { st: "", Hiyerarsi: "", Marka: "", ft: "", fd: "", od: "" },
      };
    default:
      return state;
  }
}
