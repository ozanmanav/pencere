import React, { Component } from "react";
import {
  SafeAreaView,
  View,
  Image,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
  ActivityIndicator,
  Animated,
  FlatList,
  Platform,
  StatusBar,
  LayoutAnimation,
} from "react-native";
import { Text, SimpleHeader } from "@components";
import { connect } from "react-redux";
import { filterSearch, removeFilter } from "@redux/actions";
import colors from "@common/styles/colors";
import { moderateScale, scale } from "react-native-size-matters";
import { CREATE_SPRINGXY, DELETE_SPRINGXY } from "@common/LayoutAnimations";

const NAVBAR_HEIGHT = 100;
const STATUS_BAR_HEIGHT = 0;
const win = Dimensions.get("window");

var SpringYLayoutAnim = {
  duration: 400,
  create: {
    type: LayoutAnimation.Types.spring,
    property: LayoutAnimation.Properties.scaleY,
    springDamping: 0.9,
  },
  update: {
    type: LayoutAnimation.Types.spring,
    property: LayoutAnimation.Properties.scaleY,
    springDamping: 0.9,
  },
};
class SearchFilterScreen extends Component {
  static navigationOptions = {
    tabBarIcon: ({ tintColor }) => (
      <Image source={require("@assets/images/search.png")} style={[styles.icon, { tintColor }]} />
    ),
  };

  componentDidMount() {
    this.willBlurSubscription = this.props.navigation.addListener("willBlur", () => {
      this.setState({ detailView: false });
    });
  }

  componentWillUnmount() {
    this.willBlurSubscription.remove();
  }

  constructor(props) {
    super(props);
    const scrollAnim = new Animated.Value(0);
    const offsetAnim = new Animated.Value(0);
    this.state = {
      fd: "",
      selectedFeatureDetails: [],
      detailView: false,
      scrollAnim,
      clampedScroll: Animated.diffClamp(
        Animated.add(
          scrollAnim.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 1],
            extrapolateLeft: "clamp",
          }),
          offsetAnim,
        ),
        0,
        NAVBAR_HEIGHT - STATUS_BAR_HEIGHT,
      ),
    };
  }

  onBackPressed = () => {
    if (this.state.detailView) {
      this.setState({ detailView: false });
    } else
      this.props.navigation.navigate("SearchScreen", {
        focus: false,
        reload: false,
      });
  };

  renderItem(item) {
    if (!this.state.detailView)
      return (
        <TouchableOpacity
          style={styles.row}
          onPress={() => {
            this.setState({
              detailView: true,
              selectedFeatureDetails: item.IlSiraliOzellikOzet,
              fd: item.OzellikTanim,
            });
            LayoutAnimation.configureNext(SpringYLayoutAnim);
          }}
        >
          <Text style={{ fontSize: 14, color: "gray" }}>
            {item.OzellikTanim + " (" + item.OzelliktekiToplamUrun + ")"}
          </Text>
        </TouchableOpacity>
      );
    else {
      console.log("see this", item.FiltreTipi + "//" + item.Anahtar);
      return (
        <TouchableOpacity
          style={styles.row}
          onPress={() => {
            let params = {};
            params.ft = item.FiltreTipi ?? "";
            params.fd = this.state.fd ?? "";
            params.od = item.Anahtar ?? "";
            Object.assign(item, params);
            this.props.filterSearch({ ...item });
            LayoutAnimation.configureNext(CREATE_SPRINGXY);
            this.setState({ detailView: false });
          }}
        >
          <Text style={{ fontSize: 14, color: "gray" }}>{item.Text + " (" + item.Adet + ")"}</Text>
          {this.props.search.currentFilters.filter(
            i =>
              i.FiltreTipi + "//" + i.Deger === item.FiltreTipi + "//" + item.Anahtar ||
              (i.FiltreTipi === "EFiyat" && item.Anahtar === "" + i.MinimumFiyat + i.MaximumFiyat),
          ).length > 0 && (
            <Image
              source={require("@assets/images/checked.png")}
              style={{
                marginRight: moderateScale(10, 0.5),
                width: moderateScale(14, 0.5),
                height: moderateScale(14, 0.5),
                tintColor: "#8BC34A",
                resizeMode: "contain",
              }}
            />
          )}
        </TouchableOpacity>
      );
    }
  }

  render() {
    return (
      <SafeAreaView
        style={{ backgroundColor: colors.primary_dark, flex: 1 }}
        forceInset={{ top: "always" }}
      >
        <SimpleHeader
          style={{
            backgroundColor: colors.primary_dark,
            borderColor: colors.primary_dark,
          }}
          titleStyle={{ color: "white" }}
          iconColor="white"
          navigation={this.props.navigation}
          title="Filtreler"
          button={() => (
            <View style={{ flexDirection: "row", alignItems: "center" }}>
              {this.props.search.isRefreshingSearch && (
                <ActivityIndicator color="white" size="small" />
              )}
              <TouchableOpacity
                style={{ padding: scale(5, 0.4) }}
                onPress={() => {
                  this.props.navigation.navigate({
                    key: "SearchScreen",
                    routeName: "SearchScreen",
                    focus: false,
                    reload: false,
                  });
                }}
              >
                <Text
                  fontScale={0.6}
                  style={{
                    color: "white",
                    fontSize: 16,
                    fontFamily: "Poppins-SemiBold",
                  }}
                >
                  Tamam
                </Text>
              </TouchableOpacity>
            </View>
          )}
          onBackPress={() => this.props.navigation.goBack()}
        />
        <View
          style={{
            width: win.width,
            backgroundColor: "#efefef",
            flexGrow: 1,
            marginTop:
              moderateScale(50, 0.4) +
              (Platform.OS === "android"
                ? Platform.Version < 20
                  ? 0
                  : StatusBar.currentHeight
                : 0),
          }}
        >
          {this.props.search.currentFilters.length > 0 && (
            <View
              onLayout={({
                nativeEvent: {
                  layout: { height },
                },
              }) => (this.cFiltersHeight = height)}
              style={{ flexDirection: "column" }}
            >
              <View
                style={{
                  flexDirection: "row",
                  flexWrap: "wrap",
                  backgroundColor: "#B0BEC5",
                  width: win.width,
                  padding: moderateScale(10, 0.5),
                }}
              >
                {this.props.search.currentFilters.map(filter => {
                  return (
                    <TouchableOpacity
                      onPress={() => {
                        this.props.removeFilter(filter.FiltreKey);
                        if (Platform.OS === "ios") LayoutAnimation.configureNext(DELETE_SPRINGXY);
                      }}
                      style={{
                        backgroundColor: "#607D8B",
                        paddingRight: moderateScale(12, 0.5),
                        paddingLeft: moderateScale(4, 0.5),
                        borderRadius: 2,
                        justifyContent: "space-between",
                        alignItems: "center",
                        marginHorizontal: moderateScale(5, 0.5),
                        marginBottom: moderateScale(5, 0.5),
                        flexDirection: "row",
                      }}
                    >
                      {filter.FiltreTipi === "DOzellik" && (
                        <View style={{ flexDirection: "column" }}>
                          <Text style={{ color: "white", fontSize: 11 }}>{filter.Tanim}</Text>
                          <Text style={{ color: "white", fontSize: 9 }}>{filter.Deger}</Text>
                        </View>
                      )}
                      {filter.FiltreTipi === "EFiyat" && (
                        <View style={{ flexDirection: "column" }}>
                          <Text style={{ color: "white", fontSize: 11 }}>{"Fiyat"}</Text>
                          <Text style={{ color: "white", fontSize: 9 }}>
                            {filter.MinimumFiyat + "-" + filter.MaximumFiyat}
                          </Text>
                        </View>
                      )}
                      {filter.FiltreTipi === "BMarka" && (
                        <View style={{ flexDirection: "column" }}>
                          <Text style={{ color: "white", fontSize: 11 }}>{"Marka"}</Text>
                          <Text style={{ color: "white", fontSize: 9 }}>{filter.Deger}</Text>
                        </View>
                      )}
                      {filter.FiltreTipi === "CStok" && (
                        <View style={{ flexDirection: "column" }}>
                          <Text style={{ color: "white", fontSize: 11 }}>{"Stok"}</Text>
                          <Text style={{ color: "white", fontSize: 9 }}>{filter.Deger}</Text>
                        </View>
                      )}
                      {filter.FiltreTipi === "AHiyerarsi" && (
                        <View style={{ flexDirection: "column" }}>
                          <Text style={{ color: "white", fontSize: 11 }}>{"Kategori"}</Text>
                          <Text style={{ color: "white", fontSize: 9 }}>{filter.Deger}</Text>
                        </View>
                      )}
                      <Text
                        style={{
                          color: "white",
                          fontSize: 9,
                          position: "absolute",
                          top: 0,
                          right: scale(2),
                        }}
                      >
                        x
                      </Text>
                    </TouchableOpacity>
                  );
                })}
              </View>
              {this.props.search.currentFilters.length > 0 && (
                <TouchableOpacity
                  style={[styles.row, { marginBottom: 0 }]}
                  onPress={() => {
                    this.props.search.currentFilters.map(filter => {
                      this.props.removeFilter(filter.FiltreKey);
                    });
                  }}
                >
                  <Text
                    style={{
                      fontFamily: "Poppins",
                      fontSize: 14,
                      fontWeight: "500",
                      color: colors.price_color,
                    }}
                  >
                    Tüm Filtreleri Kaldır
                  </Text>
                </TouchableOpacity>
              )}
            </View>
          )}
          {this.props.search.features.length === 0 && (
            <View style={{ padding: 20, alignItems: "center" }}>
              <Text style={{ fontSize: 16, color: "gray" }}>Filtre listesi boş</Text>
            </View>
          )}
          {
            <FlatList
              removeClippedSubviews={false}
              overScrollMode="never"
              scrollIndicatorInsets={{
                //top: this.props.search.currentFilters.length > 0 ? 0 : moderateScale(50, .4),
                left: 0,
                bottom: this.props.search.currentFilters.length > 0 ? this.cFiltersHeight : 0,
                right: 0,
              }}
              contentContainerStyle={{
                flexGrow: 1,
                paddingBottom:
                  this.props.search.currentFilters.length > 0 ? this.cFiltersHeight : 0,
              }}
              data={
                this.state.detailView
                  ? this.state.selectedFeatureDetails
                  : this.props.search.features
              }
              renderItem={({ item }) => this.renderItem(item)}
              stickyHeaderIndices={[0]}
              ListHeaderComponent={() => {
                if (this.state.detailView)
                  return (
                    <View
                      style={{
                        flexDirection: "column",
                        marginBottom: moderateScale(10, 0.5),
                      }}
                    >
                      {this.state.detailView && (
                        <TouchableOpacity
                          style={[
                            styles.row,
                            {
                              justifyContent: "flex-start",
                              marginBottom: 0,
                            },
                          ]}
                          onPress={() => {
                            this.setState({ detailView: false });
                            LayoutAnimation.configureNext(SpringYLayoutAnim);
                          }}
                        >
                          <Image
                            style={[
                              {
                                marginRight: moderateScale(5, 0.5),
                                width: moderateScale(14, 0.5),
                                height: moderateScale(14, 0.5),
                                tintColor: this.props.iconColor || "#212121",
                              },
                            ]}
                            source={require("@assets/images/left-arrow-mini.png")}
                          />
                          <Text
                            style={{
                              fontFamily: "Poppins",
                              fontSize: 14,
                              fontWeight: "500",
                              color: "gray",
                            }}
                          >
                            {this.state.fd}
                          </Text>
                        </TouchableOpacity>
                      )}
                    </View>
                  );
                else return null;
              }}
            />
          }
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  icon: {
    width: 26,
    height: 26,
  },
  row: {
    paddingVertical: 10,
    paddingLeft: 10,
    backgroundColor: "white",
    borderBottomWidth: 1,
    borderBottomColor: "#e4e4e4",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
});

function mapStateToProps(state) {
  return {
    search: state.search,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    removeFilter: id => dispatch(removeFilter(id)),
    filterSearch: params => dispatch(filterSearch(params)),
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(SearchFilterScreen);
