import React, { Component } from "react";
import {
  View,
  StyleSheet,
  Dimensions,
  ActivityIndicator,
  Animated,
  SafeAreaView,
  FlatList,
} from "react-native";
import { GridProductThumb, AnimatedHeader } from "@components";
import colors from "@common/styles/colors";
import { fetchSeeAllProductsFromApi, clearAllProducts } from "@redux/actions";
import { connect } from "react-redux";

var { width } = Dimensions.get("window");
const NAVBAR_HEIGHT = AnimatedHeader.height;
const STATUS_BAR_HEIGHT = 0;

const AnimatedListView = Animated.createAnimatedComponent(FlatList);

const win = Dimensions.get("window");

class GridScreen extends Component {
  page = 1;

  constructor(props) {
    super(props);
    const scrollAnim = new Animated.Value(0);
    const offsetAnim = new Animated.Value(0);

    this.state = {
      products: [],
      scrollAnim,
      clampedScroll: Animated.diffClamp(
        Animated.add(
          scrollAnim.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 1],
            extrapolateLeft: "clamp",
          }),
          offsetAnim,
        ),
        0,

        NAVBAR_HEIGHT - STATUS_BAR_HEIGHT,
      ),
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      prevProps.navigation.state.params.category !== this.props.navigation.state.params.category
    ) {
      this.page = 1;
      this.listview.scrollTo({ x: 0, y: 0, animated: false });
      this.props.getAllProducts(
        this.page,
        this.props.navigation.state.params.category,
        false,
        true,
      );
    }
  }

  componentWillUnmount() {
    this.props.clearAllProducts();
  }
  componentDidMount() {
    this.page = 0;
    this.props.getAllProducts(1, this.props.navigation.state.params.category, false, true);
    this.state.scrollAnim.addListener(({ value }) => {
      // This is the same calculations that diffClamp does.
      this.diff = value - this._scrollValue;
      this._scrollValue = value;
      // if (this.diff > 0 && value > (dim.width * 9 / 16 + 50))
      //     this.refs.fixedBtn.startFadeAnim(false);
      // else if (this.refs.fixedBtn.state.isArrowVisible)
      //     this.refs.fixedBtn.startFadeAnim(true);

      this._clampedScrollValue = Math.min(
        Math.max(this._clampedScrollValue + this.diff, 0),
        NAVBAR_HEIGHT - STATUS_BAR_HEIGHT,
      );
    });
  }

  _genRows = () => {
    if (this.props.products.seeAllProducts.length / 25 + 1 > this.page) {
      this.page++;
      this.props.getAllProducts(
        this.page,
        this.props.navigation.state.params.category,
        false,
        false,
      );
    }
  };

  _onRefresh() {
    this.props.getAllProducts(1, this.props.navigation.state.params.category, true, true);
  }

  _renderRow(rowData) {
    return (
      <GridProductThumb
        onPress={() =>
          this.props.navigation.navigate({
            key: "ProductDetailsScreen",
            routeName: "ProductDetailsScreen",
            params: {
              UrunKodu: rowData.UrunKodu,
              SessionID: this.props.navigation.state.params.SessionID,
            },
          })
        }
        product={rowData}
      />
    );
  }

  _renderFooter() {
    if (this.props.products.isFetchingAllProducts && this.page > 1)
      return (
        <View
          style={{
            flexDirection: "row",
            width: width,
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <ActivityIndicator size="large" color="#0000ff" />
        </View>
      );
    else return null;
  }
  render() {
    return (
      <SafeAreaView style={{ backgroundColor: colors.primary_dark }} forceInset={{ top: "always" }}>
        <View style={{ height: win.height, width: win.width, backgroundColor: "#efefef" }}>
          <View style={{ height: win.height, width: win.width, backgroundColor: "white" }}>
            {
              <AnimatedListView
                ref={c => (this.listview = c)}
                data={this.props.products.seeAllProducts}
                renderItem={rowData =>
                  !(this.props.products.isFetchingAllProducts && this.page === 1) &&
                  this._renderRow(rowData)
                }
                ListFooterComponent={() => this._renderFooter()}
                onEndReached={() => this._genRows()}
                keyExtractor={item => item.UrunKodu}
                scrollIndicatorInsets={{ top: 50, left: 0, bottom: 50, right: 0 }}
                contentContainerStyle={styles.list}
                keyboardShouldPersistTaps={"always"}
                scrollEventThrottle={16}
                onScroll={Animated.event(
                  [{ nativeEvent: { contentOffset: { y: this.state.scrollAnim } } }],
                  {
                    useNativeDriver: true,
                  },
                )}
              />
            }
            <AnimatedHeader
              navigation={this.props.navigation}
              title={this.props.navigation.state.params.title || "Pencere"}
              clampedScroll={this.state.clampedScroll}
              back={true}
              alignLeft={true}
            />
          </View>
          {this.props.products.isFetchingAllProducts &&
            this.props.products.seeAllProducts.length === 0 && (
              <View
                style={{
                  width: win.width,
                  height: win.height,
                  alignItems: "center",
                  justifyContent: "center",
                  backgroundColor: "rgba(0,0,0,0.3)",
                  position: "absolute",
                  top: 0,
                  left: 0,
                }}
              >
                <ActivityIndicator size="large" />
              </View>
            )}
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  list: {
    flexDirection: "row",
    justifyContent: "space-between",
    flexWrap: "wrap",
    padding: 10,
    paddingTop: 60,
  },
});

function mapStateToProps(state) {
  return {
    products: state.products,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    getAllProducts: (page, category, refresh, clear) =>
      dispatch(fetchSeeAllProductsFromApi(page, category, refresh, clear)),
    clearAllProducts: () => dispatch(clearAllProducts()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(GridScreen);
