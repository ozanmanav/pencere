import React, { Component } from "react";
import {
  Platform,
  SafeAreaView,
  StatusBar,
  Image,
  ActivityIndicator,
  View,
  TouchableOpacity,
  Animated,
  Dimensions,
  ScrollView,
  Alert,
  FlatList,
  LayoutAnimation,
  Keyboard,
} from "react-native";
import { connect } from "react-redux";
import {
  selectCategory,
  setFilter,
  filterSearch,
  moreProducts,
  addToSearchHistory,
  clearSearchHistory,
  getSearchHistory,
  setCart,
  clearFilters,
  showToast,
} from "@redux/actions";
import { Text, FixedButton, BottomPicker, AnimatedHeader, PencereIndicator } from "@components";
import { fetchSearchFromApi, clearSearch, removeFromSearchHistory } from "@redux/actions";
import colors from "@common/styles/colors";
import Icon from "react-native-vector-icons/FontAwesome";
import { scale, moderateScale, ScaledSheet } from "react-native-size-matters";
import { UPDATE_SPRINGY } from "@common/LayoutAnimations";
import { convertToUsd, getEndPoint } from "@common/Utils";
import analytics from "@react-native-firebase/analytics";
import { SingleLargeProduct } from "@features/arama/components";

const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);

const win = Dimensions.get("window");

var CREATE_ANIM = {
  duration: 400,
  create: {
    type: LayoutAnimation.Types.spring,
    property: LayoutAnimation.Properties.scaleY,
    springDamping: 0.8,
  },
};
var DELETE_ANIM = {
  duration: 400,
  delete: {
    type: LayoutAnimation.Types.spring,
    property: LayoutAnimation.Properties.scaleY,
    springDamping: 0.8,
  },
};

class SearchScreen extends Component {
  text = "";
  offset = 0;
  timeStamp = 0;

  constructor(props) {
    super(props);
    const filterBarAnim = new Animated.Value(0);
    const scrollAnim = new Animated.Value(0);
    this.startNewSearch = this.startNewSearch.bind(this);
    this.state = {
      keyboardHeight: 0,
      filterBarPosition:
        AnimatedHeader.height +
        (Platform.OS === "android" ? (Platform.Version < 20 ? 0 : StatusBar.currentHeight) : 0),
      layout: "small",
      Marka: "",
      Hiyerarsi: "",
      SearchText: "",
      showSuggestions: false,
      autoCompleteList: [],
      filterBarAnim,
      scrollAnim,
    };
    this.page = 1;
    this.onChangeText = this.onChangeText.bind(this);
    this.getAutoComplete = this.getAutoComplete.bind(this);
  }

  onChangeText(text) {
    this.setState({ SearchText: text });
    this.getAutoComplete(text);
  }

  animateFilterBar(dir) {
    if (dir === "up") {
      Animated.timing(this.state.filterBarAnim, {
        duration: 100,
        useNativeDriver: true,
        toValue: -moderateScale(40, 0.5),
      }).start();
    } else if (dir === "down") {
      Animated.timing(this.state.filterBarAnim, {
        duration: 100,
        useNativeDriver: true,
        toValue: 0,
      }).start();
    }
  }

  getAutoComplete(text) {
    console.log("debounce", text);
    fetch(
      getEndPoint("AUTOCOMPLETE_URL") + "?si=" + this.props.session.info.SessionID + "&q=" + text,
    )
      .then(res => res.json())
      .then(responseJson => {
        this.setState({
          autoCompleteList: responseJson,
        });
      });
  }

  startNewSearch = async (params, text) => {
    this.page = 1;
    try {
      await this.props.searchProducts(
        typeof text !== "undefined" ? text : this.state.SearchText,
        true,
        params,
      );
    } catch (e) {
      console.error(e);
      Alert.alert("Hata", "Beklenmedik bir hata oluştu");
      this.setState({
        progress: false,
      });
    }
    this.props.navigation.setParams({
      focus: false,
      doSearch: false,
      searchParams: { Hiyerarsi: "", Marka: "", SearchText: "" },
    });
    this.setState({
      Hiyerarsi: "",
      Marka: "",
    });
    analytics().logEvent("search", { search_term: text });
    Keyboard.dismiss();
  };

  genRows = () => {
    if (
      this.props.search.items.length < this.props.search.totalQuantity &&
      !this.props.search.loadingMore &&
      this.props.search.items.length > 0
    ) {
      this.page++;
      this.props.moreProducts(this.page);
    }
  };

  sortSearch = st => {
    console.log("sort", st);
    this.page = 1;
    console.log("sort", st);
    this.props.setFilter({ st: st });
    let params = {};
    params.st = st;
    this.props.filterSearch(params);
  };

  componentDidUpdate(prevProps, prevState) {
    if (prevProps !== this.props) {
      var h = this.props.navigation.state.params?.searchParams?.Hiyerarsi || "";
      var ch = prevProps.navigation.state.params?.searchParams?.Hiyerarsi || "";

      var m = this.props.navigation.state.params?.searchParams?.Marka || "";
      var cm = prevProps.navigation.state.params?.searchParams?.Marka || "";

      var t = this.props.navigation.state.params?.searchParams?.SearchText || "";
      var ct = prevProps.navigation.state.params?.searchParams?.SearchText || "";

      console.log("search componentDidUpdate", h, m, t, this.state);
      if (
        (h !== "" && typeof h !== "undefined" && h !== ch) ||
        (m !== "" && typeof m !== "undefined" && m !== cm) ||
        (t !== "" && typeof t !== "undefined" && t !== ct)
      ) {
        this.setState({
          Hiyerarsi: h,
          Marka: m,
          SearchText: "",
        });
        this.startNewSearch({ Marka: m, Hiyerarsi: h }, t);
      }
    }
    if (prevProps.search.currentPage !== this.props.search.currentPage) {
      this.page = this.props.search.currentPage;
    }
  }

  _keyboardDidShow = e => {
    this.setState({
      keyboardVisible: true,
      keyboardHeight: e.endCoordinates.height,
    });
  };

  componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener("keyboardWillShow", this._keyboardDidShow);

    this.getAutoComplete("");
    var h = this.props.navigation.state.params?.searchParams?.Hiyerarsi || "";

    var m = this.props.navigation.state.params?.searchParams?.Marka || "";

    var t = this.props.navigation.state.params?.searchParams?.SearchText || "";

    if (
      (h !== "" && typeof h !== "undefined") ||
      (m !== "" && typeof m !== "undefined") ||
      (t !== "" && typeof t !== "undefined")
    ) {
      console.log("search mounted", h, m, t);
      this.setState({
        Hiyerarsi: h,
        Marka: m,
      });
      this.startNewSearch({ Marka: m, Hiyerarsi: h }, t);
    }
    if (this.props.navigation.state.params && this.props.navigation.state.params.focus) {
      this.animatedHeader.getWrappedInstance().focusSearch(true);
      //this.toggleSuggestions(true, false);
    }

    this.didFocusSubscription = this.props.navigation.addListener("didFocus", payload => {
      this.props.getSearchHistory();
      console.log("payload", payload, this.props.navigation);
      if (this.props.navigation.state.params && this.props.navigation.state.params.focus) {
        this.animatedHeader.getWrappedInstance().focusSearch(true);
        //this.toggleSuggestions(true, false);
      }
    });

    this.willBlurSubscription = this.props.navigation.addListener("willBlur", () => {
      this.animatedHeader.getWrappedInstance().focusSearch(false);
      this.props.navigation.setParams({
        focus: false,
        doSearch: false,
      });
    });
    this.props.getSearchHistory();

    this.state.scrollAnim.addListener(({ value }) => {
      const diff = value - this._scrollValue;
      this._scrollValue = value;
      if (diff > 0 && value > (win.width * 9) / 16 + scale(50)) this.fixedBtn.startFadeAnim(false);
      else if (this.fixedBtn.state.isArrowVisible) this.fixedBtn.startFadeAnim(true);
    });
  }

  componentWillUnmount() {
    this.state.scrollAnim.removeAllListeners();
    this.props.setFilter({
      st: "",
      Hiyerarsi: "",
      Marka: "",
      ft: "",
      fd: "",
      od: "",
    });
    this.willBlurSubscription.remove();
    this.didFocusSubscription.remove();
  }

  addToCart = (item, number) => {
    this.setState({
      progress: true,
    });
    fetch(
      getEndPoint("CART_URL") +
        "?Si=" +
        this.props.session.info.SessionID +
        "&func=8" +
        "&uk=" +
        item.UrunKodu +
        "&adt=" +
        number +
        "&sn=" +
        item.UrunSeriNo,
      {
        method: "POST",
      },
    )
      .then(response => response.json())
      .then(responseJson => {
        const priceSplitted = item.MobileUrunFiyatListesi[0].UrunFiyatStr.replace(".", "")
          .replace(",", ".")
          .split(" ");
        const price = Number(
          convertToUsd(priceSplitted[0], priceSplitted[priceSplitted.length - 1]),
        );
        if (responseJson.MesajTipi === "S") {
          this.props.setCart(responseJson);
          this.props.showToast("Ürün sepete eklendi");
          try {
            const pp = {
              quantity: Number(number),
              item_name: item.UrunAciklamasi,
              item_id: item.UrunKodu,
              price: price,
              currency: priceSplitted[priceSplitted.length - 1],
            };
            analytics().logEvent("add_to_cart", pp);
          } catch (error) {
            console.log("trackAddToCart error", error);
          }
        } else {
          this.props.showToast("Bir hata oluştu lütfen tekrar deneyin");
        }
        this.setState({
          progress: false,
        });
      })
      .catch(error => {
        this.props.showToast("Bir hata oluştu lütfen tekrar deneyin");
        console.log("addToCart error", error);
        this.setState({
          progress: false,
        });
      });
  };

  toggleSuggestions = (show, animate) => {
    this.setState({ showSuggestions: show, autoCompleteList: [] });
    if (animate) {
      if (show) {
        LayoutAnimation.configureNext(CREATE_ANIM);
      } else {
        if (Platform.OS === "ios") LayoutAnimation.configureNext(DELETE_ANIM);
      }
    }
  };

  scrollToTop = () => {
    this.flatlist.getNode().scrollToOffset({ offset: -150, animated: true });
  };

  scrollY = 0;

  onScroll = event => {
    var currentOffset = event.nativeEvent.contentOffset.y;
    this.scrollY = currentOffset;
    currentOffset = currentOffset < 0 ? 0 : currentOffset;
    var direction = currentOffset > this.offset ? "down" : "up";
    if (direction === "up") {
      if (
        this.state.filterBarPosition !==
        AnimatedHeader.height +
          (Platform.OS === "android" ? (Platform.Version < 20 ? 0 : StatusBar.currentHeight) : 0)
      ) {
        this.setState({
          filterBarPosition:
            AnimatedHeader.height +
            (Platform.OS === "android" ? (Platform.Version < 20 ? 0 : StatusBar.currentHeight) : 0),
        });
        LayoutAnimation.configureNext(UPDATE_SPRINGY);
      }
    } else {
      if (
        this.state.filterBarPosition !==
        AnimatedHeader.height +
          (Platform.OS === "android" ? (Platform.Version < 20 ? 0 : StatusBar.currentHeight) : 0) -
          moderateScale(40, 0.5)
      ) {
        this.setState({
          filterBarPosition:
            AnimatedHeader.height +
            (Platform.OS === "android"
              ? Platform.Version < 20
                ? 0
                : StatusBar.currentHeight
              : 0) -
            moderateScale(40, 0.5),
        });
        LayoutAnimation.configureNext(UPDATE_SPRINGY);
      }
    }
    this.offset = currentOffset;
  };

  renderNoContent = () => {
    return (
      <View
        style={{
          width: win.width,
          opacity:
            this.props.search.isRefreshingSearch || this.props.search.isFetchingSearchProducts
              ? 0
              : 1,
        }}
      >
        <Text
          style={{
            fontSize: 16,
            color: "gray",
            textAlign: "center",
            marginVertical: 10,
            backgroundColor: "transparent",
          }}
        >
          Kriterlerinize uygun sonuç bulunamadı
        </Text>
      </View>
    );
  };

  render() {
    const sortingOptions = Object.keys(this.props.search.searchIds).map(key => {
      return { key: key, value: this.props.search.searchIds[key] };
    });
    return (
      <SafeAreaView
        style={{ backgroundColor: colors.primary_dark, flex: 1 }}
        forceInset={{ top: "always" }}
      >
        <View
          style={{
            flexGrow: 1,
            width: win.width,
            backgroundColor: "#efefef",
          }}
        >
          {this.state.showSuggestions && (
            <ScrollView
              keyboardShouldPersistTaps="handled"
              style={{
                marginTop:
                  AnimatedHeader.height +
                  (Platform.OS === "android"
                    ? Platform.Version < 20
                      ? 0
                      : StatusBar.currentHeight
                    : 0),
              }}
            >
              {this.state.autoCompleteList.length > 0
                ? this.state.autoCompleteList.map(item => {
                    return (
                      <View style={{ backgroundColor: "white" }}>
                        <Text
                          fontScale={0.6}
                          style={{
                            padding: moderateScale(10, 0.5),
                            fontSize: 18,
                          }}
                          onPress={() => {
                            console.log("onpress autocomplete", item.SuggestedWord);
                            this.setState({
                              SearchText: item.SuggestedWord,
                              Marka: "",
                              Hiyerarsi: "",
                              showSuggestions: false,
                            });
                            this.animatedHeader.getWrappedInstance().searchBox.blur();
                            this.startNewSearch(null, item.SuggestedWord);
                            this.props.addToSearchHistory(item.SuggestedWord);
                          }}
                        >
                          {item.SuggestedWord}
                        </Text>
                        <View
                          style={{
                            height: 1,
                            flex: 1,
                            backgroundColor: "#cecece",
                          }}
                        />
                      </View>
                    );
                  })
                : this.props.searchHistory.items.length > 0 && (
                    <View style={{ height: win.height }}>
                      <View
                        style={{
                          marginBottom: 10,
                          borderBottomColor: "#dedede",
                          borderBottomWidth: 1,
                          flexDirection: "row",
                          justifyContent: "space-between",
                          padding: 10,
                          backgroundColor: "white",
                        }}
                      >
                        <Text
                          style={{
                            fontSize: 16,
                            color: colors.price_color,
                          }}
                        >
                          Geçmiş Aramalarınız
                        </Text>
                        <Text
                          style={{
                            fontSize: 16,
                            color: colors.price_color,
                          }}
                          onPress={() => this.props.clearSearchHistory()}
                        >
                          Tümünü Sil
                        </Text>
                      </View>
                      {this.props.searchHistory.items.map((item, index) => {
                        return (
                          <TouchableOpacity
                            key={item}
                            onPress={() => {
                              this.setState({
                                SearchText: item,
                                Marka: "",
                                Hiyerarsi: "",
                                showSuggestions: false,
                              });
                              this.animatedHeader.getWrappedInstance().searchBox.blur();
                              this.startNewSearch(null, item);
                              this.props.addToSearchHistory(item);
                            }}
                            style={{
                              padding: 10,
                              backgroundColor: "white",
                              flexDirection: "row",
                              alignItems: "center",
                              justifyContent: "space-between",
                              borderBottomColor: "#dedede",
                              borderBottomWidth: 1,
                            }}
                          >
                            <Text fontScale={0.6} style={{ fontSize: 18 }}>
                              {item}
                            </Text>
                            <TouchableOpacity
                              onPress={() => {
                                this.props.removeFromSearchHistory(index);
                                LayoutAnimation.configureNext(UPDATE_SPRINGY);
                              }}
                            >
                              <Icon
                                name="times-circle"
                                size={moderateScale(16, 0.5)}
                                color="rgba(0,0,0,0.2)"
                              />
                            </TouchableOpacity>
                          </TouchableOpacity>
                        );
                      })}
                    </View>
                  )}
            </ScrollView>
          )}
          {(!this.state.showSuggestions ||
            (this.props.searchHistory.items.length < 1 &&
              this.state.autoCompleteList.length < 1)) && (
            <Animated.View
              style={[
                {
                  flexDirection: "row",
                  zIndex: 999,
                  height: moderateScale(50, 0.5),
                  alignItems: "center",
                  width: win.width,
                  backgroundColor: "white",
                  position: "absolute",
                  paddingTop: moderateScale(10, 0.5),
                  top: this.state.filterBarPosition - moderateScale(10, 0.5),
                },
              ]}
            >
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate("SearchFilterScreen");
                }}
                style={{
                  flex: 8,
                  alignItems: "center",
                  borderRightColor: "#e6e6e6",
                  borderRightWidth: 1,
                  height: moderateScale(40, 0.5),
                  flexDirection: "row",
                  justifyContent: "center",
                }}
              >
                {
                  <View>
                    <Image
                      source={require("@assets/images/filter.png")}
                      style={{
                        width: moderateScale(15, 0.5),
                        height: moderateScale(15, 0.5),
                        tintColor: "black",
                        marginRight: moderateScale(6, 0.5),
                        resizeMode: "contain",
                      }}
                    />
                    {this.props.search.currentFilters.length > 0 && (
                      <View style={styles.numberWrap}>
                        <Text fontScale={0.5} style={styles.number}>
                          {this.props.search.currentFilters.length}
                        </Text>
                      </View>
                    )}
                  </View>
                }
                <Text style={{ fontSize: 14, color: "gray" }}>Filtrele</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  if (Object.keys(this.props.search.searchIds).length > 0) {
                    this.sortPicker.openPicker(null, null, Platform.OS === "android");
                  } else {
                    Alert.alert("Bilgi", "Görüntülenecek sıralama seçeneği bulunamadı.", [
                      { text: "Tamam" },
                    ]);
                  }
                }}
                style={{
                  flex: 8,
                  height: moderateScale(40, 0.5),
                  alignItems: "center",
                  borderRightColor: "#e6e6e6",
                  borderRightWidth: 1,
                  flexDirection: "row",
                  justifyContent: "center",
                }}
              >
                <Text style={{ fontSize: 14, color: "#3396d8" }}>Sırala</Text>
                <Image
                  source={require("@assets/images/sort.png")}
                  style={{
                    width: moderateScale(15, 0.5),
                    height: moderateScale(15, 0.5),
                    marginLeft: moderateScale(4, 0.5),
                    resizeMode: "contain",
                  }}
                />
              </TouchableOpacity>
              <View
                style={{
                  flexDirection: "row",
                  flex: 4,
                  justifyContent: "center",
                  alignItems: "center",
                  height: moderateScale(40, 0.5),
                }}
              >
                <TouchableOpacity
                  onPress={() => {
                    if (this.state.layout !== "large") {
                      //LayoutAnimation.configureNext(UPDATE_ANIM)
                      this.setState({ layout: "large" });
                    }
                  }}
                  style={{
                    alignItems: "center",
                    flexDirection: "row",
                    marginRight: win.width / 32,
                  }}
                >
                  <Image
                    source={require("@assets/images/square.png")}
                    style={{
                      width: moderateScale(22, 0.5),
                      height: moderateScale(22, 0.5),
                      tintColor: this.state.layout === "large" ? "#3396d8" : "#d0d0d0",
                    }}
                  />
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => {
                    if (this.state.layout !== "small") {
                      //LayoutAnimation.configureNext(UPDATE_ANIM)
                      this.setState({ layout: "small" });
                    }
                  }}
                  style={{
                    alignItems: "center",
                    flexDirection: "row",
                  }}
                >
                  <Image
                    source={require("@assets/images/2square.png")}
                    style={{
                      width: moderateScale(22, 0.5),
                      height: moderateScale(22, 0.5),
                      tintColor: this.state.layout === "small" ? "#3396d8" : "#d0d0d0",
                    }}
                  />
                </TouchableOpacity>
              </View>
            </Animated.View>
          )}
          {!this.state.showSuggestions && (
            <AnimatedFlatList
              testID="scroll_search"
              ref={c => (this.flatlist = c)}
              scrollIndicatorInsets={{
                top: AnimatedHeader.height + moderateScale(40, 0.5) + moderateScale(10, 0.5),
                left: 0,
                bottom: 0,
                right: 0,
              }}
              contentContainerStyle={{
                flexGrow: 1,
                paddingBottom:
                  Platform.OS === "android"
                    ? moderateScale(20, 0.5) + (Platform.Version < 20 ? 0 : StatusBar.currentHeight)
                    : moderateScale(20, 0.5),
                paddingTop:
                  Platform.OS === "android"
                    ? AnimatedHeader.height +
                      (Platform.Version < 20 ? 0 : StatusBar.currentHeight) +
                      moderateScale(50, 0.5)
                    : AnimatedHeader.height + moderateScale(50, 0.5),
              }}
              data={this.props.search.items}
              extraData={this.props.search.items}
              renderItem={this.renderItem}
              onEndReached={this.genRows}
              onEndReachedThreshold={0.2}
              keyExtractor={item => item.index}
              ListEmptyComponent={this.renderNoContent}
              onScroll={this.onScroll}
              scrollEventThrottle={100}
              ListFooterComponent={() =>
                this.props.search.loadingMore ? (
                  <View
                    style={{
                      flexDirection: "row",
                      flex: 1,
                      justifyContent: "center",
                    }}
                  >
                    <ActivityIndicator size={"small"} />
                  </View>
                ) : (
                  this.props.search.items.length === this.props.search.totalQuantity &&
                  this.props.search.items.length > 0 && (
                    <View
                      style={{
                        flexDirection: "row",
                        flex: 1,
                        justifyContent: "center",
                      }}
                    >
                      <Text
                        style={{
                          fontFamily: "Poppins-Light",
                          color: "gray",
                          fontSize: 12,
                        }}
                      >
                        Gösterilecek daha fazla ürün yok
                      </Text>
                    </View>
                  )
                )
              }
              overScrollMode="never"
            />
          )}
        </View>
        {(this.props.search.isRefreshingSearch || this.state.progress) && (
          <View style={styles.container}>
            <PencereIndicator />
          </View>
        )}
        <AnimatedHeader
          ref={node => {
            this.animatedHeader = node;
          }}
          navigation={this.props.navigation}
          clampedScroll={this.state.clampedScroll}
          back={false}
          alignLeft={false}
          search={this.startNewSearch}
          searchBar={true}
          searchText={this.state.SearchText}
          showSuggestions={this.state.showSuggestions}
          toggleSuggestions={this.toggleSuggestions}
          onChangeText={this.onChangeText}
        />
        <FixedButton ref={c => (this.fixedBtn = c)} scrollToTop={this.scrollToTop} />
        <BottomPicker
          ref={c => (this.sortPicker = c)}
          items={sortingOptions}
          valueExtractor={item => item.key}
          labelExtractor={item => item.value}
          initialItem={sortingOptions.find(
            item =>
              item.key ===
              (this.props.search.currentSorting === "StokDurumu"
                ? "Stok"
                : this.props.search.currentSorting),
          )}
          onPressOK={selectedItem => this.sortSearch(selectedItem)}
        />
      </SafeAreaView>
    );
  }
  renderItem = ({ item, index }) => {
    return (
      <SingleLargeProduct
        index={index}
        layout={this.state.layout}
        scrollToFocusedInput={offset => {
          if (offset > win.height - this.state.keyboardHeight - AnimatedHeader.height)
            this.flatlist.getNode().scrollToOffset({
              offset:
                this.scrollY +
                offset -
                (win.height - this.state.keyboardHeight) +
                AnimatedHeader.height,
              animated: true,
            });
        }}
        navigation={this.props.navigation}
        search={true}
        item={item}
        addToCart={this.addToCart}
        showToast={this.props.showToast}
      />
    );
  };
}

const styles = ScaledSheet.create({
  icon: {
    width: 26,
    height: 26,
    tintColor: "gray",
  },
  container: {
    height: win.height,
    width: win.width,
    paddingTop: moderateScale(50, 0.4),
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0,0,0,0.5)",
    position: "absolute",
    top: -(Platform.Version < 20 ? 0 : StatusBar.currentHeight),
    left: 0,
  },
  numberWrap: {
    position: "absolute",
    top: "-5@ms0.4",
    left: "-9@ms0.4",
    height: "15@ms0.4",
    minWidth: "15@ms0.4",
    backgroundColor: "#E1F5FE",
    borderRadius: "8@ms0.4",
    alignItems: "center",
    justifyContent: "center",
  },
  number: {
    fontSize: 13,
    textAlign: "center",
    color: colors.light_black,
  },
});

function mapStateToProps(state) {
  return {
    cart: state.cart,
    session: state.session,
    search: state.search,
    category: state.category,
    filters: state.filters,
    searchHistory: state.searchHistory,
    homePage: state.homePage,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    searchProducts: (query, refresh, params) =>
      dispatch(fetchSearchFromApi(query, refresh, params)),
    moreProducts: page => dispatch(moreProducts(page)),
    filterSearch: (params, ya) => dispatch(filterSearch(params, ya)),
    clearSearch: () => dispatch(clearSearch()),
    selectCategory: cat => dispatch(selectCategory(cat)),
    setFilter: params => dispatch(setFilter(params)),
    addToSearchHistory: q => dispatch(addToSearchHistory(q)),
    clearSearchHistory: () => dispatch(clearSearchHistory()),
    getSearchHistory: () => dispatch(getSearchHistory()),
    removeFromSearchHistory: i => dispatch(removeFromSearchHistory(i)),
    setCart: cart => dispatch(setCart(cart)),
    clearFilters: () => dispatch(clearFilters()),
    showToast: msg => dispatch(showToast(msg)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchScreen);
