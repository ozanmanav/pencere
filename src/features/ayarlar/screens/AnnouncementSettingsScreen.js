import React, { Component } from "react";
import {
  View,
  SafeAreaView,
  TouchableOpacity,
  Dimensions,
  Platform,
  StatusBar,
  Switch,
  TextInput,
  Alert,
} from "react-native";
import { SimpleHeader, Text, PencereIndicator } from "@components";
import { connect } from "react-redux";
import { getHomePage, setDirty, showToast } from "@redux/actions";
import { saveSettings } from "@common/Utils";
import Toast from "react-native-easy-toast";
import colors from "@common/styles/colors";
import { moderateScale } from "react-native-size-matters";
import Euromsg from "react-native-euromsg";

const win = Dimensions.get("window");

class AnnouncementSettingsScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      DuyuruAl: true,
      SmsAl: true,
      MobilePushAl: true,
      saving: false,
    };
  }

  componentDidMount() {
    if (this.props.homePage.info?.KisiselAyarlar) {
      this.setState({
        DuyuruAl: this.props.homePage.info.KisiselAyarlar.DuyuruAl,
        SmsAl: this.props.homePage.info.KisiselAyarlar.SmsAl,
        MobilePushAl: this.props.homePage.info.KisiselAyarlar.MobilePushAl,
      });
    }
  }

  onBackPress = () => {
    if (this.props.homePage.dirty) {
      Alert.alert(
        "Değişiklikler yapıldı",
        "Bu sayfayı terkederek yaptığınız değişikliklerin iptal edilmesini onaylıyormusunuz?",
        [
          {
            text: "Evet",
            onPress: () => {
              this.props.setDirty(false);
              this.props.navigation.goBack();
            },
          },
          {
            text: "Hayır",
            style: "cancel",
          },
        ],
        { cancelable: false },
      );
    } else this.props.navigation.goBack();
  };

  componentDidUpdate(prevProps, prevState) {
    //when stage is changed check if home page is dirty
    //if redux dirty is different than actual dirty, set dirty
    const KisiselAyarlar = this.props.homePage.info.KisiselAyarlar;
    if (prevState !== this.state) {
      let isDirty =
        KisiselAyarlar.DuyuruAl !== this.state.DuyuruAl ||
        KisiselAyarlar.SmsAl !== this.state.SmsAl ||
        KisiselAyarlar.MobilePushAl !== this.state.MobilePushAl;
      if (isDirty !== this.props.homePage.dirty) {
        this.props.setDirty(isDirty);
      }
    }
    //if changes are canceled set homepage state to component state
    if (prevProps.homePage.dirty !== this.props.homePage.dirty && !this.props.homePage.dirty) {
      this.setState({
        DuyuruAl: KisiselAyarlar.DuyuruAl,
        SmsAl: KisiselAyarlar.SmsAl,
      });
    }
  }

  onPressSave = () => {
    if (this.props.homePage.dirty) {
      this.setState({ saving: true });
      var params = [];
      params.push({ name: "Si", value: this.props.session.info.SessionID });
      params.push({ name: "e", value: this.state.DuyuruAl ? 1 : 0 });
      params.push({ name: "m", value: this.state.SmsAl ? 1 : 0 });
      params.push({ name: "p", value: this.state.MobilePushAl ? 1 : 0 });

      saveSettings(params, 1)
        .then(res => {
          res.json().then(json => {
            this.props
              .getHomePage()
              .then(payload => {
                this.setState(
                  {
                    DuyuruAl: payload.KisiselAyarlar.DuyuruAl,
                    SmsAl: payload.KisiselAyarlar.SmsAl,
                    MobilePushAl: payload.KisiselAyarlar.MobilePushAl,
                    saving: false,
                  },
                  () => {
                    if (payload.KisiselAyarlar.MobilePushAl !== undefined)
                      Euromsg.setPermit(payload.KisiselAyarlar.MobilePushAl);
                    this.props.showToast("Ayarlarınız kaydedilidi.");
                  },
                );
              })
              .catch(err => {
                console.error(err);
              });
          });
        })
        .catch(err => {
          console.error(err);
          Alert.alert("Hata", "Bir hata oluştu.");
        });
    } else {
      Alert.alert("Bilgi", "Kaydedilecek bir değişiklik yok");
    }
  };

  render() {
    return (
      <SafeAreaView style={{ backgroundColor: colors.primary_dark }} forceInset={{ top: "always" }}>
        <View
          style={{
            paddingTop:
              moderateScale(50, 0.4) +
              (Platform.OS === "android"
                ? Platform.Version < 20
                  ? 0
                  : StatusBar.currentHeight
                : 0),
            height: win.height,
            width: win.width,
            backgroundColor: "white",
          }}
        >
          <SimpleHeader
            navigation={this.props.navigation}
            title="Duyuru Ayarları"
            button={() => (
              <TouchableOpacity
                style={{ padding: 5, paddingHorizontal: 0 }}
                onPress={this.onPressSave}
              >
                <Text fontScale={0.6} style={{ color: "white", fontSize: 16, fontWeight: "bold" }}>
                  Kaydet
                </Text>
              </TouchableOpacity>
            )}
            onBackPress={this.onBackPress}
            style={{
              backgroundColor: colors.primary_dark,
              borderColor: colors.primary_dark,
            }}
            iconColor="white"
            titleStyle={{ color: "white" }}
          />

          <View style={{ flexDirection: "column", padding: moderateScale(10, 0.5) }}>
            <View
              style={{
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "space-between",
              }}
            >
              <Text fontScale={0.6} style={{ fontSize: 16 }}>
                Bildirim almak istiyorum
              </Text>
              <Switch
                value={this.state.MobilePushAl}
                onValueChange={val => {
                  this.setState({ MobilePushAl: val });
                }}
              />
            </View>
            <View
              style={{
                width: win.width,
                height: 1,
                backgroundColor: "#dedede",
                left: -moderateScale(10, 0.5),
                marginVertical: moderateScale(10, 0.5),
              }}
            />
            <View
              style={{
                flexDirection: "row",
                marginBottom: moderateScale(10, 0.5),
                alignItems: "center",
                justifyContent: "space-between",
              }}
            >
              <Text fontScale={0.6} style={{ fontSize: 16 }}>
                Duyuru almak istiyorum
              </Text>
              <Switch
                value={this.state.DuyuruAl}
                onValueChange={val => this.setState({ DuyuruAl: val })}
              />
            </View>
            <View
              style={{
                flexDirection: "column",
                marginBottom: moderateScale(10, 0.5),
              }}
            >
              <Text fontScale={0.6} style={{ fontSize: 16 }}>
                E-posta adresi:
              </Text>
              <TextInput
                editable={false}
                keyboardType="email-address"
                value={this.props.session.info.Email}
                style={{
                  padding: 5,
                  borderColor: "#cecece",
                  borderWidth: 1,
                  marginTop: moderateScale(5, 0.5),
                  fontSize: moderateScale(16, 0.4),
                  color: "gray",
                }}
              />
            </View>

            <View
              style={{
                width: win.width,
                height: 1,
                backgroundColor: "#dedede",
                left: -moderateScale(10, 0.5),
                marginVertical: moderateScale(10, 0.5),
              }}
            />

            <View
              style={{
                flexDirection: "row",
                marginBottom: moderateScale(10, 0.5),
                alignItems: "center",
                justifyContent: "space-between",
              }}
            >
              <Text fontScale={0.6} style={{ fontSize: 16 }}>
                SMS almak istiyorum
              </Text>
              <Switch
                value={this.state.SmsAl}
                onValueChange={val => this.setState({ SmsAl: val })}
              />
            </View>
            <View
              style={{
                flexDirection: "column",
                marginBottom: moderateScale(10, 0.5),
              }}
            >
              <Text fontScale={0.6} style={{ fontSize: 16 }}>
                Cep telefonu:
              </Text>
              <TextInput
                editable={false}
                keyboardType="phone-pad"
                value={this.props.session.info.Telefon}
                style={{
                  padding: 5,
                  borderColor: "#cecece",
                  borderWidth: 1,
                  marginTop: moderateScale(5, 0.5),
                  fontSize: moderateScale(16, 0.4),
                  color: "gray",
                }}
              />
            </View>
          </View>
          <Toast style={{ backgroundColor: "black", opacity: 0.8, zIndex: 999 }} />
          {(this.state.saving || this.props.homePage.isFetchingHomePage) && (
            <View
              style={{
                width: win.width,
                height: win.height,
                justifyContent: "center",
                alignItems: "center",
                position: "absolute",
                top: 0,
                left: 0,
                backgroundColor: "rgba(0,0,0,0.3)",
              }}
            >
              <PencereIndicator />
            </View>
          )}
        </View>
      </SafeAreaView>
    );
  }
}
function mapStateToProps(state) {
  return {
    homePage: state.homePage,
    session: state.session,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    getHomePage: () => dispatch(getHomePage()),
    setDirty: dirty => dispatch(setDirty(dirty)),
    showToast: msg => dispatch(showToast(msg)),
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(AnnouncementSettingsScreen);
