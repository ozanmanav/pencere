import React, { Component } from "react";
import {
  SafeAreaView,
  View,
  Dimensions,
  Image,
  ScrollView,
  Animated,
  TouchableOpacity,
  Platform,
} from "react-native";
import { AnimatedHeader, Text, PencereIndicator } from "@components";
import { connect } from "react-redux";
import colors from "@common/styles/colors";
import { moderateScale, ScaledSheet } from "react-native-size-matters";

const AnimatedScrollView = Animated.createAnimatedComponent(ScrollView);

const win = Dimensions.get("window");

export class SettingsScreen extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <SafeAreaView style={{ backgroundColor: colors.primary_dark }} forceInset={{ top: "always" }}>
        <View
          style={{
            height: win.height,
            width: win.width,
            backgroundColor: "white",
          }}
        >
          {
            <AnimatedScrollView
              removeClippedSubviews={false}
              scrollEventThrottle={16}
              contentOffset={{ y: -moderateScale(50, 0.4) }}
              contentInset={{
                top: moderateScale(50, 0.4),
                left: 0,
                bottom: 0,
                right: 0,
              }}
              contentContainerStyle={{
                alignItems: "center",
                paddingBottom: moderateScale(60, 0.4),
                paddingTop:
                  Platform.OS === "android" ? moderateScale(100, 0.4) : moderateScale(10, 0.4),
                width: win.width,
              }}
            >
              <TouchableOpacity
                style={styles.itemContainer}
                onPress={() => this.props.navigation.push("PriceSettingsScreen")}
              >
                <Text style={styles.titleTxt}>Fiyat Ayarları</Text>
                <Image source={require("@assets/images/arrow-right.png")} style={styles.icon} />
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.itemContainer}
                onPress={() => this.props.navigation.push("ListingSettingsScreen")}
              >
                <Text style={styles.titleTxt}>Listeleme Ayarları</Text>
                <Image source={require("@assets/images/arrow-right.png")} style={styles.icon} />
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.itemContainer}
                onPress={() => this.props.navigation.push("AnnouncementSettingsScreen")}
              >
                <Text style={styles.titleTxt}>Duyuru Ayarları</Text>
                <Image source={require("@assets/images/arrow-right.png")} style={styles.icon} />
              </TouchableOpacity>
            </AnimatedScrollView>
          }
          <AnimatedHeader
            navigation={this.props.navigation}
            back={true}
            alignLeft={true}
            title="Kişisel Ayarlar"
            searchBar={false}
          />

          {this.props.homePage.isFetching && (
            <View style={[styles.loadingContainer]}>
              <PencereIndicator size="large" color="#0000ff" />
            </View>
          )}
        </View>
      </SafeAreaView>
    );
  }
}

function mapStateToProps(state) {
  return {
    homePage: state.homePage,
  };
}

export default connect(mapStateToProps, null)(SettingsScreen);

const styles = ScaledSheet.create({
  container: {
    alignItems: "center",
    paddingTop: 10,
  },
  loadingContainer: {
    height: win.height,
    width: win.width,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0,0,0,0.3)",
    position: "absolute",
  },
  itemContainer: {
    flexDirection: "row",
    width: win.width * 0.94,
    paddingHorizontal: "15@ms0.5",
    alignItems: "center",
    justifyContent: "space-between",
    backgroundColor: "#f3f3f3",
    borderRadius: 3,
    marginBottom: "10@ms0.5",
    height: "50@ms0.5",
  },
  icon: {
    width: "8@ms0.5",
    height: "8@ms0.5",
    marginRight: "10@ms0.5",
    tintColor: "black",
  },
  titleTxt: {
    fontSize: 13,
    color: "black",
    fontFamily: "Poppins-Medium",
  },
});
