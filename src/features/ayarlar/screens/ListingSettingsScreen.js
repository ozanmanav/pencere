import React, { Component } from "react";
import {
  View,
  SafeAreaView,
  TouchableOpacity,
  Dimensions,
  StatusBar,
  Platform,
  Alert,
} from "react-native";
import { SimpleHeader, RadioButton, Text, PencereIndicator } from "@components";
import { connect } from "react-redux";
import { getHomePage, setDirty, showToast } from "@redux/actions";
import { saveSettings } from "@common/Utils";
import Toast from "react-native-easy-toast";
import colors from "@common/styles/colors";
import { moderateScale } from "react-native-size-matters";

const win = Dimensions.get("window");

class ListingSettingsScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      TumUrunleriGoster: props.homePage.info.KisiselAyarlar.TumUrunleriGoster,
      StoktakileriGoster: props.homePage.info.KisiselAyarlar.StoktakileriGoster,
      IstanbulDepoGoster: props.homePage.info.KisiselAyarlar.IstanbulDepoGoster,
      AnkaraDepoGoster: props.homePage.info.KisiselAyarlar.AnkaraDepoGoster,
      IzmirDepoGoster: props.homePage.info.KisiselAyarlar.IzmirDepoGoster,
      saving: false,
    };
  }

  onBackPress = () => {
    if (this.props.homePage.dirty) {
      Alert.alert(
        "Değişiklikler yapıldı",
        "Bu sayfayı terkederek yaptığınız değişikliklerin iptal edilmesini onaylıyormusunuz?",
        [
          {
            text: "Evet",
            onPress: () => {
              this.props.setDirty(false);
              this.props.navigation.goBack();
            },
          },
          {
            text: "Hayır",
            style: "cancel",
          },
        ],
        { cancelable: false },
      );
    } else this.props.navigation.goBack();
  };

  componentDidUpdate(prevProps, prevState) {
    //when stage is changed check if home page is dirty
    //if redux dirty is different than actual dirty, set dirty
    const KisiselAyarlar = this.props.homePage?.info?.KisiselAyarlar;
    if (prevState !== this.state) {
      let isDirty =
        KisiselAyarlar.TumUrunleriGoster !== this.state.TumUrunleriGoster ||
        KisiselAyarlar.StoktakileriGoster !== this.state.StoktakileriGoster ||
        KisiselAyarlar.IstanbulDepoGoster !== this.state.IstanbulDepoGoster ||
        KisiselAyarlar.AnkaraDepoGoster !== this.state.AnkaraDepoGoster ||
        KisiselAyarlar.IzmirDepoGoster !== this.state.IzmirDepoGoster;

      if (isDirty !== this.props.homePage.dirty) {
        this.props.setDirty(isDirty);
      }
    }
    //if changes are canceled set homepage state to component state
    if (prevProps.homePage.dirty !== this.props.homePage.dirty && !this.props.homePage.dirty) {
      this.setState({
        TumUrunleriGoster: KisiselAyarlar.TumUrunleriGoster,
        StoktakileriGoster: KisiselAyarlar.StoktakileriGoster,
        IstanbulDepoGoster: KisiselAyarlar.IstanbulDepoGoster,
        AnkaraDepoGoster: KisiselAyarlar.AnkaraDepoGoster,
        IzmirDepoGoster: KisiselAyarlar.IzmirDepoGoster,
      });
    }
  }

  onPressSave = () => {
    if (this.props.homePage.dirty) {
      this.setState({ saving: true });
      var params = [];
      params.push({ name: "Si", value: this.props.session.info.SessionID });
      params.push({ name: "sa", value: this.state.TumUrunleriGoster });
      params.push({ name: "so", value: this.state.StoktakileriGoster });
      params.push({ name: "is", value: this.state.IstanbulDepoGoster });
      params.push({ name: "as", value: this.state.AnkaraDepoGoster });
      params.push({ name: "izs", value: this.state.IzmirDepoGoster });
      params.push({ name: "func", value: "4" });

      saveSettings(params)
        .then(res => {
          res.json().then(json => {
            console.log("save respnose", json);
            this.props
              .getHomePage()
              .then(payload => {
                this.setState(
                  {
                    TumUrunleriGoster: payload.KisiselAyarlar.TumUrunleriGoster,
                    StoktakileriGoster: payload.KisiselAyarlar.StoktakileriGoster,
                    IstanbulDepoGoster: payload.KisiselAyarlar.IstanbulDepoGoster,
                    AnkaraDepoGoster: payload.KisiselAyarlar.AnkaraDepoGoster,
                    IzmirDepoGoster: payload.KisiselAyarlar.IzmirDepoGoster,
                    saving: false,
                  },
                  () => this.props.showToast("Ayarlarınız kaydedilidi."),
                );
              })
              .catch(err => {
                console.error(err);
              });
          });
        })
        .catch(err => {
          console.error(err);
          Alert.alert("Hata", "Bir hata oluştu.");
        });
    } else {
      Alert.alert("Bilgi", "Kaydedilecek bir değişiklik yok");
    }
  };

  render() {
    return (
      <SafeAreaView style={{ backgroundColor: colors.primary_dark }} forceInset={{ top: "always" }}>
        <View
          style={{
            paddingTop:
              moderateScale(50, 0.4) +
              (Platform.OS === "android"
                ? Platform.Version < 20
                  ? 0
                  : StatusBar.currentHeight
                : 0),
            height: win.height,
            width: win.width,
            backgroundColor: "white",
          }}
        >
          <SimpleHeader
            navigation={this.props.navigation}
            title="Listeleme Ayarları"
            onBackPress={this.onBackPress}
            button={() => (
              <TouchableOpacity
                style={{ padding: 5, paddingHorizontal: 0 }}
                onPress={this.onPressSave}
              >
                <Text fontScale={0.6} style={{ color: "white", fontSize: 16, fontWeight: "bold" }}>
                  Kaydet
                </Text>
              </TouchableOpacity>
            )}
            style={{
              backgroundColor: colors.primary_dark,
              borderColor: colors.primary_dark,
            }}
            iconColor="white"
            titleStyle={{ color: "white" }}
          />
          <View style={{ flexDirection: "column", padding: 10 }}>
            <View
              style={{
                flexDirection: "row",
                marginBottom: 5,
                alignItems: "center",
                marginLeft: 5,
              }}
            >
              <RadioButton
                selected={this.state.TumUrunleriGoster}
                onPress={() =>
                  this.setState({
                    TumUrunleriGoster: true,
                    StoktakileriGoster: false,
                    IstanbulDepoGoster: false,
                    AnkaraDepoGoster: false,
                    IzmirDepoGoster: false,
                  })
                }
              />
              <Text fontScale={0.6} style={{ fontSize: 16, marginLeft: 5, color: "black" }}>
                Tüm ürünleri göster
              </Text>
            </View>
            <View
              style={{
                flexDirection: "row",
                marginBottom: 5,
                alignItems: "center",
                marginLeft: 5,
              }}
            >
              <RadioButton
                selected={this.state.StoktakileriGoster}
                onPress={() =>
                  this.setState({
                    TumUrunleriGoster: false,
                    StoktakileriGoster: true,
                    IstanbulDepoGoster: false,
                    AnkaraDepoGoster: false,
                    IzmirDepoGoster: false,
                  })
                }
              />
              <Text fontScale={0.6} style={{ fontSize: 16, marginLeft: 5, color: "black" }}>
                Sadece stokta olan ürünüleri göster
              </Text>
            </View>
            <View
              style={{
                flexDirection: "row",
                marginBottom: 5,
                alignItems: "center",
                marginLeft: 5,
              }}
            >
              <RadioButton
                selected={this.state.IstanbulDepoGoster}
                onPress={() =>
                  this.setState({
                    TumUrunleriGoster: false,
                    StoktakileriGoster: false,
                    IstanbulDepoGoster: true,
                    AnkaraDepoGoster: false,
                    IzmirDepoGoster: false,
                  })
                }
              />
              <Text fontScale={0.6} style={{ fontSize: 16, marginLeft: 5, color: "black" }}>
                Sadece İstanbul depoda olanları göster
              </Text>
            </View>
            <View
              style={{
                flexDirection: "row",
                marginBottom: 5,
                alignItems: "center",
                marginLeft: 5,
              }}
            >
              <RadioButton
                selected={this.state.AnkaraDepoGoster}
                onPress={() =>
                  this.setState({
                    TumUrunleriGoster: false,
                    StoktakileriGoster: false,
                    IstanbulDepoGoster: false,
                    AnkaraDepoGoster: true,
                    IzmirDepoGoster: false,
                  })
                }
              />
              <Text fontScale={0.6} style={{ fontSize: 16, marginLeft: 5, color: "black" }}>
                Sadece Ankara depoda olanları göster
              </Text>
            </View>
            <View
              style={{
                flexDirection: "row",
                marginBottom: 5,
                alignItems: "center",
                marginLeft: 5,
              }}
            >
              <RadioButton
                selected={this.state.IzmirDepoGoster}
                onPress={() =>
                  this.setState({
                    TumUrunleriGoster: false,
                    StoktakileriGoster: false,
                    IstanbulDepoGoster: false,
                    AnkaraDepoGoster: false,
                    IzmirDepoGoster: true,
                  })
                }
              />
              <Text fontScale={0.6} style={{ fontSize: 16, marginLeft: 5, color: "black" }}>
                Sadece İzmir depoda olanları göster
              </Text>
            </View>
          </View>
          <Toast
            style={{ backgroundColor: "black", opacity: 0.8, zIndex: 999 }}
            ref={c => (this.toast = c)}
          />
          {this.state.saving && (
            <View
              style={{
                width: win.width,
                height: win.height,
                justifyContent: "center",
                alignItems: "center",
                position: "absolute",
                top: 0,
                left: 0,
                backgroundColor: "rgba(0,0,0,0.3)",
              }}
            >
              <PencereIndicator />
            </View>
          )}
        </View>
      </SafeAreaView>
    );
  }
}
function mapStateToProps(state) {
  return {
    homePage: state.homePage,
    session: state.session,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    getHomePage: () => dispatch(getHomePage()),
    setDirty: dirty => dispatch(setDirty(dirty)),
    showToast: msg => dispatch(showToast(msg)),
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(ListingSettingsScreen);
