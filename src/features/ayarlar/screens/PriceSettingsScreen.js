import React, { Component } from "react";
import {
  View,
  SafeAreaView,
  StatusBar,
  Image,
  TouchableWithoutFeedback,
  TouchableOpacity,
  Dimensions,
  Platform,
  Switch,
  TextInput,
  Alert,
  Keyboard,
} from "react-native";
import { SimpleHeader, RadioButton, Text, PencereIndicator } from "@components";
import { connect } from "react-redux";
import { getHomePage, setDirty, showToast } from "@redux/actions";
import { saveSettings } from "@common/Utils";
import colors from "@common/styles/colors";
import { moderateScale } from "react-native-size-matters";

const win = Dimensions.get("window");

class PriceSettingsScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      OzelFiyatlarimiGoster: props.homePage.info.KisiselAyarlar.OzelFiyatlarimiGoster,
      BenimFiyatimiGoster: props.homePage.info.KisiselAyarlar.BenimFiyatimiGoster,
      BenimFiyatimOrani: props.homePage.info.KisiselAyarlar.BenimFiyatimOrani,
      TESFiyatlariniGoster: props.homePage.info.KisiselAyarlar.TESFiyatlariniGoster,
      isRateValid: true,
      saving: false,
    };
  }

  increaseNumber = () => {
    if (this.state.BenimFiyatimOrani < 20)
      this.setState({
        BenimFiyatimOrani: this.state.BenimFiyatimOrani + 1,
      });
    else {
      this.props.showToast("Bu oran 0 ile 20 arasında olmalıdır");
      this.setState({
        BenimFiyatimOrani: 20,
      });
    }
    if (this.state.BenimFiyatimOrani < 0)
      this.setState({
        BenimFiyatimOrani: 0,
      });
    this.setState({ isRateValid: true });
  };

  decreaseNumber = () => {
    if (this.state.BenimFiyatimOrani > 0)
      this.setState({
        BenimFiyatimOrani: this.state.BenimFiyatimOrani - 1,
      });
    else
      this.setState({
        BenimFiyatimOrani: 0,
      });
    if (this.state.BenimFiyatimOrani > 20)
      this.setState({
        BenimFiyatimOrani: 20,
      });

    this.setState({ isRateValid: true });
  };

  onPressAddress = item => {
    this.props
      .changeAddress(item.AdresType, item.AdresKey)
      .then(mes => {
        if (mes === "START_ORDER_SUCCESS") this.props.navigation.navigate("EditOrderScreen");
      })
      .catch(err => {
        console.log(err);
      });
  };

  onBackPress = () => {
    if (this.props.homePage.dirty) {
      Alert.alert(
        "Değişiklikler yapıldı",
        "Bu sayfayı terkederek yaptığınız değişikliklerin iptal edilmesini onaylıyormusunuz?",
        [
          {
            text: "Evet",
            onPress: () => {
              this.props.setDirty(false);
              this.props.navigation.goBack();
            },
          },
          {
            text: "Hayır",
            style: "cancel",
          },
        ],
        { cancelable: true },
      );
    } else this.props.navigation.goBack();
  };

  componentDidUpdate(prevProps, prevState) {
    //when stage is changed check if home page is dirty
    //if redux dirty is different than actual dirty, set dirty
    const KisiselAyarlar = this.props.homePage.info.KisiselAyarlar;
    if (prevState !== this.state) {
      let isDirty =
        KisiselAyarlar.OzelFiyatlarimiGoster !== this.state.OzelFiyatlarimiGoster ||
        KisiselAyarlar.BenimFiyatimiGoster !== this.state.BenimFiyatimiGoster ||
        KisiselAyarlar.TESFiyatlariniGoster !== this.state.TESFiyatlariniGoster ||
        KisiselAyarlar.BenimFiyatimOrani !== this.state.BenimFiyatimOrani;

      if (isDirty !== this.props.homePage.dirty) {
        this.props.setDirty(isDirty);
      }
    }
    //if changes are canceled set homepage state to component state
    if (prevProps.homePage.dirty !== this.props.homePage.dirty && !this.props.homePage.dirty) {
      this.setState({
        OzelFiyatlarimiGoster: KisiselAyarlar.OzelFiyatlarimiGoster,
        BenimFiyatimiGoster: KisiselAyarlar.BenimFiyatimiGoster,
        TESFiyatlariniGoster: KisiselAyarlar.TESFiyatlariniGoster,
        BenimFiyatimOrani: KisiselAyarlar.BenimFiyatimOrani,
      });
    }
  }

  saveSettings() {
    this.setState({ saving: true });
    var params = [];
    params.push({ name: "Si", value: this.props.session.info.SessionID });
    params.push({ name: "tf", value: this.state.TESFiyatlariniGoster });
    params.push({ name: "sb", value: this.state.OzelFiyatlarimiGoster });
    params.push({ name: "bc", value: this.state.BenimFiyatimiGoster });
    params.push({ name: "bf", value: this.state.BenimFiyatimOrani });
    params.push({ name: "func", value: "3" });

    saveSettings(params)
      .then(res => {
        res.json().then(json => {
          console.log("save respnose", json);
          this.props
            .getHomePage()
            .then(payload => {
              this.setState(
                {
                  BenimFiyatimiGoster: payload.KisiselAyarlar.BenimFiyatimiGoster,
                  OzelFiyatlarimiGoster: payload.KisiselAyarlar.OzelFiyatlarimiGoster,
                  BenimFiyatimOrani: payload.KisiselAyarlar.BenimFiyatimOrani,
                  saving: false,
                },
                () => {
                  this.props.showToast("Ayarlarınız kaydedilidi.");
                  Keyboard.dismiss();
                },
              );
            })
            .catch(err => {
              console.error(err);
            });
        });
      })
      .catch(err => {
        console.error(err);
        this.input.blur();
        Alert.alert(
          "Hata",
          "Bir hata oluştu.",
          [
            {
              text: "Tamam",
              onPress: () => {},
            },
          ],
          { cancelable: true },
        );
      });
  }

  onPressSave = () => {
    if (this.props.homePage.dirty) {
      if (this.state.isRateValid) this.saveSettings();
      else {
        Alert.alert(
          "Uyarı",
          "Benim fiyatım oranı 0 ile 20 arasında olmalı!",
          [
            {
              text: "Tamam",
              onPress: () => {},
            },
          ],
          { cancelable: true },
        );
      }
    } else {
      Alert.alert(
        "Bilgi",
        "Kaydedilecek bir değişiklik yok",
        [
          {
            text: "Tamam",
            onPress: () => {
              console.log("tamam");
            },
          },
        ],
        { cancelable: false },
      );
    }
  };

  render() {
    return (
      <SafeAreaView style={{ backgroundColor: colors.primary_dark }} forceInset={{ top: "always" }}>
        <View
          style={{
            paddingTop:
              moderateScale(50, 0.4) +
              (Platform.OS === "android"
                ? Platform.Version < 20
                  ? 0
                  : StatusBar.currentHeight
                : 0),
            height: win.height,
            width: win.width,
            backgroundColor: "white",
          }}
        >
          <SimpleHeader
            navigation={this.props.navigation}
            title="Fiyat Ayarları"
            button={() => (
              <TouchableOpacity
                style={{ padding: 5, paddingHorizontal: 0 }}
                onPress={this.onPressSave}
              >
                <Text fontScale={0.6} style={{ color: "white", fontSize: 16, fontWeight: "bold" }}>
                  Kaydet
                </Text>
              </TouchableOpacity>
            )}
            onBackPress={this.onBackPress}
            style={{
              backgroundColor: colors.primary_dark,
              borderColor: colors.primary_dark,
            }}
            iconColor="white"
            titleStyle={{ color: "white" }}
          />
          <TouchableWithoutFeedback
            style={{ flexDirection: "column" }}
            onPress={() => Keyboard.dismiss()}
          >
            <View style={{ padding: 8 }}>
              <View
                style={{
                  flexDirection: "row",
                  marginBottom: 5,
                  alignItems: "center",
                  justifyContent: "space-between",
                }}
              >
                <Text fontScale={0.6} style={{ fontSize: 16 }}>
                  Size özel veya benim fiyatımı göster
                </Text>
                <Switch
                  value={this.state.OzelFiyatlarimiGoster}
                  onValueChange={val => this.setState({ OzelFiyatlarimiGoster: val })}
                />
              </View>
              <View
                style={{
                  flexDirection: "row",
                  marginBottom: 5,
                  alignItems: "center",
                  marginLeft: 5,
                }}
              >
                <RadioButton
                  selected={!this.state.BenimFiyatimiGoster}
                  disabled={!this.state.OzelFiyatlarimiGoster}
                  onPress={() =>
                    this.state.BenimFiyatimiGoster
                      ? this.setState({ BenimFiyatimiGoster: false })
                      : null
                  }
                />
                <Text
                  fontScale={0.6}
                  style={{
                    fontSize: 16,
                    marginLeft: 5,
                    color: this.state.OzelFiyatlarimiGoster ? "black" : "gray",
                  }}
                >
                  {this.props.session.info.YayginKanalMi
                    ? "Tek Fiyat"
                    : "Size özel veya benim fiyatımı göster"}
                </Text>
              </View>
              <View
                style={{
                  flexDirection: "row",
                  marginBottom: 5,
                  alignItems: "center",
                  marginLeft: 5,
                }}
              >
                <RadioButton
                  selected={this.state.BenimFiyatimiGoster}
                  disabled={!this.state.OzelFiyatlarimiGoster}
                  onPress={() =>
                    !this.state.BenimFiyatimiGoster
                      ? this.setState({ BenimFiyatimiGoster: true })
                      : null
                  }
                />
                <Text
                  fontScale={0.6}
                  style={{
                    fontSize: 16,
                    marginLeft: 5,
                    color: this.state.OzelFiyatlarimiGoster ? "black" : "gray",
                  }}
                >
                  Benim fiyatımı göster
                </Text>
              </View>
              <View
                style={{
                  width: win.width - 16,
                  height: 1,
                  backgroundColor: "transparent",
                  marginVertical: 4,
                }}
              />
              {!this.props.session.info.YayginKanalMi && (
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "space-between",
                  }}
                >
                  <Text fontScale={0.6} style={{ fontSize: 16 }}>
                    TES fiyatını göster
                  </Text>
                  <Switch
                    value={this.state.TESFiyatlariniGoster}
                    onValueChange={val => this.setState({ TESFiyatlariniGoster: val })}
                  />
                </View>
              )}
              <View
                style={{
                  width: win.width - 16,
                  height: 1,
                  backgroundColor: "transparent",
                  marginVertical: 4,
                }}
              />
              <View style={{ flexDirection: "column" }}>
                <Text
                  fontScale={0.6}
                  style={{
                    fontSize: 16,
                    fontFamily: "Poppins",
                    marginBottom: 5,
                  }}
                >
                  Benim Fiyatım Oranı (%)
                </Text>
                <View style={{ flexDirection: "column" }}>
                  <View
                    style={{
                      flexDirection: "row",
                      marginBottom: 10,
                      alignItems: "center",
                    }}
                  >
                    <TouchableOpacity
                      style={{
                        height: moderateScale(30, 0.5),
                        alignItems: "center",
                        justifyContent: "center",
                        backgroundColor: "#bebebe",
                        paddingHorizontal: moderateScale(5, 0.5),
                      }}
                      activeOpacity={0.5}
                      onPress={this.decreaseNumber}
                      disabled={
                        !this.state.OzelFiyatlarimiGoster || !this.state.BenimFiyatimiGoster
                      }
                    >
                      <Text
                        style={{
                          fontSize: 14,
                          fontWeight: "500",
                          color: "white",
                        }}
                      >
                        -
                      </Text>
                    </TouchableOpacity>
                    <TextInput
                      ref={c => (this.input = c)}
                      editable={this.state.OzelFiyatlarimiGoster && this.state.BenimFiyatimiGoster}
                      style={{
                        fontSize: moderateScale(14, 0.4),
                        height: moderateScale(30, 0.5),
                        paddingHorizontal: moderateScale(5, 0.5),
                        borderColor: "#bebebe",
                        borderWidth: 4,
                        borderLeftWidth: 0,
                        borderRightWidth: 0,
                        textAlign: "center",
                        paddingVertical: 0,
                        color: this.state.isRateValid ? "gray" : "red",
                      }}
                      autoCorrect={false}
                      autoFocus={false}
                      onChangeText={text =>
                        this.setState({
                          BenimFiyatimOrani: Number(text),
                          isRateValid: Number(text) >= 0 && Number(text) <= 20,
                        })
                      }
                      value={this.state.BenimFiyatimOrani.toString()}
                      keyboardType="numeric"
                      returnKeyType="done"
                    />
                    <TouchableOpacity
                      style={{
                        height: moderateScale(30, 0.5),
                        backgroundColor: "#bebebe",
                        alignItems: "center",
                        justifyContent: "center",
                        paddingHorizontal: moderateScale(5, 0.5),
                      }}
                      activeOpacity={0.5}
                      onPress={this.increaseNumber}
                      disabled={
                        !this.state.OzelFiyatlarimiGoster || !this.state.BenimFiyatimiGoster
                      }
                    >
                      <Text
                        style={{
                          fontSize: 14,
                          fontWeight: "500",
                          color: "white",
                        }}
                      >
                        +
                      </Text>
                    </TouchableOpacity>
                  </View>
                  {(!this.state.OzelFiyatlarimiGoster || !this.state.BenimFiyatimiGoster) && (
                    <View
                      style={{
                        flexDirection: "row",
                        alignItems: "center",
                        marginBottom: 10,
                      }}
                    >
                      <Image
                        source={require("@assets/images/info-white.png")}
                        style={{
                          width: 20,
                          height: 20,
                          marginRight: 5,
                          tintColor: "red",
                          marginTop: 2,
                        }}
                      />
                      <Text
                        fontScale={0.6}
                        style={{
                          fontSize: 16,
                          color: "gray",
                          fontFamily: "Poppins",
                          flex: 1,
                        }}
                      >
                        Bu oranı değiştirebilmek için önce &quot;Benim fiyatımı göster&quot;
                        seçeneğini etkinleştirin.
                      </Text>
                    </View>
                  )}
                  <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <Image
                      source={require("@assets/images/info-white.png")}
                      style={{
                        width: 20,
                        height: 20,
                        marginRight: 5,
                        tintColor: this.state.isRateValid ? "green" : "red",
                      }}
                    />
                    <Text
                      fontScale={0.6}
                      style={{
                        fontSize: 16,
                        color: "gray",
                        fontFamily: "Poppins",
                      }}
                    >
                      0 ile 20 arasında olmalıdır.
                    </Text>
                  </View>
                  <View
                    style={{
                      width: win.width - 16,
                      height: 1,
                      backgroundColor: "transparent",
                      marginVertical: 5,
                    }}
                  />
                  <View style={{ marginTop: 0 }}>
                    <Text
                      fontScale={0.6}
                      style={{
                        fontSize: 14,
                        marginBottom: 5,
                        fontFamily: "Poppins-SemiBold",
                      }}
                    >
                      Benim fiyatım nedir?
                    </Text>
                    <Text
                      fontScale={0.6}
                      style={{
                        fontSize: 14,
                        color: "gray",
                        marginBottom: 5,
                        fontFamily: "Poppins",
                      }}
                    >
                      Benim fiyatım size özel fiyat üzerinden, sizin belirleyeceğiniz oran eklenerek
                      hesaplanır.
                    </Text>
                    <Text
                      fontScale={0.6}
                      style={{
                        fontSize: 14,
                        color: "gray",
                        marginBottom: 5,
                        fontFamily: "Poppins",
                      }}
                    >
                      &quot;Bayi Fiyatı&quot; etiketi ile ürün listesinde ve ürün detayında görünür.
                    </Text>
                    <Text
                      fontScale={0.6}
                      style={{
                        fontSize: 14,
                        color: "gray",
                        marginBottom: 5,
                        fontFamily: "Poppins",
                      }}
                    >
                      Yukarıdaki ayarlar ile hangisinin görünceğine karar verebilirsiniz
                    </Text>
                  </View>
                </View>
              </View>
            </View>
          </TouchableWithoutFeedback>
          {this.state.saving && (
            <View
              style={{
                width: win.width,
                height: win.height,
                justifyContent: "center",
                alignItems: "center",
                position: "absolute",
                top: 0,
                left: 0,
                backgroundColor: "rgba(0,0,0,0.3)",
              }}
            >
              <PencereIndicator />
            </View>
          )}
        </View>
      </SafeAreaView>
    );
  }
}
function mapStateToProps(state) {
  return {
    homePage: state.homePage,
    session: state.session,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    getHomePage: () => dispatch(getHomePage()),
    setDirty: dirty => dispatch(setDirty(dirty)),
    showToast: msg => dispatch(showToast(msg)),
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(PriceSettingsScreen);
