import React, { Component } from "react";
import { View, TouchableOpacity, Image, LayoutAnimation } from "react-native";
import { Text } from "@components";
import { UPDATE_SPRINGY } from "@common/LayoutAnimations";

export default class SupportCategory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showAll: false,
    };
  }

  render() {
    const itemsToShow = this.state.showAll
      ? this.props.item.AltKategoriler
      : this.props.item.AltKategoriler.length > 4
      ? this.props.item.AltKategoriler.slice(0, 4)
      : this.props.item.AltKategoriler;
    const showAllButton =
      itemsToShow.length < this.props.item.AltKategoriler.length || this.state.showAll;
    return (
      <View style={{ flexDirection: "column", marginRight: 30 }}>
        <Text
          fontScale={0.5}
          style={{
            marginBottom: 10,
            color: "#0196e6",
            fontFamily: "Poppins",
            fontSize: 14,
            lineHeight: 18,
            marginTop: 4,
          }}
        >
          {this.props.item.AnaKategori}
        </Text>
        {itemsToShow.map((subItem, index) => {
          return (
            <TouchableOpacity onPress={() => this.props.showInfo(subItem)}>
              <Text
                fontScale={0.5}
                style={{
                  marginBottom: index < itemsToShow.length ? 10 : 0,
                  fontFamily: "Poppins",
                  fontSize: 12,
                  lineHeight: 16,
                  marginTop: 3,
                  color: "gray",
                }}
              >
                {subItem.Baslik}
              </Text>
            </TouchableOpacity>
          );
        })}
        {showAllButton && (
          <TouchableOpacity
            onPress={() => {
              this.setState({ showAll: !this.state.showAll });
              LayoutAnimation.configureNext(UPDATE_SPRINGY);
            }}
            style={{ flexDirection: "row", alignItems: "center" }}
          >
            {this.state.showAll && (
              <Image
                source={require("@assets/images/left-arrow-mini.png")}
                style={{ marginRight: 5, width: 8, height: 8, tintColor: "#0196e6" }}
              />
            )}
            <Text
              fontScale={0.5}
              style={{
                color: "#0196e6",
                fontFamily: "Poppins",
                fontSize: 12,
                lineHeight: 16,
                marginTop: 3,
              }}
            >
              {this.state.showAll ? "Küçült" : "Hepsini Göster"}
            </Text>
            {!this.state.showAll && (
              <Image
                source={require("@assets/images/arrow-right.png")}
                style={{ marginLeft: 5, width: 8, height: 8, tintColor: "#0196e6" }}
              />
            )}
          </TouchableOpacity>
        )}
      </View>
    );
  }
}
