import React, { Component } from "react";
import {
  Platform,
  Keyboard,
  TextInput,
  Image,
  View,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
} from "react-native";
import { Text, ModalBox } from "@components";

const win = Dimensions.get("window");

export default class QuestionModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isVisible: false,
      keyboardVisible: false,
      keyboardHeight: 0,
    };
  }

  componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener("keyboardDidShow", this._keyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener("keyboardDidHide", this._keyboardDidHide);
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow = e => {
    this.setState({
      keyboardVisible: true,
      keyboardHeight: e.endCoordinates.height,
    });
  };

  _keyboardDidHide = () => {
    this.setState({ keyboardVisible: false });
  };

  componentDidUpdate(prevProps, prevState) {
    if (this.props.isVisible !== prevProps.isVisible)
      this.setState({
        isVisible: this.props.isVisible,
      });
  }

  toggleVisibility = () => {
    this.modalBox.toggleVisibility();
  };

  render() {
    return (
      <ModalBox
        ref={c => (this.modalBox = c)}
        statusBarHidden={false}
        animationType="slide"
        scrollable={false}
        presentationStyle="overFullScreen"
      >
        <TouchableOpacity
          style={[
            styles.container,
            {
              marginBottom: this.state.keyboardVisible
                ? this.state.keyboardHeight - (Platform.OS === "ios" ? win.height / 16 : 0)
                : 0,
            },
          ]}
          onPress={() => Keyboard.dismiss()}
        >
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "space-between",
              width: win.width * 0.9,
              paddingHorizontal: 16,
              paddingVertical: 12,
            }}
          >
            <Text
              style={{
                fontSize: 11,
                letterSpacing: 1.5,
                color: "black",
                fontFamily: "Poppins-SemiBold",
              }}
            >
              YENİ SORU OLUŞTURMA
            </Text>
            <TouchableOpacity onPress={() => this.toggleVisibility()}>
              <Image
                source={require("@assets/images/close.png")}
                style={{
                  width: 24,
                  height: 24,
                  tintColor: "#afafaf",
                  resizeMode: "contain",
                }}
              />
            </TouchableOpacity>
          </View>
          <TextInput
            placeholder="Konu"
            placeholderTextColor="gray"
            style={{
              borderColor: "#dedede",
              borderWidth: 1,
              width: win.width * 0.8,
              height: 30,
              borderRadius: 3,
              marginBottom: 15,
              padding: 8,
            }}
            multiline={false}
          />
          <TextInput
            placeholder="Mesaj"
            placeholderTextColor="gray"
            style={{
              borderColor: "#dedede",
              borderWidth: 1,
              width: win.width * 0.8,
              height: 120,
              borderRadius: 3,
              marginBottom: 15,
              padding: 8,
            }}
            multiline={true}
          />
          <View
            style={{
              flexDirection: "row",
              width: win.width * 0.9,
              justifyContent: "flex-end",
              paddingHorizontal: win.width * 0.05,
              marginBottom: 15,
            }}
          >
            <TouchableOpacity style={{ backgroundColor: "#33ba22", borderRadius: 3 }}>
              <Text
                style={{
                  color: "white",
                  fontFamily: "Poppins-SemiBold",
                  fontSize: 16,
                  lineHeight: 20,
                  marginTop: 4,
                  paddingHorizontal: 16,
                  paddingVertical: 4,
                }}
              >
                Oluştur
              </Text>
            </TouchableOpacity>
          </View>
        </TouchableOpacity>
      </ModalBox>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "column",
    backgroundColor: "white",
    width: win.width * 0.9,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.5,
    shadowRadius: 2,
    elevation: 1,
    alignItems: "center",
    marginBottom: 50,
    borderRadius: 6,
  },
});
