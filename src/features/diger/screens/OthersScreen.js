import React, { Component } from "react";
import {
  SafeAreaView,
  View,
  TouchableOpacity,
  Dimensions,
  Animated,
  FlatList,
  Platform,
  StyleSheet,
  StatusBar,
  Image,
} from "react-native";
import { Text, TabBarIcon, AnimatedHeader } from "@components";
import colors from "@common/styles/colors";
import { scale, moderateScale } from "react-native-size-matters";
import { connect } from "react-redux";

const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);

const win = Dimensions.get("window");

class OthersScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    header: null,
    tabBarIcon: ({ tintColor }) => (
      <TabBarIcon
        navigation={navigation}
        style={{ width: 20, height: 20 }}
        icon={require("@assets/images/ellipsis.png")}
        label="Diğer"
        tintColor={tintColor}
      />
    ),
  });

  constructor(props) {
    super(props);
    const scrollAnim = new Animated.Value(0);
    const offsetAnim = new Animated.Value(0);
    this.state = {
      isChatOn: false,
      chatVisible: false,
      scrollAnim,
      clampedScroll: Animated.diffClamp(
        Animated.add(
          scrollAnim.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 1],
            extrapolateLeft: "clamp",
          }),
          offsetAnim,
        ),
        0,
        50,
      ),
    };
  }

  renderHeader = () => {
    return (
      <View style={{ flexDirection: "column", alignItems: "center" }}>
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate("ContactScreen")}
          style={styles.itemContainer}
        >
          <Text style={styles.itemTitle}>İletişim</Text>
          <Image source={require("@assets/images/arrow-right.png")} style={styles.arrow} />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate("BankAccountsScreen")}
          style={styles.itemContainer}
        >
          <Text style={styles.itemTitle}>Havale Bilgileri</Text>
          <Image source={require("@assets/images/arrow-right.png")} style={styles.arrow} />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate("SupportScreen")}
          style={styles.itemContainer}
        >
          <Text style={styles.itemTitle}>Destek</Text>
          <Image source={require("@assets/images/arrow-right.png")} style={styles.arrow} />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate("MusteriTemsilcisiScreen")}
          style={styles.itemContainer}
        >
          <Text style={styles.itemTitle}>Müşteri Temsilciniz</Text>
          <Image source={require("@assets/images/arrow-right.png")} style={styles.arrow} />
        </TouchableOpacity>
      </View>
    );
  };

  render() {
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: colors.primary_dark }}>
        <View style={{ flex: 1, width: win.width, backgroundColor: "white" }}>
          <AnimatedFlatList
            style={{ marginBottom: 20 }}
            contentOffset={{ y: -moderateScale(100, 0.4) }}
            contentInset={{
              top: moderateScale(100, 0.4),
              left: 0,
              bottom: 0,
              right: 0,
            }}
            onScroll={Animated.event(
              [{ nativeEvent: { contentOffset: { y: this.state.scrollAnim } } }],
              {
                useNativeDriver: true,
              },
            )}
            scrollEventThrottle={16}
            overScrollMode="never"
            contentContainerStyle={{
              flexGrow: 1,
              alignItems: "center",
              paddingBottom: scale(60),
              paddingTop:
                Platform.OS === "android"
                  ? moderateScale(100, 0.4) +
                    (Platform.Version < 20 ? 0 : StatusBar.currentHeight) +
                    moderateScale(10, 0.5)
                  : moderateScale(10, 0.5),
            }}
            data={this.data}
            renderItem={this.renderItem}
            ListHeaderComponent={this.renderHeader}
            stickyHeaderIndices={[0]}
          />

          <AnimatedHeader
            navigation={this.props.navigation}
            clampedScroll={this.state.clampedScroll}
            back={false}
            alignLeft={false}
            searchBar={true}
          />
        </View>
      </SafeAreaView>
    );
  }
}

function mapStateToProps(state) {
  return {
    session: state.session,
  };
}

export default connect(mapStateToProps, null)(OthersScreen);

const styles = StyleSheet.create({
  itemContainer: {
    flexDirection: "row",
    width: win.width * 0.94,
    paddingHorizontal: scale(15),
    alignItems: "center",
    justifyContent: "space-between",
    backgroundColor: "#f3f3f3",
    borderRadius: 3,
    marginBottom: 10,
    height: moderateScale(50, 0.5),
  },
  itemTitle: {
    fontSize: 13,
    lineHeight: 16,
    marginTop: 3,
    fontFamily: "Poppins-Medium",
  },
  arrow: {
    width: moderateScale(8, 0.5),
    height: moderateScale(8, 0.5),
    tintColor: "black",
  },
});
