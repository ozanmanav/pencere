import React, { Component } from "react";
import {
  View,
  SafeAreaView,
  Dimensions,
  Linking,
  Alert,
  Platform,
  StatusBar,
  TouchableOpacity,
  Image,
} from "react-native";
import { connect } from "react-redux";
import { colors } from "@common/styles/index.style";
import { Text, AnimatedHeader } from "@components";
import { moderateScale } from "react-native-size-matters";

const win = Dimensions.get("window");

class MusteriTemsilcisiScreen extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <SafeAreaView
        style={{ backgroundColor: colors.primary_dark, flex: 1 }}
        forceInset={{ top: "always" }}
      >
        <View
          style={{
            flexGrow: 1,
            width: win.width,
            backgroundColor: "#efefef",
            paddingTop:
              AnimatedHeader.height +
              moderateScale(20, 0.4) +
              (Platform.OS === "android"
                ? Platform.Version < 20
                  ? 0
                  : StatusBar.currentHeight
                : 0),
          }}
        >
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              paddingHorizontal: win.width / 25,
            }}
          >
            <Text style={{ fontFamily: "Poppins", fontSize: 14 }}>Müşteri Temsilcisi Adı: </Text>
            <Text style={{ fontFamily: "Poppins", color: "gray", fontSize: 14 }}>
              {this.props.session.info.bayiMTAdi ?? "Müşteri Temsilciniz"}
            </Text>
          </View>
          <TouchableOpacity
            onPress={() => {
              if (this.props.session.info.bayiMTTelefon !== "")
                Alert.alert(
                  "Arama",
                  "Müşteri temsilcinizin numarası olan " +
                    this.props.session.info.bayiMTTelefon +
                    " numarası aranmak üzere uygulamayı terkedeceksiniz. Devam etmek istiyor musunuz?",
                  [
                    { text: "İptal" },
                    {
                      text: "Devam Et",
                      onPress: () =>
                        Linking.openURL(`tel:${this.props.session.info.bayiMTTelefon.trim()}`),
                    },
                  ],
                );
              else {
                Alert.alert("Bilgi", "Müşteri temsilci numarası bulunamadı.");
              }
            }}
            style={{
              flexDirection: "row",
              alignItems: "center",
              paddingHorizontal: win.width / 25,
              marginTop: moderateScale(10, 0.5),
            }}
          >
            <Text style={{ fontFamily: "Poppins", fontSize: 14 }}>Masa Telefonu: </Text>
            <Text style={{ fontFamily: "Poppins", color: "gray", fontSize: 14 }}>
              {this.props.session.info.bayiMTTelefon}
            </Text>
            <Image
              source={require("@assets/images/call.png")}
              style={{
                marginLeft: moderateScale(10, 0.5),
                width: moderateScale(14, 0.4),
                marginBottom: moderateScale(2, 0.3),
                height: moderateScale(14, 0.4),
                tintColor: "#03A9F4",
                resizeMode: "contain",
              }}
            />
          </TouchableOpacity>
        </View>
        <AnimatedHeader
          navigation={this.props.navigation}
          title="Müşteri Temsilciniz"
          searchBar={true}
          back={true}
          alignLeft={true}
        />
      </SafeAreaView>
    );
  }
}

function mapStateToProps(state) {
  return {
    session: state.session,
  };
}

export default connect(mapStateToProps, null)(MusteriTemsilcisiScreen);
