import React, { Component } from "react";
import {
  View,
  SafeAreaView,
  ScrollView,
  Animated,
  Alert,
  Dimensions,
  Platform,
  LayoutAnimation,
  Image,
  StatusBar,
  TouchableOpacity,
} from "react-native";
import { AnimatedHeader, Text, PencereIndicator } from "@components";
import { fetchBankAccounts } from "@common/Utils";
import { ScaledSheet, moderateScale } from "react-native-size-matters";
import colors from "@common/styles/colors";
import { UPDATE_SPRINGY } from "@common/LayoutAnimations";

const AnimatedScrollView = Animated.createAnimatedComponent(ScrollView);
const win = Dimensions.get("window");

const SPACE_HORIZONTAL = win.width / 50;

class BankComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isCollapsed: true,
    };
  }

  render() {
    return (
      <TouchableOpacity
        onPress={() => {
          this.setState({ isCollapsed: !this.state.isCollapsed });
          LayoutAnimation.configureNext(UPDATE_SPRINGY);
        }}
        style={[styles.itemContainer]}
      >
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            height: moderateScale(40, 0.5),
          }}
        >
          <Text style={styles.itemTitle}>{this.props.bank.BankaAdi}</Text>
          <Image
            source={require("@assets/images/arrow-right.png")}
            style={{
              marginBottom: moderateScale(2, 0.5),
              marginLeft: moderateScale(10, 0.5),
              transform: this.state.isCollapsed ? [{ rotateZ: "90deg" }] : [{ rotateZ: "-90deg" }],
              width: moderateScale(12, 0.5),
              height: moderateScale(12, 0.5),
              tintColor: "#212121",
            }}
          />
        </View>
        {!this.state.isCollapsed &&
          this.props.subeler_keys.map((sube, i) => {
            const hesaplar_keys = Object.keys(this.props.bank.Subeler[sube].Hesaplar);
            return (
              <View key={i.toString()} style={{ flexDirection: "column" }}>
                <Text style={{ fontFamily: "Poppins-Medium", fontSize: 12 }}>{sube}</Text>
                {hesaplar_keys.map((hesap, index) => {
                  return (
                    <Text key={index.toString()} style={{ fontFamily: "Poppins", fontSize: 12 }}>
                      {hesap + " " + this.props.bank.Subeler[sube].Hesaplar[hesap]}
                    </Text>
                  );
                })}
              </View>
            );
          })}
      </TouchableOpacity>
    );
  }
}

class CityComponent extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const bankalar = this.props.item.banks;
    return (
      <View style={{ flexDirection: "column" }}>
        <TouchableOpacity
          style={{
            flexDirection: "row",
            alignItems: "center",
            height: moderateScale(40, 0.5),
          }}
        >
          <Text
            style={{
              fontFamily: "Poppins-Medium",
              fontSize: 14,
              marginRight: moderateScale(10, 0.5),
            }}
          >
            {this.props.item.city}
          </Text>
        </TouchableOpacity>
        <View style={[{ flexDirection: "column" }]}>
          {bankalar.map((bank, i) => {
            const subeler_keys = Object.keys(bank.Subeler);
            return <BankComponent key={i.toString()} bank={bank} subeler_keys={subeler_keys} />;
          })}
        </View>
      </View>
    );
  }
}
export default class BankAccountsScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isFetching: false,
      accounts: [],
    };
  }

  componentDidMount() {
    this.setState({ isFetching: true });
    fetchBankAccounts()
      .then((accounts) => {
        var array = Object.keys(accounts).map(function (key) {
          return { city: key, banks: accounts[key] };
        });
        this.setState({ accounts: array });
        this.setState({ isFetching: false });
      })
      .catch(() => {
        this.setState({ isFetching: false });
        Alert.alert(
          "Hata",
          "Banka hesapları alınırken bir hata oluştu. Lütfen daha sonra tekrar deneyiniz.",
        );
      });
  }

  renderItem = ({ item }) => {
    return (
      <View style={styles.cityContainer}>
        <CityComponent item={item} />
      </View>
    );
  };

  render() {
    return (
      <SafeAreaView style={{ backgroundColor: colors.primary_dark, flex: 1 }}>
        <View style={{ flexGrow: 1, width: win.width, backgroundColor: "white" }}>
          <AnimatedScrollView
            contentOffset={{ y: -AnimatedHeader.height }}
            contentInset={{
              top: AnimatedHeader.height,
              left: 0,
              bottom: 0,
              right: 0,
            }}
            onScroll={Animated.event(
              [{ nativeEvent: { contentOffset: { y: this.state.scrollAnim } } }],
              {
                useNativeDriver: true,
              },
            )}
            scrollEventThrottle={16}
            overScrollMode="never"
            contentContainerStyle={{
              flexGrow: 1,
              flexDirection: "column",
              paddingTop:
                Platform.OS === "android"
                  ? AnimatedHeader.height +
                    (Platform.Version < 20 ? 0 : StatusBar.currentHeight) +
                    moderateScale(10, 0.5)
                  : moderateScale(10, 0.5),
            }}
          >
            {this.state.accounts.map((item) => this.renderItem({ item }))}
          </AnimatedScrollView>
        </View>
        {this.state.isFetching && (
          <View style={[styles.loadingContainer]}>
            <PencereIndicator />
          </View>
        )}

        <AnimatedHeader
          navigation={this.props.navigation}
          clampedScroll={this.state.clampedScroll}
          back={true}
          title={"Havale Bilgileri"}
          alignLeft={true}
          searchBar={true}
        />
      </SafeAreaView>
    );
  }
}

const styles = ScaledSheet.create({
  itemTitle: {
    color: colors.price_color,
    fontFamily: "Poppins-SemiBold",
    fontSize: 14,
  },
  itemContainer: {
    flexDirection: "column",
    backgroundColor: "#ffedc2",
    marginBottom: "10@ms0.5",
    padding: SPACE_HORIZONTAL,
  },
  cityContainer: {
    width: win.width,
    paddingHorizontal: SPACE_HORIZONTAL,
  },
  loadingContainer: {
    position: "absolute",
    top: 0,
    left: 0,
    height: win.height,
    width: win.width,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0,0,0,0.3)",
  },
});
