import React, { Component } from "react";
import {
  SafeAreaView,
  Linking,
  View,
  TouchableOpacity,
  Image,
  Dimensions,
  Animated,
  FlatList,
  Platform,
  Alert,
} from "react-native";
import { Text, TabBarIcon, AnimatedHeader } from "@components";
import MapView, { Marker } from "react-native-maps";
import colors from "@common/styles/colors";
import { moderateScale } from "react-native-size-matters";
import { getEndPoint } from "@common/Utils";

const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);

const win = Dimensions.get("window");

export default class ContactScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    header: null,
    tabBarIcon: ({ tintColor }) => (
      <TabBarIcon
        navigation={navigation}
        style={{ width: 20, height: 20 }}
        icon={require("@assets/images/ellipsis.png")}
        label="Diğer"
        tintColor={tintColor}
      />
    ),
  });

  componentDidMount() {
    fetch(getEndPoint("CONTACTS_URL"))
      .then((res) => res.json())
      .then((responseJson) => {
        this.locations = responseJson;
        var coords = [];
        responseJson.forEach((location) => {
          coords.push(location.coords);
        });
        this.setState({ coords });
      })
      .catch((err) => {
        console.log(err);
        Alert.alert("Hata", "Bir hata oluştu.", [
          { text: "Tamam", onPress: () => this.props.navigation.goBack() },
        ]);
      });
  }

  constructor(props) {
    super(props);
    const scrollAnim = new Animated.Value(0);
    this.state = {
      scrollAnim,
      clampedScroll: Animated.diffClamp(
        Animated.add(
          Animated.add(scrollAnim, AnimatedHeader.height).interpolate({
            inputRange: [0, 1],
            outputRange: [0, 1],
            extrapolateLeft: "clamp",
          }),
          -AnimatedHeader.height,
        ),
        0,
        AnimatedHeader.height,
      ),
      coords: [],
    };
    this.renderItem = this.renderItem.bind(this);
  }

  onRegionChangeComplete(region, index) {
    var regions = this.state.coords;
    regions[index] = region;
    this.setState({ coords: regions });
  }

  returnToDefault(index) {
    var regions = this.state.coords;
    regions[index] = this.locations[index].coords;
    this.setState({ coords: regions });
  }

  renderItem({ item, index }) {
    return (
      <View style={{ marginTop: win.height * 0.05 }}>
        <View>
          <MapView
            style={{
              width: win.width * 0.9,
              height: win.width * 0.9 * 0.5,
              alignSelf: "center",
            }}
            region={this.state.coords[index]}
            onRegionChangeComplete={(region) => this.onRegionChangeComplete(region, index)}
          >
            <Marker coordinate={item.coords} title={"Arena Bilgisayar"} description={item.title} />
          </MapView>
          <TouchableOpacity
            onPress={() =>
              Linking.openURL(
                "https://www.google.com/maps/search/?api=1&query=" +
                  item.coords.latitude +
                  "," +
                  item.coords.longitude,
              )
            }
            style={{
              width: 40,
              height: 40,
              opacity: 0.5,
              position: "absolute",
              marginLeft: 10,
              marginTop: 10,
              alignItems: "center",
            }}
          >
            <Image
              source={require("@assets/images/gmaps.png")}
              style={{ width: 36, height: 36, resizeMode: "contain" }}
            />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.returnToDefault(index)}
            style={{
              borderRadius: 4,
              width: 30,
              height: 30,
              opacity: 0.5,
              position: "absolute",
              marginLeft: win.width * 0.9 - 40,
              marginTop: 10,
              backgroundColor: "gray",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Image
              source={require("@assets/images/target.png")}
              style={{ width: 24, height: 24, resizeMode: "contain" }}
            />
          </TouchableOpacity>
        </View>
        <View
          style={{
            width: win.width * 0.9,
            padding: win.width * 0.05,
            height: win.width * 0.9 * 0.5,
            alignSelf: "center",
            backgroundColor: "white",
          }}
        >
          <Text
            style={{
              fontSize: 14,
              lineHeight: 18,
              fontFamily: "Poppins-SemiBold",
              color: colors.price_color,
            }}
          >
            {item.title}
          </Text>
          {item.desc.map((descItem, i) => {
            return (
              <Text
                key={i.toString()}
                style={{
                  fontSize: 12,
                  lineHeight: 15,
                  fontFamily: "Poppins",
                  marginTop: 8,
                }}
              >
                {descItem}
              </Text>
            );
          })}
        </View>
      </View>
    );
  }

  render() {
    if (this.state.coords.length > 0)
      return (
        <SafeAreaView style={{ flex: 1, backgroundColor: colors.primary_dark }}>
          <View
            style={{
              flexGrow: 1,
              width: win.width,
              backgroundColor: "#efefef",
            }}
          >
            <AnimatedFlatList
              contentOffset={{ y: -AnimatedHeader.height }}
              contentInset={{
                top: AnimatedHeader.height,
                left: 0,
                bottom: 0,
                right: 0,
              }}
              overScrollMode="never"
              contentContainerStyle={{
                flexGrow: 1,
                alignItems: "center",
                paddingTop: Platform.OS === "android" ? moderateScale(120, 0.4) : 0,
                paddingBottom: moderateScale(20, 0.4),
              }}
              onScroll={Animated.event(
                [
                  {
                    nativeEvent: { contentOffset: { y: this.state.scrollAnim } },
                  },
                ],
                { useNativeDriver: true },
              )}
              scrollEventThrottle={16}
              data={this.locations}
              renderItem={this.renderItem}
              ListHeaderComponent={this.renderHeader}
            />

            <AnimatedHeader
              navigation={this.props.navigation}
              title="İletişim"
              clampedScroll={this.state.clampedScroll}
              searchBar={true}
              back={true}
              alignLeft={true}
            />
          </View>
        </SafeAreaView>
      );
    else return null;
  }
}
