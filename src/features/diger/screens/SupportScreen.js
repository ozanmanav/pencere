import React, { Component } from "react";
import {
  StatusBar,
  StyleSheet,
  View,
  Platform,
  LayoutAnimation,
  Animated,
  TouchableOpacity,
  Image,
  Dimensions,
  FlatList,
  ScrollView,
  ImageBackground,
  SafeAreaView,
} from "react-native";
import { connect } from "react-redux";
import {
  AnimatedHeader,
  CollapsibleButton,
  PencereIndicator,
  ScrollModal,
  Text,
} from "@components";
import { QuestionModal, SupportCategory } from "@features/diger/components";
import colors from "@common/styles/colors";
import menuJson from "@common/destek-menu.json";
import { scale, moderateScale } from "react-native-size-matters";
import { CREATE_UPDATE_SPRINGY, UPDATE_SPRINGY } from "@common/LayoutAnimations";
import { TextInput } from "react-native-gesture-handler";

const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);

const win = Dimensions.get("window");
const NAVBAR_HEIGHT = AnimatedHeader.height;
const STATUS_BAR_HEIGHT = 0;

var faqData = [
  {
    title: "Sipariş Verme İşlemi",
    ana: 0,
    alt: 0,
  },
  {
    title: "Sipariş Takibi Yapmak için",
    ana: 0,
    alt: 2,
  },
  {
    title: "Siparişinizi Teslim Alırken",
    ana: 0,
    alt: 3,
  },
  {
    title: "Sipariş İptal",
    ana: 0,
    alt: 4,
  },
  {
    title: "Teklif Verme",
    ana: 4,
    alt: 1,
  },
  {
    title: "Defolu Ürünler",
    ana: 0,
    alt: 6,
  },
  {
    title: "İade Esasları / İade Takip",
    ana: 1,
    alt: 1,
  },
  {
    title: "Paynet Üyeliği",
    ana: 6,
    alt: 2,
  },
];

class SupportScreen extends Component {
  constructor(props) {
    super(props);
    const scrollAnim = new Animated.Value(0);
    this.state = {
      selectedCat: {},
      searchView: false,
      searchResults: [],
      isFetching: false,
      items: [],
      searchText: "",
      scrollAnim,
      clampedScroll: Animated.diffClamp(
        Animated.add(
          Animated.add(scrollAnim, 100).interpolate({
            inputRange: [0, 1],
            outputRange: [0, 1],
            extrapolateLeft: "clamp",
          }),
          -100,
        ),
        0,
        NAVBAR_HEIGHT - STATUS_BAR_HEIGHT,
      ),
    };
  }

  renderItem = ({ item }) => {
    return (
      <TouchableOpacity
        style={styles.itemContainer}
        onPress={() => this.props.navigation.navigate("HelpDetailsScreen", { item })}
      >
        <Text style={{ fontSize: 16 }}>{item.Baslik}</Text>
        <Image
          source={require("@assets/images/arrow-right.png")}
          style={{ width: 16, height: 16, tintColor: "#d0d0d0" }}
        />
      </TouchableOpacity>
    );
  };

  search = text => {
    var searchResults = [];
    menuJson.forEach(mainCat => {
      mainCat.AltKategoriler.forEach(subCat => {
        if (subCat.Baslik.toLowerCase().includes(text)) {
          searchResults.push(subCat);
        }
      });
    });
    if (searchResults !== this.state.searchResults) {
      LayoutAnimation.configureNext(CREATE_UPDATE_SPRINGY);
    }
    this.setState({ searchResults });
  };

  renderHeader = () => {
    return (
      <View style={{ flexDirection: "column" }}>
        <ImageBackground
          style={{
            width: win.width,
            height: win.height / 4,
            resizeMode: "cover",
            paddingHorizontal: win.width * 0.05,
            paddingVertical: ((438 / 750) * win.width) / 10,
            flexDirection: "column",
            alignItems: "center",
            justifyContent: "space-between",
          }}
          source={require("@assets/images/support_bg.png")}
        >
          <View>
            <Text
              style={{
                textAlign: "center",
                fontFamily: "Poppins-SemiBold",
                color: "white",
                fontSize: 16,
                lineHeight: 20,
              }}
            >
              ARADIĞINIZ TÜM CEVAPLAR BURADA
            </Text>
            <Text
              style={{
                textAlign: "center",
                fontFamily: "Poppins",
                color: "white",
                fontSize: 12,
                lineHeight: 16,
                marginHorizontal: 10,
              }}
            >
              Kategorilerden size uygun seçin veya arama kutusunu kullanın
            </Text>
          </View>
          {/* <TouchableOpacity
                        onPress={() => this.refs.questionModal.toggleVisibility()}
                        style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', height: 36, backgroundColor: '#e76652', width: win.width * .6 }}>
                        <View style={{ flex: .2, borderRightWidth: .5, borderRightColor: '#b94435', height: 36, justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 16, }}>+</Text>
                        </View>
                        <View
                            style={{ flex: .8, height: 36, justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 13, }}>YENİ SORU OLUŞTUR</Text>
                        </View>
                    </TouchableOpacity> */}
          <View
            style={{
              flexDirection: "row",
              backgroundColor: "white",
              borderRadius: 3,
              height: moderateScale(30),
              marginTop: 0,
              marginHorizontal: 10,
              alignItems: "center",
              justifyContent: "center",
              paddingHorizontal: 5,
            }}
          >
            <TextInput
              ref={node => {
                this.searchBox = node;
              }}
              style={{
                fontSize: moderateScale(14, 0.5),
                flex: 1,
                height: moderateScale(30),
                color: "black",
                paddingLeft: scale(5),
                paddingVertical: scale(5),
              }}
              placeholder="Tüm kategorilerde ara"
              placeholderTextColor="gray"
              autoCapitalize="none"
              selectTextOnFocus
              multiline={false}
              value={this.state.searchText}
              onFocus={() => {
                if (!this.state.searchView) {
                  this.setState({ searchView: true });
                  LayoutAnimation.configureNext(CREATE_UPDATE_SPRINGY);
                }
              }}
              onChangeText={searchText => {
                this.setState({ searchText });
                if (searchText === "") {
                  if (this.state.searchView) {
                    this.setState({ searchView: false });
                    LayoutAnimation.configureNext(CREATE_UPDATE_SPRINGY);
                  }
                } else {
                  if (!this.state.searchView) {
                    this.setState({ searchView: true });
                    LayoutAnimation.configureNext(CREATE_UPDATE_SPRINGY);
                  }
                  this.search(searchText);
                }
              }}
            />
            <TouchableOpacity style={{ justifyContent: "center", alignItems: "center" }}>
              <Image
                source={require("@assets/images/search.png")}
                style={{
                  width: moderateScale(16, 0.5),
                  height: moderateScale(16, 0.5),
                  tintColor: colors.light_gray,
                  marginRight: scale(5),
                }}
              />
            </TouchableOpacity>
          </View>
        </ImageBackground>
        {!this.state.searchView ? (
          <View style={{ flexDirection: "column", backgroundColor: "white" }}>
            <View style={{ paddingLeft: scale(20), paddingVertical: scale(5) }}>
              <Text
                fontScale={0.5}
                style={{
                  color: "#d6421e",
                  fontFamily: "Poppins",
                  fontSize: 16,
                  lineHeight: 20,
                  marginTop: 4,
                }}
              >
                Kategoriler
              </Text>
            </View>
            <ScrollView
              horizontal={true}
              showsHorizontalScrollIndicator={false}
              contentContainerStyle={{
                paddingLeft: scale(20),
                paddingBottom: scale(15),
                backgroundColor: "white",
              }}
            >
              <View style={{ flexDirection: "column" }}>
                <View style={{ flexDirection: "row" }}>
                  {menuJson.map(item => {
                    return (
                      <SupportCategory
                        item={item}
                        showInfo={selectedItem => {
                          console.log("showInfo", selectedItem);
                          this.setState({ selectedCat: selectedItem }, () =>
                            this.scrollModal.toggleVisibility(selectedItem.Baslik),
                          );
                        }}
                      />
                    );
                  })}
                </View>
              </View>
            </ScrollView>
          </View>
        ) : (
          <View
            style={{ flexDirection: "column", backgroundColor: "white", paddingBottom: scale(20) }}
          >
            <View
              style={{
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "space-between",
                padding: scale(20),
                paddingVertical: scale(10),
              }}
            >
              <Text
                fontScale={0.5}
                style={{ fontFamily: "Poppins-Medium", color: "black", fontSize: 16 }}
              >
                Arama Sonuçları
              </Text>
              <TouchableOpacity
                onPress={() => {
                  if (this.state.searchView) {
                    LayoutAnimation.configureNext(UPDATE_SPRINGY);
                  }
                  this.setState({ searchView: false });
                }}
                style={{ flexDirection: "row", alignItems: "center" }}
              >
                <Text
                  fontScale={0.5}
                  style={{ fontFamily: "Poppins-Medium", color: "#0196e6", fontSize: 12 }}
                >
                  Tüm Kategoriler
                </Text>
                <Image
                  source={require("@assets/images/arrow-right.png")}
                  style={{ marginLeft: 5, width: 8, height: 8, tintColor: "#0196e6" }}
                />
              </TouchableOpacity>
            </View>
            <ScrollView
              contentContainerStyle={{ paddingHorizontal: scale(20) }}
              style={{
                maxHeight: win.height * 0.3,
                flexDirection: "column",
                width: win.width,
                backgroundColor: "white",
              }}
            >
              {this.state.searchResults.length > 0 ? (
                this.state.searchResults.map(item => {
                  return (
                    <TouchableOpacity
                      style={{
                        paddingVertical: scale(5),
                        flexDirection: "row",
                        alignItems: "center",
                      }}
                      onPress={() =>
                        this.setState({ selectedCat: item }, () =>
                          this.scrollModal.toggleVisibility(item.Baslik),
                        )
                      }
                    >
                      <Text
                        fontScale={0.5}
                        style={{ fontFamily: "Poppins", color: "gray", fontSize: 12 }}
                      >
                        {item.Baslik}
                      </Text>
                    </TouchableOpacity>
                  );
                })
              ) : (
                <Text
                  fontScale={0.5}
                  style={{ fontFamily: "Poppins", color: "gray", fontSize: 12 }}
                >
                  Aradığınız konuya benzer bir sonuç bulamadık :(
                </Text>
              )}
            </ScrollView>
          </View>
        )}

        <View style={{ flexDirection: "column", padding: 15 }}>
          <Text
            fontScale={0.5}
            style={{ fontFamily: "Poppins-Medium", fontSize: 16, lineHeight: 20, marginTop: 4 }}
          >
            Sık Sorulan Sorular
          </Text>
          {faqData.map(item => {
            return (
              <CollapsibleButton
                style={{ marginBottom: 5, marginTop: 10 }}
                renderContent={isSelected => (
                  <View
                    style={{
                      flexDirection: "row",
                      width: win.width - 30,
                      height: 40,
                      justifyContent: "space-between",
                      paddingHorizontal: 15,
                      alignItems: "center",
                    }}
                  >
                    <Text
                      fontScale={0.5}
                      style={{
                        fontSize: 12,
                        fontFamily: isSelected ? "Poppins-SemiBold" : "Poppins",
                        color: isSelected ? colors.price_color : "gray",
                      }}
                    >
                      {item.title}
                    </Text>
                    <Text>{isSelected ? "-" : "+"}</Text>
                  </View>
                )}
                renderCollapsibleContent={() => (
                  <View style={{ padding: 15 }}>
                    <Text
                      fontScale={0.5}
                      style={{ fontSize: 12, fontFamily: "Poppins", color: "gray" }}
                    >
                      {menuJson[item.ana].AltKategoriler[item.alt].Aciklama}
                    </Text>
                  </View>
                )}
              />
            );
          })}
        </View>
      </View>
    );
  };

  render() {
    return (
      <SafeAreaView style={{ backgroundColor: colors.primary_dark }} forceInset={{ top: "always" }}>
        <View style={{ width: win.width, backgroundColor: "#efefef" }}>
          <AnimatedFlatList
            renderItem={this.renderItem}
            contentOffset={{ y: -AnimatedHeader.height }}
            ListHeaderComponent={this.renderHeader}
            contentInset={{ top: AnimatedHeader.height, left: 0, bottom: 0, right: 0 }}
            contentContainerStyle={{
              paddingBottom: 10,
              paddingTop:
                Platform.OS === "android"
                  ? AnimatedHeader.height + (Platform.Version < 20 ? 0 : StatusBar.currentHeight)
                  : 0,
            }}
            // onScroll={
            //     Animated.event(
            //         [
            //             { nativeEvent: { contentOffset: { y: this.state.scrollAnim } } }
            //         ],
            //         { useNativeDriver: true }
            //     )
            // }
            scrollEventThrottle={16}
          />
          {this.state.isFetching && (
            <View style={[styles.loadingContainer]}>
              <PencereIndicator size="large" color="#0000ff" />
            </View>
          )}
        </View>
        <QuestionModal ref={c => (this.questionModal = c)} />
        <ScrollModal
          ref={c => (this.scrollModal = c)}
          forceInset={{ top: "never" }}
          title={this.state.selectedCat.Baslik}
          contentContainerStyle={{
            padding: moderateScale(10, 0.5),
            paddingBottom: moderateScale(30, 0.5),
          }}
          renderContent={() => {
            console.log("renderContent", this.state.selectedCat.Aciklama);
            return (
              <Text fontScale={0.5} style={{ fontFamily: "Poppins", fontSize: 14, color: "black" }}>
                {this.state.selectedCat.Aciklama}
              </Text>
            );
          }}
        />
        <AnimatedHeader
          ref={c => (this.animatedHeader = c)}
          navigation={this.props.navigation}
          title="Destek"
          clampedScroll={this.state.clampedScroll}
          home={true}
          searchBar={true}
          search={false}
          back={true}
          alignLeft={true}
        />
      </SafeAreaView>
    );
  }
}
function mapStateToProps(state) {
  return {
    session: state.session,
  };
}

export default connect(mapStateToProps, null)(SupportScreen);

const styles = StyleSheet.create({
  loadingContainer: {
    height: win.height,
    width: win.width,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0,0,0,0.5)",
    position: "absolute",
    top: 0,
    left: 0,
  },
  itemContainer: {
    padding: 5,
    paddingVertical: 10,
    backgroundColor: "white",
    borderBottomColor: "#dedede",
    borderBottomWidth: 1,
    flexDirection: "row",
    flex: 1,
    justifyContent: "space-between",
  },
});
