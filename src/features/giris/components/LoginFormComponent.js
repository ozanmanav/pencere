import React, { Component } from "react";
import {
  View,
  TextInput,
  TouchableOpacity,
  Dimensions,
  Platform,
  Alert,
  ScrollView,
  Keyboard,
} from "react-native";
import { connect } from "react-redux";
import {
  login,
  fetchCampaignsFromApi,
  initialHomePageProducts,
  getBucket,
  getHomePage,
  setAppState,
} from "@redux/actions";
import { Text } from "@components";
import { moderateScale, verticalScale } from "react-native-size-matters";
import { Api, ApiMode } from "@common/api";

const win = Dimensions.get("window");

class LoginFormComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      disabled: false,
      code: "",
      uname: "",
      pass: "",
      showToast: false,
    };
    this.signIn = this.signIn.bind(this);
  }

  render() {
    return (
      <View style={{ flexDirection: "column", flex: 1 }}>
        <ScrollView
          contentContainerStyle={{
            flex: 1,
            paddingTop: moderateScale(20, 0.6),
          }}
          scrollEnabled={false}
        >
          <View
            style={{
              alignItems: "center",
              marginVertical: moderateScale(10, 0.5),
            }}
          >
            <TextInput
              collapsable={false}
              testID="input_bayi_kodu"
              placeholder="Müşteri Kodu"
              style={{
                color: "gray",
                borderWidth: 1,
                borderColor: "#dedede",
                borderRadius: 3,
                width: win.width * 0.85,
                height: moderateScale(46, 0.4),
                paddingHorizontal: moderateScale(10, 0.5),
                fontSize: 16,
                justifyContent: "center",
                alignItems: "center",
              }}
              placeholderTextColor="gray"
              keyboardType="numeric"
              onChangeText={code => this.setState({ code })}
              value={this.state.code}
              onSubmitEditing={Keyboard.dismiss}
            />
          </View>
          <View
            style={{
              alignItems: "center",
              justifyContent: "flex-end",
              marginVertical: moderateScale(10, 0.6),
            }}
          >
            <TextInput
              collapsable={false}
              testID="input_kullanici_kodu"
              placeholder="Kullanıcı Adı"
              autoCapitalize="none"
              style={{
                color: "gray",
                borderWidth: 1,
                borderColor: "#dedede",
                borderRadius: 3,
                width: win.width * 0.85,
                height: moderateScale(46, 0.4),
                paddingHorizontal: moderateScale(10, 0.5),
                fontSize: 16,
              }}
              placeholderTextColor="gray"
              onChangeText={uname => this.setState({ uname })}
              value={this.state.uname}
              onSubmitEditing={Keyboard.dismiss}
            />
          </View>
          <View
            style={{
              alignItems: "center",
              justifyContent: "flex-end",
              marginVertical: moderateScale(10, 0.6),
            }}
          >
            <TextInput
              ref={input => {
                this.password = input;
              }}
              collapsable={false}
              testID="input_sifre"
              placeholder="Şifre"
              autoCapitalize="none"
              style={{
                color: "gray",
                borderWidth: 1,
                borderColor: "#dedede",
                borderRadius: 3,
                width: win.width * 0.85,
                height: moderateScale(46, 0.4),
                paddingHorizontal: moderateScale(10, 0.5),
                fontSize: 16,
              }}
              maxLength={20}
              placeholderTextColor="gray"
              secureTextEntry={true}
              onChangeText={pass => this.setState({ pass })}
              value={this.state.pass}
              onSubmitEditing={Keyboard.dismiss}
            />
          </View>
          <View
            style={{
              alignItems: "center",
              justifyContent: "flex-end",
              marginTop: moderateScale(10, 0.5),
            }}
          >
            <TouchableOpacity
              testID="button_login"
              onPress={this.signIn}
              disabled={this.state.disabled}
              style={{
                width: win.width * 0.85,
                backgroundColor: "#54a932",
                height: moderateScale(46, 0.4),
                borderRadius: 3,
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <Text fontScale={0.6} style={{ color: "white", fontSize: 18, fontWeight: "700" }}>
                {this.state.disabled ? "Yükleniyor..." : "GİRİŞ"}
              </Text>
            </TouchableOpacity>
          </View>
          <TouchableOpacity
            style={{
              alignItems: "center",
              justifyContent: "center",
              paddingVertical: verticalScale(15, 0.5),
            }}
            onPress={() => {
              if (this.state.uname !== "" && this.state.code !== "")
                this.props.toggleForgotModal(this.state.uname, this.state.code);
              else {
                Alert.alert(
                  "Eksik Bilgi",
                  "Lütfen önce yukarıdaki alanlara bayi kodunuzu ve kullanıcı adınızı giriniz.",
                );
              }
            }}
          >
            <Text
              style={{
                textAlign: "center",
                color: "#95a5a6",
                fontSize: 11,
                fontFamily: "OpenSans-Regular",
              }}
            >
              ŞİFREMİ UNUTTUM
            </Text>
          </TouchableOpacity>
          {Platform.OS === "android" && (
            <TouchableOpacity
              testID="button_bayi_ol"
              onPress={() => this.props.toggleBayiOlModal()}
              style={{
                alignSelf: "center",
                width: win.width * 0.85,
                backgroundColor: "#d55749",
                marginBottom: moderateScale(10, 0.5),
                height: moderateScale(46, 0.4),
                borderRadius: 3,
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <Text
                fontScale={0.6}
                style={{
                  color: "white",
                  fontSize: 18,
                  fontFamily: "Poppins-SemiBold",
                }}
              >
                Pencere Bayisi Olun!
              </Text>
            </TouchableOpacity>
          )}
        </ScrollView>
      </View>
    );
  }

  signIn() {
    var _uname = this.state.uname;
    if (this.state.uname.startsWith("test3@")) {
      _uname = this.state.uname.substring(6);
      this.props.setAppState({ stage: "TEST" });
    } else {
      this.props.setAppState({ stage: "PROD" });
    }
    if (this.state.pass === "" || _uname === "" || this.state.code === "") {
      Alert.alert(
        "Eksik Bilgi",
        "Bütün alanları doldurmalısınız!",
        [{ text: "Tamam", onPress: () => console.log("Tamam") }],
        { cancelable: false },
      );
    } else {
      this.setState({
        disabled: true,
      });

      this.props
        .login(this.state.code, _uname, this.state.pass)
        .then(() => {
          this.setState({
            disabled: true,
          });
          this.props.navigation.navigate("App");
        })
        .catch(err => {
          console.log(err);
          this.setState({
            disabled: false,
          });
          Alert.alert("Hata", err, [{ text: "Tamam", onPress: () => console.log("Tamam") }], {
            cancelable: false,
          });
        });
    }
  }
}

function mapStateToProps(state) {
  return {
    session: state.session,
    products: state.products,
    cart: state.cart,
    homePage: state.homePage,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    login: (customerID, uname, pass) => dispatch(login(customerID, uname, pass)),
    initialHomePageProducts: refresh => dispatch(initialHomePageProducts(refresh)),
    getCampaigns: sid => dispatch(fetchCampaignsFromApi(sid)),
    getBucket: refresh => dispatch(getBucket(refresh)),
    getHomePage: () => dispatch(getHomePage()),
    setAppState: params => dispatch(setAppState(params)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginFormComponent);
