import React, { Component } from "react";
import {
  Platform,
  Keyboard,
  StatusBar,
  Alert,
  TextInput,
  Image,
  View,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
} from "react-native";
import { Text, ModalBox } from "@components";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import { getEndPoint } from "@common/Utils";

const win = Dimensions.get("window");

export default class ForgotPasswordModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isVisible: false,
      uname: "",
      code: "",
      stage: 0,
      verificationCode: "",
      password: "",
      passwordAgain: "",
      showErrors: false,
      showPassValidation: false,
      keyboardVisible: false,
      keyboardHeight: 0,
    };
  }

  componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener("keyboardDidShow", this._keyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener("keyboardDidHide", this._keyboardDidHide);
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow = e => {
    this.setState({
      keyboardVisible: true,
      keyboardHeight: e.endCoordinates.height,
    });
  };

  _keyboardDidHide = () => {
    this.setState({ keyboardVisible: false });
  };

  componentDidUpdate(prevProps, prevState) {
    if (this.props.isVisible !== prevProps.isVisible)
      this.setState({
        isVisible: this.props.isVisible,
      });
  }

  toggleVisibility = (uname, code) => {
    if (uname != null && uname.includes("test3@")) {
      uname = uname.substring(6);
      this.props.setAppState({ stage: "TEST" });
    }
    this.setState({
      stage: 0,
      code,
      uname,
      showErrors: false,
      showPassValidation: false,
      password: "",
      passwordAgain: "",
      verificationCode: "",
    });
    this.modalBox.toggleVisibility();
  };

  getCodeFromServer = refresh => {
    fetch(
      getEndPoint("FORGOT_PASSWORD_URL") +
        "?func=1&bk=" +
        this.state.code +
        "&kk=" +
        this.state.uname,
    )
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson.Message.includes("İşlem başarılı")) {
          if (refresh) {
            Alert.alert(
              "İşlem Başarılı",
              "Doğrulama kodu cep telefonunuza yeniden gönderildi.",
              [{ text: "Tamam", onPress: () => console.log("Tamam") }],
              { cancelable: false },
            );
          } else {
            Alert.alert(
              "İşlem Başarılı",
              "Doğrulama kodunuz çok kısa bir süre sonra SMS olarak cep telefonunuza gönderilecek.",
              [{ text: "Tamam", onPress: () => this.setState({ stage: 1 }) }],
              { cancelable: false },
            );
          }
        } else {
          Alert.alert(
            "Hata",
            responseJson.Message,
            [{ text: "Tamam", onPress: () => console.log("OK") }],
            {
              cancelable: false,
            },
          );
        }
      });
  };

  validateForm() {
    return (
      this.state.verificationCode !== "" &&
      this.state.password === this.state.passwordAgain &&
      this.state.password.length > 3 &&
      this.state.password.length < 21
    );
  }

  sendPasswordToServer() {
    if (this.validateForm()) {
      fetch(
        getEndPoint("FORGOT_PASSWORD_URL") +
          "?func=2&bk=" +
          this.state.code +
          "&kk=" +
          this.state.uname +
          "&dk=" +
          this.state.verificationCode +
          "&pw=" +
          this.state.password +
          "&pwc=" +
          this.state.passwordAgain,
      )
        .then(response => response.json())
        .then(responseJson => {
          if (responseJson.Message.includes("Şifreniz başarıyla değiştirildi"))
            Alert.alert(
              "İşlem Başarılı",
              "Şifreniz başarıyla değiştirildi.",
              [{ text: "Tamam", onPress: () => this.toggleVisibility() }],
              { cancelable: false },
            );
          else {
            Alert.alert(
              "Hata",
              responseJson.Message,
              [{ text: "Tamam", onPress: () => console.log("OK") }],
              {
                cancelable: false,
              },
            );
          }
        });
    } else {
      Alert.alert("Hata", "Formdaki hataları düzelttikten sonra tekrar deneyiniz.");
      this.setState({ showErrors: true, showPassValidation: true });
    }
  }

  render() {
    return (
      <ModalBox
        ref={c => (this.modalBox = c)}
        statusBarHidden={true}
        animationType="slide"
        scrollable={false}
        presentationStyle="overFullScreen"
      >
        <StatusBar hidden={true} backgroundColor="transparent" />

        <TouchableOpacity
          style={[
            styles.container,
            {
              marginBottom: this.state.keyboardVisible
                ? this.state.keyboardHeight - (Platform.OS === "ios" ? win.height / 16 : 0)
                : 0,
            },
          ]}
          onPress={() => Keyboard.dismiss()}
        >
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "space-between",
              width: win.width * 0.9,
              paddingHorizontal: 16,
              paddingVertical: 12,
            }}
          >
            <Text
              style={{
                fontSize: 11,
                letterSpacing: 1.5,
                color: "black",
                fontFamily: "Poppins-SemiBold",
              }}
            >
              YENİ ŞİFRE OLUŞTURMA
            </Text>
            <TouchableOpacity onPress={() => this.toggleVisibility()}>
              <Image
                source={require("@assets/images/close.png")}
                style={{
                  width: 24,
                  height: 24,
                  tintColor: "#afafaf",
                  resizeMode: "contain",
                }}
              />
            </TouchableOpacity>
          </View>
          <View
            style={{
              width: win.width * 0.9,
              backgroundColor: "#dedede",
              height: 1,
            }}
          />
          <View style={{ width: win.width * 0.9, paddingHorizontal: 16 }}>
            <Text style={{ fontFamily: "Poppins", marginTop: 10, fontSize: 13 }}>
              Aşağıdaki kullanıcı için şifre oluşturulacaktır.
            </Text>
          </View>
          <View
            style={{
              width: win.width * 0.9 - 32,
              backgroundColor: "#e4e4e4",
              flexDirection: "column",
              padding: 10,
              marginVertical: 10,
            }}
          >
            <View style={{ flexDirection: "row" }}>
              <Text
                style={{
                  color: "#9a9a9a",
                  fontFamily: "Poppins",
                  fontSize: 14,
                }}
              >
                Bayi Kodu:{" "}
              </Text>
              <Text style={{ fontFamily: "Poppins", fontSize: 13 }}>{this.state.code}</Text>
            </View>
            <View style={{ flexDirection: "row" }}>
              <Text
                style={{
                  color: "#9a9a9a",
                  fontFamily: "Poppins",
                  fontSize: 14,
                }}
              >
                Kullanıcı Adı:{" "}
              </Text>
              <Text style={{ fontFamily: "Poppins", fontSize: 13 }}>{this.state.uname}</Text>
            </View>
          </View>
          <View style={{ width: win.width * 0.9, paddingHorizontal: 16 }}>
            <Text style={{ fontSize: 11, fontFamily: "Poppins", color: "#848484" }}>
              {this.state.stage === 1
                ? "Not: Cep telefonunuza gönderilen kodu aşağıdaki kutucuğa giriniz."
                : "Not: Cep telefonunuza SMS ile doğrulama kodu gönderilecektir."}
            </Text>
            {this.state.stage === 1 && (
              <View>
                <TextInput
                  value={this.state.verificationCode}
                  onChangeText={verificationCode => this.setState({ verificationCode })}
                  style={{
                    marginTop: 6,
                    borderBottomColor: "#cecece",
                    borderBottomWidth: 1,
                    paddingVertical: 5,
                  }}
                  placeholder="Doğrulama Kodu"
                  autoCapitalize="characters"
                  placeholderTextColor={"gray"}
                  returnKeyType="done"
                />
                <View
                  style={{
                    flexDirection: "row",
                    borderBottomColor: "#cecece",
                    borderBottomWidth: 1,
                    alignItems: "center",
                  }}
                >
                  <TextInput
                    value={this.state.password}
                    onChangeText={password => this.setState({ password, showPassValidation: true })}
                    style={{ flex: 0.9, marginTop: 6, paddingVertical: 6 }}
                    placeholder="Yeni Şifreniz"
                    placeholderTextColor="gray"
                    returnKeyType="done"
                    secureTextEntry={true}
                    maxLength={20}
                  />
                  {this.state.showPassValidation ? (
                    this.state.password.length > 3 ? (
                      <MaterialIcons style={{ flex: 0.1 }} size={18} name="check" color="green" />
                    ) : (
                      <MaterialIcons style={{ flex: 0.1 }} size={18} name="close" color="red" />
                    )
                  ) : null}
                </View>
                <View
                  style={{
                    flexDirection: "row",
                    borderBottomColor: "#cecece",
                    borderBottomWidth: 1,
                    alignItems: "center",
                  }}
                >
                  <TextInput
                    value={this.state.passwordAgain}
                    onChangeText={passwordAgain => this.setState({ passwordAgain })}
                    style={{ flex: 0.9, marginTop: 6, paddingVertical: 5 }}
                    placeholder="Şifre Tekrar"
                    secureTextEntry={true}
                    placeholderTextColor="gray"
                    returnKeyType="done"
                    maxLength={20}
                  />
                  {this.state.showPassValidation ? (
                    this.state.password === this.state.passwordAgain &&
                    this.state.password.length > 3 ? (
                      <MaterialIcons style={{ flex: 0.1 }} size={18} name="check" color="green" />
                    ) : (
                      <MaterialIcons style={{ flex: 0.1 }} size={18} name="close" color="red" />
                    )
                  ) : null}
                  <Image />
                </View>
                {this.state.password.length < 4 && (
                  <Text
                    style={{
                      fontFamily: "Poppins",
                      fontSize: 12,
                      color: "red",
                      marginTop: 8,
                    }}
                  >
                    * Şifreniz en az 4 ve en fazla 20 karakterden oluşmalıdır.
                  </Text>
                )}
                {this.state.password !== this.state.passwordAgain && (
                  <Text
                    style={{
                      fontFamily: "Poppins",
                      fontSize: 12,
                      color: "red",
                      marginTop: 8,
                    }}
                  >
                    * Girdiğiniz şifreler eşleşmiyor.
                  </Text>
                )}
                {this.state.showErrors && this.state.verificationCode === "" && (
                  <Text
                    style={{
                      fontFamily: "Poppins",
                      fontSize: 12,
                      color: "red",
                      marginTop: 8,
                    }}
                  >
                    * Doğrulama kodu girmeniz gerekmektedir.
                  </Text>
                )}
                <Text
                  style={{
                    fontFamily: "Poppins",
                    fontSize: 12,
                    color: "#b0b0b0",
                    marginTop: 8,
                  }}
                >
                  Cep telefonunuza kod gelmediyse yeniden göndermeyi deneyebilirsiniz.
                </Text>
                <TouchableOpacity
                  style={{
                    flexDirection: "row",
                    marginTop: 5,
                    alignItems: "center",
                  }}
                  onPress={() => this.getCodeFromServer(true)}
                >
                  <Text
                    style={{
                      fontFamily: "Poppins",
                      fontSize: 14,
                      color: "gray",
                    }}
                  >
                    Yeniden Kod Gönder
                  </Text>
                  <MaterialIcons size={18} name="refresh" color="gray" />
                </TouchableOpacity>
              </View>
            )}
          </View>
          <View
            style={{
              flexDirection: "row",
              width: win.width * 0.9,
              justifyContent: "center",
              alignItems: "center",
              marginTop: 10,
              borderTopColor: "#dedede",
              borderTopWidth: 1,
            }}
          >
            <TouchableOpacity
              style={{
                flex: 1,
                alignItems: "center",
                padding: 10,
                borderRightWidth: 1,
                borderRightColor: "#dedede",
              }}
              onPress={() => this.toggleVisibility()}
            >
              <Text style={{ fontSize: 16, color: "#5d5d5d" }}>İptal</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{ flex: 1, alignItems: "center", padding: 10 }}
              onPress={() => {
                this.state.stage === 0
                  ? this.getCodeFromServer(false)
                  : this.sendPasswordToServer();
              }}
            >
              <Text style={{ fontSize: 16, color: "#5d5d5d" }}>
                {this.state.stage === 0 ? "Kod Gönder" : "Gönder"}
              </Text>
            </TouchableOpacity>
          </View>
        </TouchableOpacity>
      </ModalBox>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "column",
    backgroundColor: "white",
    width: win.width * 0.9,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.5,
    shadowRadius: 2,
    elevation: 1,
    alignItems: "center",
    marginBottom: 50,
    borderRadius: 6,
  },
});
