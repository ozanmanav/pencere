import React, { Component } from "react";
import {
  Alert,
  TextInput,
  View,
  ScrollView,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  Image,
} from "react-native";
import { Text, ModalBox, CrossPicker, DatePicker, DatePickerDialog } from "@components";
import { CITIES } from "@common/Constants";
import CheckBox from "react-native-check-box";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import { requestBayiOl } from "@common/Utils";
import bugsnag from "@common/bugsnag_config";
import { moderateScale } from "react-native-size-matters";
import { isIphoneX } from "react-native-iphone-x-helper";

const win = Dimensions.get("window");

export default class BayiOlModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fa: "",
      adrs1: "",
      adrs2: "",
      adrs3: "",
      shr: "",
      pk: "",
      vd: "",
      vn: "",
      ka: "",
      a: "",
      s: "",
      tckn: "",
      dt: new Date(1980, 0, 1),
      ct: "",
      st: "",
      em: "",
      emd: "",
      cd: "",
      g: "",
      us: false,
      kvk: false,
    };
  }

  toggleVisibility = () => {
    this.modalBox.toggleVisibility();
  };

  sendForm = () => {
    var form = {};
    form.fa = this.state.fa;
    form.adrs1 = this.state.adrs1;
    form.adrs2 = this.state.adrs2;
    form.adrs3 = this.state.adrs3;
    form.shr = this.state.shr;
    form.pk = this.state.pk;
    form.vd = this.state.vd;
    form.vn = this.state.vn;
    form.ka = this.state.ka;
    form.a = this.state.a;
    form.s = this.state.s;
    form.tckn = this.state.tckn;
    form.dy = this.state.dt.getFullYear();
    form.da = this.state.dt.getMonth() + 1;
    form.dg = this.state.dt.getDate();
    form.ct = this.state.ct;
    form.st = this.state.st;
    form.em = this.state.em;
    form.emd = this.state.emd;
    form.cd = this.state.cd;
    form.g = this.state.g;
    form.us = this.state.us ? "1" : "0";
    form.kvk = this.state.kvk ? "1" : "0";
    console.log("bayiOl form", form);
    requestBayiOl(form)
      .then((resp) => {
        Alert.alert("", resp.Message);
      })
      .catch((err) => {
        bugsnag.notify(err, (report) => {
          report.errorClass = "sendForm bayiOl";
        });
        console.log(err);
        Alert.alert("Hata", "Bir hata oluştu.");
      });
  };

  infoText = [
    "Pencere.com Türkiye’de teknoloji ürünleri ve ilgili tedarik zinciri yönetim hizmetleri alanında lider bir platformdur.",
    "Amacımız internet teknolojilerinin ve B2B portalının sağladığı tüm imkanları ve yenilikleri kullanarak Arena Müşterilerine çeşitlilik, hız, 7x24 erişim, fiyat ve ödeme esneklikleri, deneyim, avantaj ve bilgiyi sunmaktır.",
    "Pencere.com binlerce Teknoloji ürününü en iyi fiyatlarla sağlar. Pencere.com sahip olduğu ürünler ile ilgili kaliteli ve geniş içerik sunar. İlgilendiğiniz ürünlerle ilgili tüm bilgilere Pencere.com altından ulaşabilirsiniz.",
    "Pencere.com'da ürünlerle ilgili kampanyaları ve fırsatları takip edebilir, kampanyalı ürünleri çok avantajlı fiyatlarla sahip olabilirsiniz.",
    "Türkiye çapında 8000’den fazla bilgisayar şirketi Pencere.com'u kullanarak, işlerine, rekabet ortamının gerektirdiği verimliliği kazandırmaktadır.",
    "Pencere.com'a üye olmak artık çok kolay",
    "Pencere.com müşterisi olabilmeniz için bilişim sektöründe faaliyet gösteren ticari vergi mükellefi kişi veya işletme olmalısınız.",
  ];

  force() {
    this.forceUpdate();
  }

  render() {
    return (
      <ModalBox
        ref={(c) => (this.modalBox = c)}
        animationType="slide"
        statusBarHidden={true}
        scrollable={true}
      >
        <ScrollView keyboardShouldPersistTaps="always" keyboardDismissMode="on-drag">
          <TouchableOpacity
            style={{ width: win.width, height: moderateScale(100, 0.5) }}
            onPress={() => this.toggleVisibility()}
          />
          <View
            style={{
              backgroundColor: "white",
              padding: win.width / 12,
              paddingBottom: isIphoneX ? 60 : 0,
            }}
          >
            <Text
              style={{
                marginBottom: moderateScale(15, 0.5),
                fontFamily: "Poppins-Medium",
                fontSize: 19,
              }}
            >
              Türkiye&apos;nin Teknoloji Tedarikçisi
            </Text>
            {this.infoText.map((paragraph, index) => {
              return (
                <Text
                  key={index}
                  style={{
                    fontSize: 13,
                    lineHeight: 16,
                    fontFamily: "Poppins",
                    marginBottom: index < this.infoText.length - 1 ? 10 : 0,
                  }}
                >
                  {paragraph}
                </Text>
              );
            })}
            <Text
              style={{
                fontFamily: "Poppins",
                fontSize: 12,
                color: "#3d3d3d",
                letterSpacing: 1,
                marginTop: 24,
                marginBottom: 10,
              }}
            >
              FİRMA BİLGİLERİ
            </Text>
            <TextInput
              value={this.state.fa}
              onChangeText={(text) => this.setState({ fa: text })}
              placeholder="Firma Adı"
              placeholderTextColor="#afafaf"
              style={{
                marginBottom: moderateScale(12, 0.5),
                fontSize: 12,
                fontFamily: "Poppins",
                paddingHorizontal: moderateScale(8, 0.5),
                height: moderateScale(36, 0.5),
                borderColor: "#dedede",
                borderWidth: 1,
                borderRadius: 3,
              }}
            />
            <TextInput
              value={this.state.adrs1}
              onChangeText={(text) => this.setState({ adrs1: text })}
              placeholder="Adres 1"
              placeholderTextColor="#afafaf"
              style={{
                marginBottom: moderateScale(12, 0.5),
                fontSize: 12,
                fontFamily: "Poppins",
                paddingHorizontal: moderateScale(8, 0.5),
                height: moderateScale(36, 0.5),
                borderColor: "#dedede",
                borderWidth: 1,
                borderRadius: 3,
              }}
            />
            <TextInput
              value={this.state.adrs2}
              onChangeText={(text) => this.setState({ adrs2: text })}
              placeholder="Adres 2"
              placeholderTextColor="#afafaf"
              style={{
                marginBottom: moderateScale(12, 0.5),
                fontSize: 12,
                fontFamily: "Poppins",
                paddingHorizontal: moderateScale(8, 0.5),
                height: moderateScale(36, 0.5),
                borderColor: "#dedede",
                borderWidth: 1,
                borderRadius: 3,
              }}
            />
            <TextInput
              value={this.state.adrs3}
              onChangeText={(text) => this.setState({ adrs3: text })}
              placeholder="Adres 3"
              placeholderTextColor="#afafaf"
              style={{
                marginBottom: moderateScale(12, 0.5),
                fontSize: 12,
                fontFamily: "Poppins",
                paddingHorizontal: moderateScale(8, 0.5),
                height: moderateScale(36, 0.5),
                borderColor: "#dedede",
                borderWidth: 1,
                borderRadius: 3,
              }}
            />
            <TouchableOpacity
              style={{
                marginBottom: moderateScale(12, 0.5),
                flex: 1,
                flexDirection: "row",
                borderColor: "#dedede",
                borderWidth: 1,
                height: moderateScale(36, 0.5),
              }}
              onPress={() => this.cityPicker.toggleVisibility()}
            >
              <View
                style={{
                  flex: 0.9,
                  marginLeft: 8,
                  justifyContent: "center",
                  borderRightColor: "#dedede",
                  borderRightWidth: 1,
                }}
              >
                <Text
                  style={{
                    fontSize: 12,
                    fontFamily: "Poppins",
                    color: "#afafaf",
                  }}
                >
                  {this.cityPicker && this.cityPicker.state.selectedVal
                    ? this.cityPicker.state.selectedVal
                    : "Şehir Seçiniz"}
                </Text>
              </View>
              <View
                style={{
                  flex: 0.1,
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <Image
                  source={require("@assets/images/down-arrow.png")}
                  style={{ width: 12, height: 12, tintColor: "#cecece" }}
                />
              </View>
            </TouchableOpacity>
            <TextInput
              value={this.state.pk}
              onChangeText={(text) => this.setState({ pk: text })}
              placeholder="Posta Kodu"
              placeholderTextColor="#afafaf"
              style={{
                marginBottom: moderateScale(12, 0.5),
                fontSize: 12,
                fontFamily: "Poppins",
                paddingHorizontal: moderateScale(8, 0.5),
                height: moderateScale(36, 0.5),
                borderColor: "#dedede",
                borderWidth: 1,
                borderRadius: 3,
              }}
            />
            <View
              style={{
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "space-between",
              }}
            >
              <TextInput
                value={this.state.vd}
                onChangeText={(text) => this.setState({ vd: text })}
                placeholder="Vergi Dairesi"
                placeholderTextColor="#afafaf"
                style={{
                  flex: 0.5,
                  marginRight: 10,
                  fontSize: 12,
                  fontFamily: "Poppins",
                  paddingHorizontal: moderateScale(8, 0.5),
                  height: moderateScale(36, 0.5),
                  borderColor: "#dedede",
                  borderWidth: 1,
                  borderRadius: 3,
                }}
              />
              <TextInput
                value={this.state.vn}
                onChangeText={(text) => this.setState({ vn: text })}
                placeholder="Vergi No"
                placeholderTextColor="#afafaf"
                style={{
                  flex: 0.5,
                  fontSize: 12,
                  fontFamily: "Poppins",
                  paddingHorizontal: moderateScale(8, 0.5),
                  height: moderateScale(36, 0.5),
                  borderColor: "#dedede",
                  borderWidth: 1,
                  borderRadius: 3,
                }}
              />
            </View>
            <Text
              style={{
                fontFamily: "Poppins",
                fontSize: 12,
                color: "#3d3d3d",
                letterSpacing: 1,
                marginTop: 24,
                marginBottom: 10,
              }}
            >
              İLETİŞİM BİLGİLERİ
            </Text>
            <TextInput
              value={this.state.ka}
              onChangeText={(text) => this.setState({ ka: text })}
              placeholder="Kullanıcı Adı"
              placeholderTextColor="#afafaf"
              style={{
                marginBottom: moderateScale(12, 0.5),
                fontSize: 12,
                fontFamily: "Poppins",
                paddingHorizontal: moderateScale(8, 0.5),
                height: moderateScale(36, 0.5),
                borderColor: "#dedede",
                borderWidth: 1,
                borderRadius: 3,
              }}
            />
            <View
              style={{
                marginBottom: moderateScale(12, 0.5),
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "space-between",
              }}
            >
              <TextInput
                value={this.state.a}
                onChangeText={(text) => this.setState({ a: text })}
                placeholder="Ad"
                placeholderTextColor="#afafaf"
                style={{
                  flex: 0.5,
                  marginRight: 10,
                  fontSize: 12,
                  fontFamily: "Poppins",
                  paddingHorizontal: moderateScale(8, 0.5),
                  height: moderateScale(36, 0.5),
                  borderColor: "#dedede",
                  borderWidth: 1,
                  borderRadius: 3,
                }}
              />
              <TextInput
                value={this.state.s}
                onChangeText={(text) => this.setState({ s: text })}
                placeholder="Soyad"
                placeholderTextColor="#afafaf"
                style={{
                  flex: 0.5,
                  fontSize: 12,
                  fontFamily: "Poppins",
                  paddingHorizontal: moderateScale(8, 0.5),
                  height: moderateScale(36, 0.5),
                  borderColor: "#dedede",
                  borderWidth: 1,
                  borderRadius: 3,
                }}
              />
            </View>
            <TextInput
              value={this.state.tckn}
              onChangeText={(text) => this.setState({ tckn: text })}
              placeholder="TC Kimlik No"
              placeholderTextColor="#afafaf"
              style={{
                marginBottom: moderateScale(12, 0.5),
                fontSize: 12,
                fontFamily: "Poppins",
                paddingHorizontal: moderateScale(8, 0.5),
                height: moderateScale(36, 0.5),
                borderColor: "#dedede",
                borderWidth: 1,
                borderRadius: 3,
              }}
            />
            <View style={styles.dateRow}>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Image source={require("@assets/images/calendar.png")} style={styles.icon} />
                <Text
                  style={{
                    marginRight: 10,
                    fontSize: 14,
                    color: "gray",
                    fontFamily: "Poppins",
                  }}
                >
                  Doğum Tarihiniz
                </Text>
              </View>
              <DatePicker
                ref={(c) => (this.datePicker1 = c)}
                date={this.state.dt}
                id="1"
                onPressDatePicker={this.onPressDatePicker}
                openDatePickerIOS={(date, id, minimumDate, maximumDate) =>
                  this.datePickerDialog.open(date, id, minimumDate, maximumDate)
                }
                maximumDate={new Date()}
                minimumDate={new Date(1900, 0, 1)}
              />
            </View>
            <TextInput
              value={this.state.ct}
              onChangeText={(text) => this.setState({ ct: text })}
              placeholder="Cep Telefonu"
              placeholderTextColor="#afafaf"
              style={{
                marginBottom: moderateScale(12, 0.5),
                fontSize: 12,
                fontFamily: "Poppins",
                paddingHorizontal: moderateScale(8, 0.5),
                height: moderateScale(36, 0.5),
                borderColor: "#dedede",
                borderWidth: 1,
                borderRadius: 3,
              }}
            />
            <TextInput
              keyboardType="phone-pad"
              value={this.state.st}
              onChangeText={(text) => this.setState({ st: text })}
              placeholder="Telefon"
              placeholderTextColor="#afafaf"
              style={{
                marginBottom: moderateScale(12, 0.5),
                fontSize: 12,
                fontFamily: "Poppins",
                paddingHorizontal: moderateScale(8, 0.5),
                height: moderateScale(36, 0.5),
                borderColor: "#dedede",
                borderWidth: 1,
                borderRadius: 3,
              }}
            />
            <TextInput
              keyboardType="email-address"
              value={this.state.em}
              onChangeText={(text) => this.setState({ em: text })}
              placeholder="E-posta"
              placeholderTextColor="#afafaf"
              style={{
                marginBottom: moderateScale(12, 0.5),
                fontSize: 12,
                fontFamily: "Poppins",
                paddingHorizontal: moderateScale(8, 0.5),
                height: moderateScale(36, 0.5),
                borderColor: "#dedede",
                borderWidth: 1,
                borderRadius: 3,
              }}
            />
            <TextInput
              value={this.state.emd}
              onChangeText={(text) => this.setState({ emd: text })}
              placeholder="E-posta Tekrar"
              placeholderTextColor="#afafaf"
              style={{
                marginBottom: moderateScale(12, 0.5),
                fontSize: 12,
                fontFamily: "Poppins",
                paddingHorizontal: moderateScale(8, 0.5),
                height: moderateScale(36, 0.5),
                borderColor: "#dedede",
                borderWidth: 1,
                borderRadius: 3,
              }}
            />
            <TextInput
              value={this.state.cd}
              onChangeText={(text) => this.setState({ cd: text })}
              placeholder="Çalıştığınız Departman"
              placeholderTextColor="#afafaf"
              style={{
                marginBottom: moderateScale(12, 0.5),
                fontSize: 12,
                fontFamily: "Poppins",
                paddingHorizontal: moderateScale(8, 0.5),
                height: moderateScale(36, 0.5),
                borderColor: "#dedede",
                borderWidth: 1,
                borderRadius: 3,
              }}
            />
            <TextInput
              value={this.state.g}
              onChangeText={(text) => this.setState({ g: text })}
              placeholder="Göreviniz"
              placeholderTextColor="#afafaf"
              style={{
                marginBottom: moderateScale(12, 0.5),
                fontSize: 12,
                fontFamily: "Poppins",
                paddingHorizontal: moderateScale(8, 0.5),
                height: moderateScale(36, 0.5),
                borderColor: "#dedede",
                borderWidth: 1,
                borderRadius: 3,
              }}
            />
            <View
              style={{
                marginBottom: moderateScale(12, 0.5),
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <CheckBox
                style={{
                  height: moderateScale(20, 0.5),
                  marginRight: moderateScale(4, 0.5),
                }}
                onClick={() => this.setState({ us: !this.state.us })}
                isChecked={this.state.us}
                checkedImage={<MaterialIcons size={18} name="check-box" color="green" />}
                unCheckedImage={<MaterialIcons size={18} name="check-box-outline-blank" />}
              />
              <Text
                style={{
                  fontFamily: "Poppins",
                  fontSize: 12,
                  lineHeight: 14,
                  flex: 1,
                }}
              >
                Üyelik sözleşmesi&apos;ni okudum ve kabul ediyorum.
              </Text>
            </View>
            <View
              style={{
                marginBottom: moderateScale(12, 0.5),
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <CheckBox
                style={{
                  height: moderateScale(20, 0.5),
                  marginRight: moderateScale(4, 0.5),
                }}
                onClick={() => this.setState({ kvk: !this.state.kvk })}
                isChecked={this.state.kvk}
                checkedImage={<MaterialIcons size={18} name="check-box" color="green" />}
                unCheckedImage={<MaterialIcons size={18} name="check-box-outline-blank" />}
              />
              <Text
                style={{
                  fontFamily: "Poppins",
                  fontSize: 12,
                  lineHeight: 14,
                  flex: 1,
                }}
              >
                Tarafımla pazarlama ve tanıtım amaçlı iletişime geçilmesine izin veriyorum.
              </Text>
            </View>
            <TouchableOpacity
              onPress={this.sendForm}
              style={{
                backgroundColor: "#54a932",
                height: moderateScale(36, 0.5),
                flex: 1,
                justifyContent: "center",
                alignItems: "center",
                borderRadius: 3,
              }}
            >
              <Text
                style={{
                  color: "white",
                  fontFamily: "Poppins-SemiBold",
                  fontSize: 16,
                }}
              >
                Gönder
              </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
        <DatePickerDialog
          ref={(c) => (this.datePickerDialog = c)}
          onChangeDate={(date) => this.setState({ dt: date })}
          setDate={this.setDate}
          collapseDatesView={() => this.setState({ isCollapsed: true })}
        />
        <CrossPicker
          onChangeValue={(val) => this.setState({ shr: val })}
          ref={(c) => (this.cityPicker = c)}
          items={CITIES}
          forceUpdate={() => this.force()}
        />
      </ModalBox>
    );
  }
}

const styles = StyleSheet.create({
  dateRow: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    flex: 1,
    marginBottom: 10,
  },
  icon: {
    width: 20,
    height: 20,
    marginRight: 10,
    tintColor: "gray",
  },
});
