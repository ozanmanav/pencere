import React, { Component } from "react";
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  Dimensions,
  Animated,
  findNodeHandle,
  Keyboard,
  StatusBar,
  LayoutAnimation,
} from "react-native";

import { Text } from "@components";

import { LoginFormComponent, ForgotPasswordModal, BayiOlModal } from "@features/giris/components";

import { connect } from "react-redux";
import { UPDATE_SPRINGY } from "@common/LayoutAnimations";
import { setLiveChatVisibility } from "@redux/actions";

const win = Dimensions.get("window");

class LoginScreen extends Component {
  topPanelTranslate = new Animated.Value(0);
  botPanelTranslate = new Animated.Value(0);

  state = {
    spinValue: new Animated.Value(0),
    isSpinning: false,
    keyboardVisible: false,
    keyboardHeight: 0,
  };
  static navigationOptions = {
    tabBarIcon: ({ tintColor }) => (
      <Image source={require("@assets/images/user.png")} style={[styles.icon, { tintColor }]} />
    ),
    header: null,
  };

  componentDidMount() {
    this.props.setLiveChatVisibility(false);
    this.keyboardDidShowListener = Keyboard.addListener("keyboardDidShow", this._keyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener("keyboardDidHide", this._keyboardDidHide);
    this.loginAnimation();
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow = (e) => {
    this.setState({
      keyboardVisible: true,
      keyboardHeight: e.endCoordinates.height,
    });
    LayoutAnimation.configureNext(UPDATE_SPRINGY);
  };

  _keyboardDidHide = () => {
    this.setState({ keyboardVisible: false });
    LayoutAnimation.configureNext(UPDATE_SPRINGY);
  };

  startAnimation = () => {
    this.setState({
      isSpinning: true,
    });
  };
  stopAnimation = () => {
    this.setState({
      isSpinning: false,
    });
  };

  imageLoaded() {
    this.setState({ viewRef: findNodeHandle(this.backgroundImage) });
  }

  loginAnimation() {
    Animated.timing(this.topPanelTranslate, {
      duration: 1000,
      useNativeDriver: true,
      toValue: win.height * 0.35,
    }).start();
    Animated.timing(this.botPanelTranslate, {
      duration: 1000,
      useNativeDriver: true,
      toValue: -win.height * 0.55,
    }).start();
  }

  toggleForgotModal = (uname, code) => {
    this.forgotModal.toggleVisibility(uname, code);
  };

  toggleForgotCodeDialog = (uname, code) => {
    this.forgotCodeDialog.toggleVisibility(uname, code);
  };

  toggleBayiOlModal = () => {
    this.bayiOlModal.toggleVisibility();
  };

  render() {
    return (
      <View style={{ height: win.height }}>
        <StatusBar hidden={true} backgroundColor="transparent" />
        <Image
          ref={(img) => {
            this.backgroundImage = img;
          }}
          onLoadEnd={this.imageLoaded.bind(this)}
          source={require("@assets/images/stock-login.jpg")}
          style={[
            { width: win.width, height: win.height / 2, resizeMode: "cover" },
            styles.absolute,
          ]}
        />
        <Animated.View
          style={[
            {
              position: "absolute",
              top: -win.height * 0.35,
              left: 0,
              width: win.width,
              height: win.height * 0.35,
              paddingHorizontal: win.width / 16,
              alignItems: "center",
              justifyContent: "center",
            },
            { transform: [{ translateY: this.topPanelTranslate }] },
          ]}
        >
          <View style={{ flexDirection: "column", alignItems: "center" }}>
            <Image
              source={require("@assets/images/logo_text_white.png")}
              style={{
                width: win.width * 0.3 + 60,
                height: win.width * 0.3 * 0.2 + 12,
                resizeMode: "contain",
              }}
            />
            <Text
              style={{
                color: "white",
                fontSize: 26,
                textAlign: "center",
                marginTop: win.height / 32,
                fontFamily: "Poppins-Medium",
              }}
            >
              Türkiye&apos;nin Teknoloji Tedarikçisi
            </Text>
          </View>
        </Animated.View>
        <Animated.View
          style={[
            {
              position: "absolute",
              bottom:
                this.state.keyboardVisible &&
                !(this.forgotModal && this.forgotModal.state.isVisible) &&
                !(this.BayiOlModal && this.BayiOlModal.state.isVisible)
                  ? -win.height * 0.55 + 120
                  : -win.height * 0.55,
              left: 0,
              width: win.width,
              height: win.height * 0.65,
              backgroundColor: "white",
            },
            { transform: [{ translateY: this.botPanelTranslate }] },
          ]}
        >
          <TouchableOpacity style={{ flex: 1 }}>
            <LoginFormComponent
              toggleBayiOlModal={this.toggleBayiOlModal}
              navigation={this.props.navigation}
              toggleForgotModal={this.toggleForgotModal}
            />
          </TouchableOpacity>
        </Animated.View>
        <ForgotPasswordModal ref={(c) => (this.forgotModal = c)} />
        <BayiOlModal ref={(c) => (this.bayiOlModal = c)} />
      </View>
    );
  }
}
function mapStateToProps(state) {
  return {
    session: state.session,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    setLiveChatVisibility: (isVisible) => dispatch(setLiveChatVisibility(isVisible)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);

const styles = StyleSheet.create({
  absolute: {
    position: "absolute",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
});
