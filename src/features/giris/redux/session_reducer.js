const INITAL_STATE = {
  info: [],
  mobileConfig: {},
  isQuerying: false,
  mobileConfigError: false,
};
import {
  LOGIN_START,
  LOGIN_SUCCESS,
  LOGOUT_SUCCESS,
  LOGIN_FAILURE,
  MOBILE_CONFIG_SUCCESS,
  MOBILE_CONFIG_FAILURE,
} from "@redux/actions/types";

export default function sessionReducer(state = INITAL_STATE, action) {
  switch (action.type) {
    case LOGIN_START:
      return {
        ...state,
        isQuerying: true,
      };
    case LOGIN_SUCCESS:
      return {
        ...state,
        info: action.info,
        isQuerying: false,
      };
    case LOGIN_FAILURE:
      return {
        ...state,
        info: [],
        isQuerying: false,
      };
    case LOGOUT_SUCCESS:
      return {
        ...state,
        info: [],
      };
    case MOBILE_CONFIG_SUCCESS:
      return {
        ...state,
        mobileConfig: action.data,
      };
    case MOBILE_CONFIG_FAILURE:
      return {
        ...state,
        mobileConfigError: true,
      };
    default:
      return state;
  }
}
