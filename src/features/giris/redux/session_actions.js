import {
  LOGIN_START,
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
  LOGOUT_SUCCESS,
  MOBILE_CONFIG_SUCCESS,
  MOBILE_CONFIG_FAILURE,
} from "@redux/actions/types";
import { MOBILE_CONFIG_URL } from "@common/Constants";
import bugsnag from "@common/bugsnag_config";
import { getEndPoint } from "@common/Utils";
import analytics from "@react-native-firebase/analytics";
import * as Keychain from "react-native-keychain";
import axios from "axios";

export function login(id, uname, pass) {
  if (uname != null && uname.startsWith("test3@")) {
    uname = uname.substring(6);
  }
  return (dispatch, getState) => {
    return new Promise(function(resolve, reject) {
      dispatch({ type: LOGIN_START });
      axios
        .get(
          getEndPoint("LOGIN_URL") +
            "?bk=" +
            id +
            "&kk=" +
            encodeURIComponent(uname) +
            "&pw=" +
            encodeURIComponent(pass),
          {
            timeout: 10000,
          },
        )
        .then(responseJson => {
          if (responseJson !== undefined) {
            if (responseJson.data.MesajTipi === "S") {
              resolve("SUCCESS");
              let sessionInfo = {
                ...responseJson.data,
                date: Date.now(),
              };
              var mSessionInfo = sessionInfo;
              if (getState().appState.stage === "TEST") {
                mSessionInfo.KullaniciKodu = "test3@" + mSessionInfo.KullaniciKodu;
              }
              Keychain.setGenericPassword(
                "session",
                JSON.stringify({
                  BayiKodu: id,
                  KullaniciKodu: mSessionInfo.KullaniciKodu,
                  Sifre: pass,
                }),
              );
              analytics().setUserId(mSessionInfo.BayiKodu + "_" + mSessionInfo.KullaniciKodu);
              analytics().setUserProperties({
                BayiKodu: mSessionInfo.BayiKodu,
                KullaniciKodu: mSessionInfo.KullaniciKodu,
                Email: mSessionInfo.Email,
              });
              dispatch(loginSuccess(sessionInfo));
            } else {
              dispatch(loginFailure());
              reject(responseJson.data.Mesaj);
              /*                     Toast.show({
                                                  text: 'Bilgiler hatalı!',
                                                  position: 'bottom',
                                              }); */
            }
          } else {
            dispatch(loginFailure());
            reject("Bir hata oluştu. Daha sonra tekrar deneyiniz.");
          }
        })
        .catch(error => {
          bugsnag.notify(error, report => {
            report.errorClass = "login";
          });
          console.error(error);
        });
    });
  };
}

export function getSessionFromStorage() {
  return dispatch => {
    return new Promise((resolve, reject) => {
      Keychain.getGenericPassword()
        .then(credentials => {
          console.log("get session", credentials);
          if (credentials !== false && credentials != null && credentials.password !== "") {
            var info = JSON.parse(credentials.password);
            dispatch(loginSuccess(info));
            resolve(info);
          } else reject();
        })
        .catch(err => {
          console.log("getSessionFromStorage catch", err);
          reject();
          bugsnag.notify(err, report => {
            report.errorClass = "getSessionFromStorage";
          });
        });
    });
  };
}

export function logout() {
  return dispatch => {
    return new Promise(function(resolve, reject) {
      Keychain.resetGenericPassword()
        .then(() => {
          resolve("LOGOUT");
          dispatch({
            type: LOGOUT_SUCCESS,
          });
        })
        .catch(() => {
          reject();
        });
    });
  };
}

export function getMobileConfig() {
  return dispatch => {
    return new Promise((resolve, reject) => {
      fetch(MOBILE_CONFIG_URL, {
        headers: {
          "Cache-Control": "no-cache",
        },
      })
        .then(res => res.json())
        .then(responseJson => {
          console.log("config json: ", responseJson);
          resolve(responseJson);
          dispatch({
            type: MOBILE_CONFIG_SUCCESS,
            data: responseJson,
          });
        })
        .catch(error => {
          reject(error);
          bugsnag.notify(error, report => {
            report.errorClass = "getMobileConfig";
          });
          dispatch({
            type: MOBILE_CONFIG_FAILURE,
            error,
          });
        });
    });
  };
}

function loginSuccess(info) {
  return {
    type: LOGIN_SUCCESS,
    info,
  };
}

function loginFailure() {
  return {
    type: LOGIN_FAILURE,
  };
}
