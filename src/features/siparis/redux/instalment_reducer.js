const INITAL_STATE = {
  instalmentInfo: {},
  selectedIndex: null,
};
import { CHOOSE_INSTALMENT } from "@redux/actions/types";

export default function instalmentReducer(state = INITAL_STATE, action) {
  switch (action.type) {
    case CHOOSE_INSTALMENT:
      return {
        ...state,
        instalmentInfo: action.instalment,
        selectedIndex: action.index,
      };
    default:
      return state;
  }
}
