const INITAL_STATE = {
  paynetSession: {},
  token: {},
  authError: false,
  tokenError: false,
  progress: false,
};

import {
  AUTH_SUCCESS,
  AUTH_ERROR,
  CREATE_TOKEN_SUCCESS,
  CREATE_TOKEN_FAILURE,
  TOGGLE_PAYMENT_PROGRESS,
} from "@redux/actions/types";

export default function paymentReducer(state = INITAL_STATE, action) {
  //console.log("payment action", action);
  switch (action.type) {
    case TOGGLE_PAYMENT_PROGRESS:
      return {
        ...state,
        progress: action.progress,
      };
    case AUTH_SUCCESS:
      return {
        ...state,
        paynetSession: action.data,
        authError: false,
      };
    case AUTH_ERROR:
      return {
        ...state,
        paynetSession: {},
        authError: true,
        progress: false,
      };
    case CREATE_TOKEN_SUCCESS:
      return {
        ...state,
        token: action.data,
        tokenError: false,
      };
    case CREATE_TOKEN_FAILURE:
      return {
        ...state,
        token: {},
        tokenError: true,
      };
    default:
      return state;
  }
}
