import { CHOOSE_INSTALMENT } from "@redux/actions/types";

export function chooseInstalment(instalment, index) {
  return (dispatch) => {
    dispatch({ type: CHOOSE_INSTALMENT, instalment, index });
  };
}
