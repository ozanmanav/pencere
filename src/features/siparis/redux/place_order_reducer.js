import {
  START_ORDER,
  START_ORDER_SUCCESS,
  START_ORDER_FAILURE,
  CONFIRM_ORDER_SUCCESS,
  CONFIRM_ORDER_FAILURE,
  TOGGLE_PROGRESS_ORDER,
} from "@redux/actions/types";

const initialState = {
  response: {},
  orderResult: {},
  isFetching: false,
  error: false,
  errMsg: "",
};

export default function favoritesReducer(state = initialState, action) {
  switch (action.type) {
    case TOGGLE_PROGRESS_ORDER:
      return {
        ...state,
        isFetching: action.isFetching,
      };
    case START_ORDER:
      return {
        ...state,
        error: false,
        isFetching: true,
      };
    case START_ORDER_SUCCESS:
      return {
        ...state,
        isFetching: false,
        response: action.data,
      };
    case START_ORDER_FAILURE:
      return {
        ...state,
        response: {},
        error: true,
        errMsg: action.error,
      };
    case CONFIRM_ORDER_SUCCESS:
      return {
        ...state,
        orderResult: action.data,
      };
    case CONFIRM_ORDER_FAILURE:
      return {
        ...state,
        error: true,
        errMsg: action.error,
      };
    default:
      return state;
  }
}
