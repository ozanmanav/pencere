import {
  START_ORDER,
  START_ORDER_SUCCESS,
  START_ORDER_FAILURE,
  CONFIRM_ORDER_SUCCESS,
  CONFIRM_ORDER_FAILURE,
} from "@redux/actions/types";
import bugsnag from "@common/bugsnag_config";
import { getEndPoint } from "@common/Utils";
import NavigationService from "@app/NavigationService";
import { Alert } from "react-native";

export function startOrder() {
  return (dispatch, getState) => {
    return new Promise(function (resolve, reject) {
      dispatch({
        type: START_ORDER,
      });
      console.log(
        "ksa1",
        getEndPoint("ORDER_URL") + "?Si=" + getState().session.info.SessionID + "&c=basla",
      );
      fetch(getEndPoint("ORDER_URL") + "?Si=" + getState().session.info.SessionID + "&c=basla")
        .then((response) => response.json())
        .then((responseJson) => {
          dispatch({
            type: START_ORDER_SUCCESS,
            data: responseJson,
          });
          resolve(responseJson);
        })
        .catch((error) => {
          bugsnag.notify(error, (report) => {
            report.errorClass = "startOrder";
          });
          console.log(error);
          reject(error);
          dispatch({
            type: START_ORDER_FAILURE,
            error,
          });
        });
    });
  };
}

export function taksitSec(ts) {
  return (dispatch, getState) => {
    dispatch({
      type: START_ORDER,
    });
    fetch(
      getEndPoint("ORDER_URL") +
        "?si=" +
        getState().session.info.SessionID +
        "&c=taksit_secenegi_guncelle&ts=" +
        ts,
    )
      .then((res) => res.json())
      .then((responseJson) => {
        console.log("taksitSec", responseJson);
        dispatch({
          type: START_ORDER_SUCCESS,
          data: responseJson,
        });
      })
      .catch((err) => {
        bugsnag.notify(err, (report) => {
          report.errorClass = "taksitSec";
        });
        console.log(err);
        dispatch({
          type: START_ORDER_FAILURE,
          err,
        });
      });
  };
}
export function changeAddress(at, ak) {
  return (dispatch, getState) => {
    return new Promise(function (resolve, reject) {
      dispatch({
        type: START_ORDER,
      });

      fetch(
        getEndPoint("ORDER_URL") +
          "?Si=" +
          getState().session.info.SessionID +
          "&c=baslikdegisti" +
          "&at=" +
          at +
          "&ak=" +
          ak,
      )
        .then((response) => response.json())
        .then((responseJson) => {
          dispatch({
            type: START_ORDER_SUCCESS,
            data: responseJson,
          });
          resolve(START_ORDER_SUCCESS);
        })
        .catch((error) => {
          bugsnag.notify(error, (report) => {
            report.errorClass = "changeAddress";
          });
          console.log(error);
          reject(START_ORDER_FAILURE);
          dispatch({
            type: START_ORDER_FAILURE,
            error,
          });
        });
    });
  };
}
export function changeDeliveryType(sst) {
  return (dispatch, getState) => {
    return new Promise(function (resolve, reject) {
      dispatch({
        type: START_ORDER,
      });
      var preparedUrl =
        getEndPoint("ORDER_URL") +
        "?Si=" +
        getState().session.info.SessionID +
        "&c=basla" +
        "&sst=" +
        sst;
      fetch(preparedUrl)
        .then((response) => response.json())
        .then((responseJson) => {
          dispatch({
            type: START_ORDER_SUCCESS,
            data: responseJson,
          });
          resolve(START_ORDER_SUCCESS);
        })
        .catch((error) => {
          bugsnag.notify(error, (report) => {
            report.errorClass = "changeDeliveryType";
          });
          console.log(error);
          reject(START_ORDER_FAILURE);
          dispatch({
            type: START_ORDER_FAILURE,
            error,
          });
        });
    });
  };
}

export function changePaymentMethod(ob, pb) {
  return (dispatch, getState) => {
    return new Promise(function (resolve, reject) {
      dispatch({
        type: START_ORDER,
      });
      var preparedUrl =
        getEndPoint("ORDER_URL") +
        "?Si=" +
        getState().session.info.SessionID +
        "&c=baslikdegisti" +
        "&ob=" +
        ob;
      if (typeof pb !== undefined || pb !== "") preparedUrl = preparedUrl + "&pb=" + pb;
      fetch(preparedUrl)
        .then((response) => response.json())
        .then((responseJson) => {
          dispatch({
            type: START_ORDER_SUCCESS,
            data: responseJson,
          });
          resolve(START_ORDER_SUCCESS);
        })
        .catch((error) => {
          bugsnag.notify(error, (report) => {
            report.errorClass = "changePaymentMethod";
          });
          console.log("changePaymentMethod", error);
          reject(START_ORDER_FAILURE);
          dispatch({
            type: START_ORDER_FAILURE,
            error,
          });
        });
    });
  };
}

export function changePaymentCondition(uy, kk, ko) {
  return (dispatch, getState) => {
    dispatch({
      type: START_ORDER,
    });
    return new Promise(function (resolve, reject) {
      var preparedUrl =
        getEndPoint("ORDER_URL") +
        "?Si=" +
        getState().session.info.SessionID +
        "&c=kalem_odemekosulu" +
        "&uy=" +
        uy +
        "&kk=" +
        kk +
        "&ko=" +
        ko;
      fetch(preparedUrl)
        .then((res) => res.json())
        .then((responseJson) => {
          dispatch({
            type: START_ORDER_SUCCESS,
            data: responseJson,
          });
          resolve(responseJson);
        })
        .catch((error) => {
          bugsnag.notify(error, (report) => {
            report.errorClass = "changePaymentCondition";
          });
          console.log(error);
          reject();
          dispatch({
            type: START_ORDER_FAILURE,
            error,
          });
        });
    });
  };
}

export function changeShipment(uy, kf) {
  return (dispatch, getState) => {
    return new Promise(function (resolve, reject) {
      dispatch({
        type: START_ORDER,
      });
      fetch(
        getEndPoint("ORDER_URL") +
          "?Si=" +
          getState().session.info.SessionID +
          "&c=kargo_degisti" +
          "&uy=" +
          uy +
          "&kf=" +
          kf.replace(/^0+/, ""),
      )
        .then((response) => response.json())
        .then((responseJson) => {
          console.log("shipment responseJson", responseJson);
          dispatch({
            type: START_ORDER_SUCCESS,
            data: responseJson,
          });
          resolve(START_ORDER_SUCCESS);
        })
        .catch((error) => {
          bugsnag.notify(error, (report) => {
            report.errorClass = "changeShipment";
          });
          console.log(error);
          reject(START_ORDER_FAILURE);
          dispatch({
            type: START_ORDER_FAILURE,
            error,
          });
        });
    });
  };
}

export function createNewAddress(a1, a2, a3, a4, il, k, aad) {
  return (dispatch, getState) => {
    return new Promise(function (resolve, reject) {
      dispatch({
        type: START_ORDER,
      });
      fetch(
        getEndPoint("ORDER_URL") +
          "?Si=" +
          getState().session.info.SessionID +
          "&c=baslikdegisti&ak=ya" +
          "&a1=" +
          a1 +
          "&a2=" +
          a2 +
          "&a3=" +
          a3 +
          "&a4=" +
          a4 +
          "&il=" +
          il +
          "&k=" +
          k +
          "&aad=" +
          aad,
      )
        .then((response) => response.json())
        .then((responseJson) => {
          dispatch({
            type: START_ORDER_SUCCESS,
            data: responseJson,
          });
          resolve(START_ORDER_SUCCESS);
        })
        .catch((error) => {
          bugsnag.notify(error, (report) => {
            report.errorClass = "createNewAddress";
          });
          console.log(error);
          reject(START_ORDER_FAILURE);
          dispatch({
            type: START_ORDER_FAILURE,
            error,
          });
        });
    });
  };
}

export function confirmOrder() {
  return (dispatch, getState) => {
    return new Promise(function (resolve, reject) {
      fetch(getEndPoint("ORDER_RESULT_URL") + "?si=" + getState().session.info.SessionID)
        .then((res) => res.json())
        .then((responseJson) => {
          console.log("orderResult json", responseJson);
          dispatch({ type: CONFIRM_ORDER_SUCCESS, data: responseJson });
          resolve(responseJson);
        })
        .catch((error) => {
          bugsnag.notify(error, (report) => {
            report.errorClass = "confirmOrder";
          });
          console.log(error);
          dispatch({ type: CONFIRM_ORDER_FAILURE, error });
          reject(error);
        });
    });
  };
}

export function useCoupon(kuko) {
  return (dispatch, getState) => {
    const uy = getState().placeOrder.response?.SiparisVerisi?.IlBaslikUyOut[0].UretimYeri;
    dispatch({
      type: START_ORDER,
    });
    return new Promise(function (resolve) {
      fetch(
        getEndPoint("ORDER_URL") +
          "?si=" +
          getState().session.info.SessionID +
          "&c=kupon" +
          "&uy=" +
          uy +
          "&kuko=" +
          kuko,
      )
        .then((res) => res.json())
        .then((responseJson) => {
          dispatch({
            type: START_ORDER_SUCCESS,
            data: responseJson,
          });
          resolve(START_ORDER_SUCCESS);
        });
    });
  };
}

export function startFromPriceReq(ftn) {
  return (dispatch, getState) => {
    return new Promise(function (resolve, reject) {
      dispatch({
        type: START_ORDER,
      });
      console.log(
        "ksa1",
        getEndPoint("ORDER_URL") +
          "?Si=" +
          getState().session.info.SessionID +
          "&c=fiyattalebindenbasla&ftn=" +
          ftn,
      );
      fetch(
        getEndPoint("ORDER_URL") +
          "?Si=" +
          getState().session.info.SessionID +
          "&c=fiyattalebindenbasla&ftn=" +
          ftn,
      )
        .then((response) => response.json())
        .then((responseJson) => {
          dispatch({
            type: START_ORDER_SUCCESS,
            data: responseJson,
          });
          resolve(responseJson);
          if (responseJson.MesajTipi === "S")
            NavigationService.navigate(null, "CheckoutStack", {
              latestScreen: "PriceRequestsScreen",
            });
          else {
            Alert.alert("Başarısız", responseJson.Mesaj);
          }
        })
        .catch((error) => {
          bugsnag.notify(error, (report) => {
            report.errorClass = "startOrder";
          });
          console.log(error);
          reject(error);
          dispatch({
            type: START_ORDER_FAILURE,
            error,
          });
        });
    });
  };
}
