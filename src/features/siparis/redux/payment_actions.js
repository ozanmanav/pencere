import {
  AUTH_SUCCESS,
  AUTH_ERROR,
  CREATE_TOKEN_SUCCESS,
  CREATE_TOKEN_FAILURE,
  TOGGLE_PAYMENT_PROGRESS,
} from "@redux/actions/types";
import bugsnag from "@common/bugsnag_config";
import { getEndPoint } from "@common/Utils";

export function saveCardUpdate(save_card, card_desc) {
  return (dispatch, getState) => {
    dispatch({ type: TOGGLE_PAYMENT_PROGRESS, progress: true });
    return new Promise(function (resolve, reject) {
      const session_id = getState().payment.paynetSession.session_id;
      const gsm = getState().session.info.Telefon;
      var postBody = {
        session_id,
        save_card,
        user_gsm_no: gsm,
        card_desc,
      };
      fetch(getEndPoint("SAVE_CARD_UPDATE_URL", "paynet"), {
        method: "post",
        headers: new Headers({
          Authorization: "Basic " + getState().placeOrder.response.PaynetData.PublicKey,
          "Content-Type": "application/json",
        }),
        body: JSON.stringify(postBody),
      })
        .then((res) => res.json())
        .then((responseJson) => {
          dispatch({ type: TOGGLE_PAYMENT_PROGRESS, progress: false });
          resolve(responseJson);
        })
        .catch((err) => {
          console.log("error", err);
          dispatch({ type: TOGGLE_PAYMENT_PROGRESS, progress: false });
          reject(err);
        });
    });
  };
}
export function authenticatePaynet(amount, agentID, card_owner_id, user_unique_id) {
  return (dispatch, getState) => {
    return new Promise(function (resolve, reject) {
      var postBody = {
        amount: Math.ceil(amount.toFixed(2) * 100),
        agent_id: getState().placeOrder.response.PaynetData.AgentId,
        pos_type: 4,
        add_commission: false,
        domain: "pencere.com",
        save_card: false,
      };
      if (card_owner_id != null) {
        postBody.card_owner_id = card_owner_id;
      }
      if (user_unique_id != null) {
        postBody.user_unique_id = user_unique_id;
      }
      console.log("ksa4", getEndPoint("PAYNET_AUTH_URL", "paynet"));
      fetch(getEndPoint("PAYNET_AUTH_URL", "paynet"), {
        // Paynet base url + /v1/paynetj/auth
        method: "post",
        headers: new Headers({
          Authorization: "Basic " + getState().placeOrder.response.PaynetData.PublicKey,
          "Content-Type": "application/json",
        }),
        body: JSON.stringify(postBody),
      })
        .then((res) => res.json())
        .then((responseJson) => {
          if (responseJson.type && responseJson.type === "api_error") {
            reject({ type: "API_ERROR" });
            console.log("api error", responseJson.message || "");
            dispatch({
              type: AUTH_ERROR,
            });
          } else {
            resolve({ type: "AUTH_SUCCESS", data: responseJson });
            dispatch({
              type: AUTH_SUCCESS,
              data: responseJson,
            });
          }
        })
        .catch((err) => {
          bugsnag.notify(err, (report) => {
            report.errorClass = "authenticatePaynet";
          });
          reject({ type: "AUTH_ERROR" });
          console.log("auth error", err);
          dispatch({
            type: AUTH_ERROR,
          });
        });
    });
  };
}

export function createToken(
  session_id,
  card_holder,
  pan,
  month,
  year,
  cvc,
  instalment_key,
  do_3ds,
  ip,
) {
  return (dispatch, getState) => {
    return new Promise(function (resolve, reject) {
      var body = "";
      body = JSON.stringify({
        session_id,
        card_holder,
        pan,
        month,
        year,
        cvc,
        do_3ds,
        ip,
      });
      console.log("ksa5", getEndPoint("PAYNET_CREATE_TOKEN_URL", "paynet"), body);
      fetch(getEndPoint("PAYNET_CREATE_TOKEN_URL", "paynet"), {
        method: "post",
        headers: new Headers({
          Authorization: "Basic " + getState().placeOrder.response.PaynetData.PublicKey,
          "Content-Type": "application/json",
        }),
        body,
      })
        .then((res) => res.json())
        .then((responseJson) => {
          if (responseJson.type && responseJson.type === "api_error") {
            dispatch({ type: CREATE_TOKEN_FAILURE, message: responseJson.message });
            reject({ type: "API_ERROR", error: responseJson.message });
          } else {
            dispatch({ type: CREATE_TOKEN_SUCCESS, data: responseJson });
            resolve({ type: CREATE_TOKEN_SUCCESS, data: responseJson });
          }
        })
        .catch((err) => {
          bugsnag.notify(err, (report) => {
            report.errorClass = "createToken";
          });
          console.error(err);
          reject({ type: CREATE_TOKEN_FAILURE, error: err });
        });
    });
  };
}

export function createCardToken(card_hash, card_owner_id, do_3ds, instalment_key) {
  return (dispatch, getState) => {
    return new Promise(function (resolve, reject) {
      console.log(
        "ksa5",
        getEndPoint("PAYNET_CARD_TOKEN_URL", "paynet"),
        getState().payment.paynetSession.session_id,
        card_hash,
        card_owner_id,
        instalment_key,
        do_3ds,
      );
      fetch(getEndPoint("PAYNET_CARD_TOKEN_URL", "paynet"), {
        method: "post",
        headers: new Headers({
          Authorization: "Basic " + getState().placeOrder.response.PaynetData.PublicKey,
          "Content-Type": "application/json",
        }),
        body: JSON.stringify({
          session_id: getState().payment.paynetSession.session_id,
          card_hash,
          card_owner_id,
          instalment_key,
          do_3ds,
        }),
      })
        .then((res) => res.json())
        .then((responseJson) => {
          if (responseJson.type && responseJson.type === "api_error") {
            dispatch({ type: CREATE_TOKEN_FAILURE, message: responseJson.message });
            reject({ type: "API_ERROR", error: responseJson.message });
          } else {
            dispatch({ type: CREATE_TOKEN_SUCCESS, data: responseJson });
            resolve({ type: CREATE_TOKEN_SUCCESS, data: responseJson });
          }
        })
        .catch((err) => {
          bugsnag.notify(err, (report) => {
            report.errorClass = "createCardToken";
          });
          console.error(err);
          reject({ type: CREATE_TOKEN_FAILURE, error: err });
        });
    });
  };
}
