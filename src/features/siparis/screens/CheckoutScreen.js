import React, { Component } from "react";
import {
  View,
  Dimensions,
  FlatList,
  Animated,
  Image,
  Platform,
  LayoutAnimation,
  TouchableOpacity,
  StatusBar,
  StyleSheet,
  Keyboard,
  Alert,
} from "react-native";
import {
  AnimatedHeader,
  Text,
  RadioButton,
  WebViewModal,
  BottomPicker,
  ListModal,
  PencereIndicator,
} from "@components";
import {
  BankAccountsModal,
  ChargeResultModal,
  CargoCompanyItem,
  OrderProductBundle,
  OrderProductItem,
  OrderConfirmationDialog,
  CreditCardForm,
  SavedCard,
  StickyFooter,
  CouponPanel,
} from "@features/siparis/components";
import colors from "@common/styles/colors";
import { connect } from "react-redux";
import {
  changeShipment,
  changePaymentMethod,
  chooseInstalment,
  confirmOrder,
  authenticatePaynet,
  createToken,
  createCardToken,
  fetchAccountsFromApi,
  changePaymentCondition,
  setSafeViewBackgroundColor,
  saveCardUpdate,
  useCoupon,
  taksitSec,
} from "@redux/actions";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import { UPDATE_SPRINGY } from "@common/LayoutAnimations";
import { verticalScale, scale, moderateScale, ScaledSheet } from "react-native-size-matters";
import Interactable from "react-native-interactable";
import { fetchBankAccounts, getEndPoint, numberToLocaleString } from "@common/Utils";
import { saveOrder, trackCheckout, getRatios } from "../CheckoutUtils";
const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);
const NAVBAR_HEIGHT = AnimatedHeader.height;
const STATUS_BAR_HEIGHT = 0;
const win = Dimensions.get("window");
const SPACE_HORIZONTAL = win.width / 25;

const months = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"];

const years = [];
var year = new Date().getFullYear();
for (var i = 0; i < 10; i++) {
  years.push((year + i).toString());
}

class CheckoutScreen extends Component {
  constructor(props) {
    super(props);
    this.availables = [];
    this.scrollY = 0;
    var scrollAnim = new Animated.Value(0);
    this.state = {
      seciliKuponIndex: -1,
      couponCode: "",
      showCreditCardForm: true,
      seciliKayitliKartIndex: 0,
      accounts: [],
      selectedKalem: null,
      selectedVadeSecenekleri: [],
      paymentViewHeight: 0,
      showUygunsuzlar: true,
      focsuedY: 0,
      inProgress: false,
      bddk: false,
      kss: false,
      obf: false,
      shadowOpacity: 0,
      listCollapsed: false,
      keyboardVisible: false,
      keyboardHeight: 0,
      creditCardForm: {
        ccn: "",
        ccName: "",
        expMonthIndex: 0,
        expYear: years[0],
        selectedBankIndex: -1,
        selectedBankData: {},
        cardType: 0,
        saveCard: false,
        cvc: "",
        isCardNameDefault: true,
        newCardDesc: "",
        allowOtherUsers: false,
      },
      spb: "",
      scrollY: 0,
      scrollAnim,
      clampedScroll: Animated.diffClamp(
        Animated.add(
          Animated.add(scrollAnim, NAVBAR_HEIGHT).interpolate({
            inputRange: [0, 1],
            outputRange: [0, 1],
            extrapolateLeft: "clamp",
          }),
          -NAVBAR_HEIGHT,
        ),
        0,
        NAVBAR_HEIGHT - STATUS_BAR_HEIGHT,
      ),
    };
  }

  clearCCForm = persist => {
    this.setState({
      ...this.state,
      ...persist,
      creditCardForm: {
        ccn: "",
        ccName: "",
        expMonthIndex: 0,
        expYear: years[0],
        selectedBankIndex: -1,
        selectedBankData: {},
        cardType: 0,
        saveCard: false,
        cvc: "",
        isCardNameDefault: true,
        newCardDesc: "",
        allowOtherUsers: false,
      },
    });
  };

  componentDidMount() {
    if (
      this.props.session.info.SadeceUlusalPB &&
      this.props.placeOrder.response?.SiparisVerisi?.BaslikOut?.SiparisPb !== "TRY"
    ) {
      this.props.changePaymentMethod(
        this.props.placeOrder.response?.SiparisVerisi?.BaslikOut.SiparisOdemeBicimi,
        "TRY",
      );
    }
    this.props.chooseInstalment(null, null);
    this.props.getAccounts();
    fetchBankAccounts()
      .then(accounts => {
        var array = Object.keys(accounts).map(function(key) {
          return { city: key, banks: accounts[key] };
        });
        this.setState({ accounts: array });
      })
      .catch(err => {
        console.log(err);
      });
    this.setState({
      spb: this.props.placeOrder.response?.SiparisVerisi?.IlBaslikUyOut[0].SiparisPb,
    });
    this.keyboardDidShowListener = Keyboard.addListener("keyboardWillShow", this._keyboardDidShow);
    const kargoFirmalari = this.props.placeOrder.response?.IlKargoFirmalari;
    const baslikUyOut = this.props.placeOrder.response?.SiparisVerisi?.IlBaslikUyOut[0];

    kargoFirmalari.forEach(element => {
      if (
        element.UretimYeri === baslikUyOut.UretimYeri &&
        element.MinimumDesi < baslikUyOut.ToplamDesi &&
        element.MaximumDesi > baslikUyOut.ToplamDesi
      ) {
        var length = this.availables.push(element);
        console.log("availables pushed element", element, this.availables, length);
      }
    });

    if (
      this.props.placeOrder.response?.SiparisVerisi?.BaslikOut?.SiparisOdemeBicimi === "A" &&
      this.props.placeOrder.response.SiparisVerisi?.BaslikOut?.SiparisPb !==
        this.props.session.info?.BayiPb
    ) {
      this.props.changePaymentMethod("A", this.props.session.info?.BayiPb || "USD");
    }
    const KayitliKartlar = this.props.placeOrder?.response?.KartSaklamaBilgileri?.KayitliKartlar;
    if (KayitliKartlar != null && KayitliKartlar.length > 0) {
      const selectedSavedCard = KayitliKartlar[this.state.seciliKayitliKartIndex]?.KartDetayi;
      const PaynetData = this.props?.placeOrder?.response?.PaynetData;
      if (selectedSavedCard != null) {
        const cardType = selectedSavedCard.card_type === "cc" ? 0 : 1;
        const banks = PaynetData?.RatioData?.Data[cardType]?.banks;
        const bankIndex = banks.findIndex(item => item.bank_id === selectedSavedCard.card_bank_id);
        this.setState({
          ...this.state,
          creditCardForm: {
            ...this.state.creditCardForm,
            selectedBankData: banks[bankIndex],
            selectedBankIndex: bankIndex,
          },
        });
        console.log(
          "selectedBankData",
          this.state.creditCardForm.selectedBankData,
          cardType,
          selectedSavedCard.card_bank_id,
        );
      }
    }
    this.setState({
      listCollapsed: !(
        this.props.placeOrder.response.SiparisVerisi.BaslikOut.SiparisOdemeBicimi === "A"
      ),
      showCreditCardForm: !(
        this.props.placeOrder.response?.KartSaklamaBilgileri?.KayitliKartlar?.length > 0
      ),
    });
  }

  componentDidUpdate(prevProps, prevState) {
    const baslikUyOut = this.props.placeOrder.response?.SiparisVerisi?.IlBaslikUyOut[0];
    if (
      JSON.stringify(prevProps.placeOrder?.response?.IlKargoFirmalari) !==
      JSON.stringify(this.props.placeOrder?.response?.IlKargoFirmalari)
    ) {
      const kargoFirmalari = this.props.placeOrder.response?.IlKargoFirmalari;
      console.log(
        "availables changed",
        JSON.stringify(prevProps.placeOrder?.response?.IlKargoFirmalari),
        JSON.stringify(this.props.placeOrder?.response?.IlKargoFirmalari),
      );
      this.availables = [];
      kargoFirmalari.forEach(element => {
        if (
          element.UretimYeri === baslikUyOut.UretimYeri &&
          element.MinimumDesi < baslikUyOut.ToplamDesi &&
          element.MaximumDesi > baslikUyOut.ToplamDesi
        ) {
          this.availables.push(element);
        }
      });
    }

    if (
      this.props.placeOrder.response?.SiparisVerisi?.BaslikOut?.SiparisOdemeBicimi !==
      prevProps.placeOrder.response?.SiparisVerisi?.BaslikOut?.SiparisOdemeBicimi
    ) {
      console.log("paymentMethod");
      LayoutAnimation.configureNext(UPDATE_SPRINGY);
      if (this.props.placeOrder.response?.SiparisVerisi?.BaslikOut?.SiparisOdemeBicimi === "A") {
        if (this.state.listCollapsed) {
          this.setState({ listCollapsed: false });
        }
      } else if (!this.state.listCollapsed) {
        this.setState({ listCollapsed: true });
      }
      if (this.props.placeOrder.response?.SiparisVerisi?.BaslikOut?.SiparisOdemeBicimi === "K") {
        this.setState({
          showCreditCardForm: !(
            this.props.placeOrder.response?.KartSaklamaBilgileri?.KayitliKartlar?.length > 0
          ),
        });
      }
      if (this.props.placeOrder.response?.MesajTipi === "E") {
        Alert.alert("Hata", this.props.placeOrder.response?.Mesaj, [
          {
            text: "Tamam",
            onPress: () => {
              this.props.navigation.navigate("ShoppingCartScreen");
            },
          },
        ]);
      }
    }

    if (
      this.props.placeOrder.response?.SiparisVerisi?.BaslikOut?.SiparisOdemeBicimi === "K" &&
      ((prevState.showCreditCardForm !== this.state.showCreditCardForm &&
        !this.state.showCreditCardForm) ||
        prevState.seciliKayitliKartIndex !== this.state.seciliKayitliKartIndex)
    ) {
      if (this?.bottomInstalmentPicker?.state?.isOpen) this.bottomInstalmentPicker.openPicker();
      const KayitliKartlar = this.props.placeOrder?.response?.KartSaklamaBilgileri?.KayitliKartlar;
      if (KayitliKartlar != null && KayitliKartlar.length > 0) {
        const selectedSavedCard = KayitliKartlar[this.state.seciliKayitliKartIndex]?.KartDetayi;
        const PaynetData = this.props?.placeOrder?.response?.PaynetData;
        if (selectedSavedCard != null) {
          const cardType = selectedSavedCard.card_type === "cc" ? 0 : 1;
          const banks = PaynetData?.RatioData?.Data[cardType]?.banks;
          const bankIndex = banks.findIndex(
            item => item.bank_id === selectedSavedCard.card_bank_id,
          );
          this.setState({
            ...this.state,
            creditCardForm: {
              ...this.state.creditCardForm,
              selectedBankData: banks[bankIndex],
              selectedBankIndex: bankIndex,
            },
          });
          console.log(
            "selectedBankData",
            this.state.creditCardForm.selectedBankData,
            cardType,
            selectedSavedCard.card_bank_id,
          );
        }
      }
    }
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
  }

  _keyboardDidShow = e => {
    this.setState({
      keyboardVisible: true,
      keyboardHeight: e.endCoordinates.height,
    });
    console.log("scroll to", this.state.scrollY, e.endCoordinates.height);
  };

  renderFooter = selectedOb => {
    var prices = Array.from(this.props.placeOrder.response.SiparisVerisi.IlBaslikToplam);
    if (
      this.props.instalment?.instalmentInfo?.payyed_commision != null &&
      this.props.instalment?.instalmentInfo?.payyed_commision !== 0
    ) {
      if (
        this.props.instalment.instalmentInfo?.campaign_commision_support != null &&
        this.props.instalment.instalmentInfo?.campaign_commision_support < 0
      ) {
        prices.push({
          ToplamAciklama: "Komisyon Tutarı",
          ToplamTutar: this.props.instalment.instalmentInfo?.default_commision_amount,
          ToplamPb: this.props.placeOrder.response?.SiparisVerisi?.GenelToplam?.ToplamPb,
        });
        prices.push({
          ToplamAciklama: "Kampanya Desteği",
          ToplamTutar: this.props.instalment.instalmentInfo?.campaign_commision_support,
          ToplamPb: this.props.placeOrder.response?.SiparisVerisi?.GenelToplam?.ToplamPb,
        });
      }
      prices.push({
        ToplamAciklama: "Ödenecek Komisyon",
        ToplamTutar: this.props.instalment.instalmentInfo?.payyed_commision,
        ToplamPb: this.props.placeOrder.response?.SiparisVerisi?.GenelToplam?.ToplamPb,
      });
      prices.push({
        ToplamAciklama: "Ödenecek Toplam Tutar",
        ToplamTutar: this.props.instalment.instalmentInfo?.charged_amount,
        ToplamPb: this.props.placeOrder.response?.SiparisVerisi?.GenelToplam?.ToplamPb,
      });
    }

    if (this.props.placeOrder.response?.SiparisVerisi?.BaslikOut?.SiparisOdemeBicimi !== "K") {
      prices = Array.from(this.props.placeOrder.response.SiparisVerisi.IlBaslikToplam);
    }

    return (
      <View
        style={{
          flexDirection: "column",
          backgroundColor: "white",
          borderRadius: 4,
          overflow: "hidden",
          width: win.width,
          alignSelf: "center",
          marginBottom: verticalScale(60),
          paddingBottom: verticalScale(10),
          marginTop: verticalScale(10),
        }}
      >
        <View
          style={{
            flexDirection: "row",
            borderBottomColor: "#ddd",
            borderBottomWidth: 1,
            height: verticalScale(40),
            paddingLeft: scale(10),
            marginHorizontal: SPACE_HORIZONTAL * 2 - moderateScale(10, 0.5),
            alignItems: "center",
          }}
        >
          <Text fontScale={0.5} style={{ fontFamily: "Poppins", fontSize: 15, color: "#d55749" }}>
            Sipariş Özeti
          </Text>
        </View>
        <View
          style={{
            flexDirection: "column",
            flex: 1,
            paddingTop: verticalScale(10),
          }}
        >
          {prices.map((item, index) => {
            return (
              <View
                key={index.toString()}
                style={{
                  flexDirection: "row",
                  flex: 1,
                  height: verticalScale(32),
                  alignItems: "center",
                  marginHorizontal: SPACE_HORIZONTAL * 2,
                  justifyContent: "space-between",
                }}
              >
                <Text
                  fontScale={0.5}
                  style={{
                    fontFamily: "Poppins",
                    fontSize: 13,
                    color: "#999999",
                  }}
                >
                  {item.ToplamAciklama}
                </Text>
                <Text
                  fontScale={0.5}
                  style={{
                    fontFamily: "Poppins",
                    fontSize: 13,
                    color: "#333333",
                  }}
                >
                  {numberToLocaleString((item.ToplamTipi === "I" ? -1 : 1) * item.ToplamTutar) +
                    " " +
                    item.ToplamPb}
                </Text>
              </View>
            );
          })}
        </View>
        <View
          style={{
            flexDirection: "row",
            flex: 1,
            alignItems: "center",
            marginHorizontal: SPACE_HORIZONTAL * 2 - moderateScale(10, 0.5),
            marginVertical: moderateScale(8, 0.5),
            paddingTop: moderateScale(8, 0.5),
            borderTopColor: "#ddd",
            borderTopWidth: 1,
          }}
        >
          <TouchableOpacity
            style={{
              flexDirection: "row",
              justifyContent: "flex-start",
              alignItems: "center",
              padding: moderateScale(5, 0.5),
              marginTop: moderateScale(5, 0.5),
            }}
            onPress={() => this.setState({ obf: !this.state.obf })}
          >
            {this.state.obf ? (
              <FontAwesome name="check-square-o" color="#3498db" size={moderateScale(20, 0.4)} />
            ) : (
              <FontAwesome name="square-o" color="gray" size={moderateScale(20, 0.4)} />
            )}
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() =>
              this.wvModal.toggleModal(
                { uri: "https://www.pencere.com/onBilgilendirmeFormu.htm" },
                "Ön Bilgilendirme Formu",
              )
            }
          >
            <Text fontScale={0.5} style={{ marginTop: 3 }}>
              <Text
                fontScale={0.5}
                style={{
                  fontFamily: "Poppins",
                  fontSize: 13,
                  color: "#439ef3",
                  marginLeft: scale(5),
                }}
              >
                Ön bilgilendirme formunu
              </Text>
              <Text
                fontScale={0.5}
                style={{
                  fontFamily: "Poppins",
                  fontSize: 13,
                  color: "#666",
                  marginLeft: scale(5),
                }}
              >
                {" "}
                kabul ediyorum.
              </Text>
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  renderShipmentOptions = (baslikOut, baslikUyOut) => {
    return (
      (baslikOut.SevkSekli === "01" || baslikOut.SevkSekli === "03") && (
        <View style={{ marginTop: 10 }}>
          <Text
            fontScale={0.5}
            style={{
              fontFamily: "Poppins",
              color: "#d6421e",
              fontSize: 16,
              marginBottom: 10,
            }}
          >
            Kargo Seçimi
          </Text>
          <View style={{ flexDirection: "row" }}>
            {this.availables.length > 0 ? (
              this.availables.map((item, index) => {
                return (
                  <CargoCompanyItem
                    key={i.toString()}
                    style={{
                      flexDirection: "row",
                      alignItems: "center",
                      borderColor: "#e3e3e3",
                      borderWidth: 1,
                      borderRadius: 4,
                      height: 50,
                      padding: 5,
                      justifyContent: "center",
                      flex: 1,
                      marginRight: index < this.availables.length - 1 ? 10 : 0,
                    }}
                    onPress={_item => {
                      this.props.changeShipment(_item.UretimYeri, _item.NakliyeciID);
                    }}
                    item={item}
                    Nakliyeci={baslikUyOut.Nakliyeci}
                  />
                );
              })
            ) : (
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  flex: 1,
                  padding: scale(5),
                }}
              >
                <Image
                  source={require("@assets/images/info_white.png")}
                  style={{
                    width: moderateScale(14, 0.5),
                    height: moderateScale(14, 0.5),
                    tintColor: "#A88F80",
                    resizeMode: "contain",
                    marginRight: scale(8),
                    marginLeft: scale(3),
                  }}
                />
                <Text fontScale={0.5} style={{ flexWrap: "wrap", flex: 1 }}>
                  <Text
                    fontScale={0.5}
                    style={{
                      fontFamily: "Poppins",
                      color: "#707070",
                      fontSize: 11,
                    }}
                  >
                    {"Uygun kargo seçeneği bulunamadı."}
                  </Text>
                </Text>
              </View>
            )}
          </View>
          {/* <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', height: 50 }}>
        <CheckBox
          style={{ height: 20, tintColor: 'gray' }}
          isChecked={baslikUyOut.TamTeslimat}
          checkedImage={<FontAwesome size={18} name="check-square" color='#3498db' />}
          unCheckedImage={<FontAwesome size={18} name="square" color='#c4c4c4' />}
        />
        <Text fontScale={.5} style={{ fontFamily: 'Poppins', fontSize: 14, marginLeft: 10 }}>Tam Teslimat İstiyorum</Text>
      </TouchableOpacity> */}
        </View>
      )
    );
  };

  shouldShowParaBirimiPicker = selectedOb =>
    selectedOb?.IlParaBirimleri &&
    Object.keys(selectedOb.IlParaBirimleri).length > 1 &&
    !this.props.session.info.SadeceUlusalPB;

  renderHeader = selectedOb => {
    const baslikOut = this.props.placeOrder.response?.SiparisVerisi?.BaslikOut;
    const baslikUyOut = this.props.placeOrder.response?.SiparisVerisi?.IlBaslikUyOut[0];
    const adresler = this.props.placeOrder.response?.IlSubeler;
    const odemeBicimleri = this.props.placeOrder.response.IdOdemeBicimleri;
    var KayitliKartlar = this.props.placeOrder?.response?.KartSaklamaBilgileri?.KayitliKartlar;

    return (
      <View style={{ flexDirection: "column" }}>
        {this.props.placeOrder.response.SiparisVerisi?.IlUygunsuzlar.length > 0 &&
          this.state.showUygunsuzlar && (
            <Interactable.View
              style={{
                flexDirection: "row",
                alignItems: "center",
                flex: 1,
                backgroundColor: "#ffedc2",
                padding: scale(5),
                marginHorizontal: SPACE_HORIZONTAL,
                borderRadius: 4,
                borderWidth: 1,
                borderColor: "#dec69c",
              }}
              horizontalOnly={true}
              snapPoints={[
                { x: win.width - SPACE_HORIZONTAL * 2 },
                { x: 0, damping: 0.65, tension: 500, haptics: true },
                { x: -win.width - SPACE_HORIZONTAL * 2 },
              ]}
              onSnap={({ nativeEvent }) => {
                nativeEvent.index !== 1 && this.setState({ showUygunsuzlar: false });
                LayoutAnimation.configureNext(UPDATE_SPRINGY);
              }}
            >
              <Image
                source={require("@assets/images/info_white.png")}
                style={{
                  width: moderateScale(14, 0.5),
                  height: moderateScale(14, 0.5),
                  tintColor: "#A88F80",
                  resizeMode: "contain",
                  marginRight: scale(8),
                  marginLeft: scale(3),
                }}
              />
              <TouchableOpacity
                style={{ flex: 1 }}
                onPress={() => this.listModal.toggleVisibility()}
              >
                <Text
                  fontScale={0.5}
                  style={{
                    flexWrap: "wrap",
                    flex: 1,
                    marginTop: moderateScale(4, 0.3),
                  }}
                >
                  <Text
                    fontScale={0.5}
                    style={{
                      fontFamily: "Poppins",
                      color: "#707070",
                      fontSize: 11,
                      lineHeight: 13,
                    }}
                  >
                    {"Sepetinizden temin edilemeyen " +
                      this.props.placeOrder.response.SiparisVerisi?.IlUygunsuzlar.length +
                      " adet ürün bulunmaktadır. Ürünleri görmek için"}
                  </Text>
                  <Text
                    fontScale={0.5}
                    style={{
                      fontFamily: "Poppins-SemiBold",
                      color: "#439ef3",
                      fontSize: 11,
                      lineHeight: 13,
                    }}
                  >
                    {" "}
                    tıklayın.
                  </Text>
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  this.setState({ showUygunsuzlar: false });
                  LayoutAnimation.configureNext(UPDATE_SPRINGY);
                }}
              >
                <Image
                  source={require("@assets/images/close.png")}
                  style={{
                    width: moderateScale(18, 0.5),
                    height: moderateScale(18, 0.5),
                    tintColor: "#adadad",
                  }}
                />
              </TouchableOpacity>
            </Interactable.View>
          )}
        <View
          ref={c => (this.firstBox = c)}
          style={{
            flexDirection: "column",
            flex: 1,
            marginHorizontal: SPACE_HORIZONTAL,
            backgroundColor: "white",
            padding: SPACE_HORIZONTAL,
            paddingTop: SPACE_HORIZONTAL - 5,
            borderRadius: 4,
            marginTop: 10,
          }}
        >
          <Text fontScale={0.5} style={{ fontFamily: "Poppins", color: "#d6421e", fontSize: 16 }}>
            Teslimat Adresi
          </Text>
          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate({
                key: "AddressOptionsScreen",
                routeName: "AddressOptionsScreen",
                params: {
                  items: adresler,
                  selectedAddressCode: baslikOut.MaliTeslimAlanAdresKodu,
                },
              })
            }
            style={{
              flex: 1,
              flexDirection: "row",
              borderRadius: 4,
              marginTop: 10,
              borderColor: "#c9c9c9",
              borderWidth: 1,
              padding: 10,
              paddingRight: 0,
            }}
          >
            <View style={{ flexDirection: "column", flex: 1 }}>
              <Text
                fontScale={0.5}
                numberOfLines={1}
                ellipsizeMode="tail"
                style={{ fontFamily: "Poppins-Medium", fontSize: 12 }}
              >
                {baslikOut?.SiparisVeren}
              </Text>
              <Text
                fontScale={0.5}
                numberOfLines={1}
                ellipsizeMode="tail"
                style={{ fontFamily: "Poppins-Light", fontSize: 12 }}
              >
                {baslikOut?.MaliTeslimAlanA1}
              </Text>
              {(baslikUyOut.SevkiyatSekli === "01" || baslikUyOut.SevkiyatSekli === "03") && (
                <Text
                  fontScale={0.5}
                  numberOfLines={1}
                  ellipsizeMode="tail"
                  style={{ fontFamily: "Poppins-Light", fontSize: 12 }}
                >
                  {baslikOut?.MaliTeslimAlanA2 +
                    " " +
                    baslikOut?.MaliTeslimAlanA3 +
                    " " +
                    baslikOut?.MaliTeslimAlanA4 +
                    " " +
                    baslikOut?.MaliTeslimAlanIl}
                </Text>
              )}
            </View>
            <View
              style={{
                width: moderateScale(30, 0.5),
                alignItems: "center",
                justifyContent: "center",
                marginLeft: moderateScale(15, 0.5),
                borderLeftWidth: 1,
                borderLeftColor: "#c9c9c9",
              }}
            >
              <Image
                source={require("@assets/images/down-arrow.png")}
                style={{
                  width: moderateScale(14, 0.5),
                  height: moderateScale(14, 0.5),
                  tintColor: "#cecece",
                }}
              />
            </View>
          </TouchableOpacity>

          {/* <View style={{ flexDirection: 'row', marginTop: 10, alignItems: 'center', flex: 1, justifyContent: 'space-between', }}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Image source={require('@assets/images/calendar.png')} style={{ width: 18, height: 18, tintColor: 'black' }} />
              <Text fontScale={.5} style={{ marginLeft: 8, fontFamily: 'Poppins', fontSize: 14 }}>Teslimat Zamanı</Text>
            </View>
            <Text fontScale={.5} style={{ fontFamily: 'Poppins', fontSize: 14 }}>{sevkSaatleri[0] ?.TeslimatTarihi + ' / ' + (sevkSaatleri[0] ?.TeslimatSaati.length > 5 ? (sevkSaatleri[0] ?.TeslimatSaati.substring(0, 5)) : sevkSaatleri[0] ?.TeslimatSaati)}</Text>
          </View> */}
          {this.renderShipmentOptions(baslikOut, baslikUyOut)}
        </View>
        {this.props.placeOrder?.response?.MobileKuponlar?.MobileKuponList?.length > 0 && (
          <CouponPanel
            couponCode={this.state.couponCode}
            setCouponCode={couponCode => this.setState({ couponCode })}
            MobileKuponlar={this.props.placeOrder?.response?.MobileKuponlar}
            useCoupon={this.props.useCoupon}
            toggleKuponList={() => this.kuponListModal.toggleVisibility()}
          />
        )}
        <View
          style={{
            flexDirection: "column",
            flex: 1,
            marginHorizontal: SPACE_HORIZONTAL,
            backgroundColor: "white",
            paddingTop: SPACE_HORIZONTAL - 5,
            borderRadius: 4,
            marginTop: 10,
          }}
        >
          <View style={{ paddingHorizontal: SPACE_HORIZONTAL }}>
            <Text
              fontScale={0.5}
              style={{
                fontFamily: "Poppins",
                color: "#d6421e",
                fontSize: 16,
                marginBottom: 10,
              }}
            >
              Ödeme Yöntemi
            </Text>
          </View>
          <View style={{ flexDirection: "column" }}>
            <View
              style={{
                flexDirection: "row",
                flex: 1,
                height: moderateScale(50, 0.5),
                alignItems: "center",
              }}
            >
              {odemeBicimleri.map((item, index) => {
                if (!(baslikOut.SevkSekli === "08" && item.OdemeBicimi1 === "N"))
                  // Mecidiyeköy sevk şekli ise kapıda ödeme seçeneğini gösterme
                  return (
                    <View
                      key={index.toString()}
                      style={{
                        borderWidth: baslikOut.SiparisOdemeBicimi === item.OdemeBicimi1 ? 1 : 0,
                        borderBottomWidth:
                          baslikOut.SiparisOdemeBicimi !== item.OdemeBicimi1 &&
                          this.state.paymentViewHeight > 10
                            ? 1
                            : 0,
                        height: moderateScale(50, 0.5),
                        flex:
                          item.OdemeBicimiText.length < 7 || item.OdemeBicimiText.includes("Paynet")
                            ? 0.7
                            : 1,
                        alignItems: "center",
                        justifyContent: "center",
                        borderColor: "#d7d7d7",
                        borderTopLeftRadius: 4,
                        borderTopRightRadius: 4,
                      }}
                    >
                      <TouchableOpacity
                        onPress={() => {
                          this.props.changePaymentMethod(item.OdemeBicimi1, baslikUyOut.SiparisPb);
                        }}
                        style={{
                          paddingVertical: moderateScale(5, 0.5),
                          height: moderateScale(50, 0.5),
                          alignItems: "center",
                          justifyContent: "center",
                        }}
                      >
                        <Text
                          fontScale={0.5}
                          style={{
                            fontFamily: "Poppins-Medium",
                            color:
                              baslikOut.SiparisOdemeBicimi !== item.OdemeBicimi1
                                ? "#7d7d7d"
                                : "#d6421e",
                            fontSize: 12,
                            textAlign: "center",
                          }}
                        >
                          {item.OdemeBicimiText.includes("Paynet")
                            ? "Paynet"
                            : item.OdemeBicimiText}
                        </Text>
                      </TouchableOpacity>
                    </View>
                  );
              })}
            </View>
            <View
              onLayout={({
                nativeEvent: {
                  layout: { height },
                },
              }) => {
                console.log("payment height", height);
                this.setState({ paymentViewHeight: height });
              }}
              style={{
                flexDirection: "column",
                borderColor: "#d7d7d7",
                borderWidth: this.state.paymentViewHeight > 10 ? 1 : 0,
                borderTopWidth: 0,
              }}
            >
              {this.shouldShowParaBirimiPicker(selectedOb) && (
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    paddingHorizontal: SPACE_HORIZONTAL,
                    marginVertical: scale(10),
                  }}
                >
                  <Text
                    fontScale={0.5}
                    style={{
                      fontFamily: "Poppins",
                      color: "#7d7d7d",
                      fontSize: 12,
                      marginRight: scale(10),
                    }}
                  >
                    Para birimi seçiniz:
                  </Text>
                  {
                    <TouchableOpacity
                      onPress={() =>
                        this.bottomCurrencyPicker.openPicker(null, null, Platform.OS === "android")
                      }
                      style={{
                        flexDirection: "row",
                        height: 32,
                        width: 90,
                        borderColor: "#e3e3e3",
                        borderWidth: 1,
                        borderRadius: 3,
                      }}
                    >
                      <View
                        style={{
                          flex: 3,
                          height: 32,
                          alignItems: "center",
                          justifyContent: "center",
                          borderRightColor: "#e3e3e3",
                          borderRightWidth: 1,
                        }}
                      >
                        <Text
                          fontScale={0.5}
                          style={{
                            fontFamily: "Poppins",
                            fontSize: 14,
                            color: "#afafaf",
                          }}
                        >
                          {baslikOut.SiparisPb}
                        </Text>
                      </View>
                      <View
                        style={{
                          flex: 2,
                          height: 32,
                          alignItems: "center",
                          justifyContent: "center",
                        }}
                      >
                        <Image
                          source={require("@assets/images/down-arrow.png")}
                          style={{
                            width: 12,
                            height: 12,
                            tintColor: "#afafaf",
                          }}
                        />
                      </View>
                    </TouchableOpacity>
                  }
                </View>
              )}
              {this.props.placeOrder.response?.SiparisVerisi?.IlBaslikUyOut[0]
                .SiparisOdemeBicimi === "H" && (
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    padding: moderateScale(4, 0.5),
                    paddingHorizontal: SPACE_HORIZONTAL,
                    paddingBottom: moderateScale(10, 0.5),
                    paddingTop: this.shouldShowParaBirimiPicker(selectedOb)
                      ? 0
                      : moderateScale(4, 0.5),
                  }}
                >
                  <Image
                    source={require("@assets/images/info_white.png")}
                    style={{
                      width: moderateScale(16, 0.5),
                      height: moderateScale(16, 0.5),
                      tintColor: "#A88F80",
                      resizeMode: "contain",
                      marginRight: scale(8),
                      marginLeft: scale(3),
                    }}
                  />
                  <Text style={{ flex: 1 }}>
                    <Text
                      fontScale={0.5}
                      style={{
                        fontFamily: "Poppins",
                        fontSize: 12,
                        color: "gray",
                        flex: 1,
                      }}
                    >
                      Havale yapabileceğiniz banka hesaplarını görmek için{" "}
                    </Text>
                    <Text
                      onPress={() => this.accountsModal.toggleVisibility()}
                      fontScale={0.5}
                      style={{
                        fontFamily: "Poppins",
                        fontSize: 12,
                        color: colors.price_color,
                        flex: 1,
                        textDecorationLine: "underline",
                      }}
                    >
                      lütfen tıklayın.
                    </Text>
                  </Text>
                </View>
              )}
              {this.props.placeOrder.response?.SiparisVerisi?.IlBaslikUyOut[0]
                .SiparisOdemeBicimi === "A" &&
                typeof this.props.accounts.info.KrediLimiti !== "undefined" && (
                  <View
                    style={{
                      flexDirection: "row",
                      alignItems: "center",
                      padding: moderateScale(4, 0.5),
                      paddingHorizontal: SPACE_HORIZONTAL,
                      paddingBottom: moderateScale(10, 0.5),
                      paddingTop:
                        this.shouldShowParaBirimiPicker(selectedOb) &&
                        !this.props.session.info.SadeceUlusalPB
                          ? 0
                          : moderateScale(4, 0.5),
                    }}
                  >
                    <Text
                      fontScale={0.6}
                      style={{
                        fontFamily: "Poppins",
                        fontSize: 12,
                        color: "gray",
                      }}
                    >
                      Kullanılabilir Kredi Limitiniz:{" "}
                    </Text>
                    <Text
                      fontScale={0.6}
                      style={{
                        fontFamily: "Poppins-Medium",
                        fontSize: 15,
                        color: "#8BC34A",
                        alignSelf: "baseline",
                      }}
                    >
                      {numberToLocaleString(this.props.accounts.info.KrediLimiti) + " TL"}
                    </Text>
                  </View>
                )}
              {this.props.placeOrder.response?.ilUyarilar.length > 0 &&
                this.props.placeOrder.response?.ilUyarilar.map(item => {
                  return (
                    <View
                      style={{
                        flexDirection: "row",
                        alignItems: "center",
                        padding: moderateScale(6, 0.5),
                        paddingVertical: moderateScale(6, 0.5),
                      }}
                    >
                      <Image
                        source={require("@assets/images/info_white.png")}
                        style={{
                          width: moderateScale(16, 0.5),
                          height: moderateScale(16, 0.5),
                          tintColor: "#A88F80",
                          resizeMode: "contain",
                          marginRight: scale(8),
                          marginLeft: scale(3),
                        }}
                      />
                      <Text
                        fontScale={0.5}
                        style={{
                          fontFamily: "Poppins",
                          fontSize: 12,
                          color: "gray",
                          flex: 1,
                        }}
                      >
                        {item.Mesaj}
                      </Text>
                    </View>
                  );
                })}
              {KayitliKartlar && KayitliKartlar.length > 0 && (
                <View
                  style={{
                    flexDirection: "column",
                    marginLeft: SPACE_HORIZONTAL,
                    marginTop: SPACE_HORIZONTAL,
                  }}
                >
                  <TouchableOpacity
                    style={{
                      flexDirection: "row",
                      alignItems: "center",
                      marginBottom: moderateScale(8, 0.4),
                    }}
                    onPress={() => {
                      if (!this.state.showCreditCardForm) {
                        this.clearCCForm({ showCreditCardForm: true });
                        this.props.chooseInstalment(null, null);
                        LayoutAnimation.configureNext(UPDATE_SPRINGY);
                      }
                    }}
                  >
                    <RadioButton
                      onPress={() => {
                        if (!this.state.showCreditCardForm) {
                          this.clearCCForm({ showCreditCardForm: true });
                          this.props.chooseInstalment(null, null);
                          LayoutAnimation.configureNext(UPDATE_SPRINGY);
                        }
                      }}
                      style={{
                        width: moderateScale(20, 0.5),
                        height: moderateScale(20, 0.5),
                        borderRadius: moderateScale(10, 0.5),
                        marginRight: 8,
                      }}
                      innerStyle={{
                        width: moderateScale(10, 0.5),
                        height: moderateScale(10, 0.5),
                        borderRadius: moderateScale(5, 0.5),
                        backgroundColor: "#d6421e",
                      }}
                      selected={this.state.showCreditCardForm}
                    />
                    <Text style={{ fontSize: 14, color: "#464646" }}>Yeni bir kart ile öde</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={{ flexDirection: "row", alignItems: "center" }}
                    onPress={() => {
                      if (this.state.showCreditCardForm) {
                        this.clearCCForm({ showCreditCardForm: false });
                        this.props.chooseInstalment(null, null);
                        LayoutAnimation.configureNext(UPDATE_SPRINGY);
                      }
                    }}
                  >
                    <RadioButton
                      onPress={() => {
                        if (this.state.showCreditCardForm) {
                          this.clearCCForm({ showCreditCardForm: false });
                          this.props.chooseInstalment(null, null);
                          LayoutAnimation.configureNext(UPDATE_SPRINGY);
                        }
                      }}
                      style={{
                        width: moderateScale(20, 0.5),
                        height: moderateScale(20, 0.5),
                        borderRadius: moderateScale(10, 0.5),
                        marginRight: 8,
                      }}
                      innerStyle={{
                        width: moderateScale(10, 0.5),
                        height: moderateScale(10, 0.5),
                        borderRadius: moderateScale(5, 0.5),
                        backgroundColor: "#d6421e",
                      }}
                      selected={!this.state.showCreditCardForm}
                    />
                    <Text style={{ fontSize: 14, color: "#464646" }}>Kayıtlı bir kart ile öde</Text>
                  </TouchableOpacity>
                </View>
              )}
              {!this.state.showCreditCardForm &&
                KayitliKartlar != null &&
                KayitliKartlar.length > 0 && (
                  <SavedCard
                    onPress={() => this.cardListModal.toggleVisibility()}
                    item={KayitliKartlar[this.state.seciliKayitliKartIndex]}
                    containerStyle={{
                      marginHorizontal: SPACE_HORIZONTAL,
                      marginTop: SPACE_HORIZONTAL,
                    }}
                  />
                )}
              {!this.state.showCreditCardForm &&
                KayitliKartlar != null &&
                KayitliKartlar.length > 0 && (
                  <View
                    style={{
                      flexDirection: "row",
                      flex: 1,
                      alignItems: "center",
                      justifyContent: "center",
                      marginTop: SPACE_HORIZONTAL,
                    }}
                  >
                    <TouchableOpacity
                      onPress={() => {
                        LayoutAnimation.configureNext(UPDATE_SPRINGY);
                        this.clearCCForm({ showCreditCardForm: true });
                      }}
                      style={{ flexDirection: "row", alignItems: "center" }}
                    >
                      <Text
                        style={{
                          fontFamily: "Poppins-Medium",
                          fontSize: 16,
                          color: "rgb(52,134,203)",
                          marginRight: 5,
                        }}
                      >
                        +
                      </Text>
                      <Text
                        style={{
                          fontFamily: "Poppins",
                          fontSize: 14,
                          color: "rgb(52,134,203)",
                        }}
                      >
                        Yeni Kart Ekle
                      </Text>
                    </TouchableOpacity>
                  </View>
                )}
              {this.props.placeOrder.response?.SiparisVerisi?.IlBaslikUyOut[0]
                .SiparisOdemeBicimi === "K" && (
                <CreditCardForm
                  setSelectedBankData={this.setSelectedBankData}
                  session={this.props.session}
                  showCreditCardForm={this.state.showCreditCardForm}
                  currency={this.props.placeOrder.response.SiparisVerisi.GenelToplam.ToplamPb}
                  selectedSavedCard={KayitliKartlar[this.state.seciliKayitliKartIndex]?.KartDetayi}
                  ref={c => (this.creditCardForm = c)}
                  scrollToFocusedInput={offset => {
                    console.log("scroll offset", offset - (win.height - this.state.keyboardHeight));
                    if (offset > win.height - this.state.keyboardHeight - AnimatedHeader.height)
                      this.flatList.getNode().scrollToOffset({
                        offset:
                          this.scrollY +
                          offset -
                          (win.height - this.state.keyboardHeight) +
                          AnimatedHeader.height,
                        animated: true,
                      });
                  }}
                  setName={ccName =>
                    this.setState({
                      ...this.state,
                      creditCardForm: { ...this.state.creditCardForm, ccName },
                    })
                  }
                  setNumber={ccn => {
                    this.setState({
                      ...this.state,
                      creditCardForm: { ...this.state.creditCardForm, ccn },
                    });
                    console.log("cc changed", ccn, ccn.length);
                  }}
                  setCvc={cvc =>
                    this.setState({
                      creditCardForm: { ...this.state.creditCardForm, cvc },
                    })
                  }
                  setNewCardDesc={newCardDesc => {
                    this.setState({
                      ...this.state,
                      creditCardForm: {
                        ...this.state.creditCardForm,
                        newCardDesc,
                        isCardNameDefault: false,
                      },
                    });
                  }}
                  toggleSaveCard={() => {
                    if (this.state.creditCardForm.selectedBankData?.bank_id != null) {
                      const bank = this.state.creditCardForm.selectedBankData.bank_name;
                      const defaultName = (bank != null ? bank + " " : "") + "Kartım";
                      this.setState({
                        ...this.state,
                        creditCardForm: {
                          ...this.state.creditCardForm,
                          saveCard: !this.state.creditCardForm.saveCard,
                          newCardDesc:
                            this.state.creditCardForm.newCardDesc === ""
                              ? defaultName
                              : this.state.creditCardForm.newCardDesc,
                        },
                      });
                    } else {
                      Alert.alert("Öncelikle banka seçiniz");
                    }
                  }}
                  setCardType={cardType =>
                    this.setState({
                      ...this.state,
                      creditCardForm: { ...this.state.creditCardForm, cardType },
                    })
                  }
                  toggleMonthPicker={() =>
                    this.monthPicker &&
                    this.monthPicker.openPicker(null, null, Platform.OS === "android")
                  }
                  creditCardForm={this.state.creditCardForm}
                  onChangeForm={form => this.setState({ ...this.state, creditCardForm: form })}
                  toggleYearPicker={() =>
                    this.yearPicker &&
                    this.yearPicker.openPicker(null, null, Platform.OS === "android")
                  }
                  toggleBankPicker={() =>
                    this.bankPicker &&
                    this.bankPicker.openPicker(null, null, Platform.OS === "android")
                  }
                  toggleInstalmentPicker={() =>
                    this.bottomInstalmentPicker &&
                    this.bottomInstalmentPicker.openPicker(null, null, Platform.OS === "android")
                  }
                  PaynetData={this.props.placeOrder?.response?.PaynetData}
                  instalment={this.props.instalment}
                  savedCard={KayitliKartlar[this.state.seciliKayitliKartIndex]}
                  seciliTaksit={this.state.seciliTaksit}
                  selectedInstalment={this.props.placeOrder?.response?.SiparisVerisi?.Taksit}
                  selectedBankData={this.state.creditCardForm.selectedBankData}
                  chooseInstalment={this.props.chooseInstalment}
                />
              )}
              {this.props.placeOrder.response?.SiparisVerisi?.IlBaslikUyOut[0]
                .SiparisOdemeBicimi === "K" &&
                this.state.creditCardForm.saveCard &&
                this.state.showCreditCardForm && (
                  <View
                    ref={c => (this.kssToggle = c)}
                    style={{
                      flexDirection: "row",
                      flex: 1,
                      alignItems: "center",
                      paddingHorizontal: SPACE_HORIZONTAL,
                      marginVertical: scale(8),
                    }}
                  >
                    <TouchableOpacity
                      style={{ marginRight: moderateScale(5, 0.3) }}
                      onPress={() => this.setState({ kss: !this.state.kss })}
                    >
                      {this.state.kss ? (
                        <FontAwesome
                          name="check-square-o"
                          color="#3498db"
                          size={moderateScale(20, 0.4)}
                        />
                      ) : (
                        <FontAwesome name="square-o" color="gray" size={moderateScale(20, 0.4)} />
                      )}
                    </TouchableOpacity>
                    <Text fontScale={0.5} style={{ marginLeft: moderateScale(5, 0.5) }}>
                      <Text
                        fontScale={0.5}
                        style={{
                          fontFamily: "Poppins",
                          fontSize: 13,
                          color: "#666",
                          marginLeft: scale(5),
                        }}
                      >
                        Kredi kartı bilgilerimin BDDK onaylı Paynet ile kaydedilmesini onaylıyor ve{" "}
                      </Text>
                      <Text
                        fontScale={0.5}
                        onPress={() =>
                          this.wvModal.toggleModal(
                            {
                              uri:
                                "https://www.pencere.com/uploads/paynet/kart_saklama_ve_duzenli_odeme_musteri_cerceve_sozlesmesi.html",
                            },
                            "Kart Saklama Sözleşmesi",
                          )
                        }
                        style={{
                          fontFamily: "Poppins",
                          fontSize: 13,
                          color: "#439ef3",
                          marginLeft: scale(5),
                        }}
                      >
                        kart saklama sözleşmesini
                      </Text>
                      <Text
                        fontScale={0.5}
                        style={{
                          fontFamily: "Poppins",
                          fontSize: 13,
                          color: "#666",
                          marginLeft: scale(5),
                        }}
                      >
                        {" "}
                        kabul ediyorum.
                      </Text>
                    </Text>
                  </View>
                )}
              {/* {
                                this.props.placeOrder.response ?.SiparisVerisi ?.IlBaslikUyOut[0].SiparisOdemeBicimi === 'K' &&
                                this.state.creditCardForm.saveCard && this.state.showCreditCardForm &&
                <View style={{ flexDirection: 'row', alignItems: 'center', paddingHorizontal: SPACE_HORIZONTAL }}>
                  <SwitchToggle
                    containerStyle={{
                      width: moderateScale(40, .35),
                      height: moderateScale(24, .35),
                      borderRadius: moderateScale(12, .35),
                      borderColor: 'rgba(0,0,0,.1)',
                      borderWidth: 1,
                      padding: moderateScale(4, .35),
                    }}
                    circleStyle={{
                      width: moderateScale(18, .35),
                      height: moderateScale(18, .35),
                      borderRadius: moderateScale(9, .35),
                    }}
                    switchOn={this.props.creditCardForm.saveCard}
                    onPress={() => this.props.toggleSaveCard()}
                    circleColorOff='white'
                    circleColorOn='#3486cb'
                    backgroundColorOn='#eee'
                    backgroundColorOff='#efefef'
                    duration={200}
                  />
                  <Text style={{ marginLeft: moderateScale(10, .5), fontSize: 14, fontFamily: 'Poppins', color: '#666' }}>Diğer kullanıcılar kullanabilir</Text>
                </View>
              } */}
            </View>
          </View>
        </View>
      </View>
    );
  };

  dontUpdateBank = false;
  isFetchingBank = false;

  setSelectedBankData = async ccn => {
    if (ccn.length > 5) {
      try {
        this.isFetchingBank = true;
        const res = await getRatios(
          ccn.substring(0, 6),
          this.props.placeOrder.response.PaynetData.PublicKey,
          this.props.placeOrder.response.PaynetData.AgentId,
        );
        if (this.isFetchingBank) {
          this.isFetchingBank = false;
          var selectedBankIndex = -1;
          this.props.placeOrder.response.PaynetData.RatioData.Data[
            this.state.creditCardForm.cardType
          ].banks.forEach((item, index) => {
            if (item.bank_id === res.data[0].bank_id) {
              selectedBankIndex = index;
            }
          });
          this.setState({
            ...this.state,
            creditCardForm: {
              ...this.state.creditCardForm,
              selectedBankData: this.props.placeOrder.response.PaynetData.RatioData.Data[
                this.state.creditCardForm.cardType
              ].banks[selectedBankIndex],
              selectedBankIndex,
            },
          });
        }
      } catch (err) {
        this.isFetchingBank = false;
        console.log(err);
        this.setState({
          ...this.state,
          creditCardForm: { ...this.state.creditCardForm, selectedBankData: {} },
        });
        this.props.chooseInstalment(null, null);
      }
    } else {
      this.isFetchingBank = false;
      this.setState({
        ...this.state,
        creditCardForm: { ...this.state.creditCardForm, selectedBankData: {} },
      });
      this.props.chooseInstalment(null, null);
    }
  };

  onScroll = event => {
    var dir = event.nativeEvent.contentOffset.y > this.currentOffset ? "out" : "in";
    // if (Math.abs(event.nativeEvent.contentOffset.y - this.currentOffset) > 5)
    //   this.refs.animatedHeader.getWrappedInstance().startSearchBarAnim(dir);

    this.currentOffset = event.nativeEvent.contentOffset.y;
    this.currentOffset = this.currentOffset < 0 ? 0 : this.currentOffset;
    this.scrollY = event.nativeEvent.contentOffset.y;
    dir =
      this.currentOffset >
      event.nativeEvent.contentSize.height - verticalScale(250) - this.innerHeight
        ? "in"
        : "out";
    if (dir === "in" && this.state.shadowOpacity === 0) {
      this.setState({ ...this.state, shadowOpacity: 0.05 });
    } else if (dir === "out" && this.state.shadowOpacity === 0.05) {
      this.setState({ ...this.state, shadowOpacity: 0 });
    }
  };

  _renderRow(rowData) {
    return (
      <View
        style={{
          borderRadius: 5,
          borderWidth: StyleSheet.hairlineWidth,
          marginHorizontal: SPACE_HORIZONTAL,
          borderColor: "#cecece",
          marginTop: moderateScale(10, 0.5),
        }}
      >
        <View
          style={{
            flexDirection: "row",
            padding: SPACE_HORIZONTAL,
            justifyContent: "space-between",
            alignItems: "center",
            height: moderateScale(50, 0.5),
            backgroundColor: "white",
            borderTopLeftRadius: 4,
            borderTopRightRadius: 4,
            borderBottomColor: "#dadada",
            borderBottomWidth: 1,
          }}
        >
          <View style={{ flexDirection: "row" }}>
            <Text fontScale={0.5} style={{ fontFamily: "Poppins", color: "#d6421e", fontSize: 14 }}>
              {rowData.UretimYeri === "ZHAS" ? "Defolu Siparişiniz" : "Siparişiniz"}
            </Text>
            <Text
              fontScale={0.5}
              style={{ fontFamily: "Poppins", color: "#bebebe", fontSize: 14 }}
            >{` (${rowData.IlKalemlerOut?.length} ürün)`}</Text>
          </View>
          <TouchableOpacity
            style={{ flexDirection: "row", alignItems: "center", height: 40 }}
            onPress={() => {
              LayoutAnimation.configureNext(UPDATE_SPRINGY);
              this.setState({
                ...this.state,
                listCollapsed: !this.state.listCollapsed,
              });
            }}
          >
            <Image
              source={
                this.state.listCollapsed
                  ? require("@assets/images/eye.png")
                  : require("@assets/images/hide.png")
              }
              style={{
                width: 16,
                height: 16,
                resizeMode: "contain",
                tintColor: "#c4c4c4",
              }}
            />
            <Text
              fontScale={0.5}
              style={{
                fontFamily: "Poppins",
                color: "#c4c4c4",
                fontSize: 12,
                marginLeft: 5,
              }}
            >
              {this.state.listCollapsed ? "Göster" : "Gizle"}
            </Text>
          </TouchableOpacity>
        </View>
        {rowData.IlKalemlerOut.map(urun => {
          if (!this.state.listCollapsed) {
            if (urun.IlaltKalemler.length === 0 && urun.KalemReferans3 === "")
              return (
                <OrderProductItem
                  onPressVade={(list, selectedVade, selectedKalem) => {
                    console.log("onPressVade", list, selectedVade, selectedKalem);
                    this.setState(
                      {
                        ...this.state,
                        selectedVadeSecenekleri: list,
                        selectedKalem,
                      },
                      () =>
                        this.vadePicker.openPicker(null, selectedVade, Platform.OS === "android"),
                    );
                  }}
                  item={urun}
                  IdUrunExt={this.props.placeOrder.response.IdUrunExt}
                  navigation={this.props.navigation}
                  removeFromCart={this.removeFromCart}
                />
              );
            else if (urun.KalemReferans3 === "0") {
              var subItems = this.props.placeOrder.response.SiparisVerisi.IlBaslikUyOut[0].IlKalemlerOut.filter(
                item => item.KalemReferans1 === urun.KalemReferans1 && item.KalemReferans3 !== "0",
              );
              return (
                <OrderProductBundle
                  onPressVade={(list, selectedVade, selectedKalem) => {
                    console.log("onPressVade", list, selectedVade, selectedKalem);
                    this.setState(
                      {
                        ...this.state,
                        selectedVadeSecenekleri: list,
                        selectedKalem,
                      },
                      () =>
                        this.vadePicker.openPicker(null, selectedVade, Platform.OS === "android"),
                    );
                  }}
                  item={urun}
                  IdUrunExt={this.props.placeOrder.response.IdUrunExt}
                  subItems={subItems}
                  navigation={this.props.navigation}
                  removeFromCart={this.removeFromCart}
                  kalemlerOut={
                    this.props.placeOrder.response.SiparisVerisi.IlBaslikUyOut[0].IlKalemlerOut
                  }
                />
              );
            } else if (urun.IlaltKalemler.length > 0) {
              return (
                <OrderProductBundle
                  onPressVade={(list, selectedVade, selectedKalem) => {
                    console.log("onPressVade", list, selectedVade, selectedKalem);
                    this.setState(
                      {
                        ...this.state,
                        selectedVadeSecenekleri: list,
                        selectedKalem,
                      },
                      () =>
                        this.vadePicker.openPicker(null, selectedVade, Platform.OS === "android"),
                    );
                  }}
                  item={urun}
                  IdUrunExt={this.props.placeOrder.response.IdUrunExt}
                  subItems={urun.IlaltKalemler}
                  altKalem={true}
                  navigation={this.props.navigation}
                  removeFromCart={this.removeFromCart}
                />
              );
            } else return null;
          } else return null;
        })}
      </View>
    );
  }

  render() {
    if (this.props.placeOrder?.response?.MesajTipi === "E") {
      this.props.navigation.goBack();
      return null;
    } else {
      const baslikOut = this.props.placeOrder.response?.SiparisVerisi?.BaslikOut;
      var selectedOb = this.props.placeOrder.response.IdOdemeBicimleri.filter(
        item => item.OdemeBicimi1 === baslikOut.SiparisOdemeBicimi,
      );
      if (selectedOb.length > 0) selectedOb = selectedOb[0];
      else selectedOb = null;
      var KayitliKartlar = this.props.placeOrder?.response?.KartSaklamaBilgileri?.KayitliKartlar;
      const IdUrunExt = this.props.placeOrder?.response?.IdUrunExt;
      var total =
        this.props.instalment?.instalmentInfo?.charged_amount &&
        this.props.placeOrder.response?.SiparisVerisi?.IlBaslikUyOut[0].SiparisOdemeBicimi === "K"
          ? this.props.instalment.instalmentInfo?.charged_amount
          : this.props.placeOrder.response.SiparisVerisi.GenelToplam.ToplamTutar;
      total =
        numberToLocaleString(total) +
        " " +
        this.props.placeOrder.response.SiparisVerisi.GenelToplam.ToplamPb;

      return (
        <View
          style={{ backgroundColor: colors.primary_dark, flex: 1 }}
          forceInset={{ top: "always" }}
        >
          <View
            style={{
              flexGrow: 1,
              width: win.width,
              backgroundColor: "#efefef",
            }}
          >
            <AnimatedFlatList
              ref={c => (this.flatList = c)}
              onLayout={({
                nativeEvent: {
                  layout: { height },
                },
              }) => (this.innerHeight = height)}
              ListHeaderComponent={this.renderHeader(selectedOb)}
              ListFooterComponent={this.renderFooter(selectedOb)}
              data={this.props.placeOrder.response?.SiparisVerisi?.IlBaslikUyOut}
              renderItem={({ item }) => this._renderRow(item)}
              keyExtractor={item => item.KalemUrunKodu}
              scrollIndicatorInsets={{
                top: moderateScale(50, 0.4),
                left: 0,
                bottom: 0,
                right: 0,
                height: this.state.listCollapsed ? null : 0,
              }}
              contentContainerStyle={{
                flexGrow: 1,
                backgroundColor: "#efefef",
                paddingTop:
                  Platform.OS === "android"
                    ? moderateScale(50, 0.4) +
                      SPACE_HORIZONTAL +
                      (Platform.Version < 20 ? 0 : StatusBar.currentHeight)
                    : moderateScale(50, 0.4) + SPACE_HORIZONTAL,
              }}
              onScroll={this.onScroll}
              scrolllEventThrottle={16}
            />
            {this.props.placeOrder.response.MesajTipi === "E" &&
              this.props.placeOrder.response.Mesaj != null && (
                <View style={{ marginTop: moderateScale(50, 0.4) }}>
                  <Text fontScale={0.5} style={{ fontSize: 14, color: "gray" }}>
                    {this.props.placeOrder.response.Mesaj}
                  </Text>
                </View>
              )}
          </View>

          {this.props.placeOrder.response?.SiparisVerisi?.IlUygunsuzlar &&
            this.props.placeOrder.response?.SiparisVerisi?.IlUygunsuzlar.length > 0 && (
              <ListModal
                contentContainerstyle={{ padding: moderateScale(10, 0.5) }}
                title="Temin Edilemeyen Ürünler"
                ref={c => (this.listModal = c)}
                items={this.props.placeOrder.response?.SiparisVerisi?.IlUygunsuzlar}
                renderItem={item => {
                  return (
                    <View
                      style={{
                        flexDirection: "column",
                        padding: moderateScale(10, 0.5),
                        paddingVertical: moderateScale(5, 0.5),
                        borderRadius: 3,
                        borderWidth: 1,
                        borderColor: "#ddd",
                        marginBottom: moderateScale(10, 0.5),
                      }}
                    >
                      <Text fontScale={0.5}>
                        <Text
                          fontScale={0.5}
                          style={{
                            fontFamily: "Poppins",
                            fontSize: 12,
                            color: "gray",
                          }}
                        >
                          Ürün kodu:{" "}
                        </Text>
                        <Text fontScale={0.5} style={{ fontFamily: "Poppins", fontSize: 12 }}>
                          {item.KalemUrunKodu}
                        </Text>
                      </Text>
                      <Text fontScale={0.5}>
                        <Text
                          fontScale={0.5}
                          style={{
                            fontFamily: "Poppins",
                            fontSize: 12,
                            color: "gray",
                          }}
                        >
                          Ürün adedi:{" "}
                        </Text>
                        <Text fontScale={0.5} style={{ fontFamily: "Poppins", fontSize: 12 }}>
                          {item.KalemUrunMiktari}
                        </Text>
                      </Text>
                      <Text
                        fontScale={0.5}
                        style={{
                          fontFamily: "Poppins",
                          fontSize: 12,
                          color: "#f44336",
                        }}
                      >
                        {item.KalemUygunsuzluSebebiText}
                      </Text>
                    </View>
                  );
                }}
              />
            )}
          {this.props.placeOrder.response?.KartSaklamaBilgileri?.KayitliKartlar &&
            this.props.placeOrder.response?.KartSaklamaBilgileri?.KayitliKartlar.length > 0 && (
              <ListModal
                contentContainerstyle={{ padding: moderateScale(10, 0.5) }}
                title="Kayıtlı Kartlarım"
                ref={c => (this.cardListModal = c)}
                items={KayitliKartlar}
                renderItem={(item, index) => {
                  const card = item.KartDetayi;
                  return (
                    <TouchableOpacity
                      onPress={() => {
                        if (this.state.seciliKayitliKartIndex !== index) {
                          this.props.chooseInstalment(null, null);
                          this.setState({
                            ...this.state,
                            seciliKayitliKartIndex: index,
                          });
                        }
                      }}
                      style={[
                        styles.savedCardContainer,
                        {
                          borderBottomWidth: index < KayitliKartlar.length - 1 ? 1 : 0,
                          paddingVertical: moderateScale(10, 0.5),
                          backgroundColor:
                            this.state.seciliKayitliKartIndex === index ? "#DCEDC8" : "white",
                        },
                      ]}
                    >
                      <View style={{ flexDirection: "row", alignItems: "center" }}>
                        <Text
                          style={{
                            fontFamily: "Poppins",
                            fontSize: 11,
                            color: "black",
                          }}
                        >
                          {card.card_desc}
                        </Text>
                        <Text
                          style={{
                            fontFamily: "Poppins-Light",
                            fontSize: 11,
                            color: "#cecece",
                          }}
                        >
                          {" | " +
                            (card.card_type === "cc"
                              ? "Bireysel"
                              : card.card_type === "bc"
                              ? "Ticari"
                              : "Debit")}
                        </Text>
                      </View>
                      <Text
                        style={{
                          fontFamily: "Poppins-Light",
                          fontSize: 11,
                          color: colors.light_black,
                        }}
                      >
                        {card.card_holder}
                      </Text>
                      <Text
                        style={{
                          fontFamily: "Poppins-Light",
                          fontSize: 11,
                          color: colors.light_black,
                        }}
                      >
                        {card.card_no}
                      </Text>
                      <Image
                        source={{
                          uri: card.card_logo_url.startsWith("https")
                            ? card.card_logo_url
                            : "https://www.pencere.com/" + card.card_logo_url.substring(2),
                        }}
                        style={{
                          position: "absolute",
                          top: -5,
                          right: SPACE_HORIZONTAL,
                          width: 70,
                          height: 30,
                          resizeMode: "cover",
                        }}
                        resizeMode="contain"
                      />
                    </TouchableOpacity>
                  );
                }}
              />
            )}
          {this.props.placeOrder.response?.MobileKuponlar?.MobileKuponList != null &&
            this.props.placeOrder.response?.MobileKuponlar?.MobileKuponList.length > 0 && (
              <ListModal
                contentContainerstyle={{ padding: moderateScale(10, 0.5) }}
                title="Kuponlarım"
                ref={c => (this.kuponListModal = c)}
                items={this.props.placeOrder.response?.MobileKuponlar?.MobileKuponList}
                renderItem={(item, index) => {
                  const kList = this.props.placeOrder.response?.MobileKuponlar?.MobileKuponList;
                  return (
                    <TouchableOpacity
                      onPress={() =>
                        this.setState({
                          ...this.state,
                          seciliKuponIndex: index,
                          couponCode: item.KuponKodu,
                        })
                      }
                      style={[
                        styles.couponItemContainer,
                        {
                          borderBottomWidth: index < kList.length - 1 ? 1 : 0,
                          backgroundColor:
                            this.state.seciliKuponIndex === index ? "#DCEDC8" : "white",
                        },
                      ]}
                    >
                      <View style={{ flexDirection: "row", alignItems: "center" }}>
                        <Text
                          style={{
                            fontFamily: "Poppins",
                            fontSize: 11,
                            color: "black",
                          }}
                        >
                          {item.KuponKodu}
                        </Text>
                      </View>
                      <Text
                        style={{
                          fontFamily: "Poppins",
                          fontSize: 11,
                          color: colors.light_black,
                        }}
                      >
                        {item.KuponText}
                      </Text>
                    </TouchableOpacity>
                  );
                }}
              />
            )}
          {this.props.placeOrder.response?.PaynetData?.IlPaynetKampanya &&
            this.props.placeOrder.response?.PaynetData?.IlPaynetKampanya.length > 0 && (
              <ListModal
                title="Paynet Kampanyaları"
                items={this.props.placeOrder.response?.PaynetData?.IlPaynetKampanya}
                titleStyle={{ marginBottom: 0 }}
                renderItem={(item, index) => {
                  return (
                    <View style={{ flexDirection: "column" }}>
                      <View style={{ padding: 10 }}>
                        <Text fontScale={0.5}>
                          <Text fontScale={0.5} style={{ fontFamily: "Poppins", fontSize: 14 }}>
                            {"Sepetinizdeki " +
                              item.Tutar +
                              " TL tutarındaki ürün için uygun taksit seçeneğini seçmeniz durumunda "}
                          </Text>
                          <Text
                            fontScale={0.5}
                            style={{
                              fontFamily: "Poppins-SemiBold",
                              fontSize: 14,
                            }}
                          >
                            {"" + item.KampanyaAciklama + ""}
                          </Text>
                          <Text fontScale={0.5} style={{ fontFamily: "Poppins", fontSize: 14 }}>
                            {" "}
                            kampanyasından yararlanabilirsiniz.
                          </Text>
                        </Text>
                      </View>
                      {index <
                        this.props.placeOrder.response?.PaynetData?.IlPaynetKampanya.length - 1 && (
                        <View
                          style={{
                            height: 1,
                            flexDirection: "row",
                            flex: 1,
                            backgroundColor: "#cdcdcd",
                          }}
                        />
                      )}
                    </View>
                  );
                }}
              />
            )}
          <StickyFooter
            total={total}
            validateForm={this.validateForm}
            getValidationAlertMessage={this.getValidationAlertMessage}
            confirmOrder={this.confirmOrder}
            placeOrder={this.props.placeOrder}
            inProgress={this.state.inProgress}
            payment={this.props.payment}
            shadowOpacity={this.state.shadowOpacity}
          />
          {this.shouldShowParaBirimiPicker(selectedOb) &&
            this.props.placeOrder.response?.SiparisVerisi && (
              <BottomPicker
                ref={c => (this.bottomCurrencyPicker = c)}
                items={Object.keys(selectedOb.IlParaBirimleri)}
                initialItem={this.props.placeOrder.response?.SiparisVerisi?.BaslikOut.SiparisPb}
                onPressOK={pb =>
                  this.props.changePaymentMethod(
                    this.props.placeOrder.response?.SiparisVerisi?.BaslikOut.SiparisOdemeBicimi,
                    pb,
                  )
                }
              />
            )}
          {this.props.placeOrder.response?.PaynetData?.RatioData?.Data != null &&
            this.state.creditCardForm.selectedBankIndex !== -1 && (
              <BottomPicker
                ref={c => (this.bottomInstalmentPicker = c)}
                initialItem={
                  this.props.placeOrder.response?.PaynetData?.RatioData?.Data[
                    this.state.creditCardForm.cardType
                  ].banks[this.state.creditCardForm.selectedBankIndex].ratio[
                    this.props.instalment.selectedIndex || 0
                  ]
                }
                getIndicatorColor={item => {
                  (item?.campaign_commision_support != null && item?.campaign_commision_support) !==
                  0
                    ? "#00c853"
                    : "#dedede";
                }}
                items={
                  this.props.placeOrder.response?.PaynetData?.RatioData?.Data[
                    this.state.creditCardForm.cardType
                  ].banks[this.state.creditCardForm.selectedBankIndex].ratio
                }
                valueExtractor={item => item}
                labelExtractor={item =>
                  `${item.desc} %${(item.ratio * 100).toFixed(2)}` +
                  (item.campaign_commision_support !== 0
                    ? ` (${item.campaign_commision_support +
                        " " +
                        (this.props.placeOrder.response.SiparisVerisi?.BaslikOut?.SiparisPb ===
                        "TRY"
                          ? "TL"
                          : this.props.placeOrder.response.SiparisVerisi?.BaslikOut?.SiparisPb ||
                            "")})`
                    : "")
                }
                onPressOK={(instalment, index) => {
                  console.log(
                    "ok taksit",
                    instalment,
                    this.state.creditCardForm.selectedBankData?.ratio,
                  );
                  this.props.chooseInstalment(instalment, index);
                }}
              />
            )}

          {this.props.placeOrder.response?.SiparisVerisi?.IlBaslikUyOut[0].SiparisOdemeBicimi ===
            "K" && (
            <BottomPicker
              ref={c => (this.monthPicker = c)}
              initialItem={months[this.state.creditCardForm.expMonthIndex]}
              items={months}
              onPressOK={(val, index) =>
                this.setState({
                  ...this.state,
                  creditCardForm: {
                    ...this.state.creditCardForm,
                    expMonthIndex: index,
                  },
                })
              }
            />
          )}
          {this.props.placeOrder.response?.SiparisVerisi?.IlBaslikUyOut[0].SiparisOdemeBicimi ===
            "K" && (
            <BottomPicker
              ref={c => (this.yearPicker = c)}
              initialItem={this.state.creditCardForm.expYear}
              items={years}
              onPressOK={(val, index) => {
                console.log("year picker ok", val, index);
                this.setState({
                  ...this.state,
                  creditCardForm: { ...this.state.creditCardForm, expYear: val },
                });
              }}
            />
          )}
          {IdUrunExt &&
            this.state.selectedKalem?.KalemUrunKodu != null &&
            IdUrunExt[this.state.selectedKalem.KalemUrunKodu].IlVadeSecenekleri.length > 0 && (
              <BottomPicker
                ref={c => (this.vadePicker = c)}
                items={IdUrunExt[this.state.selectedKalem.KalemUrunKodu].IlVadeSecenekleri}
                valueExtractor={item => item.OdemeKosulu}
                labelExtractor={item => item.Aciklama}
                onPressOK={(val, index) => {
                  console.log("vade picker ok", val, index);
                  this.props.changePaymentCondition(
                    this.props.placeOrder.response?.SiparisVerisi?.IlBaslikUyOut[0].UretimYeri,
                    this.state.selectedKalem.KalemKey,
                    val,
                  );
                }}
              />
            )}

          <WebViewModal
            ref={c => (this.wvModal = c)}
            title={"Ön Bilgilendirme Formu"}
            style={{ height: win.height * 0.8 }}
            charge={this.charge}
          />
          <OrderConfirmationDialog
            ref={c => (this.orderConfirmDialog = c)}
            navigation={this.props.navigation}
            confirmOrder={this.confirmOrder}
          />
          <ChargeResultModal
            ref={c => (this.resultModal = c)}
            placeOrder={this.props.placeOrder}
            chargeResult={this.state.chargeResult}
            navigation={this.props.navigation}
          />
          <BankAccountsModal ref={c => (this.accountsModal = c)} accounts={this.state.accounts} />
          {(this.props.placeOrder.isFetching ||
            this.state.inProgress ||
            this.props.payment.progress) && (
            <View
              style={{
                width: win.width,
                height: win.height,
                justifyContent: "center",
                alignItems: "center",
                position: "absolute",
                top: 0,
                left: 0,
                backgroundColor: "rgba(0,0,0,0.3)",
              }}
            >
              <PencereIndicator />
            </View>
          )}

          <AnimatedHeader
            title="Sipariş Ver"
            navigation={this.props.navigation}
            alignLeft={true}
            back={true}
            hideIcons={true}
            searchBar={false}
            onBackPressed={() =>
              this.props.navigation.navigate(
                this.props.navigation.state?.params?.latestScreen || "ShoppingCartScreen",
              )
            }
          />
        </View>
      );
    }
  }

  confirmOrder = () => {
    Keyboard.dismiss();
    this.setState({ ...this.state, inProgress: true });
    console.log(
      "ksa2",
      getEndPoint("ORDER_URL") + "?Si=" + this.props.session.info.SessionID + "&c=siparis_saklandi",
    );
    saveOrder()
      .then(responseJson => {
        console.log("confirmOrder", responseJson);
        if (responseJson.MesajTipi === "S") {
          this.props
            .confirmOrder()
            .then(result => {
              console.log("saveOrder response", result);
              this.setState({ ...this.state, inProgress: false });
              if (
                this.props.placeOrder.response?.SiparisVerisi?.IlBaslikUyOut[0]
                  .SiparisOdemeBicimi === "K"
              ) {
                this.payFromCreditCard();
              } else {
                trackCheckout();
                this.props.navigation.push("OrderResultScreen", {
                  response: result,
                });
              }
            })
            .catch(err => {
              console.log(err);
              Alert.alert(
                "Hata",
                "Bir hata oluştu. Lütfen tekrar deneyiniz.",
                [{ text: "Tamam", onPress: () => console.log("Tamam") }],
                { cancelable: false },
              );
            });
        } else {
          this.setState({ ...this.state, inProgress: false });
          Alert.alert(
            "Hata",
            responseJson.Mesaj,
            [{ text: "Tamam", onPress: () => console.log("Tamam") }],
            {
              cancelable: false,
            },
          );
        }
      })
      .catch(() => {
        Alert.alert(
          "Hata",
          "Bir hata oluştu. Lütfen tekrar deneyiniz.",
          [{ text: "Tamam", onPress: () => console.log("Tamam") }],
          { cancelable: false },
        );
        this.setState({ ...this.state, inProgress: false });
      });
  };

  get3DSecureUrl(authPayload, tokenPayload) {
    console.log("ksa6", getEndPoint("GET_3DSECURE_URL", "paynet"));
    fetch(getEndPoint("GET_3DSECURE_URL", "paynet"), {
      method: "POST",
      headers: new Headers({
        Authorization: "Basic " + this.props.placeOrder.response.PaynetData.PublicKey,
        "Content-Type": "application/json",
      }),
      body: JSON.stringify({
        token_id: tokenPayload.data.token_id,
        session_id: authPayload.data.session_id,
        publishable_key: this.props.placeOrder.response.PaynetData.PublicKey,
      }),
    })
      .then(res => res.text())
      .then(responseHtml => {
        Keyboard.dismiss();
        if (Platform.OS === "android") {
          const rgbHex = /#([0-9A-F][0-9A-F])([0-9A-F][0-9A-F])([0-9A-F][0-9A-F])/gi;
          responseHtml = responseHtml.replace(rgbHex, (m, r, g, b) => {
            return "rgb(" + parseInt(r, 16) + "," + parseInt(g, 16) + "," + parseInt(b, 16) + ")";
          });
          responseHtml = responseHtml.replace(/#/g, ".");
          responseHtml = responseHtml.replace("id=", "class=");
        }
        console.log(
          "3d html",
          responseHtml,
          responseHtml.lastIndexOf("<html>"),
          responseHtml.lastIndexOf("</html>"),
        );

        this.wvModal.toggleModal({ html: responseHtml }, "3D Güvenlik Sistemi", {
          height: win.height * 0.6,
        });
        this.setState({ ...this.state, inProgress: false });
        //this.props.navigation.navigate('OrderConfirmationScreen');
      });
  }

  validateForm = () => {
    if (
      this.props.placeOrder.response?.SiparisVerisi?.IlBaslikUyOut[0].SiparisOdemeBicimi === "K"
    ) {
      if (this.state.showCreditCardForm)
        return (
          this.state.creditCardForm.ccName !== "" &&
          this.state.creditCardForm.ccn.length > 15 &&
          this.state.creditCardForm.cvc.length > 2 &&
          (this.props.instalment.instalmentInfo?.instalment_key ||
            this.props.placeOrder.response?.SiparisVerisi?.BaslikOut?.Taksit) &&
          ((this.state.creditCardForm.saveCard && this.state.kss) ||
            !this.state.creditCardForm.saveCard) &&
          this.state.obf &&
          (!this.state.showCreditCardForm || this.state.creditCardForm.selectedBankData?.bank_id)
        );
      else
        return (
          (this.props.instalment.instalmentInfo?.instalment_key ||
            this.props.placeOrder.response?.SiparisVerisi?.BaslikOut?.Taksit) &&
          this.state.obf
        );
    } else {
      return this.state.obf;
    }
  };

  payFromCreditCard = () => {
    this.setState({ ...this.state, inProgress: true });
    var ownerId = null;
    var userUniqueId = null;
    if (
      this.props.placeOrder.response.KartSaklamaBilgileri.BayiBilgileri.PaynetCardOwnerId !== ""
    ) {
      ownerId = this.props.placeOrder.response.KartSaklamaBilgileri.BayiBilgileri.PaynetCardOwnerId;
    } else {
      userUniqueId = this.props.placeOrder.response.KartSaklamaBilgileri.BayiBilgileri.BayiUniqueId;
    }

    var total = this.props.instalment?.instalmentInfo?.charged_amount;
    if (total != null) {
      this.props
        .authPaynet(total, this.props.session.info.BayiKodu, ownerId, userUniqueId)
        .then(authPayload => {
          if (authPayload.type === "AUTH_SUCCESS") {
            if (this.state.showCreditCardForm) {
              this.props.updateSaveCard(
                this.state.creditCardForm.saveCard,
                this.state.creditCardForm.newCardDesc,
              );
              this.props
                .createToken(
                  authPayload.data.session_id,
                  this.state.creditCardForm.ccName,
                  this.state.creditCardForm.ccn,
                  this.state.creditCardForm.expMonthIndex + 1,
                  this.state.creditCardForm.expYear,
                  this.state.creditCardForm.cvc,
                  this.props?.instalment?.instalmentInfo?.instalment_key,
                  true,
                  "pencere.com",
                )
                .then(tokenPayload => {
                  if ((tokenPayload.type = "CREATE_TOKEN_SUCCESS")) {
                    this.get3DSecureUrl(authPayload, tokenPayload);
                  }
                })
                .catch(payload => {
                  if (payload.type === "API_ERROR") {
                    this.setState({ ...this.state, inProgress: false });
                    Alert.alert(
                      "Hata",
                      payload.error,
                      [{ text: "Tamam", onPress: () => console.log("Tamam") }],
                      {
                        cancelable: false,
                      },
                    );
                  }
                });
            } else {
              const selectedCard = this.props.placeOrder.response.KartSaklamaBilgileri
                .KayitliKartlar[this.state.seciliKayitliKartIndex].KartDetayi;
              this.props
                .createCardToken(
                  selectedCard.card_hash,
                  selectedCard.card_owner_id,
                  true,
                  this.props.instalment?.instalmentInfo?.instalment_key,
                )
                .then(tokenPayload => {
                  if ((tokenPayload.type = "CREATE_TOKEN_SUCCESS")) {
                    console.log("token payload", tokenPayload);
                    this.get3DSecureUrl(authPayload, tokenPayload);
                  }
                })
                .catch(payload => {
                  if (payload.type === "API_ERROR") {
                    this.setState({ ...this.state, inProgress: false });
                    Alert.alert(
                      "Hata",
                      payload.error,
                      [{ text: "Tamam", onPress: () => console.log("Tamam") }],
                      {
                        cancelable: false,
                      },
                    );
                  }
                });
            }
          }
        })
        .catch(err => {
          console.log("authPaynet err", err);
          Alert.alert("Hata", "Beklenmeyen bir hata oluştu.");
          this.setState({ ...this.state, inProgress: false });
        });
    } else {
      Alert.alert("Hata", "Beklenmeyen bir hata oluştu.");
    }
  };

  scrollToCreditCardForm = offset => {
    if (!offset) offset = 0;
    this.firstBox.measure((fx, fy, width, height) => {
      this.flatList.getNode().scrollToOffset({ offset: fy + height + offset, animated: true });
    });
  };

  getValidationAlertMessage = () => {
    var msg = "Lütfen bütün alanları doldurunuz!";
    if (
      this.props.placeOrder.response?.SiparisVerisi?.IlBaslikUyOut[0].SiparisOdemeBicimi === "K"
    ) {
      if (this.state.showCreditCardForm) {
        if (!this.props.instalment.instalmentInfo?.instalment_key) {
          var bankNotFound =
            this.state.creditCardForm?.selectedBankData?.bank_id != null &&
            this.state?.creditCardForm?.selectedBankIndex === -1;
          msg = bankNotFound
            ? "Bankanıza uygun ödeme seçeneği bulanamadı. Lütfen başka bir kart deneyiniz."
            : "Lütfen istediğiniz taksit seçeneğini seçiniz.";
          this.scrollToCreditCardForm(win.height / 3);
          return msg;
        } else if (
          this.state.creditCardForm.ccName === "" ||
          this.state.creditCardForm.cvc.length < 3 ||
          this.state.creditCardForm.ccn.length < 16
        ) {
          msg = "Kredi kartı bilgilerinizi eksik girdiniz. Lütfen kontrol ediniz.";
          if (
            this.state.creditCardForm.ccn.length === 16 &&
            this.state.creditCardForm.ccName !== "" &&
            this.state.creditCardForm.cvc.length < 3
          ) {
            msg = "Güvenlik kodu 3 haneli olmalıdır";
          } else if (
            this.state.creditCardForm.cvc.length === 3 &&
            this.state.creditCardForm.ccName !== "" &&
            this.state.creditCardForm.ccn.length < 16
          ) {
            msg = "Kredi kartı numaranızı eksik girdiniz. Lütfen kontrol ediniz.";
          } else if (
            this.state.creditCardForm.ccn.length === 16 &&
            this.state.creditCardForm.ccName === "" &&
            this.state.creditCardForm.cvc.length === 3
          ) {
            msg = "Lütfen kredi kartınızın üzerindeki ismi giriniz.";
          }
          this.creditCardForm.formInputs.measure((fx, fy) => {
            this.scrollToCreditCardForm(fy);
          });
          return msg;
        } else if (this.state.creditCardForm.saveCard && !this.state.kss) {
          msg = "Lütfen kart saklama sözleşmesini okuduğunuzu onaylayın.";
          this.kssToggle.measure((fx, fy) => {
            this.scrollToCreditCardForm(fy);
          });
          return msg;
        }
      } else {
        if (
          !this.props.instalment.instalmentInfo?.instalment_key &&
          !this.props.placeOrder.response?.SiparisVerisi?.BaslikOut?.Taksit
        ) {
          msg = "Lütfen istediğiniz taksit seçeneğini seçiniz.";
          this.scrollToCreditCardForm();
          return msg;
        }
      }
    }
    if (!this.state.obf) {
      msg = "Lütfen ön bilgilendirme formunu okuduğunuzu onaylayın.";
      this.flatList.getNode().scrollToEnd();
    }
    return msg;
  };

  chargeResponse = {
    IslemBasarili: true,
    Mesaj1: "Sipariş oluşturuldu",
    PaynetOdemesiAlindi: false,
    OrtalamaKdv: 0,
  };

  charge = () => {
    this.setState({ ...this.state, inProgress: true });
    if (!this.props.payment.authError && !this.props.payment.tokenError) {
      var bank_id = this.state.creditCardForm.selectedBankData?.bank_id;
      if (!this.state.showCreditCardForm) {
        bank_id = this.props.placeOrder.response?.KartSaklamaBilgileri?.KayitliKartlar[
          this.state.seciliKayitliKartIndex
        ]?.KartDetayi?.card_bank_id;
      }
      console.log("ksa7", bank_id);
      if (bank_id == null) {
        this.wvModal.toggleModal().then(() => {
          setTimeout(() => {
            Alert.alert(
              "Hata",
              "Beklenmedik bir hata oluştu. Kredi kartı numarınızı kontrol ediniz.",
            );
            this.setState({ ...this.state, inProgress: false });
          }, 1000);
        });
      } else {
        fetch(
          getEndPoint("UTIL_URL") +
            "?Si=" +
            this.props.session.info.SessionID +
            "&func=11&paynet_islem_id=" +
            this.props.placeOrder.orderResult[0].PaynetIslemID +
            "&item_key=" +
            this.props.instalment.instalmentInfo?.item_key +
            "&token_id=" +
            this.props.payment.token.token_id +
            "&diger_kullanicilar_kullanabilir=" +
            this.state.creditCardForm.allowOtherUsers +
            "&session_id=" +
            this.props.payment.paynetSession.session_id +
            "&bank_id=" +
            bank_id,
        )
          .then(res => {
            return res.json();
          })
          .then(responseJson => {
            trackCheckout();
            this.setState(
              {
                ...this.state,
                chargeResult: responseJson,
              },
              () => {
                setTimeout(() => {
                  this.setState({ ...this.state, inProgress: false });
                  this.resultModal.getWrappedInstance().toggleVisibility();
                }, 1000);
              },
            );
          })
          .catch(err => {
            console.log(err);
            this.setState({ ...this.state, inProgress: false });
            Alert.alert("Hata", "Bir hata oluştu. Lütfen daha sonra tekrar deneyin.");
          });
      }
    }
  };
}

function mapStateToProps(state) {
  return {
    placeOrder: state.placeOrder,
    instalment: state.instalment,
    session: state.session,
    payment: state.payment,
    accounts: state.accounts,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    useCoupon: kuko => dispatch(useCoupon(kuko)),
    updateSaveCard: (save_card, card_desc) => dispatch(saveCardUpdate(save_card, card_desc)),
    getAccounts: () => dispatch(fetchAccountsFromApi()),
    changeShipment: (uy, kf) => dispatch(changeShipment(uy, kf)),
    changePaymentMethod: (ob, pb) => dispatch(changePaymentMethod(ob, pb)),
    changePaymentCondition: (uy, kk, ko) => dispatch(changePaymentCondition(uy, kk, ko)),
    chooseInstalment: (instalment, index) => dispatch(chooseInstalment(instalment, index)),
    taksitSec: ts => dispatch(taksitSec(ts)),
    confirmOrder: () => dispatch(confirmOrder()),
    authPaynet: (amount, agentID, card_owner_id, user_unique_id) =>
      dispatch(authenticatePaynet(amount, agentID, card_owner_id, user_unique_id)),
    setStatusBarColor: (color, trickColor) =>
      dispatch(setSafeViewBackgroundColor(color, trickColor)),
    createToken: (session_id, card_holder, pan, edMonth, edYear, cvc, instalment_key, do_3ds, ip) =>
      dispatch(
        createToken(session_id, card_holder, pan, edMonth, edYear, cvc, instalment_key, do_3ds, ip),
      ),
    createCardToken: (card_hash, card_owner_id, do_3ds, instalment_key) =>
      dispatch(createCardToken(card_hash, card_owner_id, do_3ds, instalment_key)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(CheckoutScreen);

const styles = ScaledSheet.create({
  savedCardContainer: {
    flexDirection: "column",
    flex: 1,
    borderBottomColor: "#cecece",
    paddingHorizontal: moderateScale(10, 0.5),
  },
  couponItemContainer: {
    flexDirection: "column",
    flex: 1,
    borderBottomColor: "#cecece",
    paddingVertical: moderateScale(10, 0.5),
    paddingHorizontal: moderateScale(10, 0.5),
  },
});
