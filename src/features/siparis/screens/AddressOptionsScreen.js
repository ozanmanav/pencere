import React, { Component } from "react";
import {
  View,
  ScrollView,
  SafeAreaView,
  TouchableOpacity,
  LayoutAnimation,
  Image,
  Dimensions,
  Platform,
  StatusBar,
} from "react-native";
import { AnimatedHeader, Text, RadioButton, PencereIndicator } from "@components";
import { connect } from "react-redux";
import { changeAddress, changeDeliveryType, setSafeViewBackgroundColor } from "@redux/actions";
import Device from "@common/Device";
import { scale } from "react-native-size-matters";
import { CREATE_UPDATE_SPRINGY } from "@common/LayoutAnimations";

const win = Dimensions.get("window");

class AddressOptionsScreen extends Component {
  onPressAddress = item => {
    this.props
      .changeAddress(item.AdresType, item.AdresKey)
      .then(mes => {
        if (mes === "START_ORDER_SUCCESS") this.props.navigation.navigate("CheckoutScreen");
      })
      .catch(err => {
        console.log(err);
      });
  };

  componentDidUpdate(prevProps, prevState) {
    if (
      prevProps.placeOrder.response?.SiparisVerisi?.BaslikOut.SevkiyatSekli !==
      this.props.placeOrder.response?.SiparisVerisi?.BaslikOut.SevkiyatSekli
    ) {
      LayoutAnimation.configureNext(CREATE_UPDATE_SPRINGY);
    }
  }

  render() {
    const baslikOut = this.props.placeOrder.response?.SiparisVerisi?.BaslikOut;
    return (
      <SafeAreaView style={{ backgroundColor: "#efefef" }} forceInset={{ top: "always" }}>
        <View
          style={{
            height: win.height,
            width: win.width,
            backgroundColor: "#efefef",
          }}
        >
          {
            <ScrollView
              contentContainerStyle={{
                paddingTop:
                  50 +
                  (Platform.OS === "android"
                    ? Platform.Version < 20
                      ? 0
                      : StatusBar.currentHeight
                    : 0),
                paddingBottom: 20 + (Device.isIphoneX ? 50 : 0),
              }}
            >
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate("NewAddressScreen")}
                style={{
                  margin: scale(5),
                  padding: scale(10),
                  flexDirection: "row",
                  borderRadius: 4,
                  marginTop: 10,
                  backgroundColor: "#dddddd",
                  height: 56,
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <Image
                  source={require("@assets/images/plus_bold.png")}
                  style={{ width: 18, height: 18, tintColor: "black" }}
                />
                <Text
                  style={{
                    fontFamily: "Poppins",
                    fontSize: 14,
                    marginLeft: 10,
                    marginTop: 2,
                  }}
                >
                  Yeni Adres Ekle
                </Text>
              </TouchableOpacity>
              {this.props.placeOrder.response.IlSevkSekilleri.map(item => {
                if (item.SevkiyatSekliKodu !== "01" && item.SevkiyatSekliKodu !== "03") {
                  return (
                    <TouchableOpacity
                      onPress={() => {
                        baslikOut.SevkSekli !== item.SevkiyatSekliKodu &&
                          this.props.changeDeliveryType(item.SevkiyatSekliKodu);
                        this.props.navigation.navigate("CheckoutScreen");
                      }}
                      style={{
                        backgroundColor: "white",
                        margin: scale(5),
                        padding: scale(10),
                        flexDirection: "row",
                        marginTop: 10,
                        alignItems: "center",
                      }}
                    >
                      <RadioButton
                        disabled
                        style={{ width: 16, height: 16, borderRadius: 8 }}
                        innerStyle={{
                          width: 8,
                          height: 8,
                          borderRadius: 4,
                          backgroundColor: "#E65100",
                        }}
                        selected={baslikOut.SevkSekli === item.SevkiyatSekliKodu}
                        onPress={() =>
                          baslikOut.SevkSekli !== item.SevkiyatSekliKodu &&
                          this.props.changeDeliveryType(item.SevkiyatSekliKodu)
                        }
                      />
                      <Text
                        style={{
                          marginLeft: scale(10),
                          fontFamily: "Poppins-Medium",
                          fontSize: 14,
                          color: "#d55749",
                        }}
                      >
                        {item.SevkiyatSekliAciklama}
                      </Text>
                    </TouchableOpacity>
                  );
                } else return null;
              })}
              <View
                style={{
                  flexDirection: "column",
                  backgroundColor: "white",
                  margin: scale(5),
                }}
              >
                <TouchableOpacity
                  onPress={() => {
                    baslikOut.SevkSekli !== "01" &&
                      baslikOut.SevkSekli !== "03" &&
                      this.props.changeDeliveryType("01");
                  }}
                  style={{
                    backgroundColor: "white",
                    padding: scale(10),
                    elevation: 1,
                    flexDirection: "row",
                    alignItems: "center",
                  }}
                >
                  <RadioButton
                    style={{ width: 16, height: 16, borderRadius: 8 }}
                    innerStyle={{
                      width: 8,
                      height: 8,
                      borderRadius: 4,
                      backgroundColor: "#E65100",
                    }}
                    selected={baslikOut.SevkSekli === "01" || baslikOut.SevkSekli === "03"}
                    onPress={() =>
                      baslikOut.SevkSekli !== "01" &&
                      baslikOut.SevkSekli !== "03" &&
                      this.props.changeDeliveryType("01")
                    }
                  />
                  <Text
                    style={{
                      marginLeft: 10,
                      fontFamily: "Poppins-Medium",
                      fontSize: 14,
                      color: "#d55749",
                    }}
                  >
                    Adrese Teslim
                  </Text>
                </TouchableOpacity>
                {(baslikOut.SevkSekli === "01" || baslikOut.SevkSekli === "03") && (
                  <View
                    style={{
                      flexDirection: "column",
                      borderTopColor: "#ddd",
                      borderTopWidth: 1,
                    }}
                  >
                    {this.props.navigation.state.params.items.map((item, i) => {
                      return (
                        <TouchableOpacity
                          key={i.toString()}
                          style={[
                            {
                              flexDirection: "row",
                              backgroundColor: "white",
                            },
                            { margin: scale(5), padding: scale(10) },
                          ]}
                          onPress={() => this.onPressAddress(item)}
                        >
                          <RadioButton
                            style={{
                              width: 16,
                              height: 16,
                              borderRadius: 8,
                              marginTop: 2,
                              marginRight: 5,
                            }}
                            innerStyle={{
                              width: 8,
                              height: 8,
                              borderRadius: 4,
                              backgroundColor: "black",
                            }}
                            selected={
                              baslikOut.MaliTeslimAlanAdresKodu === item.AdresKey ||
                              (baslikOut.MaliTeslimAlanAdresKodu === "" &&
                                item.AdresKey === this.props.session.info.BayiKodu)
                            }
                            onPress={() => this.onPressAddress(item)}
                          />
                          <Text
                            style={{
                              leftMargin: scale(10),
                              fontFamily: "Poppins",
                              fontSize: 14,
                            }}
                          >
                            {item.AdresLabel}
                          </Text>
                        </TouchableOpacity>
                      );
                    })}
                  </View>
                )}
              </View>
            </ScrollView>
          }

          {this.props.placeOrder.isFetching && (
            <View
              style={{
                width: win.width,
                height: win.height,
                justifyContent: "center",
                alignItems: "center",
                position: "absolute",
                top: 0,
                left: 0,
                backgroundColor: "rgba(0,0,0,0.3)",
              }}
            >
              <PencereIndicator />
            </View>
          )}
        </View>
        <AnimatedHeader
          navigation={this.props.navigation}
          title="Adres Seçenekleri"
          back={true}
          alignLeft={true}
          hideIcons={true}
        />
      </SafeAreaView>
    );
  }
}
function mapStateToProps(state) {
  return {
    placeOrder: state.placeOrder,
    session: state.session,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    changeAddress: (at, ak) => dispatch(changeAddress(at, ak)),
    changeDeliveryType: sst => dispatch(changeDeliveryType(sst)),
    setStatusBarColor: (color, trickColor) =>
      dispatch(setSafeViewBackgroundColor(color, trickColor)),
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(AddressOptionsScreen);
