import React, { Component } from "react";
import {
  View,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  SafeAreaView,
  Platform,
  StatusBar,
} from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";
import { getBucket } from "@redux/actions";
import { connect } from "react-redux";
import { Text } from "@components";

import colors from "@common/styles/colors";
import { moderateScale } from "react-native-size-matters";

const win = Dimensions.get("window");

class OrderResultScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      odemeYap: false,
    };
  }

  componentDidMount() {
    this.props.getBucket(true);
    console.log("order result", this.props.navigation.state.params);
    if (this.props.navigation.state.params.response[0]) {
      this.props.getBucket();
    }
  }

  render() {
    return (
      <SafeAreaView style={{ backgroundColor: colors.primary_dark }} forceInset={{ top: "always" }}>
        <View
          style={{
            height: win.height,
            width: win.width,
            backgroundColor: "#efefef",
            marginBottom: moderateScale(10, 0.5),
          }}
        >
          <View
            style={{
              width: win.width,
              alignItems: "center",
              padding: 10,
              paddingTop:
                10 +
                (Platform.OS === "android"
                  ? Platform.Version < 20
                    ? 0
                    : StatusBar.currentHeight
                  : 0),
              flexDirection: "row",
              justifyContent: "space-between",
              borderBottomColor: "gray",
              borderBottomWidth: 1,
              backgroundColor: colors.primary_dark,
            }}
          >
            <Text style={{ fontSize: 16, fontWeight: "bold", color: "white" }}>Sipariş Sonucu</Text>
            <TouchableOpacity
              style={{ width: 24, height: 24 }}
              onPress={() => this.props.navigation.navigate("HomeScreen")}
            >
              <Icon name="times-circle" size={24} color="rgba(255,255,255,0.7)" />
            </TouchableOpacity>
          </View>
          <View style={{ padding: 5, marginBottom: 8 }}>
            {this.props.navigation.state.params.response.map((item) => {
              return (
                <View style={styles.infoContainer}>
                  {typeof item.Mesaj1 !== "undefined" && item.Mesaj1 !== "" && (
                    <Text style={styles.msg}>{item.Mesaj1}</Text>
                  )}
                  {typeof item.Mesaj2 !== "undefined" && item.Mesaj2 !== "" && (
                    <Text style={styles.msg}>{item.Mesaj2}</Text>
                  )}
                  {typeof item.Mesaj3 !== "undefined" && item.Mesaj3 !== "" && (
                    <Text style={styles.msg}>{item.Mesaj3}</Text>
                  )}
                </View>
              );
            })}
          </View>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  infoContainer: {
    flexDirection: "column",
    backgroundColor: "#fff",
    borderWidth: 1,
    borderRadius: 2,
    borderColor: "#ddd",
    borderBottomWidth: 0,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.5,
    shadowRadius: 2,
    elevation: 1,
    padding: 10,
  },
  msg: {
    fontSize: 14,
    color: "gray",
    marginBottom: 5,
  },
});

function mapStateToProps(state) {
  return {
    placeOrder: state.placeOrder,
    homePage: state.homePage,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    getBucket: (refresh) => dispatch(getBucket(refresh)),
  };
}
export default connect(mapStateToProps, mapDispatchToProps, null, { withRef: true })(
  OrderResultScreen,
);
