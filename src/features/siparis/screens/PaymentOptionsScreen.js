import React, { Component } from "react";
import {
  View,
  ScrollView,
  TouchableOpacity,
  SafeAreaView,
  Dimensions,
  Platform,
  StatusBar,
} from "react-native";
import { AnimatedHeader, Text, PencereIndicator } from "@components";
import { connect } from "react-redux";
import { changePaymentMethod } from "@redux/actions";
import colors from "@common/styles/colors";

const win = Dimensions.get("window");

class PaymnentOptionsScreen extends Component {
  onPressItem = (item) => {
    this.props
      .changePaymentMethod(item.OdemeBicimi1)
      .then((mes) => {
        if (mes === "START_ORDER_SUCCESS") this.props.navigation.navigate("EditOrderScreen");
      })
      .catch((err) => {
        console.log(err);
      });
  };

  render() {
    return (
      <SafeAreaView style={{ backgroundColor: colors.primary_dark }} forceInset={{ top: "always" }}>
        <View
          style={{
            height: win.height,
            width: win.width,
            backgroundColor: "#efefef",
          }}
        >
          {
            <ScrollView
              contentContainerStyle={{
                height: win.height,
                paddingTop:
                  50 +
                  (Platform.OS === "android"
                    ? Platform.Version < 20
                      ? 0
                      : StatusBar.currentHeight
                    : 0),
              }}
            >
              {this.props.navigation.state.params.items.map((item) => {
                return (
                  <TouchableOpacity
                    style={[
                      Platform.OS === "ios" && {
                        borderWidth: 1,
                        borderRadius: 1,
                        borderColor: "#ddd",
                        borderBottomWidth: 0,
                        shadowColor: "#000",
                        shadowOffset: { width: 0, height: 1 },
                        shadowOpacity: 0.4,
                        shadowRadius: 1,
                      },
                      {
                        margin: 5,
                        padding: 10,
                        elevation: 1,
                        backgroundColor: "white",
                      },
                    ]}
                    onPress={() => this.onPressItem(item)}
                  >
                    <Text style={{ fontSize: 18, color: "gray" }}>{item.OdemeBicimiText}</Text>
                  </TouchableOpacity>
                );
              })}
            </ScrollView>
          }

          {this.props.placeOrder.isFetching && (
            <View
              style={{
                width: win.width,
                height: win.height,
                justifyContent: "center",
                alignItems: "center",
                position: "absolute",
                top: 0,
                left: 0,
                backgroundColor: "rgba(0,0,0,0.3)",
              }}
            >
              <PencereIndicator />
            </View>
          )}
        </View>
        <AnimatedHeader
          navigation={this.props.navigation}
          title="Ödeme Seçenekleri"
          searchBar={false}
          back={true}
          alignLeft={true}
        />
      </SafeAreaView>
    );
  }
}
function mapStateToProps(state) {
  return {
    placeOrder: state.placeOrder,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    changePaymentMethod: (ob) => dispatch(changePaymentMethod(ob)),
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(PaymnentOptionsScreen);
