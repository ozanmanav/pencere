import React, { Component } from "react";
import { View, Dimensions, SafeAreaView, Alert, Platform, StatusBar } from "react-native";
import { SimpleHeader, PencereIndicator } from "@components";
import { OrderConfirmationDialog } from "@features/siparis/components";
import Device from "@common/Device";
import colors from "@common/styles/colors";
import { confirmOrder } from "@redux/actions";
import { connect } from "react-redux";
import { getEndPoint } from "@common/Utils";
import { WebView } from "react-native-webview";

const win = Dimensions.get("window");

class OrderConfirmationScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inProgress: false,
    };
  }

  toggleDialog() {
    this.orderConfirmDialog.toggleVisibility();
  }

  render() {
    return (
      <SafeAreaView style={{ backgroundColor: colors.primary_dark }} forceInset={{ top: "always" }}>
        <View
          style={{
            height: win.height - (Device.isIphoneX ? 30 : 0),
            width: win.width,
            backgroundColor: "white",
          }}
        >
          {this.state.inProgress && (
            <View
              style={{
                width: win.width,
                height: win.height,
                justifyContent: "center",
                alignItems: "center",
                position: "absolute",
                top: 0,
                left: 0,
                backgroundColor: "rgba(0,0,0,0.3)",
                zIndex: 9999,
              }}
            >
              <PencereIndicator />
            </View>
          )}
          <OrderConfirmationDialog
            ref={(c) => (this.orderConfirmDialog = c)}
            navigation={this.props.navigation}
            confirmOrder={this.confirmOrder}
          />
          <SimpleHeader
            navigation={this.props.navigation}
            title="Ön Bilgilendirme Formu"
            contract={true}
            toggleDialog={() => this.toggleDialog()}
            style={{ backgroundColor: colors.primary_dark, borderWidth: 0 }}
            titleStyle={{ color: "white" }}
            iconColor="white"
          />
          <WebView
            ref={(c) => (this.wvModal = c)}
            startInLoadingState={true}
            scrollEnabled={true}
            style={{
              flex: 1,
              height: win.height * 0.7,
              backgroundColor: "transparent",
              marginTop:
                50 +
                (Platform.OS === "android"
                  ? Platform.Version < 20
                    ? 0
                    : StatusBar.currentHeight
                  : 0),
            }}
            contentInset={{ top: 0, left: 0, bottom: 30, right: 0 }}
          />
        </View>
      </SafeAreaView>
    );
  }

  confirmOrder = (pass) => {
    this.setState({ inProgress: true });
    fetch(
      getEndPoint("ORDER_URL") +
        "?Si=" +
        this.props.session.info.SessionID +
        "&c=siparis_saklandi" +
        "&pwd=" +
        pass,
    )
      .then((res) => res.json())
      .then((responseJson) => {
        if (responseJson.MesajTipi === "S") {
          this.props
            .confirmOrder()
            .then((result) => {
              this.setState({ inProgress: false });
              this.props.navigation.push("OrderResultScreen", {
                response: result,
              });
            })
            .catch((err) => {
              console.log(err);
              Alert.alert(
                "Hata",
                "Bir hata oluştu. Lütfen tekrar deneyiniz.",
                [{ text: "Tamam", onPress: () => console.log("Tamam") }],
                { cancelable: false },
              );
            });
        } else {
          this.setState({ inProgress: false });
          Alert.alert(
            "Hata",
            responseJson.Mesaj,
            [{ text: "Tamam", onPress: () => console.log("Tamam") }],
            {
              cancelable: false,
            },
          );
        }
      })
      .catch(() => {
        Alert.alert(
          "Hata",
          "Bir hata oluştu. Lütfen tekrar deneyiniz.",
          [{ text: "Tamam", onPress: () => console.log("Tamam") }],
          { cancelable: false },
        );
        this.setState({ inProgress: false });
      });
  };
}

function mapStateToProps(state) {
  return {
    session: state.session,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    confirmOrder: () => dispatch(confirmOrder()),
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(OrderConfirmationScreen);
