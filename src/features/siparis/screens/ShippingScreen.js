import React, { Component } from "react";
import { View, ScrollView, Image, Dimensions, SafeAreaView, TouchableOpacity } from "react-native";
import { AnimatedHeader, PencereIndicator } from "@components";
import { connect } from "react-redux";
import { changeShipment } from "@redux/actions";
import colors from "@common/styles/colors";

const win = Dimensions.get("window");

class ShippingScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      availableOptions: [],
    };
  }
  onPressItem = (item) => {
    this.props
      .changeShipment(item.UretimYeri, item.NakliyeciID)
      .then((mes) => {
        if (mes === "START_ORDER_SUCCESS") this.props.navigation.navigate("EditOrderScreen");
      })
      .catch((err) => {
        console.log(err);
      });
  };
  componentDidMount() {
    var availables = [];
    var props = this.props.navigation.state.params;

    props.items.forEach((element) => {
      if (
        element.UretimYeri === props.uretimYeri &&
        element.MinimumDesi < props.toplamDesi &&
        element.MaximumDesi > props.toplamDesi
      )
        availables.push(element);
    });
    this.setState({ availableOptions: availables });
  }

  render() {
    return (
      <SafeAreaView style={{ backgroundColor: colors.primary_dark }} forceInset={{ top: "always" }}>
        <View style={{ width: win.width, backgroundColor: "#efefef" }}>
          {
            <ScrollView contentContainerStyle={{ height: win.height }}>
              {this.state.availableOptions.map((item) => {
                return (
                  <TouchableOpacity
                    style={{
                      padding: 5,
                      borderBottomColor: "gray",
                      borderBottomWidth: 1,
                    }}
                    onPress={() => this.onPressItem(item)}
                  >
                    <Image
                      source={{
                        uri: "https://www.pencere.com/assets/img/main/" + item.NakliyeciID + ".png",
                      }}
                      style={{ width: 150, height: 50, resizeMode: "contain" }}
                    />
                  </TouchableOpacity>
                );
              })}
            </ScrollView>
          }
          {this.props.placeOrder.isFetching && (
            <View
              style={{
                width: win.width,
                height: win.height,
                justifyContent: "center",
                alignItems: "center",
                position: "absolute",
                top: 0,
                left: 0,
                backgroundColor: "rgba(0,0,0,0.3)",
              }}
            >
              <PencereIndicator />
            </View>
          )}
        </View>
        <AnimatedHeader
          navigation={this.props.navigation}
          title="Kargo Seçenekleri"
          back={true}
          alignLeft={true}
        />
      </SafeAreaView>
    );
  }
}

function mapStateToProps(state) {
  return {
    placeOrder: state.placeOrder,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    changeShipment: (uy, kf) => dispatch(changeShipment(uy, kf)),
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(ShippingScreen);
