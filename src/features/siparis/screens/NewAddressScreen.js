import React, { Component } from "react";
import {
  View,
  TextInput,
  Keyboard,
  StatusBar,
  Dimensions,
  Picker,
  TouchableOpacity,
  Platform,
  Switch,
  CheckBox,
  Alert,
  ScrollView,
  SafeAreaView,
} from "react-native";
import { connect } from "react-redux";
import { createNewAddress } from "@redux/actions";
import { SimpleHeader, BottomPicker, Text, PencereIndicator } from "@components";
import { CITIES } from "@common/Constants";
import colors from "@common/styles/colors";

const win = Dimensions.get("window");

class NewAddressScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      a1: "",
      a2: "",
      a3: "",
      a4: "",
      il: "",
      k: false,
      aad: "",
      selectedSehirIndex: 0,
      keyboardHeight: 0,
    };
    this.scrollY = 0;
  }

  componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener("keyboardDidShow", this._keyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener("keyboardDidHide", this._keyboardDidHide);
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow = e => {
    this.setState({
      keyboardVisible: true,
      keyboardHeight: e.endCoordinates.height,
    });
  };

  _keyboardDidHide = () => {
    this.setState({ keyboardVisible: false });
  };

  selectCity = (val, index) => {
    console.log("il: ", this.state.il);
    this.setState({ selectedSehirIndex: index, il: CITIES[index] });
  };

  onPressContinue = () => {
    console.log("onPressContinue", this.state);
    var isInfoMissing =
      this.state.a1 === "" ||
      this.state.a2 === "" ||
      this.state.a3 === "" ||
      this.state.a4 === "" ||
      this.state.il === "" ||
      (this.state.k && this.state.aad === "");
    if (isInfoMissing)
      Alert.alert(
        "Eksik Bilgi",
        "Lütfen gerekli alanları doldurunuz!",
        [{ text: "Tamam", onPress: () => console.log("Tamam") }],
        { cancelable: false },
      );
    else
      this.props
        .createNewAddress(
          this.state.a1,
          this.state.a2,
          this.state.a3,
          this.state.a4,
          this.state.il,
          this.state.k,
          this.state.aad,
        )
        .then(res => {
          if (res === "START_ORDER_SUCCESS") this.props.navigation.navigate("CheckoutScreen");
        })
        .catch(err => {
          console.log(err);
        });
  };

  render() {
    return (
      <SafeAreaView
        style={{ backgroundColor: colors.primary_dark, flex: 1 }}
        forceInset={{ top: "always" }}
      >
        <View style={{ flexGrow: 1, width: win.width, backgroundColor: "#efefef" }}>
          <SimpleHeader
            navigation={this.props.navigation}
            title="Yeni Adres Oluştur"
            newAddress={true}
            onPressContinue={this.onPressContinue}
            style={{ backgroundColor: colors.primary_dark, borderWidth: 0 }}
            titleStyle={{ color: "white" }}
            iconColor="white"
          />
          <ScrollView
            ref={c => (this.scrollView = c)}
            onScroll={event => {
              this.scrollY = event.nativeEvent.contentOffset.y;
            }}
            style={{ backgroundColor: "white" }}
            scrollIndicatorInsets={{ top: 50, left: 0, bottom: 0, right: 0 }}
            contentContainerStyle={{
              flexGrow: 1,
              paddingBottom: 20,
              paddingTop:
                Platform.OS === "android"
                  ? 50 + (Platform.Version < 20 ? 0 : StatusBar.currentHeight)
                  : 50,
              backgroundColor: "white",
            }}
          >
            <View style={{ flexDirection: "column", padding: 10 }}>
              <View style={{ paddingBottom: 5 }}>
                <Text style={{ fontSize: 16, color: "gray" }}>Firma / Şahıs Adı *</Text>
                <TextInput
                  value={this.state.a1}
                  onChangeText={input => this.setState({ a1: input })}
                  style={{
                    padding: 5,
                    borderColor: "#e0e0e0",
                    borderWidth: 1,
                    backgroundColor: "white",
                    fontSize: 18,
                    marginTop: 5,
                    marginBottom: 10,
                    width: win.width - 20,
                    paddingBottom: 5,
                  }}
                />
              </View>
              <View style={{ paddingBottom: 5 }}>
                <Text style={{ fontSize: 16, color: "gray" }}>Adres 1 *</Text>
                <TextInput
                  value={this.state.a2}
                  onChangeText={input => this.setState({ a2: input })}
                  style={{
                    padding: 5,
                    borderColor: "#e0e0e0",
                    borderWidth: 1,
                    backgroundColor: "white",
                    fontSize: 18,
                    marginTop: 5,
                    marginBottom: 10,
                    width: win.width - 20,
                    paddingBottom: 5,
                  }}
                />
              </View>
              <View style={{ paddingBottom: 5 }}>
                <Text style={{ fontSize: 16, color: "gray" }}>Adres 2 *</Text>
                <TextInput
                  value={this.state.a3}
                  onChangeText={input => this.setState({ a3: input })}
                  style={{
                    padding: 5,
                    borderColor: "#e0e0e0",
                    borderWidth: 1,
                    backgroundColor: "white",
                    fontSize: 18,
                    marginTop: 5,
                    marginBottom: 10,
                    width: win.width - 20,
                    paddingBottom: 5,
                  }}
                />
              </View>
              <View style={{ paddingBottom: 5 }}>
                <Text style={{ fontSize: 16, color: "gray" }}>Semt / Telefon *</Text>
                <TextInput
                  value={this.state.a4}
                  onChangeText={input => this.setState({ a4: input })}
                  style={{
                    padding: 5,
                    borderColor: "#e0e0e0",
                    borderWidth: 1,
                    backgroundColor: "white",
                    fontSize: 18,
                    marginTop: 5,
                    marginBottom: 10,
                    width: win.width - 20,
                    paddingBottom: 5,
                  }}
                />
              </View>
              <View style={{ paddingBottom: 5 }}>
                <Text style={{ fontSize: 16, color: "gray" }}>Şehir *</Text>
                {Platform.OS === "ios" ? (
                  <TouchableOpacity
                    style={{
                      padding: 5,
                      borderColor: "#e0e0e0",
                      borderWidth: 1,
                      alignItems: "center",
                      justifyContent: "center",
                      marginTop: 5,
                    }}
                    onPress={() => this.bottomPicker.openPicker()}
                  >
                    <Text>{!this.state.il === "" ? this.state.il : "Seçim Yapınız"}</Text>
                  </TouchableOpacity>
                ) : (
                  <Picker
                    selectedValue={CITIES[this.state.selectedSehirIndex]}
                    style={{ height: 50, width: win.width * 0.8 }}
                    onValueChange={(itemValue, itemIndex) => {
                      this.setState({
                        selectedSehirIndex: itemIndex,
                        il: CITIES[itemIndex],
                      });
                    }}
                  >
                    {CITIES.map((item, index) => {
                      return (
                        <Picker.Item
                          key={index}
                          value={item}
                          label={item}
                          onPress={() => this.setState({ selectedIndex: index })}
                        />
                      );
                    })}
                  </Picker>
                )}
              </View>
              <View
                style={{
                  paddingVertical: 10,
                  flexDirection: "row",
                  width: win.width - 20,
                  justifyContent: "space-between",
                  alignItems: "center",
                }}
              >
                <Text style={{ fontSize: 16, color: "gray" }}>Adresi Kaydet</Text>
                {Platform.OS === "android" ? (
                  <CheckBox value={this.state.k} onValueChange={k => this.setState({ k })} />
                ) : (
                  <Switch value={this.state.k} onValueChange={k => this.setState({ k })} />
                )}
              </View>
              {this.state.k && (
                <View style={{ paddingBottom: 5 }}>
                  <Text style={{ fontSize: 16, color: "gray" }}>Kısa Adres Adı *</Text>
                  <TextInput
                    ref={c => (this.kisaAdresAdi = c)}
                    onFocus={() => {
                      this.kisaAdresAdi.measure((fx, fy, width, height, px, py) => {
                        this.setState({ scrollBefore: this.scrollY });
                        this.scrollView.scrollTo({
                          y: this.scrollY + py - (win.height - this.state.keyboardHeight - 50),
                        });
                      });
                    }}
                    onBlur={() => {
                      this.scrollView.scrollTo({ y: this.state.scrollBefore });
                    }}
                    value={this.state.aad}
                    onChangeText={input => this.setState({ aad: input })}
                    style={{
                      padding: 5,
                      borderColor: "#e0e0e0",
                      borderWidth: 1,
                      backgroundColor: "white",
                      fontSize: 18,
                      marginTop: 5,
                      marginBottom: 10,
                      width: win.width - 20,
                      paddingBottom: 5,
                    }}
                  />
                </View>
              )}
            </View>
            <BottomPicker
              ref={c => (this.bottomPicker = c)}
              items={CITIES}
              onPressOK={this.selectCity}
            />
            {this.props.placeOrder.isFetching && (
              <View
                style={{
                  width: win.width,
                  height: win.height,
                  justifyContent: "center",
                  alignItems: "center",
                  position: "absolute",
                  top: 0,
                  left: 0,
                  backgroundColor: "rgba(0,0,0,0.3)",
                }}
              >
                <PencereIndicator />
              </View>
            )}
          </ScrollView>
        </View>
      </SafeAreaView>
    );
  }
}

function mapStateToProps(state) {
  return {
    placeOrder: state.placeOrder,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    createNewAddress: (a1, a2, a3, a4, il, k, aad) =>
      dispatch(createNewAddress(a1, a2, a3, a4, il, k, aad)),
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(NewAddressScreen);
