import React, { Component } from "react";
import {
  Dimensions,
  View,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  Image,
  SafeAreaView,
  Alert,
  Platform,
  StatusBar,
} from "react-native";
import { Text, PencereIndicator } from "@components";
import { KalemItem } from "@features/siparis/components";

import { connect } from "react-redux";
import colors from "@common/styles/colors";

const win = Dimensions.get("window");

class EditOrderScreen extends Component {
  render() {
    if (this.props.placeOrder.response.SiparisVerisi) {
      const baslik = this.props.placeOrder.response.SiparisVerisi.BaslikOut;
      const baslik2 = this.props.placeOrder.response.SiparisVerisi.IlBaslikUyOut[0];
      const toplamlar = this.props.placeOrder.response.SiparisVerisi.IlBaslikToplam;
      const kargoFirmalari = this.props.placeOrder.response.IlKargoFirmalari;
      const odemeBicimleri = this.props.placeOrder.response.IdOdemeBicimleri;
      const adresler = this.props.placeOrder.response.IlSubeler;

      return (
        <SafeAreaView
          style={{ backgroundColor: colors.primary_dark }}
          forceInset={{ top: "always" }}
        >
          <View style={{ width: win.width, backgroundColor: "#efefef" }}>
            <View style={styles.header}>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.goBack();
                  }}
                  style={{ padding: 0 }}
                >
                  <Image
                    style={[{ width: 20, height: 20, tintColor: "white" }]}
                    source={require("@assets/images/left-arrow-mini.png")}
                  />
                </TouchableOpacity>
                <Text
                  style={{
                    color: colors.white,
                    fontSize: 18,
                    fontWeight: "bold",
                    marginLeft: 10,
                  }}
                >
                  Sipariş Detayı
                </Text>
              </View>
              <TouchableOpacity
                style={{ padding: 5 }}
                onPress={() => this.props.navigation.push("OrderConfirmationScreen")}
              >
                <Text style={{ color: "white", fontSize: 16, fontWeight: "bold" }}>
                  Siparişi Onayla
                </Text>
              </TouchableOpacity>
            </View>
            <ScrollView
              contentContainerStyle={{
                paddingTop: 10,
                paddingBottom:
                  130 +
                  (Platform.OS === "android"
                    ? Platform.Version < 20
                      ? 0
                      : StatusBar.currentHeight
                    : 0),
              }}
            >
              <View style={{ padding: 5 }}>
                <View style={[styles.infoContainer, { marginBottom: 10 }]}>
                  <View
                    style={{
                      padding: 5,
                      borderBottomColor: "gray",
                      borderBottomWidth: 1,
                    }}
                  >
                    <Text
                      style={{
                        fontSize: 16,
                        color: colors.light_black,
                        fontWeight: "500",
                      }}
                    >
                      Adres Bilgisi
                    </Text>
                  </View>
                  <View style={{ padding: 5 }}>
                    <Text>
                      {baslik.MaliTeslimAlanA1 +
                        " " +
                        baslik.MaliTeslimAlanA2 +
                        " " +
                        baslik.MaliTeslimAlanA3}
                    </Text>
                    <Text>{baslik.MaliTeslimAlanA4 + " " + baslik.MaliTeslimAlanIl}</Text>
                  </View>
                  <TouchableOpacity
                    style={{ flexDirection: "row", alignItems: "center" }}
                    onPress={() =>
                      this.props.navigation.navigate({
                        key: "AddressOptionsScreen",
                        routeName: "AddressOptionsScreen",
                        params: { items: adresler },
                      })
                    }
                  >
                    <Text style={{ fontSize: 16, color: "#2196F3", padding: 5 }}>Değiştir</Text>
                    <Image
                      source={require("@assets/images/arrow-right.png")}
                      style={{
                        width: 16,
                        height: 16,
                        tintColor: "#2294FA",
                        opacity: 0.8,
                      }}
                    />
                  </TouchableOpacity>
                </View>
                <View style={[styles.infoContainer, { marginBottom: 10 }]}>
                  <View
                    style={{
                      padding: 5,
                      borderBottomColor: "gray",
                      borderBottomWidth: 1,
                    }}
                  >
                    <Text
                      style={{
                        fontSize: 16,
                        color: colors.light_black,
                        fontWeight: "500",
                      }}
                    >
                      Ödeme Şekli
                    </Text>
                  </View>
                  <Text style={{ padding: 5 }}>{baslik.SiparisOdemeKosuluText}</Text>

                  <TouchableOpacity
                    style={{ flexDirection: "row", alignItems: "center" }}
                    onPress={() =>
                      this.props.navigation.navigate({
                        key: "PaymentOptionsScreen",
                        routeName: "PaymentOptionsScreen",
                        params: { items: odemeBicimleri },
                      })
                    }
                  >
                    <Text style={{ fontSize: 16, color: "#2196F3", padding: 5 }}>Değiştir</Text>
                    <Image
                      source={require("@assets/images/arrow-right.png")}
                      style={{
                        width: 16,
                        height: 16,
                        tintColor: "#2294FA",
                        opacity: 0.8,
                      }}
                    />
                  </TouchableOpacity>
                </View>
                <View style={[styles.infoContainer, { marginBottom: 10 }]}>
                  <View
                    style={{
                      padding: 5,
                      borderBottomColor: "gray",
                      borderBottomWidth: 1,
                    }}
                  >
                    <Text
                      style={{
                        fontSize: 16,
                        color: colors.light_black,
                        fontWeight: "500",
                      }}
                    >
                      Teslimat Bilgileri
                    </Text>
                  </View>
                  <Text style={{ padding: 5 }}>{"Çıkış Tarihi: " + baslik2.TeslimatTarihi}</Text>
                  <View style={{ flexDirection: "row" }}>
                    <Text style={{ padding: 5, marginRight: 5 }}>{"Kargo Firması:"}</Text>
                    <Image
                      source={{
                        uri:
                          "https://www.pencere.com/assets/img/main/" + baslik2.Nakliyeci + ".png",
                      }}
                      style={{ width: 96, height: 24, resizeMode: "contain" }}
                    />
                  </View>
                  <TouchableOpacity
                    style={{ flexDirection: "row", alignItems: "center" }}
                    onPress={() => {
                      if (kargoFirmalari.length > 0)
                        this.props.navigation.navigate({
                          key: "ShippingScreen",
                          routeName: "ShippingScreen",
                          params: {
                            items: kargoFirmalari,
                            uretimYeri: baslik2.UretimYeri,
                            toplamDesi: baslik2.ToplamDesi,
                          },
                        });
                      else Alert.alert("Uygun kargo seçeneği bulunamadı.");
                    }}
                  >
                    <Text style={{ fontSize: 16, color: "#2196F3", padding: 5 }}>Değiştir</Text>
                    <Image
                      source={require("@assets/images/arrow-right.png")}
                      style={{
                        width: 16,
                        height: 16,
                        tintColor: "#2294FA",
                        opacity: 0.8,
                      }}
                    />
                  </TouchableOpacity>
                </View>
                <View style={[styles.infoContainer]}>
                  <View
                    style={{
                      padding: 5,
                      borderBottomColor: "gray",
                      borderBottomWidth: 1,
                    }}
                  >
                    <Text
                      style={{
                        fontSize: 16,
                        color: colors.light_black,
                        fontWeight: "500",
                      }}
                    >
                      Toplamlar
                    </Text>
                  </View>
                  <View
                    style={{
                      flexDirection: "row",
                      padding: 5,
                      flex: 1,
                      justifyContent: "space-between",
                    }}
                  >
                    <View style={{ flexDirection: "column" }}>
                      {toplamlar.map((item) => {
                        return <Text>{item.ToplamAciklama}</Text>;
                      })}
                    </View>
                    <View style={{ flexDirection: "column" }}>
                      {toplamlar.map((item) => {
                        return <Text>{item.ToplamTutar + " " + item.ToplamPb}</Text>;
                      })}
                    </View>
                  </View>
                </View>
                <View style={[styles.infoContainer, { marginTop: 10 }]}>
                  <View
                    style={{
                      padding: 5,
                      borderBottomColor: "gray",
                      borderBottomWidth: 1,
                      marginBottom: 5,
                    }}
                  >
                    <Text
                      style={{
                        fontSize: 16,
                        color: colors.light_black,
                        fontWeight: "500",
                      }}
                    >
                      Ürünler
                    </Text>
                  </View>
                  {baslik2.IlKalemlerOut.map((item, index) => {
                    return (
                      <View
                        style={{
                          borderBottomColor: "#dfdfdf",
                          borderBottomWidth: index < baslik2.IlKalemlerOut.length - 1 ? 1 : 0,
                        }}
                      >
                        <KalemItem
                          item={item}
                          IdUrunExt={this.props.placeOrder.response.IdUrunExt}
                          navigation={this.props.navigation}
                        />
                      </View>
                    );
                  })}
                </View>
              </View>
            </ScrollView>
            {!this.props.placeOrder.response.SiparisVerisi && (
              <View
                style={{
                  width: win.width,
                  height: win.height - 50,
                  justifyContent: "center",
                  alignItems: "center",
                  position: "absolute",
                  top: 0,
                  left: 0,
                }}
              >
                <PencereIndicator />
              </View>
            )}
          </View>
        </SafeAreaView>
      );
    } else return null;
  }
}

function mapStateToProps(state) {
  return {
    placeOrder: state.placeOrder,
    cart: state.cart,
  };
}

export default connect(mapStateToProps, null)(EditOrderScreen);

const styles = StyleSheet.create({
  infoContainer: {
    flexDirection: "column",
    backgroundColor: "#fff",
    paddingVertical: 5,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.3,
    shadowRadius: 2,
    elevation: 1,
    padding: 10,
  },
  header: {
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 4 },
    shadowOpacity: 0.2,
    shadowRadius: 2,
    elevation: 1,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingHorizontal: 10,
    backgroundColor: colors.primary_dark,
    width: win.width,
    height:
      50 + (Platform.OS === "android" ? (Platform.Version < 20 ? 0 : StatusBar.currentHeight) : 0),
    paddingTop:
      Platform.OS === "android" ? (Platform.Version < 20 ? 0 : StatusBar.currentHeight) : 0,
    zIndex: 999,
  },
});
