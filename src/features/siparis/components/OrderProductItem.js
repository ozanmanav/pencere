import React, { Component } from "react";
import { View, TouchableOpacity, Image, Dimensions } from "react-native";

import { Text, SmartImage } from "@components";
import { ScaledSheet, scale, moderateScale } from "react-native-size-matters";
const win = Dimensions.get("window");
const SPACE_HORIZONTAL = win.width / 25;

export default class OrderProductItem extends Component {
  increaseNumber = (skk, num) => {
    this.props.setNumber(skk, num + 1);
  };
  decreaseNumber = (skk, num) => {
    this.props.setNumber(skk, num - 1);
  };
  constructor(props) {
    super(props);
    this.IdUrunExt = props.IdUrunExt[this.props.item.KalemUrunKodu];
    this.state = {
      number: 0,
    };
  }

  componentDidMount() {
    this.setState({
      number: this.props.item.number,
    });
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps?.item?.number !== this.props?.item.number) {
      this.setState({ number: this.props.item.number });
    }
  }

  render() {
    return (
      <TouchableOpacity
        onPress={() =>
          this.props.navigation.navigate({
            key: "main",
            routeName: "ProductDetailsScreen",
            params: {
              UrunKodu: this.props.item.KalemUrunKodu,
              SeriNo: this.props.item.KalemSeriNo,
              isFavorite: false,
              latestScreen: "CheckoutScreen",
            },
          })
        }
        style={{
          flexDirection: "row",
          backgroundColor: this.props.item.UrunCrossSell ? "rgba(0,255,255,0.1)" : "white",
          borderRadius: 3,
          padding: SPACE_HORIZONTAL,
          borderBottomColor: "#dadada",
          borderBottomWidth: 1,
        }}
      >
        <View style={styles.prdImgContainer}>
          <SmartImage
            style={styles.prdImg}
            resizeMode="contain"
            source={{ uri: this.IdUrunExt.ResimUrl }}
          />
        </View>
        <View style={{ flexDirection: "row", marginLeft: 8, flex: 1 }}>
          <View
            style={{
              flexDirection: "column",
              flex: 1,
              justifyContent: this.props.item.UrunTeminEdilememekte
                ? "space-between"
                : "flex-start",
            }}
          >
            <View>
              <View style={{ paddingTop: 0 }}>
                <Text
                  numberOfLines={2}
                  ellipsizeMode="tail"
                  style={{
                    fontFamily: "Poppins-Medium",
                    fontSize: 11,
                    textAlign: "left",
                    paddingRight: 5,
                    color: "black",
                  }}
                >
                  {this.IdUrunExt.UrunAciklama}
                </Text>
                <Text style={{ fontFamily: "Poppins", fontSize: 11, color: "gray" }}>
                  {this.props.item.KalemUrunMiktari + "  adet"}
                </Text>
              </View>
            </View>
            {
              <View style={{ flexDirection: "column", marginTop: moderateScale(5, 0.5) }}>
                {typeof this.IdUrunExt !== "undefined" &&
                this.IdUrunExt.IlVadeSecenekleri.length > 0 ? (
                  <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <TouchableOpacity
                      onPress={() =>
                        this.props.onPressVade(
                          this.IdUrunExt.IlVadeSecenekleri,
                          this.props.item.KalemOdemeKosulu,
                          this.props.item,
                        )
                      }
                      style={{
                        flexDirection: "row",
                        alignItems: "center",
                        paddingVertical: scale(2, 0.5),
                        paddingHorizontal: moderateScale(10, 0.5),
                        borderColor: "#c9c9c9",
                        borderWidth: 1,
                        borderRadius: 3,
                      }}
                    >
                      <Text style={{ fontSize: 11, fontFamily: "Poppins" }}>
                        {this.props.item.KalemOdemeKosuluText}
                      </Text>
                      <Image
                        source={require("@assets/images/down-arrow.png")}
                        style={{
                          marginLeft: scale(5),
                          width: 14,
                          height: 14,
                          tintColor: "#cecece",
                        }}
                      />
                    </TouchableOpacity>
                  </View>
                ) : (
                  <Text
                    style={{
                      fontFamily: "Poppins",
                      fontSize: 11,
                      textAlign: "left",
                      color: "#343434",
                    }}
                  >{`Birim Fiyatı: ${Number(this.props.item.KalemBirimTutar)
                    .toFixed(2)
                    .toString()
                    .replace(".", ",")} ${this.props.item.KalemPb}`}</Text>
                )}
              </View>
            }
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = ScaledSheet.create({
  prdImg: {
    width: "60@s",
    height: "60@s",
    resizeMode: "contain",
  },
  prdImgContainer: {
    borderColor: "#dddddd",
    borderWidth: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    height: "70@s",
    width: "70@s",
  },
});
