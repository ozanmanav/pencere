import React, { PureComponent } from "react";
import { View, TextInput } from "react-native";
import { moderateScale } from "react-native-size-matters";

var numberRegex = new RegExp("^[0-9]+$");

export default class CreditCardInput extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      input1: "",
      input2: "",
      input3: "",
      input4: "",
    };
  }

  getCCN() {
    let ccn = this.state.input1 + this.state.input2 + this.state.input3 + this.state.input4;
    return ccn;
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      prevState.input1 !== this.state.input1 ||
      prevState.input2 !== this.state.input2 ||
      prevState.input3 !== this.state.input3 ||
      prevState.input4 !== this.state.input4
    ) {
      this.props.onChange(this.getCCN());
    }
  }

  render() {
    return (
      <View
        style={{
          flexDirection: "row",
          flex: 1,
          backgroundColor: "white",
          justifyContent: "space-evenly",
          alignItems: "center",
          height: moderateScale(46),
          marginTop: moderateScale(10),
          borderColor: "#c3c3c3",
          borderWidth: 1,
          borderRadius: 3,
        }}
      >
        <TextInput
          ref={(c) => {
            this.input1 = c;
          }}
          collapsable={false}
          onFocus={() => {
            this.input1.measure((fx, fy, width, height, px, py) => {
              this.props.scrollToFocusedInput(py);
            });
          }}
          value={this.state.input1}
          maxLength={4}
          textContentType="creditCardNumber"
          onChangeText={(text) => {
            if (text.length === 4) {
              this.input2.focus();
            }
            this.setState({ input1: String(text) });
          }}
          onKeyPress={({ nativeEvent: { key: keyValue } }) => {
            if (numberRegex.test(keyValue) && this.state.input1.length === 4) {
              this.input2.focus();
            }
          }}
          placeholderTextColor="#c8c8c8"
          placeholder="_ _ _ _"
          style={{
            fontSize: moderateScale(14, 0.4),
            minWidth: moderateScale(45, 0.5),
            height: moderateScale(40, 0.5),
          }}
          keyboardType="numeric"
          returnKeyType="done"
        />
        <TextInput
          ref={(c) => {
            this.input2 = c;
          }}
          collapsable={false}
          onFocus={() => {
            this.input2.measure((fx, fy, width, height, px, py) => {
              this.props.scrollToFocusedInput(py);
            });
          }}
          value={this.state.input2}
          maxLength={4}
          textContentType="creditCardNumber"
          onChangeText={(text) => {
            if (text.length === 4) {
              this.input3.focus();
            }
            this.setState({ input2: String(text) });
          }}
          onKeyPress={({ nativeEvent: { key: keyValue } }) => {
            if (keyValue === "Backspace") {
              if (this.state.input2.length === 0) {
                this.input1.focus();
              }
            } else if (numberRegex.test(keyValue) && this.state.input2.length === 4) {
              this.input3.focus();
            }
          }}
          placeholderTextColor="#c8c8c8"
          placeholder="_ _ _ _"
          style={{
            fontSize: moderateScale(14, 0.4),
            minWidth: moderateScale(45, 0.5),
            height: moderateScale(40, 0.5),
          }}
          keyboardType="numeric"
          returnKeyType="done"
        />
        <TextInput
          ref={(c) => {
            this.input3 = c;
          }}
          collapsable={false}
          onFocus={() => {
            this.input3.measure((fx, fy, width, height, px, py) => {
              this.props.scrollToFocusedInput(py);
            });
          }}
          value={this.state.input3}
          maxLength={4}
          textContentType="creditCardNumber"
          onChangeText={(text) => {
            if (text.length === 4) {
              this.input4.focus();
            }
            this.setState({ input3: String(text) });
          }}
          onKeyPress={({ nativeEvent: { key: keyValue } }) => {
            if (keyValue === "Backspace") {
              if (this.state.input3.length === 0) {
                this.input2.focus();
              }
            }
            if (numberRegex.test(keyValue) && this.state.input3.length === 4) {
              this.input4.focus();
            }
          }}
          placeholderTextColor="#c8c8c8"
          placeholder="_ _ _ _"
          style={{
            fontSize: moderateScale(14, 0.4),
            minWidth: moderateScale(45, 0.5),
            height: moderateScale(40, 0.5),
          }}
          keyboardType="numeric"
          returnKeyType="done"
        />
        <TextInput
          ref={(c) => {
            this.input4 = c;
          }}
          collapsable={false}
          onFocus={() => {
            this.input4.measure((fx, fy, width, height, px, py) => {
              this.props.scrollToFocusedInput(py);
            });
          }}
          value={this.state.input4}
          maxLength={4}
          textContentType="creditCardNumber"
          onChangeText={(text) => {
            this.setState({ input4: String(text) });
          }}
          onKeyPress={({ nativeEvent: { key: keyValue } }) => {
            if (keyValue === "Backspace") {
              if (this.state.input4.length === 0) {
                this.input3.focus();
              }
            }
          }}
          placeholderTextColor="#c8c8c8"
          placeholder="_ _ _ _"
          style={{
            fontSize: moderateScale(14, 0.4),
            minWidth: moderateScale(45, 0.5),
            height: moderateScale(40, 0.5),
          }}
          keyboardType="numeric"
          returnKeyType="done"
        />
      </View>
    );
  }
}
