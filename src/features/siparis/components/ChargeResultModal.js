import React, { Component } from "react";
import { Animated, View, StyleSheet, Dimensions, TouchableOpacity } from "react-native";
import { Text, ModalBox } from "@components";
import Icon from "react-native-vector-icons/FontAwesome";
import { setCart, getBucket } from "@redux/actions";
import { connect } from "react-redux";
import colors from "@common/styles/colors";

const deviceWidth = Dimensions.get("window").width;
const deviceHeight = Dimensions.get("window").height;
const win = Dimensions.get("window");

var self;

class ChargeResultModal extends Component {
  animVal = new Animated.Value(0);
  constructor(props) {
    super(props);
    self = this;
    this.state = {
      isVisible: false,
      scrollX: 0,
      number: "1",
      progress: false,
    };
  }

  toggleVisibility = () => {
    this.modalBox.toggleVisibility();
    this.props.getBucket(true);
  };

  handleScroll = function (event) {
    self.setState({ scrollX: event.nativeEvent.contentOffset.x });
    Animated.event([{ nativeEvent: { contentOffset: { x: this.animVal } } }]);
  };

  decreaseNumber = () => {
    this.setState({
      number: Number(this.state.number) > 1 ? (Number(this.state.number) - 1).toString() : "1",
    });
  };

  increaseNumber = () => {
    this.setState({
      number: (Number(this.state.number) + 1).toString(),
    });
  };

  closeAndResetStack() {
    this.setState({ isVisible: false });
    // const resetAction = StackActions.reset({
    //     index: 0,
    //     actions: [NavigationActions.navigate({ routeName: 'HomeScreenDrawerNavigator' })],
    // });
    // this.props.navigation.dispatch(resetAction);
    this.props.navigation.navigate("HomeScreen");
  }

  goToOrders() {
    this.setState({ isVisible: false }, () => {
      this.props.navigation.navigate({ key: "OrdersScreen", routeName: "OrdersScreen" });
    });
  }

  render() {
    return (
      <ModalBox
        ref={(c) => (this.modalBox = c)}
        animationType="none"
        presentationStyle="overFullScreen"
        statusBarHidden={false}
      >
        {this.props.chargeResult && (
          <View style={styles.container}>
            <View
              style={{
                width: deviceWidth,
                alignItems: "center",
                padding: 10,
                flexDirection: "row",
                justifyContent: "space-between",
                borderBottomColor: "gray",
                borderBottomWidth: 1,
                backgroundColor: colors.primary_dark,
              }}
            >
              <Text style={{ fontSize: 18, color: "white" }}>Ödeme Sonucu</Text>
              <TouchableOpacity
                style={{ width: 28, height: 28 }}
                onPress={() => this.closeAndResetStack()}
              >
                <Icon name="times-circle" size={28} color="rgba(0,0,0,0.5)" />
              </TouchableOpacity>
            </View>

            <View style={styles.infoContainer}>
              {this.props.chargeResult.Mesaj1 && (
                <Text
                  style={{
                    fontFamily: "Poppins",
                    color: "gray",
                    fontSize: 14,
                    textAlign: "center",
                    marginBottom: 5,
                  }}
                >
                  {this.props.chargeResult.Mesaj1}
                </Text>
              )}
              {this.props.chargeResult.Mesaj2 && (
                <Text
                  style={{
                    fontFamily: "Poppins",
                    color: "gray",
                    fontSize: 14,
                    textAlign: "center",
                    marginBottom: 5,
                  }}
                >
                  {this.props.chargeResult.Mesaj2}
                </Text>
              )}
              <Text
                style={{
                  fontFamily: "Poppins",
                  color: "gray",
                  fontSize: 14,
                  textAlign: "center",
                  marginBottom: 5,
                }}
              >
                {"Siparişinizin durumunu siparişlerim sayfasından görebilirsiniz."}
              </Text>
            </View>
            <TouchableOpacity
              style={{
                padding: 8,
                backgroundColor: colors.primary_dark,
                width: win.width * 0.75,
                marginTop: 10,
                alignItems: "center",
                borderRadius: 3,
              }}
              onPress={() => this.goToOrders()}
            >
              <Text style={{ fontFamily: "Poppins-Medium", color: "white", fontSize: 16 }}>
                Siparişlerim
              </Text>
            </TouchableOpacity>
          </View>
        )}
      </ModalBox>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: win.width,
    height: deviceHeight,
    backgroundColor: "white",
    alignItems: "center",
  },
  infoContainer: {
    flexDirection: "column",
    backgroundColor: "#fff",
    borderWidth: 1,
    borderRadius: 1,
    borderColor: "#ddd",
    borderBottomWidth: 0,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.2,
    shadowRadius: 2,
    elevation: 1,
    padding: 10,
    marginTop: 10,
    width: win.width * 0.9,
    alignItems: "center",
  },
});

function mapStateToProps(state) {
  return {
    homePage: state.homePage,
    placeOrder: state.placeOrder,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    setCart: (cart) => dispatch(setCart(cart)),
    getBucket: (refresh) => dispatch(getBucket(refresh)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps, null, { withRef: true })(
  ChargeResultModal,
);
