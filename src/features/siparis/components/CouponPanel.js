import React, { Component } from "react";
import { View, TouchableOpacity, TextInput, Image } from "react-native";
import { Text } from "@components";
import { getWidth } from "@common/Utils";
import { moderateScale } from "react-native-size-matters";
import colors from "@common/styles/colors";

const SPACE_HORIZONTAL = getWidth() / 25;

export default class CouponPanel extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return this.props?.MobileKuponlar?.MobileKuponList ? (
      <View
        style={{
          flexDirection: "column",
          flex: 1,
          marginHorizontal: SPACE_HORIZONTAL,
          backgroundColor: "white",
          padding: SPACE_HORIZONTAL - 5,
          borderRadius: 4,
          marginTop: 10,
        }}
      >
        {this.props?.MobileKuponlar?.MobileKuponList &&
          this.props?.MobileKuponlar?.MobileKuponList.length > 0 && (
            <View style={{ flexDirection: "column" }}>
              {this.props?.MobileKuponlar?.MobileKuponList &&
                this.props?.MobileKuponlar?.MobileKuponList.length > 0 && (
                  <TouchableOpacity
                    style={{
                      flexDirection: "row",
                      alignItems: "center",
                      marginBottom: moderateScale(5, 0.5),
                    }}
                    onPress={this.props.toggleKuponList}
                  >
                    <Image
                      source={require("@assets/images/coupon.png")}
                      style={{
                        marginRight: moderateScale(5, 0.5),
                        width: moderateScale(16.5),
                        height: moderateScale(16, 0.5),
                        tintColor: colors.price_color,
                        resizeMode: "contain",
                      }}
                    />
                    <Text style={{ color: colors.price_color, fontFamily: "Poppins-Medium" }}>
                      Kupon Seç
                    </Text>

                    <Image
                      source={require("@assets/images/arrow-right.png")}
                      style={{
                        marginLeft: moderateScale(5, 0.5),
                        width: moderateScale(13, 0.3),
                        height: moderateScale(13, 0.3),
                        tintColor: colors.price_color,
                        resizeMode: "contain",
                      }}
                    />
                  </TouchableOpacity>
                )}
              {this.props?.MobileKuponlar?.MobileKuponList && (
                <View style={{ flexDirection: "row", alignItems: "center" }}>
                  <TextInput
                    style={{
                      paddingLeft: moderateScale(5, 0.5),
                      height: moderateScale(40, 0.5),
                      borderColor: "#dedede",
                      borderWidth: 1,
                      borderRadius: 4,
                      flex: 1,
                      marginRight: moderateScale(5, 0.5),
                    }}
                    value={this.props.couponCode}
                    placeholder="Kupon kodunu giriniz"
                    placeholderTextColor="gray"
                    onChangeText={this.props.setCouponCode}
                  />
                  <TouchableOpacity
                    style={{
                      borderRadius: 4,
                      alignItems: "center",
                      justifyContent: "center",
                      backgroundColor: colors.light_green,
                      height: moderateScale(40, 0.5),
                      width: moderateScale(100, 0.5),
                    }}
                    onPress={() => this.props.useCoupon(this.props.couponCode)}
                  >
                    <Text
                      fontScale={0.5}
                      style={{ color: colors.white, fontFamily: "Poppins-Medium", fontSize: 13 }}
                    >
                      Kupon Kullan
                    </Text>
                  </TouchableOpacity>
                </View>
              )}
            </View>
          )}
        {this.props?.MobileKuponlar?.Mesaj != null && this.props?.MobileKuponlar?.Mesaj !== "" && (
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              alignSelf: "center",
              marginTop: moderateScale(10, 0.5),
            }}
          >
            <Image
              source={require("@assets/images/info_white.png")}
              style={{
                width: moderateScale(16, 0.5),
                height: moderateScale(16, 0.5),
                tintColor: "#A88F80",
                resizeMode: "contain",
                marginRight: moderateScale(8, 0.5),
                marginLeft: moderateScale(3, 0.5),
              }}
            />
            <Text
              fontScale={0.5}
              style={{ fontFamily: "Poppins", fontSize: 12, color: "gray", flex: 1 }}
            >
              {this.props?.MobileKuponlar?.Mesaj}
            </Text>
          </View>
        )}
      </View>
    ) : null;
  }
}
