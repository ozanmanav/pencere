import React, { Component } from "react";
import { TouchableOpacity, Image, View } from "react-native";
import { Text } from "@components";

export default class CargoCompanyItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      imgFailed: false,
    };
  }

  componentDidMount() {
    Image.getSize(
      "https://www.pencere.com/assets/img/main/" + this.props.item.NakliyeciID + ".png",
      () => {},
      () => {
        this.setState({ imgFailed: true });
      },
    );
  }

  render() {
    return (
      <TouchableOpacity
        onPress={() => this.props.onPress(this.props.item)}
        style={[
          this.props.style,
          {
            borderColor: this.props.item.NakliyeciID.includes(this.props.Nakliyeci)
              ? "#d6421e"
              : "#c9c9c9",
          },
        ]}
      >
        {this.props.item.NakliyeciID.includes(this.props.Nakliyeci) && (
          <View
            style={{
              heigth: 16,
              width: 16,
              alignItems: "center",
              justifyContent: "center",
              backgroundColor: "#d6421e",
              borderBottomLeftRadius: 3,
              position: "absolute",
              top: 0,
              right: 0,
            }}
          >
            <Image
              source={require("@assets/images/checked.png")}
              style={{
                width: 8,
                height: 10,
                tintColor: "white",
                resizeMode: "contain",
              }}
            />
          </View>
        )}
        {!this.state.imgFailed ? (
          <View
            style={{
              flexDirection: "column",
              alignItems: "center",
              justifyContent: "center",
              padding: 5,
            }}
          >
            <Image
              source={{
                uri:
                  "https://www.pencere.com/assets/img/main/" + this.props.item.NakliyeciID + ".png",
              }}
              style={{
                marginLeft: 10,
                width: 60,
                height: 20,
                resizeMode: "contain",
              }}
            />
            <Text
              numberOfLines={1}
              style={{
                marginLeft: 8,
                fontFamily: "Poppins",
                fontSize: 10,
                textAlign: "center",
                color: "gray",
              }}
            >
              {this.props.item.NakliyeciAdi}
            </Text>
          </View>
        ) : (
          <Text
            numberOfLines={1}
            style={{
              marginLeft: 8,
              fontFamily: "Poppins",
              fontSize: 10,
              textAlign: "center",
              color: "gray",
            }}
          >
            {this.props.item.NakliyeciAdi}
          </Text>
        )}
      </TouchableOpacity>
    );
  }
}
