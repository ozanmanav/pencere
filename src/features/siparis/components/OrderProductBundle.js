import React, { Component } from "react";
import { View, TouchableOpacity, Image, Dimensions } from "react-native";

import { Text, SmartImage } from "@components";
import { scale, ScaledSheet, moderateScale } from "react-native-size-matters";

const win = Dimensions.get("window");
const HORIZONTAL_PADDING = win.width / 25;

export default class OrderProductBundle extends Component {
  increaseNumber = (skk, num) => {
    this.props.setNumber(skk, num + 1);
  };
  decreaseNumber = (skk, num) => {
    this.props.setNumber(skk, num - 1);
  };
  constructor(props) {
    super(props);
    this.state = {
      image: "",
      imageError: false,
    };
  }
  componentDidMount() {
    this.setState({
      number: this.props.item.number,
      image: { uri: this.props.item?.UrunResimleri?.DefaultResim?.Connact?.URL },
    });
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps?.item?.number !== this.props?.item.number) {
      this.setState({ number: this.props.item.number });
    }
  }

  render() {
    const ext = this.props.IdUrunExt[this.props.item.KalemUrunKodu];
    return (
      <TouchableOpacity
        onPress={() =>
          this.props.navigation.navigate({
            key: "checkout",
            routeName: "ProductDetailsScreen",
            params: {
              UrunKodu: this.props.item.KalemUrunKodu,
              SeriNo: this.props.item.UrunSeriNo,
              isFavorite: false,
            },
          })
        }
        style={{
          backgroundColor: "white",
          borderRadius: 3,
          padding: HORIZONTAL_PADDING,
          borderBottomColor: "#dadada",
          borderBottomWidth: 1,
        }}
      >
        <View style={{ flexDirection: "row" }}>
          <View style={styles.prdImgContainer}>
            <SmartImage
              style={styles.prdImg}
              resizeMode="contain"
              source={{ uri: this.props.IdUrunExt[this.props.item.KalemUrunKodu].ResimUrl }}
            />
          </View>
          {
            <View style={{ flexDirection: "row", flex: 1, paddingLeft: 8 }}>
              <View style={{ flexDirection: "column" }}>
                <Text
                  numberOfLines={2}
                  ellipsizeMode="tail"
                  style={{
                    fontFamily: "Poppins-Medium",
                    fontSize: 11,
                    textAlign: "left",
                    paddingRight: 5,
                  }}
                >
                  {this.props.IdUrunExt[this.props.item.KalemUrunKodu].UrunAciklama}
                </Text>
                <Text style={{ fontFamily: "Poppins", fontSize: 11, color: "gray" }}>
                  {this.props.item.KalemUrunMiktari + "  adet"}
                </Text>
                {typeof ext !== "undefined" && ext.IlVadeSecenekleri.length > 0 ? (
                  <View
                    style={{
                      flexDirection: "row",
                      alignItems: "center",
                      marginTop: moderateScale(5, 0.5),
                    }}
                  >
                    <TouchableOpacity
                      onPress={() =>
                        this.props.onPressVade(
                          ext.IlVadeSecenekleri,
                          this.props.item.KalemOdemeKosulu,
                          this.props.item,
                        )
                      }
                      style={{
                        flexDirection: "row",
                        alignItems: "center",
                        paddingVertical: scale(2, 0.5),
                        paddingHorizontal: moderateScale(10, 0.5),
                        borderColor: "#c9c9c9",
                        borderWidth: 1,
                        borderRadius: 3,
                      }}
                    >
                      <Text style={{ fontSize: 11, fontFamily: "Poppins" }}>
                        {this.props.item.KalemOdemeKosuluText}
                      </Text>
                      <Image
                        source={require("@assets/images/down-arrow.png")}
                        style={{
                          marginLeft: scale(5),
                          width: 14,
                          height: 14,
                          tintColor: "#cecece",
                        }}
                      />
                    </TouchableOpacity>
                  </View>
                ) : (
                  <Text
                    style={{
                      fontFamily: "Poppins",
                      fontSize: 11,
                      textAlign: "left",
                      color: "#343434",
                    }}
                  >{`Birim Fiyatı: ${Number(this.props.item.KalemBirimTutar)
                    .toFixed(2)
                    .toString()
                    .replace(".", ",")} ${this.props.item.KalemPb}`}</Text>
                )}
              </View>
            </View>
          }
        </View>
        {this.props.subItems.length > 0 &&
          this.props.subItems.map((item, index) => {
            if (this.props.altKalem) {
              const IdUrunExt = this.props.IdUrunExt[item.KalemUrunKodu];
              return (
                <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                  <View style={{ flexDirection: "column", width: 30, alignItems: "center" }}>
                    <View
                      style={{
                        flexDirection: "column",
                        backgroundColor: "#dedede",
                        width: 2,
                        flexGrow: 0.5,
                      }}
                    />
                    <View style={{ flexDirection: "row", justifyContent: "flex-end", width: 30 }}>
                      <View
                        style={{
                          flexDirection: "row",
                          backgroundColor: "#dedede",
                          width: 16,
                          height: 2,
                          justifyContent: "flex-end",
                          alignItems: "center",
                        }}
                      />
                    </View>
                    {this.props.subItems.length - 1 > index && (
                      <View
                        style={{
                          flexDirection: "column",
                          backgroundColor: "#dedede",
                          width: 2,
                          flexGrow: 0.5,
                        }}
                      />
                    )}
                  </View>
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate({
                        key: "checkout",
                        routeName: "ProductDetailsScreen",
                        params: {
                          UrunKodu: item.KalemUrunKodu,
                          SeriNo: item.UrunSeriNo,
                          isFavorite: false,
                        },
                      })
                    }
                    style={{
                      backgroundColor: "#F0F4F5",
                      flex: 1,
                      borderRadius: 3,
                      marginRight: HORIZONTAL_PADDING,
                      padding: HORIZONTAL_PADDING / 2,
                      marginVertical: 10,
                    }}
                  >
                    <View style={{ flexDirection: "row" }}>
                      <View style={styles.subPrdImgContainer}>
                        <SmartImage
                          style={styles.subPrdImg}
                          resizeMode="contain"
                          source={{ uri: IdUrunExt.ResimUrl || "" }}
                        />
                      </View>
                      <View style={{ flexDirection: "row", flex: 1, paddingLeft: 8 }}>
                        <View style={{ flexDirection: "column" }}>
                          <Text
                            numberOfLines={2}
                            ellipsizeMode="tail"
                            style={{
                              fontFamily: "Poppins-Medium",
                              fontSize: 11,
                              textAlign: "left",
                              paddingRight: 5,
                            }}
                          >
                            {item.KalemUrunTanim}
                          </Text>
                          <Text style={{ fontFamily: "Poppins", fontSize: 11, color: "gray" }}>
                            {item.KalemUrunMiktari + "  adet"}
                          </Text>
                        </View>
                      </View>
                    </View>
                  </TouchableOpacity>
                </View>
              );
            } else {
              const IdUrunExt = this.props.IdUrunExt[item.KalemUrunKodu];
              const item_out = this.props.kalemlerOut.filter(
                i => i.KalemUrunKodu === item.KalemUrunKodu,
              )[0];
              return (
                <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                  <View style={{ flexDirection: "column", width: 30, alignItems: "center" }}>
                    <View
                      style={{
                        flexDirection: "column",
                        backgroundColor: "#dedede",
                        width: 2,
                        flexGrow: 0.5,
                      }}
                    />
                    <View style={{ flexDirection: "row", justifyContent: "flex-end", width: 30 }}>
                      <View
                        style={{
                          flexDirection: "row",
                          backgroundColor: "#dedede",
                          width: 16,
                          height: 2,
                          justifyContent: "flex-end",
                          alignItems: "center",
                        }}
                      />
                    </View>
                    {this.props.subItems.length - 1 > index && (
                      <View
                        style={{
                          flexDirection: "column",
                          backgroundColor: "#dedede",
                          width: 2,
                          flexGrow: 0.5,
                        }}
                      />
                    )}
                  </View>
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate({
                        key: "checkout",
                        routeName: "ProductDetailsScreen",
                        params: {
                          UrunKodu: item.KalemUrunKodu,
                          SeriNo: item.KalemSeriNo,
                          isFavorite: false,
                        },
                      })
                    }
                    style={{
                      backgroundColor: "#F0F4F5",
                      flex: 1,
                      borderRadius: 3,
                      marginRight: HORIZONTAL_PADDING,
                      padding: HORIZONTAL_PADDING / 2,
                      marginVertical: scale(10),
                    }}
                  >
                    <View style={{ flexDirection: "row" }}>
                      <View style={{ flexDirection: "column" }}>
                        <View style={styles.subPrdImgContainer}>
                          <SmartImage
                            style={styles.subPrdImg}
                            resizeMode="contain"
                            source={{ uri: this.props.IdUrunExt[item.KalemUrunKodu].ResimUrl }}
                          />
                        </View>
                        <Text
                          style={{
                            fontFamily: "Poppins",
                            marginTop: 2,
                            fontSize: 10,
                            color: "gray",
                            alignSelf: "center",
                          }}
                        >{`${this.props.item.KalemUrunMiktari} adet`}</Text>
                      </View>

                      <View style={{ flexDirection: "row", flex: 1, paddingLeft: scale(8) }}>
                        <View style={{ flexDirection: "column" }}>
                          <Text
                            style={{
                              fontFamily: "Poppins-Medium",
                              fontSize: 11,
                              textAlign: "left",
                              paddingRight: 5,
                            }}
                          >
                            {this.props.IdUrunExt[item.KalemUrunKodu].UrunAciklama}
                          </Text>
                          <View style={{ flexDirection: "row" }}>
                            {typeof IdUrunExt !== "undefined" &&
                            IdUrunExt.IlVadeSecenekleri.length > 0 ? (
                              <View style={{ flexDirection: "row", alignItems: "center" }}>
                                <TouchableOpacity
                                  onPress={() => {
                                    console.log("onPressVade", {
                                      item,
                                      idUrunExt: this.props.IdUrunExt,
                                      ilVadeSecenekleri: ext.IlVadeSecenekleri,
                                      selectedKalem: item_out,
                                    });
                                    this.props.onPressVade(
                                      ext.IlVadeSecenekleri,
                                      item_out.KalemOdemeKosulu,
                                      item_out,
                                    );
                                  }}
                                  style={{
                                    backgroundColor: "white",
                                    flexDirection: "row",
                                    alignItems: "center",
                                    paddingVertical: scale(2),
                                    paddingHorizontal: scale(10),
                                    borderColor: "#c9c9c9",
                                    borderWidth: 1,
                                    borderRadius: 3,
                                  }}
                                >
                                  <Text style={{ fontSize: 11, fontFamily: "Poppins" }}>
                                    {item_out.KalemOdemeKosuluText}
                                  </Text>
                                  <Image
                                    source={require("@assets/images/down-arrow.png")}
                                    style={{
                                      marginLeft: scale(5),
                                      width: 14,
                                      height: 14,
                                      tintColor: "#cecece",
                                    }}
                                  />
                                </TouchableOpacity>
                              </View>
                            ) : (
                              <Text
                                style={{
                                  fontFamily: "Poppins",
                                  fontSize: 11,
                                  textAlign: "left",
                                  color: "#343434",
                                }}
                              >{`Birim Fiyatı: ${Number(item.KalemBirimTutar)
                                .toFixed(2)
                                .toString()
                                .replace(".", ",")} ${item.KalemPb}`}</Text>
                            )}
                          </View>
                        </View>
                      </View>
                    </View>
                  </TouchableOpacity>
                </View>
              );
            }
          })}
      </TouchableOpacity>
    );
  }
}

const styles = ScaledSheet.create({
  prdImg: {
    width: "60@s",
    height: "60@s",
  },
  prdImgContainer: {
    borderColor: "#dddddd",
    borderWidth: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    height: "70@s",
    width: "70@s",
  },
  subPrdImg: {
    width: "45@s",
    height: "45@s",
  },
  subPrdImgContainer: {
    borderColor: "#dddddd",
    borderWidth: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    height: "50@s",
    width: "50@s",
    backgroundColor: "white",
  },
});
