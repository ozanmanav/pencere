import React, { Component } from "react";
import { View, Text, Dimensions, Image, FlatList } from "react-native";

const win = Dimensions.get("window");

export default class CheckoutPaynetCampaigns extends Component {
  render() {
    return (
      <View>
        <View style={{ flexDirection: "row", flex: 1, height: 1, backgroundColor: "#ddd" }} />
        <FlatList
          ref={(c) => {
            this.listView = c;
          }}
          style={{ backgroundColor: "transparent", padding: win.width / 25, paddingBottom: 0 }}
          data={this.props.campaigns}
          renderItem={(rowData) => this._renderRow(rowData)}
          ListHeaderComponent={() => {
            return (
              <Text style={{ fontSize: 16, color: "#607D8B", fontWeight: "600", marginBottom: 10 }}>
                Kart Kampanyaları
              </Text>
            );
          }}
          keyExtractor={(item) => item.UrunKodu}
        />
      </View>
    );
  }
  _renderRow(rowData) {
    return (
      <View style={{ flexDirection: "row", marginBottom: 10 }}>
        <Image
          source={require("@assets/images/info_white.png")}
          style={[{ width: 16, height: 16, marginRight: 6, marginTop: 4, tintColor: "#607D8B" }]}
        />
        <Text style={{ fontSize: 15, color: "grey" }}>
          {rowData.Tutar} {this.props.currency} için uygun taksit seçeneğini seçmeniz durumunda{" "}
          {rowData.KampanyaAciklama} kampanyasından yararlanabilirsiniz.
        </Text>
      </View>
    );
  }
}
