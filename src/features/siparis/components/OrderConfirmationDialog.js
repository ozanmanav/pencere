import React, { Component } from "react";
import { TextInput, View, TouchableOpacity, StyleSheet, Dimensions, Keyboard } from "react-native";
const win = Dimensions.get("window");
import { Text, ModalBox } from "@components";
import PencereIndicator from "../../../components/PencereIndicator";

export default class OrderConfirmationDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      input: "",
      inProgress: false,
      keyboardVisible: false,
      keyboardHeight: 0,
    };
  }

  componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener("keyboardDidShow", this._keyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener("keyboardDidHide", this._keyboardDidHide);
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow = (e) => {
    this.setState({
      keyboardVisible: true,
      keyboardHeight: e.endCoordinates.height,
    });
  };

  _keyboardDidHide = () => {
    this.setState({ keyboardVisible: false });
  };

  toggleVisibility = () => {
    this.modalBox.toggleVisibility();
  };

  render() {
    return (
      <ModalBox
        ref={(c) => (this.modalBox = c)}
        animationType="none"
        presentationStyle="overFullScreen"
        scrollable={false}
        statusBarHidden={false}
      >
        <TouchableOpacity
          style={[
            styles.container,
            this.state.keyboardVisible && {
              marginBottom: this.state.keyboardHeight,
            },
          ]}
        >
          <Text
            style={{
              fontSize: 14,
              color: "gray",
              paddingVertical: 10,
              paddingHorizontal: 20,
              paddingBottom: 0,
            }}
          >
            Siparişi onaylamak için güvenlik amacıyla kullanıcı şifrenizi giriniz:
          </Text>
          <TextInput
            ref={(c) => (this.passInput = c)}
            value={this.state.input}
            autoFocus
            placeholder="Kullanıcı şifreniz..."
            autoCapitalize="none"
            secureTextEntry={true}
            onChangeText={(input) => this.setState({ input })}
            onSubmitEditing={() => {
              this.toggleVisibility();
              this.props.confirmOrder(this.state.input);
            }}
            style={{
              borderColor: "#e0e0e0",
              borderWidth: 1,
              backgroundColor: "white",
              borderRadius: 5,
              fontSize: 16,
              marginVertical: 15,
              width: win.width * 0.8 - 40,
              padding: 4,
              paddingHorizontal: 6,
            }}
          />
          <View
            style={{
              flexDirection: "row",
              width: win.width * 0.9,
              justifyContent: "center",
              alignItems: "center",
              borderTopColor: "#dedede",
              borderTopWidth: 1,
            }}
          >
            <TouchableOpacity
              style={{
                alignItems: "center",
                flex: 1,
                borderRightColor: "#dedede",
                borderRightWidth: 1,
              }}
              onPress={() => this.toggleVisibility()}
            >
              <Text style={{ fontSize: 16, color: "gray", padding: 5 }}>İptal</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{ flex: 1, alignItems: "center", padding: 5 }}
              onPress={() => {
                this.passInput.blur();
                this.toggleVisibility();
                this.props.confirmOrder(this.state.input);
              }}
            >
              <Text style={{ fontSize: 16, color: "gray" }}>Tamam</Text>
            </TouchableOpacity>
          </View>
        </TouchableOpacity>
        {this.state.inProgress && (
          <View
            style={{
              width: win.width,
              height: win.height,
              justifyContent: "center",
              alignItems: "center",
              position: "absolute",
              top: 0,
              left: 0,
              backgroundColor: "rgba(0,0,0,0.3)",
              zIndex: 9999,
            }}
          >
            <PencereIndicator />
          </View>
        )}
      </ModalBox>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "column",
    backgroundColor: "white",
    width: win.width * 0.9,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.5,
    shadowRadius: 2,
    elevation: 1,
    alignItems: "center",
    marginBottom: 50,
    borderRadius: 5,
  },
});
