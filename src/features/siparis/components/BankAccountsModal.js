import React, { Component } from "react";
import colors from "@common/styles/colors";
import Icon from "react-native-vector-icons/FontAwesome";
import {
  View,
  ScrollView,
  StatusBar,
  Animated,
  Dimensions,
  Platform,
  LayoutAnimation,
  Image,
  TouchableOpacity,
} from "react-native";
import { Text, ScrollModal } from "@components";
import { ScaledSheet, moderateScale } from "react-native-size-matters";
import { UPDATE_SPRINGY } from "@common/LayoutAnimations";
import { isIphoneX } from "react-native-iphone-x-helper";
import commonColors from "@common/styles/colors";

const win = Dimensions.get("window");
const AnimatedScrollView = Animated.createAnimatedComponent(ScrollView);
const SPACE_HORIZONTAL = win.width / 50;

class BankComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isCollapsed: true,
      isVisible: false,
    };
  }
  render() {
    return (
      <TouchableOpacity
        onPress={() => {
          this.setState({ isCollapsed: !this.state.isCollapsed });
          LayoutAnimation.configureNext(UPDATE_SPRINGY);
        }}
        style={[styles.itemContainer]}
      >
        <View
          style={{ flexDirection: "row", alignItems: "center", height: moderateScale(40, 0.5) }}
        >
          <Text style={styles.itemTitle}>{this.props.bank.BankaAdi}</Text>
          <Image
            source={require("@assets/images/arrow-right.png")}
            style={{
              marginBottom: moderateScale(2, 0.5),
              marginLeft: moderateScale(10, 0.5),
              transform: this.state.isCollapsed ? [{ rotateZ: "90deg" }] : [{ rotateZ: "-90deg" }],
              width: moderateScale(12, 0.5),
              height: moderateScale(12, 0.5),
              tintColor: "#212121",
            }}
          />
        </View>
        {!this.state.isCollapsed &&
          this.props.subeler_keys.map((sube, i) => {
            const hesaplar_keys = Object.keys(this.props.bank.Subeler[sube].Hesaplar);
            return (
              <View key={i.toString()} style={{ flexDirection: "column" }}>
                <Text style={{ fontFamily: "Poppins-Medium", fontSize: 12 }}>{sube}</Text>
                {hesaplar_keys.map((hesap, index) => {
                  return (
                    <Text key={index.toString()} style={{ fontFamily: "Poppins", fontSize: 12 }}>
                      {hesap + " " + this.props.bank.Subeler[sube].Hesaplar[hesap]}
                    </Text>
                  );
                })}
              </View>
            );
          })}
      </TouchableOpacity>
    );
  }
}

class CityComponent extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const bankalar = this.props.item.banks;
    return (
      <View style={{ flexDirection: "column" }}>
        <TouchableOpacity
          style={{ flexDirection: "row", alignItems: "center", height: moderateScale(40, 0.5) }}
        >
          <Text
            style={{
              fontFamily: "Poppins-Medium",
              fontSize: 14,
              marginRight: moderateScale(10, 0.5),
            }}
          >
            {this.props.item.city}
          </Text>
        </TouchableOpacity>
        <View style={[{ flexDirection: "column" }]}>
          {bankalar.map(bank => {
            const subeler_keys = Object.keys(bank.Subeler);
            return <BankComponent bank={bank} subeler_keys={subeler_keys} />;
          })}
        </View>
      </View>
    );
  }
}

export default class BankAccountsModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isVisible: false,
      items: [],
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.isVisible !== prevProps.isVisible)
      this.setState({
        isVisible: this.props.isVisible,
      });
  }

  close() {
    this.toggleVisibility();
  }

  renderContent(content) {
    var contentClean = content.replace(/["”“]+/g, "");
    const textArray = contentClean.split("\r\n");
    return (
      <View style={{ marginTop: 10 }}>
        {textArray.map(item => {
          return <Text style={{ fontFamily: "Poppins", fontSize: 12 }}>{item}</Text>;
        })}
      </View>
    );
  }

  renderItem = ({ item }) => {
    return (
      <View style={styles.cityContainer}>
        <CityComponent item={item} />
      </View>
    );
  };

  toggleVisibility = () => {
    this.scrollModal.toggleVisibility();
  };

  render() {
    return (
      <ScrollModal
        ref={c => (this.scrollModal = c)}
        animationType="slide"
        statusBarHidden={false}
        scrollable={true}
        forceInset={{ top: "never" }}
        title={"Banka Hesapları"}
        headerColor={commonColors.primary_dark}
        titleColor={"white"}
        renderContent={() => {
          return (
            <View style={{ backgroundColor: "white", flexGrow: 1 }}>
              {this.props.accounts && (
                <AnimatedScrollView
                  style={{
                    flexGrow: 1,
                    marginBottom:
                      (Platform.OS === "android"
                        ? Platform.Version < 20
                          ? 0
                          : StatusBar.currentHeight
                        : 0) + moderateScale(60, 0.5),
                  }}
                  contentContainerStyle={{
                    flexDirection: "column",
                    backgroundColor: "white",
                    paddingBottom: isIphoneX ? moderateScale(40, 0.5) : moderateScale(10, 0.5),
                  }}
                  onScroll={Animated.event(
                    [{ nativeEvent: { contentOffset: { y: this.state.scrollAnim } } }],
                    {
                      useNativeDriver: true,
                    },
                  )}
                  scrollEventThrottle={16}
                  overScrollMode="never"
                >
                  {this.props.accounts.map(item => this.renderItem({ item }))}
                </AnimatedScrollView>
              )}
            </View>
          );
        }}
      />
    );
  }
}

const styles = ScaledSheet.create({
  container: {
    backgroundColor: colors.primary_dark,
    width: win.width,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.3,
    shadowRadius: 1,
    elevation: 1,
    flex: 1,
  },
  overlay: {
    position: "absolute",
    flex: 1,
    width: win.width,
    height: win.height,
    backgroundColor: "rgba(0,0,0,0.5)",
  },
  itemTitle: {
    color: colors.price_color,
    fontFamily: "Poppins-SemiBold",
    fontSize: 14,
  },
  itemContainer: {
    flexDirection: "column",
    backgroundColor: "#ffedc2",
    marginBottom: "10@ms0.5",
    padding: SPACE_HORIZONTAL,
  },
  cityContainer: {
    width: win.width,
    paddingHorizontal: SPACE_HORIZONTAL,
  },
  loadingContainer: {
    position: "absolute",
    top: 0,
    left: 0,
    height: win.height,
    width: win.width,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0,0,0,0.3)",
  },
});
