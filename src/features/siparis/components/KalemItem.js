import React, { Component } from "react";
import { View, TouchableOpacity, Image } from "react-native";

import { Text } from "@components";
import colors from "@common/styles/colors";

export default class KalemItem extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    if (this.props.item.KalemKey) {
      const key = this.props.item.KalemKey.substring(0, this.props.item.KalemKey.indexOf("/"));
      return (
        <TouchableOpacity
          onPress={() =>
            this.props.navigation.navigate({
              routeName: "ProductDetailsScreen",
              params: {
                UrunKodu: this.props.item.UrunKodu,
                SeriNo: this.props.item.UrunSeriNo,
                isFavorite: false,
              },
            })
          }
          style={{
            alignItems: "center",
            justifyContent: "center",
            backgroundColor: "white",
          }}
        >
          <View
            style={{
              flex: 1,
              flexDirection: "row",
              alignItems: "center",
              padding: 5,
            }}
            onPress={() =>
              this.props.navigation.navigate({
                key: "ProductDetailsScreen",
                routeName: "ProductDetailsScreen",
                params: {
                  product: this.props.item,
                  SeriNo: this.props.item.UrunSeriNo,
                },
              })
            }
          >
            <Image
              style={{
                flex: 0.3,
                flexDirection: "column",
                width: 75,
                height: 75,
              }}
              source={{ uri: this.props.IdUrunExt[key].ResimUrl }}
              resizeMode={"contain"}
            />
            <View
              style={{
                flex: 0.7,
                flexDirection: "column",
                padding: 10,
                justifyContent: "center",
              }}
            >
              <Text
                style={{
                  fontSize: 16,
                  alignSelf: "flex-start",
                  paddingVertical: 5,
                  color: colors.light_black,
                }}
              >
                {this.props.item.KalemUrunTanim}
              </Text>
              <Text style={{ fontSize: 16, alignSelf: "flex-start" }}>Tutar:</Text>
              <Text style={{ paddingVertical: 5, color: colors.light_black }}>
                {this.props.item.KalemBirimTutar.toFixed(2).toString().replace(".", ",") +
                  " x " +
                  this.props.item.KalemUrunMiktari +
                  " = " +
                  this.props.item.KalemToplamTutar.toFixed(2).toString().replace(".", ",") +
                  " " +
                  this.props.item.KalemPb}
              </Text>
            </View>
          </View>
        </TouchableOpacity>
      );
    } else return null;
  }
}
