import React, { Component } from "react";
import {
  View,
  Dimensions,
  TouchableOpacity,
  Image,
  TextInput,
  LayoutAnimation,
  Alert,
} from "react-native";
import Text from "@components/MyText";
import RadioButton from "@components/RadioButton";
import { CreditCardInput, CheckoutPaynetCampaigns } from "@features/siparis/components";
import SwitchToggle from "react-native-switch-toggle";
import { UPDATE_SPRINGY } from "@common/LayoutAnimations";
import { moderateScale } from "react-native-size-matters";

const win = Dimensions.get("window");
const SPACE_HORIZONTAL = win.width / 25;

const months = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"];

const years = new Array(10);
var year = new Date().getFullYear();
for (var i = 0; i < 10; i++) {
  years.push((year + i).toString());
}

export default class CreditCardForm extends Component {
  constructor(props) {
    super(props);
    this.taksitData = null;
    this.props.selectedBankData = null;
    //this.selectedInstalment = null;
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.selectedBankData?.bank_id !== this.props.selectedBankData?.bank_id) {
      LayoutAnimation.configureNext(UPDATE_SPRINGY);
    }
  }

  render() {
    var bankNotFound =
      this.props.creditCardForm?.selectedBankData?.bank_id != null &&
      this.props?.creditCardForm?.selectedBankIndex === -1;
    return (
      <View
        style={{
          flexDirection: "column",
          backgroundColor: "white",
          borderRadius: 4,
          paddingVertical: SPACE_HORIZONTAL,
        }}
      >
        {
          <View>
            <View
              style={{
                flexDirection: "column",
                paddingHorizontal: SPACE_HORIZONTAL,
              }}
            >
              {this.props.showCreditCardForm && (
                <View
                  style={{
                    flexDirection: "column",
                    marginBottom: moderateScale(10, 0.5),
                  }}
                >
                  <Text
                    style={{
                      fontFamily: "Poppins",
                      color: "#afafaf",
                      fontSize: 12,
                    }}
                  >
                    Kart Tipi
                  </Text>
                  <View
                    style={{
                      flexDirection: "row",
                      marginTop: moderateScale(10, 0.5),
                      flex: 1,
                      justifyContent: "space-between",
                      alignItems: "center",
                    }}
                  >
                    <TouchableOpacity
                      style={{ flexDirection: "row", alignItems: "center" }}
                      onPress={() => this.props.setCardType(0)}
                    >
                      <RadioButton
                        onPress={() => this.props.setCardType(0)}
                        style={{
                          width: moderateScale(20, 0.5),
                          height: moderateScale(20, 0.5),
                          borderRadius: moderateScale(10, 0.5),
                          marginRight: 8,
                        }}
                        innerStyle={{
                          width: moderateScale(10, 0.5),
                          height: moderateScale(10, 0.5),
                          borderRadius: moderateScale(5, 0.5),
                          backgroundColor: "#d6421e",
                        }}
                        selected={this.props.creditCardForm.cardType === 0}
                      />
                      <Text style={{ fontSize: 15, color: "#464646" }}>Bireysel Kart</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{
                        flexDirection: "row",
                        alignItems: "center",
                        marginRight: win.width / 20,
                      }}
                      onPress={() => this.props.setCardType(1)}
                    >
                      <RadioButton
                        onPress={() => this.props.setCardType(1)}
                        style={{
                          width: moderateScale(20, 0.5),
                          height: moderateScale(20, 0.5),
                          borderRadius: moderateScale(10, 0.5),
                          marginRight: moderateScale(8, 0.5),
                        }}
                        innerStyle={{
                          width: moderateScale(10, 0.5),
                          height: moderateScale(10, 0.5),
                          borderRadius: moderateScale(5, 0.5),
                          backgroundColor: "#d6421e",
                        }}
                        selected={this.props.creditCardForm.cardType === 1}
                      />
                      <Text style={{ fontSize: 15, color: "#464646" }}>Ticari Kart</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              )}
            </View>
            {this.props.showCreditCardForm && (
              <View
                style={{
                  flexDirection: "row",
                  flex: 1,
                  height: 1,
                  backgroundColor: "#ddd",
                }}
              />
            )}
            {this.props.showCreditCardForm && (
              <View
                ref={c => (this.formInputs = c)}
                style={{ flexDirection: "column", padding: SPACE_HORIZONTAL }}
              >
                <Text
                  style={{
                    fontFamily: "Poppins",
                    color: "#afafaf",
                    fontSize: 12,
                  }}
                >
                  Kart Üzerindeki İsim
                </Text>
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    height: moderateScale(46, 0.5),
                    paddingHorizontal: moderateScale(10, 0.5),
                    marginTop: moderateScale(10, 0.5),
                    borderColor: "#c3c3c3",
                    borderWidth: 1,
                    borderRadius: 3,
                  }}
                >
                  <TextInput
                    ref={c => {
                      this.nameInput = c;
                    }}
                    collapsable={false}
                    onFocus={() => {
                      this.nameInput.measure((fx, fy, width, height, px, py) => {
                        this.props.scrollToFocusedInput(py);
                      });
                    }}
                    style={{
                      fontSize: moderateScale(14, 0.5),
                      height: moderateScale(40, 0.5),
                      flex: 1,
                    }}
                    placeholderTextColor="#c8c8c8"
                    value={this.props.creditCardForm.holderName}
                    onChangeText={text => this.props.setName(text)}
                  />
                </View>
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                    alignItems: "center",
                  }}
                >
                  <Text
                    style={{
                      fontFamily: "Poppins",
                      color: "#afafaf",
                      fontSize: 12,
                      marginTop: moderateScale(10, 0.5),
                    }}
                  >
                    Kart Numarası
                  </Text>
                  {this.props.creditCardForm?.selectedBankData?.bank_logo != null && (
                    <Image
                      source={{
                        uri: this.props.creditCardForm.selectedBankData.bank_logo,
                      }}
                      style={{ width: 50, height: 25, resizeMode: "contain" }}
                    />
                  )}
                </View>

                <CreditCardInput
                  onChange={val => {
                    this.props.setNumber(val);
                    this.props.setSelectedBankData(val);
                  }}
                  scrollToFocusedInput={this.props.scrollToFocusedInput}
                />
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    flex: 1,
                    marginTop: 15,
                    marginBottom: 5,
                  }}
                >
                  <View
                    style={{
                      flexDirection: "row",
                      flex: 6.4,
                      alignItems: "center",
                    }}
                  >
                    <Text
                      style={{
                        fontFamily: "Poppins",
                        color: "#afafaf",
                        fontSize: 12,
                      }}
                    >
                      Son Kullanma Tarihi (Ay / Yıl)
                    </Text>
                  </View>
                  <View
                    style={{
                      flexDirection: "row",
                      flex: 3.6,
                      alignItems: "center",
                      paddingLeft: moderateScale(20, 0.5),
                    }}
                  >
                    <Text
                      style={{
                        fontFamily: "Poppins",
                        color: "#afafaf",
                        fontSize: 12,
                      }}
                    >
                      Güvenlik Kodu
                    </Text>
                    <TouchableOpacity
                      onPress={() =>
                        Alert.alert(
                          "CVC",
                          "Kartınızın arka yüzündeki 3 haneli güvenlik numarasıdır.",
                        )
                      }
                      style={{
                        marginHorizontal: 5,
                        alignItems: "center",
                        justifyContent: "center",
                      }}
                    >
                      <Image
                        source={require("@assets/images/qmark.png")}
                        style={{
                          width: moderateScale(14, 0.5),
                          height: moderateScale(14, 0.5),
                          resizeMode: "contain",
                          tintColor: "#c6c6c6",
                        }}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
                <View style={{ flexDirection: "row", flex: 1 }}>
                  <TouchableOpacity
                    onPress={() => this.props.toggleMonthPicker()}
                    style={{
                      flex: 3.2,
                      marginRight: moderateScale(10, 0.5),
                      height: moderateScale(46, 0.5),
                      borderRadius: 3,
                      borderColor: "#c8c8c8",
                      borderWidth: 1,
                      flexDirection: "row",
                    }}
                  >
                    <View
                      style={{
                        flex: 1,
                        height: moderateScale(46, 0.5),
                        borderRightColor: "#c8c8c8",
                        borderRightWidth: 1,
                        alignItems: "center",
                        justifyContent: "center",
                      }}
                    >
                      <Text
                        style={{
                          fontFamily: "Poppins",
                          color: "#afafaf",
                          fontSize: 12,
                        }}
                      >
                        {months[this.props.creditCardForm.expMonthIndex]}
                      </Text>
                    </View>
                    <View
                      style={{
                        width: moderateScale(26, 0.5),
                        height: moderateScale(50, 0.5),
                        justifyContent: "center",
                        alignItems: "center",
                      }}
                    >
                      <Image
                        source={require("@assets/images/down-arrow.png")}
                        style={{
                          width: moderateScale(8, 0.5),
                          height: moderateScale(8, 0.5),
                          tintColor: "#cecece",
                        }}
                      />
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => this.props.toggleYearPicker()}
                    style={{
                      flex: 3.2,
                      marginRight: 10,
                      height: moderateScale(46, 0.5),
                      borderRadius: 3,
                      borderColor: "#c8c8c8",
                      borderWidth: 1,
                      flexDirection: "row",
                    }}
                  >
                    <View
                      style={{
                        flex: 1,
                        height: moderateScale(46, 0.5),
                        borderRightColor: "#c8c8c8",
                        borderRightWidth: 1,
                        alignItems: "center",
                        justifyContent: "center",
                      }}
                    >
                      <Text
                        style={{
                          fontFamily: "Poppins",
                          color: "#afafaf",
                          fontSize: 12,
                        }}
                      >
                        {this.props.creditCardForm.expYear.slice(-2)}
                      </Text>
                    </View>
                    <View
                      style={{
                        width: moderateScale(26, 0.5),
                        height: moderateScale(50, 0.5),
                        justifyContent: "center",
                        alignItems: "center",
                      }}
                    >
                      <Image
                        source={require("@assets/images/down-arrow.png")}
                        style={{
                          width: moderateScale(8, 0.5),
                          height: moderateScale(8, 0.5),
                          tintColor: "#cecece",
                        }}
                      />
                    </View>
                  </TouchableOpacity>
                  <View
                    style={{
                      flex: 3.6,
                      height: moderateScale(46, 0.5),
                      flexDirection: "row",
                    }}
                  >
                    <TouchableOpacity
                      style={{
                        flex: 1,
                        height: moderateScale(46, 0.5),
                        borderRadius: 3,
                        borderColor: "#c8c8c8",
                        borderWidth: 1,
                        flexDirection: "row",
                        alignItems: "center",
                        justifyContent: "center",
                      }}
                    >
                      <TextInput
                        ref={c => {
                          this.cvcInput = c;
                        }}
                        collapsable={false}
                        onFocus={() => {
                          this.cvcInput.measure((fx, fy, width, height, px, py) => {
                            this.props.scrollToFocusedInput(py);
                          });
                        }}
                        value={this.props.creditCardForm.cvc}
                        onChangeText={cvc => this.props.setCvc(cvc)}
                        maxLength={3}
                        keyboardType="numeric"
                        returnKeyType="done"
                        placeholder="_ _ _"
                        style={{
                          fontSize: moderateScale(14, 0.5),
                          flex: 1,
                          height: moderateScale(40, 0.5),
                          paddingTop: 2,
                          textAlign: "center",
                        }}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            )}
          </View>
        }
        {this.props.creditCardForm.selectedBankData.bank_id != null &&
        this.props.creditCardForm.selectedBankIndex !== -1 ? (
          <TouchableOpacity
            onPress={() => this.props.toggleInstalmentPicker()}
            style={{
              flexDirection: "row",
              borderRadius: 3,
              borderWidth: 1,
              borderColor: "#c8c8c8",
              height: moderateScale(46, 0.5),
              alignItems: "center",
              marginHorizontal: SPACE_HORIZONTAL,
              marginBottom: moderateScale(10, 0.5),
            }}
          >
            <View style={{ flex: 9 }}>
              <Text
                style={{
                  fontFamily: "Poppins",
                  color: "#3d3d3d",
                  fontSize: 14,
                  marginLeft: moderateScale(10, 0.5),
                }}
              >
                {" "}
                {this.props.instalment?.instalmentInfo != null
                  ? `${this.props.instalment?.instalmentInfo?.desc} %${(
                      this.props.instalment?.instalmentInfo?.ratio * 100
                    ).toFixed(2)}` +
                    (this.props.instalment.instalmentInfo?.campaign_commision_support !== 0
                      ? ` (${this.props.instalment.instalmentInfo?.campaign_commision_support +
                          (this.props.currency === "TRY" ? "TL" : this.props.currency)})`
                      : "")
                  : "Taksit Seçenekleri"}
              </Text>
            </View>
            <View
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Image
                source={require("@assets/images/down-arrow.png")}
                style={{
                  width: moderateScale(12, 0.5),
                  height: moderateScale(12, 0.5),
                  tintColor: "#cecece",
                }}
              />
            </View>
            {(this.props?.seciliTaksit || this.props.instalment?.instalmentInfo?.desc) && (
              <Text
                style={{
                  fontSize: 10,
                  color: "gray",
                  backgroundColor: "white",
                  position: "absolute",
                  top: moderateScale(-7, 0.35),
                  left: moderateScale(5, 0.5),
                }}
              >
                Taksit Seçenekleri
              </Text>
            )}
          </TouchableOpacity>
        ) : (
          <View style={{ flexDirection: "column" }}>
            <View style={{ flexDirection: "row", padding: win.width / 25 }}>
              <Image
                source={
                  bankNotFound
                    ? require("@assets/images/info_white.png")
                    : require("@assets/images/info.png")
                }
                style={[
                  { width: 16, height: 16, marginRight: 6, marginTop: 4 },
                  bankNotFound && { tintColor: "red" },
                ]}
              />
              <Text
                style={{
                  fontFamily: "Poppins",
                  fontSize: 13,
                  color: bankNotFound ? "red" : "gray",
                  flex: 1,
                  flexWrap: "wrap",
                }}
              >
                {bankNotFound
                  ? "Bankanıza uygun ödeme seçeneği bulanamadı. Lütfen başka bir kart deneyiniz."
                  : "Taksit seçeneklerini görebilmek için üstteki alana kredi kartınızı numarınızı yazınız."}
              </Text>
            </View>
            {this.props?.PaynetData?.IlPaynetKampanya?.length > 0 && (
              <CheckoutPaynetCampaigns
                campaigns={this.props.PaynetData.IlPaynetKampanya}
                currency={this.props.currency === "TRY" ? "TL" : this.props.currency}
              />
            )}
          </View>
        )}
        {this?.props?.creditCardForm?.selectedBankData?.ratio &&
          this.props.creditCardForm.selectedBankData.ratio.filter(filterCampaignSupport).length >
            0 && (
            <View style={{ flexDirection: "row", padding: win.width / 25 }}>
              <Image
                source={
                  bankNotFound
                    ? require("@assets/images/info_white.png")
                    : require("@assets/images/info.png")
                }
                style={[
                  { width: 16, height: 16, marginRight: 6, marginTop: 4 },
                  bankNotFound && { tintColor: "red" },
                ]}
              />
              <Text
                style={{
                  fontFamily: "Poppins",
                  fontSize: 13,
                  color: bankNotFound ? "red" : "gray",
                  flex: 1,
                  flexWrap: "wrap",
                }}
              >
                {
                  "Siparişinizdeki bazı ürünler Paynet kampanyasına dahildir. Kampanya desteği olan taksit seçeneklerinin destek tutarı parantez içinde belirtilmektedir."
                }
              </Text>
            </View>
          )}
        {this.props.showCreditCardForm &&
          this.props.creditCardForm.selectedBankData?.bank_id != null && (
            <View
              style={{
                flexDirection: "row",
                alignItems: "center",
                paddingHorizontal: SPACE_HORIZONTAL,
                marginTop: moderateScale(10, 0.5),
              }}
            >
              <SwitchToggle
                containerStyle={{
                  width: moderateScale(40, 0.35),
                  height: moderateScale(24, 0.35),
                  borderRadius: moderateScale(12, 0.35),
                  borderColor: "rgba(0,0,0,.1)",
                  borderWidth: 1,
                  padding: moderateScale(4, 0.35),
                }}
                circleStyle={{
                  width: moderateScale(18, 0.35),
                  height: moderateScale(18, 0.35),
                  borderRadius: moderateScale(9, 0.35),
                }}
                switchOn={this.props.creditCardForm.saveCard}
                onPress={() => this.props.toggleSaveCard()}
                circleColorOff="white"
                circleColorOn="#3486cb"
                backgroundColorOn="#eee"
                backgroundColorOff="#efefef"
                duration={200}
              />
              <Text
                style={{
                  marginLeft: moderateScale(10, 0.5),
                  fontSize: 14,
                  fontFamily: "Poppins",
                  color: "#666",
                }}
              >
                Kartımı Kaydet
              </Text>
            </View>
          )}

        {this.props.creditCardForm.saveCard && this.props.showCreditCardForm && (
          <TextInput
            value={this.props.creditCardForm.newCardDesc}
            onChangeText={text => this.props.setNewCardDesc(text)}
            placeholder="Kaydedeceğiniz karta bir isim veriniz"
            style={{
              fontSize: moderateScale(14, 0.5),
              borderColor: "#cecece",
              borderWidth: 1,
              borderRadius: 3,
              height: moderateScale(40, 0.5),
              flex: 1,
              marginTop: moderateScale(10, 0.5),
              marginHorizontal: SPACE_HORIZONTAL,
              paddingLeft: moderateScale(5, 0.5),
            }}
          />
        )}
      </View>
    );
  }
}

var filterCampaignSupport = function(item) {
  return item.campaign_commision_support < 0;
};
