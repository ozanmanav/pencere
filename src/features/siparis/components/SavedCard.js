import React, { Component } from "react";
import { View, TouchableOpacity, Image, Dimensions } from "react-native";
import { Text } from "@components";
import { ScaledSheet, moderateScale } from "react-native-size-matters";
import colors from "@common/styles/colors";

const win = Dimensions.get("window");

const SPACE = win.width / 50;

export default class SavedCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      logoWidth: 70,
      logoHeight: 30,
    };
  }

  componentDidMount() {
    Image.getSize(this.props.item.KartDetayi.card_logo_url, (width, height) => {
      if (this.state.width !== width || this.state.height !== height)
        this.setState({ logoWidth: width, logoHeight: height });
    });
  }

  render() {
    const card = this.props.item.KartDetayi;
    return (
      <TouchableOpacity
        onPress={this.props.onPress}
        style={[styles.container, this.props.containerStyle]}
      >
        <View style={{ flexDirection: "column", flex: 1 }}>
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <Text
              style={{
                fontFamily: "Poppins",
                fontSize: 11,
                color: "black",
              }}
            >
              {card.card_desc}
            </Text>
            <Text
              style={{
                fontFamily: "Poppins-Light",
                fontSize: 11,
                color: "#cecece",
              }}
            >
              {" | " +
                (card.card_type === "cc"
                  ? "Bireysel"
                  : card.card_type === "bc"
                  ? "Ticari"
                  : "Debit")}
            </Text>
          </View>
          <Text
            style={{
              fontFamily: "Poppins-Light",
              fontSize: 11,
              color: colors.light_black,
            }}
          >
            {card.card_holder}
          </Text>
          <Text
            style={{
              fontFamily: "Poppins-Light",
              fontSize: 11,
              color: colors.light_black,
            }}
          >
            {card.card_no}
          </Text>
          <Image
            source={{ uri: card.card_logo_url }}
            style={{
              position: "absolute",
              top: -5,
              right: SPACE,
              width: this.state.logoWidth * 0.7,
              height: this.state.logoHeight * 0.7,
              resizeMode: "cover",
            }}
            resizeMode="contain"
          />
        </View>
        <View
          style={{
            flexDirection: "column",
            width: moderateScale(32, 0.4),
            alignItems: "center",
            justifyContent: "center",
            borderLeftColor: "#dedede",
            borderLeftWidth: 1,
          }}
        >
          <Image
            source={require("@assets/images/down-arrow.png")}
            style={{
              width: moderateScale(14, 0.5),
              height: moderateScale(14, 0.5),
              tintColor: "#bebebe",
            }}
          />
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = ScaledSheet.create({
  container: {
    flexDirection: "row",
    flex: 1,
    borderRadius: 4,
    borderColor: "#cacaca",
    borderWidth: 1,
    padding: SPACE,
  },
});
