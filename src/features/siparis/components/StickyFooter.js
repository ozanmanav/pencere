import React, { Component } from "react";
import { View, TouchableOpacity, Alert } from "react-native";
import { Text } from "@components";
import { ScaledSheet, verticalScale } from "react-native-size-matters";
import { getWidth } from "@common/Utils";

const SPACE_HORIZONTAL = getWidth() / 25;

export default class StickyFooter extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    if (this.props.total != null)
      return (
        <View style={[styles.container, { shadowOpacity: this.props.shadowOpacity }]}>
          <View style={{ flexDirection: "column", flex: 6 }}>
            <Text fontScale={0.5} style={{ fontFamily: "Poppins", color: "#999", fontSize: 13 }}>
              Ödenecek Tutar
            </Text>
            <Text
              fontScale={0.5}
              style={{ fontFamily: "Poppins-SemiBold", color: "#d55749", fontSize: 16 }}
            >
              {this.props.total}
            </Text>
          </View>
          <TouchableOpacity
            disabled={
              this.props.placeOrder.isFetching ||
              this.props.inProgress ||
              this.props.payment.progress
            }
            style={styles.confirmBtn}
            onPress={() => {
              if (this.props.validateForm()) {
                this.props.confirmOrder();
              } else {
                Alert.alert(
                  "Eksik Bilgi",
                  this.props.getValidationAlertMessage(),
                  [{ text: "Tamam", onPress: () => console.log("Tamam") }],
                  { cancelable: false },
                );
              }
            }}
          >
            <Text
              fontScale={0.5}
              style={{ color: "white", fontSize: 14, fontFamily: "Poppins-SemiBold" }}
            >
              Onayla ve Bitir
            </Text>
          </TouchableOpacity>
        </View>
      );
    else return null;
  }
}

const styles = ScaledSheet.create({
  container: {
    position: "absolute",
    bottom: 0,
    flexDirection: "row",
    width: getWidth(),
    backgroundColor: "white",
    alignItems: "center",
    paddingHorizontal: SPACE_HORIZONTAL * 2 - 5,
    height: verticalScale(60),
    justifyContent: "space-between",
    shadowRadius: 10,
    shadowOffset: {
      width: 0,
      height: -8,
    },
    shadowColor: "#000",
    elevation: 4,
    borderTopColor: "#dedede",
    borderTopWidth: 1,
  },
  confirmBtn: {
    flexDirection: "column",
    flex: 4,
    alignItems: "center",
    justifyContent: "center",
    height: verticalScale(40),
    paddingHorizontal: "6@ms0.4",
    backgroundColor: "#5eb139",
    borderRadius: "4@ms0.3",
  },
});
