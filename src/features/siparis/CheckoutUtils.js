import { getEndPoint, convertToUsd } from "@common/Utils";
import { store } from "@redux/store";
import analytics from "@react-native-firebase/analytics";
var visilabsManager = require("@app/visilabsManager");
import axios from "axios";
import bugsnag from "@common/bugsnag_config";

export const saveOrder = () => {
  return fetch(
    getEndPoint("ORDER_URL") +
      "?Si=" +
      store.getState().session.info.SessionID +
      "&c=siparis_saklandi",
  ).then((res) => res.json());
};

export const trackCheckout = () => {
  const placeOrder = store.getState().placeOrder;
  try {
    const taxFiltered = placeOrder.response.SiparisVerisi.IlBaslikToplam.filter(
      (item) => item.ToplamTipi === "K",
    );
    const shippingFiltered = placeOrder.response.SiparisVerisi.IlBaslikToplam.filter(
      (item) => item.ToplamTipi === "M",
    );
    const shippingDiscountFiltered = placeOrder.response.SiparisVerisi.IlBaslikToplam.filter(
      (item) => item.ToplamTipi === "I",
    );
    const discountAmount =
      shippingDiscountFiltered && shippingDiscountFiltered[0]?.ToplamTutar
        ? shippingDiscountFiltered[0].ToplamTutar
        : 0;
    const transaction_firebase = {
      transaction_id: placeOrder.orderResult[0] && placeOrder.orderResult[0]?.SiparisNo,
      value: placeOrder.response.SiparisVerisi.GenelToplam.ToplamTutar,
      tax: taxFiltered ? taxFiltered[0].ToplamTutar : 0,
      shipping: shippingFiltered[0] ? shippingFiltered[0].ToplamTutar : 0 - discountAmount,
      currency: placeOrder.response.SiparisVerisi.GenelToplam.ToplamPb,
      paymentType: placeOrder.response.SiparisVerisi.IlBaslikUyOut[0].SiparisOdemeBicimi,
    };
    const transaction_ga = {
      id: placeOrder.orderResult[0] && placeOrder.orderResult[0]?.SiparisNo,
      revenue: placeOrder.response.SiparisVerisi.GenelToplam.ToplamTutar,
      tax: (taxFiltered && taxFiltered[0]?.ToplamTutar) || 0,
      shipping: shippingFiltered[0]
        ? convertToUsd(
            shippingFiltered[0].ToplamTutar - discountAmount,
            placeOrder.response.SiparisVerisi.GenelToplam.ToplamPb,
          )
        : 0,
      paymentType: placeOrder.response.SiparisVerisi.IlBaslikUyOut[0].SiparisOdemeBicimi,
    };
    const products = getImpressionProudcts(
      placeOrder.response.SiparisVerisi.IlBaslikUyOut[0].IlKalemlerOut,
    );
    analytics().logEvent("ecommerce_purchase", {
      ...transaction_firebase,
    });
    var stringCodes = "";
    var stringQuantities = "";
    var stringPrices = "";
    products.map((item) => {
      stringCodes = stringCodes + item.id + ";";
      stringQuantities = stringQuantities + String(item.quantity) + ";";
      stringPrices = stringPrices + String(item.price * item.quantity).replace(".", ",") + ";";
    });
    stringCodes = stringCodes.slice(0, -1);
    stringQuantities = stringQuantities.slice(0, -1);
    stringPrices = stringPrices.slice(0, -1);

    var data = {
      "OM.tid": transaction_ga.id || "Yok",
      "OM.pp": stringCodes,
      "OM.pu": stringQuantities,
      "OM.ppr": stringPrices,
    };
    visilabsManager.customEvent("Purchase", data);
  } catch (error) {
    console.log("error trackCheckout", error);
    bugsnag.notify(error, function (report) {
      report.errorClass = "trackCheckout";
      report.metadata = placeOrder;
    });
  }
};

export function getRatios(bin, publicKey, agent, ratioCode, posType, amount) {
  return new Promise((resolve, reject) => {
    axios
      .post(
        getEndPoint("PAYNET_GET_RATIOS_URL", "paynet"),
        {
          bin: bin,
          pos_type: posType,
          ratio_code: ratioCode,
          agent_id: agent,
          amount: amount,
        },
        {
          headers: {
            Authorization: "Basic " + publicKey,
            "Content-Type": "application/json",
          },
        },
      )
      .then((response) => {
        resolve(response.data);
      })
      .catch((error) => {
        bugsnag.notify(error, (report) => {
          report.errorClass = "getRatios";
        });
        reject(error);
      });
  });
}

function getImpressionProudcts(items) {
  var products = [];
  items.forEach((item) => {
    if (item.KalemUrunKodu != null && item.KalemUrunKodu !== "") {
      const p = {
        item_id: item.KalemUrunKodu,
        item_name: item.KalemUrunTanim,
        brand: item.KalemMarka,
        price: convertToUsd(item.KalemBirimTutar, item.KalemPb),
        currency: item.KalemPb,
        quantity: item.KalemUrunMiktari,
      };
      const p_ga = {
        id: item.KalemUrunKodu,
        name: item.KalemUrunTanim,
        brand: item.KalemMarka,
        category: "Belirsiz",
        price: convertToUsd(item.KalemBirimTutar, item.KalemPb),
        quantity: item.KalemUrunMiktari,
      };
      products.push(p_ga);
      analytics().logEvent("item_purchase", p);
    }
  });
  return products;
}
