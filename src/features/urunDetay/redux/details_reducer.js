const INITAL_STATE = {
  info: [],
  isFetching: false,
  isRefreshing: false,
  error: false,
};
import { GET_DETAILS, GET_DETAILS_SUCCESS, GET_DETAILS_FAILURE } from "@redux/actions/types";

export default function detailsReducer(state = INITAL_STATE, action) {
  switch (action.type) {
    case GET_DETAILS:
      return {
        ...state,
        isFetching: true,
        isRefreshing: action.refresh,
      };
    case GET_DETAILS_SUCCESS:
      return {
        ...state,
        info: action.dontSet ? state.info : action.data,
        isFetching: false,
        isRefreshing: false,
      };
    case GET_DETAILS_FAILURE:
      return {
        ...state,
        error: true,
        isFetching: false,
        isRefreshing: false,
      };
    default:
      return state;
  }
}
