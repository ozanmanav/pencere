import { GET_DETAILS, GET_DETAILS_SUCCESS, GET_DETAILS_FAILURE } from "@redux/actions/types";
import bugsnag from "@common/bugsnag_config";
import { getEndPoint } from "@common/Utils";
import axios from "axios";

export function getDetailsFromApi(dontSet = false, uk, pb, refresh, dg = false, sn) {
  return (dispatch, getState) => {
    return new Promise(function (resolve, reject) {
      dispatch({ type: GET_DETAILS, refresh });
      var prepared_url =
        getEndPoint("PRODUCT_DETAILS_URL") +
        "?Si=" +
        getState().session.info.SessionID +
        "&uk=" +
        uk +
        "&dg=" +
        (dg ? 1 : 0);
      if (pb != null && pb !== "") {
        prepared_url += "&pb=" + pb;
      }
      if (sn != null && sn !== "") {
        prepared_url += "&sn=" + sn;
      }
      axios
        .get(prepared_url)
        .then((response) => {
          if (
            response.headers["content-type"].includes("javascript") ||
            response.headers["content-type"].includes("json")
          ) {
            var stokVar = false;
            if (response?.data?.IlStoklar != null)
              response.data.IlStoklar.forEach((stok) => {
                if (stok.Durum !== "Yok") stokVar = true;
              });
            response.data.StokVar = stokVar ? "Var" : "Yok";
            dispatch(getDetailsSuccess(response.data, dontSet));
            resolve(response.data);
          } else {
            bugsnag.notify("detailResponseIsNotJson", (report) => {
              report.errorClass = "detailResponseIsNotJson";
              report.metadata = {
                url: prepared_url,
                response: response,
              };
            });
            console.log("not js error");
            dispatch(getDetailsFailure());
            reject("html");
          }
        })
        .catch((error) => {
          bugsnag.notify(error, (report) => {
            report.errorClass = "getDetailsFromApi";
            report.metadata = {
              url: prepared_url,
            };
          });
          console.error(error);
          dispatch(getDetailsFailure());
          reject(error);
        });
    });
  };
}

function getDetailsSuccess(data, dontSet) {
  return {
    type: GET_DETAILS_SUCCESS,
    data,
    dontSet,
  };
}

function getDetailsFailure() {
  return {
    type: GET_DETAILS_FAILURE,
  };
}
