import React, { Component } from "react";
import {
  Keyboard,
  TextInput,
  Image,
  View,
  TouchableOpacity,
  StyleSheet,
  Alert,
  Dimensions,
} from "react-native";
import { Text, ModalBox } from "@components";
import colors from "@common/styles/colors";
import { scale } from "react-native-size-matters";
import { PRICE_REQUISITIONS_URL } from "@common/Constants";

const win = Dimensions.get("window");

export default class PriceRequestModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      keyboardVisible: false,
      keyboardHeight: 0,
      quantity: "0",
    };
  }

  componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener("keyboardDidShow", this._keyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener("keyboardDidHide", this._keyboardDidHide);
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.item && prevProps.item !== this.props.item) {
      this.setState({ quantity: String(this.props.item.UrunMinFiyatTalebi) });
    }
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow = e => {
    this.setState({
      keyboardVisible: true,
      keyboardHeight: e.endCoordinates.height,
    });
  };

  _keyboardDidHide = () => {
    this.setState({ keyboardVisible: false });
  };

  toggleVisibility = () => {
    this.modalBox.toggleVisibility();
  };

  requestPrice = () => {
    if (Number(this.state.quantity) >= this.props.item.UrunMinFiyatTalebi) {
      fetch(
        PRICE_REQUISITIONS_URL +
          "?si=" +
          this.props.sessionId +
          "&func=2&uk=" +
          this.props.item.UrunKodu +
          "&ua=" +
          this.state.quantity,
      )
        .then(res => res.json())
        .then(responseJson => {})
        .catch(err => {
          console.log("price req", err);
          Alert.alert("Hata", "Bir hata oluştu. Lütfen daha sonra tekrar deneyin.");
        });
    } else {
      Alert.alert(
        "Yetersiz Adet",
        "Fiyat teklifinde bulunmak için gerekli minimum adedin altında bir adet belirlediniz.",
      );
    }
  };

  render() {
    return (
      <ModalBox
        ref={c => (this.modalBox = c)}
        statusBarHidden={false}
        animationType="slide"
        scrollable={false}
        presentationStyle="overFullScreen"
      >
        {this.props.item && this.props.item.UrunMinFiyatTalebiDurumu && (
          <View style={styles.container}>
            <View
              style={{
                flexDirection: "row",
                alignItems: "center",
                borderBottomColor: colors.light_gray,
                borderBottomWidth: 1,
                backgroundColor: "white",
                width: win.width * 0.94,
                paddingHorizontal: 15,
                justifyContent: "space-between",
                marginBottom: 15,
                paddingVertical: 10,
              }}
            >
              <Text
                style={{
                  fontFamily: "Poppins-Medium",
                  fontSize: 16,
                  color: "black",
                  marginTop: 2,
                }}
              >
                Fiyat Teklifi
              </Text>
              <TouchableOpacity onPress={() => this.modalBox.toggleVisibility()}>
                <Image
                  source={require("@assets/images/x.png")}
                  style={{ width: 14, height: 14, tintColor: "#696969" }}
                />
              </TouchableOpacity>
            </View>
            <View
              style={{
                borderColor: "#e4e4e4",
                borderWidth: 1,
                borderRadius: 4,
                flexDirection: "row",
                marginHorizontal: scale(15),
              }}
            >
              <View
                style={{
                  width: win.width * 0.15,
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <Image
                  source={{
                    uri: this.props.item.UrunResimleri.DefaultResim.UrunResimBuyukThumbnail,
                  }}
                  style={{
                    resizeMode: "contain",
                    height: win.width * 0.15,
                    width: (win.width * 0.15 * 3) / 4,
                  }}
                />
              </View>
              <View style={{ width: win.width * 0.58 }}>
                <View style={{ flexDirection: "column" }}>
                  <View style={{ flexDirection: "row" }}>
                    <Text
                      style={{
                        color: "#a81215",
                        fontFamily: "Poppins-Medium",
                        letterSpacing: 0.5,
                        fontSize: 11,
                      }}
                    >
                      {this.props.item.UrunMarkaText}{" "}
                    </Text>
                    <Text
                      style={{
                        fontFamily: "Poppins",
                        fontSize: 11,
                        color: "gray",
                      }}
                    >
                      {this.props.item.UrunKodu}
                    </Text>
                  </View>
                  <Text
                    style={{
                      fontFamily: "Poppins",
                      fontSize: 12,
                      color: "black",
                    }}
                  >
                    {this.props.item.UrunAciklamasi}
                  </Text>
                </View>
              </View>
            </View>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                alignItems: "center",
                paddingHorizontal: scale(15),
                marginTop: scale(10),
              }}
            >
              <Text
                style={{
                  fontFamily: "Poppins",
                  color: "gray",
                  marginRight: scale(10),
                }}
              >{`Minimum Adet: ${this.props.item.UrunMinFiyatTalebi}`}</Text>
              <View style={{ flexDirection: "row", alignSelf: "flex-end" }}>
                <TextInput
                  value={this.state.quantity}
                  onChangeText={text => {
                    this.setState({ quantity: text });
                  }}
                  keyboardType="numeric"
                  style={{
                    width: 34,
                    marginRight: 5,
                    borderRadius: 3,
                    borderLeftColor: "#ececec",
                    borderRightColor: "#e5e5e5",
                    borderTopColor: "#dedede",
                    borderWidth: 1,
                    borderBottomWidth: 0,
                    height: 34,
                    textAlign: "center",
                    backgroundColor: "#f5f4f5",
                    fontSize: 16,
                    color:
                      Number(this.state.quantity) >= this.props.item.UrunMinFiyatTalebi
                        ? "black"
                        : "red",
                  }}
                />
                <TouchableOpacity
                  onPress={() => this.requestPrice()}
                  style={{
                    flexDirection: "row",
                    flex: 1,
                    maxWidth: win.width * 0.4,
                    paddingHorizontal: 6,
                    backgroundColor: colors.green,
                    height: 34,
                    alignItems: "center",
                    justifyContent: "center",
                    borderRadius: 2,
                  }}
                >
                  <Text
                    style={{
                      fontFamily: "Poppins-Medium",
                      color: "white",
                      fontSize: 14,
                    }}
                  >
                    Teklif İste
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        )}
      </ModalBox>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "column",
    backgroundColor: "white",
    width: win.width * 0.94,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.5,
    shadowRadius: 2,
    elevation: 1,
    marginBottom: 50,
    borderRadius: 5,
    overflow: "hidden",
    paddingBottom: 10,
  },
});
