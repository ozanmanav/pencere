import React, { Component } from "react";
import {
  Image,
  View,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  Modal,
  KeyboardAvoidingView,
  TouchableHighlight,
  Keyboard,
  TextInput,
  Alert,
} from "react-native";
import { Text } from "@components";
import colors from "@common/styles/colors";
import { setCart, showToast, addToCart } from "@redux/actions";
import { connect } from "react-redux";
import Toast from "react-native-easy-toast";
import { moderateScale } from "react-native-size-matters";
import bugsnag from "@common/bugsnag_config";
import { getEndPoint } from "@common/Utils";

const win = Dimensions.get("window");

class CrossSellModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isVisible: false,
      packs: [],
      stage: 0,
      selectedItem: null,
      number: "1",
      progress: false,
      add: false,
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.isVisible !== prevProps.isVisible)
      this.setState({
        isVisible: this.props.isVisible,
      });
  }

  toggleVisibility = (packs, number, add) => {
    this.setState({
      isVisible: !this.state.isVisible,
      packs,
      number,
      stage: 0,
      selectedItem: null,
      add,
    });
  };

  renderStageOne = () => {
    return (
      <View style={{ flexDirection: "column", alignItems: "center" }}>
        <View
          style={{
            backgroundColor: "white",
            padding: win.width * 0.03,
            paddingTop: 0,
          }}
        >
          <Text style={{ fontFamily: "Poppins", fontSize: 12 }}>
            Sepete eklemek istediğiniz &quot;GS-01R&quot; kodlu ürün için size aşağıdaki gibi
            Avantajlı Paket önerimiz bulunmaktadır. Dilerseniz aşağıdaki Avantajlı Paket(ler)den
            birini seçerek işleminize devam edebilirsiniz.
          </Text>
        </View>
        <View style={{ alignItems: "center" }}>
          {this.state.packs.map((item, index) => {
            return (
              <View
                key={index.toString()}
                style={{
                  width: win.width * 0.9,
                  flexDirection: "row",
                  justifyContent: "center",
                  backgroundColor: "#e4e4e4",
                  alignItems: "center",
                  borderRadius: 3,
                  marginBottom: 10,
                  overflow: "hidden",
                }}
              >
                <View
                  style={{
                    flexDirection: "column",
                    flex: 1,
                    alignItems: "center",
                  }}
                >
                  <View
                    style={{
                      backgroundColor: colors.primary_dark,
                      width: win.width * 0.9,
                      height: moderateScale(30, 0.5),
                      justifyContent: "center",
                      paddingLeft: win.width * 0.02,
                    }}
                  >
                    <Text
                      style={{
                        color: "white",
                        fontFamily: "Poppins-Medium",
                        fontSize: 14,
                      }}
                    >
                      Avantajlı Paket Önerisi
                    </Text>
                  </View>
                  <View
                    style={{
                      flexDirection: "row",
                      alignItems: "center",
                      width: win.width * 0.88,
                      justifyContent: "space-between",
                      marginVertical: moderateScale(10, 0.5),
                      paddingVertical: win.width * 0.02,
                    }}
                  >
                    <View
                      style={{
                        flexDirection: "row",
                        backgroundColor: "white",
                        width: win.width * 0.7,
                        borderRadius: 2,
                        marginLeft: moderateScale(4, 0.5),
                        paddingVertical: moderateScale(5, 0.5),
                        paddingLeft: moderateScale(4, 0.5),
                        paddingRight: moderateScale(4, 0.5),
                        justifyContent: "center",
                      }}
                    >
                      <Image
                        source={{ uri: item.AltUrunTanimi.UrunResimThum }}
                        style={{
                          resizeMode: "contain",
                          height: win.width * 0.15,
                          width: (win.width * 0.15 * 3) / 4,
                          marginRight: moderateScale(10, 0.5),
                        }}
                      />
                      <View style={{ flexDirection: "column", flex: 1 }}>
                        <Text style={{ fontFamily: "Poppins", fontSize: 12 }}>
                          {item.AltUrunTanimi.IliskiAciklama}
                        </Text>
                        <View style={{ flexDirection: "row" }}>
                          <Text
                            style={{
                              fontFamily: "Poppins",
                              fontSize: 12,
                              textDecorationLine: "line-through",
                              textDecorationColor: "gray",
                              color: "gray",
                            }}
                          >
                            {item.AltUrunTanimi.UrunFiyati}
                          </Text>
                          <Text
                            style={{
                              fontFamily: "Poppins",
                              fontSize: 12,
                              marginLeft: moderateScale(10, 0.5),
                            }}
                          >
                            {item.AltUrunTanimi.ManuelFiyat}
                          </Text>
                        </View>
                      </View>
                    </View>
                    <View
                      style={{
                        width: win.width * 0.18,
                        justifyContent: "center",
                      }}
                    >
                      <TouchableOpacity
                        onPress={() => this.setState({ stage: 1, selectedItem: item })}
                        style={{
                          borderRadius: 2,
                          backgroundColor: "#5eb139",
                          paddingHorizontal: 5,
                          marginHorizontal: 8,
                          height: (win.width * 0.1) / 1.3,
                          justifyContent: "center",
                          alignItems: "center",
                        }}
                      >
                        <Text
                          style={{
                            fontFamily: "Poppins-Bold",
                            color: "white",
                            borderRadius: 5,
                            fontSize: 12,
                          }}
                        >
                          Seç
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              </View>
            );
          })}
        </View>
        <TouchableOpacity
          onPress={() => {
            if (this.state.add) {
              this.props.addToCart(
                this.state.packs[0].AnaUrunTanimi.UrunKodu,
                Number(this.state.number),
              );
            }
            this.setState({ isVisible: false });
          }}
          style={{
            backgroundColor: "#b9b9b9",
            height: moderateScale(30, 0.5),
            alignItems: "center",
            justifyContent: "center",
            borderRadius: 3,
            width: win.width * 0.9,
          }}
        >
          <Text
            style={{
              color: "white",
              fontSize: 14,
              fontFamily: "Poppins-Medium",
              marginTop: moderateScale(3, 0.5),
            }}
          >
            Hayır, Avantajlı Paket istemiyorum
          </Text>
        </TouchableOpacity>
      </View>
    );
  };

  renderStageTwo = () => {
    const priceDif = (
      (Number(this.state.selectedItem.AltUrunTanimi.UrunFiyati.replace(",", ".").split(" ")[0]) -
        Number(this.state.selectedItem.AltUrunTanimi.ManuelFiyat.replace(",", ".").split(" ")[0])) *
      Number(this.state.number === "" ? "0" : this.state.number)
    )
      .toFixed(2)
      .toString()
      .replace(".", ",");
    const currency = this.state.selectedItem.AltUrunTanimi.UrunFiyati.split(" ")[1];

    return (
      <View style={{ flexDirection: "column" }}>
        <View
          style={{
            backgroundColor: "white",
            padding: win.width * 0.03,
            paddingTop: 0,
          }}
        >
          <Text style={{ fontFamily: "Poppins", fontSize: 12 }}>
            {'Sepete eklemek istediğiniz "' +
              this.state.selectedItem.AnaUrunTanimi.UrunKodu +
              '" kodlu ürünün Avantajlı Paketi Önerisi için seçmiş olduğunuz ürünler ve adetleri aşağıda belirtilmiştir.'}
          </Text>
        </View>
        <View
          style={{
            flexDirection: "row",
            marginBottom: 15,
            justifyContent: "center",
          }}
        >
          <View
            style={{
              flexDirection: "column",
              backgroundColor: colors.primary_dark,
              width: win.width * 0.15,
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <Text
              style={{
                color: "white",
                fontFamily: "Poppins-Medium",
                fontSize: 22,
                lineHeight: 24,
                marginTop: 8,
              }}
            >
              {this.state.number === "" ? "0" : this.state.number}
            </Text>
            <Text
              style={{
                color: "white",
                fontFamily: "Poppins",
                fontSize: 12,
                lineHeight: 12,
                marginBottom: 4,
              }}
            >
              Adet
            </Text>
          </View>
          <View
            style={{
              borderColor: "#e4e4e4",
              borderWidth: 4,
              borderRadius: 4,
              flexDirection: "row",
              borderLeftWidth: 0,
            }}
          >
            <View
              style={{
                width: win.width * 0.15,
                justifyContent: "center",
                alignItems: "center",
                paddingHorizontal: 2,
              }}
            >
              <Image
                source={{
                  uri: this.state.selectedItem.AnaUrunTanimi.UrunResimThum,
                }}
                style={{
                  resizeMode: "contain",
                  height: win.width * 0.15,
                  width: (win.width * 0.15 * 3) / 4,
                }}
              />
            </View>
            <View style={{ width: win.width * 0.58 }}>
              <View style={{ flexDirection: "column" }}>
                <Text style={{ fontFamily: "Poppins", fontSize: 12, color: "gray" }}>
                  {this.state.selectedItem.AnaUrunTanimi.UrunKodu}
                </Text>
                <Text style={{ fontFamily: "Poppins", fontSize: 12 }}>
                  {this.state.selectedItem.AnaUrunTanimi.IliskiAciklama}
                </Text>
              </View>
            </View>
          </View>
        </View>
        <View
          style={{
            flexDirection: "row",
            marginBottom: 10,
            justifyContent: "center",
          }}
        >
          <View
            style={{
              flexDirection: "column",
              backgroundColor: colors.primary_dark,
              width: win.width * 0.15,
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <Text
              style={{
                color: "white",
                fontFamily: "Poppins-Medium",
                fontSize: 22,
                lineHeight: 24,
                marginTop: 8,
              }}
            >
              {this.state.number === "" ? "0" : this.state.number}
            </Text>
            <Text
              style={{
                color: "white",
                fontFamily: "Poppins",
                fontSize: 12,
                lineHeight: 12,
                marginBottom: 4,
              }}
            >
              Adet
            </Text>
          </View>
          <View
            style={{
              borderColor: "#e4e4e4",
              borderWidth: 3,
              borderRadius: 4,
              flexDirection: "row",
              borderLeftWidth: 0,
            }}
          >
            <View
              style={{
                width: win.width * 0.15,
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Image
                source={{
                  uri: this.state.selectedItem.AltUrunTanimi.UrunResimThum,
                }}
                style={{
                  resizeMode: "contain",
                  height: win.width * 0.15,
                  width: (win.width * 0.15 * 3) / 4,
                }}
              />
            </View>
            <View style={{ width: win.width * 0.58 }}>
              <View style={{ flexDirection: "column" }}>
                <Text style={{ fontFamily: "Poppins", fontSize: 12, color: "gray" }}>
                  {this.state.selectedItem.AltUrunTanimi.UrunKodu}
                </Text>
                <Text style={{ fontFamily: "Poppins", fontSize: 12 }}>
                  {this.state.selectedItem.AltUrunTanimi.IliskiAciklama}
                </Text>
              </View>
            </View>
          </View>
        </View>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "flex-end",
            alignItems: "center",
            marginBottom: moderateScale(10, 0.5),
            marginRight: win.width * 0.04,
          }}
        >
          <Text style={{ fontFamily: "Poppins", fontSize: 14, marginTop: 2 }}>
            {this.state.number + " adetlik alımda kazancınız: "}
          </Text>
          <Text
            style={{
              fontFamily: "Poppins-Medium",
              color: "#52853c",
              fontSize: 18,
            }}
          >
            {priceDif + currency}
          </Text>
        </View>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            width: win.width * 0.94,
            paddingHorizontal: win.width * 0.03,
            alignItems: "center",
          }}
        >
          <TouchableOpacity
            onPress={() => this.setState({ stage: 0 })}
            style={{
              backgroundColor: "#b9b9b9",
              height: moderateScale(34, 0.5),
              alignItems: "center",
              justifyContent: "center",
              borderRadius: 3,
              paddingHorizontal: moderateScale(6, 0.5),
            }}
          >
            <Text style={{ color: "white", fontSize: 14 }}>Paket Seçimine Dön</Text>
          </TouchableOpacity>

          <View style={{ flexDirection: "row", justifyContent: "flex-end" }}>
            <TextInput
              value={this.state.number}
              onChangeText={text => {
                this.setState({ number: text });
              }}
              returnKeyType="done"
              keyboardType="numeric"
              style={{
                width: moderateScale(34, 0.5),
                marginRight: moderateScale(5, 0.5),
                borderRadius: 3,
                borderLeftColor: "#ececec",
                borderRightColor: "#e5e5e5",
                borderTopColor: "#dedede",
                borderWidth: 1,
                borderBottomWidth: 0,
                height: moderateScale(34, 0.5),
                textAlign: "center",
                padding: 0,
                alignItems: "center",
                justifyContent: "center",
                backgroundColor: "#f5f4f5",
                fontSize: 18,
              }}
            />
            <TouchableOpacity
              onPress={() => this.addCrossSellToCart(this.state.selectedItem)}
              style={{
                flexDirection: "row",
                paddingHorizontal: moderateScale(6, 0.5),
                backgroundColor: colors.green,
                height: moderateScale(34, 0.5),
                alignItems: "center",
                justifyContent: "center",
                borderRadius: 2,
              }}
            >
              <Image
                source={require("@assets/images/cart.png")}
                style={{
                  marginRight: 10,
                  width: 17,
                  height: 17,
                  tintColor: "white",
                }}
              />
              <Text style={{ color: "white", fontSize: 14 }}>Sepete Ekle</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  };
  render() {
    if (this.state.isVisible) {
      return (
        <Modal
          animationType="none"
          transparent={true}
          presentationStyle="overFullScreen"
          visible={this.state.isVisible}
          onRequestClose={() => {
            this.setState({
              isVisible: false,
            });
          }}
        >
          <TouchableHighlight
            style={{
              width: win.width,
              height: win.height,
              backgroundColor: "rgba(0,0,0,0.6)",
              justifyContent: "center",
              alignItems: "center",
            }}
            onPress={() => this.setState({ isVisible: false })}
          >
            <KeyboardAvoidingView behavior="position">
              <TouchableOpacity style={styles.container} onPress={() => Keyboard.dismiss()}>
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    borderBottomColor: colors.light_gray,
                    borderBottomWidth: 1,
                    backgroundColor: "white",
                    width: win.width * 0.94,
                    paddingHorizontal: 15,
                    justifyContent: "space-between",
                    marginBottom: 15,
                    paddingVertical: 10,
                  }}
                >
                  <Text
                    style={{
                      fontFamily: "Poppins-Medium",
                      fontSize: 16,
                      color: "black",
                      marginTop: 2,
                    }}
                  >
                    Avantajlı Paketler
                  </Text>
                  <TouchableOpacity
                    style={{
                      padding: moderateScale(5, 0.5),
                      paddingLeft: moderateScale(10, 0.5),
                    }}
                    onPress={() => this.setState({ isVisible: false })}
                  >
                    <Image
                      source={require("@assets/images/x.png")}
                      style={{ width: 16, height: 16, tintColor: "#696969" }}
                    />
                  </TouchableOpacity>
                </View>
                {this.state.stage === 0 ? this.renderStageOne() : this.renderStageTwo()}
              </TouchableOpacity>
            </KeyboardAvoidingView>
          </TouchableHighlight>

          <Toast style={{ backgroundColor: "black", opacity: 0.8, zIndex: 999 }} />
        </Modal>
      );
    } else return null;
  }

  addCrossSellToCart(item) {
    if (this.state.number < 1) {
      Alert.alert("Hata", "Belirlediğiniz adet sayısı en az 1 olmalıdır.");
    } else {
      this.setState({
        progress: true,
      });
      fetch(
        getEndPoint("CART_URL") +
          "?Si=" +
          this.props.session.info.SessionID +
          "&func=10" +
          "&mci=" +
          item.AnaUrunTanimi.IliskiID +
          "&sci=" +
          item.AltUrunTanimi.IliskiID +
          "&muk=" +
          item.AnaUrunTanimi.UrunKodu +
          "&suk=" +
          item.AltUrunTanimi.UrunKodu +
          "&adt=" +
          this.state.number,
        {
          method: "POST",
        },
      )
        .then(response => response.json())
        .then(responseJson => {
          if (responseJson.MesajTipi === "S") {
            this.props.setCart(responseJson);
            this.props.showToast("Ürün sepete eklendi");
            this.toggleVisibility();
          } else {
            this.props.showToast("Bir hata oluştu lütfen tekrar deneyin");
          }
          this.setState({
            progress: false,
          });
        })
        .catch(error => {
          bugsnag.notify(error, report => {
            report.errorClass = "addCrossSellToCart";
          });
          this.setState({
            progress: false,
          });
          this.props.showToast("Bir hata oluştu lütfen tekrar deneyin");
          console.log("addToCart error", error);
        });
    }
  }
}

function mapDispatchToProps(dispatch) {
  return {
    setCart: cart => dispatch(setCart(cart)),
    showToast: (msg, dur) => dispatch(showToast(msg, dur)),
    addToCart: (uk, num) => dispatch(addToCart(uk, num)),
  };
}

export default connect(null, mapDispatchToProps, null, { withRef: true })(CrossSellModal);

const styles = StyleSheet.create({
  container: {
    flexDirection: "column",
    backgroundColor: "white",
    width: win.width * 0.94,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.5,
    shadowRadius: 2,
    elevation: 1,
    alignItems: "center",
    marginBottom: 50,
    borderRadius: 5,
    overflow: "hidden",
    paddingBottom: 10,
  },
});
