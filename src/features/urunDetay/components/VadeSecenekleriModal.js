import React, { Component } from "react";
import { View, TouchableOpacity, Image, StyleSheet, Dimensions, FlatList } from "react-native";
import { Text, ModalBox } from "@components";
import colors from "@common/styles/colors";
import Icon from "react-native-vector-icons/FontAwesome";
import { getWidth } from "@common/Utils";
import { moderateScale } from "react-native-size-matters";

const win = Dimensions.get("window");

export default class VadeSecenenkleriModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isVisible: false,
      specs: [],
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.isVisible !== prevProps.isVisible)
      this.setState({
        isVisible: this.props.isVisible,
      });
  }

  close() {
    this.setState({
      isVisible: false,
    });
  }

  toggleVisibility = specs => {
    this.setState({
      specs,
    });
    this.modalBox.toggleVisibility();
  };

  componentDidMount() {
    this.vadeSecenekleri = this.props.items.filter(item => item.OdemeKosulu.includes("A"));
    this.taksitSecenekleri = this.props.items.filter(item => !item.OdemeKosulu.includes("A"));
  }

  render() {
    return (
      <ModalBox
        ref={c => (this.modalBox = c)}
        animationType="slide"
        presentationStyle="fullScreen"
        statusBarHidden={false}
        scrollable={true}
      >
        <View style={styles.container}>
          <View
            style={{
              flexDirection: "row",
              backgroundColor: colors.primary_dark,
              height: 50,
              paddingHorizontal: 10,
              alignItems: "center",
            }}
          >
            <TouchableOpacity
              style={{ width: 26, height: 26, marginRight: 5 }}
              onPress={() => this.toggleVisibility()}
            >
              <Icon name="times-circle" size={26} color="rgba(255, 255, 255, 0.5)" />
            </TouchableOpacity>
            <Text
              style={{ fontFamily: "Poppins-Medium", fontSize: 16, color: "white", marginTop: 2 }}
            >
              {this.props.session.info.YayginKanalMi
                ? "Vade ve Taksit Seçenekleri"
                : "Vade Seçenekleri"}
            </Text>
          </View>
          <FlatList
            style={{ backgroundColor: "white" }}
            contentContainerStyle={{ paddingBottom: 40 }}
            ListHeaderComponent={
              <View style={{ flexDirection: "column" }}>
                <View style={{ flexDirection: "row", padding: getWidth() / 25 }}>
                  <Image
                    source={require("@assets/images/info.png")}
                    style={{ width: 16, height: 16, marginRight: 6, marginTop: 3 }}
                  />
                  <Text style={{ fontFamily: "Poppins", fontSize: 15 }}>
                    Siparişinizi oluşturuken aşağıdaki vade seçeneklerini kullanabilirsiniz.
                  </Text>
                </View>
                <View
                  style={{
                    width: getWidth(),
                    height: StyleSheet.hairlineWidth,
                    backgroundColor: "#dedede",
                    left: 0,
                  }}
                />
                <View
                  style={{
                    flexDirection: "column",
                    padding: getWidth() / 25,
                    paddingVertical: getWidth() / 30,
                  }}
                >
                  <Text
                    style={{
                      fontFamily: "Poppins-SemiBold",
                      color: colors.price_color,
                      fontSize: 16,
                      marginBottom: moderateScale(6, 0.4),
                    }}
                  >
                    Vade Seçenekleri
                  </Text>
                  {this.vadeSecenekleri &&
                    this.vadeSecenekleri.length > 0 &&
                    this.vadeSecenekleri.map(item => {
                      if (item.OdemeKosulu.includes("A")) {
                        return (
                          <View style={{ borderColor: colors.light_gray, borderBottomWidth: 0 }}>
                            <View style={{ marginBottom: moderateScale(4, 0.5) }}>
                              <Text style={{ fontFamily: "Poppins-Medium", fontSize: 15 }}>
                                {item.Aciklama}
                              </Text>
                            </View>
                          </View>
                        );
                      }
                    })}
                </View>

                <View
                  style={{
                    width: getWidth(),
                    height: StyleSheet.hairlineWidth,
                    backgroundColor: "#dedede",
                  }}
                />
                <View
                  style={{
                    flexDirection: "column",
                    padding: getWidth() / 25,
                    paddingVertical: getWidth() / 30,
                  }}
                >
                  {this.taksitSecenekleri?.length > 0 && (
                    <Text
                      style={{
                        fontFamily: "Poppins-SemiBold",
                        color: colors.price_color,
                        fontSize: 16,
                        marginBottom: moderateScale(6, 0.4),
                      }}
                    >
                      Taksit Seçenekleri
                    </Text>
                  )}
                  {this.taksitSecenekleri &&
                    this.taksitSecenekleri.length > 0 &&
                    this.taksitSecenekleri.map(item => {
                      if (!item.OdemeKosulu.includes("A")) {
                        return (
                          <View style={{ borderColor: colors.light_gray, borderBottomWidth: 0 }}>
                            <View style={{ marginBottom: moderateScale(4, 0.5) }}>
                              <Text style={{ fontFamily: "Poppins-Medium", fontSize: 15 }}>
                                {item.Aciklama}
                              </Text>
                            </View>
                          </View>
                        );
                      }
                    })}
                </View>
              </View>
            }
          />
        </View>
      </ModalBox>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.primary_dark,
    width: win.width,
    height: win.height,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.3,
    shadowRadius: 1,
    elevation: 1,
  },
});
