import React, { Component } from "react";
import { Animated, View, Image, Dimensions, TouchableOpacity } from "react-native";
import colors from "@common/styles/colors";
import { SmartImage } from "@components";
import { moderateScale, ScaledSheet } from "react-native-size-matters";
const deviceWidth = Dimensions.get("window").width;
const deviceHeight = Dimensions.get("window").height;
import Carousel from "react-native-snap-carousel";

const BAR_SPACE = 10;

export default class ProductCarousel extends Component {
  animVal = new Animated.Value(0);
  springVal = new Animated.Value(0);

  constructor(props) {
    super(props);
    this.state = {
      isVisible: false,
      index: 0,
    };
  }

  handleScroll = event => {
    this.setState({ scrollX: event.nativeEvent.contentOffset.x });
    Animated.event([{ nativeEvent: { contentOffset: { x: this.animVal } } }]);
  };

  scrollToPrev() {
    if (this.state.scrollX > 0)
      this._carousel.scrollTo({
        x: this.state.scrollX - deviceWidth * 0.8,
        y: 0,
        animated: "true",
      });
    else {
      this.bounce("right");
    }
  }

  scrollToNext(length) {
    if (this.state.scrollX < (length - 1) * deviceWidth * 0.8)
      this._carousel.scrollTo({
        x: this.state.scrollX + deviceWidth * 0.8,
        y: 0,
        animated: "true",
      });
    else {
      this.bounce("left");
    }
  }

  bounce(dir) {
    Animated.timing(this.springVal, {
      toValue: dir === "left" ? -10 : 10,
      duration: 100,
      useNativeDriver: true,
    }).start(() => {
      Animated.spring(this.springVal, {
        toValue: 0,
        friction: 5,
        tension: 10,
        useNativeDriver: true,
      }).start();
    });
  }

  _renderItem = ({ item }) => {
    if (this.props.fullScreenMode)
      return (
        <View
          key={item.UrunResimBuyuk}
          onPress={() => this.props.fullScreenCarousel()}
          style={{
            width: deviceWidth,
            alignItems: "center",
            flexDirection: "column",
            justifyContent: "center",
          }}
        >
          <SmartImage
            source={{ uri: item.UrunResimBuyuk }}
            resizeMode="contain"
            parallax={true}
            parallaxProps
            fullWidth={true}
            style={{
              width: deviceWidth,
              height: deviceWidth * 0.656,
              resizeMode: "contain",
            }}
          />
        </View>
      );
    else
      return (
        <TouchableOpacity
          key={item.UrunResimBuyuk}
          onPress={() => this.props.fullScreenCarousel()}
          style={{
            width: this.props.fullScreenMode ? deviceWidth * 1 : deviceWidth * 0.8,
            alignItems: "center",
            flexDirection: "column",
            justifyContent: "center",
          }}
        >
          <SmartImage
            source={{ uri: item.UrunResimBuyuk }}
            resizeMode="contain"
            style={{
              width: this.props.fullScreenMode ? deviceWidth * 1 : deviceWidth * 0.8,
              height: ((this.props.fullScreenMode ? deviceWidth * 1 : deviceWidth * 0.8) * 9) / 16,
              resizeMode: "contain",
            }}
          />
        </TouchableOpacity>
      );
  };

  render() {
    var imageSources = [];
    if (this.props.details?.UrunResmi != null) {
      imageSources = new Array({ UrunResimBuyuk: this.props.details?.UrunResmi });
    } else if (this.props.details?.UrunResimleri?.DefaultResim != null) {
      imageSources = new Array(this.props.details.UrunResimleri.DefaultResim);
      if (imageSources && this.props.details?.UrunResimleri?.ilDigerResimler != null) {
        imageSources = imageSources.concat(this.props.details.UrunResimleri.ilDigerResimler);
      }
    }

    if (imageSources.length > 0) {
      const numItems = imageSources.length;

      let barArray = [];
      imageSources.forEach((item, i) => {
        const index = this.state.index;
        const thisBar = (
          <View
            key={`bar${i}`}
            style={[
              styles.track,
              !this.props.fullScreenMode
                ? {
                    height: 8,
                    width: 8,
                    borderRadius: 4,
                    marginLeft: BAR_SPACE,
                    backgroundColor: i === index ? colors.primary_dark : colors.light_gray,
                  }
                : {
                    height: moderateScale(2, 0.5),
                    width: (deviceWidth * 0.9 - BAR_SPACE * (numItems - 1)) / numItems,
                    maxWidth: moderateScale(30),
                    marginLeft: BAR_SPACE,
                    backgroundColor: i === index ? colors.primary_dark : colors.light_gray,
                  },
            ]}
          />
        );
        barArray.push(thisBar);
      });

      return (
        <View
          style={[
            {
              flexDirection: "column",
              alignItems: "center",
              paddingTop: moderateScale(10, 0.5),
              height: this.props.fullScreenMode ? deviceHeight * 0.7 : null,
              justifyContent: "center",
              paddingBottom: this.props.fullScreenMode ? moderateScale(50, 0.5) : 0,
            },
            this.props.containerStyle,
          ]}
        >
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <Carousel
              ref={c => {
                this._carousel = c;
              }}
              data={imageSources}
              renderItem={this._renderItem}
              sliderWidth={deviceWidth}
              itemWidth={this.props.fullScreenMode ? deviceWidth * 1 : deviceWidth * 0.8}
              inactiveSlideScale={0.8}
              loop={false}
              inactiveSlideOpacity={0}
              activeSlideAlignment={"center"}
              onSnapToItem={index => this.setState({ index: index })}
            />
            {!this.props.fullScreenMode && (
              <TouchableOpacity
                onPress={() => this._carousel.snapToNext()}
                style={{
                  width: deviceWidth * 0.1,
                  alignItems: "center",
                  paddingVertical: moderateScale(20, 0.5),
                  position: "absolute",
                  right: moderateScale(20, 0.5),
                }}
              >
                <Image
                  source={require("@assets/images/arrow-right.png")}
                  style={{ width: 16, height: 16, tintColor: colors.light_gray, marginLeft: 10 }}
                />
              </TouchableOpacity>
            )}
            {!this.props.fullScreenMode && (
              <TouchableOpacity
                onPress={() => this._carousel.snapToPrev()}
                style={{
                  width: deviceWidth * 0.1,
                  alignItems: "center",
                  paddingVertical: moderateScale(20, 0.5),
                  position: "absolute",
                  left: moderateScale(20, 0.5),
                }}
              >
                <Image
                  source={require("@assets/images/left-arrow-mini.png")}
                  style={{ width: 16, height: 16, tintColor: colors.light_gray, marginRight: 10 }}
                />
              </TouchableOpacity>
            )}
          </View>
          <View
            style={[
              styles.barContainer,
              this.props.fullScreenMode && { position: "absolute", bottom: moderateScale(10, 0.5) },
            ]}
          >
            {barArray}
          </View>
        </View>
      );
    } else return null;
  }
}

const styles = ScaledSheet.create({
  sliderTitle: {
    backgroundColor: "rgba(0,0,0,0.2)",
    width: deviceWidth,
    height: (deviceWidth * 9) / 16,
    alignContent: "center",
    textAlign: "center",
    color: "white",
    position: "absolute",
    fontSize: 22,
    padding: 10,
    top: 0,
    textShadowColor: "gray",
    textShadowRadius: 6,
    textShadowOffset: {
      width: 1,
      height: 1,
    },
  },
  container: {
    flex: 1,
    height: deviceHeight,
    backgroundColor: "white",
    alignItems: "center",
    justifyContent: "center",
  },
  barContainer: {
    paddingHorizontal: "10@ms0.5",
    flexDirection: "row",
    justifyContent: "center",
    paddingBottom: moderateScale(10, 0.5),
    paddingTop: moderateScale(20, 0.5),
  },
  track: {
    backgroundColor: "white",
    overflow: "hidden",
    height: 2,
  },
  bar: {
    backgroundColor: "gold",
    height: 2,
  },
});
