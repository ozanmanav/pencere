import React, { Component } from "react";
import {
  View,
  TouchableOpacity,
  SafeAreaView,
  StyleSheet,
  Dimensions,
  Modal,
  FlatList,
} from "react-native";
import { Text } from "@components";
import colors from "@common/styles/colors";
import Icon from "react-native-vector-icons/FontAwesome";

const win = Dimensions.get("window");

export default class ProductCampaignsModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isVisible: false,
      items: [],
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.isVisible !== prevProps.isVisible)
      this.setState({
        isVisible: this.props.isVisible,
      });
  }

  close() {
    this.setState({
      isVisible: false,
    });
  }

  toggleVisibility = items => {
    this.setState({
      isVisible: !this.state.isVisible,
      items,
    });
  };

  renderContent(content) {
    var contentClean = content.replace(/["”“]+/g, "");
    const textArray = contentClean.split("\r\n");
    return (
      <View style={{ marginTop: 10 }}>
        {textArray.map(item => {
          return <Text style={{ fontFamily: "Poppins", fontSize: 12 }}>{item}</Text>;
        })}
      </View>
    );
  }
  render() {
    if (this.state.isVisible) {
      return (
        <Modal
          animationType="slide"
          transparent={true}
          presentationStyle="overFullScreen"
          visible={this.state.isVisible}
          onRequestClose={() => {
            this.setState({
              isVisible: false,
            });
          }}
        >
          <SafeAreaView style={styles.container}>
            <View
              style={{
                flexDirection: "row",
                backgroundColor: colors.primary_dark,
                height: 50,
                paddingHorizontal: 10,
                alignItems: "center",
              }}
            >
              <TouchableOpacity
                style={{ width: 26, height: 26, marginRight: 5 }}
                onPress={() => this.close()}
              >
                <Icon name="times-circle" size={26} color="rgba(255, 255, 255, 0.8)" />
              </TouchableOpacity>
              <Text
                style={{
                  fontFamily: "Poppins-Medium",
                  fontSize: 16,
                  color: "white",
                  lineHeight: 16,
                  marginTop: 8,
                }}
              >
                Kampanyalar
              </Text>
            </View>
            <FlatList
              style={{ backgroundColor: "white" }}
              data={this.state.items}
              contentContainerStyle={{ paddingBottom: 40 }}
              renderItem={({ item }) => {
                return (
                  <View
                    style={{
                      borderBottomColor: colors.light_gray,
                      borderBottomWidth: 1,
                      padding: win.width * 0.05,
                      marginBottom: 15,
                    }}
                  >
                    <Text style={{ fontFamily: "Poppins-Medium", fontSize: 16, color: "#444" }}>
                      {item.Konu}
                    </Text>
                    {this.renderContent(item.Icerik)}
                  </View>
                );
              }}
            />
          </SafeAreaView>
        </Modal>
      );
    } else return null;
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.primary_dark,
    width: win.width,
    height: win.height,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.3,
    shadowRadius: 1,
    elevation: 1,
  },
});
