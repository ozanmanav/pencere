import React, { Component } from "react";
import { View, Dimensions } from "react-native";
import { connect } from "react-redux";
const win = Dimensions.get("window");
const ww = win.width / 100;
import colors from "@common/styles/colors";
import { Text } from "@components";
import { moderateScale } from "react-native-size-matters";

class PricePanel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      TESFiyatlariGoster: false,
      OzelFiyatlariGoster: false,
      FirsatFiyatiVar: false,
      TESF: "",
      TESFKDV: "",
      OzelFiyat: "",
      OzelFiyatKDV: "",
      Firsat: "",
      FirsatKDV: "",
    };
  }

  updatePrices = () => {
    //toggles
    const TESFiyatlariGoster = this.props.details.info?.FiyatBilgisi?.TESFiyatlariGoster;
    const OzelFiyatlariGoster = this.props.details.info?.FiyatBilgisi?.OzelFiyatlariGoster;
    const FirsatFiyatiVar = this.props.details.info?.FiyatBilgisi?.FirsatFiyatiVar;

    //PROGRAMMER'S DILEMMA
    const cc = this.props.currency === "TL" ? "TRY" : this.props.currency;
    const DEFOF = this.props.details?.info?.FiyatBilgisi["DefoluFiyat" + cc];
    const DEFOFKDV = this.props.details?.info?.FiyatBilgisi["DefoluFiyat" + cc + "_KDV"];
    const TESF = this.props.details?.info?.FiyatBilgisi["TESF" + cc];
    const TESFKDV = this.props.details?.info?.FiyatBilgisi["TESF" + cc + "_KDV"];
    const OzelFiyat = this.props.details?.info?.FiyatBilgisi["OzelFiyat" + cc];
    const OzelFiyatKDV = this.props.details?.info?.FiyatBilgisi["OzelFiyat" + cc + "_KDV"];
    const Firsat = this.props.details?.info?.FiyatBilgisi["Firsat" + cc];
    const FirsatKDV =
      cc === "USD"
        ? this.props.details?.info?.FiyatBilgisi.FirsaUSD_KDV //typo in service
        : this.props.details?.info?.FiyatBilgisi["Firsat" + cc + "_KDV"];

    this.setState(
      {
        TESFiyatlariGoster,
        OzelFiyatlariGoster,
        FirsatFiyatiVar,
        TESF,
        TESFKDV,
        OzelFiyat,
        OzelFiyatKDV,
        Firsat,
        FirsatKDV,
        DEFOF,
        DEFOFKDV,
      },
      () => console.log("after updatePrices", this.state),
    );
  };

  componentDidUpdate(prevProps) {
    if (
      this.props.details.info.FiyatBilgisi !== prevProps.details.info.FiyatBilgisi ||
      this.props.currency !== prevProps.currency
    ) {
      this.updatePrices();
    }
  }

  componentDidMount() {
    this.updatePrices();
  }

  render() {
    const {
      OzelFiyatlariGoster,
      TESFiyatlariGoster,
      TESF,
      TESFKDV,
      OzelFiyat,
      OzelFiyatKDV,
      Firsat,
      FirsatKDV,
      DEFOF,
      DEFOFKDV,
    } = this.state;
    const FiyatBilgisi = this.props.details.info?.FiyatBilgisi;

    //FirsatFiyatiVar doesn't come correctly from service
    const FirsatFiyatiVar = typeof Firsat !== "undefined" && Firsat !== "";
    return (typeof FiyatBilgisi !== "undefined" && (
      <View
        style={[
          {
            flexDirection: "row",
            backgroundColor: "#f5f5f5",
            paddingHorizontal: ww * 4,
            paddingTop: ww * 2,
            paddingBottom: ww,
            flex: 1,
          },
          this.props.containerStyle,
        ]}
      >
        <View style={{ flexDirection: "column" }}>
          {typeof TESF !== "undefined" && TESFiyatlariGoster && (
            <Text
              fontScale={0.6}
              style={{
                fontFamily: "Poppins-Light",
                fontSize: 15,
                color: OzelFiyatlariGoster || FirsatFiyatiVar ? "#454545" : colors.price_color,
              }}
            >
              {"TES Fiyatı"}
            </Text>
          )}
          {typeof OzelFiyat !== "undefined" && OzelFiyatlariGoster && (
            <Text
              fontScale={0.6}
              style={{
                fontSize: 15,
                fontFamily: "Poppins-Light",
                color: FirsatFiyatiVar || DEFOF != null ? "#454545" : colors.price_color,
              }}
            >
              {FiyatBilgisi.OzelFiyatEtiket === "Size Özel Fiyat"
                ? "Size Özel"
                : FiyatBilgisi.OzelFiyatEtiket}
            </Text>
          )}
          {typeof Firsat !== "undefined" && FirsatFiyatiVar && (
            <Text
              fontScale={0.6}
              style={{
                fontFamily: "Poppins-Light",
                fontSize: 15,
                color: DEFOF != null ? "#454545" : colors.price_color,
              }}
            >
              {"Fırsat Fiyatı"}
            </Text>
          )}
          {typeof DEFOF !== "undefined" && (
            <Text
              fontScale={0.6}
              style={{
                fontFamily: "Poppins-Light",
                fontSize: 15,
                color: colors.price_color,
              }}
            >
              {"Defolu Fiyatı"}
            </Text>
          )}
        </View>
        <View style={{ flexDirection: "column", flex: 1 }}>
          {typeof TESF !== "undefined" && TESFiyatlariGoster && (
            <View style={{ flexDirection: "row", flex: 1 }}>
              <Text
                fontScale={0.6}
                style={{
                  fontFamily: "Poppins",
                  fontSize: 15,
                  marginHorizontal: moderateScale(5, 0.5),
                }}
              >
                {" "}
                :{" "}
              </Text>
              {OzelFiyatlariGoster || FirsatFiyatiVar ? (
                <View style={{ flexDirection: "column" }}>
                  <Text
                    fontScale={0.6}
                    style={{
                      fontFamily: "Poppins-Light",
                      fontSize: 15,
                      color: "#454545",
                    }}
                  >
                    {TESFKDV}
                  </Text>
                </View>
              ) : (
                <View style={{ flex: 1, flexDirection: "column" }}>
                  <Text
                    fontScale={0.6}
                    style={{
                      fontSize: 18,
                      fontFamily: "Poppins-Medium",
                      color: colors.price_color,
                      lineHeight: FirsatFiyatiVar || OzelFiyatlariGoster ? null : 20,
                      marginTop: FirsatFiyatiVar || OzelFiyatlariGoster ? 0 : 4,
                    }}
                  >
                    {TESF + " + KDV"}
                  </Text>
                  <Text
                    fontScale={0.6}
                    style={{
                      fontFamily: "Poppins-Light",
                      fontSize: 13,
                      color: "#464646",
                      textDecorationLine:
                        OzelFiyatlariGoster || FirsatFiyatiVar ? "line-through" : "none",
                      top: moderateScale(-3, 0.5),
                    }}
                  >
                    {"(KDV Dahil " + TESFKDV + ")"}
                  </Text>
                </View>
              )}
            </View>
          )}
          {typeof OzelFiyat !== "undefined" && OzelFiyatlariGoster && (
            <View style={{ flexDirection: "row", flex: 1, alignSelf: "flex-start" }}>
              <Text
                fontScale={0.6}
                style={{
                  fontFamily: "Poppins",
                  fontSize: 15,
                  alignSelf: "baseline",
                  marginHorizontal: moderateScale(5, 0.5),
                }}
              >
                :
              </Text>
              {FirsatFiyatiVar || DEFOF != null ? (
                <View style={{ flex: 1, flexDirection: "column" }}>
                  <Text
                    fontScale={0.6}
                    style={{
                      fontSize: 15,
                      fontFamily: FirsatFiyatiVar || DEFOF != null ? "Poppins-Light" : "Poppins",
                      color: FirsatFiyatiVar || DEFOF != null ? "#454545" : colors.price_color,
                      textDecorationColor: "#adadad",
                      textDecorationLine:
                        FirsatFiyatiVar || DEFOF != null ? "line-through" : "none",
                    }}
                  >
                    {OzelFiyatKDV}
                  </Text>
                </View>
              ) : (
                <View style={{ flex: 1, flexDirection: "column" }}>
                  <Text
                    fontScale={0.6}
                    style={{
                      fontSize: FirsatFiyatiVar || DEFOF != null ? 15 : 18,
                      fontFamily: FirsatFiyatiVar || DEFOF != null ? "Poppins" : "Poppins-Medium",
                      color: FirsatFiyatiVar || DEFOF != null ? "#454545" : colors.price_color,
                      textDecorationLine:
                        FirsatFiyatiVar || DEFOF != null ? "line-through" : "none",
                      textDecorationColor: "#7d7d7d",
                      lineHeight: FirsatFiyatiVar || OzelFiyatlariGoster ? null : 20,
                      marginTop: FirsatFiyatiVar || OzelFiyatlariGoster ? 0 : 4,
                    }}
                  >
                    {OzelFiyat + " + KDV"}
                  </Text>
                  <Text
                    fontScale={0.6}
                    style={{
                      fontSize: 13,
                      fontFamily: "Poppins-Light",
                      color: "#464646",
                      top: moderateScale(-3, 0.5),
                    }}
                  >
                    {"(KDV Dahil " + OzelFiyatKDV + ")"}
                  </Text>
                </View>
              )}
            </View>
          )}
          {typeof Firsat !== "undefined" && FirsatFiyatiVar != null && (
            <View
              style={{
                flexDirection: "row",
                flex: 1,
                alignItems: "flex-start",
              }}
            >
              <Text
                fontScale={0.6}
                style={{
                  fontSize: DEFOF != null ? 15 : 18,
                  fontFamily: DEFOF != null ? "Poppins" : "Poppins-Medium",
                  color: DEFOF != null ? "#454545" : colors.price_color,
                  textDecorationLine: DEFOF != null ? "line-through" : "none",
                  textDecorationColor: "#7d7d7d",
                  lineHeight: DEFOF != null ? null : 20,
                  marginTop: DEFOF != null ? 0 : 4,
                }}
              >
                {" "}
                :{" "}
              </Text>
              <View style={{ flex: 1, flexDirection: "column" }}>
                <Text
                  fontScale={0.6}
                  style={{
                    fontFamily: "Poppins-Medium",
                    fontSize: 18,
                    lineHeight: 20,
                    marginTop: 4,
                    color: colors.price_color,
                  }}
                >
                  {Firsat + " + KDV"}
                </Text>
                <Text
                  fontScale={0.6}
                  style={{
                    fontFamily: "Poppins-Light",
                    fontSize: 13,
                    color: "#464646",
                    top: moderateScale(-3, 0.5),
                  }}
                >
                  {"(KDV Dahil " + FirsatKDV + ")"}
                </Text>
              </View>
            </View>
          )}
          {DEFOFKDV != null && (
            <View
              style={{
                flexDirection: "row",
                flex: 1,
                alignItems: "flex-start",
              }}
            >
              <Text
                fontScale={0.6}
                style={{
                  fontFamily: "Poppins",
                  fontSize: 15,
                  marginHorizontal: moderateScale(5, 0.5),
                  alignSelf: "baseline",
                }}
              >
                :
              </Text>
              <View style={{ flex: 1, flexDirection: "column" }}>
                <Text
                  fontScale={0.6}
                  style={{
                    fontFamily: "Poppins-Medium",
                    fontSize: 18,
                    lineHeight: 20,
                    marginTop: 4,
                    color: colors.price_color,
                  }}
                >
                  {DEFOF + " + KDV"}
                </Text>
                <Text
                  fontScale={0.6}
                  style={{
                    fontFamily: "Poppins-Light",
                    fontSize: 13,
                    color: "#464646",
                    top: moderateScale(-3, 0.5),
                  }}
                >
                  {"(KDV Dahil " + DEFOFKDV + ")"}
                </Text>
              </View>
            </View>
          )}
        </View>
      </View>
    ): null);
  }
}
function mapStateToProps(state) {
  return {
    details: state.details,
  };
}

export default connect(mapStateToProps, null)(PricePanel);
