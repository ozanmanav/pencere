import React, { Component } from "react";
import { View, TouchableOpacity, StyleSheet, Dimensions, FlatList } from "react-native";
import { Text, ModalBox } from "@components";
import colors from "@common/styles/colors";
import Icon from "react-native-vector-icons/FontAwesome";

const win = Dimensions.get("window");

export default class SpecsModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      specs: [],
    };
  }

  toggleVisibility = (specs) => {
    this.setState({
      specs,
    });
    this.modalBox.toggleVisibility();
  };

  render() {
    return (
      <ModalBox
        ref={(c) => (this.modalBox = c)}
        animationType="slide"
        presentationStyle="fullScreen"
        statusBarHidden={false}
        scrollable={true}
      >
        <View style={styles.container}>
          <View
            style={{
              flexDirection: "row",
              backgroundColor: colors.primary_dark,
              height: 50,
              paddingHorizontal: 10,
              alignItems: "center",
            }}
          >
            <TouchableOpacity
              style={{ width: 26, height: 26, marginRight: 5 }}
              onPress={() => this.toggleVisibility()}
            >
              <Icon name="times-circle" size={26} color="rgba(255, 255, 255, 0.5)" />
            </TouchableOpacity>
            <Text
              style={{
                fontFamily: "Poppins-Medium",
                fontSize: 16,
                color: "white",
                lineHeight: 16,
                marginTop: 8,
              }}
            >
              Teknik Özellikler
            </Text>
          </View>
          <FlatList
            style={{ backgroundColor: "white" }}
            data={this.state.specs}
            contentContainerStyle={{ paddingBottom: 40 }}
            renderItem={({ item }) => {
              return (
                <View
                  style={{
                    borderBottomColor: colors.light_gray,
                    borderBottomWidth: 1,
                  }}
                >
                  <View style={{ padding: 15 }}>
                    <Text>{item.OzellikTanim}</Text>
                    <Text style={{ marginTop: 4 }}>{item.OzellikDeger}</Text>
                  </View>
                </View>
              );
            }}
          />
        </View>
      </ModalBox>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.primary_dark,
    width: win.width,
    height: win.height,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.3,
    shadowRadius: 1,
    elevation: 1,
  },
});
