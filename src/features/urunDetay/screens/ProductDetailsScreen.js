import React, { Component } from "react";
import {
  Platform,
  Modal,
  Clipboard,
  Animated,
  FlatList,
  View,
  Image,
  Keyboard,
  Dimensions,
  TextInput,
  LayoutAnimation,
  TouchableOpacity,
  Alert,
  StatusBar,
} from "react-native";
import {
  AnimatedHeader,
  Text,
  BottomPicker,
  ListModal,
  AddToFavoriteDialog,
  AnimatedBasePage,
} from "@components";
import {
  ProductCampaignsModal,
  CrossSellModal,
  SpecsModal,
  PriceRequestModal,
  ProductCarousel,
  PricePanel,
  VadeSecenekleriModal,
} from "@features/urunDetay/components";

import { connect } from "react-redux";
import { getDetailsFromApi } from "@redux/actions";
import {
  setCart,
  addFavorite,
  removeFavorite,
  setSafeViewBackgroundColor,
  showToast,
} from "@redux/actions";

import Toast from "react-native-easy-toast";
import colors from "@common/styles/colors";
import { convertToUsd, getEndPoint, getWidth } from "@common/Utils";
import { scale, moderateScale, ScaledSheet } from "react-native-size-matters";
import analytics from "@react-native-firebase/analytics";
var visilabsManager = require("@app/visilabsManager");
import bugsnag from "@common/bugsnag_config";
import Interactable from "react-native-interactable";

const win = Dimensions.get("window");
const ww = win.width / 100;

class ProductDetailsScreen extends Component {
  updateScroll = () => {
    console.log("scrollview", this.basePage.scrollView);
    if (this.basePage.scrollView)
      this.basePage.scrollView.scrollTo({
        x: 0,
        y: this.beforeScroll + (this.beforeScroll !== this.scrollY ? moderateScale(60, 0.5) : 0),
        animated: true,
      });
  };

  listHeight = 0;

  constructor(props) {
    super(props);
    this.scrollY = 0;
    this.state = {
      defolular: [],
      keyboardPadding: 0,
      fsCarousel: false,
      isSpecsCollapsed: true,
      isDetailsCollapsed: true,
      ratios: [],
      arttirKazan: false,
      isFavorite: false,
      currency: "TRY",
      progress: false,
      details: [],
      seg: 0,
      isShowToTop: false,
      scrollY: new Animated.Value(0),
      number: "1",
      seg1height: 0,
      seg2height: 0,
      contentHeaderHeight: 0,
      isFetching: true,
    };
  }

  _keyboardDidShow = e => {
    this.setState({
      keyboardVisible: true,
      keyboardHeight: e.endCoordinates.height,
    });
  };

  _keyboardDidHide = e => {
    this.updateScroll();
    this.setState({
      keyboardVisible: false,
      keyboardHeight: e.endCoordinates.height,
    });
  };

  renderArttirKazanLogo = () => {
    return (
      <View
        style={{
          position: "absolute",
          right: 0,
          top: -10,
          width: 64,
          height: 64,
          backgroundColor: "#d55749",
          justifyContent: "center",
          alignItems: "center",
          shadowColor: "#000",
          shadowOffset: { width: 0, height: 2 },
          shadowOpacity: 0.3,
          shadowRadius: 2,
          elevation: 1,
        }}
      >
        <View style={{ flexDirection: "row", marginTop: 6, marginBottom: 4 }}>
          <Image
            source={require("@assets/images/plus_white.png")}
            style={{
              width: 10,
              height: 10,
              tintColor: "white",
              resizeMode: "contain",
              opacity: 0.25,
              marginRight: 2,
            }}
          />
          <Image
            source={require("@assets/images/plus_white.png")}
            style={{
              width: 10,
              height: 10,
              tintColor: "white",
              resizeMode: "contain",
              opacity: 0.5,
              marginRight: 2,
            }}
          />
          <Image
            source={require("@assets/images/plus_white.png")}
            style={{
              width: 10,
              height: 10,
              tintColor: "white",
              resizeMode: "contain",
              opacity: 0.75,
              marginRight: 2,
            }}
          />
          <Image
            source={require("@assets/images/plus_white.png")}
            style={{
              width: 10,
              height: 10,
              tintColor: "white",
              resizeMode: "contain",
              opacity: 1,
            }}
          />
        </View>
        <Text
          fontScale={0.5}
          style={{
            color: "white",
            fontFamily: "Poppins-Medium",
            fontSize: 10,
            lineHeight: 10,
            margin: 0,
            padding: 0,
            marginTop: 2,
            alignSelf: "center",
            textAlign: "center",
          }}
        >
          ARTTIR KAZAN ÜRÜNÜ
        </Text>
      </View>
    );
  };

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      this.props.navigation.state.params.UrunKodu !== prevProps.navigation.state.params.UrunKodu
    ) {
      this.getInitialDetails();
    }

    if (prevProps.favorites !== this.props.favorites) {
      var isFavorite = false;
      this.props.favorites.items.forEach(item => {
        if (item.urunKodu === this.props.navigation.state.params.UrunKodu) {
          isFavorite = true;
        }
      });
      this.setState({ isFavorite });
    }
  }

  getInitialDetails = () => {
    this.props
      .getDetails(
        false,
        this.props.navigation.state.params.UrunKodu,
        this.props.navigation.state.params?.pb === "TL" ? "TRY" : null,
        false,
        false,
        this.props.navigation.state.params.SeriNo,
      )
      .then(responseJson => {
        if (responseJson.FiyatBilgisi.DefaultPb === "TL") {
          this.props.getDetails(
            false,
            this.props.navigation.state.params.UrunKodu,
            "TRY",
            false,
            false,
            this.props.navigation.state.params.SeriNo,
          );
        }
        if (
          this.props.session.info.SadeceUlusalPB &&
          !(this.state.currency === "TL" || this.state.currency === "TRY")
        )
          this.props.getDetails(
            false,
            this.props.navigation.state.params.UrunKodu,
            "TRY",
            false,
            false,
            this.props.navigation.state.params.SeriNo,
          );
        try {
          //send event to GA
          const product = {
            item_id: responseJson.UrunKodu,
            item_name: responseJson.UrunAciklamasi,
            item_category: responseJson.UrunSinifText,
            brand: responseJson.UrunMarkaText,
            price: this.getPrice(),
            currency: this.state.currency,
          };
          analytics().logEvent("view_item", product);
          const product_ga = {
            id: responseJson.UrunKodu,
            name: responseJson.UrunAciklamasi,
            category: responseJson.UrunSinifText,
            brand: responseJson.UrunMarkaText,
            price: !isNaN(convertToUsd(this.getPrice(), this.state.currency))
              ? convertToUsd(this.getPrice(), this.state.currency)
              : 0,
          };
          var data = {
            "OM.pv": product_ga.id,
            "OM.pn": product_ga.name,
            "OM.ppr": String(convertToUsd(this.getPrice(), this.state.currency)).replace(".", ","),
            "OM.pv.1": product_ga.brand,
            "OM.inv": "Number of items in stock",
          };
          visilabsManager.customEvent("Product View", data);
        } catch (error) {
          console.log("trackDetails error", error);
          bugsnag.notify(error, report => {
            report.errorClass = "trackDetails";
          });
        }
      })
      .catch(msg => {
        this.setState({ isFetching: false });
        console.log("details error", msg);
        bugsnag.notify(msg, report => {
          report.errorClass = "getDetails";
          report.metadata = {
            uk: this.props.navigation.state.params?.UrunKodu || "undefined",
          };
        });
        Alert.alert(
          "Sunucu Hatası",
          "Ürün detaylarına ulaşılamadı.",
          [{ text: "Tamam", onPress: () => this.props.navigation.goBack() }],
          { cancelable: false },
        );
      });
  };

  componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener("keyboardWillShow", this._keyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener("keyboardWillHide", this._keyboardDidHide);

    this.setState({ isFetching: true });
    this.getInitialDetails();
    var isFavorite = false;
    this.props.favorites.items.forEach(item => {
      if (item.urunKodu === this.props.navigation.state.params.UrunKodu) {
        isFavorite = true;
      }
    });
    this.setState({ isFavorite });
  }

  decreaseNumber = () => {
    this.setState({
      number: Number(this.state.number) > 1 ? (Number(this.state.number) - 1).toString() : "1",
    });
  };

  increaseNumber = () => {
    this.setState({
      number: (Number(this.state.number) + 1).toString(),
    });
  };

  getPrice = currency => {
    if (currency == null) {
      currency = this.state.currency;
    }
    const fiyatBilgisi = this.props.details.info.FiyatBilgisi;
    const cc = this.state.currency === "TL" ? "TRY" : this.state.currency;
    const TESF = fiyatBilgisi["TESF" + cc];
    const OzelFiyat = fiyatBilgisi["OzelFiyat" + cc];
    const Firsat = fiyatBilgisi["Firsat" + cc];

    var price = Firsat || OzelFiyat || TESF;
    if (this.props.session.info.SadeceUlusalPB) {
      price = fiyatBilgisi.FirsatTRY || fiyatBilgisi.OzelFiyatTRY;
    }
    if (price != null)
      price = parseFloat(
        price
          .split(" ")[0]
          .replace(".", "")
          .replace(",", "."),
      );
    else price = 0;
    console.log("look price", price);
    return price;
  };

  addToCart = () => {
    if (Number(this.state.number) > 0) {
      this.setState({
        progress: true,
      });
      if (this.props.details.info.UrunCrossSellVar) {
        this.setState({
          progress: false,
        });
        this.crossSellModal
          .getWrappedInstance()
          .toggleVisibility(this.props.details.info.IlCrossSellPaketleri, this.state.number, true);
      } else {
        console.log(
          "add to cart",
          getEndPoint("CART_URL") +
            "?Si=" +
            this.props.session.info.SessionID +
            "&func=8" +
            "&uk=" +
            this.props.navigation.state.params.UrunKodu +
            "&adt=" +
            this.state.number +
            "&sn=" +
            this.props.details.info.UrunSeriNo,
        );
        fetch(
          getEndPoint("CART_URL") +
            "?Si=" +
            this.props.session.info.SessionID +
            "&func=8" +
            "&uk=" +
            this.props.navigation.state.params.UrunKodu +
            "&adt=" +
            this.state.number +
            "&sn=" +
            this.props.details.info.UrunSeriNo,
          {
            method: "POST",
          },
        )
          .then(response => response.json())
          .then(responseJson => {
            console.log("addToCart response", responseJson);
            if (responseJson.MesajTipi === "S") {
              this.props.setCart(responseJson);
              this.props.showToast("Ürün sepete eklendi");
              try {
                analytics().logEvent("add_to_cart", {
                  quantity: Number(this.state.number),
                  item_category: this.props.details.info.UrunSinifText,
                  item_name: this.props.details.info.UrunAciklamasi,
                  item_id: this.props.details.info.UrunKodu,
                  price: !isNaN(this.getPrice()) ? this.getPrice() : 0,
                  currency: this.state.currency,
                });
                const product = {
                  id: this.props.details.info.UrunKodu,
                  name: this.props.details.info.UrunAciklamasi,
                  category: this.props.details.info.UrunSinifText,
                  brand: this.props.details.info.UrunMarkaText,
                  price: !isNaN(this.getPrice("USD")) ? this.getPrice("USD") : 0,
                  quantity: Number(this.state.number),
                };
                var data = {
                  "OM.pbid": this.props.cart.cartInfo.IlKullaniciSepetleri[0].SepetKodu,
                  "OM.pb": product.id,
                  "OM.pu": String(product.quantity),
                  "OM.ppr": String(product.quantity * product.price).replace(".", ","),
                };
                visilabsManager.customEvent("Cart", data);
              } catch (error) {
                console.log("trackAddToCart error", error);
              }
            } else {
              this.props.showToast("Bir hata oluştu lütfen tekrar deneyin");
            }
            this.setState({
              progress: false,
            });
          })
          .catch(error => {
            this.props.showToast("Bir hata oluştu lütfen tekrar deneyin");
            console.log("addToCart error", error);
            this.setState({
              progress: false,
            });
          });
      }
    } else {
      Alert.alert("Hata", "Ürün adedi 0'dan büyük olmalıdır.");
    }
  };

  getMinHeight() {
    let height = win.height - this.state.contentHeaderHeight - moderateScale(120, 0.5);
    if (Platform.OS === "ios") height = height - 30;
    return height;
  }

  renderCrossSellWidget = () => {
    return (
      <TouchableOpacity
        onPress={() =>
          this.crossSellModal
            .getWrappedInstance()
            .toggleVisibility(this.props.details.info.IlCrossSellPaketleri, this.state.number)
        }
        style={{
          flexDirection: "row",
          backgroundColor: "white",
          marginTop: 10,
          flex: 1,
          borderRadius: 4,
          borderWidth: 1,
          borderColor: "#26a1da",
          padding: scale(10),
        }}
      >
        <Image
          source={{
            uri: this.props.details.info.IlCrossSellPaketleri[0].AltUrunTanimi.UrunResimThum,
          }}
          style={{
            paddingHorizontal: scale(8),
            width: scale(70),
            height: scale(70),
            resizeMode: "contain",
          }}
        />
        <View style={{ flexDirection: "row", flex: 1, marginLeft: scale(10) }}>
          <View style={{ flexDirection: "column" }}>
            <Text
              fontScale={0.5}
              style={{
                fontFamily: "Poppins-Medium",
                color: "#26a1da",
                fontSize: 14,
              }}
            >
              Avantajlı Paket Önerisi
            </Text>
            <Text fontScale={0.5}>
              <Text fontScale={0.5} style={{ fontFamily: "Poppins", color: "black", fontSize: 14 }}>
                {this.props.details.info.IlCrossSellPaketleri[0].AltUrunTanimi.IliskiAciklama + " "}
              </Text>
              <Text
                fontScale={0.5}
                style={{
                  fontFamily: "Poppins",
                  color: "black",
                  fontSize: 14,
                  textDecorationLine: "line-through",
                }}
              >
                {this.props.details.info.IlCrossSellPaketleri[0].AltUrunTanimi.UrunFiyati}
              </Text>
              <Text fontScale={0.5} style={{ fontFamily: "Poppins", color: "black", fontSize: 14 }}>
                {" "}
                yerine{" "}
              </Text>
              <Text
                fontScale={0.5}
                style={{
                  fontFamily: "Poppins",
                  color: colors.price_color,
                  fontSize: 14,
                }}
              >
                {this.props.details.info.IlCrossSellPaketleri[0].AltUrunTanimi.ManuelFiyat}
              </Text>
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  scrollToFocusedInput = offset => {
    this.beforeScroll = this.scrollY;
    if (offset > win.height - this.state.keyboardHeight - AnimatedHeader.height) {
      this.setState({
        keyboardPadding: offset - (win.height - this.state.keyboardHeight) + AnimatedHeader.height,
      });
      this.basePage.scrollView.scrollTo({
        x: 0,
        y: this.scrollY + offset - (win.height - this.state.keyboardHeight) + AnimatedHeader.height,
        animated: true,
      });
    }
  };

  renderContent() {
    console.log("render content");
    if (this.props.details.info?.VadeliFiyatlar?.Basarili) {
      var defaultVade = this.props.details.info.VadeliFiyatlar.Vadeler.filter(
        item => item.IsDefault,
      );
      var tekCekim = this.props.details.info.VadeliFiyatlar.Vadeler.filter(
        item => item.OdemeKosulu === "1",
      )[0];
    }
    if (
      this.props.details.info &&
      (this.props.details.info.UrunResimleri != null || this.props.details.info.UrunResmi != null)
    )
      return (
        <View>
          {this.props.details.info.UrunResimleri != null ||
          this.props.details.info.UrunResmi != null ? (
            <Animated.View style={{ backgroundColor: "white" }}>
              <View
                style={{
                  flexDirection: "row",
                  paddingHorizontal: ww * 4,
                  marginTop: moderateScale(20, 0.5),
                }}
              >
                <View style={{ flexDirection: "column", flex: 0.8 }}>
                  <View
                    style={{
                      flexDirection: "row",
                      alignItems: "center",
                      marginBottom: moderateScale(5, 0.5),
                    }}
                  >
                    <Text
                      fontScale={0.5}
                      style={{
                        color: "#a81215",
                        fontFamily: "Poppins-Bold",
                        letterSpacing: 0.5,
                        fontSize: 12,
                      }}
                    >
                      {this.props.details.info.UrunMarkaText}
                    </Text>
                    <Text
                      onPress={() => {
                        Clipboard.setString(this.props.details.info.UrunKodu);
                        this.props.showToast("Ürün kodu kopyalandı");
                      }}
                      fontScale={0.5}
                      style={{
                        marginRight: moderateScale(5, 0.4),
                        color: "#cecece",
                        fontSize: 12,
                        fontFamily: "Poppins",
                      }}
                    >
                      {" " + this.props.details.info.UrunKodu}
                    </Text>
                    <TouchableOpacity
                      onPress={() => {
                        Clipboard.setString(this.props.details.info.UrunKodu);
                        this.props.showToast("Ürün kodu kopyalandı");
                      }}
                    >
                      <Image
                        source={require("@assets/images/copy.png")}
                        style={{
                          tintColor: "#cecece",
                          width: moderateScale(12, 0.4),
                          height: moderateScale(12, 0.4),
                        }}
                      />
                    </TouchableOpacity>
                  </View>
                  <Text
                    fontScale={0.5}
                    style={{ fontSize: 18, fontFamily: "Poppins" }}
                    onPress={() => {
                      Clipboard.setString(this.props.details.info.UrunAciklamasi);
                      this.props.showToast("Ürün açıklaması kopyalandı");
                    }}
                  >
                    {this.props.details.info.UrunAciklamasi
                      ? this.props.details.info.UrunAciklamasi
                      : ""}
                  </Text>
                </View>
                <View
                  style={{
                    flex: 0.2,
                    alignItems: "flex-end",
                    justifyContent: "flex-start",
                  }}
                >
                  <TouchableOpacity
                    onPress={() => {
                      if (!this.state.isFavorite) {
                        this.addFavoriteDialog.toggleVisibility();
                      } else {
                        this.props.removeFavorite(this.props.details.info).then(msg => {
                          if (msg === "REMOVE_FAVORITE_SUCCESS") {
                            this.props.showToast("Ürün favorilerinizden çıkarıldı!");
                          }
                        });
                      }
                    }}
                  >
                    <Image
                      source={require("@assets/images/star.png")}
                      style={[
                        {
                          width: moderateScale(20, 0.5),
                          height: moderateScale(20, 0.5),
                          tintColor: "gray",
                        },
                        this.state.isFavorite && { tintColor: "#e4ca4c" },
                      ]}
                    />
                  </TouchableOpacity>
                </View>
              </View>
              <View>
                <ProductCarousel
                  fullScreenCarousel={() => {
                    this.setState({ fsCarousel: true });
                    StatusBar.setHidden(true);
                  }}
                  details={this.props.details.info}
                />
              </View>
            </Animated.View>
          ) : (
            <View style={{ width: win.width, height: (win.width * 9) / 16 }} />
          )}
          {!this.props.session.info.SadeceUlusalPB ? (
            <View
              style={{
                flexDirection: "row-reverse",
                backgroundColor: "#f5f5f5",
                alignItems: "center",
                paddingHorizontal: ww * 4,
                flex: 1,
                justifyContent: "space-between",
              }}
            >
              {!this.props.session.info.SadeceUlusalPB && (
                <TouchableOpacity
                  onPress={() =>
                    this.bottomCurrencyPicker.openPicker(null, null, Platform.OS === "android")
                  }
                  style={{
                    flexDirection: "row",
                    height: moderateScale(26, 0.5),
                    width: moderateScale(64, 0.5),
                    borderColor: "#cecece",
                    borderWidth: 1,
                    alignItems: "center",
                    justifyContent: "center",
                    marginTop: moderateScale(8, 0.5),
                    backgroundColor: "white",
                  }}
                >
                  <Text
                    fontScale={0.5}
                    style={{
                      fontFamily: "Poppins",
                      color: "gray",
                      textAlign: "center",
                      marginRight: moderateScale(5, 0.5),
                      fontSize: 12,
                    }}
                  >
                    {this.state.currency}
                  </Text>
                  <Image
                    source={require("@assets/images/down-arrow.png")}
                    style={{
                      width: moderateScale(10, 0.5),
                      height: moderateScale(10, 0.5),
                      tintColor: "#cecece",
                    }}
                  />
                </TouchableOpacity>
              )}
              <View style={{ flexDirection: "column", justifyContent: "center" }}>
                {this.props.details.info?.IlStoklar != null &&
                  this.props.details.info?.IlStoklar?.length > 0 && (
                    <View
                      style={{
                        flexDirection: "row",
                        marginTop: moderateScale(5, 0.5),
                      }}
                    >
                      <Text
                        fontScale={0.5}
                        style={{
                          fontFamily: "Poppins-Light",
                          color: this.props.details.info?.IlStoklar[0]?.Durum.toLowerCase().includes(
                            "yok",
                          )
                            ? "#7a7a7a"
                            : "#7a7a7a",
                          fontSize: 13,
                        }}
                      >
                        {this.props.details.info.IlStoklar[0].UretimYeri + ": "}
                      </Text>
                      <Text
                        style={{
                          color: this.props.details.info?.IlStoklar[0]?.Durum.toLowerCase().includes(
                            "yok",
                          )
                            ? "#e57373"
                            : "#7abf5a",
                          fontFamily: this.props.details.info?.IlStoklar[0]?.Durum.toLowerCase().includes(
                            "yok",
                          )
                            ? "Poppins"
                            : "Poppins-Medium",
                          fontSize: 13,
                        }}
                      >
                        {this.props.details.info.IlStoklar[0].Durum}
                      </Text>
                    </View>
                  )}
                {this.props.details.info?.IlStoklar != null &&
                  this.props.details.info?.IlStoklar?.length > 1 &&
                  this.props.details.info.IlStoklar[1] && (
                    <View style={{ flexDirection: "row" }}>
                      <Text
                        fontScale={0.5}
                        style={{
                          fontFamily: "Poppins-Light",
                          color: this.props.details.info?.IlStoklar[1]?.Durum.toLowerCase().includes(
                            "yok",
                          )
                            ? "#7a7a7a"
                            : "#7a7a7a",
                          fontSize: 13,
                        }}
                      >
                        {this.props.details.info.IlStoklar[1].UretimYeri + ": "}
                      </Text>
                      <Text
                        style={{
                          color: this.props.details.info?.IlStoklar[1]?.Durum.toLowerCase().includes(
                            "yok",
                          )
                            ? "#e57373"
                            : "#03A9F4",
                          fontFamily: this.props.details.info?.IlStoklar[1]?.Durum.toLowerCase().includes(
                            "yok",
                          )
                            ? "Poppins"
                            : "Poppins-Medium",
                          fontSize: 13,
                        }}
                      >
                        {this.props.details.info.IlStoklar[1].Durum}
                      </Text>
                    </View>
                  )}
              </View>
            </View>
          ) : (
            <View
              style={{
                flexDirection: "row",
                backgroundColor: "#f5f5f5",
                alignItems: "center",
                paddingHorizontal: ww * 4,
                flex: 1,
                justifyContent: "space-between",
              }}
            >
              <View style={{ flexDirection: "column" }}>
                <PricePanel
                  minHeight={this.getMinHeight()}
                  currency={this.state.currency}
                  containerStyle={{ paddingHorizontal: 0 }}
                />
                <View style={{ flexDirection: "row", width: getWidth() * 0.7 }}>
                  <TouchableOpacity
                    onPress={() => {
                      this.props
                        .getDetails(
                          true,
                          this.props.details.info.UrunKodu,
                          this.props.details.info.ParaBirimi,
                          true,
                          true,
                        )
                        .then(res => {
                          this.setState({ defolular: res }, () => {
                            this.defoluListModal.toggleVisibility();
                          });
                        })
                        .catch(err => {
                          Alert.alert("Hata", "Gösterilecek ürün bulunamadı");
                          console.log(err);
                        });
                    }}
                  >
                    {this.props.details.info?.UrunDefoluVar && (
                      <Image
                        source={require("@assets/images/defoluvar.png")}
                        style={{
                          width: 235 * 0.9,
                          height: 48 * 0.9,
                          resizeMode: "contain",
                        }}
                      />
                    )}
                  </TouchableOpacity>
                </View>
              </View>
              <View style={{ flexDirection: "column", justifyContent: "center" }}>
                {this.props.details.info?.IlStoklar != null &&
                  this.props.details.info?.IlStoklar?.length > 0 && (
                    <View
                      style={{
                        flexDirection: "row",
                        marginTop: moderateScale(5, 0.5),
                      }}
                    >
                      <Text
                        fontScale={0.5}
                        style={{
                          fontFamily: "Poppins-Light",
                          color: this.props.details.info?.IlStoklar[0]?.Durum.toLowerCase().includes(
                            "yok",
                          )
                            ? "#7a7a7a"
                            : "#7a7a7a",
                          fontSize: 13,
                        }}
                      >
                        {this.props.details.info.IlStoklar[0].UretimYeri + ": "}
                      </Text>
                      <Text
                        style={{
                          color: this.props.details.info?.IlStoklar[0]?.Durum.toLowerCase().includes(
                            "yok",
                          )
                            ? "#e57373"
                            : "#7abf5a",
                          fontFamily: this.props.details.info?.IlStoklar[0]?.Durum.toLowerCase().includes(
                            "yok",
                          )
                            ? "Poppins"
                            : "Poppins-Medium",
                          fontSize: 13,
                        }}
                      >
                        {this.props.details.info.IlStoklar[0].Durum}
                      </Text>
                    </View>
                  )}
                {this.props.details.info?.IlStoklar != null &&
                  this.props.details.info?.IlStoklar?.length > 1 &&
                  this.props.details.info.IlStoklar[1] && (
                    <View style={{ flexDirection: "row" }}>
                      <Text
                        fontScale={0.5}
                        style={{
                          fontFamily: "Poppins-Light",
                          color: this.props.details.info?.IlStoklar[1]?.Durum.toLowerCase().includes(
                            "yok",
                          )
                            ? "#7a7a7a"
                            : "#7a7a7a",
                          fontSize: 13,
                        }}
                      >
                        {this.props.details.info.IlStoklar[1].UretimYeri + ": "}
                      </Text>
                      <Text
                        style={{
                          color: this.props.details.info?.IlStoklar[1]?.Durum.toLowerCase().includes(
                            "yok",
                          )
                            ? "#e57373"
                            : "#03A9F4",
                          fontFamily: this.props.details.info?.IlStoklar[1]?.Durum.toLowerCase().includes(
                            "yok",
                          )
                            ? "Poppins"
                            : "Poppins-Medium",
                          fontSize: 13,
                        }}
                      >
                        {this.props.details.info.IlStoklar[1].Durum}
                      </Text>
                    </View>
                  )}
              </View>
            </View>
          )}
          {!this.props.session.info.SadeceUlusalPB && (
            <PricePanel minHeight={this.getMinHeight()} currency={this.state.currency} />
          )}
          {this.props.session?.mobileConfig?.FiyatTalebiAcik &&
            this.props.details.info.UrunMinFiyatTalebiDurumu && (
              <TouchableOpacity
                onPress={() => this.priceRequestModal.toggleVisibility()}
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  paddingHorizontal: ww * 4,
                  paddingVertical: ww * 2,
                }}
              >
                <Text
                  fontScale={0.5}
                  style={{
                    fontFamily: "Poppins",
                    color: colors.price_color,
                    fontSize: 12,
                  }}
                >
                  Fiyat Teklifi İste
                </Text>
                <Image
                  source={require("@assets/images/arrow-right.png")}
                  style={{
                    marginLeft: moderateScale(5, 0.5),
                    width: moderateScale(12, 0.5),
                    height: moderateScale(12, 0.5),
                    tintColor: colors.price_color,
                  }}
                />
              </TouchableOpacity>
            )}
          <View
            style={{
              backgroundColor: "#f5f5f5",
              paddingBottom: moderateScale(12, 0.5),
              justifyContent: "center",
              alignItems: "center",
              paddingHorizontal: ww * 4,
            }}
          >
            {this.props.details.info.VadeliFiyatlar != null &&
              this.props.details.info.VadeliFiyatlar.Basarili && (
                <TouchableOpacity
                  onPress={() => this.vadeSecenkleriModal.toggleVisibility()}
                  style={{
                    backgroundColor: "white",
                    flexDirection: "row",
                    width: win.width - 2 * ww * 4,
                    borderColor: "#cecece",
                    borderWidth: 1,
                    borderRadius: 3,
                  }}
                >
                  <View style={{ flexDirection: "row", flex: 1 }}>
                    <View
                      style={{
                        flexDirection: "row",
                        flex: 1,
                        borderRightColor: "#ddd",
                        borderRightWidth: 1,
                      }}
                    >
                      <View
                        style={{
                          flexDirection: "column",
                          paddingHorizontal: moderateScale(8, 0.5),
                          paddingVertical: moderateScale(4, 0.5),
                        }}
                      >
                        <Text
                          fontScale={0.5}
                          style={{
                            fontFamily: "Poppins-SemiBold",
                            fontSize: 13,
                            color: colors.price_color,
                          }}
                        >
                          {this.props.session.info.YayginKanalMi
                            ? "Vade ve Taksit Seçenekleri"
                            : "Vade Seçenekleri"}
                        </Text>
                        {defaultVade && defaultVade[0] && (
                          <Text
                            fontScale={0.5}
                            style={{
                              fontFamily: "Poppins-Medium",
                              fontSize: 11,
                              color: "#afafaf",
                            }}
                          >
                            {defaultVade[0].Aciklama +
                              (tekCekim != null ? " | " + tekCekim.Aciklama : "")}
                          </Text>
                        )}
                      </View>
                    </View>
                    <View
                      style={{
                        flexDirection: "row",
                        minWidth: moderateScale(80, 0.7),
                        justifyContent: "center",
                        alignItems: "center",
                        paddingHorizontal: moderateScale(5, 0.5),
                      }}
                    >
                      <Text
                        fontScale={0.5}
                        style={{
                          fontFamily: "Poppins",
                          textAlign: "center",
                          color: "gray",
                          fontSize: 11,
                          flex: 1,
                        }}
                      >
                        Tümünü Göster
                      </Text>
                      <Image
                        source={require("@assets/images/arrow-right.png")}
                        style={{
                          width: moderateScale(12, 0.5),
                          height: moderateScale(12, 0.5),
                          marginLeft: moderateScale(5, 0.5),
                          tintColor: "#bababa",
                        }}
                      />
                    </View>
                  </View>
                </TouchableOpacity>
              )}
            {this.props.details.info.FirsatMaxAlimAdedi && (
              <Text
                style={{
                  fontFamily: "Poppins",
                  fontSize: 13,
                  alignSelf: "flex-start",
                  marginTop: ww * 2,
                }}
              >{`Fırsat ürününden her bayimiz ${this.props.details.info.FirsatMaxAlimAdedi} adet alabilir`}</Text>
            )}
          </View>
          <View
            style={{
              backgroundColor: "white",
              paddingVertical: moderateScale(12, 0.5),
            }}
          >
            <View
              style={{
                flexDirection: "row",
                flex: 1,
                paddingHorizontal: ww * 4,
              }}
            >
              <TextInput
                ref={c => (this.quantityInput = c)}
                onFocus={() =>
                  this.quantityInput.measure((fx, fy, width, height, px, py) => {
                    this.scrollToFocusedInput(py);
                  })
                }
                value={this.state.number}
                onChangeText={text => this.setState({ number: text })}
                keyboardType="numeric"
                style={{
                  fontSize: moderateScale(14, 0.4),
                  borderRadius: 3,
                  borderLeftColor: "#ececec",
                  borderRightColor: "#e5e5e5",
                  borderTopColor: "#dedede",
                  borderWidth: 1,
                  borderBottomWidth: 0,
                  height: moderateScale(40, 0.5),
                  flex: 0.12,
                  textAlign: "center",
                  backgroundColor: "#f5f4f5",
                  color: Number(this.state.number) > 0 ? "black" : "red",
                }}
              />
              <TouchableOpacity
                onPress={this.addToCart}
                style={{
                  flex: 0.88,
                  flexDirection: "row",
                  marginLeft: moderateScale(12, 0.5),
                  height: moderateScale(40, 0.5),
                  borderRadius: 3,
                  backgroundColor: colors.green,
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <Image
                  source={require("@assets/images/cart.png")}
                  style={{
                    marginRight: moderateScale(10, 0.5),
                    width: moderateScale(17, 0.5),
                    height: moderateScale(17, 0.5),
                    tintColor: "white",
                  }}
                />
                <Text fontScale={0.5} style={{ color: "white", fontSize: 18 }}>
                  Sepete Ekle
                </Text>
              </TouchableOpacity>
            </View>
          </View>
          <View
            style={{
              flexDirection: "column",
              flex: 1,
              paddingHorizontal: ww * 4,
            }}
          >
            {this.props.details.info.UrunCrossSellVar &&
              this.props.details.info.IlCrossSellPaketleri.length > 0 &&
              this.renderCrossSellWidget()}
            {this.props.details?.info?.DefoAciklamasi != null && (
              <Text style={{ marginTop: moderateScale(10, 0.5) }}>
                <Text fontScale={0.5} style={{ fontSize: 15, fontFamily: "Poppins-Medium" }}>
                  Defo:{" "}
                </Text>
                <Text fontScale={0.5} style={{ fontSize: 14, fontFamily: "Poppins" }}>
                  {this.props.details?.info?.DefoAciklamasi}
                </Text>
              </Text>
            )}
            <View
              style={{
                flexDirection: "column",
                backgroundColor: "white",
                marginVertical: moderateScale(10, 0.5),
                paddingHorizontal: moderateScale(10, 0.5),
              }}
            >
              {this.props.details.info.IlUrunOzellikleri != null && (
                <TouchableOpacity
                  onPress={() => {
                    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
                    this.setState({
                      isSpecsCollapsed: !this.state.isSpecsCollapsed,
                    });
                  }}
                  style={{
                    flexDirection: "row",
                    borderRadius: 3,
                    flex: 1,
                    height: moderateScale(40, 0.5),
                    alignItems: "center",
                    justifyContent: "space-between",
                  }}
                >
                  <Text fontScale={0.5} style={{ fontSize: 14, fontWeight: "bold" }}>
                    Teknik Özellikleri
                  </Text>
                  <Image
                    source={require("@assets/images/arrow-right.png")}
                    style={{
                      transform: this.state.isSpecsCollapsed ? [{ rotateZ: "-90deg" }] : [],
                      width: moderateScale(12, 0.5),
                      height: moderateScale(12, 0.5),
                      tintColor: "#212121",
                    }}
                  />
                </TouchableOpacity>
              )}

              <View style={{ height: this.state.isSpecsCollapsed ? null : 0 }}>
                {this.props.details.info.IlUrunOzellikleri && (
                  <FlatList
                    data={this.props.details.info.IlUrunOzellikleri}
                    renderItem={({ item, index }) => {
                      if (item.OzellikTanim !== "Pencere Arama Text")
                        return (
                          <View
                            style={{
                              borderBottomColor: colors.light_gray,
                              borderBottomWidth:
                                index < this.props.details.info.IlUrunOzellikleri.length - 1
                                  ? 1
                                  : 0,
                            }}
                          >
                            <View
                              style={{
                                paddingVertical: ww * 4,
                                flexDirection: "row",
                                alignItems: "center",
                                flex: 1,
                                justifyContent: "space-between",
                              }}
                            >
                              <Text>{item.OzellikTanim}</Text>
                              <Text style={{ marginTop: 4 }}>{item.OzellikDeger}</Text>
                            </View>
                          </View>
                        );
                      else return null;
                    }}
                  />
                )}
              </View>
            </View>
            {this.props.details?.info?.UrunSDMetni != null &&
              this.props.details?.info?.UrunSDMetni !== "" && (
                <View
                  style={{
                    flexDirection: "column",
                    backgroundColor: "white",
                    marginVertical: moderateScale(10, 0.5),
                    paddingHorizontal: moderateScale(10, 0.5),
                  }}
                >
                  <TouchableOpacity
                    onPress={() => {
                      LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
                      this.setState({
                        isDetailsCollapsed: !this.state.isDetailsCollapsed,
                      });
                    }}
                    style={{
                      flexDirection: "row",
                      borderRadius: 3,
                      flex: 1,
                      height: moderateScale(40, 0.5),
                      alignItems: "center",
                      justifyContent: "space-between",
                    }}
                  >
                    <Text fontScale={0.5} style={{ fontSize: 14, fontWeight: "bold" }}>
                      Detay Bilgileri
                    </Text>
                    <Image
                      source={require("@assets/images/arrow-right.png")}
                      style={{
                        transform: this.state.isSpecsCollapsed ? [{ rotateZ: "-90deg" }] : [],
                        width: moderateScale(12, 0.5),
                        height: moderateScale(12, 0.5),
                        tintColor: "#212121",
                      }}
                    />
                  </TouchableOpacity>
                  <View
                    style={{
                      height: this.state.isDetailsCollapsed ? null : 0,
                      overflow: "hidden",
                    }}
                  >
                    <Text>{this.props.details.info.UrunSDMetni}</Text>
                  </View>
                </View>
              )}
            {this.props.details.info.IlKampanyalar &&
              this.props.details.info.IlKampanyalar.length > 0 && (
                <TouchableOpacity
                  onPress={() => {
                    if (this.props.details.info.IlKampanyalar.length > 0)
                      this.productCampaignsModal.toggleVisibility(
                        this.props.details.info.IlKampanyalar,
                      );
                    else Alert.alert("Bu ürün için bir kampanya bulunamadı.");
                  }}
                  style={{
                    flexDirection: "row",
                    borderRadius: 3,
                    flex: 1,
                    height: moderateScale(40, 0.5),
                    backgroundColor: "white",
                    marginBottom: moderateScale(10, 0.5),
                    alignItems: "center",
                    justifyContent: "space-between",
                    paddingHorizontal: moderateScale(10, 0.5),
                  }}
                >
                  <Text fontScale={0.5} style={{ fontSize: 14, fontWeight: "bold" }}>
                    Kampanyalar
                  </Text>
                  <Image
                    source={require("@assets/images/arrow-right.png")}
                    style={{
                      width: moderateScale(12, 0.5),
                      height: moderateScale(12, 0.5),
                      tintColor: "#212121",
                    }}
                  />
                </TouchableOpacity>
              )}
            {/* {
              this.state.ratios.length > 0 &&
              <TouchableOpacity style={{
                flexDirection: 'row', borderRadius: 3, flex: 1, height: moderateScale(40, .5), backgroundColor: 'white',
                marginBottom: moderateScale(10, .5), alignItems: 'center', justifyContent: 'space-between', paddingHorizontal: moderateScale(10, .5)
              }}
                onPress={() => {
                  if (this.state.ratios.length > 0) {
                    // @TODO show ratios
                  }
                }}
              >
                <Text fontScale={.5} style={{ fontSize: 14, fontWeight: 'bold' }}>Banka Oranları</Text>
                <Image source={require('@assets/images/arrow-right.png')} style={{ width: moderateScale(12, .5), height: moderateScale(12, .5), tintColor: '#212121' }} />
              </TouchableOpacity>
            } */}
          </View>
        </View>
      );
    else return null;
  }

  render() {
    return (
      <AnimatedBasePage
        ref={c => (this.basePage = c)}
        style={{ backgroundColor: colors.primary_dark, flex: 1 }}
        forceInset={{ top: "always" }}
        showScrollView={
          this.props.navigation.state.params &&
          this.props.details.info &&
          (!this.props.details.isFetching || this.props.details.isRefreshing)
        }
        navigation={this.props.navigation}
        headerProps={{
          title: "Ürün Detayları",
          back: true,
          alignLeft: false,
          showCart: true,
          searchBar: true,
          search: false,
          onBackPressed: () =>
            this.props.navigation.state?.params?.latestScreen
              ? this.props.navigation.navigate(this.props.navigation.state?.params?.latestScreen)
              : this.props.navigation.pop(),
        }}
        isLoading={this.props.details.isFetching}
        scrollViewProps={{
          renderContent: this.renderContent(),
          contentContainerStyle: {
            paddingBottom: this.state.keyboardVisible ? this.state.keyboardPadding : 0,
          },
          onScroll: event => {
            this.scrollY = event.nativeEvent.contentOffset.y;
          },
        }}
      >
        <Toast style={{ backgroundColor: "black", opacity: 0.8, zIndex: 999 }} />
        <CrossSellModal ref={c => (this.crossSellModal = c)} session={this.props.session} />
        <SpecsModal ref={c => (this.specsModal = c)} />
        <PriceRequestModal
          ref={c => (this.priceRequestModal = c)}
          item={this.props.details.info}
          sessionId={this.props.session.info.SessionID}
        />
        <ProductCampaignsModal ref={c => (this.productCampaignsModal = c)} />
        <AddToFavoriteDialog
          ref={c => (this.addFavoriteDialog = c)}
          addFavorite={(f, s) => {
            this.props.addFavorite(this.props.details.info.UrunKodu, f, s).then(msg => {
              if (msg === "ADD_FAVORITE_SUCCESS") {
                Alert.alert("Başarılı İşlem", "Ürün favorilerinize eklendi.", [
                  {
                    text: "Tamam",
                    onPress: () => this.addFavoriteDialog.toggleVisibility(),
                  },
                ]);
                const product = {
                  item_id: this.props.details.info.UrunKodu,
                  item_name: this.props.details.info.UrunAciklamasi,
                  item_category: this.props.details.info.UrunSinifText,
                  quantity: this.props.details.info.UrunMarkaText,
                  price: this.getPrice(),
                  currency: this.state.currency,
                };
                analytics().logEvent("add_to_wishlist", product);
              }
            });
          }}
        />
        {this.props.details.info && this.props.details.info.VadeliFiyatlar && (
          <VadeSecenekleriModal
            ref={c => (this.vadeSecenkleriModal = c)}
            items={this.props.details.info.VadeliFiyatlar.Vadeler}
            session={this.props.session}
          />
        )}
        {this.state.fsCarousel && (
          <Modal
            presentationStyle="overFullScreen"
            transparent={true}
            animationType="slide"
            onRequestClose={() => this.setState({ fsCarousel: false })}
          >
            <Interactable.View
              boundaries={{ top: 0 }}
              snapPoints={[{ y: 0 }, { y: win.height }]}
              onSnapStart={({ nativeEvent }) => {
                if (nativeEvent.index === 1 && Platform.OS === "ios") {
                  this.setState({ fsCarousel: false });
                  StatusBar.setHidden(false);
                }
              }}
              onSnap={({ nativeEvent }) => {
                console.log("closing onSnap", nativeEvent);
                if (nativeEvent.index === 1) {
                  this.setState({ fsCarousel: false });
                  StatusBar.setHidden(false);
                }
              }}
              verticalOnly={true}
              animatedNativeDriver={true}
              style={{
                height: win.height,
                width: win.width,
                backgroundColor: "#fff",
                position: "absolute",
                top: 0,
                left: 0,
                flexDirection: "column",
                justifyContent: "center",
                zIndex: 9,
              }}
            >
              <TouchableOpacity
                style={{
                  position: "absolute",
                  top: moderateScale(10, 0.5),
                  right: moderateScale(10, 0.5),
                  height: moderateScale(40, 0.5),
                  width: moderateScale(40, 0.5),
                  alignItems: "center",
                  justifyContent: "center",
                }}
                onPress={() => {
                  this.setState({ fsCarousel: false });
                  StatusBar.setHidden(false);
                }}
              >
                <Image
                  source={require("@assets/images/x.png")}
                  style={{
                    width: moderateScale(20, 0.5),
                    height: moderateScale(20, 0.5),
                    tintColor: "#cacaca",
                    resizeMode: "contain",
                  }}
                />
              </TouchableOpacity>
              <ProductCarousel
                key={this.props.details.UrunKodu}
                fullScreenMode={true}
                fullScreenCarousel={() => {
                  this.setState({ fsCarousel: false });
                  StatusBar.setHidden(false);
                }}
                details={this.props.details.info}
              />
            </Interactable.View>
          </Modal>
        )}
        {Array.isArray(this.state.defolular) && this.state.defolular?.length > 0 && (
          <ListModal
            contentContainerstyle={{ padding: moderateScale(10, 0.5) }}
            title="Defolu Ürünler"
            ref={c => (this.defoluListModal = c)}
            items={this.state.defolular}
            renderItem={item => {
              return (
                <TouchableOpacity
                  onPress={() => {
                    this.defoluListModal.toggleVisibility();
                    this.props.getDetails(
                      false,
                      item.UrunKodu,
                      item.UrunFiyatParaBirimi,
                      true,
                      false,
                      item.UrunSeriNo,
                    );
                  }}
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    padding: moderateScale(10, 0.5),
                    paddingVertical: moderateScale(5, 0.5),
                    borderRadius: 3,
                    borderWidth: 1,
                    borderColor: "#ddd",
                    marginBottom: moderateScale(10, 0.5),
                    flex: 1,
                  }}
                >
                  <View
                    style={{
                      flexDirection: "column",
                    }}
                  >
                    <Image
                      source={{ uri: item.UrunResmi }}
                      style={{
                        width: moderateScale(50, 0.5),
                        height: moderateScale(50, 0.5),
                        paddingRight: moderateScale(5, 0.5),
                        resizeMode: "contain",
                        marginRight: moderateScale(5, 0.5),
                      }}
                    />
                  </View>
                  <View
                    style={{
                      flexDirection: "column",
                      flex: 1,
                    }}
                  >
                    <Text fontScale={0.5}>
                      <Text
                        fontScale={0.5}
                        style={{
                          fontFamily: "Poppins",
                          fontSize: 12,
                          color: "gray",
                        }}
                      >
                        Ürün kodu:{" "}
                      </Text>
                      <Text fontScale={0.5} style={{ fontFamily: "Poppins", fontSize: 12 }}>
                        {item.UrunKodu}
                      </Text>
                    </Text>
                    <Text fontScale={0.5}>
                      <Text
                        fontScale={0.5}
                        style={{
                          fontFamily: "Poppins",
                          fontSize: 12,
                          color: "gray",
                        }}
                      >
                        {"Fiyat: "}
                      </Text>
                      <Text fontScale={0.5} style={{ fontFamily: "Poppins", fontSize: 12 }}>
                        {item.UrunFiyat}
                      </Text>
                      <Text fontScale={0.5} style={{ fontFamily: "Poppins", fontSize: 12 }}>
                        {" " + item.UrunFiyatParaBirimi}
                      </Text>
                    </Text>
                    <Text
                      fontScale={0.5}
                      style={{
                        fontFamily: "Poppins",
                        fontSize: 12,
                        color: "#f44336",
                      }}
                    >
                      {item.UrunHasarliAciklamasi}
                    </Text>
                  </View>
                </TouchableOpacity>
              );
            }}
          />
        )}
        {!this.props.session.info.SadeceUlusalPB && (
          <BottomPicker
            ref={c => (this.bottomCurrencyPicker = c)}
            items={["TRY", "USD", "EUR"]}
            initialItem={this.state.currency}
            onPressOK={pb => {
              this.setState({ currency: pb });
              this.props.getDetails(false, this.props.details.info?.UrunKodu, pb, true);
            }}
          />
        )}
      </AnimatedBasePage>
    );
  }
}

function mapStateToProps(state) {
  return {
    cart: state.cart,
    session: state.session,
    details: state.details,
    favorites: state.favorites,
    progress: state.progress,
    homePage: state.homePage,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    getDetails: (dontSet, uk, pb, refresh, dg, sn) =>
      dispatch(getDetailsFromApi(dontSet, uk, pb, refresh, dg, sn)),
    setCart: cart => dispatch(setCart(cart)),
    addFavorite: (urunKodu, f, s) => dispatch(addFavorite(urunKodu, f, s)),
    removeFavorite: _prd => dispatch(removeFavorite(_prd)),
    setSafeViewBackgroundColor: color => dispatch(setSafeViewBackgroundColor(color)),
    showToast: msg => dispatch(showToast(msg)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductDetailsScreen);

const styles = ScaledSheet.create({
  loadingContainer: {
    position: "absolute",
    top: 0,
    left: 0,
    height: win.height,
    width: win.width,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0,0,0,0.3)",
  },
});
