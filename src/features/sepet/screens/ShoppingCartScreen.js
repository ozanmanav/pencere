import React, { Component } from "react";
import {
  View,
  FlatList,
  RefreshControl,
  TouchableOpacity,
  Dimensions,
  StatusBar,
  SafeAreaView,
  Animated,
  Image,
  StyleSheet,
  Alert,
  Platform,
  Keyboard,
  InteractionManager,
} from "react-native";
import { connect } from "react-redux";
import { AnimatedHeader, TabBarIcon, BottomPicker, Text, AnimatedBasePage } from "@components";
import {
  CartItem,
  CartItemBundle,
  NewCartDialog,
  PriceInfoModal,
  PriceRequestResultModal,
  PriceRequestWidget,
  AddToFavoriteDialog,
  QuantityDialog,
  FiyatTeklifiMaxAdetModal,
} from "@features/sepet/components";
import {
  setCart,
  setBucket,
  deleteBucket,
  addBucket,
  startOrder,
  getBucket,
  setNumber,
  addFavorite,
  showToast,
  getPriceRequests,
  getFavorites,
} from "@redux/actions";

import Device from "@common/Device";
import colors from "@common/styles/colors";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import { scale, verticalScale, moderateScale } from "react-native-size-matters";
import {
  emptyCart,
  downlaodCsv,
  downloadPdf,
  getEndPoint,
  numberToLocaleString,
} from "@common/Utils";
import bugsnag from "@common/bugsnag_config";
import analytics from "@react-native-firebase/analytics";

const NAVBAR_HEIGHT = AnimatedHeader.height;
const win = Dimensions.get("window");
const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);
const SPACE_HORIZONTAL = win.width / 36;

class ShoppingCartScreen extends Component {
  offset = 0;
  listHeight = 0;
  innerHeight = 0;
  currentOffset = 0;

  static navigationOptions = ({ navigation }) => ({
    header: null,
    tabBarIcon: ({ tintColor }) => (
      <TabBarIcon
        iconName="cart"
        navigation={navigation}
        style={{ width: 20, height: 20 }}
        icon={require("@assets/images/cart.png")}
        label="Sepetim"
        tintColor={tintColor}
      />
    ),
  });

  constructor(props) {
    super(props);
    this.state = {
      keyboardHeight: 0,
      selectedItem: null,
      listHeight: 0,
      currency: "TL",
      total: 0,
      basic: true,
      shadowOpacity: 0,
      sections: [],
      progress: true,
    };
  }

  _keyboardDidShow = (e) => {
    this.setState({
      keyboardVisible: true,
      keyboardHeight: e.endCoordinates.height,
    });
  };

  deleteBucket = () => {
    Alert.alert(
      "Sepeti Sil",
      "Bu sepeti silmek istediğinize emin misiniz?",
      [
        {
          text: "Hayır",
          style: "cancel",
        },
        {
          text: "Evet",
          onPress: () => {
            this.setState({ progress: true });
            this.props
              .deleteBucket(this.props.cart.cartInfo.SeciliSepetBaslik.SepetKodu)
              .then((msg) => {
                console.log("delete bucket", msg);
                this.setState({ progress: false });
                this.props.showToast("Sepet silindi.");
              })
              .catch((err) => {
                console.log("delete bucket", err);
                this.setState({ progress: false });
                this.props.showToast("Bir hata oluştu. Lütfen tekrar deneyiniz.");
              });
          },
        },
      ],
      { cancelable: false },
    );
  };

  addNewBucket = (name) => {
    this.props.addBucket(name);
  };

  addToCart = (item, number) => {
    if (true) {
      // Should we allow user to add no-stock products to cart?
      this.setState({
        progress: true,
      });
      fetch(
        getEndPoint("CART_URL") +
          "?Si=" +
          this.props.session.info.SessionID +
          "&func=8" +
          "&uk=" +
          item.UrunKodu +
          "&adt=" +
          number +
          "&sn=" +
          item.UrunSeriNo,
        {
          method: "POST",
        },
      )
        .then((response) => response.json())
        .then((responseJson) => {
          if (responseJson.MesajTipi === "S") {
            this.props.setCart(responseJson);
            this.props.showToast("Ürün sepete eklendi");
          } else {
            this.props.showToast("Bir hata oluştu lütfen tekrar deneyin");
          }
          this.setState({
            progress: false,
          });
        })
        .catch((error) => {
          this.props.showToast("Bir hata oluştu lütfen tekrar deneyin");
          console.log("addToCart error", error);
        });
    } else {
      Alert.alert(
        "Yetersiz Stok",
        "Yeterli stok olmadığı için işleminiz gerçekleştirilemedi.",
        [{ text: "Tamam", onPress: () => console.log("Tamam") }],
        { cancelable: false },
      );
    }
  };

  renderHeader = () => {
    if (this.props.cart.error) return null;
    return (
      <View style={{ alignItems: "center", width: win.width }}>
        <View
          style={{
            flexDirection: "row",
            backgroundColor: "white",
            width: win.width,
            paddingVertical: moderateScale(10, 0.5),
            alignItems: "center",
            justifyContent: "space-between",
            paddingHorizontal: SPACE_HORIZONTAL,
          }}
        >
          <TouchableOpacity
            style={{ marginRight: moderateScale(10, 0.5) }}
            onPress={() => this.deleteBucket()}
          >
            <Image
              source={require("@assets/images/delete-button.png")}
              style={{
                width: verticalScale(17),
                height: verticalScale(17),
                tintColor: "gray",
              }}
            />
          </TouchableOpacity>
          <View style={{ flexDirection: "column", flex: 0.55 }}>
            <Text
              style={{
                fontFamily: "Poppins-Medium",
                fontSize: 10,
                color: "black",
                textAlign: "left",
              }}
            >
              {this.props.cart.cartInfo.SeciliSepetBaslik?.SepetAdi}
            </Text>
            <Text
              style={{
                fontFamily: "Poppins",
                fontSize: 10,
                color: "gray",
                textAlign: "left",
              }}
            >
              {this.props.session.info.Ad +
                " " +
                this.props.session.info.Soyad +
                " - " +
                this.props.cart.cartInfo?.SeciliSepetBaslik?.StrSepetOlusturmaTarihi ?? ""}
            </Text>
          </View>
          <View
            style={{
              flexDirection: "row",
              flex: 0.45,
              justifyContent: "space-between",
              alignItems: "center",
              height: moderateScale(30, 0.5),
              borderColor: "#c4c4c4",
              borderWidth: 1,
              borderRadius: 3,
            }}
          >
            <TouchableOpacity
              onPress={() =>
                Platform.OS === "ios"
                  ? this.bottomPicker.openPicker()
                  : this.bottomPicker.openPicker(null, null, true)
              }
              style={{
                flexDirection: "row",
                flex: 1,
                alignItems: "center",
                justifyContent: "space-evenly",
              }}
            >
              <Text
                style={{
                  fontFamily: "Poppins-SemiBold",
                  fontSize: 11,
                  color: "#7b7a7a",
                  textAlign: "left",
                }}
              >
                DİĞER SEPETLER
              </Text>
              <Image
                source={require("@assets/images/down-arrow.png")}
                style={{ width: 10, height: 10, tintColor: "#cecece" }}
              />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.newcartdialog.toggleVisibility()}
              style={{
                width: moderateScale(30, 0.5),
                justifyContent: "center",
                alignItems: "center",
                borderLeftColor: "#c4c4c4",
                borderLeftWidth: 1,
                height: moderateScale(30, 0.5),
              }}
            >
              <Image
                source={require("@assets/images/plus.png")}
                style={{
                  width: moderateScale(12, 0.5),
                  height: moderateScale(12, 0.5),
                  tintColor: "gray",
                }}
              />
            </TouchableOpacity>
          </View>
        </View>
        {!this.props.session.info.SadeceUlusalPB && this.props.homePage.info.GununKuru && (
          <View
            style={{
              flexDirection: "row",
              width: win.width - SPACE_HORIZONTAL * 2,
              marginHorizontal: SPACE_HORIZONTAL,
              backgroundColor: "white",
              marginTop: SPACE_HORIZONTAL,
              paddingHorizontal: moderateScale(10, 0.5),
              alignItems: "center",
              justifyContent: "space-between",
              height: verticalScale(40),
              borderRadius: 4,
            }}
          >
            <View style={{ flexDirection: "row" }}>
              <Text style={{ fontFamily: "Poppins", fontSize: 12, color: "gray" }}>
                Döviz Kurları:
              </Text>
            </View>
            <View
              style={{
                flexDirection: "row",
                flex: 1,
                justifyContent: "space-around",
              }}
            >
              <Text style={{ fontFamily: "Poppins", fontSize: 12, color: "gray" }}>
                {"USD: " + this.props.homePage.info.GununKuru.UsdKur}
              </Text>
              <Text style={{ fontFamily: "Poppins", fontSize: 12, color: "gray" }}>
                {"EUR: " + this.props.homePage.info.GununKuru.EuroKur}
              </Text>
            </View>
          </View>
        )}
        {this.props.cart.cartInfo.MinFiyatTalepTutari != null && (
          <PriceRequestWidget cart={this.props.cart.cartInfo} />
        )}
        {this.props.cart.cartInfo.IlSeciliSepetKalemler &&
          this.props.cart.cartInfo.IlSeciliSepetKalemler.length > 0 && (
            <View
              style={{
                flexDirection: "row",
                height: moderateScale(50, 0.5),
                borderBottomColor: "#ddd",
                borderBottomWidth: 1,
                alignItems: "center",
                justifyContent: "space-between",
                width: win.width - 2 * SPACE_HORIZONTAL,
                marginTop: SPACE_HORIZONTAL,
                backgroundColor: "white",
                borderTopLeftRadius: 4,
                borderTopRightRadius: 4,
              }}
            >
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  flex: 1,
                  paddingHorizontal: SPACE_HORIZONTAL,
                  alignItems: "center",
                }}
              >
                <Text
                  style={{
                    fontSize: 18,
                    lineHeight: 24,
                    marginTop: 4,
                    color: "#d55749",
                    fontFamily: "Poppins",
                  }}
                >
                  SEPETTEKİ ÜRÜNLER
                </Text>
                {!this.props.session.info.SadeceUlusalPB && (
                  <TouchableOpacity
                    onPress={() => {
                      if (Platform.OS === "ios") this.bottomCurrencyPicker.openPicker();
                      else this.bottomCurrencyPicker.openPicker(null, null, true);
                    }}
                    style={{
                      flexDirection: "row",
                      width: moderateScale(80, 0.5),
                      justifyContent: "space-around",
                      alignItems: "center",
                      borderRadius: 3,
                      borderWidth: 1,
                      borderColor: "#c9c9c9",
                      height: moderateScale(30, 0.5),
                    }}
                  >
                    <Text
                      style={{
                        fontFamily: "Poppins-Medium",
                        fontSize: 12,
                        color: "#7b7a7a",
                      }}
                    >
                      {this.props.cart.cartInfo.paraBirimi}
                    </Text>
                    <Image
                      source={require("@assets/images/down-arrow.png")}
                      style={{
                        width: moderateScale(14, 0.5),
                        height: moderateScale(14, 0.5),
                        tintColor: "#c4c4c4",
                      }}
                    />
                  </TouchableOpacity>
                )}
              </View>
            </View>
          )}
        {/* {
          this.props.homePage.YouViewed &&
          this.props.homePage.YouViewed.length > 0 &&
          <View style={[{ flex: 1, marginBottom: 20, marginLeft: SPACE_HORIZONTAL, marginTop: SPACE_HORIZONTAL }]}>
            <Swiper indicator={true} title="Sizin İçin Seçtiklerimiz" itemWidth={((win.width / 1.3) + 15)} style={{ borderTopLeftRadius: 4, borderBottomLeftRadius: 4 }}>
              {
                this.props.homePage.YouViewed&&
                this.props.homePage.YouViewed.map((item, idx) => {
                  return <View style={{ flexDirection: 'row' }} >
                    <SwiperProductThumb index={idx} itemCount={this.props.homePage.YouViewed.length} horizontal={true} addToCart={this.addToCart} onPress={() => this.props.navigation.navigate({ key: 'home', routeName: 'ProductDetailsScreen', params: { UrunKodu: item.UrunKodu || item.Code, SessionID: this.props.session.info.SessionID } })} key={idx} {...item} />
                  </View>
                })
              }
            </Swiper>
          </View>
        } */}
        {this.props.cart.cartInfo.IlSeciliSepetKalemler.length === 0 && (
          <View
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center",
              marginTop: 10,
            }}
          >
            <Text style={{ fontFamily: "Poppins", color: "gray", fontSize: 14 }}>
              Sepetiniz Boş
            </Text>
          </View>
        )}
      </View>
    );
  };

  startOrder() {
    this.props
      .startOrder()
      .then((res) => {
        if (res.MesajTipi === "S") {
          this.props.navigation.navigate("CheckoutStack");
        } else {
          var mes = res.Mesaj.replace(new RegExp("<br/>", "g"), " ");
          Alert.alert("Bilgi", mes, [{ text: "Tamam", onPress: () => console.log("Tamam") }], {
            cancelable: false,
          });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }

  renderSectionHeader = () => {
    return (
      <View
        style={{
          position: "absolute",
          bottom: moderateScale(60, 0.5),
          width: win.width,
          height: moderateScale(60, 0.5),
        }}
      >
        <View
          style={{
            flex: 1,
            flexDirection: "row",
            justifyContent: "space-around",
            paddingHorizontal: moderateScale(10, 0.5),
            paddingVertical: moderateScale(6, 0.5),
            backgroundColor: "white",
            borderTopColor: "#e4e4e4",
            borderTopWidth: 1,
          }}
        >
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              height: moderateScale(30, 0.5),
              justifyContent: "flex-start",
            }}
          >
            <Text style={{ fontSize: 14, color: "gray", padding: 0 }}>{"Genel Toplam: "}</Text>
            <Text style={{ fontSize: 14, color: "#0097A7", padding: 0 }}>
              {this.props.cart.cartInfo.SeciliSepetAltToplam.GenelToplam.toFixed(2)
                .toString()
                .replace(".", ",") +
                " " +
                this.props.cart.cartInfo.paraBirimi}
            </Text>
            <TouchableOpacity
              onPress={() => this.priceInfoModal.toggleVisibility()}
              style={{ marginLeft: moderateScale(5, 0.5) }}
            >
              <Image
                source={require("@assets/images/info.png")}
                style={{
                  width: moderateScale(16, 0.5),
                  height: moderateScale(16, 0.5),
                }}
              />
            </TouchableOpacity>
          </View>
          <TouchableOpacity
            style={{
              paddingVertical: moderateScale(4, 0.5),
              paddingHorizontal: moderateScale(6, 0.5),
              backgroundColor: "#5eb139",
              borderRadius: 3,
            }}
            onPress={() => this.startOrder()}
          >
            <Text style={{ color: "white", fontSize: 14 }}>Sipariş Ver</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  changeSelectedCart = (sk) => {
    console.log("changeSelectedCart sk", sk);
    this.props.setBucket(sk, this.props.cart.cartInfo.paraBirimi);
  };

  updateSelectedCart(pb) {
    this.props.setBucket(this.props.cart.cartInfo.SeciliSepetBaslik.SepetKodu, pb);
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      this.props.session?.info?.SadeceUlusalPB &&
      (prevProps.session?.info?.SadeceUlusalPB !== this.props.session?.info?.SadeceUlusalPB ||
        this.props.cart?.cartInfo?.SeciliSepetBaslik?.SepetKodu !==
          prevProps.cart?.cartInfo.SeciliSepetBaslik?.SepetKodu) &&
      this.props.cart?.cartInfo?.paraBirimi !== "TRY"
    ) {
      this.props.setBucket(this.props.cart.cartInfo?.SeciliSepetBaslik?.SepetKodu, "TRY");
    }

    if (
      prevProps.cart.cartInfo !== this.props.cart.cartInfo &&
      typeof prevProps.cart.cartInfo.Mesaj !== "undefined"
    ) {
      this.setState({
        currency: this.props.cart.cartInfo.ParaBirimi,
      });
      if (
        this.props.cart.cartInfo.IlSeciliSepetKalemler &&
        this.props.cart.cartInfo.IlSeciliSepetKalemler.length === 0
      ) {
        if (this.basePage.flatlist) {
          console.log("cart empty scroll");
          if (this.basePage.flatlist)
            this.basePage.flatlist.scrollToOffset({ offset: -NAVBAR_HEIGHT, animated: true });
        }
      }
    }
    if (
      prevProps.navigation.state.params?.msg != null &&
      prevProps.navigation.state.params.msg !== this.props.navigation.state.params.msg
    ) {
      this.props.showToast("Ürün sepete eklendi");
    }
  }

  componentDidMount() {
    this.getInitialData();
  }

  getInitialData = async () => {
    await this.props.getFavorites();
    if (!this.props.cart.cartInfo.IlSeciliSepetKalemler) {
      try {
        await this.props.getBucket(false);
      } catch (e) {
        Alert.alert("Hata", "Bir hata oluştu. Lütfen daha sonra tekrar deneyiniz.");
        this.setState({ progress: false });
      }
    }
    this.setState({ progress: false });
  };

  scrollToFocusedInput = (offset) => {
    console.log("flatlist", this.basePage.flatlist);
    if (offset > this.innerHeight - AnimatedHeader.height - 100)
      this.basePage.flatlist.scrollToOffset({
        offset:
          this.currentOffset +
          offset -
          (this.innerHeight - AnimatedHeader.height - 100) +
          AnimatedHeader.height,
        animated: true,
      });
  };

  onScroll = (event) => {
    var dir = event.nativeEvent.contentOffset.y > this.currentOffset ? "out" : "in";
    dir = (
      this.currentOffset < 0
        ? 0
        : this.currentOffset > event.nativeEvent.contentSize.height - 260 - this.innerHeight
    )
      ? "out"
      : "in";
    if (dir === "in" && this.state.shadowOpacity === 0.05) {
      this.setState({ shadowOpacity: 0 });
    } else if (dir === "out" && this.state.shadowOpacity === 0) {
      this.setState({ shadowOpacity: 0.05 });
    }
    this.currentOffset = event.nativeEvent.contentOffset.y;
  };

  onPressQuantity(rowData) {
    this.setState({ selectedSkk: rowData }, () => {
      if (Platform.OS === "ios") this.quantityPicker.openPicker();
      else {
        this.quantityDialog.toggleVisibility(rowData.UrunMiktar);
      }
    });
  }

  _renderRow(rowData, index) {
    if (!rowData.UrunCrossSell && rowData.IlAltKalemler.length === 0)
      return (
        <CartItem
          scrollToFocusedInput={this.scrollToFocusedInput}
          setSelectedItem={this.setSelectedItem}
          addFavoriteDialog={this.addFavoriteDialog}
          onPressQuantity={() => this.onPressQuantity(rowData)}
          item={rowData}
          index={index}
          length={this.props.cart.count}
          navigation={this.props.navigation}
        />
      );
    else if (
      (rowData.UrunCrossSell && rowData.Referans3 === "0") ||
      rowData.IlAltKalemler.length > 0
    ) {
      var subItems = [];
      if (rowData.UrunCrossSell && rowData.Referans3 === "0")
        subItems = this.props.cart.cartInfo.IlSeciliSepetKalemler.filter(
          (item) => item.Referans1 === rowData.Referans1 && item.Referans3 !== "0",
        );
      else {
        subItems = rowData.IlAltKalemler;
      }
      const altKalem = rowData.IlAltKalemler.length > 0;
      return (
        <CartItemBundle
          scrollToFocusedInput={this.scrollToFocusedInput}
          setSelectedItem={this.setSelectedItem}
          addFavoriteDialog={this.addFavoriteDialog}
          onPressQuantity={() => this.onPressQuantity(rowData)}
          item={rowData}
          index={index}
          length={this.props.cart.count}
          subItems={subItems}
          navigation={this.props.navigation}
          altKalem={altKalem}
        />
      );
    } else return null;
  }

  _onRefresh() {
    this.props.getBucket(true);
  }

  requestPrice = () => {
    const prepared_url =
      getEndPoint("PRICE_REQUISITIONS_URL") +
      "?si=" +
      this.props.session.info.SessionID +
      "&func=3";
    this.setState({ progress: true });
    //this.requestResultModal.toggleVisibility()
    fetch(prepared_url)
      .then((res) => res.json())
      .then((responseJson) => {
        this.setState({ progress: false });
        if (responseJson.IslemBasarili) {
          Alert.alert("Başarılı İşlem", "Fiyat teklifi oluşturuldu.", [
            {
              text: "Tamam",
              onPress: () => {
                this.props.navigation.navigate({
                  routeName: "PriceRequestsScreen",
                  key: "PriceRequestsScreen",
                  params: {
                    showAlert: true,
                    latestScreen: "ShoppingCartScreen",
                  },
                });
                InteractionManager.runAfterInteractions(this.props.getPriceRequests());
              },
            },
          ]);
        } else {
          var sepetKalemler = this.props.cart.cartInfo.IlSeciliSepetKalemler;
          var UygunsuzUrunAdetleri = [];
          sepetKalemler.forEach((kalem, index) => {
            var item = responseJson.UygunsuzUrunAdetleri.find(
              (_item) => _item.Kod === kalem.UrunKodu && kalem.UrunMiktar > _item.Adet,
            );
            if (item != null) {
              UygunsuzUrunAdetleri.push(item);
            }
          });
          if (UygunsuzUrunAdetleri.length > 0) {
            this.fiyatTeklifiMaxAdetModal.toggleVisibility(UygunsuzUrunAdetleri);
          } else {
            Alert.alert(
              "Başarısız İşlem",
              responseJson?.Message ||
                "Beklenmedik bir hata oluştu. Lütfen daha sonra tekrar deneyiniz",
              [{ text: "Tamam" }],
            );
          }
        }
      })
      .catch((err) => {
        this.setState({ progress: false });
        bugsnag.notify(err, (report) => {
          report.errorClass = "price request parse json";
        });
        console.log("price req err", err);
        Alert.alert("Hata", "Bir hata oluştu. Lütfen daha sonra tekrar deneyiniz.");
      });
  };

  renderCartSummary = () => {
    if (this.props.cart.error) return null;
    if (this.props.cart.cartInfo.IlSeciliSepetKalemler.length > 0)
      return (
        <View style={{ flexDirection: "column" }}>
          <View
            style={{
              flexDirection: "row",
              flex: 1,
              padding: SPACE_HORIZONTAL,
              marginHorizontal: SPACE_HORIZONTAL,
              backgroundColor: "white",
              marginTop: SPACE_HORIZONTAL,
              alignItems: "center",
              justifyContent: "space-between",
              height: verticalScale(56),
              borderRadius: 4,
            }}
          >
            <TouchableOpacity
              onPress={() => {
                this.setState({ progress: true });
                this.basePage.animatedHeader.getWrappedInstance().startSearchBarAnim("in");
                emptyCart({ func: "11", si: this.props.session.info.SessionID })
                  .then(() => {
                    this.setState({ progress: false });
                  })
                  .catch((err) => {
                    console.log("error empty cart", err);
                  });
              }}
              style={{
                flexDirection: "row",
                flex: Device.isTablet() ? 1 : 0.3,
                height: verticalScale(42),
                borderRightColor: "#adadad",
                borderRightWidth: 1,
                alignItems: "center",
                justifyContent: "space-evenly",
              }}
            >
              <Image
                source={require("@assets/images/delete-button.png")}
                style={{
                  width: verticalScale(17),
                  height: verticalScale(17),
                  tintColor: "gray",
                }}
              />
              {!Device.isTablet() ? (
                <View style={{ flexDirection: "column" }}>
                  <Text
                    style={{
                      fontFamily: "Poppins",
                      color: "#999999",
                      fontSize: 11,
                      flexWrap: "wrap",
                    }}
                  >
                    Sepeti
                  </Text>
                  <Text
                    style={{
                      fontFamily: "Poppins",
                      color: "#999999",
                      fontSize: 11,
                      flexWrap: "wrap",
                    }}
                  >
                    Boşalt
                  </Text>
                </View>
              ) : (
                <Text
                  style={{
                    fontFamily: "Poppins",
                    color: "#999999",
                    fontSize: 11,
                    flexWrap: "wrap",
                  }}
                >
                  Sepeti Boşalt
                </Text>
              )}
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                this.setState({ progress: true });
                downloadPdf(
                  getEndPoint("EXPORT_PDF_URL") +
                    "?r=1&pb=" +
                    this.props.cart.cartInfo.paraBirimi +
                    "&si=" +
                    this.props.session.info.SessionID,
                  this.props.cart.cartInfo.SeciliSepetBaslik.SepetAdi + ".pdf",
                  "pdf",
                )
                  .then(() => {
                    this.setState({ progress: false });
                  })
                  .catch((err) => {
                    this.setState({ progress: false });
                    Alert.alert("Hata", err);
                  });
              }}
              style={{
                flexDirection: "row",
                flex: Device.isTablet() ? 1 : 0.35,
                height: verticalScale(42),
                borderRightColor: "#adadad",
                borderRightWidth: 1,
                alignItems: "center",
                justifyContent: "space-evenly",
              }}
            >
              <Image
                source={require("@assets/images/pdf.png")}
                style={{
                  width: verticalScale(20),
                  height: verticalScale(20),
                  tintColor: "gray",
                }}
              />
              {!Device.isTablet() ? (
                <View style={{ flexDirection: "column" }}>
                  <Text
                    style={{
                      fontFamily: "Poppins",
                      color: "#999999",
                      fontSize: 11,
                      flexWrap: "wrap",
                    }}
                  >
                    PDF Olarak
                  </Text>
                  <Text
                    style={{
                      fontFamily: "Poppins",
                      color: "#999999",
                      fontSize: 11,
                      flexWrap: "wrap",
                    }}
                  >
                    İndir
                  </Text>
                </View>
              ) : (
                <Text
                  style={{
                    fontFamily: "Poppins",
                    color: "#999999",
                    fontSize: 11,
                    flexWrap: "wrap",
                  }}
                >
                  PDF Olarak İndir
                </Text>
              )}
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                this.setState({ progress: true });
                downlaodCsv(
                  this.props.cart.cartInfo.IlSeciliSepetKalemler,
                  this.props.cart.cartInfo.SeciliSepetBaslik.SepetAdi + ".csv",
                  "csv",
                )
                  .then(() => {
                    this.setState({ progress: false });
                  })
                  .catch((err) => {
                    this.setState({ progress: false });
                    Alert.alert("Hata", err);
                  });
              }}
              style={{
                flexDirection: "row",
                flex: Device.isTablet() ? 1 : 0.35,
                height: verticalScale(42),
                borderRightColor: "#adadad",
                borderRightWidth: 0,
                alignItems: "center",
                justifyContent: "space-evenly",
              }}
            >
              <Image
                source={require("@assets/images/xls.png")}
                style={{
                  width: verticalScale(20),
                  height: verticalScale(20),
                  tintColor: "gray",
                }}
              />
              {!Device.isTablet() ? (
                <View style={{ flexDirection: "column" }}>
                  <Text
                    style={{
                      fontFamily: "Poppins",
                      color: "#999999",
                      fontSize: 11,
                      flexWrap: "wrap",
                    }}
                  >
                    Excel Olarak
                  </Text>
                  <Text
                    style={{
                      fontFamily: "Poppins",
                      color: "#999999",
                      fontSize: 11,
                      flexWrap: "wrap",
                    }}
                  >
                    İndir
                  </Text>
                </View>
              ) : (
                <Text
                  style={{
                    fontFamily: "Poppins",
                    color: "#999999",
                    fontSize: 11,
                    flexWrap: "wrap",
                  }}
                >
                  Excel Olarak İndir
                </Text>
              )}
            </TouchableOpacity>
          </View>

          <View
            style={{
              flexDirection: "row",
              flex: 1,
              marginHorizontal: SPACE_HORIZONTAL,
              marginTop: SPACE_HORIZONTAL,
              alignItems: "center",
              justifyContent: "space-between",
              marginBottom: SPACE_HORIZONTAL,
            }}
          >
            {/* <TouchableOpacity style={{ flexDirection: 'row', flex: 1, marginRight: SPACE_HORIZONTAL, backgroundColor: 'white', height: verticalScale(50), paddingHorizontal: verticalScale(8), alignItems: 'center', justifyContent: 'center', borderRadius: 4 }}>
              <Text style={{ fontFamily: 'Poppins-Medium', fontSize: 14, color: '#3486BA' }}>Sepetleri Birleştir</Text>
            </TouchableOpacity> */}
          </View>
          <View
            style={{
              flexDirection: "column",
              backgroundColor: "white",
              borderRadius: 0,
              overflow: "hidden",
              width: win.width,
              alignSelf: "center",
            }}
          >
            <View
              style={{
                flexDirection: "row",
                borderBottomColor: "#ddd",
                borderBottomWidth: 1,
                height: verticalScale(40),
                paddingLeft: scale(10),
                marginHorizontal: SPACE_HORIZONTAL * 2 - scale(10),
                alignItems: "center",
              }}
            >
              <Text
                style={{
                  fontFamily: "Poppins",
                  fontSize: 15,
                  color: "#d55749",
                }}
              >
                Sepet Özeti
              </Text>
            </View>
            <View
              style={{
                flexDirection: "column",
                flex: 1,
                borderBottomColor: "#dedede",
                borderBottomWidth: 0,
                height: verticalScale(112),
              }}
            >
              <View
                style={{
                  flexDirection: "row",
                  flex: 1,
                  borderBottomColor: "#f4f4f4",
                  borderBottomWidth: 0,
                  height: verticalScale(34),
                  marginTop: verticalScale(10),
                  alignItems: "center",
                  marginHorizontal: SPACE_HORIZONTAL * 2,
                  justifyContent: "space-between",
                }}
              >
                <Text
                  style={{
                    fontFamily: "Poppins",
                    fontSize: 13,
                    color: "#999999",
                  }}
                >
                  Ara Toplam
                </Text>
                <Text
                  style={{
                    fontFamily: "Poppins",
                    fontSize: 13,
                    color: "#333333",
                  }}
                >
                  {numberToLocaleString(this.props.cart.cartInfo.SeciliSepetAltToplam.AraToplam) +
                    " " +
                    this.props.cart.cartInfo.paraBirimi}
                </Text>
              </View>
              <View
                style={{
                  flexDirection: "row",
                  flex: 1,
                  borderBottomColor: "#f4f4f4",
                  borderBottomWidth: 0,
                  height: verticalScale(34),
                  alignItems: "center",
                  marginHorizontal: SPACE_HORIZONTAL * 2,
                  justifyContent: "space-between",
                }}
              >
                <Text
                  style={{
                    fontFamily: "Poppins",
                    fontSize: 13,
                    color: "#999999",
                  }}
                >
                  KDV Tutarı
                </Text>
                <Text
                  style={{
                    fontFamily: "Poppins",
                    fontSize: 13,
                    color: "#333333",
                  }}
                >
                  {numberToLocaleString(this.props.cart.cartInfo.SeciliSepetAltToplam.KdvToplam) +
                    " " +
                    this.props.cart.cartInfo.paraBirimi}
                </Text>
              </View>
              <View
                style={{
                  flexDirection: "row",
                  flex: 1,
                  height: verticalScale(34),
                  marginBottom: verticalScale(10),
                  marginHorizontal: SPACE_HORIZONTAL * 2,
                  justifyContent: "space-between",
                  alignItems: "center",
                }}
              >
                <Text
                  style={{
                    fontFamily: "Poppins",
                    fontSize: 13,
                    color: "#999999",
                  }}
                >
                  Genel Toplam
                </Text>
                <Text
                  style={{
                    fontFamily: "Poppins",
                    fontSize: 13,
                    color: "#333333",
                  }}
                >
                  {numberToLocaleString(this.props.cart.cartInfo.SeciliSepetAltToplam.GenelToplam) +
                    " " +
                    this.props.cart.cartInfo.paraBirimi}
                </Text>
              </View>
            </View>
          </View>
        </View>
      );
    else return null;
  };

  renderCollapsedCartSummary = () => {
    if (this.props.cart.error) return null;
    if (
      this.props.cart.cartInfo.IlSeciliSepetKalemler &&
      this.props.cart.cartInfo.IlSeciliSepetKalemler.length > 0
    )
      return (
        <View
          style={{
            position: "absolute",
            bottom: 0,
            flexDirection: "row",
            width: win.width,
            backgroundColor: "white",
            alignItems: "center",
            height: verticalScale(60),
            justifyContent: "space-between",
            shadowRadius: 10,
            shadowOffset: {
              width: 0,
              height: -8,
            },
            shadowColor: "#000",
            shadowOpacity: this.state.shadowOpacity,
            elevation: this.state.shadowOpacity === 0 ? 0 : 5,
            borderTopColor: "#dedede",
            borderTopWidth: 1,
            paddingHorizontal: moderateScale(5, 0.5),
          }}
        >
          <View
            style={{
              flexDirection: "row",
              flex: 3,
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <View style={{ flexDirection: "column" }}>
              <Text style={{ fontFamily: "Poppins", color: "#999", fontSize: 13 }}>
                Toplam Tutar
              </Text>
              <Text
                style={{
                  fontFamily: "Poppins-SemiBold",
                  color: "#d55749",
                  fontSize: 15,
                }}
              >
                {numberToLocaleString(this.props.cart.cartInfo.SeciliSepetAltToplam.GenelToplam) +
                  " " +
                  this.props.cart.cartInfo.paraBirimi}
              </Text>
            </View>
          </View>
          <View
            style={{
              flexDirection: "row",
              justifyContent:
                this.props.cart.cartInfo.FiyatTalepToplam >=
                this.props.cart.cartInfo.MinFiyatTalepTutari
                  ? "space-evenly"
                  : "flex-end",
              flex: 7,
              alignItems: "center",
            }}
          >
            {this.props.cart.cartInfo.FiyatTalepToplam >=
              this.props.cart.cartInfo.MinFiyatTalepTutari && (
              <TouchableOpacity
                onPress={this.requestPrice}
                style={[styles.btn, { backgroundColor: true ? "#fc7c05" : "#dedede" }]}
              >
                <Text
                  fontScale={0.5}
                  style={{
                    fontFamily: "Poppins-SemiBold",
                    fontSize: 12,
                    color: "white",
                  }}
                >
                  Hemen Teklif Al
                </Text>
              </TouchableOpacity>
            )}
            <TouchableOpacity
              style={[styles.btn, { backgroundColor: colors.light_green }]}
              onPress={() => this.startOrder()}
            >
              <Text
                fontScale={0.5}
                style={{
                  color: "white",
                  fontSize: 13,
                  fontFamily: "Poppins-SemiBold",
                }}
              >
                Sipariş Ver
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      );
    else return null;
  };

  renderNoContent = () => {
    return (
      <View style={{ width: win.width }}>
        <Text
          style={{
            fontSize: 20,
            color: "gray",
            textAlign: "center",
            marginTop: 10,
            backgroundColor: "transparent",
          }}
        >
          Sepet Boş
        </Text>
      </View>
    );
  };

  setSelectedItem = (item) => {
    this.setState({ selectedItem: item });
  };

  render() {
    console.log("rendering cart", this.props, this.state);
    return (
      <AnimatedBasePage
        ref={(c) => (this.basePage = c)}
        testID="screen_cart"
        forceInset={{ top: "always" }}
        isLoading={
          this.props.cart.isFetching || this.props.placeOrder.isFetching || this.state.progress
        }
        style={{ backgroundColor: colors.primary_dark, flex: 1 }}
        navigation={this.props.navigation}
        showFlatList={this.props.cart.cartInfo.SeciliSepetBaslik !== null && !this.state.progress}
        headerProps={{
          title: "Sepetim",
          back: false,
          alignLeft: false,
          showCart: true,
          searchBar: true,
          search: true,
        }}
        flatListProps={{
          testID: "scroll_cart",
          onLayout: ({
            nativeEvent: {
              layout: { height },
            },
          }) => {
            console.log("onlayout", this.innerHeight);
            this.innerHeight = height;
          },
          onContentSizeChange: (width, height) => (this.listHeight = height),
          overScrollMode: "never",
          refreshControl: (
            <RefreshControl
              refreshing={this.props.cart.refreshing}
              onRefresh={this._onRefresh.bind(this)}
              progressViewOffset={AnimatedHeader.height}
            />
          ),
          automaticallyAdjustContentInsets: false,
          data: this.props.cart.cartInfo.IlSeciliSepetKalemler,
          onScroll: this.onScroll,
          scrollEventThrottle: 50,
          contentContainerStyle: {
            paddingBottom: verticalScale(60),
            backgroundColor: "#efefef",
          },
          removeClippedSubviews: false,
          enableEmptySections: true,
          ListHeaderComponent: this.renderHeader,
          ListFooterComponent: this.renderCartSummary,
          renderItem: ({ item, index }) => this._renderRow(item, index),
          keyExtractor: (item) => {
            return item.SepetKalemKodu.toString();
          },
        }}
      >
        {this.renderErrorPrompt()}
        {this.renderCollapsedCartSummary()}
        <NewCartDialog ref={(c) => (this.newcartdialog = c)} addNewBucket={this.addNewBucket} />
        <BottomPicker
          ref={(c) => (this.bottomCurrencyPicker = c)}
          items={["TL", "USD", "EUR"]}
          initialItem={this.props.cart.cartInfo.paraBirimi}
          onPressOK={(pb) => this.updateSelectedCart(pb)}
        />
        {this.props.cart.cartInfo.IlKullaniciSepetleri && (
          <BottomPicker
            ref={(c) => (this.bottomPicker = c)}
            items={this.props.cart.cartInfo.IlKullaniciSepetleri}
            onPressOK={(sk) => this.changeSelectedCart(sk)}
            valueExtractor={(item) => item.SepetKodu}
            labelExtractor={(item) => item.SepetAdi}
            initialItem={this.props.cart.cartInfo.SeciliSepetBaslik}
          />
        )}
        {this.props.cart.cartInfo.SeciliSepetAltToplam && (
          <PriceInfoModal
            ref={(c) => (this.priceInfoModal = c)}
            GenelToplam={this.props.cart.cartInfo.SeciliSepetAltToplam.GenelToplam.toFixed(2)}
            AraToplam={this.props.cart.cartInfo.SeciliSepetAltToplam.AraToplam.toFixed(2)}
            KdvToplam={this.props.cart.cartInfo.SeciliSepetAltToplam.KdvToplam.toFixed(2)}
            ParaBirimi={this.props.cart.cartInfo.paraBirimi}
          />
        )}
        <BottomPicker
          ref={(c) => (this.quantityPicker = c)}
          initialItem={this.state.selectedSkk ? String(this.state.selectedSkk.UrunMiktar) : "1"}
          items={Array.from(new Array(300), (x, i) => String(i + 1))}
          onPressOK={(val) =>
            this.props.setNumber(this.state.selectedSkk.SepetKalemKodu, Number(val))
          }
        />
        <QuantityDialog
          ref={(c) => (this.quantityDialog = c)}
          onPressOK={() => {
            this.props.setNumber(
              this.state.selectedSkk.SepetKalemKodu,
              Number(this.quantityDialog.state.quantity),
            );
            this.quantityDialog.toggleVisibility();
          }}
        />
        <AddToFavoriteDialog
          ref={(c) => (this.addFavoriteDialog = c)}
          addFavorite={(f, s) => {
            const p = this.state.selectedItem;
            this.props.addFavorite(this.state.selectedItem.UrunKodu, f, s).then((msg) => {
              if (msg === "ADD_FAVORITE_SUCCESS") {
                Alert.alert("Başarılı İşlem", "Ürün favorilerinize eklendi.", [
                  {
                    text: "Tamam",
                    onPress: () => this.addFavoriteDialog.toggleVisibility(),
                  },
                ]);
                this.trackWichlist(p);
              }
            });
          }}
        />
        <PriceRequestResultModal ref={(c) => (this.requestResultModal = c)} />
        <FiyatTeklifiMaxAdetModal
          applyQuantities={async (UygunsuzUrunler) => {
            console.log("apply quantities", UygunsuzUrunler);
            this.fiyatTeklifiMaxAdetModal.toggleVisibility();

            for (var i = 0; i < UygunsuzUrunler.length; i++) {
              var kalemIndex = this.props.cart.cartInfo.IlSeciliSepetKalemler.findIndex(
                (kalem) => kalem.UrunKodu === UygunsuzUrunler[i].Kod,
              );
              if (kalemIndex !== -1) {
                await this.props.setNumber(
                  this.props.cart.cartInfo.IlSeciliSepetKalemler[kalemIndex].SepetKalemKodu,
                  UygunsuzUrunler[i].Adet,
                );
              }
            }
            this.requestPrice();
          }}
          ref={(c) => (this.fiyatTeklifiMaxAdetModal = c)}
        />
      </AnimatedBasePage>
    );
  }

  renderErrorPrompt = () =>
    ((this.props.cart.cartInfo.MesajTipi && this.props.cart.cartInfo.MesajTipi === "E") ||
      this.props.cart.error) && (
      <View
        style={{
          flexDirection: "column",
          width: win.width,
          flex: 1,
          alignItems: "center",
          justifyContent: "flex-start",
        }}
      >
        <Text style={{ fontFamily: "Poppins", fontSize: 14 }}>
          Sepetiniz yüklenirken bir problem oluştu.
        </Text>
        <TouchableOpacity
          onPress={() => this.props.getBucket()}
          style={{ flexDirection: "row", alignItems: "center" }}
        >
          <MaterialIcons size={14} name="refresh" color="blue" />
          <Text style={{ fontFamily: "Poppins", fontSize: 14, color: "blue" }}>Yeniden Yükle</Text>
        </TouchableOpacity>
      </View>
    );
  trackWichlist = (product) => {
    const _product = {
      item_id: product.UrunKodu,
      item_name: product.UrunAciklamasi,
      item_category: product.UrunHiyerarsi,
      quantity: product.UrunMiktar,
      price: product.UrunBirimFiyati,
      currency: product.UrunBirimFiyatPb,
    };
    analytics().logEvent("add_to_wishlist", _product);
  };
}

function mapStateToProps(state) {
  return {
    cart: state.cart,
    session: state.session,
    placeOrder: state.placeOrder,
    homePage: state.homePage,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    getBucket: (refresh) => dispatch(getBucket(refresh)),
    setCart: (cart) => dispatch(setCart(cart)),
    setBucket: (sk, pb) => dispatch(setBucket(sk, pb)),
    deleteBucket: (sk) => dispatch(deleteBucket(sk)),
    addBucket: (name) => dispatch(addBucket(name)),
    startOrder: () => dispatch(startOrder()),
    setNumber: (skk, quantity) => dispatch(setNumber(skk, quantity)),
    addFavorite: (pid, f, s) => dispatch(addFavorite(pid, f, s)),
    showToast: (msg) => dispatch(showToast(msg)),
    getPriceRequests: (refresh) => dispatch(getPriceRequests(refresh)),
    getFavorites: () => dispatch(getFavorites()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ShoppingCartScreen);
const styles = StyleSheet.create({
  loadingContainer: {
    height: win.height,
    width: win.width,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0,0,0,0.3)",
    position: "absolute",
  },
  btn: {
    flexDirection: "column",
    width: moderateScale(110, 0.4),
    alignItems: "center",
    justifyContent: "center",
    height: moderateScale(36, 0.4),
    borderRadius: 4,
  },
});
