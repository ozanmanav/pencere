import {
  SET_CART,
  GET_BUCKET,
  GET_BUCKET_FAILURE,
  IN_PROGRESS,
  ADD_REQUEST_TO_CART,
  ADD_REQUEST_TO_CART_SUCCESS,
  ADD_REQUEST_TO_CART_FAILURE,
} from "@redux/actions/types";
import { getEndPoint } from "@common/Utils";
import bugsnag from "@common/bugsnag_config";
import { showToast } from "@redux/actions/toast";
import { setGlobalProgress } from "@redux/actions/progress";
import analytics from "@react-native-firebase/analytics";
import axios from "axios";
import qs from "querystring";

import Endpoints from "@common/endpoints";
import { Api } from "@common/api";

var visilabsManager = require("@app/visilabsManager");

export function addPriceRequestToCart(ftk, pb) {
  return (dispatch, getState) => {
    return new Promise(async (resolve, reject) => {
      dispatch({
        type: ADD_REQUEST_TO_CART,
      });
      var response = await Api.instance.get(
        Endpoints.CART_URL,
        qs.stringify({
          Si: getState().session.info.SessionID,
          func: "12",
          pb: pb,
          ftk: ftk,
        }),
      );

      if (response?.data != null && "MesajTipi" in response.data) {
        if (response.data.DurumMesaj.toLowerCase().includes("eklendi")) {
          dispatch({ type: ADD_REQUEST_TO_CART_SUCCESS });
          dispatch(getBucket(true));
          resolve(response.data);
        } else {
          dispatch({ type: ADD_REQUEST_TO_CART_FAILURE });
          reject(response.data.DurumMesaj);
        }
      } else {
        dispatch({ type: ADD_REQUEST_TO_CART_FAILURE });
        reject("Beklenmedik bir hata oluştu. Daha sonra tekrar deneyiniz");
      }
    });
  };
}

export function getBucket(refresh) {
  return (dispatch, getState) => {
    return new Promise((resolve, reject) => {
      dispatch({
        type: GET_BUCKET,
        refresh,
      });
      Api.instance
        .get(
          "/mSepet.js?" +
            qs.stringify({
              si: getState().session.info.SessionID,
              func: 1,
            }),
        )
        .then((response) => {
          if (
            response?.headers != null &&
            (response.headers["content-type"].includes("javascript") ||
              response.headers["content-type"].includes("json")) &&
            response?.data?.MesajTipi !== "E"
          ) {
            dispatch(setCart(response.data));
            resolve(response.data);
          } else {
            dispatch({ type: GET_BUCKET_FAILURE });
            reject();
          }
        })
        .catch((error) => {
          bugsnag.notify(error, (report) => {
            report.errorClass = "getBucket";
          });
          console.log(error);
          dispatch({
            type: GET_BUCKET_FAILURE,
          });
          reject(error);
        });
    });
  };
}

export function removeFromCart(item) {
  return (dispatch, getState) => {
    dispatch({ type: IN_PROGRESS, progress: true });
    return new Promise((resolve, reject) => {
      Api.instance
        .get(
          Endpoints.CART_URL +
            "?" +
            qs.stringify({
              si: getState().session.info.SessionID,
              func: 6,
              skk: item.SepetKalemKey,
            }),
        )
        .then((response) => {
          console.log("removeFromCart response", response.data);
          dispatch(setCart(response.data));
          try {
            const pp = {
              item_id: item.UrunKodu,
              item_name: item.UrunAciklamasi,
              category: "Belirsiz",
              brand: item.UrunMarka,
              price: item.UrunBirimFiyati,
              currency: item.UrunBirimFiyatPb,
              quantity: Number(item.UrunMiktar),
            };
            analytics().logEvent("remove_from_cart", pp);
          } catch (error) {
            bugsnag.notify(error, (report) => {
              report.errorClass = "trackRemoveFromCart";
            });
          }
          dispatch({ type: IN_PROGRESS, progress: false });
          resolve(response.data);
          dispatch(showToast("Ürün sepetinizden çıkarıldı", 2000));
        })
        .catch((error) => {
          bugsnag.notify(error, (report) => {
            report.errorClass = "removeFromCart";
          });
          console.log("removeFromCart error", error);
          reject(error);
          dispatch({ type: IN_PROGRESS, progress: false });
        });
    });
  };
}

export function addToCart(uk, number) {
  return (dispatch, getState) => {
    dispatch(setGlobalProgress(true));
    Api.instance
      .get(
        Endpoints.CART_URL +
          "?" +
          qs.stringify({
            si: getState().session.info.SessionID,
            func: 8,
            uk,
            adt: number,
          }),
      )
      .then((response) => {
        if (response.data.MesajTipi === "S") {
          dispatch(setCart(response.data));
          dispatch(showToast("Ürün sepete eklendi"));
        } else {
          dispatch(showToast("Bir hata oluştu lütfen tekrar deneyin"));
        }
        dispatch(setGlobalProgress(false));
      })
      .catch((error) => {
        dispatch(setGlobalProgress(false));
        dispatch(showToast("Bir hata oluştu lütfen tekrar deneyin"));
        console.log("addToCart error", error);
      });
  };
}

export function setCart(cart) {
  visilabsManager.reportCart(cart.SeciliSepetBaslik.SepetKodu, cart.IlSeciliSepetKalemler);
  return (dispatch) => {
    dispatch({
      type: SET_CART,
      data: cart,
    });
  };
}

export function setBucket(sk, pb) {
  return (dispatch, getState) => {
    dispatch({ type: GET_BUCKET, refresh: false });
    Api.instance
      .get(
        Endpoints.CART_URL +
          "?" +
          qs.stringify({
            si: getState().session.info.SessionID,
            func: 5,
            sk,
            pb,
          }),
      )
      .then((response) => {
        dispatch(setCart(response.data));
      });
  };
}

export function setNumber(skk, quantity) {
  return (dispatch, getState) => {
    dispatch({ type: IN_PROGRESS, progress: true });
    Api.instance
      .get(
        Endpoints.CART_URL +
          "?" +
          qs.stringify({
            si: getState().session.info.SessionID,
            func: 7,
            skk,
            adt: quantity,
          }),
      )
      .then((response) => {
        dispatch(setCart(response.data));
      })
      .catch((error) => {
        bugsnag.notify(error, (report) => {
          report.errorClass = "setNumber";
        });
        console.log("setNumber error", error);
      });
  };
}

export function deleteBucket(sk) {
  return (dispatch, getState) => {
    return new Promise(function (resolve, reject) {
      Api.instance
        .get(
          Endpoints.CART_URL +
            "?" +
            qs.stringify({
              si: getState().session.info.SessionID,
              func: 3,
              sk,
            }),
        )
        .then((response) => {
          dispatch(setCart(response.data));
          resolve({ type: "DELETE_BUCKET" });
        })
        .catch((err) => {
          bugsnag.notify(err, (report) => {
            report.errorClass = "fetchHesapEkstresiFromApi";
          });
          reject(err);
        });
    });
  };
}

export function addBucket(name) {
  return (dispatch, getState) => {
    dispatch({ type: IN_PROGRESS, progress: true });
    Api.instance
      .get(
        Endpoints.CART_URL +
          "?" +
          qs.stringify({
            si: getState().session.info.SessionID,
            func: 2,
            sa: name,
          }),
      )
      .then((response) => {
        dispatch(setCart(response.data));
      });
  };
}
