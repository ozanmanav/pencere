const INITAL_STATE = {
  cartInfo: [],
  isFetching: false,
  error: false,
  refreshing: false,
  count: 0,
};
import {
  GET_BUCKET_SUCCESS,
  SET_CART,
  IN_PROGRESS,
  GET_BUCKET,
  GET_BUCKET_FAILURE,
} from "@redux/actions/types";

export default function cartReducer(state = INITAL_STATE, action) {
  switch (action.type) {
    case IN_PROGRESS:
      return {
        ...state,
        isFetching: action.progress,
      };
    case GET_BUCKET:
      return {
        ...state,
        refreshing: action.refresh,
        isFetching: !action.refresh,
      };
    case SET_CART:
      var count = 0;
      if (action.data.IlSeciliSepetKalemler != null)
        action.data.IlSeciliSepetKalemler.forEach((element) => {
          if (element.Referans3 === "" || element.Referans3 === "0") {
            count++;
          }
        });
      return {
        ...state,
        cartInfo: action.data,
        isFetching: false,
        refreshing: false,
        count,
      };
    case GET_BUCKET_SUCCESS:
      return {
        ...state,
        cartInfo: action.data,
        refreshing: false,
        isFetching: false,
        error: false,
      };
    case GET_BUCKET_FAILURE:
      return {
        ...state,
        error: true,
        refreshing: false,
        isFetching: false,
      };
    default:
      return state;
  }
}
