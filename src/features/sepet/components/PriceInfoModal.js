import React, { Component } from "react";
import { View, StyleSheet, Dimensions, Modal, TouchableHighlight } from "react-native";
import { Text } from "@components";

const win = Dimensions.get("window");

export default class PriceInfoModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isVisible: false,
    };
  }
  currencies = ["TL", "USD", "EUR"];

  componentDidUpdate(prevProps, prevState) {
    if (this.props.isVisible !== prevProps.isVisible)
      this.setState({
        isVisible: this.props.isVisible,
      });
  }

  toggleVisibility = () => {
    this.setState({
      isVisible: !this.state.isVisible,
    });
  };
  render() {
    if (this.state.isVisible) {
      return (
        <Modal
          animationType="none"
          transparent={true}
          presentationStyle="overFullScreen"
          visible={this.state.isVisible}
          onRequestClose={() => {
            this.setState({
              isVisible: false,
            });
          }}
        >
          <TouchableHighlight
            style={{
              width: win.width,
              justifyContent: "center",
              alignItems: "center",
              height: win.height,
              backgroundColor: "rgba(0,0,0,0.2)",
            }}
            onPress={() => this.setState({ isVisible: false })}
          >
            <View style={styles.container}>
              <View style={{ flexDirection: "row", justifyContent: "space-between", padding: 10 }}>
                <Text style={{ fontSize: 15, color: "gray" }}>Ara Toplam:</Text>
                <Text style={{ fontSize: 15, color: "gray" }}>
                  {this.props.AraToplam + " " + this.props.ParaBirimi}
                </Text>
              </View>
              <View style={{ flexDirection: "row", justifyContent: "space-between", padding: 10 }}>
                <Text style={{ fontSize: 15, color: "gray" }}>Kdv Toplam:</Text>
                <Text style={{ fontSize: 15, color: "gray" }}>
                  {this.props.KdvToplam + " " + this.props.ParaBirimi}
                </Text>
              </View>
              <View
                style={{
                  backgroundColor: "#dedede",
                  width: win.width * 0.7,
                  height: 1,
                  flexDirection: "column",
                }}
              />
              <View style={{ flexDirection: "row", justifyContent: "space-between", padding: 10 }}>
                <Text style={{ fontSize: 15, color: "gray", fontWeight: "bold" }}>
                  Genel Toplam:
                </Text>
                <Text style={{ fontSize: 15, color: "#d6421e", fontWeight: "bold" }}>
                  {this.props.GenelToplam + " " + this.props.ParaBirimi}
                </Text>
              </View>
            </View>
          </TouchableHighlight>
        </Modal>
      );
    } else return null;
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
    width: win.width * 0.7,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.5,
    shadowRadius: 1,
    elevation: 1,
    borderRadius: 3,
  },
});
