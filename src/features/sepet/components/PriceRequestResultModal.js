import React, { Component } from "react";
import { View, TouchableOpacity, Image } from "react-native";
import { ScaledSheet, moderateScale } from "react-native-size-matters";
import { Text, SwipableModal } from "@components";
import { getWidth, getHeight } from "@common/Utils";
import colors from "@common/styles/colors";
import NavigationService from "@app/NavigationService";

export default class PriceRequestResultModal extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  toggleVisibility = () => {
    this.modalBox.toggleVisibility();
  };

  render() {
    return (
      <SwipableModal ref={(c) => (this.modalBox = c)}>
        <View style={styles.container}>
          <TouchableOpacity
            style={{
              position: "absolute",
              top: moderateScale(10, 0.5),
              right: moderateScale(10, 0.5),
            }}
            onPress={() => this.toggleVisibility()}
          >
            <Image source={require("@assets/images/x.png")} style={styles.x} />
          </TouchableOpacity>

          <Text
            fontScale={0.5}
            style={{ fontFamily: "Poppins-SemiBold", color: "black", fontSize: 24 }}
          >
            Teklif Oluşturuldu
          </Text>
          {/* <View style={{ flexDirection: 'row' }}>
                        <Text fontScale={0.5}  style={{ fontFamily: 'Poppins', color: 'black', fontSize: 15 }}>Teklif Adı:</Text>
                        <Text fontScale={0.5}  style={{ fontFamily: 'Poppins-SemiBold', color: 'black', fontSize: 15 }}> Teklif Adı gelecek</Text>
                    </View> */}
          <Text fontScale={0.5} style={{ fontSize: 12 }}>
            <Text fontScale={0.5} style={{ fontFamily: "Poppins", color: "gray" }}>
              Fiyat teklifinize en kısa zamanda geri dönüş yapılacaktır. Tüm tekliflerinizi takip
              etmek ve detaylarını görmek için
            </Text>
            <Text
              fontScale={0.5}
              style={{ fontFamily: "Poppins-SemiBold", color: colors.price_color }}
            >
              {" "}
              Fiyat Tekliflerim{" "}
            </Text>
            <Text fontScale={0.5} style={{ fontFamily: "Poppins", color: "gray" }}>
              sayfasına gidebilirsiniz
            </Text>
          </Text>
          <TouchableOpacity
            style={styles.btn}
            onPress={() => {
              this.toggleVisibility();
              NavigationService.navigate(null, "PriceRequestsScreen", { showAlert: false });
            }}
          >
            <Text
              fontScale={0.5}
              style={{ fontFamily: "Poppins-Medium", color: "white", fontSize: 12 }}
            >
              Hemen Git!
            </Text>
            <Image source={require("@assets/images/arrow-right.png")} style={styles.arrow} />
          </TouchableOpacity>
        </View>
      </SwipableModal>
    );
  }
}

const styles = ScaledSheet.create({
  container: {
    backgroundColor: "white",
    borderRadius: "10@ms0.4",
    padding: getWidth() / 25,
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "space-evenly",
    height: getHeight() / 3,
    marginHorizontal: "5@ms0.5",
  },
  btn: {
    flexDirection: "row",
    width: moderateScale(120, 0.5),
    alignItems: "center",
    justifyContent: "center",
    height: moderateScale(32, 0.5),
    borderRadius: 4,
    backgroundColor: colors.light_green,
  },
  arrow: {
    width: "12@ms0.3",
    height: "12@ms0.3",
    resizeMode: "contain",
    marginLeft: "5@ms0.4",
  },
  x: {
    width: moderateScale(14, 0.4),
    height: moderateScale(14, 0.4),
    tintColor: "#696969",
  },
});
