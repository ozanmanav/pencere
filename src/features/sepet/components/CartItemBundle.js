import React, { Component } from "react";
import {
  View,
  TouchableOpacity,
  Clipboard,
  Image,
  Dimensions,
  LayoutAnimation,
  Animated,
  TouchableHighlight,
  Alert,
  TextInput,
} from "react-native";

import { Text, SmartImage } from "@components";
import { connect } from "react-redux";
import { setCart, setNumber, removeFromCart, showToast } from "@redux/actions";

import { ScaledSheet } from "react-native-size-matters";
import { scale, moderateScale } from "react-native-size-matters";
import Interactable from "react-native-interactable";
import { UPDATE_SPRINGY } from "@common/LayoutAnimations";
import { removeFavorite } from "@redux/actions";

const win = Dimensions.get("window");
const HORIZONTAL_PADDING = win.width / 36;

class CartItemBundle extends Component {
  increaseNumber = (skk, num) => {
    console.log("skk", skk, num);
    this.props.setNumber(skk, num + 1);
  };
  decreaseNumber = (skk, num) => {
    console.log("skk", skk, "num", num);
    this.props.setNumber(skk, num - 1);
  };
  constructor(props) {
    super(props);
    this.state = {
      position: 0,
      number: 0,
      image: "",
      imageError: false,
      isFavorite: false,
    };
    this._deltaX = new Animated.Value(0);
  }

  componentDidMount() {
    this.setState({
      number: this.props.item.UrunMiktar,
      image: { uri: this.props.item.UrunResimleri.DefaultResim.Connact.URL },
    });
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.item.UrunMiktar !== this.props.item.UrunMiktar)
      this.setState({ number: this.props.item.UrunMiktar });
    if (prevProps.favorites !== this.props.favorites) {
      var isFavorite = false;
      this.props.favorites.items.forEach(item => {
        if (item.urunKodu === this.props.item.UrunKodu) {
          isFavorite = true;
        }
      });
      this.setState({ isFavorite });
    }
  }

  onSnap({ nativeEvent }) {
    const { index } = nativeEvent;
    console.log("onsnap", index);
    this.setState({ position: index });
  }

  renderTag = () => {
    if (this.props.item.UrunBundle || this.props.item.UrunTipi === "Bundle") {
      return (
        <Text>
          <Image
            source={require("@assets/images/bundle.png")}
            style={{
              width: scale((14 * 144) / 41),
              height: scale(14),
              resizeMode: "contain",
            }}
          />
          <Text> </Text>
        </Text>
      );
    } else if (this.props.item.UrunHediyeli || this.props.item.UrunTipi === "Hediyeli") {
      return (
        <Text>
          <Image
            source={require("@assets/images/hediyeli.png")}
            style={{
              width: scale((14 * 144) / 41),
              height: scale(14),
              resizeMode: "contain",
            }}
          />
          <Text> </Text>
        </Text>
      );
    } else if (this.props.item.UrunCrossSell || this.props.item.UrunTipi === "CrossSell") {
      return (
        <Text>
          <Image
            source={require("@assets/images/crossSell.png")}
            style={{
              width: scale((14 * 273) / 41),
              height: scale(14),
              resizeMode: "contain",
            }}
          />
          <Text> </Text>
        </Text>
      );
    } else if (this.props.item.UrunSeriNolu) {
      return (
        <Text>
          <Image
            source={require("@assets/images/defolu.png")}
            style={{
              width: scale((14 * 213) / 41),
              height: scale(14),
              resizeMode: "contain",
            }}
          />
          <Text> </Text>
        </Text>
      );
    } else return null;
  };

  renderPriceReqTag = () => {
    if (this.props.item.KalemFiyatTalepDurumu === "Y")
      return (
        <View
          style={[
            styles.pqTag,
            {
              backgroundColor: "orange",
            },
          ]}
        >
          <Text
            fontScale={0.5}
            style={{
              fontFamily: "Poppins-Medium",
              color: "white",
              fontSize: 13,
            }}
          >
            Teklif İstenebilir
          </Text>
        </View>
      );
    else if (this.props.item.KalemFiyatTalepDurumu === "X")
      return (
        <View
          style={[
            styles.pqTag,
            {
              backgroundColor: "#dedede",
            },
          ]}
        >
          <Text
            fontScale={0.5}
            style={{
              fontFamily: "Poppins-Medium",
              color: "white",
              fontSize: 13,
            }}
          >
            Teklife Uygun Değil
          </Text>
        </View>
      );
    else if (this.props.item.KalemFiyatTalepDurumu === "N")
      return (
        <View
          style={[
            styles.pqTag,
            {
              backgroundColor: "#dedede",
            },
          ]}
        >
          <Text
            fontScale={0.5}
            style={{
              fontFamily: "Poppins-Medium",
              color: "white",
              fontSize: 13,
            }}
          >
            Teklif İçin Yetersiz Adet
          </Text>
        </View>
      );
  };

  snapToDefault = () => {
    this.cartView.snapTo({ index: 0 });
  };

  render() {
    console.log("bundle props", this.props);
    return (
      <View
        style={{
          flexDirection: "row",
          justifyContent: "flex-end",
          marginHorizontal: HORIZONTAL_PADDING,
          backgroundColor: "white",
        }}
      >
        <View
          style={{
            position: "absolute",
            right: moderateScale(80, 0.5),
            backgroundColor: this.state.isFavorite ? "#c4c4c4" : "#4CAF50",
            alignItems: "center",
            justifyContent: "center",
            height: "100%",
            width: moderateScale(80, 0.5),
          }}
        >
          <TouchableOpacity
            style={{
              flexDirection: "column",
              alignItems: "center",
              justifyContent: "center",
            }}
            onPress={() => {
              if (!this.state.isFavorite) {
                this.props.setSelectedItem(this.props.item);
                this.props.addFavoriteDialog.toggleVisibility();
              } else {
                Alert.alert(
                  "Favorilerimden Çıkar",
                  "Bu ürünü favorilerinizden çıkarmak istediğinize emin misiniz?",
                  [
                    {
                      text: "Hayır",
                    },
                    {
                      text: "Evet",
                      onPress: () => {
                        this.props.removeFavorite(this.props.item);
                      },
                    },
                  ],
                  { cancelable: false },
                );
              }
            }}
          >
            <Animated.View
              style={{
                alignItems: "center",
                justifyContent: "center",
                paddingHorizontal: moderateScale(5, 0.5),
                transform: [
                  {
                    scaleX: this._deltaX.interpolate({
                      inputRange: [-moderateScale(160, 0.5), -moderateScale(80, 0.5)],
                      outputRange: [1, 0],
                      extrapolateLeft: "clamp",
                      extrapolateRight: "clamp",
                    }),
                  },
                  {
                    scaleY: this._deltaX.interpolate({
                      inputRange: [-moderateScale(160, 0.5), -moderateScale(80, 0.5)],
                      outputRange: [1, 0],
                      extrapolateLeft: "clamp",
                      extrapolateRight: "clamp",
                    }),
                  },
                ],
              }}
            >
              <Image
                source={require("@assets/images/star.png")}
                style={{
                  marginBottom: moderateScale(5, 0.5),
                  width: moderateScale(24, 0.7),
                  height: moderateScale(24, 0.7),
                  tintColor: "white",
                }}
              />
              <Text
                style={{
                  fontFamily: "Poppins",
                  color: "white",
                  fontSize: 10,
                  textAlign: "center",
                }}
              >
                {this.state.isFavorite ? "Favorilerden Çıkar" : "Favorilere Ekle"}
              </Text>
            </Animated.View>
          </TouchableOpacity>
        </View>
        <TouchableOpacity
          onPress={() => {
            Alert.alert(
              "Ürünü Sil",
              "Bu ürünü sepetinizden çıkarmak istediğinize emin misiniz?",
              [
                {
                  text: "Hayır",
                },
                {
                  text: "Evet",
                  onPress: () => {
                    this.props.removeFromCart(this.props.item).then(() => {
                      LayoutAnimation.configureNext(UPDATE_SPRINGY);
                    });
                  },
                },
              ],
              { cancelable: false },
            );
          }}
          style={{
            position: "absolute",
            right: 1,
            backgroundColor: "#e53935",
            flexDirection: "row",
            height: "100%",
            width: moderateScale(80, 0.5),
            borderColor: "white",
            borderBottomRightRadius: this.props.index === this.props.length - 1 ? 5 : 0,
            paddingHorizontal: moderateScale(10, 0.5),
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <Animated.View
            style={{
              flexDirection: "column",
              alignItems: "center",
              justifyContent: "center",
              paddingHorizontal: moderateScale(5, 0.5),
              transform: [
                {
                  scaleX: this._deltaX.interpolate({
                    inputRange: [-moderateScale(80, 0.5), -moderateScale(0, 0.5)],
                    outputRange: [1, 0],
                    extrapolateLeft: "clamp",
                    extrapolateRight: "clamp",
                  }),
                },
                {
                  scaleY: this._deltaX.interpolate({
                    inputRange: [-moderateScale(80, 0.5), -moderateScale(0, 0.5)],
                    outputRange: [1, 0],
                    extrapolateLeft: "clamp",
                    extrapolateRight: "clamp",
                  }),
                },
              ],
            }}
          >
            <Image
              source={require("@assets/images/trash.png")}
              style={{
                marginBottom: moderateScale(5, 0.5),
                width: moderateScale(24, 0.7),
                height: moderateScale(24, 0.7),
                tintColor: "white",
              }}
            />
            <Text
              style={{
                fontFamily: "Poppins",
                color: "white",
                fontSize: 10,
                textAlign: "center",
              }}
            >
              Sepetten Çıkar
            </Text>
          </Animated.View>
        </TouchableOpacity>
        <Interactable.View
          animatedNativeDriver={true}
          ref={c => (this.cartView = c)}
          style={{
            flex: 1,
          }}
          animatedValueX={this._deltaX}
          horizontalOnly={true}
          boundaries={{
            left: -win.width + moderateScale(160, 0.5),
            right: 0,
            bounce: 0,
          }}
          snapPoints={[
            { x: 0, damping: 0.5, tension: 600, haptics: true },
            { x: -moderateScale(80, 0.5) },
            { x: -moderateScale(160, 0.5) },
          ]}
          onSnap={this.onSnap.bind(this)}
          // }}
        >
          <TouchableHighlight
            activeOpacity={0.1}
            underlayColor="white"
            onPress={() => {
              if (this.state.position !== 0) {
                this.snapToDefault();
              } else {
                this.props.navigation.navigate({
                  key: "cart",
                  routeName: "ProductDetailsScreen",
                  params: {
                    UrunKodu: this.props.item.UrunKodu,
                    SeriNo: this.props.item.UrunSerino,
                    isFavorite: false,
                    pb: this.props.item.paraBirimi,
                  },
                });
              }
            }}
            style={{
              backgroundColor: "white",
              borderRadius: 3,
              padding: HORIZONTAL_PADDING,
              paddingVertical: HORIZONTAL_PADDING * 2,
              borderColor: "#ededed",
              borderBottomWidth: this.props.index < this.props.length - 1 ? 1 : 0,
              borderTopLeftRadius: 0,
              borderTopRightRadius: 0,
              borderBottomLeftRadius: this.props.index === this.props.length - 1 ? 4 : 0,
              borderBottomRightRadius: this.props.index === this.props.length - 1 ? 4 : 0,
            }}
          >
            <View>
              <View style={{ flexDirection: "row", flex: 1 }}>
                <View style={styles.prdImgContainer}>
                  <SmartImage
                    style={styles.prdImg}
                    resizeMode="contain"
                    source={{
                      uri: this.props.item.UrunResimleri.DefaultResim.Connact.URL,
                    }}
                  />
                </View>
                <View
                  style={{
                    flexDirection: "row",
                    flex: 1,
                    paddingLeft: win.width * 0.03,
                    minHeight: moderateScale(75, 0.5),
                  }}
                >
                  <View
                    style={{
                      flexDirection: "column",
                      minHeight: moderateScale(70, 0.5),
                      flexGrow: 1,
                      justifyContent: "space-between",
                    }}
                  >
                    <View
                      style={{
                        flexDirection: "row",
                        alignItems: "flex-start",
                        flex: 1,
                      }}
                    >
                      <Text
                        onPress={() => {
                          Clipboard.setString(this.props.item.UrunKodu);
                          this.props.showToast("Ürün kodu kopyalandı");
                        }}
                        numberOfLines={3}
                        ellipsizeMode="tail"
                        style={{
                          flexDirection: "row",
                          flexWrap: "wrap",
                          flex: 1,
                        }}
                      >
                        {this.renderTag()}
                        <Text
                          style={{
                            alignSelf: "flex-start",
                            flexWrap: "wrap",
                            flex: 1,
                            paddingRight: 5,
                          }}
                        >
                          <Text
                            style={{
                              fontFamily: "Poppins",
                              color: this.props.item.UrunBelirtilenAdetteTeminEdilememekte
                                ? "gray"
                                : "#03A9F4",
                              fontSize: 11,
                            }}
                          >
                            {this.props.item.UrunMarka + " "}
                          </Text>
                          <Text
                            style={{
                              fontFamily: "Poppins",
                              color: "gray",
                              fontSize: 11,
                            }}
                          >
                            {this.props.item.UrunKodu + " "}
                          </Text>
                          <Text
                            onLongPress={() => {
                              Clipboard.setString(this.props.item.UrunAciklamasi);
                              this.props.showToast("Ürün açıklaması kopyalandı");
                            }}
                            onPress={() => {
                              this.props.navigation.navigate({
                                key: "cart",
                                routeName: "ProductDetailsScreen",
                                params: {
                                  UrunKodu: this.props.item.UrunKodu,
                                  SeriNo: this.props.item.UrunSerino,
                                  isFavorite: false,
                                },
                              });
                            }}
                            style={{
                              fontFamily: "Poppins",
                              fontSize: 11,
                              textAlign: "left",
                              color: this.props.item.UrunBelirtilenAdetteTeminEdilememekte
                                ? "gray"
                                : "black",
                              flexWrap: "wrap",
                            }}
                          >
                            {this.props.item.UrunAciklamasi}
                          </Text>
                        </Text>
                      </Text>
                      <TouchableOpacity
                        onPress={() => {
                          Alert.alert(
                            "Ürünü Sil",
                            "Bu ürünü sepetinizden çıkarmak istediğinize emin misiniz?",
                            [
                              {
                                text: "Hayır",
                                onPress: () => {
                                  console.log("hayır");
                                },
                                style: "cancel",
                              },
                              {
                                text: "Evet",
                                onPress: () => {
                                  this.props.removeFromCart(this.props.item).then(() => {
                                    LayoutAnimation.configureNext(UPDATE_SPRINGY);
                                  });
                                },
                              },
                            ],
                            { cancelable: false },
                          );
                        }}
                        style={{
                          paddingTop: moderateScale(10, 0.5),
                          paddingBottom: moderateScale(10, 0.5),
                          paddingLeft: moderateScale(10, 0.5),
                          alignItems: "flex-end",
                          justifyContent: "flex-start",
                        }}
                      >
                        <Image
                          source={require("@assets/images/delete-button.png")}
                          style={{
                            width: moderateScale(17, 0.7),
                            height: moderateScale(17, 0.7),
                            tintColor: "gray",
                            top: -moderateScale(10, 0.5),
                          }}
                        />
                      </TouchableOpacity>
                    </View>
                    {this.props.item.UrunBelirtilenAdetteTeminEdilememekte && (
                      <View
                        style={{
                          flexDirection: "row",
                          height: 32,
                          backgroundColor: "#cbcbcb",
                          alignItems: "center",
                          justifyContent: "center",
                          borderRadius: 3,
                        }}
                      >
                        <Text
                          style={{
                            fontFamily: "Poppins-Medium",
                            fontSize: 13,
                            textAlign: "center",
                            color: "white",
                          }}
                        >
                          Ürün temin edilememektedir.
                        </Text>
                      </View>
                    )}
                    {
                      <View
                        style={{
                          flexDirection: "row",
                          flex: 1,
                          justifyContent: "space-between",
                          alignItems: "flex-start",
                          marginTop: moderateScale(5, 0.5),
                        }}
                      >
                        {this.props.item.UrunFirsatUrunuMu ? (
                          <View
                            style={{
                              flexDirection: "row",
                              flex: 1,
                              justifyContent: "space-between",
                              maxWidth: win.width * 0.4,
                            }}
                          >
                            <View
                              style={{
                                flexDirection: "column",
                                alignItems: "flex-start",
                              }}
                            >
                              <Text
                                style={{
                                  fontFamily: "Poppins",
                                  fontSize: 11,
                                  lineHeight: 13,
                                  textAlign: "left",
                                  color: "#909090",
                                }}
                              >
                                Birim Fiyatı
                              </Text>
                              <Text
                                style={{
                                  fontFamily: "Poppins",
                                  fontSize: 11,
                                  lineHeight: 13,
                                  textAlign: "left",
                                  color: "#909090",
                                }}
                              >
                                {" "}
                              </Text>
                              <Text
                                style={{
                                  fontFamily: "Poppins",
                                  fontSize: 11,
                                  lineHeight: 13,
                                  textAlign: "left",
                                  color: "#909090",
                                  marginTop: 2,
                                }}
                              >
                                Toplam Fiyat
                              </Text>
                            </View>
                            <View
                              style={{
                                flexDirection: "column",
                                alignItems: "flex-end",
                              }}
                            >
                              <Text
                                style={{
                                  fontFamily: "Poppins",
                                  fontSize: 11,
                                  lineHeight: 13,
                                  textAlign: "left",
                                  color: "gray",
                                  textDecorationLine: "line-through",
                                }}
                              >
                                {Number(this.props.item.EskiUrunBirimFiyati)
                                  .toFixed(2)
                                  .toString()
                                  .replace(".", ",") +
                                  " " +
                                  this.props.item.UrunBirimFiyatPb}
                              </Text>
                              <Text
                                style={{
                                  fontFamily: "Poppins",
                                  fontSize: 11,
                                  lineHeight: 13,
                                  textAlign: "left",
                                  color: "black",
                                }}
                              >
                                {Number(this.props.item.UrunBirimFiyati)
                                  .toFixed(2)
                                  .toString()
                                  .replace(".", ",") +
                                  " " +
                                  this.props.item.UrunBirimFiyatPb}
                              </Text>
                              <Text
                                style={{
                                  fontFamily: "Poppins",
                                  fontSize: 11,
                                  lineHeight: 13,
                                  textAlign: "left",
                                  color: "gray",
                                  textDecorationLine: "line-through",
                                  marginTop: 2,
                                }}
                              >
                                {Number(this.props.item.EskiUrunToplamFiyat)
                                  .toFixed(2)
                                  .toString()
                                  .replace(".", ",") +
                                  " " +
                                  this.props.item.UrunBirimFiyatPb}
                              </Text>
                              <Text
                                style={{
                                  fontFamily: "Poppins",
                                  fontSize: 11,
                                  lineHeight: 13,
                                  textAlign: "left",
                                  color: "black",
                                }}
                              >
                                {Number(this.props.item.UrunToplamFiyat)
                                  .toFixed(2)
                                  .toString()
                                  .replace(".", ",") +
                                  " " +
                                  this.props.item.UrunBirimFiyatPb}
                              </Text>
                            </View>
                          </View>
                        ) : (
                          <View
                            style={{
                              flexDirection: "row",
                              flex: 1,
                              justifyContent: "space-between",
                              maxWidth: win.width * 0.4,
                            }}
                          >
                            <View style={{ flexDirection: "column" }}>
                              <Text
                                style={{
                                  fontFamily: "Poppins",
                                  fontSize: 11,
                                  lineHeight: 13,
                                  textAlign: "left",
                                  color: "#909090",
                                }}
                              >
                                Birim Fiyatı
                              </Text>
                              <Text
                                style={{
                                  fontFamily: "Poppins",
                                  fontSize: 11,
                                  lineHeight: 13,
                                  textAlign: "left",
                                  color: "#909090",
                                  marginTop: 2,
                                }}
                              >
                                Toplam Fiyat
                              </Text>
                            </View>
                            <View style={{ flexDirection: "column" }}>
                              <Text
                                style={{
                                  fontFamily: "Poppins",
                                  fontSize: 11,
                                  lineHeight: 13,
                                  textAlign: "left",
                                  color: "black",
                                }}
                              >
                                {Number(this.props.item.UrunBirimFiyati)
                                  .toFixed(2)
                                  .toString()
                                  .replace(".", ",") +
                                  " " +
                                  this.props.item.UrunBirimFiyatPb}
                              </Text>
                              <Text
                                style={{
                                  fontFamily: "Poppins",
                                  fontSize: 11,
                                  lineHeight: 13,
                                  textAlign: "left",
                                  color: "black",
                                  marginTop: 2,
                                }}
                              >
                                {Number(this.props.item.UrunToplamFiyat)
                                  .toFixed(2)
                                  .toString()
                                  .replace(".", ",") +
                                  " " +
                                  this.props.item.UrunBirimFiyatPb}
                              </Text>
                            </View>
                          </View>
                        )}
                        <View
                          style={{
                            flexDirection: "row",
                            backgroundColor: "#e2e2e2",
                            paddingVertical: moderateScale(1, 0.4),
                          }}
                        >
                          <TouchableOpacity
                            onPress={() =>
                              this.decreaseNumber(this.props.item.SepetKalemKodu, this.state.number)
                            }
                            style={{
                              width: moderateScale(24, 0.5),
                              borderTopLeftRadius: 0,
                              borderBottomLeftRadius: 0,
                              borderColor: "#e2e2e2",
                              borderRadius: 3,
                              borderWidth: 1,
                              height: moderateScale(28, 0.5),
                              alignItems: "center",
                              justifyContent: "center",
                            }}
                          >
                            <Text
                              style={{
                                textAlign: "center",
                                fontSize: 19,
                                color: "rgba(0,0,0,0.5)",
                                fontWeight: "bold",
                              }}
                            >
                              -
                            </Text>
                          </TouchableOpacity>
                          <TextInput
                            maxLength={3}
                            onFocus={() => {
                              this.qInput.measure((fx, fy, width, height, px, py) => {
                                this.props.scrollToFocusedInput(py);
                              });
                            }}
                            ref={c => (this.qInput = c)}
                            selectTextOnFocus={true}
                            returnKeyType="done"
                            keyboardType="numeric"
                            style={{
                              color: Number(this.state.number) > 0 ? "black" : "red",
                              padding: moderateScale(2, 0.4),
                              fontFamily: "Poppins",
                              fontSize: 15,
                              width: moderateScale(30, 0.5),
                              height: moderateScale(28, 0.5),
                              borderTopColor: "#e2e2e2",
                              borderTopWidth: 1,
                              borderBottomColor: "#e2e2e2",
                              borderBottomWidth: 1,
                              backgroundColor: "white",
                              textAlign: "center",
                            }}
                            onChangeText={text => {
                              this.setState({ number: text });
                            }}
                            onEndEditing={() => {
                              if (Number(this.state.number) > 0) {
                                if (Number(this.state.number) !== this.props.item.UrunMiktar)
                                  this.props.setNumber(
                                    this.props.item.SepetKalemKodu,
                                    Number(this.state.number),
                                  );
                              } else {
                                Alert.alert("Hata", "Adet sayısı 0'dan büyük olmalı");
                                this.setState({
                                  number: this.props.item.UrunMiktar,
                                });
                              }
                            }}
                            value={String(this.state.number)}
                          />
                          <TouchableOpacity
                            onPress={() =>
                              this.increaseNumber(this.props.item.SepetKalemKodu, this.state.number)
                            }
                            style={{
                              width: moderateScale(24, 0.5),
                              borderTopLeftRadius: 0,
                              borderBottomLeftRadius: 0,
                              borderColor: "#e2e2e2",
                              borderRadius: 3,
                              borderWidth: 1,
                              height: moderateScale(28, 0.5),
                              alignItems: "center",
                              justifyContent: "center",
                            }}
                          >
                            <Text
                              style={{
                                textAlign: "center",
                                fontSize: 19,
                                color: "rgba(0,0,0,0.5)",
                                fontWeight: "bold",
                              }}
                            >
                              +
                            </Text>
                          </TouchableOpacity>
                        </View>
                      </View>
                    }
                  </View>
                </View>
              </View>
              {this.props.subItems &&
                this.props.subItems.length > 0 &&
                this.props.subItems.map((item, index) => {
                  if (this.props.altKalem)
                    return (
                      <View
                        style={{
                          flexDirection: "row",
                          justifyContent: "space-between",
                        }}
                      >
                        <View
                          style={{
                            flexDirection: "column",
                            width: 30,
                            alignItems: "center",
                          }}
                        >
                          <View
                            style={{
                              flexDirection: "column",
                              backgroundColor: "#dedede",
                              width: 2,
                              flexGrow: 0.5,
                            }}
                          />
                          <View
                            style={{
                              flexDirection: "row",
                              justifyContent: "flex-end",
                              width: 30,
                            }}
                          >
                            <View
                              style={{
                                flexDirection: "row",
                                backgroundColor: "#dedede",
                                width: 16,
                                height: 2,
                                justifyContent: "flex-end",
                                alignItems: "center",
                              }}
                            />
                          </View>
                          {this.props.subItems.length - 1 > index && (
                            <View
                              style={{
                                flexDirection: "column",
                                backgroundColor: "#dedede",
                                width: 2,
                                flexGrow: 0.5,
                              }}
                            />
                          )}
                        </View>
                        <TouchableOpacity
                          onPress={() =>
                            this.props.navigation.navigate({
                              key: "cart",
                              routeName: "ProductDetailsScreen",
                              params: {
                                UrunKodu: item.AltUrunKodu,
                                SeriNo: item.UrunSerino,
                                isFavorite: false,
                              },
                            })
                          }
                          style={{
                            backgroundColor: "#F0F4F5",
                            flex: 1,
                            marginTop: 10,
                            borderRadius: 3,
                            marginRight: HORIZONTAL_PADDING,
                            padding: 8,
                            paddingVertical: 6,
                            marginBottom: item < this.props.subItems.length - 1 ? 10 : 0,
                          }}
                        >
                          <View style={{ flexDirection: "row" }}>
                            <View style={styles.subPrdImgContainer}>
                              <SmartImage
                                style={styles.subPrdImg}
                                resizeMode="contain"
                                source={{ uri: item.UrunResimBuyukThump }}
                              />
                            </View>
                            <View
                              style={{
                                flexDirection: "row",
                                flex: 1,
                                marginLeft: win.width * 0.02,
                                height: scale(50),
                              }}
                            >
                              <View
                                style={{
                                  flexDirection: "column",
                                  justifyContent: "center",
                                }}
                              >
                                <Text
                                  numberOfLines={1}
                                  ellipsizeMode="tail"
                                  onLongPress={() => {
                                    Clipboard.setString(item.UrunAciklamasi);
                                    this.props.showToast("Ürün açıklaması kopyalandı");
                                  }}
                                  onPress={() => {
                                    this.props.navigation.navigate({
                                      key: "cart",
                                      routeName: "ProductDetailsScreen",
                                      params: {
                                        UrunKodu: item.AltUrunKodu,
                                        SeriNo: item.UrunSerino,
                                        isFavorite: false,
                                      },
                                    });
                                  }}
                                  style={{
                                    fontFamily: "Poppins-Medium",
                                    fontSize: 12,
                                    textAlign: "left",
                                    paddingRight: 5,
                                  }}
                                >
                                  {item.AltUrunKodu + " " + item.UrunAciklamasi}
                                </Text>
                                <Text
                                  style={{
                                    fontFamily: "Poppins-Light",
                                    fontSize: 10,
                                  }}
                                >
                                  {item.AltUrunMiktari + "  Adet"}
                                </Text>
                              </View>
                            </View>
                          </View>
                        </TouchableOpacity>
                      </View>
                    );
                  else {
                    // cross sell
                    return (
                      <View
                        style={{
                          flexDirection: "row",
                          justifyContent: "space-between",
                        }}
                      >
                        <View
                          style={{
                            flexDirection: "column",
                            width: 30,
                            alignItems: "center",
                          }}
                        >
                          <View
                            style={{
                              flexDirection: "column",
                              backgroundColor: "#c4c4c4",
                              width: 2,
                              flexGrow: 0.5,
                            }}
                          />
                          <View
                            style={{
                              flexDirection: "row",
                              justifyContent: "flex-end",
                              width: 30,
                            }}
                          >
                            <View
                              style={{
                                flexDirection: "row",
                                backgroundColor: "#c4c4c4",
                                width: 16,
                                height: 2,
                                borderBottomLeftRadius: 2,
                                justifyContent: "flex-end",
                                alignItems: "center",
                              }}
                            />
                          </View>
                          {this.props.subItems.length - 1 > index && (
                            <View
                              style={{
                                flexDirection: "column",
                                backgroundColor: "#c4c4c4",
                                width: 2,
                                flexGrow: 0.5,
                              }}
                            />
                          )}
                        </View>

                        <TouchableOpacity
                          onPress={() =>
                            this.props.navigation.navigate({
                              key: "cart",
                              routeName: "ProductDetailsScreen",
                              params: {
                                UrunKodu: item.UrunKodu,
                                SeriNo: item.UrunSerino,
                                isFavorite: false,
                              },
                            })
                          }
                          style={{
                            backgroundColor: "#F0F4F5",
                            flex: 1,
                            marginTop: 10,
                            borderRadius: 3,
                            padding: 8,
                            paddingVertical: 6,
                            marginBottom: 10,
                          }}
                        >
                          <View style={{ flexDirection: "row" }}>
                            <View style={{ flexDirection: "column" }}>
                              <View style={styles.subPrdImgContainer}>
                                <SmartImage
                                  style={styles.subPrdImg}
                                  resizeMode="contain"
                                  source={{
                                    uri: item.UrunResimleri.DefaultResim.Connact.URL,
                                  }}
                                />
                              </View>
                              <Text
                                style={{
                                  fontFamily: "Poppins",
                                  marginTop: 2,
                                  fontSize: 10,
                                  color: "gray",
                                  alignSelf: "center",
                                }}
                              >
                                {item.UrunMiktar + " adet"}
                              </Text>
                            </View>
                            <View
                              style={{
                                flexDirection: "row",
                                flex: 1,
                                maxWidth: win.width * 0.6,
                                paddingLeft: scale(8),
                              }}
                            >
                              <View style={{ flexDirection: "column" }}>
                                <Text
                                  onPress={() => {
                                    Clipboard.setString(item.UrunKodu);
                                    this.props.showToast("Ürün kodu kopyalandı");
                                  }}
                                  numberOfLines={1}
                                  ellipsizeMode="tail"
                                >
                                  <Text
                                    style={{
                                      fontFamily: "Poppins-Medium",
                                      fontSize: 11,
                                      textAlign: "left",
                                      paddingRight: 5,
                                      color: "gray",
                                    }}
                                  >
                                    {item.UrunKodu + " "}
                                  </Text>
                                  <Text
                                    numberOfLines={1}
                                    ellipsizeMode="tail"
                                    style={{
                                      fontFamily: "Poppins-Medium",
                                      fontSize: 11,
                                      textAlign: "left",
                                      paddingRight: 5,
                                    }}
                                  >
                                    {item.UrunAciklamasi}
                                  </Text>
                                </Text>
                                <View style={{ flexDirection: "row" }}>
                                  <View style={{ flexDirection: "column" }}>
                                    <Text
                                      style={{
                                        fontFamily: "Poppins",
                                        fontSize: 11,
                                        textAlign: "left",
                                        color: "#909090",
                                      }}
                                    >
                                      Birim Fiyatı
                                    </Text>
                                    <Text
                                      style={{
                                        fontFamily: "Poppins",
                                        fontSize: 11,
                                        textAlign: "left",
                                        color: "#909090",
                                      }}
                                    >
                                      Toplam Fiyat
                                    </Text>
                                  </View>
                                  <View style={{ flexDirection: "column" }}>
                                    <Text
                                      style={{
                                        fontFamily: "Poppins",
                                        fontSize: 11,
                                        textAlign: "left",
                                        color: "#343434",
                                      }}
                                    >
                                      {" :  " +
                                        Number(item.UrunBirimFiyati)
                                          .toFixed(2)
                                          .toString()
                                          .replace(".", ",") +
                                        " " +
                                        item.UrunBirimFiyatPb}
                                    </Text>
                                    <Text
                                      style={{
                                        fontFamily: "Poppins",
                                        fontSize: 11,
                                        textAlign: "left",
                                        color: "#343434",
                                      }}
                                    >
                                      {" :  " +
                                        Number(item.UrunToplamFiyat)
                                          .toFixed(2)
                                          .toString()
                                          .replace(".", ",") +
                                        " " +
                                        item.UrunBirimFiyatPb}
                                    </Text>
                                  </View>
                                </View>
                              </View>
                            </View>
                          </View>
                          {this.renderPriceReqTag()}
                        </TouchableOpacity>
                      </View>
                    );
                  }
                })}
            </View>
          </TouchableHighlight>
        </Interactable.View>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    cart: state.cart,
    session: state.session,
    favorites: state.favorites,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    setCart: cart => dispatch(setCart(cart)),
    setNumber: (skk, quantity) => dispatch(setNumber(skk, quantity)),
    removeFromCart: item => dispatch(removeFromCart(item)),
    showToast: (msg, dur) => dispatch(showToast(msg, dur)),
    removeFavorite: p => dispatch(removeFavorite(p)),
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(CartItemBundle);

const styles = ScaledSheet.create({
  pqTag: {
    paddingVertical: moderateScale(2, 0.5),
    paddingHorizontal: moderateScale(5, 0.5),
    borderRadius: moderateScale(3, 0.4),
    alignSelf: "baseline",
    justifyContent: "center",
    alignItems: "center",
    marginTop: moderateScale(5, 0.5),
  },
  prdImg: {
    width: "60@s",
    height: "60@s",
  },
  prdImgContainer: {
    borderColor: "#dddddd",
    borderWidth: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    width: "75@s",
  },
  subPrdImg: {
    width: "40@s",
    height: "40@s",
  },
  subPrdImgContainer: {
    borderColor: "#dddddd",
    borderWidth: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    height: "45@s",
    width: "45@s",
    backgroundColor: "white",
  },
});
