import React, { Component } from "react";
import {
  TextInput,
  View,
  TouchableOpacity,
  StyleSheet,
  KeyboardAvoidingView,
  Dimensions,
  Modal,
  TouchableHighlight,
} from "react-native";
import { Text } from "@components";

const win = Dimensions.get("window");

export default class NewCartDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isVisible: false,
      input: "",
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.isVisible !== prevProps.isVisible)
      this.setState({
        isVisible: this.props.isVisible,
      });
  }

  toggleVisibility = () => {
    this.setState({
      isVisible: !this.state.isVisible,
    });
  };
  render() {
    if (this.state.isVisible) {
      return (
        <Modal
          animationType="none"
          transparent={true}
          presentationStyle="overFullScreen"
          visible={this.state.isVisible}
          onRequestClose={() => {
            this.setState({
              isVisible: false,
            });
          }}
        >
          <TouchableHighlight
            style={{
              width: win.width,
              height: win.height,
              backgroundColor: "rgba(0,0,0,0.2)",
              justifyContent: "center",
              alignItems: "center",
            }}
            onPress={() => this.setState({ isVisible: false })}
          >
            <KeyboardAvoidingView behavior="position" enabled>
              <View style={styles.container}>
                <Text style={{ fontSize: 16, color: "gray", padding: 10 }}>Yeni Sepet Oluştur</Text>
                <View
                  style={{
                    width: win.width * 0.9,
                    backgroundColor: "#dedede",
                    height: 1,
                  }}
                />
                <TextInput
                  autoCapitalize={"none"}
                  value={this.state.input}
                  autoFocus
                  selectTextOnFocus={true}
                  placeholder="Yeni sepetinizin ismini yazın"
                  onChangeText={input => this.setState({ input })}
                  style={{
                    borderBottomWidth: 1,
                    borderBottomColor: "#cecece",
                    fontSize: 16,
                    marginTop: 10,
                    marginBottom: 10,
                    width: win.width * 0.9 - 40,
                    paddingBottom: 5,
                  }}
                />
                <View
                  style={{
                    flexDirection: "row",
                    width: win.width * 0.9,
                    justifyContent: "center",
                    alignItems: "center",
                    marginTop: 10,
                    borderTopColor: "#dedede",
                    borderTopWidth: 1,
                  }}
                >
                  <TouchableOpacity
                    style={{
                      alignItems: "center",
                      flex: 1,
                      borderRightColor: "#dedede",
                      borderRightWidth: 1,
                    }}
                    onPress={() => this.setState({ isVisible: false })}
                  >
                    <Text style={{ fontSize: 18, padding: 8 }}>İptal</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={{ flex: 1, alignItems: "center", padding: 8 }}
                    onPress={() => {
                      this.props.addNewBucket(this.state.input);
                      this.toggleVisibility();
                    }}
                  >
                    <Text style={{ fontSize: 18 }}>Tamam</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </KeyboardAvoidingView>
          </TouchableHighlight>
        </Modal>
      );
    } else return null;
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "column",
    backgroundColor: "white",
    width: win.width * 0.9,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.5,
    shadowRadius: 2,
    elevation: 1,
    alignItems: "center",
    marginBottom: 50,
    borderRadius: 10,
  },
});
