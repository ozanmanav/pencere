import React, { Component } from "react";
import {
  TextInput,
  View,
  TouchableOpacity,
  StyleSheet,
  Image,
  Dimensions,
  Keyboard,
  Platform,
} from "react-native";
import { Text, ModalBox } from "@components";

const win = Dimensions.get("window");

export default class QuantityDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      keyboardVisible: false,
      keyboardHeight: 0,
      quantity: "1",
      item: {},
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.isVisible !== prevProps.isVisible)
      this.setState({
        isVisible: this.props.isVisible,
      });
  }

  componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener("keyboardDidShow", this._keyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener("keyboardDidHide", this._keyboardDidHide);
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow = e => {
    this.setState({ keyboardVisible: true, keyboardHeight: e.endCoordinates.height });
  };

  _keyboardDidHide = () => {
    this.setState({ keyboardVisible: false });
  };

  toggleVisibility = q => {
    this.setState({ quantity: String(q) });
    this.modalBox.toggleVisibility();
  };

  onPressOK = () => {
    console.log("press ok", this.state.item, this.props);
    this.toggleVisibility();
    if (this.props.addCrossSellToCart) this.props.addCrossSellToCart(this.state.item);
    else if (this.props.addFavoriteToCart) this.props.addFavoriteToCart(this.state.item);
  };

  increaseQuantity() {
    this.setState({
      quantity: (Number(this.state.quantity) + 1).toString(),
    });
  }

  decreaseQuantity() {
    if (this.state.quantity > 1)
      this.setState({
        quantity: (Number(this.state.quantity) - 1).toString(),
      });
  }

  render() {
    return (
      <ModalBox
        ref={c => (this.modalBox = c)}
        statusBarHidden={false}
        animationType="slide"
        scrollable={false}
        presentationStyle="overFullScreen"
      >
        <TouchableOpacity
          style={[
            styles.container,
            {
              marginBottom: this.state.keyboardVisible
                ? this.state.keyboardHeight - (Platform.OS === "ios" ? win.height / 16 : 0)
                : 0,
            },
          ]}
          onPress={() => Keyboard.dismiss()}
        >
          <View style={{ flexDirection: "row", paddingVertical: 15, alignItems: "center" }}>
            <TouchableOpacity style={{ padding: 4 }} onPress={() => this.decreaseQuantity()}>
              <Image
                source={require("@assets/images/minus.png")}
                style={{ width: 16, height: 16, tintColor: "#9d9d9d" }}
              />
            </TouchableOpacity>
            <TextInput
              style={{
                height: 24,
                borderRadius: 3,
                width: 48,
                borderColor: "gray",
                borderWidth: 1,
                marginHorizontal: 5,
                fontSize: 16,
                padding: 1,
                textAlign: "center",
                color: "gray",
              }}
              autoCorrect={false}
              autoFocus={false}
              onChangeText={text => {
                if (Number(text) > 0) this.setState({ quantity: text });
                else this.setState({ quantity: "1" });
              }}
              value={this.state.quantity}
              keyboardType="numeric"
            />
            <TouchableOpacity style={{ padding: 4 }} onPress={() => this.increaseQuantity()}>
              <Image
                source={require("@assets/images/plus.png")}
                style={{ width: 16, height: 16, tintColor: "#adadad" }}
              />
            </TouchableOpacity>
          </View>
          <View
            style={{
              flexDirection: "row",
              width: win.width * 0.9,
              justifyContent: "center",
              alignItems: "center",
              borderTopColor: "#dedede",
              borderTopWidth: 1,
            }}
          >
            <TouchableOpacity
              onPress={() => this.toggleVisibility()}
              style={{
                flex: 1,
                borderRightWidth: 1,
                borderRightColor: "#dedede",
                justifyContent: "center",
                alignItems: "center",
                padding: 5,
              }}
            >
              <Text style={{ fontSize: 16, color: "gray" }}>İptal</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={this.props.onPressOK}
              style={{ flex: 1, justifyContent: "center", alignItems: "center", padding: 5 }}
            >
              <Text style={{ fontSize: 16, color: "gray" }}>Tamam</Text>
            </TouchableOpacity>
          </View>
        </TouchableOpacity>
      </ModalBox>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "column",
    backgroundColor: "white",
    width: win.width * 0.9,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.5,
    shadowRadius: 2,
    elevation: 1,
    alignItems: "center",
    justifyContent: "center",
    marginBottom: 50,
    borderRadius: 5,
    paddingTop: 10,
  },
});
