import React, { Component } from "react";
import { Text } from "@components";
import { getWidth } from "@common/Utils";
import { View, StyleSheet, Animated, Easing } from "react-native";
import { moderateScale } from "react-native-size-matters";
import colors from "@common/styles/colors";

const SPACE_HORIZONTAL = getWidth() / 36;

class PriceRequestWidget extends Component {
  constructor(props) {
    super(props);
    this.barRatio = new Animated.Value(0);
    this.bgAnim = new Animated.Value(0);
    this.min = this.props.cart.MinFiyatTalepTutari;
    this.total = this.props.cart.FiyatTalepToplam;
    this.pb =
      this.props.cart.FiyatTalepParaBirimi === "TRY" ? "TL" : this.props.cart.FiyatTalepParaBirimi;
  }

  componentDidMount() {
    setTimeout(() => {
      Animated.timing(this.barRatio, {
        toValue: this.total / this.min > 1 ? 1 : this.total / this.min,
        easing: Easing.linear,
        duration: 1500,
      }).start();
    }, 500);
  }

  componentDidUpdate(prevProps, prevState) {
    const prevCart = prevProps.cart;
    const currentCart = this.props.cart;
    if (
      prevCart.MinFiyatTalepTutari !== currentCart.MinFiyatTalepTutari ||
      prevCart.FiyatTalepToplam !== currentCart.FiyatTalepToplam ||
      prevCart.FiyatTalepParaBirimi !== currentCart.FiyatTalepParaBirimi
    ) {
      const oldRatio = this.total / this.min;
      this.min = currentCart.MinFiyatTalepTutari;
      this.total = currentCart.FiyatTalepToplam;
      this.pb =
        currentCart.FiyatTalepParaBirimi === "TRY" ? "TL" : currentCart.FiyatTalepParaBirimi;
      const newRatio = this.total / this.min;
      Animated.timing(this.barRatio, {
        toValue: newRatio > 1 ? 1 : newRatio,
        easing: Easing.linear,
        duration: 1500 * Math.abs((oldRatio > 1 ? 1 : oldRatio) - (newRatio > 1 ? 1 : newRatio)),
      }).start();
    }
  }

  render() {
    const widthInterpolated = this.barRatio.interpolate({
      inputRange: [0, 1],
      outputRange: [0, getWidth() * 0.7],
      extrapolate: "clamp",
    });

    const bgColor = this.barRatio.interpolate({
      inputRange: [0, 0.8, 1],
      outputRange: ["rgb(238,0,1)", "rgb(248,193,0)", "rgb(46,186,14)"],
      extrapolate: "clamp",
    });

    return (
      <View
        style={{
          flexDirection: "row",
          width: getWidth() - SPACE_HORIZONTAL * 2,
          marginHorizontal: SPACE_HORIZONTAL,
          backgroundColor: "white",
          marginTop: SPACE_HORIZONTAL,
          paddingHorizontal: moderateScale(10, 0.5),
          alignItems: "center",
          justifyContent: "center",
          paddingVertical: moderateScale(5, 0.3),
          borderRadius: 4,
        }}
      >
        <View
          style={{
            flexDirection: "column",
            alignItems: "center",
            justifyContent: "space-evenly",
          }}
        >
          <Animated.Text>
            <Animated.Text
              style={{
                fontFamily: "Poppins-Medium",
                fontSize: 12,
                color: bgColor,
              }}
            >
              {"Teklif Tutarınız: "}
            </Animated.Text>
            <Animated.Text
              style={{
                fontFamily: "Poppins-SemiBold",
                fontSize: 14,
                color: bgColor,
              }}
            >
              {`${this.total
                .toFixed(2)
                .replace(",", "")
                .replace(".", ",")} ${this.pb}`}
            </Animated.Text>
          </Animated.Text>
          <View
            style={{
              width: getWidth() * 0.7,
              height: moderateScale(16, 0.3),
              backgroundColor: "#dedede",
              borderColor: "#bebebe",
              borderWidth: StyleSheet.hairlineWidth,
              overflow: "hidden",
            }}
          >
            <Animated.View
              style={{
                height: moderateScale(16, 0.3),
                backgroundColor: bgColor,
                width: widthInterpolated,
                shadowColor: "black",
                shadowOpacity: 0.4,
                shadowRadius: 2,
              }}
            >
              <Animated.Image
                source={require("@assets/images/slash.png")}
                style={{
                  position: "absolute",
                  left: 0,
                  width: widthInterpolated,
                  height: moderateScale(16, 0.3),
                  tintColor: "black",
                  opacity: 0.03,
                  resizeMode: "cover",
                }}
              />
            </Animated.View>
          </View>
          {this.total / this.min >= 1 ? (
            <Text
              style={{
                fontFamily: "Poppins-Medium",
                fontSize: 12,
                color: colors.price_color,
                marginTop: moderateScale(5, 0.3),
              }}
            >
              {"Teklif alabilirsiniz."}
            </Text>
          ) : (
            <Text style={{ marginTop: moderateScale(5, 0.3) }}>
              <Text
                style={{
                  fontFamily: "Poppins-SemiBold",
                  fontSize: 12,
                  color: colors.price_color,
                }}
              >
                {`${(this.min - this.total)
                  .toFixed(2)
                  .replace(",", "")
                  .replace(".", ",")} ${this.pb}`}
              </Text>
              <Text
                style={{
                  fontFamily: "Poppins-Medium",
                  fontSize: 12,
                  color: colors.price_color,
                }}
              >
                {"'lik daha alımınızda teklif alabilirsiniz."}
              </Text>
            </Text>
          )}
        </View>
      </View>
    );
  }
}

export default PriceRequestWidget;
