import React, { Component } from "react";
import {
  View,
  TouchableOpacity,
  Image,
  Clipboard,
  Dimensions,
  Animated,
  LayoutAnimation,
  Alert,
  TouchableHighlight,
  TextInput,
} from "react-native";

import { Text, SmartImage } from "@components";
import { connect } from "react-redux";
import { setCart, setNumber, removeFromCart, showToast, removeFavorite } from "@redux/actions";

import { ScaledSheet } from "react-native-size-matters";
import { scale, moderateScale } from "react-native-size-matters";
import Interactable from "react-native-interactable";
import { UPDATE_SPRINGY } from "@common/LayoutAnimations";
const win = Dimensions.get("window");
const HORIZONTAL_PADDING = win.width / 36;

class CartItem extends Component {
  increaseNumber = (skk, num) => {
    console.log("skk", skk, num);
    this.props.setNumber(skk, num + 1);
  };
  decreaseNumber = (skk, num) => {
    console.log("skk", skk, "num", num);
    this.props.setNumber(skk, num - 1);
  };
  constructor(props) {
    super(props);
    this.state = {
      isFavorite: false,
      position: 0,
      number: 0,
      image: "",
      imageError: false,
    };
    this._deltaX = new Animated.Value(0);
  }

  componentDidMount() {
    this.setState({
      number: this.props.item.UrunMiktar,
      image: { uri: this.props.item.UrunResimleri.DefaultResim.Connact.URL },
    });
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.item.UrunMiktar !== this.props.item.UrunMiktar)
      this.setState({ number: this.props.item.UrunMiktar });
    if (prevProps.favorites !== this.props.favorites) {
      var isFavorite = false;
      this.props.favorites.items.forEach(item => {
        if (item.urunKodu === this.props.item.UrunKodu) {
          isFavorite = true;
        }
      });
      this.setState({ isFavorite });
    }
  }

  snapToDefault = () => {
    this.cartView.snapTo({ index: 0 });
  };

  renderPriceReqTag = () => {
    // if(this.props.item.KalemFiyatTalepDurumu === 'Y')
    // return (
    //     <View style={[styles.pqTag, {
    //         backgroundColor: 'orange'
    //     }]}>
    //         <Text fontScale={0.5} style={{ fontFamily: 'Poppins-Medium', color: 'white', fontSize: 13 }}>Teklif İstenebilir</Text>
    //     </View>
    // )
    if (this.props.item.KalemFiyatTalepDurumu === "X")
      return (
        <View
          style={[
            styles.pqTag,
            {
              backgroundColor: "#dedede",
            },
          ]}
        >
          <Text
            fontScale={0.5}
            style={{
              fontFamily: "Poppins-Medium",
              color: "white",
              fontSize: 13,
            }}
          >
            Teklife Uygun Değil
          </Text>
        </View>
      );
    // else if(this.props.item.KalemFiyatTalepDurumu === 'N')
    // return (
    //     <View style={[styles.pqTag, {
    //         backgroundColor: '#dedede'
    //     }]}>
    //         <Text fontScale={0.5} style={{ fontFamily: 'Poppins-Medium', color: 'white', fontSize: 13 }}>Teklif İçin Yetersiz Adet</Text>
    //     </View>
    // )
  };

  renderTag = () => {
    if (this.props.item.UrunFirsatUrunuMu) {
      return (
        <Text>
          <Image
            source={require("@assets/images/firsat.png")}
            style={{
              width: scale((14 * 144) / 41),
              height: scale(14),
              resizeMode: "contain",
            }}
          />
          <Text> </Text>
        </Text>
      );
    } else if (this.props.item.UrunTransit) {
      return (
        <Text>
          <Image
            source={require("@assets/images/transit.png")}
            style={{
              width: scale((14 * 273) / 41),
              height: scale(14),
              resizeMode: "contain",
            }}
          />
          <Text> </Text>
        </Text>
      );
    } else if (this.props.item.UrunCrossSell || this.props.item.UrunTipi === "CrossSell") {
      return (
        <Text>
          <Image
            source={require("@assets/images/crossSell.png")}
            style={{
              width: scale((14 * 273) / 41),
              height: scale(14),
              resizeMode: "contain",
            }}
          />
          <Text> </Text>
        </Text>
      );
    } else if (this.props.item.UrunSeriNolu) {
      return (
        <Text>
          <Image
            source={require("@assets/images/defolu.png")}
            style={{
              width: scale((14 * 213) / 41),
              height: scale(14),
              resizeMode: "contain",
            }}
          />
          <Text> </Text>
        </Text>
      );
    } else return null;
  };

  onSnap({ nativeEvent }) {
    const { index } = nativeEvent;
    this.setState({ position: index });
  }

  getBackgroundColor() {
    if (this.props.item.UrunCrossSell) return "#efefef";
    else if (this.props.item.IsFiyatTalebi) return "#ffefde";
    else return "white";
  }

  render() {
    return (
      <View
        style={{
          flexDirection: "row",
          justifyContent: "flex-end",
          marginHorizontal: HORIZONTAL_PADDING,
          backgroundColor: "transparent",
        }}
      >
        {!this.props.item.IsFiyatTalebi && (
          <View
            style={{
              position: "absolute",
              right: moderateScale(80, 0.5),
              backgroundColor: this.state.isFavorite ? "#c4c4c4" : "#4CAF50",
              alignItems: "center",
              justifyContent: "center",
              height: "100%",
              width: moderateScale(80, 0.5),
            }}
          >
            <TouchableOpacity
              style={{
                flexDirection: "column",
                alignItems: "center",
                justifyContent: "center",
              }}
              onPress={() => {
                if (!this.state.isFavorite) {
                  this.props.setSelectedItem(this.props.item);
                  this.props.addFavoriteDialog.toggleVisibility();
                } else {
                  Alert.alert(
                    "Favorilerimden Çıkar",
                    "Bu ürünü favorilerinizden çıkarmak istediğinize emin misiniz?",
                    [
                      {
                        text: "Hayır",
                      },
                      {
                        text: "Evet",
                        onPress: () => {
                          this.props.removeFavorite(this.props.item);
                        },
                      },
                    ],
                    { cancelable: false },
                  );
                }
              }}
            >
              <Animated.View
                style={{
                  alignItems: "center",
                  justifyContent: "center",
                  paddingHorizontal: moderateScale(10, 0.5),
                  transform: [
                    {
                      scaleX: this._deltaX.interpolate({
                        inputRange: [-moderateScale(160, 0.5), -moderateScale(80, 0.5)],
                        outputRange: [1, 0],
                        extrapolateLeft: "clamp",
                        extrapolateRight: "clamp",
                      }),
                    },
                    {
                      scaleY: this._deltaX.interpolate({
                        inputRange: [-moderateScale(160, 0.5), -moderateScale(80, 0.5)],
                        outputRange: [1, 0],
                        extrapolateLeft: "clamp",
                        extrapolateRight: "clamp",
                      }),
                    },
                  ],
                }}
              >
                <Image
                  source={require("@assets/images/star.png")}
                  style={{
                    marginBottom: moderateScale(5, 0.5),
                    width: moderateScale(24, 0.7),
                    height: moderateScale(24, 0.7),
                    tintColor: "white",
                  }}
                />
                <Text
                  style={{
                    fontFamily: "Poppins",
                    color: "white",
                    fontSize: 10,
                    textAlign: "center",
                  }}
                >
                  {this.state.isFavorite ? "Favorilerden Çıkar" : "Favorilere Ekle"}
                </Text>
              </Animated.View>
            </TouchableOpacity>
          </View>
        )}
        <TouchableOpacity
          onPress={() => {
            Alert.alert(
              "Ürünü Sil",
              "Bu ürünü sepetinizden çıkarmak istediğinize emin misiniz?",
              [
                {
                  text: "Hayır",
                },
                {
                  text: "Evet",
                  onPress: () => {
                    this.props.removeFromCart(this.props.item).then(() => {
                      LayoutAnimation.configureNext(UPDATE_SPRINGY);
                    });
                  },
                },
              ],
              { cancelable: false },
            );
          }}
          style={{
            position: "absolute",
            right: 1,
            backgroundColor: "#e53935",
            flexDirection: "row",
            height: "100%",
            width: moderateScale(80, 0.5),
            borderColor: "white",
            borderBottomRightRadius: this.props.index === this.props.length - 1 ? 5 : 0,
            paddingHorizontal: moderateScale(10, 0.5),
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <Animated.View
            style={{
              flexDirection: "column",
              alignItems: "center",
              justifyContent: "center",
              transform: [
                {
                  scaleX: this._deltaX.interpolate({
                    inputRange: [-moderateScale(80, 0.5), -moderateScale(0, 0.5)],
                    outputRange: [1, 0],
                    extrapolateLeft: "clamp",
                    extrapolateRight: "clamp",
                  }),
                },
                {
                  scaleY: this._deltaX.interpolate({
                    inputRange: [-moderateScale(80, 0.5), -moderateScale(0, 0.5)],
                    outputRange: [1, 0],
                    extrapolateLeft: "clamp",
                    extrapolateRight: "clamp",
                  }),
                },
              ],
            }}
          >
            <Image
              source={require("@assets/images/trash.png")}
              style={{
                marginBottom: moderateScale(5, 0.5),
                width: moderateScale(24, 0.7),
                height: moderateScale(24, 0.7),
                tintColor: "white",
              }}
            />
            <Text
              style={{
                fontFamily: "Poppins",
                color: "white",
                fontSize: 10,
                textAlign: "center",
              }}
            >
              Sepetten Çıkar
            </Text>
          </Animated.View>
        </TouchableOpacity>
        <Interactable.View
          animatedNativeDriver={true}
          ref={c => (this.cartView = c)}
          style={{
            flex: 1,
          }}
          animatedValueX={this._deltaX}
          horizontalOnly={true}
          boundaries={{
            left: -win.width + moderateScale(160, 0.5),
            right: 0,
            bounce: 0,
          }}
          snapPoints={
            this.props.item.IsFiyatTalebi
              ? [
                  { x: 0, damping: 0.5, tension: 600, haptics: true },
                  { x: -moderateScale(80, 0.5) },
                ]
              : [
                  { x: 0, damping: 0.5, tension: 600, haptics: true },
                  { x: -moderateScale(80, 0.5) },
                  { x: -moderateScale(160, 0.5) },
                ]
          }
          onSnap={this.onSnap.bind(this)}
          // }}
        >
          <TouchableHighlight
            activeOpacity={0.1}
            underlayColor="white"
            onPress={() => {
              if (this.state.position !== 0) {
                this.snapToDefault();
              } else {
                if (this.props.item.IsFiyatTalebi) {
                  this.props.navigation.navigate({ key: "cart", routeName: "PriceRequestsScreen" });
                } else {
                  this.props.navigation.navigate({
                    key: "cart",
                    routeName: "ProductDetailsScreen",
                    params: {
                      UrunKodu: this.props.item.UrunKodu,
                      SeriNo: this.props.item.UrunSerino,
                      isFavorite: false,
                      pb: this.props.item.paraBirimi,
                    },
                  });
                }
              }
            }}
            style={{
              backgroundColor: this.getBackgroundColor(),
              paddingHorizontal: HORIZONTAL_PADDING,
              paddingVertical: HORIZONTAL_PADDING * 2,
              borderColor: "#ededed",
              borderBottomWidth: this.props.index < this.props.length - 1 ? 1 : 0,
              borderBottomLeftRadius: this.props.index === this.props.length - 1 ? 4 : 0,
              borderBottomRightRadius: this.props.index === this.props.length - 1 ? 4 : 0,
            }}
          >
            <View style={{ flexDirection: "row", flex: 1 }}>
              {!this.props.item.IsFiyatTalebi && (
                <View style={styles.prdImgContainer}>
                  <SmartImage
                    style={styles.prdImg}
                    resizeMode="contain"
                    source={{
                      uri: this.props.item.UrunResimleri.DefaultResim.Connact.URL,
                    }}
                  />
                </View>
              )}
              <View style={{ flexDirection: "row", flex: 1, minHeight: scale(70) }}>
                <View
                  style={{
                    flexDirection: "column",
                    flex: 1,
                    paddingLeft: win.width * 0.03,
                    justifyContent: "space-between",
                  }}
                >
                  <View style={{ paddingTop: 0 }}>
                    <View
                      style={{
                        flexDirection: "row",
                        flex: 1,
                        alignItems: "flex-start",
                        justifyContent: "space-between",
                      }}
                    >
                      <Text
                        numberOfLines={3}
                        ellipsizeMode="tail"
                        style={{
                          flexDirection: "row",
                          flexWrap: "wrap",
                          flex: 1,
                          marginTop: 1,
                        }}
                      >
                        {this.renderTag()}
                        <Text
                          style={{
                            alignSelf: "flex-start",
                            flexWrap: "wrap",
                            flex: 1,
                            paddingRight: 5,
                          }}
                        >
                          <Text
                            style={{
                              fontFamily: "Poppins",
                              color: this.props.item.UrunBelirtilenAdetteTeminEdilememekte
                                ? "gray"
                                : "#03A9F4",
                              fontSize: 11,
                            }}
                          >
                            {this.props.item.UrunMarka + " "}
                          </Text>
                          <Text
                            onPress={() => {
                              Clipboard.setString(this.props.item.UrunKodu);
                              this.props.showToast("Ürün kodu kopyalandı");
                            }}
                            style={{
                              fontFamily: "Poppins",
                              color: "gray",
                              fontSize: 11,
                            }}
                          >
                            {this.props.item.UrunKodu + " "}
                          </Text>
                          <Text
                            onLongPress={() => {
                              Clipboard.setString(this.props.item.UrunAciklamasi);
                              this.props.showToast("Ürün açıklaması kopyalandı");
                            }}
                            onPress={() =>
                              this.props.navigation.navigate({
                                key: "cart",
                                routeName: "ProductDetailsScreen",
                                params: {
                                  UrunKodu: this.props.item.UrunKodu,
                                  SeriNo: this.props.item.UrunSerino,
                                  isFavorite: false,
                                  pb: this.props.item.paraBirimi,
                                },
                              })
                            }
                            style={{
                              fontFamily: "Poppins",
                              fontSize: 11,
                              textAlign: "left",
                              color: this.props.item.UrunBelirtilenAdetteTeminEdilememekte
                                ? "gray"
                                : "black",
                              flexWrap: "wrap",
                            }}
                          >
                            {this.props.item.UrunAciklamasi}
                          </Text>
                        </Text>
                      </Text>
                      <TouchableOpacity
                        onPress={() => {
                          Alert.alert(
                            "Ürünü Sil",
                            "Bu ürünü sepetinizden çıkarmak istediğinize emin misiniz?",
                            [
                              {
                                text: "Hayır",
                                style: "cancel",
                              },
                              {
                                text: "Evet",
                                onPress: () => {
                                  this.props.removeFromCart(this.props.item).then(() => {
                                    LayoutAnimation.configureNext(UPDATE_SPRINGY);
                                  });
                                },
                              },
                            ],
                            { cancelable: false },
                          );
                        }}
                        style={{
                          paddingTop: moderateScale(10, 0.5),
                          paddingBottom: moderateScale(10, 0.5),
                          paddingLeft: moderateScale(10, 0.5),
                          alignItems: "flex-end",
                          justifyContent: "flex-start",
                        }}
                      >
                        <Image
                          source={require("@assets/images/delete-button.png")}
                          style={{
                            width: moderateScale(17, 0.7),
                            height: moderateScale(17, 0.7),
                            tintColor: "gray",
                            top: -moderateScale(10, 0.5),
                          }}
                        />
                      </TouchableOpacity>
                    </View>
                  </View>
                  {this.props.item.UrunBelirtilenAdetteTeminEdilememekte && (
                    <View
                      style={{
                        flexDirection: "row",
                        height: moderateScale(28, 0.6),
                        backgroundColor: "#cbcbcb",
                        alignItems: "center",
                        justifyContent: "center",
                        borderRadius: 3,
                      }}
                    >
                      <Text
                        style={{
                          fontFamily: "Poppins-Medium",
                          fontSize: 12,
                          lineHeight: 14,
                          marginTop: 5,
                          textAlign: "center",
                          color: "white",
                        }}
                      >
                        Ürün temin edilememektedir
                      </Text>
                    </View>
                  )}
                  {
                    <View
                      style={{
                        flexDirection: "row",
                        justifyContent: "space-between",
                        alignItems: "flex-start",
                        marginTop: moderateScale(5, 0.5),
                      }}
                    >
                      {this.props.item.UrunFirsatUrunuMu ? (
                        <View
                          style={{
                            flexDirection: "row",
                            flex: 1,
                            justifyContent: "space-between",
                            maxWidth: win.width * 0.4,
                          }}
                        >
                          <View
                            style={{
                              flexDirection: "column",
                              alignItems: "flex-start",
                            }}
                          >
                            <Text
                              style={{
                                fontFamily: "Poppins",
                                fontSize: 11,
                                lineHeight: 13,
                                textAlign: "left",
                                color: "#909090",
                              }}
                            >
                              Birim Fiyatı
                            </Text>
                            <Text
                              style={{
                                fontFamily: "Poppins",
                                fontSize: 11,
                                lineHeight: 13,
                                textAlign: "left",
                                color: "#909090",
                              }}
                            >
                              {" "}
                            </Text>
                            <Text
                              style={{
                                fontFamily: "Poppins",
                                fontSize: 11,
                                lineHeight: 13,
                                textAlign: "left",
                                color: "#909090",
                                marginTop: 2,
                              }}
                            >
                              Toplam Fiyat
                            </Text>
                          </View>
                          <View
                            style={{
                              flexDirection: "column",
                              alignItems: "flex-end",
                            }}
                          >
                            <Text
                              style={{
                                fontFamily: "Poppins",
                                fontSize: 11,
                                lineHeight: 13,
                                textAlign: "left",
                                color: "gray",
                                textDecorationLine: "line-through",
                              }}
                            >
                              {Number(this.props.item.EskiUrunBirimFiyati)
                                .toFixed(2)
                                .toString()
                                .replace(".", ",") +
                                " " +
                                this.props.item.UrunBirimFiyatPb}
                            </Text>
                            <Text
                              style={{
                                fontFamily: "Poppins",
                                fontSize: 11,
                                lineHeight: 13,
                                textAlign: "left",
                                color: "black",
                              }}
                            >
                              {Number(this.props.item.UrunBirimFiyati)
                                .toFixed(2)
                                .toString()
                                .replace(".", ",") +
                                " " +
                                this.props.item.UrunBirimFiyatPb}
                            </Text>
                            <Text
                              style={{
                                fontFamily: "Poppins",
                                fontSize: 11,
                                lineHeight: 13,
                                textAlign: "left",
                                color: "gray",
                                textDecorationLine: "line-through",
                                marginTop: 2,
                              }}
                            >
                              {Number(this.props.item.EskiUrunToplamFiyat)
                                .toFixed(2)
                                .toString()
                                .replace(".", ",") +
                                " " +
                                this.props.item.UrunBirimFiyatPb}
                            </Text>
                            <Text
                              style={{
                                fontFamily: "Poppins",
                                fontSize: 11,
                                lineHeight: 13,
                                textAlign: "left",
                                color: "black",
                              }}
                            >
                              {Number(this.props.item.UrunToplamFiyat)
                                .toFixed(2)
                                .toString()
                                .replace(".", ",") +
                                " " +
                                this.props.item.UrunBirimFiyatPb}
                            </Text>
                          </View>
                        </View>
                      ) : (
                        <View
                          style={{
                            flexDirection: "row",
                            flex: 1,
                            justifyContent: "space-between",
                            maxWidth: win.width * 0.4,
                          }}
                        >
                          <View style={{ flexDirection: "column" }}>
                            <Text
                              style={{
                                fontFamily: "Poppins",
                                fontSize: 11,
                                lineHeight: 13,
                                textAlign: "left",
                                color: "#909090",
                              }}
                            >
                              Birim Fiyatı
                            </Text>
                            <Text
                              style={{
                                fontFamily: "Poppins",
                                fontSize: 11,
                                lineHeight: 13,
                                textAlign: "left",
                                color: "#909090",
                                marginTop: 2,
                              }}
                            >
                              Toplam Fiyat
                            </Text>
                          </View>
                          <View style={{ flexDirection: "column" }}>
                            <Text
                              style={{
                                fontFamily: "Poppins",
                                fontSize: 11,
                                lineHeight: 13,
                                textAlign: "left",
                                color: "black",
                              }}
                            >
                              {Number(this.props.item.UrunBirimFiyati)
                                .toFixed(2)
                                .toString()
                                .replace(".", ",") +
                                " " +
                                this.props.item.UrunBirimFiyatPb}
                            </Text>
                            <Text
                              style={{
                                fontFamily: "Poppins",
                                fontSize: 11,
                                lineHeight: 13,
                                textAlign: "left",
                                color: "black",
                                marginTop: 2,
                              }}
                            >
                              {Number(this.props.item.UrunToplamFiyat)
                                .toFixed(2)
                                .toString()
                                .replace(".", ",") +
                                " " +
                                this.props.item.UrunBirimFiyatPb}
                            </Text>
                          </View>
                        </View>
                      )}
                      {!this.props.item.IsFiyatTalebi && (
                        <View
                          style={{
                            flexDirection: "row",
                            backgroundColor: "#e2e2e2",
                            paddingVertical: moderateScale(1, 0.4),
                          }}
                        >
                          <TouchableOpacity
                            onPress={() =>
                              this.decreaseNumber(
                                this.props.item.SepetKalemKodu,
                                this.props.item.UrunMiktar,
                              )
                            }
                            style={{
                              width: moderateScale(24, 0.5),
                              borderTopLeftRadius: 0,
                              borderBottomLeftRadius: 0,
                              borderColor: "#e2e2e2",
                              borderRadius: 3,
                              borderWidth: 1,
                              height: moderateScale(28, 0.5),
                              alignItems: "center",
                              justifyContent: "center",
                            }}
                          >
                            <Text
                              style={{
                                textAlign: "center",
                                fontSize: 19,
                                color: "rgba(0,0,0,0.5)",
                                fontWeight: "bold",
                              }}
                            >
                              -
                            </Text>
                          </TouchableOpacity>
                          <TextInput
                            maxLength={3}
                            onFocus={() => {
                              this.qInput.measure((fx, fy, width, height, px, py) => {
                                this.props.scrollToFocusedInput(py);
                              });
                            }}
                            ref={c => (this.qInput = c)}
                            selectTextOnFocus={true}
                            returnKeyType="done"
                            keyboardType="numeric"
                            style={{
                              color: Number(this.state.number) > 0 ? "black" : "red",
                              padding: moderateScale(2, 0.4),
                              fontFamily: "Poppins",
                              fontSize: 15,
                              width: moderateScale(30, 0.5),
                              height: moderateScale(28, 0.5),
                              borderTopColor: "#e2e2e2",
                              borderTopWidth: 1,
                              borderBottomColor: "#e2e2e2",
                              borderBottomWidth: 1,
                              backgroundColor: "white",
                              textAlign: "center",
                            }}
                            onChangeText={text => {
                              this.setState({ number: text });
                            }}
                            onEndEditing={() => {
                              if (Number(this.state.number) > 0) {
                                if (Number(this.state.number) !== this.props.item.UrunMiktar)
                                  this.props.setNumber(
                                    this.props.item.SepetKalemKodu,
                                    Number(this.state.number),
                                  );
                              } else {
                                Alert.alert("Hata", "Adet sayısı 0'dan büyük olmalı");
                                this.setState({
                                  number: this.props.item.UrunMiktar,
                                });
                              }
                            }}
                            value={String(this.state.number)}
                          />

                          <TouchableOpacity
                            onPress={() =>
                              this.increaseNumber(
                                this.props.item.SepetKalemKodu,
                                this.props.item.UrunMiktar,
                              )
                            }
                            style={{
                              width: moderateScale(24, 0.5),
                              borderTopLeftRadius: 0,
                              borderBottomLeftRadius: 0,
                              borderColor: "#e2e2e2",
                              borderRadius: 3,
                              borderWidth: 1,
                              height: moderateScale(28, 0.5),
                              alignItems: "center",
                              justifyContent: "center",
                            }}
                          >
                            <Text
                              style={{
                                textAlign: "center",
                                fontSize: 19,
                                color: "rgba(0,0,0,0.5)",
                                fontWeight: "bold",
                              }}
                            >
                              +
                            </Text>
                          </TouchableOpacity>
                        </View>
                      )}
                    </View>
                  }
                  {!this.props.item.IsFiyatTalebi && this.renderPriceReqTag()}
                </View>
              </View>
            </View>
          </TouchableHighlight>
        </Interactable.View>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    cart: state.cart,
    session: state.session,
    favorites: state.favorites,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    setCart: cart => dispatch(setCart(cart)),
    setNumber: (skk, quantity) => dispatch(setNumber(skk, quantity)),
    removeFromCart: item => dispatch(removeFromCart(item)),
    showToast: (msg, dur) => dispatch(showToast(msg, dur)),
    removeFavorite: p => dispatch(removeFavorite(p)),
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(CartItem);

const styles = ScaledSheet.create({
  prdImg: {
    width: "60@s",
    height: "60@s",
  },
  prdImgContainer: {
    borderColor: "#dddddd",
    flexDirection: "column",
    borderWidth: 1,
    height: "75@s",
    width: "75@s",
    alignItems: "center",
    justifyContent: "center",
  },
  pqTag: {
    paddingVertical: moderateScale(2, 0.5),
    paddingHorizontal: moderateScale(5, 0.5),
    borderRadius: moderateScale(3, 0.4),
    alignSelf: "baseline",
    justifyContent: "center",
    alignItems: "center",
    marginTop: moderateScale(5, 0.5),
  },
});
