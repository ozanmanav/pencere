import React, { Component } from "react";
import {
  Platform,
  Keyboard,
  Image,
  View,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
} from "react-native";
import { Text, ModalBox } from "@components";
import { ScrollView } from "react-native-gesture-handler";
const win = Dimensions.get("window");
import colors from "@common/styles/colors";

export default class FiyatTeklifiMaxAdetModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isVisible: false,
      keyboardVisible: false,
      keyboardHeight: 0,
      UygunsuzUrunler: null,
    };
  }

  componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener("keyboardDidShow", this._keyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener("keyboardDidHide", this._keyboardDidHide);
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow = e => {
    this.setState({
      keyboardVisible: true,
      keyboardHeight: e.endCoordinates.height,
    });
  };

  _keyboardDidHide = () => {
    this.setState({ keyboardVisible: false });
  };

  componentDidUpdate(prevProps, prevState) {
    if (this.props.isVisible !== prevProps.isVisible)
      this.setState({
        isVisible: this.props.isVisible,
      });
  }

  toggleVisibility = UygunsuzUrunler => {
    this.setState({
      UygunsuzUrunler,
    });
    this.modalBox.toggleVisibility();
  };

  render() {
    return (
      <ModalBox
        ref={c => (this.modalBox = c)}
        statusBarHidden={false}
        animationType="slide"
        presentationStyle="overFullScreen"
        scrollable={true}
      >
        <View
          style={[
            styles.container,
            {
              marginBottom: this.state.keyboardVisible
                ? this.state.keyboardHeight - (Platform.OS === "ios" ? win.height / 16 : 0)
                : 0,
            },
          ]}
          onPress={() => Keyboard.dismiss()}
        >
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "space-between",
              width: win.width * 0.9,
              paddingHorizontal: 16,
              paddingVertical: 12,
            }}
          >
            <Text
              style={{
                fontSize: 11,
                letterSpacing: 1.5,
                color: "black",
                fontFamily: "Poppins-SemiBold",
              }}
            >
              HEMEN TEKLİF AL - ÜRÜN ADETLERİ
            </Text>
            <TouchableOpacity onPress={() => this.toggleVisibility()}>
              <Image
                source={require("@assets/images/close.png")}
                style={{
                  width: 24,
                  height: 24,
                  tintColor: "#afafaf",
                  resizeMode: "contain",
                }}
              />
            </TouchableOpacity>
          </View>
          <View
            style={{
              width: win.width * 0.9,
              backgroundColor: "#dedede",
              height: 1,
            }}
          />
          <View style={{ width: win.width * 0.9, paddingHorizontal: 16 }}>
            <Text style={{ fontFamily: "Poppins", marginTop: 10, fontSize: 13 }}>
              Teklif almak istediğiniz ürün veya ürünler için izin verilen adetlerin üzerinde giriş
              yaptınız. Aşağıda teklif verebileceğiniz maksimum adetler öneren bilgilendirmeyi
              okuyarak işleminize devam edebilirsiniz.
            </Text>
          </View>
          <ScrollView
            contentContainerStyle={{
              alignItems: "flex-start",
              width: win.width * 0.9,
              paddingHorizontal: win.width * 0.05,
              paddingTop: 20,
            }}
          >
            <View style={{ paddingBottom: 10 }}>
              <Text
                style={{
                  color: colors.price_color,
                  fontSize: 16,
                  fontWeight: "bold",
                }}
              >
                Ürünler
              </Text>
            </View>
            {this.state.UygunsuzUrunler &&
              this.state.UygunsuzUrunler.map((item, index) => {
                console.log("mapping uygunsuzlar", item);
                return (
                  <View style={{ paddingBottom: 10 }}>
                    <Text style={{ fontWeight: "500" }}>
                      <Text>{item.Kod} kodlu üründen en fazla</Text>
                      <Text style={{ fontWeight: "700" }}> {item.Adet} </Text>
                      <Text>adet için teklif alabilirsiniz.</Text>
                    </Text>
                  </View>
                );
              })}
          </ScrollView>
          <TouchableOpacity
            onPress={() => this.props.applyQuantities(this.state.UygunsuzUrunler)}
            style={[styles.btn, { backgroundColor: colors.light_green, marginBottom: 10 }]}
          >
            <Text
              fontScale={0.5}
              style={{
                color: "white",
                fontSize: 13,
                fontFamily: "Poppins-SemiBold",
              }}
            >
              Önerilen adetleri uygula ve devam et
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.toggleVisibility()}
            style={[styles.btn, { backgroundColor: colors.red, marginBottom: 10 }]}
          >
            <Text
              fontScale={0.5}
              style={{
                color: "white",
                fontSize: 13,
                fontFamily: "Poppins-SemiBold",
              }}
            >
              İptal Et
            </Text>
          </TouchableOpacity>
        </View>
      </ModalBox>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "column",
    backgroundColor: "white",
    width: win.width * 0.9,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.5,
    shadowRadius: 2,
    elevation: 1,
    alignItems: "center",
    marginBottom: 50,
    borderRadius: 6,
  },
  btn: {
    flexDirection: "column",
    width: win.width * 0.8,
    alignItems: "center",
    justifyContent: "center",
    height: 40,
    borderRadius: 4,
  },
});
