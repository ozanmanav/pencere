import React, { Component } from "react";
import { View, TouchableOpacity, StyleSheet, Dimensions, Image } from "react-native";
import { Text, ModalBox } from "@components";
import { moderateScale } from "react-native-size-matters";
const win = Dimensions.get("window");

export default class CartModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedItem: 0,
    };
  }

  toggleVisibility = () => {
    this.modalBox.toggleVisibility();
  };

  render() {
    return (
      <ModalBox ref={(c) => (this.modalBox = c)} statusBarHidden={false} scrollable={false}>
        <View style={styles.container}>
          <View
            style={{
              flexDirection: "row",
              height: moderateScale(40, 0.5),
              width: win.width * 0.75,
              borderBottomColor: "#dedede",
              borderBottomWidth: 1,
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <Text style={{ fontSize: 16, paddingBottom: 5 }}>Sepet Seç</Text>
          </View>
          {this.props.buckets &&
            this.props.buckets.map((bucket, i) => {
              return (
                <TouchableOpacity
                  onPress={() => {
                    this.props.changeSelectedCart(bucket.SepetKodu);
                    this.toggleVisibility();
                  }}
                  style={{
                    alignItems: "center",
                    flexDirection: "row",
                    paddingVertical: moderateScale(5, 0.5),
                    borderBottomColor: "#dedede",
                    borderBottomWidth: i < this.props.buckets.length - 1 ? 1 : 0,
                  }}
                >
                  {bucket.SepetKodu === this.props.selectedCart && (
                    <Image
                      source={require("@assets/images/checked.png")}
                      style={{
                        position: "absolute",
                        left: moderateScale(5, 0.5),
                        width: moderateScale(16, 0.5),
                        height: moderateScale(16, 0.5),
                        marginTop: moderateScale(3, 0.5),
                        tintColor: "green",
                      }}
                    />
                  )}
                  <Text
                    style={{
                      fontFamily:
                        bucket.SepetKodu === this.props.selectedCart ? "Poppins-Medium" : "Poppins",
                      fontSize: 16,
                      marginLeft: bucket.SepetKodu === this.state.selectedItem.SepetKodu ? 4 : 20,
                      paddingLeft: 5,
                    }}
                  >
                    {bucket.SepetAdi}
                  </Text>
                </TouchableOpacity>
              );
            })}
        </View>
      </ModalBox>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    padding: moderateScale(5, 0.5),
    backgroundColor: "white",
    width: win.width * 0.75,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.5,
    shadowRadius: 2,
    elevation: 1,
    borderRadius: 2,
    overflow: "hidden",
  },
});
