import React, { Component } from "react";
import {
  View,
  SafeAreaView,
  StyleSheet,
  Platform,
  TouchableOpacity,
  Image,
  Dimensions,
  FlatList,
} from "react-native";
import { connect } from "react-redux";
import { Text, AnimatedHeader, PencereIndicator } from "@components";
import colors from "@common/styles/colors";
import { getEndPoint } from "@common/Utils";

const win = Dimensions.get("window");

class HelpScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isFetching: false,
      items: [],
    };
  }
  componentDidMount() {
    this.setState({ isFetching: true });
    fetch(getEndPoint("UTIL_URL") + "?Si=" + this.props.session.info.SessionID + "&func=6")
      .then((res) => res.json())
      .then((responseJson) => {
        this.setState({ isFetching: false, items: responseJson });
      });
  }

  renderItem = ({ item }) => {
    return (
      <TouchableOpacity
        style={styles.itemContainer}
        onPress={() => this.props.navigation.navigate("HelpDetailsScreen", { item })}
      >
        <Text style={{ fontSize: 16 }}>{item.Baslik}</Text>
        <Image
          source={require("@assets/images/arrow-right.png")}
          style={{ width: 16, height: 16, tintColor: "#d0d0d0" }}
        />
      </TouchableOpacity>
    );
  };
  render() {
    return (
      <SafeAreaView style={{ backgroundColor: colors.primary_dark }} forceInset={{ top: "always" }}>
        <View
          style={{
            height: win.height,
            width: win.width,
            backgroundColor: "#efefef",
          }}
        >
          <FlatList
            style={{ height: win.height }}
            data={this.state.items}
            renderItem={this.renderItem}
            contentOffset={{ y: -110 }}
            contentInset={{ top: 110, left: 0, bottom: 0, right: 0 }}
            contentContainerStyle={{
              paddingBottom: 10,
              paddingTop: Platform.OS === "android" ? 110 : 0,
            }}
          />
          {this.state.isFetching && (
            <View style={[styles.loadingContainer]}>
              <PencereIndicator />
            </View>
          )}
          <AnimatedHeader
            navigation={this.props.navigation}
            title="Yardım"
            clampedScroll={this.state.clampedScroll}
            back={false}
            home={true}
            searchBar={true}
            search={false}
          />
        </View>
      </SafeAreaView>
    );
  }
}

function mapStateToProps(state) {
  return {
    session: state.session,
  };
}

export default connect(mapStateToProps, null)(HelpScreen);

const styles = StyleSheet.create({
  loadingContainer: {
    height: win.height - 50,
    width: win.width,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0,0,0,0.5)",
    position: "absolute",
    top: 0,
    left: 0,
  },
  itemContainer: {
    padding: 5,
    paddingVertical: 10,
    backgroundColor: "white",
    borderBottomColor: "#dedede",
    borderBottomWidth: 1,
    flexDirection: "row",
    flex: 1,
    justifyContent: "space-between",
  },
});
