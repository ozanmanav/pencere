import React, { Component } from "react";
import { SafeAreaView, View, Dimensions, Platform } from "react-native";
import { SimpleHeader } from "@components";
import colors from "@common/styles/colors";
import { WebView } from "react-native-webview";

const win = Dimensions.get("window");

class HelpDetailsScreen extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <SafeAreaView style={{ backgroundColor: colors.primary_dark }} forceInset={{ top: "always" }}>
        <View style={{ height: win.height - 60, width: win.width, backgroundColor: "#efefef" }}>
          <SimpleHeader
            style={{ backgroundColor: colors.primary_dark, borderColor: colors.primary_dark }}
            titleStyle={{ color: "white" }}
            iconColor="white"
            navigation={this.props.navigation}
            title={this.props.navigation.state.params.item.Baslik}
            onBackPress={() => this.props.navigation.goBack()}
          />
          <WebView
            source={{ url: this.props.navigation.state.params.item.Url }}
            style={{ width: win.width, borderRadius: 0, alignSelf: "center", borderWidth: 1 }}
            automaticallyAdjustContentInsets={false}
            scalesPageToFit={Platform.OS === "ios" ? false : true}
            javaScriptEnabled={true}
            injectedJavaScript="document.body.style.background = white;"
          />
        </View>
      </SafeAreaView>
    );
  }
}

export default HelpDetailsScreen;
