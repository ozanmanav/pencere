import {
  GET_FAVORITES,
  ADD_FAVORITE,
  GET_FAVORITES_SUCESS,
  ADD_FAVORITE_SUCCESS,
  GET_FAVORITES_FAILURE,
  ADD_FAVORITE_FAILURE,
  REMOVE_FAVORITE,
  REMOVE_FAVORITE_SUCCESS,
  REMOVE_FAVORITE_FAILURE,
} from "@redux/actions/types";
import bugsnag from "@common/bugsnag_config";
import { showToast } from "../../../redux/actions/toast";
import { getEndPoint, convertToUsd } from "@common/Utils";
import visilabsManager from "@app/visilabsManager";

export function getFavorites() {
  return (dispatch, getState) => {
    dispatch(getFavoritesInit());
    fetch(getEndPoint("FAVORITES_URL") + "?si=" + getState().session.info.SessionID + "&func=2")
      .then((res) => {
        return res.json();
      })
      .then((responseJson) => {
        console.log("takip listesi", responseJson);
        dispatch(getFavoritesSuccess(responseJson.takipListesi));
      })
      .catch((error) => {
        bugsnag.notify(error, (report) => {
          report.errorClass = "getFavorites";
        });
        console.log(error);
        dispatch(getFavoritesFailure(error));
      });
  };
}

export function addFavorite(pID, f = false, s = false) {
  return (dispatch, getState) => {
    dispatch(addFavoriteInit());
    return new Promise(function (resolve) {
      fetch(
        getEndPoint("FAVORITES_URL") +
          "?si=" +
          getState().session.info.SessionID +
          "&func=1" +
          "&s=" +
          s +
          "&f=" +
          f +
          "&id=" +
          pID,
      )
        .then((res) => res.json())
        .then((responseJson) => {
          resolve("ADD_FAVORITE_SUCCESS");
          const p = responseJson.takipListesi[0];
          var data = { "OM.pf": p.urunKodu, "OM.pfu": "1", "OM.ppr": p.bayiFiyati };
          visilabsManager.customEvent("Add To Favorites", data);
          dispatch(addFavoriteSuccess(responseJson.takipListesi));
        })
        .catch((error) => {
          bugsnag.notify(error, (report) => {
            report.errorClass = "addFavorite";
          });
          console.log(error);
          dispatch(addFavoriteFailure(error));
        });
    });
  };
}

export function removeFavorite(product) {
  return (dispatch, getState) => {
    dispatch(removeFavoriteInit());
    return new Promise(function (resolve) {
      fetch(
        getEndPoint("FAVORITES_URL") +
          "?si=" +
          getState().session.info.SessionID +
          "&func=3" +
          "&id=" +
          (product.urunKodu || product.UrunKodu),
      )
        .then((res) => res.json())
        .then((responseJson) => {
          resolve("REMOVE_FAVORITE_SUCCESS");
          dispatch(showToast("Favorilerinizden çıkarıldı"));
          dispatch(removeFavoriteSuccess(responseJson.takipListesi));
          var data = {
            "OM.pf": product.urunKodu,
            "OM.pfu": "-1",
            "OM.ppr": convertToUsd(product.bayiFiyati),
          };
          visilabsManager.customEvent("Add To Favorites", data);
        })
        .catch((error) => {
          console.error("error", error);
          dispatch(showToast("Bir hata oluştu"));
          bugsnag.notify(error, (report) => {
            report.errorClass = "removeFavorite";
          });
          dispatch(removeFavoriteFailure(error));
        });
    });
  };
}

function getFavoritesFailure(error) {
  return {
    type: GET_FAVORITES_FAILURE,
    error,
  };
}

function getFavoritesInit() {
  return {
    type: GET_FAVORITES,
  };
}

function getFavoritesSuccess(data) {
  console.log("getFavoritesSuccess", data);
  return {
    type: GET_FAVORITES_SUCESS,
    data,
  };
}

function addFavoriteInit() {
  return {
    type: ADD_FAVORITE,
  };
}

function addFavoriteSuccess(list) {
  return {
    type: ADD_FAVORITE_SUCCESS,
    data: list,
  };
}

function addFavoriteFailure(error) {
  return {
    type: ADD_FAVORITE_FAILURE,
    error,
  };
}

function removeFavoriteInit() {
  return {
    type: REMOVE_FAVORITE,
  };
}

function removeFavoriteSuccess(data) {
  return {
    type: REMOVE_FAVORITE_SUCCESS,
    data,
  };
}

function removeFavoriteFailure() {
  return {
    type: REMOVE_FAVORITE_FAILURE,
  };
}
