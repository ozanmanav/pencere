import {
  REMOVE_FAVORITE,
  REMOVE_FAVORITE_SUCCESS,
  REMOVE_FAVORITE_FAILURE,
  GET_FAVORITES,
  ADD_FAVORITE,
  GET_FAVORITES_SUCESS,
  ADD_FAVORITE_SUCCESS,
  GET_FAVORITES_FAILURE,
  ADD_FAVORITE_FAILURE,
} from "@redux/actions/types";

const initialState = {
  items: [],
  progress: false,
  error: false,
};

export default function favoritesReducer(state = initialState, action) {
  switch (action.type) {
    case GET_FAVORITES:
      return {
        ...state,
        items: [],
        progress: true,
      };
    case GET_FAVORITES_SUCESS:
      return {
        ...state,
        progress: false,
        items: action.data,
      };
    case GET_FAVORITES_FAILURE:
      return {
        ...state,
        progress: false,
        error: true,
      };
    case ADD_FAVORITE:
      return {
        ...state,
        progress: true,
      };
    case ADD_FAVORITE_SUCCESS:
      return {
        ...state,
        progress: false,
        items: action.data,
      };
    case ADD_FAVORITE_FAILURE:
      return {
        ...state,
        progress: false,
        error: true,
      };
    case REMOVE_FAVORITE:
      return {
        ...state,
        progress: true,
      };
    case REMOVE_FAVORITE_SUCCESS:
      return {
        ...state,
        progress: false,
        items: action.data,
      };
    case REMOVE_FAVORITE_FAILURE:
      return {
        ...state,
        progress: false,
        error: true,
      };
    default:
      return state;
  }
}
