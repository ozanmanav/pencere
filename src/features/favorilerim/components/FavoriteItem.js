import React, { Component } from "react";
import {
  View,
  TouchableOpacity,
  Image,
  StyleSheet,
  Alert,
  Dimensions,
  Clipboard,
  TextInput,
  LayoutAnimation,
} from "react-native";
import { Text, SmartImage } from "@components";
import { connect } from "react-redux";
import { setNumber, removeFavorite, showToast } from "@redux/actions";
import colors from "@common/styles/colors";
import { UPDATE_SPRINGY } from "@common/LayoutAnimations";
import { moderateScale } from "react-native-size-matters";

const win = Dimensions.get("window");

class FavoriteItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      number: 1,
    };
  }

  removeFavorite = (item) => {
    this.props
      .removeFavorite(item)
      .then((msg) => {
        if (msg === "REMOVE_FAVORITE_SUCCESS") {
          LayoutAnimation.configureNext(UPDATE_SPRINGY);
          this.props.showToast("Ürün favorilerinizden çıkarıldı");
        }
      })
      .catch((err) => {
        console.log("removeFav", err);
        this.props.showToast("Bir hata oluştu");
      });
  };

  render() {
    const firsatVar =
      typeof this.props.item.yeniFirsatFiyati !== "undefined" &&
      this.props.item.yeniFirsatFiyati !== "";
    return (
      <View
        style={styles.container}
        horizontalOnly={true}
        snapPoints={[
          { x: win.width - win.width * 0.03 * 2 },
          { x: 0, damping: 0.65, tension: 500, haptics: true },
          { x: -win.width - win.width * 0.03 * 2 },
        ]}
        onSnap={({ nativeEvent }) => {
          nativeEvent.index !== 1 && this.removeFavorite(this.props.item.urunKodu);
        }}
      >
        <TouchableOpacity
          style={{
            flex: 0.85,
            flexDirection: "row",
            alignItems: "center",
            padding: moderateScale(5, 0.5),
          }}
          onPress={() =>
            this.props.navigation.navigate({
              key: "favorites",
              routeName: "ProductDetailsScreen",
              params: {
                UrunKodu: this.props.item.urunKodu,
                SeriNo: this.props.item.UrunSeriNo,
              },
            })
          }
        >
          <SmartImage
            style={{
              marginHorizontal: moderateScale(6, 0.5),
              flexDirection: "column",
              width: moderateScale(80, 0.5),
              height: moderateScale(60, 0.5),
              resizeMode: "contain",
            }}
            source={{ uri: this.props.item.UrunResimThumbnail }}
            resizeMode={"contain"}
          />
          <View
            style={{
              flex: 1,
              flexDirection: "column",
              marginRight: 10,
              justifyContent: "center",
            }}
          >
            <View style={{ flexDirection: "row" }}>
              <Text
                fontScale={0.5}
                style={{
                  fontFamily: "Poppins-Bold",
                  fontSize: 10,
                  color: "#68aef2",
                  letterSpacing: 0.5,
                  marginRight: 5,
                }}
              >
                {this.props.item.marka}
              </Text>
              <View style={{ flexDirection: "row", alignItems: "center", flex: 1 }}>
                <Text
                  fontScale={0.5}
                  onPress={() => {
                    Clipboard.setString(this.props.item.urunKodu);
                    this.props.showToast("Ürün kodu kopyalandı");
                  }}
                  style={{
                    fontFamily: "Poppins",
                    fontSize: 10,
                    color: "#cecece",
                    marginRight: moderateScale(5, 0.4),
                  }}
                >
                  {this.props.item.urunKodu}
                </Text>
                <TouchableOpacity
                  onPress={() => {
                    Clipboard.setString(this.props.item.urunKodu);
                    this.props.showToast("Ürün kodu kopyalandı");
                  }}
                >
                  <Image
                    source={require("@assets/images/copy.png")}
                    style={{
                      tintColor: "#cecece",
                      width: moderateScale(12, 0.4),
                      height: moderateScale(12, 0.4),
                    }}
                  />
                </TouchableOpacity>
              </View>
            </View>
            <Text
              fontScale={0.5}
              numberOfLines={2}
              ellipsizeMode="tail"
              onLongPress={() => {
                Clipboard.setString(this.props.item.urunAciklama);
                this.props.showToast("Ürün açıklaması kopyalandı");
              }}
              onPress={() => {
                this.props.navigation.navigate({
                  key: "favorites",
                  routeName: "ProductDetailsScreen",
                  params: {
                    UrunKodu: this.props.item.urunKodu,
                    SeriNo: this.props.item.UrunSeriNo,
                  },
                });
              }}
              style={{
                fontFamily: "Poppins",
                fontSize: 12,
                alignSelf: "flex-start",
                paddingVertical: 5,
                color: colors.light_black,
              }}
            >
              {this.props.item.urunAciklama}
            </Text>
            <View style={{ flexDirection: "row" }}>
              <View
                style={{
                  flexDirection: "column",
                  paddingRight: moderateScale(5, 0.5),
                }}
              >
                {typeof this.props.item.yeniBayiFiyati !== "undefined" &&
                  this.props.item.yeniBayiFiyati !== "" && (
                    <Text
                      fontScale={0.5}
                      style={{
                        fontFamily: "Poppins",
                        fontSize: 12,
                        color: firsatVar ? "gray" : colors.price_color,
                      }}
                    >
                      Size Özel:{" "}
                    </Text>
                  )}
                {firsatVar && (
                  <Text
                    fontScale={0.5}
                    style={{
                      fontFamily: "Poppins",
                      fontSize: 12,
                      color: colors.price_color,
                    }}
                  >
                    Fırsat Fiyatı:{" "}
                  </Text>
                )}
              </View>
              <View style={{ flexDirection: "column" }}>
                {typeof this.props.item.yeniBayiFiyati !== "undefined" &&
                  this.props.item.yeniBayiFiyati !== "" && (
                    <Text
                      fontScale={0.5}
                      style={{
                        fontFamily: "Poppins",
                        fontSize: 12,
                        color: firsatVar ? "gray" : colors.price_color,
                        textDecorationLine: firsatVar ? "line-through" : "none",
                      }}
                    >
                      {this.props.item.yeniBayiFiyati}
                    </Text>
                  )}
                {firsatVar && (
                  <Text
                    fontScale={0.5}
                    style={{
                      fontFamily: "Poppins",
                      fontSize: 12,
                      color: colors.price_color,
                    }}
                  >
                    {this.props.item.yeniFirsatFiyati}
                  </Text>
                )}
              </View>
            </View>
          </View>
        </TouchableOpacity>
        <View
          style={{
            flexDirection: "column",
            flex: 0.15,
            justifyContent: "space-between",
            alignItems: "center",
            paddingVertical: 10,
          }}
        >
          <TextInput
            selectTextOnFocus={true}
            returnKeyType="done"
            value={String(this.state.number)}
            onChangeText={(text) => this.setState({ number: text })}
            keyboardType="numeric"
            style={{
              width: 38,
              height: 42,
              marginBottom: 5,
              padding: 0,
              borderRadius: 2,
              borderLeftColor: "#ececec",
              borderRightColor: "#e5e5e5",
              borderTopColor: "#dedede",
              borderWidth: 1,
              borderBottomWidth: 0,
              textAlign: "center",
              backgroundColor: "#f5f4f5",
            }}
          />
          <TouchableOpacity
            onPress={() => this.props.addFavoriteToCart(this.props.item, this.state.number)}
            style={{
              width: 38,
              height: 42,
              marginBottom: 5,
              borderColor: colors.light_green,
              borderWidth: 1,
              borderRadius: 2,
              backgroundColor: colors.light_green,
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <Image
              source={require("@assets/images/cart.png")}
              style={{ width: 16, height: 16, tintColor: "white" }}
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              width: 38,
              height: 42,
              borderColor: colors.red,
              borderWidth: 1,
              borderRadius: 2,
              backgroundColor: colors.red,
              alignItems: "center",
              justifyContent: "center",
            }}
            onPress={() => {
              Alert.alert(
                "Favorilerden Çıkar",
                "Bu ürünü favorilerinizden kaldırmak istiyor musunuz?",
                [
                  { text: "Hayır" },
                  {
                    text: "Evet",
                    onPress: () => {
                      this.removeFavorite(this.props.item);
                    },
                  },
                ],
              );
            }}
          >
            <Image
              source={require("@assets/images/trash.png")}
              style={{ width: 15, height: 15, tintColor: "white" }}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    cart: state.cart,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    setNumber: (uk, number) => dispatch(setNumber(uk, number)),
    removeFavorite: (p) => dispatch(removeFavorite(p)),
    showToast: (msg, dur) => dispatch(showToast(msg, dur)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(FavoriteItem);

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "white",
    borderBottomColor: "rgba(0,0,0,0.2)",
    borderBottomWidth: 1,
    flexDirection: "row",
    marginHorizontal: win.width * 0.03,
    padding: moderateScale(5, 0.5),
    marginBottom: win.width * 0.04,
    flex: 1,
  },
});
