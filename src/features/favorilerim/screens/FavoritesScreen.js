import React, { Component } from "react";
import { StyleSheet, View, Dimensions } from "react-native";
import { AnimatedHeader, TabBarIcon, Text, AnimatedBasePage } from "@components";
import { FavoriteItem } from "@features/favorilerim/components";
import { connect } from "react-redux";
import { setCart, getFavorites, showToast, setNumber } from "@redux/actions";
import colors from "@common/styles/colors";
import { getEndPoint } from "@common/Utils";

const win = Dimensions.get("window");

class FavoritesScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    header: null,
    tabBarIcon: ({ tintColor }) => (
      <TabBarIcon
        iconName="favorites"
        navigation={navigation}
        style={{ width: 20, height: 20 }}
        icon={require("@assets/images/star.png")}
        label="Favorilerim"
        tintColor={tintColor}
      />
    ),
  });
  constructor(props) {
    super(props);
    this.state = {
      total: 0,
      basic: true,
      quantity: "1",
    };
  }
  setQuantity(num) {
    this.setState({
      quantity: String(num),
    });
  }

  increaseQuantity() {
    this.setState({
      quantity: (Number(this.state.quantity) + 1).toString(),
    });
  }

  componentDidMount() {
    this.props.getFavorites();
  }

  decreaseQuantity() {
    if (this.state.quantity > 1)
      this.setState({
        quantity: (Number(this.state.quantity) - 1).toString(),
      });
  }

  _renderRow(rowData) {
    return (
      <FavoriteItem
        item={rowData}
        navigation={this.props.navigation}
        addFavoriteToCart={this.addFavoriteToCart}
      />
    );
  }

  addFavoriteToCart = (product, q) => {
    fetch(
      getEndPoint("CART_URL") +
        "?Si=" +
        this.props.session.info.SessionID +
        "&func=8" +
        "&uk=" +
        product.urunKodu +
        "&adt=" +
        q,
      {
        method: "POST",
      },
    )
      .then(response => response.json())
      .then(responseJson => {
        console.log("addToCart response", responseJson);
        if (responseJson.MesajTipi === "S") {
          this.props.setCart(responseJson);
          this.props.showToast("Ürün sepete eklendi");
        } else {
          this.props.showToast("Bir hata oluştu lütfen tekrar deneyin");
        }
        this.setState({
          progress: false,
        });
      })
      .catch(error => {
        this.props.showToast("Bir hata oluştu lütfen tekrar deneyin");
        console.log("addToCart error", error);
      });
  };

  render() {
    return (
      <AnimatedBasePage
        ref={c => (this.basePage = c)}
        testID="screen_favorites"
        isLoading={this.props.favorites.progress}
        style={{ backgroundColor: colors.primary_dark, flex: 1 }}
        navigation={this.props.navigation}
        showFlatList={true}
        headerProps={{
          title: "Favorilerim",
          back: false,
          alignLeft: false,
          showCart: true,
          searchBar: true,
          search: true,
        }}
        flatListProps={{
          testID: "scroll_favorites",
          overScrollMode: "never",
          automaticallyAdjustContentInsets: false,
          data: this.props.favorites.items,
          onScroll: this.onScroll,
          renderSectionHeader: this.renderHeader,
          contentContainerStyle: { paddingTop: 15 },
          ListEmptyComponent: this.renderNoContent,
          renderItem: ({ item }) => this._renderRow(item),
          keyExtractor: item => item.urunKodu,
        }}
      />
    );
  }

  renderNoContent = () => {
    return (
      <View
        style={{
          width: win.width,
        }}
      >
        <Text
          style={{
            fontSize: 16,
            color: "gray",
            textAlign: "center",
            marginVertical: 10,
            backgroundColor: "transparent",
          }}
        >
          Favori listeniz boş
        </Text>
      </View>
    );
  };
}

function mapStateToProps(state) {
  return {
    favorites: state.favorites,
    session: state.session,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setNumber: (uk, number) => dispatch(setNumber(uk, number)),
    setCart: cart => dispatch(setCart(cart)),
    getFavorites: () => dispatch(getFavorites()),
    showToast: msg => dispatch(showToast(msg)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(FavoritesScreen);

const styles = StyleSheet.create({
  loadingContainer: {
    height: win.height,
    width: win.width,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0,0,0,0.3)",
    position: "absolute",
  },
});
