import React, { Component } from "react";
import { View, Dimensions, Image, ImageBackground, TouchableOpacity } from "react-native";
import { Text } from "@components";
import { moderateScale } from "react-native-size-matters";

const win = Dimensions.get("window");

export default class CategoriesWidgetTop extends Component {
  renderHeader = () => {
    return (
      <ImageBackground
        source={require("@assets/images/shapes_texture.png")}
        style={{
          padding: moderateScale(20, 0.5),
          flexDirection: "row",
          alignItems: "center",
          backgroundColor: "#d55749",
          positon: "absolute",
          height: moderateScale(150, 0.5),
          width: win.width - (win.width / 25) * 2,
          resizeMode: "repeat",
          marginVertical: moderateScale(5, 0.5),
        }}
      >
        <View style={{ flex: 6, justifyContent: "space-between", height: moderateScale(110, 0.5) }}>
          <View>
            <Text
              style={{
                fontFamily: "Poppins-SemiBold",
                color: "white",
                fontSize: 20,
                lineHeight: 24,
                marginTop: 0,
                includeFontPadding: false,
              }}
            >
              KİŞİSEL
            </Text>
            <Text
              style={{
                fontFamily: "Poppins-SemiBold",
                color: "white",
                fontSize: 20,
                lineHeight: 24,
                marginTop: 0,
                includeFontPadding: false,
              }}
            >
              BİLGİSAYARLAR
            </Text>
          </View>
          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate({
                routeName: "SearchScreen",
                params: {
                  searchParams: { Hiyerarsi: "PC_", SearchText: "" },
                  focus: false,
                  reload: true,
                },
              })
            }
            style={{
              flexDirection: "row",
              width: moderateScale(110, 0.5),
              height: moderateScale(30, 0.5),
              justifyContent: "space-between",
              alignItems: "center",
              backgroundColor: "#ca4f41",
              borderColor: "rgba(221, 221, 221, 0.3)",
              borderWidth: 1,
              borderRadius: 3,
            }}
          >
            <Text
              style={{ fontFamily: "Poppins", fontSize: 12, color: "white", textAlign: "center" }}
            >
              Tüm Ürünler
            </Text>
            <Image
              source={require("@assets/images/arrow-right.png")}
              style={{
                marginLeft: moderateScale(5, 0.5),
                width: moderateScale(8, 0.5),
                height: moderateScale(8, 0.5),
              }}
            />
          </TouchableOpacity>
        </View>
        <View style={{ flex: 4, alignItems: "flex-end" }}>
          <Image
            source={require("@assets/images/pc_showcase.png")}
            style={{
              width: moderateScale(156, 0.5),
              height: moderateScale(142, 0.5),
              resizeMode: "contain",
            }}
          />
        </View>
      </ImageBackground>
    );
  };
  render() {
    return (
      <View
        style={{
          flexDirection: "column",
          backgroundColor: "white",
          paddingBottom: moderateScale(36, 0.5),
          paddingHorizontal: moderateScale(10, 0.5),
          width: win.width - (2 * win.width) / 25,
          marginVertical: moderateScale(5, 0.5),
        }}
      >
        <View style={{ flexDirection: "row", marginTop: moderateScale(36, 0.5) }}>
          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate({
                routeName: "SearchScreen",
                params: {
                  searchParams: {
                    SearchText: "PC_DES,%20PC_PORPOR,%20PC_PORTIO",
                    Hiyerarsi: "",
                    Marka: "",
                  },
                  focus: false,
                  reload: true,
                },
              })
            }
            style={{
              flexDirection: "column",
              flex: 1,
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <Image
              source={require("@assets/images/cat_pc.png")}
              style={{ width: win.width / 3, height: win.width / 6, resizeMode: "contain" }}
            />
            <View style={{ flexDirection: "row", alignItems: "center", marginTop: 6 }}>
              <Text style={{ fontFamily: "Poppins", fontSize: 11, color: "#666a6c" }}>
                {"Bilgisayar ve Tabletler"}
              </Text>
              <Image
                source={require("@assets/images/arrow-right.png")}
                style={{
                  width: moderateScale(10, 0.5),
                  height: moderateScale(10, 0.5),
                  marginLeft: moderateScale(2, 0.5),
                  resizeMode: "contain",
                  tintColor: "#9e9e9e",
                }}
              />
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate({
                routeName: "SearchScreen",
                params: {
                  searchParams: { Hiyerarsi: "OEM", SearchText: "", Marka: "" },
                  focus: false,
                  reload: true,
                },
              })
            }
            style={{
              flexDirection: "column",
              flex: 1,
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <Image
              source={require("@assets/images/cat_oem.png")}
              style={{ width: win.width / 3, height: win.width / 6, resizeMode: "contain" }}
            />
            <View
              style={{
                flexDirection: "row",
                alignItems: "center",
                marginTop: moderateScale(6, 0.5),
              }}
            >
              <Text style={{ fontFamily: "Poppins", fontSize: 11, color: "#666a6c" }}>
                OEM - Çevre Birimleri
              </Text>
              <Image
                source={require("@assets/images/arrow-right.png")}
                style={{
                  width: moderateScale(10, 0.5),
                  height: moderateScale(10, 0.5),
                  marginLeft: moderateScale(2, 0.5),
                  resizeMode: "contain",
                  tintColor: "#9e9e9e",
                }}
              />
            </View>
          </TouchableOpacity>
        </View>
        <View style={{ flexDirection: "row", marginTop: moderateScale(36, 0.5) }}>
          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate({
                routeName: "SearchScreen",
                params: {
                  searchParams: { Hiyerarsi: "GAM", Marka: "", SearchText: "" },
                  focus: false,
                  reload: true,
                },
              })
            }
            style={{
              flexDirection: "column",
              flex: 1,
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <Image
              source={require("@assets/images/cat_gam.png")}
              style={{ width: win.width / 3, height: win.width / 6, resizeMode: "contain" }}
            />
            <View
              style={{
                flexDirection: "row",
                alignItems: "center",
                marginTop: moderateScale(6, 0.5),
              }}
            >
              <Text style={{ fontFamily: "Poppins", fontSize: 11, color: "#666a6c" }}>
                Gaming Ürünleri
              </Text>
              <Image
                source={require("@assets/images/arrow-right.png")}
                style={{
                  width: moderateScale(10, 0.5),
                  height: moderateScale(10, 0.5),
                  marginLeft: moderateScale(2, 0.5),
                  resizeMode: "contain",
                  tintColor: "#9e9e9e",
                }}
              />
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate({
                routeName: "SearchScreen",
                params: {
                  searchParams: {
                    SearchText: "PRNLAS,%20PRNPRN,%20PRNSCA,%20PRNCOP,%20PRNFAX,%20PRNPLO",
                    Hiyerarsi: "",
                    Marka: "",
                  },
                  focus: false,
                  reload: true,
                },
              })
            }
            style={{
              flexDirection: "column",
              flex: 1,
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <Image
              source={require("@assets/images/cat_prn.png")}
              style={{ width: win.width / 3, height: win.width / 6, resizeMode: "contain" }}
            />
            <View
              style={{
                flexDirection: "row",
                alignItems: "center",
                marginTop: moderateScale(6, 0.5),
              }}
            >
              <Text style={{ fontFamily: "Poppins", fontSize: 11, color: "#666a6c" }}>
                Baskı Çözümleri
              </Text>
              <Image
                source={require("@assets/images/arrow-right.png")}
                style={{
                  width: moderateScale(10, 0.5),
                  height: moderateScale(10, 0.5),
                  marginLeft: moderateScale(2, 0.5),
                  resizeMode: "contain",
                  tintColor: "#9e9e9e",
                }}
              />
            </View>
          </TouchableOpacity>
        </View>
        <View style={{ flexDirection: "row", marginTop: 36 }}>
          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate({
                routeName: "SearchScreen",
                params: {
                  searchParams: { Hiyerarsi: "OFS", Marka: "", SearchText: "" },
                  focus: false,
                  reload: true,
                },
              })
            }
            style={{
              flexDirection: "column",
              flex: 1,
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <Image
              source={require("@assets/images/cat_ofs.png")}
              style={{ width: win.width / 3, height: win.width / 6, resizeMode: "contain" }}
            />
            <View
              style={{
                flexDirection: "row",
                alignItems: "center",
                marginTop: moderateScale(6, 0.5),
              }}
            >
              <Text style={{ fontFamily: "Poppins", fontSize: 11, color: "#666a6c" }}>
                Ofis - Tüketim Ürünleri
              </Text>
              <Image
                source={require("@assets/images/arrow-right.png")}
                style={{
                  width: moderateScale(10, 0.5),
                  height: moderateScale(10, 0.5),
                  marginLeft: moderateScale(2, 0.5),
                  resizeMode: "contain",
                  tintColor: "#9e9e9e",
                }}
              />
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate({
                routeName: "SearchScreen",
                params: {
                  searchParams: {
                    Hiyerarsi: "",
                    Marka: "",
                    SearchText: "TELCEPSMT,%20TELCMC,%20TELCEP,%20TELSWE,%20TELBUS",
                  },
                  focus: false,
                  reload: true,
                },
              })
            }
            style={{
              flexDirection: "column",
              flex: 1,
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <Image
              source={require("@assets/images/cat_tel.png")}
              style={{ width: win.width / 3, height: win.width / 6, resizeMode: "contain" }}
            />
            <View
              style={{
                flexDirection: "row",
                alignItems: "center",
                marginTop: moderateScale(6, 0.5),
              }}
            >
              <Text style={{ fontFamily: "Poppins", fontSize: 11, color: "#666a6c" }}>
                Telefonlar
              </Text>
              <Image
                source={require("@assets/images/arrow-right.png")}
                style={{
                  width: moderateScale(10, 0.5),
                  height: moderateScale(10, 0.5),
                  marginLeft: moderateScale(2, 0.5),
                  resizeMode: "contain",
                  tintColor: "#9e9e9e",
                }}
              />
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
