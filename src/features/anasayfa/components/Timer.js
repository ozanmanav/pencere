import React, { Component } from "react";
import { View, Dimensions } from "react-native";
const win = Dimensions.get("window");
import { Text } from "@components";
import colors from "@common/styles/colors";
import { ScaledSheet } from "react-native-size-matters";
import { moderateScale } from "react-native-size-matters";
export default class Timer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      remainingTime: 0,
      hour: "",
      min: "",
      sec: "",
    };
  }

  msToText(ms) {
    let hour = parseInt(ms / (60 * 60));
    let min = parseInt((ms - hour * 60 * 60) / 60);
    let sec = parseInt(ms - hour * 60 * 60 - min * 60);

    hour = hour < 10 ? "0" + hour : hour;
    min = min < 10 ? "0" + min : min;
    sec = sec < 10 ? "0" + sec : sec;
    this.setState({ hour, min, sec });
  }

  componentDidMount() {
    this.setState({ remainingTime: this.props.remainingTime }, () => {
      this._interval = setInterval(() => {
        this.setState({ remainingTime: this.state.remainingTime - 1 }, () => {
          this.msToText(this.state.remainingTime);
        });
      }, 1000);
    });
  }

  componentWillUnmount() {
    clearInterval(this._interval);
  }
  render() {
    return (
      <View
        style={{
          zIndex: -1,
          width: win.width - (win.width / 25) * 2,
          padding: win.width / 20,
          backgroundColor: "transoarent",
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "space-evenly",
        }}
      >
        <View
          style={{
            width: win.width * 0.55,
            flexDirection: "row",
            alignItems: "center",
            marginRight: win.width / 10,
          }}
        >
          <View style={styles.timeBox}>
            <Text fontScale={0.6} style={styles.timeVal}>
              {this.state.hour}
            </Text>
            <Text fontScale={0.6} style={styles.timeDesc}>
              SAAT
            </Text>
          </View>
          <Text
            fontScale={1.5}
            style={{ textAlign: "center", color: "gray", fontWeight: "bold", fontSize: 13 }}
          >
            :
          </Text>
          <View style={styles.timeBox}>
            <Text fontScale={0.6} style={styles.timeVal}>
              {this.state.min}
            </Text>
            <Text fontScale={0.6} style={styles.timeDesc}>
              DAKİKA
            </Text>
          </View>
          <Text
            fontScale={1.5}
            style={{ textAlign: "center", color: "gray", fontWeight: "bold", fontSize: 13 }}
          >
            :
          </Text>
          <View style={styles.timeBox}>
            <Text fontScale={0.6} style={styles.timeVal}>
              {this.state.sec}
            </Text>
            <Text fontScale={0.6} style={styles.timeDesc}>
              SANİYE
            </Text>
          </View>
        </View>
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            width: moderateScale(60, 0.5),
            height: moderateScale(60, 0.5),
            borderRadius: moderateScale(30.5),
            paddingHorizontal: moderateScale(5, 0.5),
            justifyContent: "space-evenly",
            backgroundColor: colors.light_green,
          }}
        >
          <View style={{ alignItems: "center", justifyContent: "center", flex: 1, paddingLeft: 2 }}>
            <Text
              fontScale={0.6}
              style={{
                fontSize: 8,
                color: "white",
                textAlign: "center",
                fontFamily: "Poppins-SemiBold",
                lineHeight: 10,
                marginTop: 2,
              }}
            >
              {"SON ADET"}
            </Text>
          </View>
          {this.props.stock && (
            <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
              <Text
                fontScale={0.5}
                style={{
                  fontSize:
                    this.props.stock.toString().length === 1
                      ? 28
                      : this.props.stock.toString().length === 2
                      ? 16
                      : 12,
                  color: "white",
                  textAlign: "center",
                  fontFamily: "Poppins-SemiBold",
                }}
              >
                {this.props.stock > 99 ? "99+" : this.props.stock}
              </Text>
            </View>
          )}
        </View>
      </View>
    );
  }
}

const styles = ScaledSheet.create({
  timeBox: {
    borderRightColor: colors.light_gray,
    backgroundColor: "#eaeaea",
    flexDirection: "column",
    width: "50@ms0.5",
    alignItems: "center",
    justifyContent: "center",
  },
  timeVal: {
    fontFamily: "Poppins-Medium",
    fontSize: 17,
    lineHeight: 15,
    paddingTop: "9@ms0.5",
    marginBottom: -7,
    textAlign: "center",
    color: "#444",
  },
  timeDesc: {
    textAlign: "center",
    fontSize: 10,
    fontFamily: "Poppins",
    color: "#444",
  },
});
