import React, { Component } from "react";
import { Animated, View, Image, Dimensions, TouchableOpacity, Alert } from "react-native";
import { Text, SmartImage } from "@components";
import Timer from "./Timer";
import { setCart, showToast } from "@redux/actions";
import { connect } from "react-redux";
import colors from "@common/styles/colors";
import { widthWithMargin } from "@common/styles/index.style";
import { ScaledSheet } from "react-native-size-matters";
import { moderateScale } from "react-native-size-matters";
import analytics from "@react-native-firebase/analytics";
import { getEndPoint } from "@common/Utils";
const deviceWidth = Dimensions.get("window").width;
const win = Dimensions.get("window");
const FIXED_BAR_WIDTH = deviceWidth * 0.8;
const BAR_SPACE = 10;

var self;
class FirsatWidget extends Component {
  animVal = new Animated.Value(0);
  springVal = new Animated.Value(0);

  constructor(props) {
    super(props);
    self = this;
    this.state = {
      isVisible: false,
      scrollX: 0,
      number: "1",
      progress: false,
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.isVisible !== prevProps.isVisible)
      this.setState({
        isVisible: this.props.isVisible,
      });
  }

  toggleVisibility = () => {
    this.setState({
      isVisible: !this.state.isVisible,
    });
  };

  handleScroll = function(event) {
    self.setState({ scrollX: event.nativeEvent.contentOffset.x });
    Animated.event([{ nativeEvent: { contentOffset: { x: this.animVal } } }]);
  };

  decreaseNumber = () => {
    this.setState({
      number: Number(this.state.number) > 1 ? (Number(this.state.number) - 1).toString() : "1",
    });
  };

  increaseNumber = () => {
    this.setState({
      number: (Number(this.state.number) + 1).toString(),
    });
  };

  addDealToCart(item) {
    fetch(
      getEndPoint("CART_URL") +
        "?Si=" +
        this.props.session.info.SessionID +
        "&func=9" +
        "&uk=" +
        item.UrunKodu +
        "&Fi=" +
        item.FirsatKodu +
        "&ft=" +
        item.FirsatKazanc +
        "&adt=" +
        this.state.number,
      {
        method: "POST",
      },
    )
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson.MesajTipi === "S") {
          this.setState({ isVisible: false });
          this.props.setCart(responseJson);
          this.props.showToast("Ürün sepete eklendi");
          const splittedPrice = item.FirsatFiyati.replace(".", "")
            .replace(",", ".")
            .split(" ");
          const pp = {
            quantity: 1,
            item_name: item.UrunAciklama,
            item_id: item.UrunKodu,
            price: splittedPrice[0],
            currency: splittedPrice[splittedPrice.length - 1],
          };
          analytics().logEvent("add_to_cart", pp);
        } else {
          this.props.showToast("Ürün sepete eklendi");
        }
      })
      .catch(error => {
        Alert.alert("Beklenmeyen bir hata oluştu.");
        console.log("addToCart error", error);
      });
  }

  bounce(dir) {
    Animated.timing(this.springVal, {
      toValue: dir === "left" ? -10 : 10,
      duration: 100,
      useNativeDriver: true,
    }).start(() => {
      Animated.spring(this.springVal, {
        toValue: 0,
        friction: 5,
        tension: 10,
        useNativeDriver: true,
      }).start();
    });
  }

  scrollToPrev() {
    if (this.state.scrollX > 0)
      this.scrollView.scrollTo({ x: this.state.scrollX - widthWithMargin, y: 0, animated: "true" });
    else {
      this.bounce("right");
    }
  }

  scrollToNext() {
    if (this.state.scrollX < (this.props.items.length - 1) * widthWithMargin)
      this.scrollView.scrollTo({ x: this.state.scrollX + widthWithMargin, y: 0, animated: "true" });
    else {
      this.bounce("left");
    }
  }

  render() {
    if (this.props.items) {
      const numItems = this.props.items.length;
      const itemWidth = FIXED_BAR_WIDTH / numItems - BAR_SPACE;

      let imageArray = [];
      let barArray = [];
      this.props.items.forEach((item, i) => {
        const thisImage = (
          <View
            key={item.UrunKodu}
            style={{
              flexDirection: "column",
              flex: 1,
              paddingBottom: moderateScale(20, 0.5),
            }}
          >
            <View style={{ flexGrow: 3 }}>
              <Timer remainingTime={parseInt(item.FirsatSure)} stock={item.FirsatKalanStokAdeti} />
              <View
                style={{ flexDirection: "row", alignItems: "center", justifyContent: "center" }}
              >
                <TouchableOpacity
                  onPress={() => {
                    this.setState({ isVisible: false });
                    const splittedPb = item.FirsatFiyati.split(" ");
                    this.props.navigation.navigate({
                      key: "main",
                      routeName: "ProductDetailsScreen",
                      params: {
                        UrunKodu: item.UrunKodu,
                        SeriNo: item.UrunSeriNo,
                        pb: splittedPb[splittedPb.length - 1],
                      },
                    });
                  }}
                  style={{ alignItems: "center" }}
                >
                  <SmartImage
                    key={`image${i}`}
                    source={{ uri: item.FirsatResim }}
                    resizeMode="contain"
                    style={{
                      width: widthWithMargin * 0.8,
                      height: (widthWithMargin * 0.8 * 9) / 16,
                      resizeMode: "contain",
                    }}
                  />
                </TouchableOpacity>
              </View>
              <TouchableOpacity
                onPress={() => {
                  this.setState({ isVisible: false });
                  const splittedPb = item.FirsatFiyati.split(" ");
                  this.props.navigation.navigate({
                    key: "main",
                    routeName: "ProductDetailsScreen",
                    params: {
                      UrunKodu: item.UrunKodu,
                      SeriNo: item.UrunSeriNo,
                      pb: splittedPb[splittedPb.length - 1],
                    },
                  });
                }}
                style={{
                  width: widthWithMargin,
                  alignItems: "center",
                  justifyContent: "center",
                  padding: moderateScale(5, 0.5),
                  paddingHorizontal: moderateScale(20, 0.5),
                  paddingBottom: 0,
                }}
              >
                <Text style={{ fontSize: 14, textAlign: "center", marginTop: 10 }}>
                  {item.UrunAciklama}
                </Text>
              </TouchableOpacity>
            </View>
            <View style={{ flexGrow: 2, justifyContent: "center", alignItems: "center" }}>
              <View
                style={{
                  width: widthWithMargin * 0.4,
                  flexDirection: "column",
                  alignItems: "center",
                  marginTop: moderateScale(10, 0.5),
                }}
              >
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "space-between",
                    height: moderateScale(50, 0.5),
                  }}
                >
                  {item.FirsatIndirim && (
                    <View
                      style={{
                        backgroundColor: "#ef1626",
                        borderRadius: 3,
                        marginRight: moderateScale(10, 0.5),
                        width: moderateScale(50, 0.5),
                        height: moderateScale(32, 0.5),
                        alignItems: "center",
                        justifyContent: "center",
                      }}
                    >
                      <Text
                        fontScale={0.6}
                        style={{
                          textAlign: "center",
                          fontSize: 18,
                          color: colors.white,
                          fontFamily: "Poppins-Bold",
                          lineHeight: 20,
                          marginTop: 8,
                        }}
                      >
                        {item.FirsatIndirim}
                      </Text>
                    </View>
                  )}
                  <View
                    style={{
                      justifyContent: "space-between",
                      alignItems: "flex-start",
                      height: moderateScale(32, 0.5),
                    }}
                  >
                    {item.FirsatEskiFiyat && (
                      <Text
                        fontScale={0.6}
                        style={{
                          fontSize: 14,
                          lineHeight: 15,
                          marginTop: 2,
                          textDecorationLine: "line-through",
                          color: "gray",
                          fontFamily: "Poppins",
                        }}
                      >
                        {item.FirsatEskiFiyat +
                          item.FirsatFiyati.substring(
                            item.FirsatFiyati.indexOf(" "),
                            item.FirsatFiyati.length,
                          )}
                      </Text>
                    )}
                    <Text
                      fontScale={0.6}
                      style={{
                        fontSize: 15,
                        lineHeight: 16,
                        marginTop: 2,
                        color: colors.black,
                        fontFamily: "Poppins-SemiBold",
                      }}
                    >
                      {item.FirsatFiyati}
                    </Text>
                  </View>
                </View>
                <Text
                  fontScale={0.6}
                  style={{
                    textAlign: "center",
                    fontSize: 12,
                    color: "gray",
                    fontFamily: "Poppins-Light",
                  }}
                >
                  {"En fazla alım adedi: " + item.FirsatMaxAlimAdeti}
                </Text>
                <View
                  style={{
                    height: moderateScale(1, 0.5),
                    width: widthWithMargin * 0.8,
                    backgroundColor: "#dedede",
                    marginBottom: moderateScale(12, 0.5),
                    marginTop: moderateScale(6, 0.5),
                  }}
                />
                <View style={{ flexDirection: "row", alignItems: "center" }}>
                  {/* <TextInput
                                        selectTextOnFocus={true}
                                        returnKeyType="done"
                                        value={this.state.number}
                                        onChangeText={(text) => this.setState({ number: text })}
                                        keyboardType="numeric"
                                        style={{
                                            width: moderateScale(40, .5),
                                            borderRadius: 3, fontSize: moderateScale(14, .4),
                                            borderLeftColor: '#ececec', borderRightColor: '#e5e5e5',
                                            borderTopColor: '#dedede', borderWidth: 1, borderBottomWidth: 0,
                                            height: moderateScale(36, .5),
                                            marginRight: moderateScale(10,.5),
                                            textAlign: 'center', backgroundColor: '#f5f4f5'
                                        }}
                                    /> */}
                  <TouchableOpacity
                    style={{
                      backgroundColor: colors.green,
                      paddingVertical: moderateScale(4, 0.5),
                      paddingHorizontal: moderateScale(15, 0.5),
                      flexDirection: "row",
                      alignItems: "center",
                      justifyContent: "center",
                      borderRadius: 2,
                    }}
                    onPress={() => this.addDealToCart(item)}
                  >
                    <Image
                      source={require("@assets/images/cart.png")}
                      style={{
                        marginRight: moderateScale(5, 0.5),
                        width: moderateScale(15, 0.5),
                        height: moderateScale(15, 0.5),
                        resizeMode: "contain",
                        tintColor: "white",
                      }}
                    />
                    <Text
                      style={{
                        fontSize: 14,
                        color: "white",
                        fontFamily: "Poppins-Medium",
                        marginTop: 2,
                      }}
                    >
                      Sepete Ekle
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>
        );
        imageArray.push(thisImage);
        const index = Math.ceil(this.state.scrollX / widthWithMargin);
        const thisBar = (
          <View
            key={`bar${i}`}
            style={[
              styles.track,
              {
                width: itemWidth / 2,
                marginLeft: BAR_SPACE,
                backgroundColor: i === index ? "#4CAF50" : "gray",
              },
            ]}
          />
        );
        barArray.push(thisBar);
      });

      return (
        <View style={styles.container} flex={1}>
          <Animated.ScrollView
            ref={c => (this.scrollView = c)}
            horizontal
            showsHorizontalScrollIndicator={false}
            scrollEventThrottle={10}
            pagingEnabled
            onScroll={this.handleScroll}
            style={{ transform: [{ translateX: this.springVal }] }}
          >
            {imageArray}
          </Animated.ScrollView>
          {imageArray.length > 1 && (
            <TouchableOpacity
              onPress={() => this.scrollToPrev()}
              style={{ position: "absolute", left: 0, padding: moderateScale(5, 0.5) }}
            >
              <Image
                source={require("@assets/images/left-arrow-mini.png")}
                style={{
                  width: moderateScale(24, 0.5),
                  height: moderateScale(24, 0.5),
                  tintColor: colors.light_gray,
                }}
              />
            </TouchableOpacity>
          )}
          {imageArray.length > 1 && (
            <TouchableOpacity
              onPress={() => this.scrollToNext()}
              style={{ position: "absolute", right: 0, padding: moderateScale(5, 0.5) }}
            >
              <Image
                source={require("@assets/images/arrow-right.png")}
                style={{
                  width: moderateScale(24, 0.5),
                  height: moderateScale(24, 0.5),
                  tintColor: colors.light_gray,
                }}
              />
            </TouchableOpacity>
          )}
        </View>
      );
    } else return null;
  }
}

const styles = ScaledSheet.create({
  sliderTitle: {
    backgroundColor: "rgba(0,0,0,0.2)",
    width: widthWithMargin,
    height: (widthWithMargin * 9) / 16,
    alignContent: "center",
    textAlign: "center",
    color: "white",
    fontSize: 22,
    padding: "10@ms0.5",
    textShadowColor: "gray",
    textShadowRadius: 6,
    textShadowOffset: {
      width: 1,
      height: 1,
    },
  },
  container: {
    backgroundColor: "white",
    alignItems: "center",
    justifyContent: "center",
    width: win.width - (win.width / 25) * 2,
    marginVertical: "10@ms0.5",
    flexDirection: "row",
  },
  barContainer: {
    position: "absolute",
    bottom: 10,
    flexDirection: "row",
  },
  track: {
    backgroundColor: "white",
    overflow: "hidden",
    height: 2,
  },
  bar: {
    backgroundColor: "gold",
    height: 2,
    position: "absolute",
    left: 0,
    top: 0,
  },
});
function mapStateToProps(state) {
  return {
    homePage: state.homePage,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    setCart: cart => dispatch(setCart(cart)),
    showToast: (msg, dur) => dispatch(showToast(msg, dur)),
  };
}
export default connect(mapStateToProps, mapDispatchToProps, null, { withRef: true })(FirsatWidget);
