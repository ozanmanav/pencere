import {
  FETCH_HOME_PRODUCTS,
  FETCH_HOME_PRODUCTS_SUCCESS,
  FETCH_HOME_PRODUCTS_FAILURE,
  REFRESH_HOME_PRODUCTS,
  FETCH_ALL_PRODUCTS,
  FETCH_ALL_PRODUCTS_SUCCESS,
  FETCH_ALL_PRODUCTS_FAILURE,
  REFRESH_ALL_PRODUCTS,
  CLEAR_ALL_PRODUCTS,
} from "@redux/actions/types";

const initialState = {
  homePageProducts: [],
  seeAllProducts: [],
  isFetchingHomeProducts: false,
  isRefreshingHomeProducts: false,
  isFetchingAllProducts: false,
  isRefreshingAllProducts: false,
  error: false,
};

export default function productsReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_HOME_PRODUCTS:
      return {
        ...state,
        isFetchingHomeProducts: true,
      };
    case FETCH_ALL_PRODUCTS:
      return {
        ...state,
        isFetchingAllProducts: true,
      };
    case REFRESH_HOME_PRODUCTS:
      return {
        ...state,
        isRefreshingHomeProducts: true,
      };
    case CLEAR_ALL_PRODUCTS:
      return {
        ...state,
        seeAllProducts: [],
      };
    case REFRESH_ALL_PRODUCTS:
      return {
        ...state,
        isRefreshingAllProducts: true,
      };
    case FETCH_ALL_PRODUCTS_SUCCESS:
      return {
        ...state,
        isFetchingAllProducts: false,
        isRefreshingAllProducts: false,
        seeAllProducts:
          action.refresh || action.clear ? action.data : state.seeAllProducts.concat(action.data),
      };
    case FETCH_HOME_PRODUCTS_SUCCESS:
      return {
        ...state,
        isFetchingHomeProducts: false,
        isRefreshingHomeProducts: false,
        homePageProducts: action.refresh ? action.data : state.homePageProducts.concat(action.data),
      };
    case FETCH_HOME_PRODUCTS_FAILURE:
      return {
        ...state,
        isFetchingHomeProducts: false,
        isRefreshingHomeProducts: false,
        error: true,
      };
    case FETCH_ALL_PRODUCTS_FAILURE:
      return {
        ...state,
        isFetchingAllProducts: false,
        isRefreshingAllProducts: false,
        error: true,
      };
    default:
      return state;
  }
}
