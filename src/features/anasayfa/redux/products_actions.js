import {
  FETCH_HOME_PRODUCTS,
  FETCH_HOME_PRODUCTS_SUCCESS,
  FETCH_HOME_PRODUCTS_FAILURE,
  REFRESH_HOME_PRODUCTS,
  FETCH_ALL_PRODUCTS,
  FETCH_ALL_PRODUCTS_SUCCESS,
  REFRESH_ALL_PRODUCTS,
  CLEAR_ALL_PRODUCTS,
} from "@redux/actions/types";
import { SEARCH_URL, CATEGORIES_URL } from "@common/Constants";
import bugsnag from "@common/bugsnag_config";

export function initialHomePageProducts(refresh = false) {
  return async (dispatch, getState) => {
    if (refresh) dispatch({ type: REFRESH_HOME_PRODUCTS });
    else dispatch({ type: FETCH_HOME_PRODUCTS });
    var cats = getState().category.items;
    if (getState().category.items.length === 0) {
      cats = await fetch(CATEGORIES_URL);
      cats = await cats.json();
    }
    let products = [];
    for (var i = 0; i < 5; i++) {
      dispatch({ type: FETCH_HOME_PRODUCTS });
      let prepared_url =
        SEARCH_URL +
        "?YA=1" +
        "&Hiyerarsi=" +
        cats[i].Hid +
        "&Marka=" +
        "&SearchText" +
        "&Si=" +
        getState().session.info.SessionID;
      let prods = await fetch(prepared_url);
      let json = await prods.json();
      let product = {};
      product.cat_id = cats[i].Hid;
      product.cat_title = cats[i].Htxt;
      product.items = json.IlUrunListesi;

      products.push(product);
      dispatch(getHomeProductsSuccess(products, refresh));
    }
  };
}

export function fetchHomePageProductsFromApi(refresh = false, nextCat) {
  return async (dispatch, getState) => {
    if (refresh) dispatch({ type: REFRESH_HOME_PRODUCTS });
    else dispatch({ type: FETCH_HOME_PRODUCTS });

    let prepared_url =
      SEARCH_URL +
      "?YA=1" +
      "&Hiyerarsi=" +
      nextCat.Hid +
      "&Marka=" +
      "&SearchText" +
      "&Si=" +
      getState().session.info.SessionID;
    let prods = await fetch(prepared_url);
    let json = await prods.json();
    let products = {};

    products.cat_id = nextCat.Hid;
    products.cat_title = nextCat.Htxt;
    products.items = json.IlUrunListesi;
    dispatch(getHomeProductsSuccess(products, refresh));
  };
}

export function fetchSeeAllProductsFromApi(page = 0, category, refresh = false, clear = false) {
  return async (dispatch, getState) => {
    if (refresh) dispatch({ type: REFRESH_ALL_PRODUCTS });
    else dispatch({ type: FETCH_ALL_PRODUCTS });

    var prepared_url =
      SEARCH_URL +
      "?Si=" +
      getState().session.info.SessionID +
      "&YA=1" +
      "&Hiyerarsi=" +
      category +
      "&Sn=" +
      page;
    await fetch(prepared_url)
      .then((response) => response.json())
      .then((responseJson) => {
        dispatch(getAllProductsSuccess(responseJson.IlUrunListesi, refresh, clear));
      })
      .catch((error) => {
        bugsnag.notify(error, (report) => {
          report.errorClass = "fetchSeeAllProductsFromApi";
        });
        dispatch(getProductsFailure());
      });
  };
}

export function clearAllProducts() {
  return {
    type: CLEAR_ALL_PRODUCTS,
  };
}
function getHomeProductsSuccess(data, refresh) {
  return {
    type: FETCH_HOME_PRODUCTS_SUCCESS,
    data,
    refresh,
  };
}

function getAllProductsSuccess(data, refresh, clear) {
  return {
    type: FETCH_ALL_PRODUCTS_SUCCESS,
    data,
    refresh,
    clear,
  };
}

function getProductsFailure() {
  return {
    type: FETCH_HOME_PRODUCTS_FAILURE,
  };
}
