import {
  GET_HOME_PAGE,
  REFRESH_HOMEPAGE,
  GET_HOME_PAGE_SUCCESS,
  GET_HOME_PAGE_FAILURE,
  SET_DIRTY,
} from "@redux/actions/types";
import bugsnag from "@common/bugsnag_config";
import Euromsg from "react-native-euromsg";
import { getEndPoint } from "@common/Utils";
import axios from "axios";
import { Api } from "@common/api";
import endpoints from "@common/endpoints";

export function getHomePage(hideLoading, refreshing) {
  return (dispatch, getState) => {
    return new Promise(function (resolve, reject) {
      let preparedUrl = endpoints.HOMEPAGE_URL + "?Si=" + getState().session.info.SessionID;
      if (!hideLoading) dispatch({ type: GET_HOME_PAGE });
      if (refreshing) dispatch({ type: REFRESH_HOMEPAGE });
      Api.instance
        .get(preparedUrl)
        .then((responseJson) => {
          if (responseJson.data) {
            dispatch({
              type: GET_HOME_PAGE_SUCCESS,
              data: responseJson.data,
            });
            if (responseJson.data?.KisiselAyarlar?.MobilePushAl !== undefined)
              Euromsg.setPermit(responseJson.data?.KisiselAyarlar?.MobilePushAl);
            resolve(responseJson.data);
          }
        })
        .catch((err) => {
          bugsnag.notify(err, (report) => {
            report.errorClass = "getHomePage";
          });
          console.log(err);
          dispatch({ type: GET_HOME_PAGE_FAILURE });
          reject(err);
        });
    });
  };
}

export function setDirty(dirty) {
  console.log("set dirty action", dirty);
  return (dispatch) => {
    dispatch({ type: SET_DIRTY, dirty });
  };
}
