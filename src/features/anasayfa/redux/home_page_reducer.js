const INITAL_STATE = {
  isFetchingHomePage: false,
  isRefreshing: false,
  info: {},
  dirty: false,
  error: false,
};
import {
  GET_HOME_PAGE,
  REFRESH_HOMEPAGE,
  GET_HOME_PAGE_SUCCESS,
  GET_HOME_PAGE_FAILURE,
  SET_DIRTY,
} from "@redux/actions/types";

export default function homePageReducer(state = INITAL_STATE, action) {
  switch (action.type) {
    case GET_HOME_PAGE:
      return {
        ...state,
        isFetchingHomePage: true,
        error: false,
      };
    case REFRESH_HOMEPAGE:
      return {
        ...state,
        isRefreshing: true,
        error: false,
      };
    case GET_HOME_PAGE_SUCCESS:
      return {
        ...state,
        info: action.data,
        isFetchingHomePage: false,
        isRefreshing: false,
        error: false,
      };
    case GET_HOME_PAGE_FAILURE:
      return {
        ...state,
        info: {},
        isFetchingHomePage: false,
        isRefreshing: false,
        error: true,
      };
    case SET_DIRTY:
      return {
        ...state,
        dirty: action.dirty,
      };
    default:
      return state;
  }
}
