import React, { Component } from "react";
import {
  View,
  Animated,
  Dimensions,
  ActivityIndicator,
  Image,
  Platform,
  TouchableOpacity,
  Alert,
  LayoutAnimation,
  RefreshControl,
  Linking,
} from "react-native";

import {
  AnimatedHeader,
  CarouselContainer,
  SwiperProductThumb,
  FirsatProductThumb,
  Swiper,
  Text,
  TabBarIcon,
  SmartImage,
  AnimatedBasePage,
} from "@components";

import {
  FirsatWidget,
  CategoriesWidgetTop,
  CategoriesWidgetBottom,
} from "@features/anasayfa/components";

import colors from "@common/styles/colors";
import { connect } from "react-redux";
import {
  fetchHomePageProductsFromApi,
  fetchCampaignsFromApi,
  initialHomePageProducts,
  getBucket,
  getHomePage,
  fetchAnnouncementsFromApi,
  fetchCategoriesFromApi,
  filterSearch,
  setLiveChatVisibility,
  sendReadInfo,
} from "@redux/actions";

import QuickActions from "react-native-quick-actions";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import { ScaledSheet } from "react-native-size-matters";
import { moderateScale } from "react-native-size-matters";
import { handleNotification, handleUrl } from "@common/Utils";
import { UPDATE_SPRINGY } from "@common/LayoutAnimations";
import Interactable from "react-native-interactable";
import Euromsg from "react-native-euromsg";

const win = Dimensions.get("window");
const NAVBAR_HEIGHT = AnimatedHeader.height;
const STATUS_BAR_HEIGHT = 0;

var { height, width } = Dimensions.get("window");

const marginHorizontal = win.width / 25;

class HomeScreen extends Component {
  nextCat = 5;
  static navigationOptions = ({ navigation }) => ({
    header: null,
    tabBarIcon: ({ tintColor }) => (
      <TabBarIcon
        navigation={navigation}
        style={{ width: 20, height: 20 }}
        icon={require("@assets/images/home.png")}
        label="Anasayfa"
        tintColor={tintColor}
      />
    ),
  });

  constructor(props) {
    super(props);
    this.state = {
      showAsModal: false,
      hideAnnouncement: false,
      products: [],
      slider1ActiveSlide: 0,
    };
  }

  componentDidMount() {
    this.props.setLiveChatVisibility(true);
    Euromsg.configUser({
      email: this.props.session.info.Email,
      userKey: this.props.session.info.BayiKodu + "_" + this.props.session.info.KullaniciKodu,
    });
    Euromsg.getInitialNotification().then(notif => {
      console.log("initial notif", notif);
      if (
        notif?.aps?.alert &&
        (notif.aps.alert.body.toLowerCase().includes("fiyat talebi") ||
          notif.aps.alert.body.toLowerCase().includes("fiyat teklifi") ||
          notif.aps.alert.title.toLowerCase().includes("fiyat talebi") ||
          notif.aps.alert.title.toLowerCase().includes("fiyat teklifi"))
      ) {
        this.props.navigation.navigate("PriceRequestsScreen");
      } else if (Platform.OS === "android") handleUrl(notif.url);
      else handleNotification({ data: notif });
    });
    Euromsg.addListener("opened", notif => {
      console.log("notif opened", notif);
      if (Platform.OS === "android" && notif?.pushId) Euromsg.reportRead(notif.pushId);
      if (Platform.OS === "android") handleUrl(notif.url);
      else handleNotification({ data: notif });
    });
    this.getInitialData();
    Linking.getInitialURL()
      .then(url => {
        if (url != null) {
          console.log("Initial url is: " + url);
          this.navigate(url);
        }
      })
      .catch(err => console.error("An error occurred", err));
    this.didFocusSubscription = this.props.navigation.addListener("didFocus", () => {
      this.props.getHomePage(typeof this.props.homePage.info.Mesaj !== "undefined");
      this.props.getAnnouncements();
    });

    QuickActions.popInitialAction()
      .then(data => {
        if (data && data.type === "Orders") {
          this.props.navigation.navigate({
            key: "OrdersScreen",
            routeName: "OrdersScreen",
          });
        } else if (data && data.type === "Account") {
          this.props.navigation.navigate({
            key: "HesapEkstresiScreen",
            routeName: "HesapEkstresiScreen",
          });
        } else if (data && data.type === "Search") {
          this.props.navigation.navigate({
            key: "SearchScreen",
            routeName: "SearchScreen",
            params: { focus: true },
          });
        }
      })
      .catch(console.error);
  }

  handleOpenURL = event => {
    if (event != null && event.url != null) {
      console.log("navigating", event.url);
      this.navigate(event.url);
    }
  };

  getInitialData = async () => {
    await this.props.getHomePage(false, false);
    await this.props.getCategories();
    this.props.getAnnouncements();
    this.props.getBucket();
  };

  navigate = url => {
    const routerUrl = url.split("//")[1];
    const splittedRouterUrl = routerUrl.split("/");
    const routeName = splittedRouterUrl[1];
    const params = {};
    if (routeName === "detay") {
      if (splittedRouterUrl.length > 1) {
        params.UrunKodu = splittedRouterUrl[2];
      }
    } else if (routeName === "ara") {
      if (splittedRouterUrl.length > 0) {
        const urlParams = splittedRouterUrl[2].split(",");
        console.log({ urlParams });
        urlParams.forEach(param => {
          const splittedParam = param.split(":");
          if (splittedParam.length > 0) {
            const key = splittedParam[0];
            const value = splittedParam[1];
            params[key] = value;
          }
        });
      }
    }
    console.log("navigating", routeName, params);
    if (routeName === "detay") {
      this.props.navigation.navigate({
        key: "ProductDetailsScreen",
        routeName: "ProductDetailsScreen",
        params: { ...params },
      });
    } else if (routeName === "ara") {
      this.props.navigation.navigate({
        key: "SearchScreen",
        routeName: "SearchScreen",
        params: { searchParams: { ...params }, focus: false, doSearch: true },
      });
    } else if (routeName === "fiyatTaleplerim") {
      this.props.navigation.navigate("PriceRequestsScreen");
    }
  };

  onReceived(notification) {
    console.log("Notification received: ", notification);
  }

  onOpened(openResult) {
    console.log("Message: ", openResult.notification.payload.body);
    console.log("Data: ", openResult.notification.payload.additionalData);
    console.log("isActive: ", openResult.notification.isAppInFocus);
    console.log("openResult: ", openResult);

    if (openResult.notification.payload.additionalData.UrunKodu) {
      var productCode = openResult.notification.payload.additionalData.UrunKodu;
      console.log("productCode", productCode, this.props);
      this.props.navigation.navigate({
        key: "home",
        routeName: "ProductDetailsScreen",
        params: { UrunKodu: productCode },
      });
    }
  }

  onIds(device) {
    console.log("Device info: ", device);
  }

  scrollToTop = () => {
    this.basePage.flatlist.scrollTo({ x: 0, y: 0, animated: true });
  };

  onRefresh = () => {
    this.props.getHomePage(true, true);
  };

  reachedEnd() {
    if (
      this.props.category.items.length > 0 &&
      this.props.products.homePageProducts.length > 4 &&
      this.nextCat < this.props.category.items.length
    ) {
      this.props.getHomePageProducts(false, this.props.category.items[this.nextCat]);
      this.nextCat++;
    }
  }

  kampanyaOnPress(item) {
    if (item.Urunkodu && item.Urunkodu !== "") {
      this.props.navigation.navigate("ProductDetailsScreen", {
        UrunKodu: item.Urunkodu,
      });
    } else if (item.Hiyerarsi && item.Hiyerarsi !== "") {
      this.props.navigation.navigate({
        key: "SearchScreen",
        routeName: "SearchScreen",
        params: {
          searchParams: { Hiyerarsi: item.Hiyerarsi, SearchText: "" },
          focus: false,
          doSearch: true,
        },
      });
    }
  }

  bannerOnPress(item) {
    console.log("bbb", item);
    if (item.AraSayfaUrl !== undefined && item.AraSayfaUrl !== "") {
      // if arasayfa
      this.props.navigation.navigate({
        routeName: "AraSayfaModal",
        params: { item: item },
      });
    } else {
      if (item.UrunKodu && item.UrunKodu !== "") {
        this.props.navigation.navigate({
          key: "main",
          routeName: "ProductDetailsScreen",
          params: { UrunKodu: item.UrunKodu },
        });
      } else if (item.Hiyerarsi !== "" || item.Marka !== "" || item.SearchText !== "") {
        this.props.navigation.navigate({
          key: "SearchScreen",
          routeName: "SearchScreen",
          params: { searchParams: item, focus: false, doSearch: true },
        });
      }
    }
  }

  renderHeader = () => {
    if (this.props.homePage.info && !this.props.homePage.error)
      return (
        <View
          style={{
            flexGrow: 1,
            minHeight: win.height * 0.6,
            width: win.width,
            alignItems: "center",
            justifyContent: "center",
            flexDirection: "column",
            flex: 1,
          }}
        >
          {this.props.homePage.info.Banner && (
            <CarouselContainer
              items={this.props.homePage.info.Banner}
              imageExtractor={item => item.URL}
              navigation={this.props.navigation}
              hasTitle={false}
              bannerOnPress={item => this.bannerOnPress(item)}
            />
          )}
          {!this.state.hideAnnouncement &&
            typeof this.props.homePage.info.Mesaj !== "undefined" &&
            this.props.announcements.lastUnreadAnnouncement && (
              <Interactable.View
                horizontalOnly={true}
                snapPoints={[
                  { x: win.width - marginHorizontal * 2 },
                  { x: 0, damping: 0.65, tension: 500, haptics: true },
                  { x: -win.width - marginHorizontal * 2 },
                ]}
                onSnapStart={({ nativeEvent }) => {
                  nativeEvent.index !== 1 && this.setState({ hideAnnouncement: true });
                  this.props.sendReadInfo(
                    this.props.announcements.lastUnreadAnnouncement.BildirimId,
                  );
                  LayoutAnimation.configureNext(UPDATE_SPRINGY);
                }}
                onSnap={({ nativeEvent }) => {
                  nativeEvent.index !== 1 && this.setState({ hideAnnouncement: true });
                  this.props.sendReadInfo(
                    this.props.announcements.lastUnreadAnnouncement.BildirimId,
                  );
                  LayoutAnimation.configureNext(UPDATE_SPRINGY);
                }}
              >
                <TouchableOpacity
                  onPress={() => {
                    this.props.sendReadInfo(
                      this.props.announcements.lastUnreadAnnouncement.BildirimId,
                    );
                    this.props.navigation.navigate({
                      routeName: "AnnouncementDetailsScreen",
                      params: {
                        item: this.props.announcements.lastUnreadAnnouncement,
                        origin: "HomeScreen",
                      },
                    });
                  }}
                  style={styles.prdWidget}
                >
                  <SmartImage
                    source={{
                      uri: this.props.announcements.lastUnreadAnnouncement.GorselUrl,
                    }}
                    defaultImageStyle={{
                      padding: 0,
                      width: moderateScale(28, 0.5),
                      height: moderateScale(28, 0.5),
                    }}
                    style={{
                      resizeMode: "contain",
                      flex: 3,
                      height: moderateScale(36, 0.5),
                    }}
                  />
                  <View
                    style={{
                      flexDirection: "row",
                      flex: 15,
                      marginRight: moderateScale(10, 0.5),
                    }}
                  >
                    <Text
                      numberOfLines={2}
                      ellipsizeMode="tail"
                      fontScale={0.8}
                      style={{
                        fontFamily: "Poppins-Medium",
                        fontSize: 12,
                        flex: 1,
                        marginLeft: moderateScale(5, 0.5),
                      }}
                    >
                      {this.props.announcements.lastUnreadAnnouncement.Icerik}
                    </Text>
                  </View>
                  <TouchableOpacity
                    style={{ marginRight: moderateScale(10, 0.5) }}
                    onPress={() => {
                      this.setState({ hideAnnouncement: true });
                      this.props.sendReadInfo(
                        this.props.announcements.lastUnreadAnnouncement.BildirimId,
                      );
                      LayoutAnimation.configureNext(UPDATE_SPRINGY);
                    }}
                  >
                    <Image
                      source={require("@assets/images/close.png")}
                      style={{
                        width: moderateScale(18, 0.5),
                        height: moderateScale(18, 0.5),
                        tintColor: "#adadad",
                      }}
                    />
                  </TouchableOpacity>
                </TouchableOpacity>
              </Interactable.View>
            )}
          {this.props.homePage.info.WidgetUrun != null &&
            this.props.homePage.info.WidgetUrun?.length > 0 &&
            this.props.homePage.info.WidgetUrun.map(item => {
              if (item.UrunResimThumbnail != null)
                return (
                  <View key={item.UrunKodu} style={styles.prdWidget}>
                    <Image
                      source={{
                        uri: item.UrunResimThumbnail,
                      }}
                      style={{
                        resizeMode: "contain",
                        flex: 3,
                        height: moderateScale(36, 0.5),
                      }}
                    />
                    <Text
                      numberOfLines={2}
                      ellipsizeMode="tail"
                      style={{
                        fontFamily: "Poppins-SemiBold",
                        fontSize: 11,
                        flex: 10,
                        flexWrap: "wrap",
                        overflow: "hidden",
                        color: "#5eb139",
                      }}
                    >
                      {item.Aciklama}
                    </Text>
                    <TouchableOpacity
                      onPress={() =>
                        this.props.navigation.navigate({
                          key: "home",
                          routeName: "ProductDetailsScreen",
                          params: {
                            UrunKodu: item.UrunKodu,
                            SessionID: this.props.session.info.SessionID,
                          },
                        })
                      }
                      style={{
                        backgroundColor: "#439ef3",
                        paddingHorizontal: 2,
                        justifyContent: "center",
                        alignItems: "center",
                        flex: 5,
                        height: moderateScale(30, 0.5),
                        marginHorizontal: moderateScale(8, 0.5),
                        borderRadius: 2,
                        overflow: "hidden",
                      }}
                    >
                      <Text
                        fontScale={0.8}
                        style={{
                          fontFamily: "Poppins-SemiBold",
                          fontSize: 10,
                          color: "white",
                        }}
                      >
                        Hemen İncele
                      </Text>
                    </TouchableOpacity>
                  </View>
                );
            })}
          {this.props.homePage.info.FirsatSaati &&
            this.props.homePage.info.FirsatSaati.length > 0 && (
              <FirsatWidget
                session={this.props.session}
                ref={c => (this.FirsatWidget = c)}
                items={this.props.homePage.info.FirsatSaati}
                navigation={this.props.navigation}
              />
            )}

          {this.props.homePage.info.altBanner &&
            typeof this.props.homePage.info.altBanner.URL !== "undefined" &&
            this.props.homePage.info.altBanner.URL !== "" && (
              <TouchableOpacity
                style={{
                  marginHorizontal: win.width / 25,
                  borderRadius: 3,
                  overflow: "hidden",
                  marginBottom: 15,
                }}
                onPress={() =>
                  Alert.alert(
                    "Geçici mesaj",
                    "Şu an için servisten yönlendirilebilecek bir ürün veya arama sorgusu dönmüyor.",
                  )
                }
              >
                <Image
                  source={{ uri: this.props.homePage.info.altBanner.URL }}
                  style={{
                    resizeMode: "contain",
                    width: win.width - (win.width / 25) * 2,
                    height: (150 / 760) * (win.width - (win.width / 25) * 2),
                  }}
                />
              </TouchableOpacity>
            )}

          {this.props.homePage.info.TopViewedProducts &&
            this.props.homePage.info.TopViewedProducts.urunler.length > 0 && (
              <View
                style={[
                  {
                    flex: 1,
                    marginBottom: moderateScale(50, 0.5),
                    marginLeft: marginHorizontal,
                    marginTop: moderateScale(5, 0.5),
                  },
                ]}
              >
                <Text
                  style={{
                    color: "black",
                    fontSize: 16,
                    fontFamily: "Poppins-Medium",
                    marginBottom: moderateScale(5, 0.5),
                  }}
                >
                  {this.props.homePage.info.TopViewedProducts.header}
                </Text>
                <Swiper>
                  {this.props.homePage.info.TopViewedProducts.urunler.map((item, idx) => {
                    return (
                      <View
                        key={item.Code}
                        style={{
                          flexDirection: "row",
                          paddingLeft: marginHorizontal,
                        }}
                      >
                        <SwiperProductThumb
                          onPress={() =>
                            this.props.navigation.navigate({
                              key: "home",
                              routeName: "ProductDetailsScreen",
                              params: {
                                UrunKodu: item.Code,
                                SessionID: this.props.session.info.SessionID,
                              },
                            })
                          }
                          key={idx}
                          {...item}
                        />
                        {idx < this.props.homePage.info.TopViewedProducts.urunler.length - 1 && (
                          <View
                            style={{
                              width: 1,
                              backgroundColor: "#dedede",
                              marginHorizontal: moderateScale(10, 0.5),
                            }}
                          />
                        )}
                      </View>
                    );
                  })}
                </Swiper>
              </View>
            )}

          {typeof this.props.homePage.info.Mesaj !== "undefined" && (
            <TouchableOpacity
              style={{ marginVertical: moderateScale(8, 0.5) }}
              onPress={() =>
                this.props.navigation.navigate({
                  key: "SearchScreen",
                  routeName: "SearchScreen",
                  params: {
                    searchParams: { SearchText: "firsat" },
                    focus: false,
                    reload: true,
                  },
                })
              }
            >
              <Image
                source={require("@assets/images/firsat_banner.png")}
                style={{
                  width: win.width - marginHorizontal * 2,
                  height: (win.width - marginHorizontal * 2) * 0.47,
                }}
              />
            </TouchableOpacity>
          )}

          {typeof this.props.homePage.info.Mesaj !== "undefined" && (
            <CategoriesWidgetTop navigation={this.props.navigation} />
          )}

          {this.props.homePage.info.YouViewed &&
            this.props.homePage.info.YouViewed.urunler.length > 0 && (
              <View
                style={[
                  {
                    flex: 1,
                    marginBottom: moderateScale(50, 0.5),
                    marginLeft: marginHorizontal,
                  },
                ]}
              >
                <Text
                  style={{
                    color: "black",
                    fontSize: 16,
                    fontFamily: "Poppins-Medium",
                    marginVertical: moderateScale(5, 0.5),
                  }}
                >
                  {this.props.homePage.info.YouViewed.header}
                </Text>
                <Swiper>
                  {this.props.homePage.info.YouViewed.urunler.map((item, idx) => {
                    return (
                      <View
                        key={item.UrunKodu ?? item.Code}
                        style={{
                          flexDirection: "row",
                          paddingLeft: marginHorizontal,
                        }}
                      >
                        <SwiperProductThumb
                          onPress={() =>
                            this.props.navigation.navigate({
                              key: "home",
                              routeName: "ProductDetailsScreen",
                              params: {
                                UrunKodu: item.UrunKodu || item.Code,
                                SessionID: this.props.session.info.SessionID,
                              },
                            })
                          }
                          key={idx}
                          {...item}
                        />
                        {idx < this.props.homePage.info.YouViewed.urunler.length - 1 && (
                          <View
                            style={{
                              width: 1,
                              backgroundColor: "#dedede",
                              marginHorizontal: moderateScale(10, 0.5),
                            }}
                          />
                        )}
                      </View>
                    );
                  })}
                </Swiper>
              </View>
            )}
          {typeof this.props.homePage.info.Mesaj !== "undefined" && (
            <TouchableOpacity
              style={{ marginVertical: moderateScale(8, 0.5) }}
              onPress={() => {
                this.props.navigation.navigate({
                  key: "SearchScreen",
                  routeName: "SearchScreen",
                  params: {
                    searchParams: { SearchText: "" },
                    focus: false,
                    reload: true,
                    banner50: true,
                  },
                });
                this.props.filterSearch(
                  { FiltreTipi: "EFiyat", MinFiyat: "0", MaxFiyat: "50" },
                  true,
                );
              }}
            >
              <Image
                source={require("@assets/images/50_banner.png")}
                style={{
                  width: win.width - marginHorizontal * 2,
                  height: (win.width - marginHorizontal * 2) * 0.47,
                }}
              />
            </TouchableOpacity>
          )}

          {typeof this.props.homePage.info.Mesaj !== "undefined" && (
            <CategoriesWidgetBottom navigation={this.props.navigation} />
          )}
          {typeof this.props.homePage.info.Mesaj !== "undefined" && (
            <TouchableOpacity
              style={{ marginVertical: moderateScale(8, 0.5) }}
              onPress={() =>
                this.props.navigation.navigate({
                  key: "SearchScreen",
                  routeName: "SearchScreen",
                  params: {
                    searchParams: { SearchText: "defolu ürünler" },
                    focus: false,
                    reload: true,
                  },
                })
              }
            >
              <Image
                source={require("@assets/images/defo_banner.png")}
                style={{
                  width: win.width - marginHorizontal * 2,
                  height: (win.width - marginHorizontal * 2) * 0.47,
                }}
              />
            </TouchableOpacity>
          )}
          {this.props.homePage.info.Firsatlar && this.props.homePage.info.Firsatlar.length > 0 && (
            <View
              style={[
                {
                  flex: 1,
                  marginBottom: moderateScale(50, 0.5),
                  marginLeft: marginHorizontal,
                },
              ]}
            >
              <Text
                style={{
                  color: "black",
                  fontSize: 16,
                  fontFamily: "Poppins-Medium",
                  marginVertical: moderateScale(5, 0.5),
                }}
              >
                Fırsatlar
              </Text>
              <Swiper>
                {this.props.homePage.info.Firsatlar.map((item, idx) => {
                  return (
                    <View
                      key={item.UrunKodu}
                      style={{
                        flexDirection: "row",
                        paddingLeft: marginHorizontal,
                        justifyContent: "center",
                      }}
                    >
                      <FirsatProductThumb
                        onPress={() =>
                          this.props.navigation.navigate({
                            key: "home",
                            routeName: "ProductDetailsScreen",
                            params: { UrunKodu: item.UrunKodu },
                          })
                        }
                        key={idx}
                        item={item}
                      />
                      {idx < this.props.homePage.info.Firsatlar.length - 1 && (
                        <View
                          style={{
                            width: 1,
                            backgroundColor: "#dedede",
                            marginHorizontal: moderateScale(10, 0.5),
                          }}
                        />
                      )}
                    </View>
                  );
                })}
              </Swiper>
            </View>
          )}
        </View>
      );
    else return null;
  };

  renderFooter() {
    if (this.props.homePage.isFetchingHomeProducts)
      return (
        <View style={[styles.loadingContainer]}>
          <ActivityIndicator size="large" color="#0000ff" />
        </View>
      );
    else return null;
  }

  render() {
    return (
      <AnimatedBasePage
        ref={c => (this.basePage = c)}
        style={{ backgroundColor: colors.primary_dark, flex: 1 }}
        showFlatList={!this.props.homePage.error}
        navigation={this.props.navigation}
        headerProps={{
          back: false,
          alignLeft: false,
          searchBar: true,
          search: false,
        }}
        isLoading={this.props.homePage.isFetchingHomePage}
        flatListProps={{
          refreshControl: (
            <RefreshControl
              refreshing={this.props.homePage.isRefreshing}
              onRefresh={this.onRefresh}
              progressViewOffset={130}
            />
          ),
          ListHeaderComponent: this.renderHeader,
          contentContainerStyle: {
            paddingBottom: this.state.keyboardVisible ? this.state.keyboardPadding : 0,
          },
        }}
      >
        {this.props.homePage.error && (
          <View
            style={{
              flexDirection: "column",
              width: win.width,
              marginTop: AnimatedHeader.height,
              height: AnimatedHeader.height,
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <Text style={{ fontFamily: "Poppins", fontSize: 14 }}>
              Anasayfa yüklenirken bir problem oluştu.
            </Text>
            <TouchableOpacity
              onPress={() => this.props.getHomePage()}
              style={{ flexDirection: "row", alignItems: "center" }}
            >
              <MaterialIcons size={14} name="refresh" color="blue" />
              <Text style={{ fontFamily: "Poppins", fontSize: 14, color: "blue" }}>
                Yeniden Yükle
              </Text>
            </TouchableOpacity>
          </View>
        )}
      </AnimatedBasePage>
    );
  }

  _pressSeeAllProducts(cat, title) {
    this.props.navigation.navigate({
      key: "tabsGrid",
      routeName: "GridScreen",
      params: {
        SessionID: this.props.session.info.SessionID,
        category: cat,
        title: title,
        navigation: this.props.navigation,
      },
    });
  }
}

function mapStateToProps(state) {
  return {
    session: state.session,
    products: state.products,
    category: state.category,
    homePage: state.homePage,
    announcements: state.announcements,
    cart: state.cart,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    initialHomePageProducts: refresh => dispatch(initialHomePageProducts(refresh)),
    getHomePageProducts: (refresh, nextCat) =>
      dispatch(fetchHomePageProductsFromApi(refresh, nextCat)),
    getCampaigns: sid => dispatch(fetchCampaignsFromApi(sid)),
    getBucket: refresh => dispatch(getBucket(refresh)),
    getHomePage: (hideLoading, refreshing) => dispatch(getHomePage(hideLoading, refreshing)),
    getCategories: () => dispatch(fetchCategoriesFromApi()),
    getAnnouncements: () => dispatch(fetchAnnouncementsFromApi()),
    filterSearch: (params, ya) => dispatch(filterSearch(params, ya)),
    setLiveChatVisibility: isVisible => dispatch(setLiveChatVisibility(isVisible)),
    sendReadInfo: b => dispatch(sendReadInfo(b)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);

const styles = ScaledSheet.create({
  icon: {
    width: "26@s",
    height: "26@s",
  },
  msgWidget: {
    flexDirection: "row",
    marginTop: 15,
    alignItems: "center",
    justifyContent: "flex-start",
    width: win.width - (win.width / 25) * 2,
    height: "50@ms0.5",
    marginHorizontal: win.width / 25,
    paddingHorizontal: moderateScale(5, 0.5),
    borderRadius: 3,
    overflow: "hidden",
    backgroundColor: "white",
    borderColor: "#d0d0d0",
    borderWidth: 1,
    flexWrap: "wrap",
  },
  prdWidget: {
    flexDirection: "row",
    marginTop: moderateScale(15, 0.5),
    alignItems: "center",
    justifyContent: "flex-start",
    width: win.width - (win.width / 25) * 2,
    height: moderateScale(50, 0.5),
    marginHorizontal: win.width / 25,
    borderRadius: 3,
    overflow: "hidden",
    backgroundColor: "white",
    borderColor: "#d0d0d0",
    borderWidth: 1,
    paddingHorizontal: moderateScale(5, 0.5),
  },
  container: {
    width: width,
    height: height - 90,
    alignItems: "center",
    justifyContent: "center",
    position: "absolute",
    top: 50,
    backgroundColor: "rgba(0,0,0,0.5)",
  },
  loadingContainer: {
    height: win.height,
    width: win.width,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0,0,0,0.3)",
    position: "absolute",
  },
  indicator: {
    height: win.height - 50,
    width: win.width,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0,0,0,0.5)",
    position: "absolute",
    top: 0,
    left: 0,
  },
  carouselTitle: {
    padding: 10,
  },
  header: {
    backgroundColor: colors.bg_header,
  },
  headerSub: {
    padding: 15,
    flexDirection: "row",
    justifyContent: "center",
  },
  logo: {
    width: 64,
    height: 28,
    resizeMode: "center",
  },
  icoSearch: {
    color: colors.txt_description,
    marginRight: 5,
  },
  btnSearchHolder: {
    padding: 15,
    paddingTop: 0,
  },
  btnSearch: {
    borderColor: colors.bd_input,
    justifyContent: "center",
    flexDirection: "row",
    padding: 8,
    backgroundColor: colors.bg1,
    borderWidth: 1,
    borderRadius: 5,
  },
  btnSearchTitle: {
    color: colors.txt_description,
    fontSize: 16,
  },
  btnDeals: {
    flexDirection: "row",
    justifyContent: "center",
    flex: 0.5,
  },
  icoDeals: {
    color: colors.txt_description,
    marginRight: 10,
  },
  section_title: {
    fontSize: 18,
    fontWeight: "600",
    padding: 20,
  },
});
