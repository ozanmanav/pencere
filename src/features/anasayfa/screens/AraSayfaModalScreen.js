import React, { Component } from "react";
import { View, Image, Dimensions, StatusBar, TouchableOpacity } from "react-native";
import colors from "@common/styles/colors";
const win = Dimensions.get("window");

export default class AraSayfaModalScreen extends Component {
  render() {
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: "white",
          flexDirection: "column",
          justifyContent: "flex-start",
        }}
      >
        <StatusBar hidden={true} />
        <View
          style={{
            flexDirection: "row",
            justifyContent: "flex-end",
            alignItems: "center",
            backgroundColor: colors.primary_dark,
            height: 50,
          }}
        >
          <TouchableOpacity
            onPress={() => this.props.navigation.pop()}
            style={{
              paddingRight: 10,
            }}
          >
            <Image
              source={require("@assets/images/x.png")}
              style={{
                width: 20,
                height: 20,
                tintColor: "white",
                resizeMode: "contain",
              }}
            />
          </TouchableOpacity>
        </View>
        <TouchableOpacity
          onPress={() => {
            let item = this.props.navigation.state.params.item;
            if (item.UrunKodu && item.UrunKodu !== "") {
              this.props.navigation.navigate({
                key: "main",
                routeName: "ProductDetailsScreen",
                params: { UrunKodu: item.UrunKodu },
              });
            } else if (item.Hiyerarsi !== "" || item.Marka !== "" || item.SearchText !== "") {
              this.props.navigation.navigate({
                key: "SearchScreen",
                routeName: "SearchScreen",
                params: { searchParams: item, focus: false, doSearch: true },
              });
            }
          }}
        >
          <Image
            source={{
              uri: this.props.navigation.state.params.item.AraSayfaUrl,
            }}
            resizeMode="contain"
            style={{
              height: win.height - 50,
            }}
          />
        </TouchableOpacity>
      </View>
    );
  }
}
