import React, { Component } from "react";
import { DatePicker, Text } from "@components";
import { View, StyleSheet, Image, Animated, TouchableOpacity } from "react-native";
import { connect } from "react-redux";
import { searchOrders } from "@redux/actions";
import Icon from "react-native-vector-icons/FontAwesome";

class DatePanel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isCollapsed: false,
    };
  }

  render() {
    return (
      <Animated.View
        style={[
          this.props.style,
          {
            backgroundColor: "white",
            shadowColor: "#000",
            shadowOffset: { width: 0, height: 2 },
            shadowOpacity: 0.5,
            shadowRadius: 2,
            elevation: 1,
          },
        ]}
      >
        <View
          style={{
            flex: 1,
            alignItems: "center",
            justifyContent: "center",
            padding: 10,
            flexDirection: "row",
            borderBottomColor: "gray",
            borderBottomWidth: 1,
            backgroundColor: "white",
            marginBottom: 10,
          }}
        >
          <Text style={{ fontSize: 18, color: "gray" }}>Tarih Seçimi</Text>
          <TouchableOpacity
            style={{ width: 28, height: 28 }}
            onPress={() => this.props.closeDatePanel()}
          >
            <Icon name="times-circle" size={28} color="rgba(0,0,0,0.5)" />
          </TouchableOpacity>
        </View>
        <View style={styles.dateRow}>
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <Image source={require("@assets/images/calendar.png")} style={styles.icon} />
            <Text style={{ marginRight: 10, fontSize: 16 }}>Başlangıç Tarihi:</Text>
          </View>
          <DatePicker
            ref={(c) => (this.datePicker1 = c)}
            date={this.props.start}
            id="1"
            onPressDatePicker={this.onPressDatePicker}
            openDatePickerIOS={this.props.openDatePickerIOS}
            maximumDate={this.props.end}
            minimumDate={new Date(this.props.end.getFullYear, this.props.end.getMonth - 3, 1)}
          />
        </View>
        <View style={styles.dateRow}>
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <Image source={require("@assets/images/calendar.png")} style={styles.icon} />
            <Text style={{ marginRight: 10, fontSize: 16 }}>Bitiş Tarihi:</Text>
          </View>
          <DatePicker
            ref={(c) => (this.datePicker2 = c)}
            date={this.props.end}
            id="2"
            onPressDatePicker={this.onPressDatePicker}
            openDatePickerIOS={this.props.openDatePickerIOS}
            minimumDate={this.props.start}
            maximumDate={new Date()}
          />
        </View>
        {
          // <TouchableOpacity
          //     style={{ width: win.width, height: 30, flexDirection: 'row', justifyContent: 'center', paddingTop: 5, borderBottomColor: 'gray', borderBottomWidth: 1 }}
          //     onPress={() => this.props.toggleCollapse({ isCollapsed: !this.props.isCollapsed })}
          // >
          //     <Image source={this.props.isCollapsed?require('@assets/images/down-arrow.png'):require('@assets/images/up-arrow.png')} style={{ width: 20, height: 20, tintColor: 'gray', }} />
          //     <Text style={{ fontSize: 16, color: 'gray', marginHorizontal: 20 }}>{this.props.isCollapsed?"Tarih Seç":"Gizle"}</Text>
          //     <Image source={this.props.isCollapsed?require('@assets/images/down-arrow.png'):require('@assets/images/up-arrow.png')} style={{ width: 20, height: 20, tintColor: 'gray', }} />
          // </TouchableOpacity>
        }
      </Animated.View>
    );
  }
}

function mapStateToProps(state) {
  return {
    orders: state.orders,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    searchOrders: (startDate, endDate) => dispatch(searchOrders(startDate, endDate)),
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(DatePanel);

const styles = StyleSheet.create({
  dateRow: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    flex: 1,
    paddingHorizontal: 10,
    marginBottom: 10,
  },
  icon: {
    width: 24,
    height: 24,
    marginRight: 10,
    tintColor: "gray",
  },
});
