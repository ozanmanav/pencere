import React, { Component } from "react";
import { View, StyleSheet } from "react-native";
import { Text, Card } from "@components";
import { moderateScale } from "react-native-size-matters";
import { numberToLocaleString } from "@common/Utils";

const styles = StyleSheet.create({
  titleTxt: {
    fontFamily: "Poppins",
    fontSize: 14,
    color: "orange",
    marginBottom: 2,
    flex: 0.5,
  },
  valueTxt: {
    fontFamily: "Poppins",
    fontSize: 14,
    color: "black",
    marginBottom: 2,
    flex: 0.5,
  },
});

export class OrderComponent extends Component {
  render() {
    return (
      <Card
        onPress={() =>
          this.props.navigation.navigate({
            routeName: "OrderDetailsScreen",
            params: { sNo: this.props.item.SiparisNo },
          })
        }
        renderContent={() => {
          return (
            <View style={{ flexDirection: "row", padding: moderateScale(10, 0.5) }}>
              <View style={{ flex: 1, backgroundColor: "transparent", flexDirection: "column" }}>
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "space-between",
                  }}
                >
                  <Text fontScale={0.6} style={styles.titleTxt}>
                    {"Sipariş Numarası"}
                  </Text>
                  <Text fontScale={0.6} numberOfLines={1} style={styles.valueTxt}>
                    {this.props.item.SiparisNo}
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "space-between",
                  }}
                >
                  <Text fontScale={0.6} style={styles.titleTxt}>
                    {"Sepet Adı/Numarası"}
                  </Text>
                  <Text fontScale={0.6} numberOfLines={1} style={styles.valueTxt}>
                    {this.props.item.SiparisSASSepetNo}
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "space-between",
                  }}
                >
                  <Text fontScale={0.6} style={styles.titleTxt}>
                    {"Siparişi Veren Kişi"}
                  </Text>
                  <Text fontScale={0.6} numberOfLines={1} style={styles.valueTxt}>
                    {this.props.item.SiparisIlgiliKisiAdi}
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "space-between",
                  }}
                >
                  <Text fontScale={0.6} style={styles.titleTxt}>
                    {"Sipariş Tarihi"}
                  </Text>
                  <Text fontScale={0.6} numberOfLines={1} style={styles.valueTxt}>
                    {this.props.item.StrSiparisTarihi}
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "space-between",
                  }}
                >
                  <Text fontScale={0.6} style={styles.titleTxt}>
                    {"Sipariş Tutarı"}
                  </Text>
                  <Text fontScale={0.6} numberOfLines={1} style={styles.valueTxt}>
                    {numberToLocaleString(this.props.item.SiparisNetTutari) +
                      " " +
                      this.props.item.SiparisPb +
                      " + KDV"}
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "space-between",
                  }}
                >
                  <Text fontScale={0.6} style={styles.titleTxt}>
                    {"Sipariş Durumu"}
                  </Text>
                  <Text fontScale={0.6} numberOfLines={1} style={styles.valueTxt}>
                    {this.props.item.SiparisDurumMetni}
                  </Text>
                </View>
              </View>
            </View>
          );
        }}
      />
    );
  }
}

export default OrderComponent;
