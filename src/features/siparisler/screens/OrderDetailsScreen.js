import React, { Component } from "react";
import {
  SafeAreaView,
  Image,
  TouchableOpacity,
  Alert,
  View,
  ScrollView,
  Animated,
  Dimensions,
  Platform,
  StatusBar,
} from "react-native";
import { connect } from "react-redux";
import { getOrderDetails } from "@redux/actions";
import {
  AnimatedHeader,
  Text,
  WebViewModal,
  ListModal,
  RadioButton,
  PencereIndicator,
} from "@components";
import colors from "@common/styles/colors";
import { moderateScale, ScaledSheet } from "react-native-size-matters";
import { getEndPoint } from "@common/Utils";
import bugsnag from "@common/bugsnag_config";

const win = Dimensions.get("window");
const AnimatedScrollView = Animated.createAnimatedComponent(ScrollView);

class OrderDetailsScreen extends Component {
  constructor(props) {
    super(props);
    const scrollAnim = new Animated.Value(0);
    const offsetAnim = new Animated.Value(0);
    this.state = {
      isFetching: false,
      iadeNedeni: -1,
      scrollAnim,
      clampedScroll: Animated.diffClamp(
        Animated.add(
          scrollAnim.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 1],
            extrapolateLeft: "clamp",
          }),
          offsetAnim,
        ),
        0,
        AnimatedHeader.height,
      ),
    };
  }
  componentDidMount() {
    this.props.getOrderDetails(this.props.navigation.state.params.sNo);
    this.state.scrollAnim.addListener(({ value }) => {
      // This is the same calculations that diffClamp does.
      this.diff = value - this._scrollValue;
      this._scrollValue = value;
      // if (this.diff > 0 && value > (dim.width * 9 / 16 + 50))
      //     this.refs.fixedBtn.startFadeAnim(false);
      // else if (this.refs.fixedBtn.state.isArrowVisible)
      //     this.refs.fixedBtn.startFadeAnim(true);

      this._clampedScrollValue = Math.min(Math.max(this._clampedScrollValue + this.diff, 0), 50);
    });
  }

  cancelOrder = () => {
    this.listModal.toggleVisibility();
    this.setState({ isFetching: true });
    fetch(
      getEndPoint("ORDER_DETAILS_URL") +
        "?si=" +
        this.props.session.info.SessionID +
        "&i=1" +
        "&in=" +
        this.state.iadeNedeni +
        "&sno=" +
        this.props.orderDetails.info.SiparisDetayi.Baslik.SIPARIS_NO,
    )
      .then((res) => res.json())
      .then((responseJson) => {
        this.setState({ isFetching: false });
        Alert.alert("Sipariş İptali", responseJson.Message, [
          {
            text: "Tamam",
            onPress: () => {
              this.props.getOrderDetails(this.props.navigation.state.params.sNo);
            },
          },
        ]);
      })
      .catch((err) => {
        console.log("cancelOrder error", err);
        bugsnag.notify(err, (report) => {
          report.errorClass = "cancelOrder";
        });
      });
  };

  render() {
    const data = this.props.orderDetails.info.SiparisDetayi;
    return (
      <SafeAreaView
        style={{ backgroundColor: colors.primary_dark, flex: 1 }}
        forceInset={{ top: "always" }}
      >
        <View style={{ width: win.width, backgroundColor: "#efefef", flexGrow: 1 }}>
          {data && (
            <AnimatedScrollView
              scrollEventThrottle={16}
              contentContainerStyle={{
                flexGrow: 1,
                paddingBottom: moderateScale(20, 0.5),
                paddingTop:
                  Platform.OS === "android"
                    ? AnimatedHeader.height + (Platform.Version < 20 ? 0 : StatusBar.currentHeight)
                    : AnimatedHeader.height,
              }}
              onScroll={Animated.event(
                [
                  {
                    nativeEvent: { contentOffset: { y: this.state.scrollAnim } },
                  },
                ],
                { useNativeDriver: true },
              )}
            >
              {this.props.orderDetails.info.SiparisIptalEdilebilir && (
                <TouchableOpacity
                  onPress={() => this.listModal.toggleVisibility()}
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    marginHorizontal: moderateScale(10, 0.5),
                    marginTop: moderateScale(10, 0.5),
                  }}
                >
                  <Image
                    source={require("@assets/images/cancel.png")}
                    style={{
                      width: moderateScale(16, 0.5),
                      height: moderateScale(16, 0.5),
                    }}
                  />
                  <Text
                    style={{
                      fontSize: 13,
                      fontFamily: "Poppins",
                      marginLeft: moderateScale(5, 0.5),
                      top: 1,
                    }}
                  >
                    Siparişi İptal Et
                  </Text>
                </TouchableOpacity>
              )}
              {
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    marginHorizontal: moderateScale(10, 0.5),
                    marginTop: moderateScale(10, 0.5),
                  }}
                >
                  <Image
                    source={require("@assets/images/truck.png")}
                    style={{
                      marginRight: 5,
                      width: moderateScale(13, 0.2),
                      height: moderateScale(13, 0.3),
                      tintColor: "black",
                    }}
                  />
                  <Text style={{ fontSize: 13, fontFamily: "Poppins" }}>Kargo Takip: </Text>
                  <TouchableOpacity
                    onPress={() => {
                      if (
                        this.props.orderDetails.info.KargoTakipLinki != null &&
                        this.props.orderDetails.info.KargoTakipLinki !== ""
                      ) {
                        this.wvModal.toggleModal(
                          { uri: this.props.orderDetails.info.KargoTakipLinki },
                          "Kargo Takip",
                        );
                      }
                    }}
                  >
                    <Text style={{ fontSize: 13, color: "gray", flex: 1 }}>
                      {this.props.orderDetails.info.KargoTakipLinki != null &&
                      this.props.orderDetails.info.KargoTakipLinki !== ""
                        ? "Görüntülemek için tıklayın..."
                        : "Daha sonra tekrar kontrol ediniz..."}
                    </Text>
                  </TouchableOpacity>
                </View>
              }
              <View style={styles.container}>
                <View style={styles.titleContainer}>
                  <Text fontScale={0.6} style={styles.titleTxt}>
                    Fatura Adresi
                  </Text>
                </View>
                <View style={{ flexDirection: "column" }}>
                  <Text fontScale={0.6} style={styles.dataTxt}>
                    <Text fontScale={0.6}>{data.FaturaAlicisi.NAME1} </Text>
                    <Text fontScale={0.6}>{data.FaturaAlicisi.NAME2} </Text>
                    <Text fontScale={0.6}>{data.FaturaAlicisi.NAME3} </Text>
                    <Text fontScale={0.6}>{data.FaturaAlicisi.NAME4} </Text>
                    <Text fontScale={0.6}>{data.FaturaAlicisi.IL}</Text>
                  </Text>
                </View>
              </View>
              <View style={styles.container}>
                <View style={styles.titleContainer}>
                  <Text fontScale={0.6} style={styles.titleTxt}>
                    Teslimat Adresi
                  </Text>
                </View>
                <View style={{ flexDirection: "column" }}>
                  <Text fontScale={0.6}>
                    <Text fontScale={0.6} style={styles.dataTxt}>
                      {data.FaturaAlicisi.NAME1}{" "}
                    </Text>
                    <Text fontScale={0.6} style={styles.dataTxt}>
                      {data.FaturaAlicisi.NAME2}{" "}
                    </Text>
                    <Text fontScale={0.6} style={styles.dataTxt}>
                      {data.FaturaAlicisi.NAME3}{" "}
                    </Text>
                    <Text fontScale={0.6} style={[styles.dataTxt, { paddingBottom: 0 }]}>
                      {data.FaturaAlicisi.NAME4}{" "}
                    </Text>
                    <Text fontScale={0.6} style={styles.dataTxt}>
                      {data.FaturaAlicisi.IL}
                    </Text>
                  </Text>
                </View>
              </View>
              <View style={styles.container}>
                <View style={styles.titleContainer}>
                  <Text fontScale={0.6} style={styles.titleTxt}>
                    Arena Sipariş Bilgilieri
                  </Text>
                </View>
                <View style={{ flexDirection: "column" }}>
                  <Text fontScale={0.6} style={styles.dataTxt}>
                    {"Arena Sipariş No: " + data.Baslik.SIPARIS_NO}
                  </Text>
                  <Text fontScale={0.6} style={styles.dataTxt}>
                    {"SAS Sepet No: " + data.Baslik.SIPARIS_SAS}
                  </Text>
                  <Text fontScale={0.6} style={styles.dataTxt}>
                    {"Siparişin Alındığı Tarih: : " + data.Baslik.SIPARIS_TARIH}
                  </Text>
                  <Text fontScale={0.6} style={styles.dataTxt}>
                    {"Çıkış Yeri: " + data.Baslik.SIPARIS_URETIM_YERI_TEXT}
                  </Text>
                  <Text fontScale={0.6} style={styles.dataTxt}>
                    {"Teslimat Şekli: " + data.Baslik.SIPARIS_SEVK_SEKLI_TEXT}
                  </Text>
                </View>
              </View>
              <View style={styles.container}>
                <View style={styles.titleContainer}>
                  <Text fontScale={0.6} style={styles.titleTxt}>
                    Sipariş Tutar Bilgileri
                  </Text>
                </View>
                <View style={{ flexDirection: "column" }}>
                  <Text fontScale={0.6} style={styles.dataTxt}>
                    {"Net Tutar: " +
                      data.Baslik.SIPARIS_NET_TUTAR.toFixed(2).toString().replace(".", ",") +
                      " " +
                      data.Baslik.SIPARIS_PB}
                  </Text>
                  <Text fontScale={0.6} style={styles.dataTxt}>
                    {"KDV Tutar: " +
                      data.Baslik.SIPARIS_KDV_TUTAR.toFixed(2).toString().replace(".", ",") +
                      " " +
                      data.Baslik.SIPARIS_PB}
                  </Text>
                  <View style={styles.line} />
                  <Text
                    fontScale={0.6}
                    style={[styles.dataTxt, { paddingBottom: 0, fontSize: 16 }]}
                  >
                    {"Toplam Tutar: " +
                      data.Baslik.SIPARIS_TOPLAM_TUTAR.toFixed(2).toString().replace(".", ",") +
                      " " +
                      data.Baslik.SIPARIS_PB}
                  </Text>
                </View>
              </View>
              <View style={styles.container}>
                <View style={styles.titleContainer}>
                  <Text fontScale={0.6} style={styles.titleTxt}>
                    Alınan Ürünler
                  </Text>
                </View>
                {data.Kalemler.map((item, index) => {
                  return (
                    <View style={{ flexDirection: "column" }}>
                      <Text fontScale={0.6} style={styles.dataTxt}>
                        {item.KALEM_URUN_ACIKLAMA}
                      </Text>
                      <Text fontScale={0.6} style={styles.dataTxt}>
                        {item.KALEM_URUN_MIKTAR + " Adet"}
                      </Text>
                      <Text fontScale={0.6} style={styles.dataTxt}>
                        {"Toplam Tutar: " +
                          item.KALEM_BIRIM_TUTAR.toFixed(2).toString().replace(".", ",") +
                          " " +
                          item.KALEM_PB}
                      </Text>

                      {index < data.Kalemler.length - 1 && <View style={styles.line} />}
                    </View>
                  );
                })}
              </View>
            </AnimatedScrollView>
          )}
          <AnimatedHeader
            navigation={this.props.navigation}
            title="Sipariş Detayı"
            clampedScroll={this.state.clampedScroll}
            back={true}
            searchBar={true}
            alignLeft={true}
          />
        </View>
        <WebViewModal
          ref={(c) => (this.wvModal = c)}
          title={"Kargo Takip"}
          style={{ height: win.height * 0.8 }}
        />
        <ListModal
          contentContainerstyle={{ padding: moderateScale(10, 0.5) }}
          title="İptal Nedeni"
          ref={(c) => (this.listModal = c)}
          items={["Müşterim almaktan vazgeçti", "Yanlış sipariş vermişim", "Ödeme problemi"]}
          renderFooter={() => {
            return (
              <TouchableOpacity
                style={{
                  marginTop: moderateScale(10, 0.5),
                  height: moderateScale(30, 0.5),
                  paddingHorizontal: moderateScale(10, 0.5),
                  backgroundColor: colors.primary_dark,
                  borderRadius: 4,
                  alignItems: "center",
                  justifyContent: "center",
                }}
                disabled={this.state.iadeNedeni === -1}
                onPress={() => this.cancelOrder()}
              >
                <Text
                  style={{
                    fontFamily: "Poppins-Medium",
                    fontSize: 13,
                    color: "white",
                  }}
                >
                  İptal Et
                </Text>
              </TouchableOpacity>
            );
          }}
          renderItem={(item, index) => {
            return (
              <TouchableOpacity
                onPress={() => this.setState({ iadeNedeni: index + 1 })}
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  marginBottom: moderateScale(10, 0.5),
                }}
              >
                <RadioButton
                  onPress={() => this.setState({ iadeNedeni: index + 1 })}
                  style={{
                    width: moderateScale(20, 0.5),
                    height: moderateScale(20, 0.5),
                    borderRadius: moderateScale(10, 0.5),
                    marginRight: 8,
                  }}
                  innerStyle={{
                    width: moderateScale(10, 0.5),
                    height: moderateScale(10, 0.5),
                    borderRadius: moderateScale(5, 0.5),
                    backgroundColor: "#d6421e",
                  }}
                  selected={this.state.iadeNedeni === index + 1}
                />
                <Text
                  style={{
                    fontSize: 13,
                    fontFamily: "Poppins",
                    color: "gray",
                    marginLeft: moderateScale(5, 0.5),
                  }}
                >
                  {item}
                </Text>
              </TouchableOpacity>
            );
          }}
        />
        {(this.props.orderDetails.isFetching || this.state.isFetching) && (
          <View
            style={{
              width: win.width,
              height: win.height,
              justifyContent: "center",
              alignItems: "center",
              position: "absolute",
              top: 0,
              left: 0,
              backgroundColor: "rgba(0,0,0,0.3)",
            }}
          >
            <PencereIndicator />
          </View>
        )}
      </SafeAreaView>
    );
  }
}
function mapStateToProps(state) {
  return {
    orderDetails: state.orderDetails,
    session: state.session,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    getOrderDetails: (sNo) => dispatch(getOrderDetails(sNo)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(OrderDetailsScreen);

const styles = ScaledSheet.create({
  container: {
    flexDirection: "column",
    backgroundColor: "#fff",
    borderWidth: 1,
    borderRadius: 2,
    borderColor: "#ddd",
    borderBottomWidth: 0,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    shadowRadius: 2,
    elevation: 1,
    marginHorizontal: "10@ms0.5",
    marginTop: "10@ms0.5",
    padding: "10@ms0.5",
  },
  titleContainer: {
    borderBottomColor: "gray",
    borderBottomWidth: 0,
    marginBottom: "10@ms0.5",
    paddingBottom: 2,
  },
  titleTxt: {
    fontFamily: "Poppins-Medium",
    fontSize: 16,
    color: colors.price_color,
  },
  dataTxt: {
    fontFamily: "Poppins",
    fontSize: 12,
    color: "#7a7a7a",
    paddingVertical: 5,
  },
  line: {
    flex: 1,
    height: 1,
    backgroundColor: "#c4c4c4",
    marginVertical: 2,
  },
});
