import React, { Component } from "react";
import {
  StatusBar,
  SafeAreaView,
  View,
  StyleSheet,
  Dimensions,
  Image,
  FlatList,
  Animated,
  TouchableOpacity,
  RefreshControl,
  Platform,
} from "react-native";
import {
  DatePickerDialog,
  AnimatedHeader,
  BottomPicker,
  Text,
  PencereIndicator,
} from "@components";
import { DatePanel, OrderComponent } from "@features/siparisler/components";
import { searchOrders } from "@redux/actions";
import { connect } from "react-redux";
import colors from "@common/styles/colors";
import { moderateScale } from "react-native-size-matters";
import moment from "pencere/moment";

const win = Dimensions.get("window");
const NAVBAR_HEIGHT = AnimatedHeader.height;
const STATUS_BAR_HEIGHT = 0;
const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);

class OrdersScreen extends Component {
  constructor(props) {
    super(props);
    const scrollAnim = new Animated.Value(0);
    var today = moment();
    var lastWeek = moment().subtract("7", "days");
    this.state = {
      showDatePanel: false,
      refreshing: false,
      isCollapsed: false,
      start: lastWeek,
      end: today,
      total: 0,
      scrollAnim,
      dateRange: "1 hafta",
      clampedScroll: Animated.diffClamp(
        Animated.add(
          Animated.add(scrollAnim, 130).interpolate({
            inputRange: [0, 1],
            outputRange: [0, 1],
            extrapolateLeft: "clamp",
          }),
          -130,
        ),
        0,
        NAVBAR_HEIGHT - STATUS_BAR_HEIGHT,
      ),
    };
    this.dates = ["3 ay", "1 ay", "1 hafta", "Bugün"];
    this.selectDate = this.selectDate.bind(this);
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      prevProps.orders.isFetching &&
      this.props.orders.isFetching !== prevProps.orders.isFetching &&
      !this.props.orders.isFetching
    ) {
      this.setState({
        refreshing: false,
      });
    }
  }
  componentDidMount() {
    this.props.searchOrders(
      this.state.start.format().split("T")[0],
      this.state.end.format().split("T")[0],
      false,
    );
  }

  onChangeDate = (date, id) => {
    if (id === "1") {
      this.setState({
        start: date,
      });
      this.props.searchOrders(
        date.format().split("T")[0],
        this.state.end.format().split("T")[0],
        false,
      );
    } else if (id === "2") {
      this.setState({
        end: date,
      });
      this.props.searchOrders(
        this.state.start.format().split("T")[0],
        date.format().split("T")[0],
        false,
      );
    }
  };

  toggleCollapse(state) {
    console.log("toggle", state);
    this.setState(state);
  }

  _onRefresh() {
    this.setState({ refreshing: true });
    this.props.searchOrders(
      this.state.start.format().split("T")[0],
      this.state.end.format().split("T")[0],
      true,
    );
  }
  selectDate(val, dateIndex) {
    var startDate = moment();
    var endDate = moment();

    switch (dateIndex) {
      case 0:
        startDate = moment().subtract("90", "days");
        this.setState({
          dateRange: this.dates[dateIndex],
          start: startDate,
          end: endDate,
        });
        break;
      case 1:
        startDate = moment().subtract("30", "days");
        this.setState({
          dateRange: this.dates[dateIndex],
          start: startDate,
          end: endDate,
        });
        break;
      case 2:
        startDate = moment().subtract("7", "days");
        this.setState({
          dateRange: this.dates[dateIndex],
          start: startDate,
          end: endDate,
        });
        break;
      case 3:
        startDate = moment();
        this.setState({
          dateRange: this.dates[dateIndex],
          start: startDate,
          end: endDate,
        });
        break;
      default:
        startDate = moment().subtract("7", "days");
        this.setState({
          dateRange: "1 hafta",
          start: startDate,
          end: moment(),
        });
    }
    this.props.searchOrders(startDate.format().split("T")[0], endDate.format().split("T")[0]);
  }

  onRefresh(startDate) {
    this.props.searchOrders(startDate.format().split("T")[0], this.today.format().split("T")[0]);
  }

  render() {
    const datePanelTranslate = this.state.clampedScroll.interpolate({
      inputRange: [0, moderateScale(50, 0.4)],
      outputRange: [moderateScale(100, 0.4), moderateScale(50, 0.4)],
      extrapolate: "clamp",
    });
    return (
      <SafeAreaView
        style={{ backgroundColor: colors.primary_dark, flex: 1 }}
        forceInset={{ top: "always" }}
      >
        <View style={{ flexGrow: 1, width: win.width, backgroundColor: "#efefef" }}>
          {this.props.orders.ordersInfo.ilSiparisler && (
            <AnimatedFlatList
              data={this.props.orders.ordersInfo.ilSiparisler}
              renderItem={({ item }) => (
                <OrderComponent item={item} navigation={this.props.navigation} />
              )}
              ListEmptyComponent={() =>
                !this.props.orders.isFetching &&
                !this.props.orders.isRefreshing && (
                  <View
                    style={{
                      width: win.width,
                      marginTop: 8,
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <Text style={{ fontSize: 14, color: "gray" }}>
                      Görüntülenecek sipariş bulunamadı.
                    </Text>
                  </View>
                )
              }
              removeClippedSubviews={false}
              scrollEventThrottle={16}
              contentOffset={{ y: -moderateScale(140, 0.4) }}
              contentInset={{
                top: moderateScale(140, 0.4),
                left: 0,
                bottom: 0,
                right: 0,
              }}
              contentContainerStyle={{
                flexGrow: 1,
                paddingBottom: moderateScale(20, 0.5),
                paddingTop:
                  Platform.OS === "android"
                    ? moderateScale(140, 0.4) +
                      (Platform.Version < 20 ? 0 : StatusBar.currentHeight)
                    : 0,
              }}
              refreshControl={
                <RefreshControl
                  refreshing={this.props.orders.isRefreshing}
                  onRefresh={this._onRefresh.bind(this)}
                  progressViewOffset={130}
                />
              }
              onScroll={Animated.event(
                [
                  {
                    nativeEvent: { contentOffset: { y: this.state.scrollAnim } },
                  },
                ],
                { useNativeDriver: true },
              )}
            />
          )}
          <Animated.View
            style={{
              width: win.width,
              height: moderateScale(36, 0.5),
              alignItems: "center",
              justifyContent: "center",
              backgroundColor: "white",
              flexDirection: "column",
              transform: [{ translateY: datePanelTranslate }],
              position: "absolute",
              borderBottomWidth: 0,
              top:
                Platform.OS === "android"
                  ? Platform.Version < 20
                    ? 0
                    : StatusBar.currentHeight
                  : 0,
              shadowColor: "#000",
              shadowOffset: { width: 0, height: 1 },
              shadowOpacity: 0.2,
              shadowRadius: 1,
              elevation: 1,
            }}
          >
            {
              <TouchableOpacity
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  width: win.width,
                  paddingHorizontal: 10,
                  backgroundColor: "white",
                }}
                onPress={() => this.bottomPicker.openPicker()}
              >
                <View style={{ flexDirection: "row", alignItems: "center" }}>
                  <Text style={{ fontSize: 16, color: "gray" }}>
                    {"Tarih Seçin:   " + this.state.dateRange}
                  </Text>
                  <Image
                    style={{
                      width: 16,
                      height: 16,
                      resizeMode: "contain",
                      tintColor: "gray",
                      marginHorizontal: 5,
                      marginTop: 2,
                    }}
                    source={require("@assets/images/down.png")}
                  />
                </View>
                {/* <TouchableOpacity style={{ flexDirection: 'row', }} onPress={() => { this.setState({ showDatePanel: !this.state.showDatePanel }) }}>
                            <Image style={{ width: 16, height: 16, resizeMode: 'contain', tintColor: 'gray', marginHorizontal: 5, marginTop: 2, padding: 2 }} source={require('@assets/images/calendar.png')} />
                        </TouchableOpacity> */}
              </TouchableOpacity>
            }

            {this.state.showDatePanel && (
              <View
                style={{
                  position: "absolute",
                  top: 0,
                  left: 0,
                  width: win.width,
                  height: win.height,
                  backgroundColor: "rgba(0,0,0,0.5)",
                  alignItems: "center",
                }}
              >
                <DatePanel
                  ref={c => (this.datePanel = c)}
                  isCollapsed={this.state.isCollapsed}
                  toggleCollapse={this.toggleCollapse.bind(this)}
                  style={{
                    backgroundColor: "white",
                    position: "absolute",
                    top: win.height / 2 - 200,
                    width: win.width - 20,
                  }}
                  openDatePickerIOS={(date, id, minimumDate, maximumDate) =>
                    this.datePickerDialog.open(date, id, minimumDate, maximumDate)
                  }
                  onChangeDate={this.onChangeDate}
                  start={this.state.start}
                  end={this.state.end}
                  closeDatePanel={() => {
                    this.setState({ showDatePanel: false });
                    this.datePickerDialog.close();
                  }}
                />
              </View>
            )}
          </Animated.View>

          <DatePickerDialog
            ref={c => (this.datePickerDialog = c)}
            onChangeDate={this.onChangeDate}
            setDate={this.setDate}
            collapseDatesView={() => this.setState({ isCollapsed: true })}
          />
        </View>
        {this.props.orders.isFetching && !this.state.refreshing && (
          <View style={[styles.loadingContainer]}>
            <PencereIndicator />
          </View>
        )}
        {
          <BottomPicker
            ref={c => (this.bottomPicker = c)}
            items={this.dates}
            initialItem={this.state.dateRange}
            onPressOK={this.selectDate}
          />
        }
        <AnimatedHeader
          navigation={this.props.navigation}
          title="Siparişlerim"
          clampedScroll={this.state.clampedScroll}
          searchBar={true}
          search={false}
          back={true}
          alignLeft={true}
          onBackPressed={() => this.props.navigation.navigate("MyAccountScreen")}
        />
      </SafeAreaView>
    );
  }
}

function mapStateToProps(state) {
  return {
    orders: state.orders,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    searchOrders: (startDate, endDate, refresh) =>
      dispatch(searchOrders(startDate, endDate, refresh)),
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(OrdersScreen);

const styles = StyleSheet.create({
  loadingContainer: {
    height: win.height,
    width: win.width,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0,0,0,0.3)",
    position: "absolute",
  },
});
