import {
  SEARCH_ORDERS,
  SEARCH_ORDERS_SUCCESS,
  SEARCH_ORDERS_FAILURE,
  REFRESH_ORDERS,
} from "@redux/actions/types";
import bugsnag from "@common/bugsnag_config";
import { getEndPoint } from "@common/Utils";
export function searchOrders(startDate, endDate, refresh) {
  return (dispatch, getState) => {
    if (refresh)
      dispatch({
        type: REFRESH_ORDERS,
      });
    else
      dispatch({
        type: SEARCH_ORDERS,
      });
    fetch(
      getEndPoint("SEARCH_ORDERS_URL") +
        "?Si=" +
        getState().session.info.SessionID +
        "&sd=" +
        startDate +
        "&ed=" +
        endDate,
    )
      .then((response) => response.json())
      .then((responseJson) => {
        dispatch({
          type: SEARCH_ORDERS_SUCCESS,
          data: responseJson,
        });
      })
      .catch((error) => {
        bugsnag.notify(error, (report) => {
          report.errorClass = "searchOrders";
        });
        console.log(error);
        dispatch({
          type: SEARCH_ORDERS_FAILURE,
        });
      });
  };
}
