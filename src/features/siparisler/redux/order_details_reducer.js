const INITAL_STATE = {
  info: [],
  isFetching: false,
  error: false,
};

import {
  GET_ORDER_DETAILS,
  GET_ORDER_DETAILS_SUCCESS,
  GET_ORDER_DETAILS_FAILURE,
} from "@redux/actions/types";

export default function orderDetailsReducer(state = INITAL_STATE, action) {
  switch (action.type) {
    case GET_ORDER_DETAILS:
      return {
        ...state,
        isFetching: true,
        error: false,
      };
    case GET_ORDER_DETAILS_SUCCESS:
      return {
        ...state,
        info: action.data,
        isFetching: false,
      };
    case GET_ORDER_DETAILS_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true,
        info: [],
      };
    default:
      return state;
  }
}
