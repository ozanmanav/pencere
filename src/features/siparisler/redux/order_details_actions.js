import {
  GET_ORDER_DETAILS,
  GET_ORDER_DETAILS_SUCCESS,
  GET_ORDER_DETAILS_FAILURE,
} from "@redux/actions/types";
import bugsnag from "@common/bugsnag_config";
import { getEndPoint } from "@common/Utils";

export function getOrderDetails(siparisNo) {
  return (dispatch, getState) => {
    dispatch({
      type: GET_ORDER_DETAILS,
    });
    fetch(
      getEndPoint("ORDER_DETAILS_URL") +
        "?Si=" +
        getState().session.info.SessionID +
        "&Sno=" +
        siparisNo,
    )
      .then((response) => response.json())
      .then((responseJson) => {
        dispatch({
          type: GET_ORDER_DETAILS_SUCCESS,
          data: responseJson,
        });
      })
      .catch((error) => {
        bugsnag.notify(error, (report) => {
          report.errorClass = "getOrderDetails";
        });
        console.log(error);
        dispatch({
          type: GET_ORDER_DETAILS_FAILURE,
        });
      });
  };
}
