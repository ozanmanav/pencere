const INITAL_STATE = {
  ordersInfo: [],
  isFetching: false,
  isRefreshing: false,
  error: false,
};

import {
  SEARCH_ORDERS,
  SEARCH_ORDERS_SUCCESS,
  SEARCH_ORDERS_FAILURE,
  REFRESH_ORDERS,
} from "@redux/actions/types";

export default function ordersReducer(state = INITAL_STATE, action) {
  switch (action.type) {
    case SEARCH_ORDERS:
      return {
        ...state,
        isFetching: true,
        error: false,
      };
    case REFRESH_ORDERS:
      return {
        ...state,
        isRefreshing: true,
        error: false,
      };
    case SEARCH_ORDERS_SUCCESS:
      return {
        ...state,
        ordersInfo: action.data,
        isFetching: false,
        isRefreshing: false,
      };
    case SEARCH_ORDERS_FAILURE:
      return {
        ...state,
        isFetching: false,
        refreshing: false,
        error: true,
        ordersInfo: [],
      };
    default:
      return state;
  }
}
