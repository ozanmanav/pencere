import { OPEN_DRAWER, CLOSE_DRAWER } from "@redux/actions/types";
import { BackHandler } from "react-native";
import store from "@redux/store";

var handler = () => {
  store.dispatch(closeDrawer());
  return true;
};

export function openDrawer() {
  BackHandler.addEventListener("hardwareBackPress", handler);
  return (dispatch) => {
    dispatch({ type: OPEN_DRAWER });
  };
}

export function closeDrawer() {
  BackHandler.removeEventListener("hardwareBackPress", handler);
  return (dispatch) => {
    dispatch({ type: CLOSE_DRAWER });
  };
}
