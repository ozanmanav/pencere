import { SHOW_TOAST } from "@redux/actions/types";

export function showToast(msg, duration) {
  return (dispatch) => {
    dispatch({ type: SHOW_TOAST, msg, duration });
  };
}
