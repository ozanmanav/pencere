import { SET_VISIBILITY } from "@redux/actions/types";

export function setLiveChatVisibility(isVisible) {
  return (dispatch) => {
    dispatch({ type: SET_VISIBILITY, isVisible });
  };
}
