import { SET_APP_STATE } from "./types";

export function setAppState(newParams) {
  return (dispatch) => {
    dispatch({ type: SET_APP_STATE, newParams });
  };
}
