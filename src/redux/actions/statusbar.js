import { SET_STATUSBAR_COLOR } from "@redux/actions/types";
import colors from "@common/styles/colors";

export function setSafeViewBackgroundColor(color, bottomTrickColor) {
  return (dispatch) => {
    dispatch({
      type: SET_STATUSBAR_COLOR,
      color,
      bottomTrickColor:
        typeof bottomTrickColor !== "undefined" ? bottomTrickColor : colors.primary_dark,
    });
  };
}
