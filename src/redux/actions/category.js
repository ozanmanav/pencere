import {
  GET_CATEGORIES,
  GET_CATEGORIES_SUCCESS,
  GET_CATEGORIES_FAILURE,
  SELECT_CATEGORY,
} from "./types";
import bugsnag from "@common/bugsnag_config";
import { getEndPoint } from "@common/Utils";

export function fetchCategoriesFromApi() {
  return (dispatch) => {
    dispatch({ type: GET_CATEGORIES });
    return fetch(getEndPoint("CATEGORIES_URL"))
      .then((response) => {
        if (response.ok) return response.json();
        else {
          dispatch(getCategoriesFailure());
        }
      })
      .then((responseJson) => {
        dispatch(getCategoriesSuccess(responseJson));
        return Promise.resolve();
      })
      .catch((error) => {
        bugsnag.notify(error, (report) => {
          report.errorClass = "fetchCategoriesFromApi";
        });
        console.error(error);
        dispatch(getCategoriesFailure());
      });
  };
}

export function selectCategory(cat) {
  return (dispatch) => {
    dispatch({ type: SELECT_CATEGORY, cat });
  };
}

function getCategoriesSuccess(data) {
  return {
    type: GET_CATEGORIES_SUCCESS,
    data,
  };
}

function getCategoriesFailure() {
  return {
    type: GET_CATEGORIES_FAILURE,
  };
}
