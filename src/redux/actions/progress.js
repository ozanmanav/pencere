import { SET_GLOBAL_PROGRESS } from "@redux/actions/types";

export function setGlobalProgress(inProgress) {
  return (dispatch) => {
    dispatch({ type: SET_GLOBAL_PROGRESS, inProgress });
  };
}
