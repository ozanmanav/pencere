import { createStore, applyMiddleware, compose } from "redux";
import app from "../reducers";
import thunk from "redux-thunk";

function configureStore() {
  let store = createStore(
    app,
    compose(
      applyMiddleware(thunk),
      window.devToolsExtension ? window.devToolsExtension() : (f) => f,
    ),
  );
  return store;
}
const store = configureStore();
export default store;
