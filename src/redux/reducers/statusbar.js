import { SET_STATUSBAR_COLOR } from "@redux/actions/types";
import colors from "@common/styles/colors";

const INITAL_STATE = {
  backgroundColor: colors.primary_dark,
  bottomTrickColor: colors.primary_dark,
};

export default function drawerReducer(state = INITAL_STATE, action) {
  switch (action.type) {
    case SET_STATUSBAR_COLOR:
      return {
        ...state,
        backgroundColor: action.color,
        bottomTrickColor: action.bottomTrickColor,
      };
    default:
      return state;
  }
}
