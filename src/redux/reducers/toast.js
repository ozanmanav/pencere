const INITAL_STATE = {
  toggle: false,
  msg: "",
  duration: 300,
};

import { SHOW_TOAST } from "@redux/actions/types";

export default function drawerReducer(state = INITAL_STATE, action) {
  switch (action.type) {
    case SHOW_TOAST:
      return {
        ...state,
        toggle: !state.toggle,
        msg: action.msg || state.msg,
        duration: action.duration || state.duration,
      };
    default:
      return state;
  }
}
