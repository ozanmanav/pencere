const INITAL_STATE = {
  isOpen: false,
};
import { OPEN_DRAWER, CLOSE_DRAWER } from "@redux/actions/types";

export default function drawerReducer(state = INITAL_STATE, action) {
  switch (action.type) {
    case OPEN_DRAWER:
      return {
        ...state,
        isOpen: true,
      };
    case CLOSE_DRAWER:
      return {
        ...state,
        isOpen: false,
      };
    default:
      return state;
  }
}
