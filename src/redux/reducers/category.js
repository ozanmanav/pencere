const INITAL_STATE = {
  items: [],
  error: false,
  selectedCategory: "",
  isFetching: false,
};
import {
  GET_CATEGORIES_SUCCESS,
  GET_CATEGORIES_FAILURE,
  SELECT_CATEGORY,
  GET_CATEGORIES,
} from "@redux/actions/types";

export default function categoryReducer(state = INITAL_STATE, action) {
  switch (action.type) {
    case GET_CATEGORIES:
      return {
        ...state,
        isFetching: true,
      };
    case GET_CATEGORIES_SUCCESS:
      return {
        ...state,
        items: action.data,
        isFetching: false,
      };
    case GET_CATEGORIES_FAILURE:
      return {
        ...state,
        error: true,
        isFetching: false,
      };
    case SELECT_CATEGORY:
      return {
        ...state,
        selectedCategory: action.cat,
      };
    default:
      return state;
  }
}
