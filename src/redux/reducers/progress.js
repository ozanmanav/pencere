const INITAL_STATE = {
  inProgress: false,
};

import { SET_GLOBAL_PROGRESS } from "@redux/actions/types";

export default function progress(state = INITAL_STATE, action) {
  switch (action.type) {
    case SET_GLOBAL_PROGRESS:
      return {
        ...state,
        inProgress: action.inProgress,
      };
    default:
      return state;
  }
}
