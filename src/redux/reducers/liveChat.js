const INITAL_STATE = {
  isVisible: false,
};

import { SET_VISIBILITY } from "@redux/actions/types";

export default function drawerReducer(state = INITAL_STATE, action) {
  switch (action.type) {
    case SET_VISIBILITY:
      return {
        ...state,
        isVisible: action.isVisible,
      };
    default:
      return state;
  }
}
