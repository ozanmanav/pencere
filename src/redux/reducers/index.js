import { combineReducers } from "redux";
import session from "../../features/giris/redux/session_reducer";
import cart from "../../features/sepet/redux/cart_reducer";
import category from "./category";
import search from "@features/arama/redux/search_reducer";
import products from "../../features/anasayfa/redux/products_reducer";
import details from "../../features/urunDetay/redux/details_reducer";
import orders from "../../features/siparisler/redux/orders_reducer";
import orderDetails from "../../features/siparisler/redux/order_details_reducer";
import accounts from "../../features/hesabim/redux/accounts_reducer";
import filters from "../../features/arama/redux/filters_reducer";
import homePage from "@features/anasayfa/redux/home_page_reducer";
import favorites from "../../features/favorilerim/redux/favorites_reducer";
import searchHistory from "../../features/arama/redux/search_history_reducer";
import placeOrder from "../../features/siparis/redux/place_order_reducer";
import payment from "../../features/siparis/redux/payment_reducer";
import instalment from "../../features/siparis/redux/instalment_reducer";
import drawer from "./drawer";
import invoices from "../../features/hesabim/redux/invoices_reducer";
import paynetCampaigns from "../../features/hesabim/redux/paynet_campaigns_reducer";
import cambiums from "../../features/hesabim/redux/cambiums_reducer";
import announcements from "../../features/duyurular/redux/announcements_reducer";
import toast from "./toast";
import statusbar from "./statusbar";
import progress from "./progress";
import liveChat from "./liveChat";
import appState from "./app_state";
import priceRequests from "../../features/hesabim/redux/price_requests_reducer";

const rootReducer = combineReducers({
  session,
  cart,
  search,
  category,
  products,
  details,
  orders,
  orderDetails,
  accounts,
  filters,
  homePage,
  favorites,
  searchHistory,
  placeOrder,
  payment,
  instalment,
  drawer,
  invoices,
  paynetCampaigns,
  cambiums,
  announcements,
  toast,
  progress,
  statusbar,
  liveChat,
  appState,
  priceRequests,
});

export default rootReducer;
