const INITAL_STATE = {
  stage: "PROD",
};
import { SET_APP_STATE } from "@redux/actions/types";
import { Api, ApiMode } from "@common/api";

export default function appStateReducer(state = INITAL_STATE, action) {
  switch (action.type) {
    case SET_APP_STATE:
      if (action.newParams.stage === "TEST") {
        Api.createApi(ApiMode.TEST);
      } else if (action.newParams.stage === "PROD") {
        Api.createApi(ApiMode.LIVE);
      }
      return {
        ...state,
        ...action.newParams,
      };
    default:
      return state;
  }
}
